//IN THIS SCRIPT//
//// control over collapses, changing page, changing category, changing activity, changing completion ////

$(function(){
  $('[data-toggle="popover"]').popover({ html: true });
  $('[data-toggle="tooltip"]').tooltip({trigger : 'hover'});
  $('[rel="tooltip"]').on('click', function () {
    $(this).tooltip('hide')
  })
  /*
  if($('#taskId').html() == 'OK'){
    selectedTaskId = urlParams.get('taskId');
    setTimeout(()=>{
      smoothScroll('#task'+selectedTaskId, 100); flashAnimation('#task'+selectedTaskId);
    })
  }
  */
  if(completionTask == 0)
    $('#optionAllCompletion').addClass('active');
  else if(completionTask == 1)
    $('#optionUncomplete').addClass('active');
  else if(completionTask == 2)
    $('#optionComplete').addClass('active');
  if($('#taskId').html() == '9'){
    //task
    selectedTaskId = urlParams.get('taskId');
    setTimeout(()=>{
      smoothScroll('#task'+selectedTaskId, 100); flashAnimation('#task'+selectedTaskId);
    })
  }
  else if($('#taskId').html() == '10'){
    //subtask
    selectedTaskId = urlParams.get('taskId');
    selectedSubtaskId = urlParams.get('subtaskId');
    setTimeout(()=>{
      smoothScroll('#task'+selectedTaskId, 100); 
      setTimeout(()=>{
        toggleCollapse(selectedTaskId,1);
        setTimeout(()=>{
          anotherSmoothScroll('#subtaskList'+selectedTaskId,'#subtask'+selectedSubtaskId); 
          flashAnimation('#subtask'+selectedSubtaskId);
        },500);
      },500);
    },100);
  }
  else if($('#taskId').html() == '11'){
    //note
    selectedTaskId = urlParams.get('taskId');
    selectedSubtaskId = urlParams.get('subtaskId');
    selectedNoteId = urlParams.get('noteId');
    if(selectedSubtaskId){
      setTimeout(()=>{
        smoothScroll('#task'+selectedTaskId, 100); 
        setTimeout(()=>{
          toggleCollapse(selectedTaskId,1);
          setTimeout(()=>{
            anotherSmoothScroll('#subtaskList'+selectedTaskId,'#subtask'+selectedSubtaskId); 
            //flashAnimation('#subtask'+selectedSubtaskId);
            setTimeout(()=>{
              openMessagesModal(selectedSubtaskId,2);
              setTimeout(()=>{
                flashAnimation('#message'+selectedNoteId);
              },500)
            },200);
          },500);
        },500);
      },100);
    }
    else{
      setTimeout(()=>{
        smoothScroll('#task'+selectedTaskId, 100); 
        setTimeout(()=>{
          openMessagesModal(selectedTaskId,1);
          setTimeout(()=>{
            flashAnimation('#message'+selectedNoteId);
          },500)
        },200);
      })
    }
  }
  else if($('#taskId').html() == '12'){
  //absence
  }
  else if($('#taskId').html() == '13'){
  //file
    selectedTaskId = urlParams.get('taskId');
    selectedFileId = urlParams.get('fileId');
    //task's file, it has taskId
    setTimeout(()=>{
      smoothScroll('#task'+selectedTaskId, 100); 
      setTimeout(()=>{
        toggleCollapse(selectedTaskId,2);
        setTimeout(()=>{
          anotherSmoothScroll('#fileList'+selectedTaskId,'#taskFile'+selectedFileId); 
          flashAnimation('#taskFile'+selectedFileId);
        },500);
      },100);
    },100);
  }
  loggedUser = {name:$('#loggedUser').html().split(" (")[0].split(" ")[0], surname:$('#loggedUser').html().split(" (")[0].split(" ")[1], role:$('#loggedUser').html().split(" (")[1].split(")")[0].toLowerCase()};
  if(loggedUser.role == 'admin' || loggedUser.role.substring(0,5) == 'vodja' || loggedUser.role == 'tajnik' || loggedUser.role == 'komercialist' || loggedUser.role == 'komerciala' || loggedUser.role == 'računovodstvo' || loggedUser.role == 'programer plc-jev' || loggedUser.role == 'programer robotov' || loggedUser.role == 'konstrukter' || loggedUser.role == 'kontruktor')
    permissionTag = true;

  $.get( "/subscribers/all", function( data ){
    allSubscribers = data.data;

    $(".select2-subscriberAdd").select2({
      data: allSubscribers,
      tags: true,
    });
  })
  //LOCALSTORAGE
  //show n tasks
  showNServices = localStorage.showNServices ? localStorage.showNServices : '10';
  if(showNServices != '10')
    $('#selectNumberServices').val(showNServices);
  //on events
  $('#selectNumberServices').on('change',onChangeSelectShowNServices);

  //set variables with session ones if they exist
  activeTask = sessionStorage.sessionActiveService ? parseInt(sessionStorage.sessionActiveService) : 1;
  categoryTask = sessionStorage.sessionCategoryService ? parseInt(sessionStorage.sessionCategoryService) : 0;
  completionTask = sessionStorage.sessionCompletionService ? parseInt(sessionStorage.sessionCompletionService) : 1;
  showMyInputTasks = sessionStorage.sessionMyServiceInputSwitch == 'true' ? true : false;
  showMyTasks = sessionStorage.sessionMyServiceSwitch == 'true' ? true : false;
  $('#showMyInputTasksSwitch').prop('checked', showMyInputTasks);
  $('#showMyTasksSwitch').prop('checked', showMyTasks);

  $('#optionAll').removeClass('active stay-focus');
  $('#optionTrue').removeClass('active stay-focus');
  $('#optionFalse').removeClass('active stay-focus');
  switch (activeTask) {
    case 0: $('#optionAll').addClass('active stay-focus'); break;
    case 1: $('#optionTrue').addClass('active stay-focus'); break;
    case 2: $('#optionFalse').addClass('active stay-focus'); break;
    default: $('#optionTrue').addClass('active stay-focus'); break;
  }
  $('#optionAllCompletion').removeClass('active stay-focus');
  $('#optionUncomplete').removeClass('active stay-focus');
  $('#optionComplete').removeClass('active stay-focus');
  switch (completionTask) {
    case 0: $('#optionAllCompletion').addClass('active stay-focus'); break;
    case 1: $('#optionUncomplete').addClass('active stay-focus'); break;
    case 2: $('#optionComplete').addClass('active stay-focus'); break;
    default: $('#optionUncomplete').addClass('active stay-focus'); break;
  }

  changeTaskTable(categoryTask);

  //on event call functions
  $('#taskPages').on('click', 'li', changePage);
  $('#categoryBtnGroup').on('click', 'button', function (event) {
    changeTaskTable(this.value);
  });
  $('#completionBtnGroup').on('click', 'button', function (event) {
    changeCompletion(this.value);
  });
  $('#activityBtnGroup').on('click', 'button', function (event) {
    changeActive(this.value);
  });
  $('#showMyInputTasksSwitch').on('click', changeToMyInputTasks);
  $('#showMyTasksSwitch').on('click', changeToMyTasks);
  $('#btnAddService').on('click', function (event) {
    addNewTask(0);
  });
  $('#addTaskBtnOR').on('click', function (event) {
    addNewTask(1);
  });
  $('#editTaskBtnOR').on('click', function (event) {
    updateEditTask(1);
  });

  $('#btnAddTaskDoc').on('click', function (event) {
    setTimeout(disableFunction, 1);
  });
})
function onEventToggleCollapse(event) {
  toggleCollapse(event.data.taskId, event.data.type);
}
//OPEN EDIT TASK COLLAPSE
function toggleCollapse(taskId, type){
  //debugger;
  //if == 0 then there is no open collapse or open is one of this task's collapse
  if(activeTaskId == 0){
    activateTask(taskId, type);
  }
  //there is open collapse
  //close collapse, empty it and open this collapse
  else{
    if(editTaskCollapseOpen){
      $('#collapseEditTask'+activeTaskId).collapse("toggle");
      editTaskCollapseOpen = false;
      $('#collapseEditTask'+activeTaskId).empty();
      //it was open, want to close collapse edit
      if(type == 0 && taskId == activeTaskId)
        activeTaskId = 0;
      else
        activateTask(taskId, type);
    }
    else if(subtaskCollapseOpen){
      $('#collapseSubtasks'+activeTaskId).collapse("toggle");
      subtaskCollapseOpen = false;
      $('#collapseSubtasks'+activeTaskId).empty();
      //it was open, want to close collapse subtasks
      if(type == 1 && taskId == activeTaskId)
        activeTaskId = 0;
      else
        activateTask(taskId, type);
    }
    else if(taskFilesCollapseOpen){
      $('#collapseTaskFiles'+activeTaskId).collapse("toggle");
      taskFilesCollapseOpen = false;
      $('#collapseTaskFiles'+activeTaskId).empty();
      //it was open, want to close collapse files
      if(type == 2 && taskId == activeTaskId)
        activeTaskId = 0;
      else
        activateTask(taskId, type);
    }
    else if(taskAbsenceCollapseOpen){
      $('#collapseTaskAbsence'+activeTaskId).collapse("toggle");
      taskAbsenceCollapseOpen = false;
      $('#collapseTaskAbsence'+activeTaskId).empty();
      //it was open, want to close collapse absence
      if(type == 3 && taskId == activeTaskId)
        activeTaskId = 0;
      else
        activateTask(taskId, type);
    }
  }
}
function activateTask(taskId, type){
  activeTaskId = taskId;
  if(type == 0)
    editTask(taskId);
  else if(type == 1)
    openSubtasks(taskId);
  else if(type == 2)
    openTaskFiles(taskId);
  else if(type == 3)
    openTaskAbsence(taskId);
}
//SHOW ONLY MY TASKS(tasks that i have created)
function changeToMyInputTasks(){
  //debugger;
  showMyInputTasks = showMyInputTasks ? false : true;
  sessionStorage.sessionMyServiceInputSwitch = showMyInputTasks;
  
  $.get( "/servis/tasks", {categoryTask, activeTask, completionTask, showMyInputTasks, showMyTasks}, function( data ) {
    //console.log(data)
    debugger
    allTasks = data.data;
    //shownTasks = data.data.splice(0,10);
    taskLoaded = true;
    //drawPagination(Math.ceil(allTasks.length/10));
    if(showNServices == '-1')
      drawPagination(1);
    else
      drawPagination(Math.ceil(allTasks.length/parseInt(showNServices)));
    drawTasks(1);
    //$('[data-toggle="popover"]').popover({trigger: "hover"});
  });
}
//SHOW ONLY MY TASKS(tasks that i have created)
function changeToMyTasks(){
  //debugger;
  showMyTasks = showMyTasks ? false : true;
  sessionStorage.sessionMyServiceSwitch = showMyTasks;
  
  $.get( "/servis/tasks", {categoryTask, activeTask, completionTask, showMyInputTasks, showMyTasks}, function( data ) {
    //console.log(data)
    debugger
    allTasks = data.data;
    //shownTasks = data.data.splice(0,10);
    taskLoaded = true;
    //drawPagination(Math.ceil(allTasks.length/10));
    if(showNServices == '-1')
      drawPagination(1);
    else
      drawPagination(Math.ceil(allTasks.length/parseInt(showNServices)));
    drawTasks(1);
    //$('[data-toggle="popover"]').popover({trigger: "hover"});
  });
}
//CHANGE CATEGORY FOR ALL TASKS
function changeTaskTable(category){
  //debugger;
  $('#option0').removeClass('active stay-focus');
  $('#option1').removeClass('active stay-focus');
  $('#option2').removeClass('active stay-focus');
  $('#option3').removeClass('active stay-focus');
  $('#option4').removeClass('active stay-focus');
  $('#option5').removeClass('active stay-focus');
  $('#option6').removeClass('active stay-focus');
  $('#option7').removeClass('active stay-focus');
  setTimeout(()=>{
    $('#option'+category+'').addClass('active stay-focus');
  })
  categoryTask = category;
  sessionStorage.sessionCategoryService = category;
  if($('#optionAll').hasClass('active'))
    activeTask = 0;
  else if($('#optionTrue').hasClass('active'))
    activeTask = 1;
  else if($('#optionFalse').hasClass('active'))
    activeTask = 2;
  $.get( "/servis/tasks", {categoryTask, activeTask, completionTask, showMyInputTasks, showMyTasks}, function( data ) {
    //console.log(data)
    debugger
    allTasks = data.data;
    //shownTasks = data.data.splice(0,10);
    taskLoaded = true;
    //drawPagination(Math.ceil(allTasks.length/10));
    if(showNServices == '-1')
      drawPagination(1);
    else
      drawPagination(Math.ceil(allTasks.length/parseInt(showNServices)));
    drawTasks(1);
    //$('[data-toggle="popover"]').popover({trigger: "hover"});
  });
}
//CHANGE TASKS TO SEE ONLY ACTIVE OR ALL
function changeActive(seeActive){
  //debugger;
  $('#optionAll').removeClass('active stay-focus');
  $('#optionTrue').removeClass('active stay-focus');
  $('#optionFalse').removeClass('active stay-focus');
  activeTask = seeActive;
  sessionStorage.sessionActiveService = seeActive;
  if($('#option0').hasClass('active'))
    categoryTask = 0;
  else if($('#option1').hasClass('active'))
    categoryTask = 1;
  else if($('#option2').hasClass('active'))
    categoryTask = 2;
  else if($('#option3').hasClass('active'))
    categoryTask = 3;
  else if($('#option4').hasClass('active'))
    categoryTask = 4;
  else if($('#option5').hasClass('active'))
    categoryTask = 5;
  else if($('#option6').hasClass('active'))
    categoryTask = 6;
  else if($('#option7').hasClass('active'))
    categoryTask = 7;
  $.get( "/servis/tasks", {categoryTask, activeTask, completionTask, showMyInputTasks, showMyTasks}, function( data ) {
    //console.log(data)
    allTasks = data.data;
    //shownTasks = data.data.splice(0,10);
    taskLoaded = true;
    debugger
    //drawPagination(Math.ceil(allTasks.length/10));
    if(showNServices == '-1')
      drawPagination(1);
    else
      drawPagination(Math.ceil(allTasks.length/parseInt(showNServices)));
    drawTasks(1);
    if(seeActive == 1)
      $('#optionTrue').addClass('active stay-focus');
    else if(seeActive == 2)
      $('#optionFalse').addClass('active stay-focus');
    else
      $('#optionAll').addClass('active stay-focus');
    //$('[data-toggle="popover"]').popover({trigger: "hover"});
  });
}
//CHANGE TASK TO SEE ALL/UNFINISHED/FINISHED
function changeCompletion(seeCompletion){
  $('#optionAllCompletion').removeClass('active stay-focus');
  $('#optionUncomplete').removeClass('active stay-focus');
  $('#optionComplete').removeClass('active stay-focus');
  completionTask = seeCompletion;
  sessionStorage.sessionCompletionService = seeCompletion;
  setTimeout(()=>{
    if(seeCompletion == 1)
      $('#optionUncomplete').addClass('active stay-focus');
    else if(seeCompletion == 2)
      $('#optionComplete').addClass('active stay-focus');
    else
      $('#optionAllCompletion').addClass('active stay-focus');
  })
  changeTaskTable(categoryTask);
}
//REMOVE/EMPTY TASK LIST AND DRAW/SHOW NEEDED TASKS
function drawTasks(page){
  $('#pagePrev').removeClass('disabled');
  $('#pageNext').removeClass('disabled');
  if(page == 1)
    $('#pagePrev').addClass('disabled');
  if($('#page'+page).next().attr('id') == 'pageNext')
    $('#pageNext').addClass('disabled');
  $('#page'+activePage).removeClass("active");
  $('#page'+page).addClass("active");
  activePage = page;
  //calculate which task to show/draw
  let limit = 10, start = 0;
  if(showNServices == '-1'){
    start = 0;
    limit = allTasks.length;
  }
  else{
    limit = page * parseInt(showNServices);
    start = limit - parseInt(showNServices);
    limit = Math.min(limit, allTasks.length);
  }

  debugger;
  //first empty task list and reset collapse and active task id
  $('#taskList').empty();
  debugger;
  editProjectCollapseOpen = false;
  editTaskCollapseOpen = false;
  subtaskCollapseOpen = false;
  taskFilesCollapseOpen = false;
  taskAbsenceCollapseOpen = false;
  activeTaskId = 0;
  var deleteTaskClass;
  //draw 10 tasks
  for(var i = start; i < limit; i++){
    var id = allTasks[i].id;
    deleteTaskClass = '';
    if((loggedUser.role == 'admin' && allTasks[i].active == false))
      deleteTaskClass = ' bg-secondary';
    //check if has no subtasks -> then add checkbox
    //if has no subtasks and completion == 100 then checked else not checked
    //checkbox
    var checkboxElement = '';
    var noteElement = '';
    var isChecked = '';
    var category = allTasks[i].category.toUpperCase();
    var categoryClass = ''; 
    if(category == 'NABAVA')
      categoryClass = 'badge-purchase';
    else if(category == 'KONSTRUKCIJA')
      categoryClass = 'badge-construction';
    else if(category == 'STROJNA IZDELAVA')
      categoryClass = 'badge-mechanic';
    else if(category == 'ELEKTRO IZDELAVA')
      categoryClass = 'badge-electrican';
    else if(category == 'MONTAŽA')
      categoryClass = 'badge-assembly';
    else if(category == 'PROGRAMIRANJE')
      categoryClass = 'badge-programmer';
    else if(category == 'BREZ')
      category = '';
    //debugger;
    //messages icon
    let commentsIcon = '<i class="far fa-comments fa-lg"></i>';
    if(allTasks[i].notes_count && allTasks[i].notes_count > 0)
      commentsIcon = '<i class="fas fa-comments fa-lg"></i>';
    //workers
    var taskWorkers = [];
    if(allTasks[i].workers){
      taskWorkers = allTasks[i].workers.split(", ");
    }
    if(loggedUser.role == 'admin' || loggedUser.role.substring(0,5) == 'vodja' || loggedUser.role == 'programer plc-jev' || loggedUser.role == 'programer robotov' || loggedUser.role == 'konstrukter' || loggedUser.role == 'kontruktor'){
      if(!allTasks[i].subtask_count){
        if(allTasks[i].completion == 100)
          isChecked = 'checked = ""';
        checkboxElement = `<input class="custom-control-input" id="customTaskCheck`+id+`" type="checkbox" `+isChecked+` />
        <label class="custom-control-label" for="customTaskCheck`+id+`"> </label>`;
      }
      noteElement = `<button class="invisible-button ml-2 btn-on-stretched">` + commentsIcon + `</button>`;
    }
    else{
      noteElement = `<button class="invisible-button ml-2 btn-on-stretched" disabled>` + commentsIcon + `</button>`;
    }
    //date
    var date = '';
    if(allTasks[i].formatted_start && allTasks[i].formatted_finish)
      date = allTasks[i].formatted_start.date + " - " + allTasks[i].formatted_finish.date;
    else if(allTasks[i].formatted_start)
      date = allTasks[i].formatted_start.date + " - /";
    else if(allTasks[i].formatted_finish)
      date = "/ - " + allTasks[i].formatted_finish.date;
    //red date text (task should be finished by now)
    var todayDate = new Date();
    var tmpDate = new Date(allTasks[i].task_finish);
    var dateText = '';
    if(todayDate > tmpDate && allTasks[i].completion < 100)
      dateText = 'text-danger';
    //priority
    var priorityClass = '';
    if(allTasks[i].priority == 'visoka')
      priorityClass = 'border-high-priority';
    else if(allTasks[i].priority == 'srednja')
      priorityClass = 'border-medium-priority';
    else if(allTasks[i].priority == 'nizka')
      priorityClass = 'border-low-priority';
    //workers
    var workers = "";
    if(allTasks[i].workers)
      workers = allTasks[i].workers;
    //workorders
    var workorders = "brez delovnega naloga";
    var workorderColor = ' text-danger';
    if(allTasks[i].workorders_id){
      workorders = allTasks[i].workorders;
      workorderColor = ' text-success';
    }
    //control buttons
    //delete button
    var deleteControlButton = '';
    if(loggedUser.role == 'admin' || loggedUser.role.substring(0,5) == 'vodja' || loggedUser.role == 'programer plc-jev' || loggedUser.role == 'programer robotov' || loggedUser.role == 'konstrukter' || loggedUser.role == 'kontruktor')
      deleteControlButton = `<button class="btn btn-danger ml-2 btn-on-stretched" id="taskButtonDelete`+id+`" data-toggle="tooltip" data-original-title="Odstrani">
        <i class="fas fa-trash fa-lg"></i>
      </button>`;
    //edit button
    var editControlButton = '';
    if(loggedUser.role == 'admin' || loggedUser.role.substring(0,5) == 'vodja' || loggedUser.role == 'programer plc-jev' || loggedUser.role == 'programer robotov' || loggedUser.role == 'konstrukter' || loggedUser.role == 'kontruktor'){
      editControlButton = `<button class="btn btn-primary ml-1 btn-on-stretched" id='taskButtonEdit`+id+`' data-toggle="tooltip" data-original-title="Uredi">
        <i class="fas fa-pen fa-lg"></i>
      </button>`;
    }
    //absence button
    var absenceControlButton = '';
    if(loggedUser.role.substring(0,5) == 'vodja' || loggedUser.role == 'admin' || loggedUser.role == 'programer plc-jev' || loggedUser.role == 'programer robotov' || loggedUser.role == 'konstrukter' || loggedUser.role == 'kontruktor'){
      absenceControlButton = `<button class="btn btn-programmer ml-1 btn-on-stretched" id='taskButtonAbsence`+id+`' data-toggle="tooltip" data-original-title="Odsotnosti">
        <i class="fas fa-clock fa-lg"></i> 
      </button>`;
      //debugger;
      if(allTasks[i].full_absence_count && allTasks[i].full_absence_count > 0)
        absenceControlButton = `<button class="btn btn-programmer ml-1 btn-on-stretched" id='taskButtonAbsence`+id+`' data-toggle="tooltip" data-original-title="Odsotnosti">
          <i class="fas fa-clock fa-lg text-dark"></i>
        </button>`;
      if(allTasks[i].task_absence_count && allTasks[i].task_absence_count > 0)
        absenceControlButton = `<button class="btn btn-programmer ml-1 btn-on-stretched" id='taskButtonAbsence`+id+`' data-toggle="tooltip" data-original-title="Odsotnosti">
          <i class="fas fa-clock fa-lg text-dark"></i>
        </button>`;
      if(!allTasks[i].workers || !allTasks[i].task_start || !allTasks[i].task_finish)
        absenceControlButton = `<button class="btn btn-programmer ml-1 btn-on-stretched" disabled id='taskButtonAbsence`+id+`' data-toggle="tooltip" data-original-title="Odsotnosti">
          <i class="fas fa-clock fa-lg"></i>
        </button>`;
    }
    //files button
    var filesControlButton = '';
    if(loggedUser.role.substring(0,5) == 'vodja' || loggedUser.role == 'admin' || loggedUser.role == 'programer plc-jev' || loggedUser.role == 'programer robotov' || loggedUser.role == 'konstrukter' || loggedUser.role == 'kontruktor'){
      filesControlButton = `<button class="btn btn-purchase ml-1 btn-on-stretched" id='taskButtonFile`+id+`' data-toggle="tooltip" data-original-title="Datoteke">
        <i class="fas fa-file fa-lg"></i>
      </button>`;
      if(allTasks[i].files_count && allTasks[i].files_count > 0)
        filesControlButton = `<button class="btn btn-purchase ml-1 btn-on-stretched" id='taskButtonFile`+id+`' data-toggle="tooltip" data-original-title="Datoteke">
          <i class="fas fa-file fa-lg text-dark"></i>
        </button>`;
    }
    //subtask button
    var subtaskControlButton = '';
    //if(loggedUser.role == 'vodja' || loggedUser.role == 'vodja projektov' || loggedUser.role == 'vodja strojnikov' || loggedUser.role == 'vodja CNC obdelave' || loggedUser.role == 'vodja električarjev' || loggedUser.role == 'vodja programerjev' || loggedUser.role == 'admin' || loggedUser.role == 'programer plc-jev' || loggedUser.role == 'programer robotov' || loggedUser.role == 'konstrukter' || loggedUser.role == 'kontruktor'){
    if(true){
      subtaskControlButton = `<button class="btn btn-info ml-1 stretched-link" id='taskButtonSubtask`+id+`' data-toggle="tooltip" data-original-title="Naloge">
        <i class="fas fa-check-square fa-lg"></i>
      </button>`;
      if(allTasks[i].subtask_count && allTasks[i].subtask_count > 0)
      subtaskControlButton = `<button class="btn btn-info ml-1 stretched-link" id='taskButtonSubtask`+id+`' data-toggle="tooltip" data-original-title="Naloge">
        <i class="fas fa-check-square fa-lg text-dark"></i>
      </button>`;
    }
    //putting everything together into one element
    var element = `<div class="list-group-item `+priorityClass+deleteTaskClass+`" id="task`+id+`">
      <div class="row">
        <div class="col-md-6">
          <span class="btn-on-stretched" id="taskName`+id+`">`+allTasks[i].task_name+` (`+allTasks[i].subscriber+`)</span>
        </div>
        <div class="col-md-6 d-flex">
          <div class="custom-control custom-checkbox table-custom-checkbox btn-on-stretched" id="taskCheckbox`+id+`">
            `+checkboxElement+`
          </div>
          `+noteElement+`
          <div class="ml-2" id="taskInfo`+id+`"><a class="workerspopover btn-on-stretched" data-toggle="popover" data-trigger="hover" data-content="`+workers+`" data-original-title="" title=""><i class="fas fa-user-friends fa-lg text-dark"></i></a></div>
          <div class="progress ml-2 w-80">
            <div class="progress-bar w-`+allTasks[i].completion+`"" id="taskProgress`+id+`"></div>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="d-flex ml-3">
          <span class="`+dateText+` btn-on-stretched" id="taskDate`+id+`">`+date+`</span>
          <div id="taskBadges`+id+`">
            <span class="badge badge-pill `+categoryClass+` ml-2">`+category+`</span>
          </div>
        </div>
        <div class="d-flex ml-auto mr-3">
          <div class="ml-auto" id="taskButtons`+id+`">
            <a class="mypopover p-1 btn-on-stretched" data-toggle="popover" data-trigger="hover" tabindex="0" data-content="`+workorders+`" data-original-title="" title="" id="taskWorkorders`+id+`"><i class="icon-dn fa-lg` + workorderColor + `"></i></a>
            <a class="mypopover p-1 btn-on-stretched" data-toggle="popover" data-trigger="focus" tabindex="0" data-content="ni zabeleženih sprememb" id="taskHistory`+id+`" data-original-title="" title=""><i class="fas fa-history fa-lg text-dark"></i></a>
            `+deleteControlButton+`
            `+absenceControlButton+`
            `+filesControlButton+`
            `+subtaskControlButton+`
            `+editControlButton+`
          </div>
        </div>
      </div>
      <div class="collapse multi-collapse task-collapse btn-on-stretched" id="collapseEditTask`+id+`"></div>
      <div class="collapse multi-collapse task-collapse btn-on-stretched" id="collapseSubtasks`+id+`"></div>
      <div class="collapse multi-collapse task-collapse btn-on-stretched" id="collapseTaskFiles`+id+`"></div>
      <div class="collapse multi-collapse task-collapse btn-on-stretched" id="collapseTaskAbsence`+id+`"></div>
    </div>`;
    //debugger;
    $('#taskList').append(element);
    $('#customTaskCheck'+id).on('change', toggleTaskCheckbox);
    $('#task'+id).find('.invisible-button').on('click', {id, type: 1}, onClickOpenMessagesModal);
    $('#taskButtonEdit'+id).on('click', {taskId: id, type: 0}, onEventToggleCollapse);
    $('#taskButtonSubtask'+id).on('click', {taskId: id, type: 1}, onEventToggleCollapse);
    $('#taskButtonFile'+id).on('click', {taskId: id, type: 2}, onEventToggleCollapse);
    $('#taskButtonAbsence'+id).on('click', {taskId: id, type: 3}, onEventToggleCollapse);
    $('#taskHistory'+id).on('click', onEventGetTaskChanges);
    $('#taskButtonDelete'+id).on('click', onEventDeleteTask);
  }
  //popover
  $('[data-toggle="popover"]').popover({ html: true });
  $('[data-toggle="tooltip"]').tooltip({trigger : 'hover'});
  $('[rel="tooltip"]').on('click', function () {
    $(this).tooltip('hide')
  })
}
//CHANGE PAGE OF TASKS LIST
function changePage(event){
  let page = this.value;
  if (page == 0){
    $('#'+this.id).hasClass('next') ? page = 'next' : page = 'prev';
  }
  debugger
  //page is prev or next
  if(isNaN(page)){
    //check if task are loaded
    if(page == 'next')
      page = activePage + 1;
    else if(page == 'prev')
      page = activePage - 1;
    if(taskLoaded){
      drawTasks(page);
    }
    else{
      $.get( "/servis/tasks", {categoryTask, activeTask, completionTask, showMyInputTasks, showMyTasks}, function( data ) {
        allTasks = data.data;
        taskLoaded = true;
        debugger
        drawTasks(page);
      });
    }
  }
  //page is number
  else{
    //page isnt active otherwise do nothing
    if(!$('#page'+page).hasClass('active')){
      debugger;
      if(taskLoaded){
        drawTasks(page);
      }
      else{
        $.get( "/servis/tasks", {categoryTask, activeTask, completionTask, showMyInputTasks, showMyTasks}, function( data ) {
          allTasks = data.data;
          taskLoaded = true;
          debugger
          drawTasks(page);
        });
      }
    }
  }
}
//DRAW PAGINATION
function drawPagination(pages){
  debugger;
  $('#taskPages').empty();
  var missingPages = '';
  for(var i = 2; i <= pages; i++){
    missingPages += '<li class="page-item" value="'+i+'" id="page'+i+'"><a class="page-link" href="#collapseTableTask">'+i+'</a></li>';
  }
  var pagesElement = `<ul class="pagination pagination-sm">
    <li class="page-item disabled" id="pagePrev"><a class="page-link" href="#collapseTableTask">«</a></li>
    <li class="page-item active" value="1" id="page1"><a class="page-link" href="#collapseTableTask">1</a></li>
    `+missingPages+`
    <li class="page-item next" value="0" id="pageNext"><a class="page-link" href="#collapseTableTask">»</a></li>
  </ul>`;
  $('#taskPages').append(pagesElement);
}
function onChangeSelectShowNServices(event) {
  let tmp = $('#selectNumberServices').val();
  if(showNServices != tmp){
    debugger
    showNServices = tmp;
    localStorage.showNServices = tmp;
    if(showNServices == '-1')
      drawPagination(1);
    else
      drawPagination(Math.ceil(allTasks.length/parseInt(showNServices)));
    drawTasks(1)
  }
}
//SMOOTH SCROLL ANIMATION TO THE JQUERY SELECTOR
function smoothScroll(selector, diff){
  if(!diff)
    diff = 0;
  $("html, body").animate({ scrollTop: $(selector).offset().top-diff }, 1000);
}
/*
function smoothScroll (selector) {
  document.querySelector(selector).scrollIntoView({
    behavior: 'smooth'
  });
}
*/
//FLASH ANIMATION WITH FADE OUT AND IN
function flashAnimation(element){
  $(element).delay(100).fadeOut().fadeIn().fadeOut().fadeIn().fadeOut().fadeIn()
}
var urlParams = new URLSearchParams(window.location.search);
var projectId = urlParams.get('id'); // dont need, there is none
var selectedTaskId;
//var tid;
var taskLoaded = false;
//tables//
var allTasks;
var allSubtasks;
var allAbsence;
var allReasons;
var allWorkersTable;
//tables//
var shownTasks;
var loggedUser;
var categoryTask = 0;
var activeTask = 1;
var activePage;
//0 = all, 1 = unfinished, 2 = finished
//var completionTask = 1;
//for select2 --> (id, text)//
var allSubscribers;
var allWorkers; 
var allCategories;
var allPriorities;
//for select2 --> (id, text)//
var editProjectCollapseOpen = false;
var editTaskCollapseOpen = false;
var subtaskCollapseOpen = false;
var taskFilesCollapseOpen = false;
var taskAbsenceCollapseOpen = false;
var activeTaskId = 0;
var showMyInputTasks = false;
var showMyTasks = false;
let showNTasks;