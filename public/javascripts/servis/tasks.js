//IN THIS SCRIPT//
//// adding note to task, checbox of task, fixing percentage of task/project, adding/editing/deleting task, showing conflicts, form control for tasks ////

$(function(){
  $('#taskNameAdd').on('input',function(e){
    if($('#taskNameAdd').val() == '')
      $('#taskNameAdd').addClass('is-invalid');
    else
      $('#taskNameAdd').removeClass('is-invalid');
  });
  $('#taskStartAdd').on('change', onDatesAddChange);
  $('#taskFinishAdd').on('change', onDatesAddChange);
  $('#collapseAddTask').find('.close').on('click', closeCollapseAddTask);
  $('#btnDeleteTaskConf').on('click', deleteTaskConfirm);
  $('#btnOverrideTask').on('click', taskOverride);
})

function toggleTaskCheckbox(element){
  debugger;
  var taskId = parseInt(this.id.substring(15));
  if((this.checked == true) && ($('#taskWorkorders'+taskId).attr('data-content') == 'brez delovnega naloga')){
    overrideTaskId = taskId;
    $('#customTaskCheck'+taskId).prop('checked', false);
    $('#btnOverrideTask').show();
    $('#btnOverrideSubtask').hide();
    $('#modalWarningWO').modal('toggle');
  }
  else{
    var completion = this.checked ? 100 : 0;
    var oldCompletion = this.checked ? 0 : 100;
    fixTaskCompletion(taskId, completion, oldCompletion);
    debugger;
  }
}
function taskOverride(){
  var taskId = overrideTaskId;
  debugger;
  $('#customTaskCheck'+taskId).prop('checked', true);
  $('#modalWarningWO').modal('toggle');
  var completion = 100, oldCompletion = 0;
  fixTaskCompletion(taskId, completion,oldCompletion);
  debugger;
}
//FIX Task percentage
function fixTaskCompletion(taskId, completion, oldCompletion){
  $.post('/projects/task/completion', {taskId, completion}, function(resp){
    //need to find correct subscriber for adding correct change
    var subId = $('#taskName'+taskId).html().split('(');
    subId = subId[subId.length-1];
    subId = subId.substring(0,subId.length-1);
    subId = allSubscribers.find(s => s.text == subId).id;
    //fix subtask count
    if(allTasks){
      var task = allTasks.find(t=>t.id == taskId);
      task.completion = completion;
    }
    //fix task display
    //$('#taskProgress'+taskId).width(completion+"%");
    $('#taskProgress'+taskId).removeClass();
    $('#taskProgress'+taskId).addClass('progress-bar w-'+completion);
    $('#taskDate'+taskId).removeClass('text-danger');
    var date = $('#taskDate'+taskId).html();
    if(date){
      date = date.split(' - ');
      if(date[1] != '/'){
        date = date[1].split('.');
        //var d = date[0]; var m = date[1]; var y = date[2];
        if(new Date() > new Date(date[2],date[1],date[0],'15') && completion < 100)
          $('#taskDate'+taskId).addClass('text-danger');
      }
    }
    //ADDING NEW CHANGE TO DB
    if(completion == 100 && oldCompletion != completion)
      addChangeSystem(4,9,null,taskId,null,null,null,null,subId);
    else
      if(oldCompletion == 100)
        addChangeSystem(5,9,null,taskId,null,null,null,null,subId);
  })
}
//ADD FINISHED DATE WHEN TASK WAS GIVEN 100 COMPLETION
function addFinishedDate(taskId){
  $.post('task/finished', {taskId}, function(resp){
    debugger;
  })
}
//OPEN EDIT TASK COLLAPSE
function editTask(taskId){
  //debugger;
  if(!allCategories || !allPriorities || !allWorkers || !allSubscribers){
    $.get( "/projects/users", function( data ) {
      allWorkers = data.data;
      $.get( "/projects/categories", function( data ) {
        allCategories = data.data;
        $.get( "/projects/priorities", function( data ) {
          allPriorities = data.data;
          $.get( "/subscribers/all", function( data ) {
            allSubscribers = data.data;
            if(taskLoaded){
              makeEditTask();
              $('#collapseEditTask'+taskId).collapse("toggle");
              editTaskCollapseOpen = true;
            }
            else{
              $.get( "/servis/tasks", {categoryTask,activeTask, completionTask}, function( data ) {
                //console.log(data)
                //debugger
                allTasks = data.data;
                taskLoaded = true;
                makeEditTask();
                $('#collapseEditTask'+taskId).collapse("toggle");
                editTaskCollapseOpen = true;
              });
            }
          })
        });
      });  
    });
  }
  else{
    if(taskLoaded){
      makeEditTask();
      $('#collapseEditTask'+taskId).collapse("toggle");
      editTaskCollapseOpen = true;
    }
    else{
      $.get( "/servis/tasks", {categoryTask,activeTask, completionTask}, function( data ) {
        //console.log(data)
        //debugger
        allTasks = data.data;
        taskLoaded = true;
        makeEditTask();
        $('#collapseEditTask'+taskId).collapse("toggle");
        editTaskCollapseOpen = true;
      });
    }
  }
}
//FILL EDIT TASK DATA (form)
function makeEditTask(){
  //debugger;
  var editTaskActivity = ''; 
  if(loggedUser.role == 'admin')
    editTaskActivity = '<div class="d-flex justify-content-center mb-2"><div class="custom-control custom-checkbox"><input class="custom-control-input" id="taskActiveEdit" type="checkbox" checked="" /><label class="custom-control-label" for="taskActiveEdit">Opravilo je aktivno</label></div></div>';
  var collapseElement = `<hr />
  <div class="form-row">
    <div class="col-md-6">
      <div class="form-group">
        <label class="w-100" for="taskNameEdit">Ime opravila
          <input class="form-control" id="taskNameEdit" type="text" name="taskNameEdit" required="" />
            <div class="invalid-feedback">Za dodajanje novega opravila je potrebno vnesti vsaj ime opravila!</div>
        </label>
      </div>
    </div>
    <div class="col-md-6">
      <div class="form-group">
        <label class="w-100" for="taskSubscriberEdit">Naročnik
          <select class="custom-select mr-sm-2 form-control select2-subscriberEdit w-100" id="taskSubscriberEdit" name="taskSubscriberAdd"></select>
        </label>
      </div>
    </div>
  </div>
  <div class="form-row">
    <div class="col-md-6">
      <div class="form-group">
        <label class="w-100" for="taskStartEdit">Začetek opravila
          <input class="form-control" id="taskStartEdit" type="date" name="taskStartEdit" />
            <div class="valid-feedback">Začetek opravila je znotraj datuma projekta.</div>
            <div class="invalid-feedback">Začetek opravila je izven datuma projekta!</div>
        </label>
      </div>
    </div>
    <div class="col-md-6">
      <div class="form-group">
        <label class="w-100" for="taskFinishEdit">Konec opravila
          <input class="form-control" id="taskFinishEdit" type="date" name="taskFinishEdit" />
            <div class="valid-feedback">Konec opravila je znotraj datuma projekta.</div>
            <div class="invalid-feedback">Konec opravila je izven datuma projekta!</div>
        </label>
      </div>
    </div>
  </div>
  <div class="form-row">
    <div class="col-md-6">
      <div class="form-group">
        <label class="w-100" for="taskDurationEdit">Število dni
          <input class="form-control" id="taskDurationEdit" type="number" name="taskDurationEdit" min="0" disabled placeholder="Se izračuna samo po vnosu obeh datumov."/>
        </label>
      </div>
    </div>
    <div class="col-md-6">
      <div class="form-group">
        <label class="w-100" for="taskAssignmentEdit">Dodeli opravilo
          <select class="custom-select mr-sm-2 form-control select2-workersEdit w-100" id="taskAssignmentEdit" name="worker"></select>
        </label>
      </div>
    </div>
  </div>
  <div class="form-row">
    <div class="col-md-6">
      <div class="form-group">
        <label class="w-100" for="taskCategoryEdit">Kategorija
          <select class="custom-select mr-sm-2 form-control select2-taskCategoryEdit w-100" id="taskCategoryEdit" name="worker"></select>
        </label>
      </div>
    </div>
    <div class="col-md-6">
      <div class="form-group">
        <label class="w-100" for="taskPriorityEdit">Prioriteta
          <select class="custom-select mr-sm-2 form-control select2-taskPriorityEdit w-100" id="taskPriorityEdit" name="worker" ></select>
        </label>
      </div>
    </div>
  </div>
  <div class="d-flex justify-content-center mb-2 mt-n2">
    <div class="custom-control custom-checkbox">
      <input class="custom-control-input" id="taskWeekendEdit" type="checkbox" />
      <label class="custom-control-label" for="taskWeekendEdit">Vikend</label>
    </div>
  </div>
  `+editTaskActivity+`
  <div class="text-center">
    <button id="btnEditService" class="btn btn-success">Posodobi</button>
  </div>`;
  $('#collapseEditTask'+activeTaskId).append(collapseElement);
  $('#btnEditService').on('click', {or:0}, onEventUpdateEditTask);
  $('#taskStartEdit').on('change', onDatesEditChange);
  $('#taskFinishEdit').on('change', onDatesEditChange);
  $('#taskNameEdit').on('input',function(e){
    if($('#taskNameEdit').val() == '')
      $('#taskNameEdit').addClass('is-invalid');
    else
      $('#taskNameEdit').removeClass('is-invalid');
  });
  $(".select2-workersEdit").select2({
    data: allWorkers,
    tags: false,
    multiple: true,
  });
  $(".select2-taskCategoryEdit").select2({
    data: allCategories,
    tags: false,
  });
  $(".select2-taskPriorityEdit").select2({
    data: allPriorities,
    tags: false,
  });
  $(".select2-subscriberEdit").select2({
    data: allSubscribers,
    tags: true,
  });
  //$('#project_notes').val(project.notes);
  var x = allTasks.find(t => t.id == activeTaskId);
  $('#taskNameEdit').val(x.task_name);
  $('#taskDurationEdit').val(x.task_duration);
  if(x.task_start){
    $('#taskStartEdit').val(moment(x.task_start).format("YYYY-MM-DD"));
    onDatesEditChange();
  }
  if(x.task_finish){
    $('#taskFinishEdit').val(moment(x.task_finish).format("YYYY-MM-DD"));
    onDatesEditChange();
  }
  $('#taskSubscriberEdit').val(x.id_subscriber).trigger('change');
  if(x.workers)
    $('#taskAssignmentEdit').val(x.workers_id.split(',')).trigger('change');
  var c = allCategories.find(c => c.text == x.category);
  var p = allPriorities.find(p => p.text == x.priority);
  $('#taskCategoryEdit').val(c.id).trigger('change');
  $('#taskPriorityEdit').val(p.id).trigger('change');
  //activity of task
  if(x.active == true)
    $('#taskActiveEdit').prop("checked", true);
  else if(x.active == false){
    $('#taskActiveEdit').prop("checked", false);
    $('#collapseEditTask'+activeTaskId).removeClass('task-collapse');
    $('#collapseEditTask'+activeTaskId).addClass('task-collapse-deleted');
  }
  if(x.weekend == true)
    $('#taskWeekendEdit').prop("checked", true);
  else
    $('#taskWeekendEdit').prop("checked", false);
}
function addNewTask(or){
  debugger;
  if(!collapseAddNewTaskOpen){
    if(!allCategories || !allPriorities || !allWorkers || !allSubscribers){
      $.get( "/projects/users", function( data ) {
        allWorkers = data.data;
        $.get( "/projects/categories", function( data ) {
          allCategories = data.data;
          $.get( "/projects/priorities", function( data ) {
            allPriorities = data.data;
            $.get( "/subscribers/all", function( data ){
              allSubscribers = data.data;

              $(".select2-workersAdd").select2({
                data: allWorkers,
                tags: false,
                multiple: true,
              });
              $(".select2-taskCategoryAdd").select2({
                data: allCategories,
                tags: false,
              });
              $(".select2-taskPriorityAdd").select2({
                data: allPriorities,
                tags: false,
              });
              $(".select2-subscriberAdd").select2({
                data: allSubscribers,
                tags: true,
              });
              $('#collapseAddTask').collapse("toggle");
              collapseAddNewTaskOpen = true;
              if(!taskLoaded){
                $.get( "/servis/tasks", {categoryTask,activeTask, completionTask}, function( data ) {
                  //console.log(data)
                  //debugger
                  allTasks = data.data;
                  taskLoaded = true;
                });
              }
            })
          });
        });  
      });
    }
    else{
      debugger;
      $(".select2-workersAdd").select2({
        data: allWorkers,
        tags: false,
        multiple: true,
      });
      $(".select2-taskCategoryAdd").select2({
        data: allCategories,
        tags: false,
      });
      $(".select2-taskPriorityAdd").select2({
        data: allPriorities,
        tags: false,
      });
      $(".select2-subscriberAdd").select2({
        data: allSubscribers,
        tags: true,
      });
      $('#collapseAddTask').collapse("toggle");
      collapseAddNewTaskOpen = true;
    }
  }
  else{
    //debugger;
    //first check if there are any empty fields that are not allowed
    if($('#taskNameAdd').val() == '')
      $('#taskNameAdd').addClass("is-invalid")
    else{
      var override = or;
      //otherwise add and while adding check if there are conflicts
      var taskName = $('#taskNameAdd').val();
      var taskDuration = $('#taskDurationAdd').val();
      var taskStart = $('#taskStartAdd').val();
      var taskFinish = $('#taskFinishAdd').val();
      var taskSubscriber = $('#taskSubscriberAdd').val();
      var taskAssignment = $('#taskAssignmentAdd').val();
      taskAssignment = taskAssignment.toString();
      var taskCategory = $('#taskCategoryAdd').val();
      var taskPriority = $('#taskPriorityAdd').val();
      var taskWeekend = false;
      if(typeof $('#taskWeekendAdd').prop('checked') === 'undefined') 
        taskWeekend = true
      else
        taskWeekend = $('#taskWeekendAdd').prop('checked');
      if(!taskDuration)
        taskDuration = 0;
      if(taskStart)
        taskStart = taskStart + " 07:00:00.000000";
      if(taskFinish)
        taskFinish = taskFinish + " 15:00:00.000000";
      debugger;
      // first check if subscriber exist and create him otherwise
      if(isNaN(taskSubscriber)){
        var subscriber = taskSubscriber;
        $.post('/subscribers/create', {subscriber}, function(resp){
          var subscriberName = subscriber;
          taskSubscriber = resp.id.id;
          allSubscribers.push({id:taskSubscriber,text:subscriberName});
          $(".select2-subscriberAdd").select2({
            data: allSubscribers,
            tags: true,
          });
          $('#taskSubscriberAdd').val(taskSubscriber).trigger('change');
          $.post('/servis/add', {taskName, taskDuration, taskStart, taskFinish, taskSubscriber, taskAssignment, taskCategory, taskPriority, taskWeekend, override}, function(resp){
  
            debugger;
            if(resp.conflicts){
              openConflictModal(resp.conflicts);
              //console.log(resp.conflicts);
            }
            if(override == 1){
              $('#modalConflicts').modal('toggle');
            }
            if(resp.success == true){
              $('#collapseAddTask').collapse("toggle");
              collapseAddNewTaskOpen = false;
              
              debugger;
              //time
              var taskStartFormat = null;
              if(taskStart){
                taskStart = new Date(taskStart).toISOString();
                var sdate = new Date(taskStart).toLocaleDateString('sl');
                var stime = taskStart.split('T')[1];
                taskStartFormat = {date:sdate, time:stime.substring(0,5)};
              }
              else
                taskStart = null;
              var taskFinishFormat = null;
              if(taskFinish){
                taskFinish = new Date(taskFinish).toISOString();
                var fdate = new Date(taskFinish).toLocaleDateString('sl');
                var ftime = taskFinish.split('T')[1];
                taskFinishFormat = {date:fdate, time:ftime.substring(0,5)};
              }
              else
                taskFinish = null;
              if(!taskDuration)
                taskDuration = 0;
              //workers
              var taskWorkers = "";
              for(var i = 0; i < $('#taskAssignmentAdd').val().length; i++){
                if(i != 0)
                  taskWorkers += ", ";
                taskWorkers += allWorkers.find(w => w.id == parseInt($('#taskAssignmentAdd').val()[i])).text;
              }
              if(!taskWorkers){
                taskWorkers = null;
                taskAssignment = null;
              }
              var newTask = {id:resp.newTaskId, task_name: taskName, task_duration: taskDuration, 
              task_start:taskStart, task_finish: taskFinish, active:true, 
              category:allCategories.find(c => c.id == taskCategory).text, completion:0, 
              files_count:null, finished:null, 
              formatted_start:taskStartFormat, 
              formatted_finish:taskFinishFormat, 
              full_absence_count:null, task_absence_count:null, priority:allPriorities.find(p => p.id == taskPriority).text, 
              subtask_count:null, task_note:"", 
              workers: taskWorkers, workers_id: taskAssignment, id_subscriber: taskSubscriber, weekend:taskWorkers,
              subscriber:allSubscribers.find(s=>s.id == taskSubscriber).text};
    
              allTasks.unshift(newTask);
              debugger;
              var myCurrentPage = 1;
              var pageCounter = Math.ceil(allTasks.length/10);
              drawPagination(pageCounter);
    
              
              drawTasks(myCurrentPage);
              debugger;
              $('#taskNameAdd').val("");
              $('#taskDurationAdd').val("");
              $('#taskStartAdd').val("");
              $('#taskStartAdd').removeClass('is-valid');
              $('#taskStartAdd').removeClass('is-invalid');
              $('#taskFinishAdd').val("");
              $('#taskFinishAdd').removeClass('is-valid');
              $('#taskFinishAdd').removeClass('is-invalid');
              $('#taskSubscriberAdd').val(0).trigger('change');
              $('#taskAssignmentAdd').val(0).trigger('change');
              $('#taskCategoryAdd').val(1).trigger('change');
              $('#taskPriorityAdd').val(1).trigger('change');
              setTimeout(()=>{
                smoothScroll('#task'+resp.newTaskId, 100); flashAnimation('#task'+resp.newTaskId);
              })
              //ADDING NEW CHANGE TO DB
              //addNewServisChange(9,1,taskSubscriber,resp.newTaskId);
              addChangeSystem(1,9,null,resp.newTaskId,null,null,null,null,taskSubscriber);
              addChangeSystem(1,8,null,null,null,null,null,null,taskSubscriber);
            }
            else{
              //$('#modalError').modal('toggle');
            }
          })   
        })
      }
      else{
        $.post('/servis/add', {taskName, taskDuration, taskStart, taskFinish, taskSubscriber, taskAssignment, taskCategory, taskPriority, taskWeekend, override}, function(resp){
  
          debugger;
          if(resp.conflicts){
            openConflictModal(resp.conflicts);
            //console.log(resp.conflicts);
          }
          if(override == 1){
            $('#modalConflicts').modal('toggle');
          }
          if(resp.success == true){
            $('#collapseAddTask').collapse("toggle");
            collapseAddNewTaskOpen = false;
            
            debugger;
            //time
            var taskStartFormat = null;
            if(taskStart){
              taskStart = new Date(taskStart).toISOString();
              var sdate = new Date(taskStart).toLocaleDateString('sl');
              var stime = taskStart.split('T')[1];
              taskStartFormat = {date:sdate, time:stime.substring(0,5)};
            }
            else
              taskStart = null;
            var taskFinishFormat = null;
            if(taskFinish){
              taskFinish = new Date(taskFinish).toISOString();
              var fdate = new Date(taskFinish).toLocaleDateString('sl');
              var ftime = taskFinish.split('T')[1];
              taskFinishFormat = {date:fdate, time:ftime.substring(0,5)};
            }
            else
              taskFinish = null;
            if(!taskDuration)
              taskDuration = 0;
            //workers
            var taskWorkers = "";
            for(var i = 0; i < $('#taskAssignmentAdd').val().length; i++){
              if(i != 0)
                taskWorkers += ", ";
              taskWorkers += allWorkers.find(w => w.id == parseInt($('#taskAssignmentAdd').val()[i])).text;
            }
            if(!taskWorkers){
              taskWorkers = null;
              taskAssignment = null;
            }
            var newTask = {id:resp.newTaskId, task_name: taskName, task_duration: taskDuration, 
            task_start:taskStart, task_finish: taskFinish, active:true, 
            category:allCategories.find(c => c.id == taskCategory).text, completion:0, 
            files_count:null, finished:null, 
            formatted_start:taskStartFormat, 
            formatted_finish:taskFinishFormat, 
            full_absence_count:null, task_absence_count:null, priority:allPriorities.find(p => p.id == taskPriority).text, 
            subtask_count:null, task_note:"", 
            workers: taskWorkers, workers_id: taskAssignment, id_subscriber: taskSubscriber, weekend:taskWorkers,
            subscriber:allSubscribers.find(s=>s.id == taskSubscriber).text};
  
            allTasks.unshift(newTask);
            debugger;
            var myCurrentPage = 1;
            var pageCounter = Math.ceil(allTasks.length/10);
            drawPagination(pageCounter);
  
            
            drawTasks(myCurrentPage);
            debugger;
            $('#taskNameAdd').val("");
            $('#taskDurationAdd').val("");
            $('#taskStartAdd').val("");
            $('#taskStartAdd').removeClass('is-valid');
            $('#taskStartAdd').removeClass('is-invalid');
            $('#taskFinishAdd').val("");
            $('#taskFinishAdd').removeClass('is-valid');
            $('#taskFinishAdd').removeClass('is-invalid');
            $('#taskSubscriberAdd').val(0).trigger('change');
            $('#taskAssignmentAdd').val(0).trigger('change');
            $('#taskCategoryAdd').val(1).trigger('change');
            $('#taskPriorityAdd').val(1).trigger('change');
            setTimeout(()=>{
              smoothScroll('#task'+resp.newTaskId, 100); flashAnimation('#task'+resp.newTaskId);
            })
            //ADDING NEW CHANGE TO DB
            //addNewServisChange(9,1,taskSubscriber,resp.newTaskId);
            addChangeSystem(1,9,null,resp.newTaskId,null,null,null,null,taskSubscriber);
          }
        })
      }
      debugger;
    }
  }
}

//open conflicts modal and fill modal with new conflicts
function openConflictModal(conflicts){
  debugger;
  $('#conflictsBody').empty();
  for(var i = 0; i < conflicts.length; i++){
    if(conflicts[i].length > 0){
      var conflictWorker = conflicts[i][0].name + " " +conflicts[i][0].surname;
      var listElements = ``;
      for(var j = 0; j < conflicts[i].length; j++){
        var linkStart = '';
        var linkEnd = '';
        var linkTask = '';
        if(conflicts[i][j].id_subscriber){
          //servis conflicts
          if(loggedUser.role.substring(0,5) == 'vodja' || loggedUser.role == 'admin' || loggedUser.role == 'programer plc-jev' || loggedUser.role == 'programer robotov' || loggedUser.role == 'konstrukter' || loggedUser.role == 'kontruktor'){
            linkStart = `<a href="/servis/?taskId=`+conflicts[i][j].id_task+`" class="alert-link">`;
            linkEnd = '</a>';
          }
          listElements += `<li class="list-group-item">
            <div>
              Servis `+linkStart+`<u>`+conflicts[i][j].task_name+`</u>`+linkEnd+` pri <u>`+conflicts[i][j].subscriber+`</u> v času <u>`+new Date(conflicts[i][j].task_start).toLocaleDateString('sl')+` - `+new Date(conflicts[i][j].task_finish).toLocaleDateString('sl')+`</u>.
            </div>
          </li>`;
        }
        else if(conflicts[i][j].id_project){
          //project conflicts
          if(loggedUser.role.substring(0,5) == 'vodja' || loggedUser.role == 'admin' || loggedUser.role == 'programer plc-jev' || loggedUser.role == 'programer robotov' || loggedUser.role == 'konstrukter' || loggedUser.role == 'kontruktor'){
            linkStart = `<a href="/projects/id?id=`+conflicts[i][j].id_project+`" class="alert-link">`;
            linkTask = `<a href="/projects/id?id=`+conflicts[i][j].id_project+`&taskId=`+conflicts[i][j].id_task+`" class="alert-link">`;
            linkEnd = '</a>';
          }
          listElements += `<li class="list-group-item">
            <div>
              `+linkStart+`<u>`+conflicts[i][j].project_name+`</u>`+linkEnd+` na opravilu `+linkTask+`<u>`+conflicts[i][j].task_name+`</u>`+linkEnd+` v času <u>`+new Date(conflicts[i][j].task_start).toLocaleDateString('sl')+` - `+new Date(conflicts[i][j].task_finish).toLocaleDateString('sl')+`</u>.
            </div>
          </li>`;
        }
        else{
          //absence
          if(loggedUser.role.substring(0,5) == 'vodja' || loggedUser.role == 'admin' || loggedUser.role == 'programer plc-jev' || loggedUser.role == 'programer robotov' || loggedUser.role == 'konstrukter' || loggedUser.role == 'kontruktor'){
            linkStart = `<a href="/employees/absence?id=`+conflicts[i][j].id_user+`" class="alert-link">`;
            linkEnd = '</a>';
          }
          listElements += `<li class="list-group-item">
            <div>
              `+linkStart+`<u>`+conflicts[i][j].project_name+`</u>`+linkEnd+` v času <u>`+new Date(conflicts[i][j].task_start).toLocaleDateString('sl')+` - `+new Date(conflicts[i][j].task_finish).toLocaleDateString('sl')+`</u> z nazivom: <u>`+conflicts[i][j].task_name+`</u>.
            </div>
          </li>`;
        }
      }
      var element = `<div><strong>`+conflictWorker+`:</strong></div>
      <ul class="list-group mb-2">
        `+listElements+`
      </ul>`;
      $('#conflictsBody').append(element);
    }
  }
  $('#modalConflicts').modal();
}
function closeCollapseAddTask(){
  $('#collapseAddTask').collapse("toggle");
  collapseAddNewTaskOpen = false;
}
function onEventUpdateEditTask(event) {
  updateEditTask(event.data.or);
}
//update task from edit form
function updateEditTask(or){
  //debugger;
  //first check if there are any empty fields that are not allowed
  if($('#taskNameEdit').val() == '')
    $('#taskNameEdit').addClass("is-invalid")
  else{
    var override = or;
    var taskId = activeTaskId;
    var currentWorkers = allTasks.find(t => t.id == taskId).workers_id;
    //otherwise add and while adding check if there are conflicts
    var taskName = $('#taskNameEdit').val();
    var taskDuration = $('#taskDurationEdit').val();
    var taskStart = $('#taskStartEdit').val();
    var taskFinish = $('#taskFinishEdit').val();
    var taskSubscriber = $('#taskSubscriberEdit').val();
    var taskAssignment = $('#taskAssignmentEdit').val();
    taskAssignment = taskAssignment.toString();
    var taskCategory = $('#taskCategoryEdit').val();
    var taskPriority = $('#taskPriorityEdit').val();
    var taskWeekend = false;
    var taskActive;
    if(typeof $('#taskActiveEdit').prop('checked') === 'undefined') 
      taskActive = true
    else
      taskActive = $('#taskActiveEdit').prop('checked');
    if(typeof $('#taskWeekendEdit').prop('checked') === 'undefined') 
      taskWeekend = true
    else
      taskWeekend = $('#taskWeekendEdit').prop('checked');
    if(!taskDuration)
      taskDuration = 0;
    if(taskStart)
      taskStart = taskStart + " 07:00:00.000000";
    if(taskFinish)
      taskFinish = taskFinish + " 15:00:00.000000";
    debugger;
    $.post('/servis/update', {taskId, taskName, taskDuration, taskStart, taskFinish, taskSubscriber, taskAssignment, currentWorkers, taskCategory, taskPriority, taskActive, taskWeekend, override}, function(resp){

      debugger;
      if(resp.conflicts){
        openConflictEditModal(resp.conflicts);
        //console.log(resp.conflicts);
      }
      if(override == 1){
        $('#modalConflictsEdit').modal('toggle');
      }
      if(resp.success == true){
        //update task in allTask && task on page
        var x = allTasks.find(t => t.id == taskId);
        //name
        x.task_name = taskName;
        x.weekend = taskWeekend;
        x.id_subscriber = taskSubscriber;
        x.subscriber = allSubscribers.find(s => s.id == taskSubscriber).text;
        $('#taskName'+taskId).html(taskName+' ('+x.subscriber+')');
        //date
        var dateString = "";
        var taskStartFormat = null;
        if(taskStart){
          taskStart = new Date(taskStart).toISOString();
          var sdate = new Date(taskStart).toLocaleDateString('sl');
          var stime = taskStart.split('T')[1];
          taskStartFormat = {date:sdate, time:stime.substring(0,5)};
          x.task_start = taskStart;
          x.formatted_start = taskStartFormat;
          dateString = taskStartFormat.date + " - ";
        }
        else{
          taskStart = null;
          x.task_start = taskStart;
          x.formatted_start = taskStartFormat;
          dateString = "/ - ";
        }
        var taskFinishFormat = null;
        var dateText = '';
        if(taskFinish){
          if(new Date() > new Date(taskFinish) && x.completion < 100)
            dateText = 'text-danger';
          taskFinish = new Date(taskFinish).toISOString();
          var fdate = new Date(taskFinish).toLocaleDateString('sl');
          var ftime = taskFinish.split('T')[1];
          taskFinishFormat = {date:fdate, time:ftime.substring(0,5)};
          x.task_finish = taskFinish;
          x.formatted_finish = taskFinishFormat;
          dateString += taskFinishFormat.date ;
        }
        else{
          taskFinish = null;
          x.task_finish = taskFinish;
          x.formatted_finish = taskFinishFormat;
          dateString += "/";
        }
        if(dateString == "/ - /") dateString = "";
        $('#taskDate'+taskId).html(dateString);
        $('#taskDate'+taskId).removeClass('text-danger');
        $('#taskDate'+taskId).addClass(dateText);
        //category
        debugger;
        $('#taskBadges'+taskId).empty();
        var badge = '';
        var taskBadge = '';
        if(taskCategory == "2"){
          badge = 'badge-purchase';
          taskBadge = 'Nabava';
          x.category = taskBadge;
        }
        else if(taskCategory == "3"){
          badge = 'badge-construction';
          taskBadge = 'Konstrukcija';
          x.category = taskBadge;
        }
        else if(taskCategory == "4"){
          badge = 'badge-mechanic';
          taskBadge = 'Strojna izdelava';
          x.category = taskBadge;
        }
        else if(taskCategory == "5"){
          badge = 'badge-electrican';
          taskBadge = 'Elektro izdelava';
          x.category = taskBadge;
        }
        else if(taskCategory == "6"){
          badge = 'badge-assembly';
          taskBadge = 'Montaža';
          x.category = taskBadge;
        }
        else if(taskCategory == "7"){
          badge = 'badge-programmer';
          taskBadge = 'Programiranje';
          x.category = taskBadge;
        }
        else{
          x.category = "Brez";
        }
        //priority
        var badgeElement = '<span class="badge badge-pill '+badge+' ml-2">'+taskBadge.toUpperCase()+'</span>';
        $('#taskBadges'+taskId).append(badgeElement);
        $('#task'+taskId).removeClass('border-low-priority');
        $('#task'+taskId).removeClass('border-medium-priority');
        $('#task'+taskId).removeClass('border-high-priority');
        if(taskPriority == "2"){
          $('#task'+taskId).addClass('border-low-priority');
          x.priority = 'nizka';
        }
        else if(taskPriority == "3"){
          $('#task'+taskId).addClass('border-medium-priority');
          x.priority = 'srednja';
        }
        else if(taskPriority == "4"){
          $('#task'+taskId).addClass('border-high-priority');
          x.priority = 'visoka';
        }
        else{
          x.priority = 'brez';
        }
        //workers
        var taskWorkers = "";
        for(var i = 0; i < $('#taskAssignmentEdit').val().length; i++){
          if(i != 0)
            taskWorkers += ", ";
          taskWorkers += allWorkers.find(w => w.id == parseInt($('#taskAssignmentEdit').val()[i])).text;
        }
        if(taskWorkers)
          $('#taskInfo'+taskId).find('.workerspopover').attr('data-content', taskWorkers);
        else
          $('#taskInfo'+taskId).find('.workerspopover').attr('data-content', " ");
        if(!taskWorkers){
          taskWorkers = null;
          taskAssignment = null;
        }
        x.workers_id = taskAssignment;
        x.workers = taskWorkers;
        x.task_duration = taskDuration;
        //absence button (if there is no start || finish || workers button is disabled)
        if(taskAssignment && taskStart && taskFinish)
          $('#taskButtonAbsence'+taskId).prop('disabled', false);
        else
          $('#taskButtonAbsence'+taskId).prop('disabled', true)
        x.active = taskActive;
        $('#task'+taskId).removeClass('bg-secondary');
        if(taskActive == false)
          $('#task'+taskId).addClass('bg-secondary');
        
        debugger;
        toggleCollapse(taskId, 0);
        //ADDING NEW CHANGE TO DB
        //addNewServisChange(9,2,taskSubscriber,taskId);
        addChangeSystem(2,9,null,taskId,null,null,null,null,taskSubscriber);
      }
    })
    debugger;
  }
}
//open conflicts modal and fill modal with new conflicts
function openConflictEditModal(conflicts){
  debugger;
  $('#conflictsEditBody').empty();
  for(var i = 0; i < conflicts.length; i++){
    if(conflicts[i].length > 0){
      var conflictWorker = conflicts[i][0].name + " " +conflicts[i][0].surname;
      var listElements = ``;
      for(var j = 0; j < conflicts[i].length; j++){
        var linkStart = '';
        var linkEnd = '';
        var linkTask = '';
        debugger;
        if(conflicts[i][j].id_subscriber){
          //servis conflicts
          if(loggedUser.role.substring(0,5) == 'vodja' || loggedUser.role == 'admin' || loggedUser.role == 'programer plc-jev' || loggedUser.role == 'programer robotov' || loggedUser.role == 'konstrukter' || loggedUser.role == 'kontruktor'){
            linkStart = `<a href="/servis/?taskId=`+conflicts[i][j].id_task+`" class="alert-link">`;
            linkEnd = '</a>';
          }
          listElements += `<li class="list-group-item">
            <div>
              Servis `+linkStart+`<u>`+conflicts[i][j].task_name+`</u>`+linkEnd+` pri <u>`+conflicts[i][j].subscriber+`</u> v času <u>`+new Date(conflicts[i][j].task_start).toLocaleDateString('sl')+` - `+new Date(conflicts[i][j].task_finish).toLocaleDateString('sl')+`</u>.
            </div>
          </li>`;
        }
        else if(conflicts[i][j].id_project){
          //project conflicts
          if(loggedUser.role.substring(0,5) == 'vodja' || loggedUser.role == 'admin' || loggedUser.role == 'programer plc-jev' || loggedUser.role == 'programer robotov' || loggedUser.role == 'konstrukter' || loggedUser.role == 'kontruktor'){
            linkStart = `<a href="/projects/id?id=`+conflicts[i][j].id_project+`" class="alert-link">`;
            linkTask = `<a href="/projects/id?id=`+conflicts[i][j].id_project+`&taskId=`+conflicts[i][j].id_task+`" class="alert-link">`;
            linkEnd = '</a>';
          }
          listElements += `<li class="list-group-item">
            <div>
              `+linkStart+`<u>`+conflicts[i][j].project_name+`</u>`+linkEnd+` na opravilu `+linkTask+`<u>`+conflicts[i][j].task_name+`</u>`+linkEnd+` v času <u>`+new Date(conflicts[i][j].task_start).toLocaleDateString('sl')+` - `+new Date(conflicts[i][j].task_finish).toLocaleDateString('sl')+`</u>.
            </div>
          </li>`;
        }
        else{
          //absence
          if(loggedUser.role.substring(0,5) == 'vodja' || loggedUser.role == 'admin' || loggedUser.role == 'programer plc-jev' || loggedUser.role == 'programer robotov' || loggedUser.role == 'konstrukter' || loggedUser.role == 'kontruktor'){
            linkStart = `<a href="/employees/absence?id=`+conflicts[i][j].id_user+`" class="alert-link">`;
            linkEnd = '</a>';
          }
          listElements += `<li class="list-group-item">
            <div>
              `+linkStart+`<u>`+conflicts[i][j].project_name+`</u>`+linkEnd+` v času <u>`+new Date(conflicts[i][j].task_start).toLocaleDateString('sl')+` - `+new Date(conflicts[i][j].task_finish).toLocaleDateString('sl')+`</u> z nazivom: <u>`+conflicts[i][j].task_name+`</u>.
            </div>
          </li>`;
        }
      }
      var element = `<div><strong>`+conflictWorker+`:</strong></div>
      <ul class="list-group mb-2">
        `+listElements+`
      </ul>`;
      $('#conflictsEditBody').append(element);
    }
  }
  $('#modalConflictsEdit').modal();
}
function onEventDeleteTask(event) {
  deleteTask(this.id.substring(16));
}
function deleteTask(taskId){
  debugger;
  //savedTaskId = taskId;
  savedTaskId = taskId;
  $('#modalDeleteTask').modal('toggle');
}
function deleteTaskConfirm(){
  var taskId = savedTaskId;
  //needs to make a call to server to actually delete task (change activity to false)
  var subId = $('#taskName'+taskId).html().split('(');
  subId = subId[subId.length-1];
  subId = subId.substring(0,subId.length-1);
  subId = allSubscribers.find(s => s.text == subId).id;
  $.post('/projects/removetask', {taskId}, function(resp){
    if(resp.success){
      console.log('Successfully deleting task.');
      $('#modalDeleteTask').modal('toggle');
      if(loggedUser.role == 'admin'){
        if(taskLoaded){
          allTasks.find(t => t.id == taskId).active = false
        }
        $('#task'+taskId).addClass('bg-secondary');
      }
      else{
        if(!taskLoaded){
          $.get( "/servis/tasks", {categoryTask,activeTask, completionTask}, function( data ) {
            //console.log(data)
            //debugger
            allTasks = data.data;
            taskLoaded = true;
            fixTaskTable(taskId);
          });
        }
        else{
          fixTaskTable(taskId);
        }
      }
      //fixCompletion();
      //ADDING NEW CHANGE TO DB
      //addNewServisChange(9,3,subId,taskId);
      addChangeSystem(3,9,null,taskId,null,null,null,null,subId);
    }
    else{
      console.log('Unsuccessfully deleting task.');
      $('#modalDeleteTask').modal('toggle');
      $('#modalError').modal('toggle');
    }
  })
}
//fix table list when user deletes a task
function fixTaskTable(taskId){
  $('#task'+taskId).remove();
  var myCurrentPage = activePage;
  allTasks = allTasks.filter(t => t.id != taskId);
  var pageCounter = Math.ceil(allTasks.length/10);
  drawPagination(pageCounter);
  activePage = 1
  if(myCurrentPage > pageCounter)
    myCurrentPage = pageCounter;
  drawTasks(myCurrentPage);
}
var collapseAddNewTaskOpen = false;
//check if date for new task is ok
function onDatesAddChange(){
  debugger;
  var start = $('#taskStartAdd').val();
  var finish = $('#taskFinishAdd').val();
  if(start && finish){
    if(new Date(start) > new Date(finish)){
      $('#taskStartAdd').addClass('is-invalid');
      $('#taskFinishAdd').addClass('is-invalid');
      $('#btnAddService').prop('disabled', true);
    }
    else{
      $('#taskStartAdd').removeClass('is-invalid');
      $('#taskFinishAdd').removeClass('is-invalid');
      $('#btnAddService').prop('disabled', false);  
    }
    countDays();
  }
  else{
    $('#taskDurationAdd').val(0);
    $('#taskStartAdd').removeClass('is-invalid');
    $('#taskFinishAdd').removeClass('is-invalid');
    $('#btnAddService').prop('disabled', false);
  }
}
//check if edit date of selected task is ok
function onDatesEditChange(){
  debugger;
  var start = $('#taskStartEdit').val();
  var finish = $('#taskFinishEdit').val();
  if(start && finish){
    if(new Date(start) > new Date(finish)){
      $('#taskStartEdit').addClass('is-invalid');
      $('#taskFinishEdit').addClass('is-invalid');
      $('#btnEditService').prop('disabled', true);
    }
    else{
      $('#taskStartEdit').removeClass('is-invalid');
      $('#taskFinishEdit').removeClass('is-invalid');
      $('#btnEditService').prop('disabled', false);
    }
    countEditDays();
  }
  else{
    $('#taskDurationEdit').val(0);
    $('#taskStartEdit').removeClass('is-invalid');
    $('#taskFinishEdit').removeClass('is-invalid');
    $('#btnEditService').prop('disabled', false);
  }
}
function countDays(){
  var start = $('#taskStartAdd').val();
  var finish = $('#taskFinishAdd').val();
  var oneDay = 24*60*60*1000;
  var firstDay = new Date(start);
  var secondDay = new Date(finish);
  var diffDays = Math.round(Math.abs((firstDay.getTime() - secondDay.getTime()) / (oneDay)))+1;
  $('#taskDurationAdd').val(diffDays);
}
function countEditDays(){
  var start = $('#taskStartEdit').val();
  var finish = $('#taskFinishEdit').val();
  var oneDay = 24*60*60*1000;
  var firstDay = new Date(start);
  var secondDay = new Date(finish);
  var diffDays = Math.round(Math.abs((firstDay.getTime() - secondDay.getTime()) / (oneDay)))+1;
  $('#taskDurationEdit').val(diffDays);
}
function onEventGetTaskChanges(event) {
  getTaskChanges(this.id.substring(11));
}
function getTaskChanges(id){
  debugger;
  var taskId = id;
  let data = {taskId};
  $.ajax({
    type: 'GET',
    url: '/servis/changes',
    contentType: 'application/json',
    data: data, // access in body
  }).done(function (resp) {
    if(resp.success){
      debugger;
      var taskChanges = resp.taskChanges;
      console.log('Successfully getting all the changes of selected task/service.');
      var element = "";
      var fileLabel = "";
        for(var i=0; i<taskChanges.length; i++){
          if(taskChanges[i].id_type == 13)
            fileLabel = " datoteko";
          else if(taskChanges[i].id_type == 11){
            if(taskChanges[i].subtask)
              fileLabel = " sporočilo nalogi " + taskChanges[i].subtask;
            else
              fileLabel = " sporočilo";
          }
          else if(taskChanges[i].id_type == 10)
            fileLabel = " nalogo " + taskChanges[i].subtask;
          else if(taskChanges[i].id_type == 12)
            fileLabel = " odsotnost";
          else
            fileLabel = "";
          element = new Date(taskChanges[i].date).toLocaleDateString('sl') + ' ' + new Date(taskChanges[i].date).toLocaleTimeString('sl').substring(0,5) + ", " + taskChanges[i].name + " " + taskChanges[i].surname + ', ' + taskChanges[i].status + fileLabel + "<br />" + element;
        }
        //$('.mypopover').attr('data-content', element);
        $('#taskHistory'+taskId).attr('data-content', element);
        $('#taskHistory'+taskId).popover('show');
        //$('[data-toggle="popover"]').popover({trigger: "hover"}); 
    }
    else{
      $('#modalError').modal('toggle');
    }
  }).fail(function (resp) {
    //console.log('FAIL');
    //debugger;
    $('#modalError').modal('toggle');
  }).always(function (resp) {
    //console.log('ALWAYS');
    //debugger;
  });
}
var savedTaskId;
var overrideTaskId;