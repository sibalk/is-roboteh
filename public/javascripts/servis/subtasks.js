//IN THIS SCRIPT//
//// adding note to subtask, adding/deleting subtask, subtask checkbox, filling collapse for subtask ////

$(function(){
  $( "#subtaskEditInput" ).on('keyup', onPressedEnterEditSubtask);
  $('#btnEditSubtask').on('click', editSubtask);
  $('#btnDeleteSubtaskConf').on('click', deleteSubtaskConfirm);
  $('#btnOverrideSubtask').on('click', subtaskOverride);
})
//TOGGLE BETWEEN MODAL FOR NOTE AND ADD NOTE
function addSubtaskNote(id){
  $("#modalNoteForm").modal();
  $('#noteInput').val("");
  //debugger;
  $('#subtaskIdNote').val(id);
  //subtaskId = id;
}
//OPEN SUBTASKS COLLAPSE AND FILL SUBSTASK LIST
function openSubtasks(taskId){
  //debugger;
  $.get('/projects/subtasks', {taskId}, function(data){
    allSubtasks = data.data;
    allInactiveSubtasks = [];
    //debugger;
    //collapse frame
    var myTask = false;
    var disableControl = ' disabled';
    var tmpTaskWorkers = $('#taskInfo'+taskId).find('a').data().content;
    if(tmpTaskWorkers){
      taskWorkers = tmpTaskWorkers.split(", ");
      var tmpWorker = taskWorkers.find(w => w == (loggedUser.name +" "+ loggedUser.surname))
      if(tmpWorker){
        myTask = true;
        disableControl = '';
      }
    }
    var addElement = '';
    if(loggedUser.role.substring(0,5) == 'vodja' || loggedUser.role == 'admin' || loggedUser.role == 'programer plc-jev' || loggedUser.role == 'programer robotov' || loggedUser.role == 'konstrukter' || loggedUser.role == 'kontruktor')
      addElement = `<div class="d-flex">
        <input class="form-control p-1 mr-auto" id="subtaskInput`+taskId+`" placeholder="" type="text" name="subtaskInput" />
        <div class="input-group-append">
          <button class="btn btn-success" id="addSubtaskBtn`+taskId+`">
            <i class="fas fa-plus fa-lg"></i>
          </button>
        </div>
      </div>`;
    var frame = `<hr />
    <div class="list-group d-flex ml-3 mr-3 span-collapse-content" id="subtaskList`+taskId+`"></div><br />
    `+addElement+`
    <br />`;
    $('#collapseSubtasks'+taskId).append(frame);
    $('#subtaskInput'+taskId).on('keyup', {id:taskId}, onEventAddNewSubtasks);
    $('#addSubtaskBtn'+taskId).on('click', onEventAddNewSubtasks);

    for(var i = 0; i < allSubtasks.length; i++){
      var checkedCB = '';
      if(allSubtasks[i].completed)
        checkedCB = 'checked=""';
      var inactiveClass = "";
      var deleteElement = '';
      var editElement = '';
      var deleteActivity = '';
      var deleteIcon = 'trash';
      if(allSubtasks[i].active == false){
        deleteActivity = '1';
        deleteIcon = 'trash-restore'
        inactiveClass = 'bg-dark';
      }
      //messages icon
      let commentsIcon = '<i class="far fa-comments fa-lg"></i>';
      if(allSubtasks[i].notes_count && allSubtasks[i].notes_count > 0)
        commentsIcon = '<i class="fas fa-comments fa-lg"></i>';
      if(loggedUser.role.substring(0,5) == 'vodja' || loggedUser.role == 'admin' || loggedUser.role == 'programer plc-jev' || loggedUser.role == 'programer robotov' || loggedUser.role == 'konstrukter' || loggedUser.role == 'kontruktor'){
        disableControl = '';
        deleteElement = `<button class="btn btn-danger task-button mr-2 mt-1" id="deleteSubtask`+allSubtasks[i].id+`">
          <i class="fas fa-`+deleteIcon+` fa-lg"></i>
        </button>`;
        editElement = `<button class="btn btn-primary task-button mr-2 mt-1" id="editSubtask`+allSubtasks[i].id+`">
          <i class="fas fa-pen fa-lg"></i>
        </button>`;
      }
      var listElement = `<div class="list-group-item `+inactiveClass+`" id="subtask`+allSubtasks[i].id+`" id="subtask`+allSubtasks[i].id+`">
        <div class="row">
          <div class='col-12 col-xl-9 col-lg-9 col-md-8'>
            <label class="p-1 mr-auto w-70" id="subtaskName`+allSubtasks[i].id+`">`+allSubtasks[i].name+`</label>
          </div>
          <div class='col-12 col-xl-3 col-lg-3 col-md-4 d-flex'>
            <div class="custom-control custom-checkbox ml-auto mt-2">
              <input class="custom-control-input" id="customCheck`+allSubtasks[i].id+`" type="checkbox" `+checkedCB+` `+disableControl+` />
              <label class="custom-control-label" for="customCheck`+allSubtasks[i].id+`"> </label>
            </div>
            <button class="invisible-button mr-1 mt-2" `+disableControl+`>` + commentsIcon + `</button>
            `+deleteElement+`
            `+editElement+`
          </div>
        </div>
        <row class="d-flex" id="subtaskBadges`+allSubtasks[i].id+`"></row>
      </div>`;
      if(allSubtasks[i].active == false){
        if(loggedUser.role == 'admin'){
          $('#subtaskList'+taskId).append(listElement);
          $('#deleteSubtask'+allSubtasks[i].id).on('click', {id: allSubtasks[i].id, activity: deleteActivity}, onEventDeleteSubtask);
          $('#editSubtask'+allSubtasks[i].id).on('click', {id: allSubtasks[i].id}, onEventOpenEditSubtask);
          $('#subtask'+allSubtasks[i].id).find('.invisible-button').on('click', {id: allSubtasks[i].id, type: 2}, onClickOpenMessagesModal);
          $('#customCheck'+allSubtasks[i].id).on('change', toggleSubtaskCheckbox);
        }
        var id = allSubtasks[i].id;
        allInactiveSubtasks.push(allSubtasks[i]);
        allSubtasks = allSubtasks.filter(s => s.id != id);
        i--;
      }
      else{
        $('#subtaskList'+taskId).append(listElement);
        $('#deleteSubtask'+allSubtasks[i].id).on('click', {id: allSubtasks[i].id, activity: deleteActivity}, onEventDeleteSubtask);
        $('#editSubtask'+allSubtasks[i].id).on('click', {id: allSubtasks[i].id}, onEventOpenEditSubtask);
        $('#subtask'+allSubtasks[i].id).find('.invisible-button').on('click', {id: allSubtasks[i].id, type: 2}, onClickOpenMessagesModal);
        $('#customCheck'+allSubtasks[i].id).on('change', toggleSubtaskCheckbox);
      }
    }
    //$('[data-toggle="popover"]').popover({trigger: "hover"});
    $('#collapseSubtasks'+taskId).collapse("toggle");
    subtaskCollapseOpen = true;
  })
}
function onEventAddNewSubtasks(event) {
  //debugger
  if(event.which == 1){
    addNewSubtask(activeTaskId);
  }
  else if(event.which == 13){
    addNewSubtask(activeTaskId);
  }
}
function addNewSubtask(taskId){
  //debugger;
  if($('#subtaskInput'+taskId).val() == ''){
    $('#subtaskInput'+taskId).addClass('is-invalid');
    $('#subtaskInput'+taskId).attr('placeholder', 'Vnesi naziv nove naloge!');
  }
  else{
    var newSubtask = $('#subtaskInput'+taskId).val();
    $.post('/projects/subtask/add', {taskId, newSubtask}, function(resp){
      if(resp.success){
        console.log("Successfully adding new servis subtask.");
        //debugger;
        var checkedCB = '';
        var popoverText = "";
        var newId = resp.data.id;
        var listElement = `<div class="list-group-item" id="subtask`+newId+`">
          <div class="row">
            <div class='col-12 col-xl-9 col-lg-9 col-md-8'>
              <label class="p-1 mr-auto w-70">`+newSubtask+`</label>
            </div>
            <div class='col-12 col-xl-3 col-lg-3 col-md-4 d-flex'>
              <div class="custom-control custom-checkbox ml-auto mt-2">
                <input class="custom-control-input" id="customCheck`+newId+`" type="checkbox" `+checkedCB+` />
                <label class="custom-control-label" for="customCheck`+newId+`"> </label>
              </div>
              <button class="invisible-button mr-1 mt-2"><i class="fas fa-comments fa-lg"></i></button>
              <button class="btn btn-danger task-button mr-2 mt-1" id="deleteSubtask`+newId+`">
                <i class="fas fa-trash fa-lg"></i>
              </button>
              <button class="btn btn-primary task-button mr-2 mt-1" id="editSubtask`+newId+`">
                <i class="fas fa-pen fa-lg"></i>
              </button>
            </div>
          </div>
          <row class="d-flex" id="subtaskBadges`+newId+`"></row>
        </div>`;
        $('#subtaskList'+taskId).append(listElement);
        $('#deleteSubtask'+newId).on('click', {id: newId}, onEventDeleteSubtask);
        $('#editSubtask'+newId).on('click', {id: newId}, onEventOpenEditSubtask);
        $('#subtask'+newId).find('.invisible-button').on('click', {id: newId, type: 2}, onClickOpenMessagesModal);
        $('#customCheck'+newId).on('change', toggleSubtaskCheckbox);
        //$('[data-toggle="popover"]').popover({trigger: "hover"});
        allSubtasks.push({id:newId, id_task: taskId, name: newSubtask, note: null, completed: false});
        var numberOfCompleted = 0;
        for(var i=0; i<allSubtasks.length; i++){
          if(allSubtasks[i].completed)
          numberOfCompleted++;
        }
        var completion = Math.round((numberOfCompleted/allSubtasks.length)*100)
        if(numberOfCompleted == '0' || allSubtasks.length == '0')
          completion = 0;
        //console.log(completion);
        //fix completion on task in db
        fixTaskCompletion(taskId, completion);
        if(allTasks){
          var task = allTasks.find(t => t.id == taskId);
          task.subtasks_count = allSubtasks.length;
        }
        //change button to black cuz now task has subtasks
        // if($('#taskButtonSubtask'+taskId).find('i').css('color') == 'rgb(255, 255, 255)'){
        //   //change button to black
        //   $('#taskButtonSubtask'+taskId).find('i').css('color', '#000000');
        //   //remove checkbox on task
        //   $('#taskCheckbox'+taskId).empty()
        // }
        $('#taskButtonSubtask'+taskId).find('i').addClass('text-dark');
        //remove checkbox on task
        $('#taskCheckbox'+taskId).empty()
        $('#subtaskInput'+taskId).val('');
        $('#subtaskInput'+taskId).attr('placeholder', '');
        $('#subtaskInput'+taskId).removeClass('is-invalid');
        setTimeout(()=>{
          var subId = $('#taskName'+taskId).html().split('(');
          subId = subId[subId.length-1];
          subId = subId.substring(0,subId.length-1);
          subId = allSubscribers.find(s => s.text == subId).id;
          //ADDING NEW CHANGE TO DB
          //addNewServisChange(10,1,subId,taskId,newId);
          addChangeSystem(1,10,null,taskId,newId,null,null,null,subId);
        })
      }
      else{
        console.log("Successfully adding new servis subtask.");
        $('#modalError').modal('toggle');
      }
    })
  }
}
function onEventOpenEditSubtask(event) {
  openEditSubtask(event.data.id);
}
function openEditSubtask(subtaskId){
  debugger;
  let tmpSubtask = allSubtasks.find(s => s.id == subtaskId);
  if(!tmpSubtask)
    tmpSubtask = allInactiveSubtasks.find(s => s.id == subtaskId);
  $('#subtaskEditInput').val(tmpSubtask.name);
  //var button = `<button class="btn btn-success" id="btnEditSubtask" onclick="editSubtask()">Posodobi</button>`;
  //$('#editSubtaskButtonBody').empty();
  //$('#editSubtaskButtonBody').append(button);
  savedSubtaskId = subtaskId;
  // $( "#subtaskEditInput" ).on( "keydown", function(event) {
  //   if(event.which == 13) 
  //      editSubtask(subtaskId);
  // });
  $("#modalEditSubtask").modal('toggle');
}
function onPressedEnterEditSubtask(event) {
  if(event.which == 13)
    editSubtask();
}
function editSubtask(){
  let subtaskId = savedSubtaskId;
  //post and close on success
  var name = $('#subtaskEditInput').val();
  $.post('/projects/subtask/edit', {subtaskId, name}, function(data){
    if(data.success){
      var tmp = allSubtasks.find(s => s.id == subtaskId);
      tmp.name = name;
      $('#subtaskName'+subtaskId).html(name);
      var subId = $('#taskName'+tmp.id_task).html().split('(');
      subId = subId[subId.length-1];
      subId = subId.substring(0,subId.length-1);
      subId = allSubscribers.find(s => s.text == subId).id;
      //addNewServisChange(10,2,subId,tmp.id_task,subtaskId);
      addChangeSystem(2,10,null,tmp.id_task,subtaskId,null,null,null,subId);
      //addNewProjectChange(3,2,projectId,tmp.id_task,subtaskId);
      $('#editSubtaskButtonBody').empty();
      $("#modalEditSubtask").modal('toggle');
    }
    else{
      console.log("Unsuccessfuly editing servis subtask.")
      $('#modalError').modal('toggle');
    }
  })
}
function onEventDeleteSubtask(event) {
  debugger;
  event.data.activity ? deleteSubtask(event.data.id, event.data.activity) : deleteSubtask(event.data.id);
}
//DELETE SELECTED SUBTASK
function deleteSubtask(id, activity){
  $('#additionalWarning').hide();
  var active = false;
  if(activity && activity == 1)
  active = true;
  savedSubtaskId = id;
  savedSubtaskActive = active;
  savedSubtaskActivity = activity;
  if(active){
    $('#btnDeleteSubtaskConf').html('Ponovno dodaj');
    $('#deleteSubtaskModalMsg').html('Ste prepričani, da želite ponovno dodati izbrisano poročilo?');
    if(allInactiveSubtasks.find(s => s.id == id).completed && allSubtasks.length == 0 && ($('#taskWorkorders'+activeTaskId).attr('data-content') == 'brez delovnega naloga')){
      $('#warningSubtaskDeleteMsg').html('<strong>Storitev nima povezanega delovnega naloga. Po ponovni dodaji naloge bo opravilo označeno kot zaključeno, čeprav nima delovnega naloga!')
      $('#additionalWarning').show();
    }
  }
  else{
    $('#btnDeleteSubtaskConf').html('Odstrani');
    $('#deleteSubtaskModalMsg').html('Ste prepričani, da želite odstraniti to nalogo?');
    var tmp = allSubtasks.filter(s => s.id != id && s.completed == false);
    if(tmp.length == 0 && ($('#taskWorkorders'+activeTaskId).attr('data-content') == 'brez delovnega naloga') && allSubtasks.filter(s => s.id != id).length > 0 ){
      $('#warningSubtaskDeleteMsg').html('<strong>Storitev nima povezanega delovnega naloga. Po izbrisani nalogi bo opravilo označeno kot zaključeno, čeprav nima delovnega naloga!')
      $('#additionalWarning').show();
    }
  }
  $('#modalDeleteSubtask').modal('toggle');
}
function deleteSubtaskConfirm(){
  var taskId = activeTaskId;
  var id = savedSubtaskId;
  var active = savedSubtaskActive;
  var activity = savedSubtaskActivity;
  var subId = $('#taskName'+taskId).html().split('(');
  subId = subId[subId.length-1];
  subId = subId.substring(0,subId.length-1);
  subId = allSubscribers.find(s => s.text == subId).id;
  //debugger;
  //not sure why i need taskId for deletin subtask
  $.post('/projects/subtask/delete', {id, active}, function(resp){
    if(resp.success){
      $('#deleteSubtask'+id).off('click');
      console.log('Successfully deleting subtask.');
      $('#modalDeleteSubtask').modal('toggle');
      //debugger
      if(loggedUser.role == 'admin'){
        if(activity && activity == 1){
          //resurection
          $('#subtask'+id).removeClass('bg-dark');
          //$('#deleteSubtask'+id).attr('onclick', 'deleteSubtask('+id+')');
          //$('#deleteSubtask'+id).find('img').attr('src', '/delete.png');
          $('#deleteSubtask'+id).on('click', {id}, onEventDeleteSubtask);
          $('#deleteSubtask'+id).find('i').removeClass('fa-trash-restore');
          $('#deleteSubtask'+id).find('i').addClass('fa-trash');
          var tmp = allInactiveSubtasks.find(s => s.id == id);
          allSubtasks.push(tmp);
          allInactiveSubtasks = allInactiveSubtasks.filter(s => s.id != id);
          //ADDING NEW CHANGE TO DB
          //addNewServisChange(10,6,subId,taskId,id);
          addChangeSystem(6,10,null,taskId,id,null,null,null,subId);
        }
        else{
          //deleting
          $('#subtask'+id).addClass('bg-dark');
          //$('#deleteSubtask'+id).attr('onclick', 'deleteSubtask('+id+',1)');
          $('#deleteSubtask'+id).on('click', {id, activity:1}, onEventDeleteSubtask);
          //$('#deleteSubtask'+id).find('img').attr('src', '/undelete1.png');
          $('#deleteSubtask'+id).find('i').removeClass('fa-trash');
          $('#deleteSubtask'+id).find('i').addClass('fa-trash-restore');
          var tmp = allSubtasks.find(s => s.id == id);
          allInactiveSubtasks.push(tmp);
          allSubtasks = allSubtasks.filter(s => s.id != id);
          //ADDING NEW CHANGE TO DB
          //addNewServisChange(10,3,subId,taskId,id);
          addChangeSystem(3,10,null,taskId,id,null,null,null,subId);
        }
      }
      else{
        $('#subtask'+id).remove();
        var tmp = allSubtasks.find(s => s.id == id);
        allInactiveSubtasks.push(tmp);
        allSubtasks = allSubtasks.filter(s => s.id != id);
        //ADDING NEW CHANGE TO DB
        //addNewServisChange(10,3,subId,taskId,id);
        addChangeSystem(3,10,null,taskId,id,null,null,null,subId);
      }
      var numberOfCompleted = 0;
      for(var i=0; i<allSubtasks.length; i++){
        if(allSubtasks[i].completed)
        numberOfCompleted++;
      }
      //calc new completion of task
      var completion = Math.round((numberOfCompleted/allSubtasks.length)*100)
      if(numberOfCompleted == 0 || allSubtasks.length == 0)
        completion = 0;
      //console.log(completion);
      //post new completion to fix in db
      fixTaskCompletion(taskId, completion);
      if(allTasks){
        var task = allTasks.find(t => t.id == taskId);
        task.subtasks_count = allSubtasks.length;
      }
      if(allSubtasks.length == 0){
        //change button to white so it can be seen from button it has no subtasks
        //$('#taskButtonSubtask'+taskId).find('img').attr('src', '/subtaskWhite.png');
        $('#taskButtonSubtask'+taskId).find('i').removeClass('text-dark');
        //add checkbox so it can be checked as task
        var checkboxElement = `<input class="custom-control-input" id="customTaskCheck`+taskId+`" type="checkbox" />
        <label class="custom-control-label" for="customTaskCheck`+taskId+`"> </label>`;
        $('#taskCheckbox'+taskId).append(checkboxElement)
        $('#customTaskCheck'+taskId).on('change', toggleTaskCheckbox);
      }
      else{
        $('#taskButtonSubtask'+taskId).find('i').addClass('text-dark');
        $('#taskCheckbox'+taskId).empty();
      }
    }
    else{
      console.log('Unsuccessfully deleting subtask.');
      $('#modalDeleteSubtask').modal('toggle');
      $('#modalError').modal('toggle');
    }
  })
}
function toggleSubtaskCheckbox(element){
  debugger;
  var subtaskId = parseInt(this.id.substring(11));
  var completed = this.checked;
  var taskId = activeTaskId;
  if((completed == true) && ($('#taskWorkorders'+activeTaskId).attr('data-content') == 'brez delovnega naloga') && (allSubtasks.filter(s => s.completed == false).length <= 1)){
    //console.log('open warning modal');
    overrideSubtaskId = subtaskId;
    $('#customCheck'+subtaskId).prop('checked', false);
    $('#btnOverrideTask').hide();
    $('#btnOverrideSubtask').show();
    $('#modalWarningWO').modal('toggle');
  }
  else{
    $.post('/projects/subtask', {subtaskId, completed}, function(data){
      var tmpCurrent = $('#taskProgress'+activeTaskId).width();
      var tmpFull = $('#taskProgress'+activeTaskId).parent().width();
      var oldCompletion = Math.round(tmpCurrent/tmpFull*100);
      //debugger;
      //calculate completion via subtask
      var temp = allSubtasks.find(s => s.id == subtaskId)
      if(temp){
        //fix completion in allSubtasks
        temp.completed = completed;
        //calc task completion
        var numberOfCompleted = 0;
        for(var i=0; i<allSubtasks.length; i++){
          if(allSubtasks[i].completed)
          numberOfCompleted++;
        }
        var completion = Math.round((numberOfCompleted/allSubtasks.length)*100)
        if(numberOfCompleted == '0' || allSubtasks.length == '0')
          completion = 0;
        //console.log(completion);
        fixTaskCompletion(taskId, completion, oldCompletion);
        //debugger;
      }
      else{
        //its deleted subtask, only fix completion in table
        var tmp = allInactiveSubtasks.find(s => s.id == subtaskId);
        if(tmp){
          tmp.completed = completed;
        }
      }
      var subId = $('#taskName'+taskId).html().split('(');
      subId = subId[subId.length-1];
      subId = subId.substring(0,subId.length-1);
      subId = allSubscribers.find(s => s.text == subId).id;
      if(completed)
        //ADDING NEW CHANGE TO DB
        //addNewServisChange(10,4,subId,taskId,subtaskId);
        addChangeSystem(4,10,null,taskId,subtaskId,null,null,null,subId);
      else
        //ADDING NEW CHANGE TO DB
        //addNewServisChange(10,5,subId,taskId,subtaskId);
        addChangeSystem(5,10,null,taskId,subtaskId,null,null,null,subId);
    })
  }
}
function subtaskOverride(){
  var subtaskId = overrideSubtaskId;
  var completed = true;
  $('#customCheck'+subtaskId).prop('checked', true);
  var taskId = activeTaskId;
  $.post('/projects/subtask', {subtaskId, completed}, function(data){
    var tmpCurrent = $('#taskProgress'+activeTaskId).width();
    var tmpFull = $('#taskProgress'+activeTaskId).parent().width();
    var oldCompletion = Math.round(tmpCurrent/tmpFull*100);
    //debugger;
    //calculate completion via subtask
    $('#modalWarningWO').modal('toggle');
    var temp = allSubtasks.find(s => s.id == subtaskId)
    if(temp){
      //fix completion in allSubtasks
      temp.completed = completed;
      //calc task completion
      var numberOfCompleted = 0;
      for(var i=0; i<allSubtasks.length; i++){
        if(allSubtasks[i].completed)
        numberOfCompleted++;
      }
      var completion = Math.round((numberOfCompleted/allSubtasks.length)*100)
      if(numberOfCompleted == '0' || allSubtasks.length == '0')
        completion = 0;
      //console.log(completion);
      fixTaskCompletion(taskId, completion, oldCompletion);
      //debugger;
    }
    else{
      //its deleted subtask, only fix completion in table
      var tmp = allInactiveSubtasks.find(s => s.id == subtaskId);
      if(tmp){
        tmp.completed = completed;
      }
    }
    var subId = $('#taskName'+taskId).html().split('(');
    subId = subId[subId.length-1];
    subId = subId.substring(0,subId.length-1);
    subId = allSubscribers.find(s => s.text == subId).id;
    if(completed)
      //ADDING NEW CHANGE TO DB
      //addNewServisChange(10,4,subId,taskId,subtaskId);
      addChangeSystem(4,10,null,taskId,subtaskId,null,null,null,subId);
    else
      //ADDING NEW CHANGE TO DB
      //addNewServisChange(10,5,subId,taskId,subtaskId);
      addChangeSystem(5,10,null,taskId,subtaskId,null,null,null,subId);
  })
}
var allInactiveSubtasks = [];
var savedSubtaskId;
var savedSubtaskActivity;
var savedSubtaskActive;
var overrideSubtaskId;