//IN THIS SCRIPT//
//// drawing all tasks of project, opening/closing collaps for files, drawing tasks files ////

$(function(){
  //$('[data-toggle="tooltip"]').tooltip();
  $('[data-toggle="popover"]').popover()
  if($('#msg').html() != ''){
    $("#modalMsg").modal();
    //set timeout so i dont loose msg right away
    if($('#taskId').html() == 'OK'){
      setTimeout(()=>{
        selectedTaskId = urlParams.get('taskId');
        window.history.pushState({}, document.title, "/" + "servis/?taskId="+selectedTaskId);
      })
    }
    else{
      setTimeout(()=>{
        window.history.pushState({}, document.title, "/" + "servis/");
      })
    }
  }
  $('#modalTaskDocumentAddForm').on('hidden.bs.modal', function () {
    $("#modalTaskFilesForm").toggle();
    $("#modalTaskFilesForm").focus();
  })
  $("#addTaskDocForm").submit(function (e) {
    //event.preventDefault();
    e.preventDefault();
  
    var file = document.getElementById("docNew").files[0];
    var formData = new FormData();
    formData.append("docNew", file);
    formData.append("taskId", activeTaskId);
    //console.log(formData);
    debugger;
    
    $.ajax({
      url: '/servis/taskFileUpload',
      type: 'POST',
      data: formData,
      contentType: false,
      processData: false,
      success: function(res) {
        //console.log('success');
        let tmpIsTask = true, tmpIsMod = false;
        $('#btnAddTaskDoc').attr('disabled',false);
        $('#docNew').val('');
        //addSystemChange(23,1,workOrderId,workOrderFileId);
        //addChangeSystem(1,23,null,null,null,null,null,null,null,null,null,workOrderId,null,null,null,null,null,workOrderFileId);
        $('#modalTaskDocumentAddForm').modal('toggle')
        //add new file to the list of all files to this work order
        var fullDate = new Date(res.file.date).toLocaleString("sl");
        var date = fullDate.substring(0,11);
        var time = fullDate.substring(11,16);
        var fileSrc = '/file'+res.file.type+'.png'
        var tmpElem = '', tmpTag = '';
        if (res.file.base64 && res.file.type == 'IMG'){
          fileSrc = res.file.base64;
          tmpElem = `<div class="overlay-zoom">
            <div class="icon-overlay">
              <i class="fas fa-search-plus"></i>
            </div>
          </div>`;
          tmpTag = `<div class="overlay-tag"">
            <img class="img-fluid" src="/imgTag.png" />
          </div>`;
        }
        else if (res.file.base64 && res.file.type == 'PDF'){
          // convertPDFCanvasToImg(res.file.base64, res.file.id);
          tmpElem = `<div class="overlay-zoom">
            <div class="icon-overlay">
              <i class="fas fa-search-plus"></i>
            </div>
          </div>`;
          tmpTag = `<div class="overlay-tag"">
            <img class="img-fluid" src="/pdfTag.png" />
          </div>`;
        }
        var listElement = `<div class="list-group-item" id="taskFile`+res.file.id+`">
          <div class="row">
            <div class="col-sm-3 justify-content-center d-flex">
              <div class="preview-icon">  
                <img class="img-fluid bg-light image-icon w-70px" src=` + fileSrc + ` alt="ikona" />
                ` + tmpElem + `
                ` + tmpTag + `
              </div>
            </div>
            <div class="col-sm-7">
              <label class="row mt-2">`+res.file.original_name+`</label>
              <label class="row mt-2 small">Avtor: `+loggedUser.name+" "+loggedUser.surname+`</label>
            </div>
            <div class="col-sm-2">
              <div class="d-flex">
                <button class="btn btn-danger ml-auto mr-2 mt-3" id="fileButtonDelete`+res.file.id+`" data-toggle="tooltip" data-original-title="Odstrani">
                  <i class="fas fa-trash fa-lg"></i>
                </button>
                <a class="btn btn-primary mt-3" href="/projects/sendMeDoc?filename=`+res.file.path_name+`">
                  <i class="fas fa-download fa-lg"></i>
                </a>
              </div>
            </div>
          </div>
        </div>`;
        $('#fileList'+activeTaskId).prepend(listElement);
        $('#taskFile' + res.file.id).find('.btn-danger').on('click', {id: res.file.id}, onEventDeleteFile);
        if (res.file.base64 && res.file.type == 'IMG')
          $('#taskFile' + res.file.id).find('.overlay-zoom').on('click', {id: res.file.id, filePath: res.file.path_name}, onEventGetIMGData);
        else if (res.file.base64 && res.file.type == 'PDF')
          $('#taskFile' + res.file.id).find('.overlay-zoom').on('click', {id: res.file.id, isTask: tmpIsTask, filePath: res.file.path_name}, onEventGetPDFData);
        allTaskFiles.unshift({
          active: res.file.active,
          author: res.file.author,
          date: res.file.date,
          id: res.file.id,
          id_project: res.file.id_project,
          id_subtask: res.file.id_subtask,
          id_task: res.file.id_task,
          name: loggedUser.name,
          note: res.file.note,
          original_name: res.file.original_name,
          path_name: res.file.path_name,
          surname: loggedUser.surname,
          type: res.file.type,
        });
        //prepare modalMsg for info about success
        $('#msgImg').attr('src','/ok_sign.png');
        $('#msg').html('Uspešno nalaganje nove datoteke.');
        $('#modalMsg').modal('toggle');
        if (res.file.type == 'PDF' && res.file.base64){
          setTimeout(()=>{
            convertPDFCanvasToImg(res.file.base64, res.file.id, true);
          },500)
        }
        //add color to file icon
        $('#taskButtonFile'+activeTaskId).find('i').addClass('text-dark');
        //correct the files count in allTasks for correct one
        let tmpTask = allTasks.find(t => t.id == activeTaskId);
        tmpTask.files_count = tmpTask.files_count ? (tmpTask.files_count + 1) : 1;
      },
      error: function(res) {
        //console.log('error');
        $('#btnAddTaskDoc').attr('disabled',false);
        $('#docNew').val('');
        $('#modalTaskDocumentAddForm').modal('toggle')
        //prepare modalMsg for info about success
        $('#msgImg').attr('src','/warning_sign.png');
        $('#msg').html(res.responseJSON.message);
        $('#modalMsg').modal('toggle');
      }
    })
  });
  //get project files
  //no need, every time new file is added page is reloaded so server can already fetch files

  $('#btnDeleteFile').on('click', deleteFileConf);
})
function onEventNewTaskDocumentModal(event) {
  newTaskDocumentModal(event.data.id);
}
function newTaskDocumentModal(taskId){
  //open modal for adding files that are connected to task
  //$("#modalTaskFilesForm").toggle();
  $("#modalTaskDocumentAddForm").modal();
  $('#taskIdTaskFile').val(taskId);
  debugger;
}
function disableFunction(){
  //$('#btnAddDoc').attr('disabled','disabled');
  $('#btnAddTaskDoc').attr('disabled','disabled');
}
function disableFunctionTask(){
  $('#btnAddTaskDoc').attr('disabled','disabled');
}

//OPEN TASK FILES COLLAPSE AND FILL TASK FILES LIST
function openTaskFiles(taskId){
  //debugger;
  $.get('/projects/taskFiles', {taskId}, function(data){
    let files = data.data;
    //debugger;
    //collapse frame
    var frame = `<hr />
    <div class="list-group span-collapse-content" id="fileList`+taskId+`"></div><br />
    <div id="addFileRow`+taskId+`">
      <div class="text-center">
        <button class="btn btn-success">Dodaj datoteko</button>
      </div>
    </div><br />`;
    $('#collapseTaskFiles'+taskId).append(frame);
    $('#addFileRow'+taskId).find('.btn-success').on('click', {id: taskId}, onEventNewTaskDocumentModal);

    for(var i = 0; i < files.length; i++){
      let deleteButton = ``;
      let downloadClassStyle = '';
      if ((files[i].name + ' ' + files[i].surname) == (loggedUser.name + ' ' + loggedUser.surname) || loggedUser.role == 'admin' || loggedUser.role.substring(0,5) == 'vodja'){
        deleteButton = `<button class="btn btn-danger ml-auto mr-2 mt-3"  id="fileButtonDelete`+files[i].id+`" data-toggle="tooltip" data-original-title="Odstrani">
          <i class="fas fa-trash fa-lg"></i>
        </button>`;
      }
      else{
        downloadClassStyle = ' ml-auto';
      }
      var downloadButton = `<a class="btn btn-primary mt-3`+downloadClassStyle+`" href="/projects/sendMeDoc?filename=`+files[i].path_name+`&projectId=`+projectId+`">
        <i class="fas fa-download fa-lg"></i>
      </a>`;
      if(files[i].active == false)
        downloadButton = `<a class="btn btn-primary mt-3 disabled`+downloadClassStyle+`" href=""><i class="fas fa-download fa-lg"></i></a>`;
      var date = new Date(files[i].date).toLocaleDateString('sl');
      var time = new Date(files[i].date).toLocaleTimeString('sl').substring(0,5);
      var fileSrc = '/file'+files[i].type+'.png'
      var listElement = `<div class="list-group-item" id="taskFile`+files[i].id+`">
        <div class="row">
          <div class="col-sm-3 justify-content-center d-flex">
            <div class="preview-icon">  
              <img class="img-fluid bg-light image-icon w-70px" src=` + fileSrc + ` alt="ikona" />
            </div>
          </div>
          <div class="col-sm-7">
            <label class="row mt-2">`+files[i].original_name+`</label>
            <label class="row mt-2 small">Avtor: `+files[i].name+" "+files[i].surname+`</label>
          </div>
          <div class="col-sm-2">
            <div class="d-flex">
              `+deleteButton+`
              `+downloadButton+`
            </div>
          </div>
        </div>
      </div>`;
      
      $('#fileList'+taskId).append(listElement);
      $('#taskFile' + files[i].id).find('.btn-danger').on('click', {id: files[i].id}, onEventDeleteFile);
    }
    allTaskFiles = files;
    $('#collapseTaskFiles'+taskId).collapse("toggle");
    taskFilesCollapseOpen = true;
    setTimeout(()=>{
      getFileThumbnails(files, true);
    },500)
  });
}
function getFileThumbnails(allFiles, isTask) {
  for(var i = 0, l = allFiles.length; i < l; i++){
    if(allFiles[i].type == 'IMG' || allFiles[i].type == 'PDF'){
      var fileId = allFiles[i].id, filePath = allFiles[i].path_name, fileType = allFiles[i].type;
      var tmpIsTask = isTask ? true : false;
      $.get('/projects/files/thumbnail', {fileId, filePath, fileType}, function(data){
        if (data.success){
          //tmpFileInfo = data.data;
          if(data.type == 'IMG'){
            if (isTask)
              $('#taskFile'+data.id).find('img').prop('src', data.data);
            else
              $('#file'+data.id).find('img').prop('src', data.data);
            var tmpElem = `<div class="overlay-zoom">
              <div class="icon-overlay">
                <i class="fas fa-search-plus"></i>
              </div>
            </div>`;
            var tmpTag = `<div class="overlay-tag"">
              <img class="img-fluid" src="/imgTag.png" />
            </div>`
            if (isTask){
              $('#taskFile' + data.id).find('div .preview-icon').append(tmpElem);
              $('#taskFile' + data.id).find('div .preview-icon').append(tmpTag);
              $('#taskFile' + data.id).find('.overlay-zoom').on('click', {id: data.id, filePath: data.filePath}, onEventGetIMGData);
            }
            else{
              $('#file' + data.id).find('div .preview-icon').append(tmpElem);
              $('#file' + data.id).find('div .preview-icon').append(tmpTag);
              $('#file' + data.id).find('.overlay-zoom').on('click', {id: data.id, filePath: data.filePath}, onEventGetIMGData);
            }
          }
          else if(data.type == 'PDF'){
            //tmpFileInfo = data;
            //tmpPDFThumbnailData.push({fileId: data.id, fileBase64: data.data});
            convertPDFCanvasToImg(data.data, data.id, tmpIsTask);
            var tmpElem = `<div class="overlay-zoom">
              <div class="icon-overlay">
                <i class="fas fa-search-plus"></i>
              </div>
            </div>`;
            var tmpTag = `<div class="overlay-tag"">
              <img class="img-fluid" src="/pdfTag.png" />
            </div>`;
            if (isTask){
              $('#taskFile' + data.id).find('div .preview-icon').append(tmpElem);
              $('#taskFile' + data.id).find('div .preview-icon').append(tmpTag);
              $('#taskFile' + data.id).find('.overlay-zoom').on('click', {id: data.id, isTask: tmpIsTask, filePath: data.filePath}, onEventGetPDFData);
            }
            else{
              $('#file' + data.id).find('div .preview-icon').append(tmpElem);
              $('#file' + data.id).find('div .preview-icon').append(tmpTag);
              $('#file' + data.id).find('.overlay-zoom').on('click', {id: data.id, isTask: tmpIsTask, filePath: data.filePath}, onEventGetPDFData);
            }
          }
        }
        else{
          if (data.type == 'IMG'){
            if (isTask)
              $('#taskFile'+data.id).find('img').prop('src', '/fileNoIMG.png');
            else
              $('#file'+data.id).find('img').prop('src', '/fileNoIMG.png');
          }
          else if(data.type == 'PDF'){
            if (isTask)
              $('#taskFile'+data.id).find('img').prop('src', '/fileNoPDF.png');
            else
              $('#file'+data.id).find('img').prop('src', '/fileNoPDF.png');
          }
          //console.log("error while getting thumnail or file does not exist");
        }
      });
    }
  }
}
function convertPDFCanvasToImg(pdfData, fileId, isTask) {
  var loadingTask = pdfjsLib.getDocument({ data: atob(pdfData), });
  loadingTask.promise.then(function(pdf) {
    var canvasdiv = document.getElementById('the-canvas');
    var totalPages = pdf.numPages
    var data = [];
      
    pdf.getPage(1).then(function(page) {
      var scale = 1.5;
      var viewport = page.getViewport({ scale: scale });

      var canvas = document.createElement('canvas');
      canvasdiv.appendChild(canvas);

      // Prepare canvas using PDF page dimensions
      var context = canvas.getContext('2d');
      canvas.height = viewport.height;
      canvas.width = viewport.width;

      // Render PDF page into canvas context
      var renderContext = { canvasContext: context, viewport: viewport };

      var renderTask = page.render(renderContext);
      renderTask.promise.then(function() {
        data.push(canvas.toDataURL('image/png'))
        //tmpData.push(data);
        if (isTask)
          $('#taskFile'+fileId).find('img.image-icon').prop('src', data[0]);
        else
          $('#file'+fileId).find('img.image-icon').prop('src', data[0]);
        //$('#file'+fileId).find('img.image-icon').css('width', '70px');
        //console.log(data.length + ' page(s) loaded in data')
      });
    })
  })
}
function onEventGetPDFData(event) {
  getPDFData(event.data.id, event.data.isTask, event.data.filePath);
}
function getPDFData(id, isTask, filePath) {
  var tmp = `<embed src="/projects/files/resources/?fileName=` + filePath + `" class="pdfobject embeded-pdf" type="application/pdf">`
  var height = isTask ? $('#taskFile'+id).find('img').css('height').match(/[\d\.]+/) : $('#file'+id).find('img').css('height').match(/[\d\.]+/);
  var width = isTask ? $('#taskFile'+id).find('img').css('width').match(/[\d\.]+/) : $('#file'+id).find('img').css('width').match(/[\d\.]+/);
  if (Math.round(parseFloat(height)/parseFloat(width) * 100)/100 == 1.41){
    $("#pdfPreviewContainer").removeClass('document-content');
    $("#pdfPreviewContainer").addClass('document-content-vertical-pdf');
  }
  else {
    $("#pdfPreviewContainer").removeClass('document-content-vertical-pdf');
    $("#pdfPreviewContainer").addClass('document-content');
  }
  $("#pdfPreviewContainer").empty();
  $("#imgPreviewContainer").empty();
  $('#imgPreviewContainer').css('display', 'none');
  $('#pdfPreviewContainer').css('display', '');
  $('#modalPDFPreview').modal('toggle');
  $("#pdfPreviewContainer").append(tmp);
}
function onEventGetIMGData(event) {
  getIMGData(event.data.id, event.data.filePath);
}
function getIMGData(id, filePath) {
  var tmp = `<img class="modal-img img-fluid" src="/projects/files/resources/?fileName=` + filePath + `">`
  $("#pdfPreviewContainer").empty();
  $("#imgPreviewContainer").empty();
  $('#imgPreviewContainer').css('display', '');
  $('#pdfPreviewContainer').css('display', 'none');
  $('#modalPDFPreview').modal('toggle');
  $("#imgPreviewContainer").append(tmp);
}
function onEventDeleteFile(event) {
  deleteFile(event.data.id);
}
function deleteFile(id) {
  debugger;
  savedFileId = id;
  $('#modalDeleteProjectFile').modal('toggle');
}
function deleteFileConf() {
  //delete file with id that is same as savedFileId --> send savedFileId to projects/file DELETE
  debugger;
  let data = {fileId:savedFileId};
  $.ajax({
    type: 'DELETE',
    url: '/projects/files',
    contentType: 'application/json',
    data: JSON.stringify(data), // access in body
  }).done(function (resp) {
    //console.log('SUCCESS');
    debugger;
    if(resp.success){
      //file was successfully deleted and marked as deleted, remove all files listing in variables and DOM elements
      //task files
      tmp = allTaskFiles ? allTaskFiles.find(pf => pf.id == resp.file.id) : null;
      if(tmp){
        $('#taskFile'+resp.file.id).remove();
        allTaskFiles = allTaskFiles.filter(pf => pf.id != resp.file.id);
      }
      if($('#fileList'+activeTaskId).children().length == 0)
        $('#taskButtonFile'+activeTaskId).find('i').removeClass('text-dark');
      //fix files count number
      let tmpTask = allTasks.find(t => t.id == resp.file.id_task);
      tmpTask.files_count =  tmpTask.files_count - 1;

      $('#modalDeleteProjectFile').modal('toggle');
    }
    else{
      $('#modalDeleteProjectFile').modal('toggle');
      $('#modalError').modal('toggle');
    }
    addChangeSystem(3,13,resp.file.id_project,resp.file.id_task,null,null,null,null,null,null,resp.file.id);
  }).fail(function (resp) {
    //console.log('FAIL');
    //debugger;
    $('#modalError').modal('toggle');
  }).always(function (resp) {
    //console.log('ALWAYS');
    //debugger;
  });
}
var taskCollapseOpen = true;
var fileCollapseOpen = false;
var filesLoaded = false;
var allTaskFiles;
let savedFileId;