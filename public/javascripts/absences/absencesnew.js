async function main() {
  // active tooltips
  $('[data-toggle="tooltip"]').tooltip();
  
  loggedUser = {name:$('#loggedUser').html().split(" (")[0].split(" ")[0], surname:$('#loggedUser').html().split(" (")[0].split(" ")[1], role:$('#loggedUser').html().split(" (")[1].split(")")[0].toLowerCase()};
  if(loggedUser.role == 'admin' || loggedUser.role.substring(0,5) == 'vodja' || loggedUser.role == 'tajnik' || loggedUser.role == 'komercialist' || loggedUser.role == 'komerciala' || loggedUser.role == 'računovodstvo')
    permissionTag = true;
  $('#selectMonth').val(new Date().toISOString().substring(0,7))
  //select for user groups
  $(".select2-categories").prepend('<option selected></option>').select2({
    data: userGroups,
    placeholder: "Oddelki",
    allowClear: true,
    minimumResultsForSearch: -1,
  });
  $(".select2-status").prepend('<option selected></option>').select2({
    data: allStatus,
    placeholder: "Status",
    allowClear: true,
    minimumResultsForSearch: -1,
  });

  // change userGroup depending on loggedUser role
  if (loggedUser.role.substring(0,5) == 'vodja'){
    if (loggedUser.role.includes('elektri') && !userId){
      //debugger;
      $('#absenceCategoryFilter').val('električarji').trigger("change");
      absencesUserGroups = 'električarji';
    }
    else if (loggedUser.role.includes('program') && !userId){
      //debugger;
      $('#absenceCategoryFilter').val('programerji').trigger("change");
      absencesUserGroups = 'programerji';
    }
    else if (loggedUser.role.includes('strojnik') && !userId){
      //debugger;
      $('#absenceCategoryFilter').val('strojniki').trigger("change");
      absencesUserGroups = 'strojniki';
    }
    else if (loggedUser.role.includes('konstrukt') && !userId){
      //debugger;
      $('#absenceCategoryFilter').val('konstrukterji').trigger("change");
      absencesUserGroups = 'konstrukterji';
    }
  }
  // get all roles with their supervisors
  await fetchRoles();
  // get all users with role info
  await fetchAbsencesInfo();

  isSupervisorRole = allRoles.find(r => r.is_supervisor == true);
  isSupervisorUser = allUsers.find(u => u.is_supervisor == true);
  
  // prepare page for logged user and then get absences
  // filter user list based on logged user role
  let tmpUsers = allUsers;
  if (absencesUserGroups == 'električarji'){
    tmpUsers = allUsers.filter(u => u.role.toLowerCase().includes('elektri'));
  }
  else if (absencesUserGroups == 'strojniki'){
    tmpUsers = allUsers.filter(u => u.role.toLowerCase().includes('strojni') || u.role.toLowerCase().includes('cnc operater') || u.role.toLowerCase().includes('varilec'));
  }
  else if (absencesUserGroups == 'konstrukterji'){
    tmpUsers = allUsers.filter(u => u.role.toLowerCase().includes('konstrukter'));
  }
  else if (absencesUserGroups == 'programerji'){
    tmpUsers = allUsers.filter(u => u.role.toLowerCase().includes('programer'));
  }
  else if (absencesUserGroups == 'vodje'){
    tmpUsers = allUsers.filter(u => u.role.toLowerCase().includes('vodja'));
  }
  else if (absencesUserGroups == 'serviserji'){
    tmpUsers = allUsers.filter(u => u.role.toLowerCase().includes('serviser'));
  }
  else if (absencesUserGroups == 'študenti'){
    tmpUsers = allUsers.filter(u => u.role.toLowerCase().includes('študent'));
  }
  else if (absencesUserGroups == 'ostali'){
    tmpUsers = allUsers.filter(u => !u.role.toLowerCase().includes('strojni') && !u.role.toLowerCase().includes('cnc operater') && !u.role.toLowerCase().includes('varilec') && !u.role.toLowerCase().includes('elektri') && !u.role.toLowerCase().includes('programer') && !u.role.toLowerCase().includes('info') && !u.role.toLowerCase().includes('vodja') && !u.role.toLowerCase().includes('konstruk') && !u.role.toLowerCase().includes('študent') && !u.role.toLowerCase().includes('serviser'));
  }

  if(isSupervisorRole || isSupervisorUser){
    // filter tmpUsers to contain only users that logged user is supervisor for, for example, if logged user is supervisor for role 'vodja projektov' he can see only users that are project leaders
    let tmpU = [];
    let tmp = allRoles.filter(r => r.is_supervisor == true);
    let allFilteredUsers = [];
    tmp.forEach(r => {
      let tmpUser = allUsers.filter(u => u.role == r.role);
      allFilteredUsers = allFilteredUsers.concat(tmpUser);
    });
    tmpU = allFilteredUsers;
    // add users that logged user is supervisor for to tmpUsers, for example, those users have is_supervisor = true
    let tmpSupervisedUsers = allUsers.filter(u => u.is_supervisor == true);
    // add these users to tmpUsers but only if they are not already in tmpUsers
    tmpSupervisedUsers.forEach(u => {
      if (!tmpU.find(tu => tu.id == u.id))
        tmpU.push(u);
    });
    // tmpUsers can have only users that are also in tmpU unless logged user is admin or tajnik or komercialist or komerciala or računovodstvo
    if (loggedUser.role != 'admin' && loggedUser.role != 'tajnik' && loggedUser.role != 'komercialist' || loggedUser.role == 'komerciala' || loggedUser.role == 'računovodstvo'){
      tmpUsers = tmpU;
    }
    // add logged user to tmpUsers if he is not already there
    let tmpLoggedUser = allUsers.find(u => u.name + ' ' + u.surname == loggedUser.name + ' ' + loggedUser.surname);
    if (tmpLoggedUser && !tmpUsers.find(u => u.id == tmpLoggedUser.id)){
      tmpUsers.push(tmpLoggedUser);
    }
  }
  else if(loggedUser.role != 'admin' && loggedUser.role != 'tajnik' && loggedUser.role != 'komercialist' || loggedUser.role == 'komerciala' || loggedUser.role == 'računovodstvo'){
    // normal user -> cannot accept or reject and can only delete or edit his own absences
    // these users will get list of his absences only after correction in get absences call
    tmpUsers = allUsers.filter(u => u.id == loggedUser.id);
    $('#absenceUsersFilter').parent().remove();
    $('#absenceCategoryFilter').parent().remove();

    // make add form for normal user
    $('#addAbsenceUser').parent().parent().remove();
    $('#addAbsenceReason').parent().parent().parent().removeClass('col-md-6').addClass('col-md-12');
    $('#addAbsenceStatus').parent().parent().remove();
  }

  $(".select2-users").prepend('<option selected></option>').select2({
    data: tmpUsers,
    placeholder: "Zaposleni",
    allowClear: true,
  });
  // if (loggedUser.role == 'vodja projektov'){
  //   let tmp = allUsers.find(u => u.name + ' ' + u.surname == loggedUser.name + ' ' + loggedUser.surname);
  //   if (tmp){
  //     $('#absenceUsersFilter').val(tmp.id).trigger('change');
  //     absencesUserId = tmp.id;
  //   }
  // }
  if (!allAbsences && userId){
    $('#absenceUsersFilter').val(userId).trigger('change');
    //absencesUserId = userId;
  }
  else if (loggedUser.role == 'vodja projektov'){
    let tmp = allUsers.find(u => u.name + ' ' + u.surname == loggedUser.name + ' ' + loggedUser.surname);
    if (tmp){
      $('#absenceUsersFilter').val(tmp.id).trigger('change');
      absencesUserId = tmp.id;
    }
  }
  $('#absenceUsersFilter').on('change', function (event) {
    // debugger;
    absencesUserId = this.value;
    if (absencesUserId){
      let tmp = allUsers.filter(u => u.id == absencesUserId);
      allFilteredUsers = tmp;
    }
    else{
      // filter user list based on logged user role
      let tmpUsers = allUsers;
      if (absencesUserGroups == 'električarji'){
        tmpUsers = allUsers.filter(u => u.role.toLowerCase().includes('elektri'));
      }
      else if (absencesUserGroups == 'strojniki'){
        tmpUsers = allUsers.filter(u => u.role.includes('strojni') || u.role.includes('cnc operater') || u.role.includes('varilec'));
      }
      else if (absencesUserGroups == 'konstrukterji'){
        tmpUsers = allUsers.filter(u => u.role.includes('konstrukter'));
      }
      else if (absencesUserGroups == 'programerji'){
        tmpUsers = allUsers.filter(u => u.role.includes('programer'));
      }
      else if (absencesUserGroups == 'vodje'){
        tmpUsers = allUsers.filter(u => u.role.includes('vodja'));
      }
      else if (absencesUserGroups == 'serviserji'){
        tmpUsers = allUsers.filter(u => u.role.includes('serviser'));
      }
      else if (absencesUserGroups == 'študenti'){
        tmpUsers = allUsers.filter(u => u.role.includes('študent'));
      }
      else if (absencesUserGroups == 'ostali'){
        tmpUsers = allUsers.filter(u => !u.role.includes('strojni') && !u.role.includes('cnc operater') && !u.role.includes('varilec') && !u.role.includes('elektri') && !u.role.includes('programer') && !u.role.includes('info') && !u.role.includes('vodja') && !u.role.includes('konstruk') && !u.role.includes('študent') && !u.role.includes('serviser'));
      }
      allFilteredUsers = tmpUsers;
    }
    $('#absencesVis').empty();
    getAbsences();
  });
  $(".select2-addabsence-users").prepend('<option selected></option>').select2({
    data: tmpUsers,
    placeholder: "Zaposleni",
    allowClear: true,
    dropdownParent: $('#modalAddAbsence'),
  });
  $(".select2-editabsence-users").prepend('<option selected></option>').select2({
    data: tmpUsers,
    placeholder: "Zaposleni",
    allowClear: true,
    dropdownParent: $('#modalEditAbsence'),
  });
  $(".select2-reasons").select2({
    data: allReasons,
    placeholder: "Razlog",
    minimumResultsForSearch: -1,
  });
  allFilteredUsers = tmpUsers;
  // get absences from database and draw them
  getAbsences();


  // call event functions
  $('#addAbsenceBtn').on('click', function () {
    $('#modalAddAbsence').modal('toggle');
  });
  $('#activityBtnGroup').on('click', 'input', function (event) {
    // debugger;
    absencesActivity = this.value;
    getAbsences();
  });
  // on click event for getting absences history or changes
  $('#absenceList').on('click', '.history-popover', async function (event) {
    let tmpId = this.id.substring(14);
    try {
      const tmpC = await fetchAbsenceChanges(tmpId);
      let newChangeContent = '';
      tmpC.forEach(c => {
        newChangeContent += new Date(c.date).toLocaleString('sl') + ', ' + c.name + ' ' + c.surname + ', ' + c.status + ' ' + c.type + '<br />';
      });
      // show popover with changes
      $('#absenceHistory' + tmpId).attr('data-content', newChangeContent);
      $('#absenceHistory' + tmpId).popover({
        html: true,
        content: newChangeContent
      }).popover('show');
    } catch (error) {
      console.error('Error fetching changes:', error);
    }
  });
  // if(!allAbsences && userId){
  //   $('#absenceUsersFilter').val(userId).trigger('change');
  //   absencesUserId = userId;
  // }
  // $('#absenceUsersFilter').on('change', function (event) {
  //   // debugger;
  //   absencesUserId = this.value;
  //   getAbsences();
  // });
  $('#absenceCategoryFilter').on('change', function (event) {
    // debugger;
    absencesUserGroups = this.value;
    // getAbsences();
    // filter user list based on user group filter
    let tmpUsers = allUsers;
    if (absencesUserGroups == 'električarji'){
      tmpUsers = allUsers.filter(u => u.role.toLowerCase().includes('elektri'));
    }
    else if (absencesUserGroups == 'strojniki'){
      tmpUsers = allUsers.filter(u => u.role.toLowerCase().includes('strojni') || u.role.toLowerCase().includes('cnc operater') || u.role.toLowerCase().includes('varilec'));
    }
    else if (absencesUserGroups == 'konstrukterji'){
      tmpUsers = allUsers.filter(u => u.role.toLowerCase().includes('konstrukter'));
    }
    else if (absencesUserGroups == 'programerji'){
      tmpUsers = allUsers.filter(u => u.role.toLowerCase().includes('programer'));
    }
    else if (absencesUserGroups == 'vodje'){
      tmpUsers = allUsers.filter(u => u.role.toLowerCase().includes('vodja'));
    }
    else if (absencesUserGroups == 'serviserji'){
      tmpUsers = allUsers.filter(u => u.role.toLowerCase().includes('serviser'));
    }
    else if (absencesUserGroups == 'študenti'){
      tmpUsers = allUsers.filter(u => u.role.toLowerCase().includes('študent'));
    }
    else if (absencesUserGroups == 'ostali'){
      tmpUsers = allUsers.filter(u => !u.role.toLowerCase().includes('strojni') && !u.role.toLowerCase().includes('cnc operater') && !u.role.toLowerCase().includes('varilec') && !u.role.toLowerCase().includes('elektri') && !u.role.toLowerCase().includes('programer') && !u.role.toLowerCase().includes('info') && !u.role.toLowerCase().includes('vodja') && !u.role.toLowerCase().includes('konstruk') && !u.role.toLowerCase().includes('študent') && !u.role.toLowerCase().includes('serviser'));
    }
    // filter user list based on logged user role and if he is supervisor for some role or user
    if(isSupervisorRole || isSupervisorUser){
      // filter tmpUsers to contain only users that logged user is supervisor for, for example, if logged user is supervisor for role 'vodja projektov' he can see only users that are project leaders
      let tmpU = [];
      let tmp = allRoles.filter(r => r.is_supervisor == true);
      let allFilteredUsers = [];
      tmp.forEach(r => {
        let tmpUser = allUsers.filter(u => u.role == r.role);
        allFilteredUsers = allFilteredUsers.concat(tmpUser);
      });
      // tmpU is list of users that logged user is supervisor for based on roles
      tmpU = allFilteredUsers;
      // add users that logged user is supervisor for to tmpUsers, for example, those users have is_supervisor = true
      let tmpSupervisedUsers = allUsers.filter(u => u.is_supervisor == true);
      // add these users to tmpUsers but only if they are not already in tmpU
      tmpSupervisedUsers.forEach(u => {
        if (!tmpU.find(tu => tu.id == u.id))
          tmpU.push(u);
      });
      // tmpUsers can have only users that are also in tmpU unless logged user is admin or tajnik or komercialist or komerciala or računovodstvo
      if (loggedUser.role != 'admin' && loggedUser.role != 'tajnik' && loggedUser.role != 'komercialist' || loggedUser.role == 'komerciala' || loggedUser.role == 'računovodstvo'){
        // intersection of tmpUsers and tmpU
        tmpUsers = tmpUsers.filter(u => tmpU.find(tu => tu.id == u.id));
      }
      // add logged user to tmpUsers if he is not already there
      let tmpLoggedUser = allUsers.find(u => u.name + ' ' + u.surname == loggedUser.name + ' ' + loggedUser.surname);
      if (tmpLoggedUser && !tmpUsers.find(u => u.id == tmpLoggedUser.id)){
        tmpUsers.push(tmpLoggedUser);
      }
    }
    
    $('.select2-users').empty();
    $(".select2-users").prepend('<option selected></option>').select2({
      data: tmpUsers,
      placeholder: "Zaposleni",
      allowClear: true,
    });
    allFilteredUsers = tmpUsers;
    $('#absencesVis').empty();
    getAbsences();
  });
  $('#absenceStatusFilter').on('change', function (event) {
    // debugger;
    absencesStatus = this.value;
    getAbsences();
  });
  $('#radioLatest').on('change', function (event) {
    // debugger;
    absencesLimit = $('#radioLatestValue').val();
    absencesDate = '';
    getAbsences();
  });
  $('#radioLatestValue').on('change', function (event) {
    if ($('#radioLatest').prop('checked')){
      // debugger;
      absencesLimit = $('#radioLatestValue').val();
      absencesDate = '';
      getAbsences();
    }
  });
  $('#radioMonths').on('change', function (event) {
    // debugger;
    absencesDate = $('#selectMonth').val();
    absencesLimit = '-1';
    getAbsences();
  });
  $('#selectMonth').on('change', function (event) {
    if ($('#radioMonths').prop('checked')){
      // debugger;
      absencesDate = $('#selectMonth').val();
      absencesLimit = '-1';
      getAbsences();
    }
  });
  // call event for buttons inside list of absences
  $('#absenceList').on('click', 'button', function (params) {
    let tmpId = this.parentElement.id.substring(14);
    //debugger;
    if (this.className.includes('danger')) {
      // delete call
      // check if absence is active or not (deleted or not)
      // let absenceActivity = $('#absence' + tmpId).hasClass('bg-secondary'); // will reverse activity because im checking if absence is deleted
      //deleteAbsence(tmpId, absenceActivity);
      deleteAbsenceModal(tmpId);
      debugger;
    }
    else if (this.className.includes('primary')) {
      // edit call
      let tmpAbsence = allAbsences.find(a => a.id == tmpId);
      $('#editAbsenceName').val(tmpAbsence.name);
      if (loggedUser.role == 'admin' || loggedUser.role == 'tajnik' || loggedUser.role == 'komercialist' || loggedUser.role == 'komerciala' || loggedUser.role == 'računovodstvo' || (loggedUser.role.substring(0,5) == 'vodja' && loggedUser.role != 'vodja projektov'))
        $('#editAbsenceUser').val(tmpAbsence.id_user).trigger('change');
      $('#editAbsenceReason').val(tmpAbsence.id_reason).trigger('change');
      $('#editAbsenceStart').val(moment(tmpAbsence.start).format("YYYY-MM-DD"));
      $('#editAbsenceFinish').val(moment(tmpAbsence.finish).format("YYYY-MM-DD"));
      $('#editAbsencePendingNote').val(tmpAbsence.pending_note);
      savedAbsenceId = tmpId;
      $('#modalEditAbsence').modal('toggle');
      debugger;
    }
    else if (this.className.includes('warning')) {
      // because of adding note to absence, user has to confirm his action and possibility to add note
      tmpAbsenceId = tmpId;
      let tmpAbsenceResolveNote = allAbsences.find(a => a.id == tmpId).resolve_note;
      $('#rejectAbsenceNote').val(tmpAbsenceResolveNote);
      $('#modalRejectAbsence').modal('toggle');

    }
    else if (this.className.includes('success')) {
      // approve call
      debugger;
      let data = {absenceId:tmpId, absenceStatus:true};
      $.ajax({
        type: 'PUT',
        url: '/apiv2/absences/'+tmpId,
        contentType: 'application/json',
        data: JSON.stringify(data), // access in body
      }).done(function (resp) {
        //console.log('SUCCESS');
        debugger;
        if (resp.absence){
          debugger;
          $('#absenceStatus' + resp.absence.id).removeClass('invisible');
          $('#absenceStatus' + resp.absence.id).removeClass('badge-success badge-warning').addClass('badge-success');
          $('#absenceStatus' + resp.absence.id).html('POTRJENO');
          $('#absenceControl' + resp.absence.id).find('.btn-warning').remove();
          $('#absenceControl' + resp.absence.id).find('.btn-success').remove();
          let rejectBtn = '<button class="btn btn-warning ml-2"><i class="fas fa-times fa-lg"></i></button>';
          $('#absenceControl' + resp.absence.id).append(rejectBtn);
          // save seccussful change to database
          addChangeSystem(8,24,null,null,null,null,null,null,null,resp.absence.id);
        }
        else{
          if (resp.msg){
            $('#msgImg').attr('src','/warning_sign.png');
            $('#msg').html(resp.msg);
            $('#modalMsg').modal('toggle');
          }
          else{ $('#modalError').modal('toggle'); }
        }
      }).fail(function (resp) {
        //console.log('FAIL');
        //debugger;
        $('#modalError').modal('toggle');
      }).always(function (resp) {
        //console.log('ALWAYS');
        //debugger;
      });
    }
  })
  $('#btnDeleteAbsence').on('click', deleteAbsence);
  $('#btnCnfRejectAbsence').on('click', rejectAbsence);
  //$('#editAbsenceBtn').on('click', editAbsence);

  // // get absences from database and draw them
  // getAbsences();

  //CONF ADD ABSENCE - in form for req control
  $('#addAbsenceform').submit(function (e) {
    e.preventDefault();
    $form = $(this);
    //debugger;
    let absenceName = addAbsenceName.value;
    let absenceReason = addAbsenceReason.value;
    let absenceUser = (isSupervisorRole || isSupervisorUser) ? addAbsenceUser.value : null;
    let absenceStatus = (isSupervisorRole || isSupervisorUser) ? addAbsenceStatus.value : null;
    let absenceStart = addAbsenceStart.value;
    let absenceFinish = addAbsenceFinish.value;
    let absencePendingNote = addAbsencePendingNote.value;
    let absenceUserRole;
    
    if(absenceStatus && absenceStatus == '-1')
      absenceStatus = null;
    //let date = compSubDateNew.value + ' ' + new Date().toISOString().split('T')[1].substring(0,5);
    //POST info to route for adding new absence
    debugger;
    let data = {absenceName, absenceReason, absenceUser, absenceStart, absenceFinish, absenceUserRole, absenceStatus, absencePendingNote};
    $.ajax({
      type: 'POST',
      url: '/apiv2/absences',
      contentType: 'application/json',
      data: JSON.stringify(data), // access in body
    }).done(function (resp) {
      if(resp.absence){
        debugger;

        let absence = resp.absence;
        //draw new absence
        let userName = absenceUser ? $('#addAbsenceUser :selected').text() : loggedUser.name + ' ' + loggedUser.surname;
        let absenceName = absence.name ? (' - ' + absence.name) : '';
        let role = absenceUserRole ? absenceUserRole : loggedUser.role;
        let pendingNote = absence.pending_note ? absence.pending_note : '';
        let resolveNote = absence.resolve_note ? absence.resolve_note : '';
        let statusColor = absence.approved ? 'success' :  'warning';
        let statusLabel = absence.approved ? 'POTRJENO' :  'ZAVRNJENO';
        let statusVisible = (absence.approved == null) ? ' invisible' : '';
        //statusLabel = (absence.approved == null) ? '' : statusLabel;
        let date = new Date(absence.start).toLocaleDateString('sl') + ' - ' + new Date(absence.finish).toLocaleDateString('sl');
        let reason = $('#addAbsenceReason :selected').text();
        let activityClass = absence.active ? '' : ' bg-secondary';

        let deleteBtnIcon = absence.active ? '' : '-restore';
        
        let historyBtn = '<button class="btn btn-outline-dark ml-2 ml-auto"><i class="fas fa-history fa-lg"></i></button>';
        let historyLinkBtn = '<a class="mypopover btn-on-stretched mr-2 mt-2 ml-auto history-popover" data-toggle="popover" data-trigger="focus" tabindex="0" data-content="ni zabeleženih sprememb" id="absenceHistory' + absence.id + '" data-original-title="" title=""><i class="fas fa-history fa-lg text-dark"></i></a>';
        let deleteBtn = `<button class="btn btn-danger ml-2"><i class="fas fa-trash` + deleteBtnIcon + ` fa-lg"></i></button>`;
        let editBtn = '<button class="btn btn-primary ml-2"><i class="fas fa-pen fa-lg"></i></button>';
        let rejectBtn = '<button class="btn btn-warning ml-2"><i class="fas fa-times fa-lg"></i></button>';
        let acceptBtn = '<button class="btn btn-success ml-2"><i class="fas fa-check fa-lg"></i></button>';
        let StatusInBtnControl = '';
        let firstElementRow = `<div class="row">
          <div class="col-md-3">
            <label id="absenceUser` + absence.id + `">` + userName + `</label>
          </div>
          <div class="col-md-3">
            <label id="absenceRole` + absence.id + `">` + role + `</label>
          </div>
          <div class="col-md-3">
            <label id="absencePendingNote` + absence.id + `">` + pendingNote + `</label>
          </div>
          <div class="col-md-3">
            <div class="d-flex">
              <h4 class="ml-auto"><span class="badge badge-pill badge-` + statusColor + statusVisible + `" id="absenceStatus` + absence.id + `">` + statusLabel + `</span></h4>
            </div>
          </div>
        </div>`;

        // activity && approved based controll buttons
        if (!absence.active){
          editBtn = '';
          rejectBtn = '';
          acceptBtn = '';
        }
        else{
          if (absence.approved == true){
            acceptBtn = '';
          }
          else if (absence.approved == false){
            rejectBtn = '';
          }
          else{

          }
        }
        tmpUser = allUsers.find(u => u.id == absence.id_user);
        tmpRole = allRoles.find(r => r.role == tmpUser.role);
        if (loggedUser.role == 'admin' || tmpUser.is_supervisor || tmpRole.is_supervisor){
          // can approve or reject
          // only admin can still delete or edit once its approved or rejected
          if ((absence.approved == true || absence.approved == false) && loggedUser.role != 'admin'){
            deleteBtn = '';
            editBtn = '';
            acceptBtn = '';
            rejectBtn = '';
          }
        }
        else{
          // firstElementRow = ``;
          acceptBtn = '';
          rejectBtn = '';
          // can only delete or edit his own absences
          if (loggedUser.name + ' ' + loggedUser.surname != userName){
            deleteBtn = '';
            editBtn = '';
          }
          if (absence.approved == true || absence.approved == false){
            // show only status and remove all buttons
            // StatusInBtnControl = `<h4 class="ml-auto"><span class="badge badge-pill badge-` + statusColor + `" id="absenceStatus` + absence.id + `">` + statusLabel + `</span></h4>`;
            deleteBtn = '';
            editBtn = '';
          }
        }


        let element = `<div class="list-group-item` + activityClass + `" id="absence` + absence.id + `">
          ` + firstElementRow + `
          <div class="row">
            <div class="col-md-3">
              <label id="absenceDate` + absence.id + `">` + date + `</label>
            </div>
            <div class="col-md-3">
              <label id="absenceReason` + absence.id + `">` + reason + absenceName + `</label>
            </div>
            <div class="col-md-3">
              <label id="absenceResolveNote` + absence.id + `">` + resolveNote + `</label>
            </div>
            <div class="col-md-3">
              <div class="d-flex" id="absenceControl` + absence.id + `">
                ` + historyLinkBtn + `
                ` + deleteBtn + `
                ` + editBtn + `
                ` + rejectBtn + `
                ` + acceptBtn + `
                ` + StatusInBtnControl + `
              </div>
            </div>
          </div>
        </div>`;

        $('#absenceList').prepend(element);
        allAbsences.push(absence);
        $('#modalAddAbsence').modal('toggle');
        
        // all good and well but now I dont know who created this absence so I have to make first change where it says who created it
        // even better would be to move all this to server side and return all this info in response
        addChangeSystem(1,24,null,null,null,null,null,null,null,absence.id);

        // add new change
        if (absence.approved == true)
          addChangeSystem(8,24,null,null,null,null,null,null,null,absence.id);
        else if (absence.approved == false)
          addChangeSystem(15,24,null,null,null,null,null,null,null,absence.id);
        else
          addChangeSystem(7,24,null,null,null,null,null,null,null,absence.id);
      
        //empty or put to default input values
        $('#addAbsenceName').val('');
        $('#addAbsenceUser').val('').trigger('change');
        $('#addAbsenceReason').val(1).trigger('change');
        $('#addAbsenceStart').val('');
        $('#addAbsenceFinish').val('');
        $('#addAbsenceStatus').val(1);
      }
      else{
        debugger;
        if (resp.msg){
          $('#modalAddAbsence').modal('toggle');
          $('#msgImg').attr('src','/warning_sign.png');
          $('#msg').html(resp.msg);
          $('#modalMsg').modal('toggle');
        }
        else{ $('#modalError').modal('toggle'); }
      }
    }).fail(function (resp) { //console.log('FAIL'); //debugger;
      $('#modalError').modal('toggle');
    }).always(function (resp) { //console.log('ALWAYS'); //debugger;
    });
  })

  //CONF EDIT ABSENCE - in form for req control
  $('#editAbsenceform').submit(function (e) {
    e.preventDefault();
    $form = $(this);
    
    debugger;
    let absenceId = savedAbsenceId;
    let absenceName = editAbsenceName.value;
    let absenceReason = editAbsenceReason.value;
    let absenceStart = editAbsenceStart.value;
    let absenceFinish = editAbsenceFinish.value;
    let absencePendingNote = editAbsencePendingNote.value;
    
    //PUT info to route for editing absence
    debugger;
    let data = {absenceId, absenceName, absenceReason, absenceStart, absenceFinish, absencePendingNote};
    $.ajax({
      type: 'PUT',
      url: '/apiv2/absences/'+absenceId,
      contentType: 'application/json',
      data: JSON.stringify(data), // access in body
    }).done(function (resp) {
      if(resp.absence){
        debugger;
        // update variable
        let tmpReason = allReasons.find(r => r.id == resp.absence.id_reason);
        let tmpAbsence = allAbsences.find(a => a.id == absenceId);
        tmpAbsence.name = resp.absence.name;
        tmpAbsence.reason = tmpReason.reason;
        tmpAbsence.id_reason = resp.absence.id_reason;
        tmpAbsence.start = resp.absence.start;
        tmpAbsence.finish = resp.absence.finish;
        tmpAbsence.pending_note = resp.absence.pending_note;
        tmpAbsence.resolve_note = resp.absence.resolve_note;
        // update element on page
        let date = new Date(tmpAbsence.start).toLocaleDateString('sl') + ' - ' + new Date(tmpAbsence.finish).toLocaleDateString('sl');
        let reasonName = tmpAbsence.name ? tmpAbsence.reason + ' - ' + tmpAbsence.name : tmpAbsence.reason;
        $('#absenceDate' + absenceId).html(date);
        $('#absenceReason' + absenceId).html(reasonName);
        $('#absencePendingNote' + absenceId).html(tmpAbsence.pending_note);
        $('#absenceResolveNote' + absenceId).html(tmpAbsence.resolve_note);

        // emty vaules and close modal
        $('#editAbsenceName').val('');
        if (loggedUser.role == 'admin' || loggedUser.role == 'tajnik' || loggedUser.role == 'komercialist' || loggedUser.role == 'komerciala' || loggedUser.role == 'računovodstvo' || (loggedUser.role.substring(0,5) == 'vodja' && loggedUser.role != 'vodja projektov')){
          $('#editAbsenceUser').val('').trigger('change');
        }
        $('#editAbsenceReason').val('').trigger('change');
        $('#editAbsenceStart').val('');
        $('#editAbsenceFinish').val('');

        // save seccussful change to database
        $('#modalEditAbsence').modal('toggle');
        addChangeSystem(2,24,null,null,null,null,null,null,null,absenceId);
      }
      else{
        debugger;
        if (resp.msg){
          $('#modalEditAbsence').modal('toggle');
          $('#msgImg').attr('src','/warning_sign.png');
          $('#msg').html(resp.msg);
          $('#modalMsg').modal('toggle');
        }
        else{
          $('#modalEditAbsence').modal('toggle');
          $('#modalError').modal('toggle');
        }
      }
    }).fail(function (resp) { //console.log('FAIL'); //debugger;
      $('#modalError').modal('toggle');
    }).always(function (resp) { //console.log('ALWAYS'); //debugger;
    });
  })

  // $('#addAbsenceUser') on change event, when user is selected, check if logged user is supervisor for that user or users role
  $('#addAbsenceUser').on('change', function (event) {
    // if this.value is null then break
    if (!this.value)
      return;
    // debugger;
    let tmpUser = allUsers.find(u => u.id == this.value);
    let tmpRole = allRoles.find(r => r.id == tmpUser.role_id);
    let canSupervise = (tmpUser.is_supervisor || tmpRole.is_supervisor) ? true : false;
    
    // console.log(canSupervise);
    // if false then set status to -1 (no status) and disable status select else enable status select unless logged user is admin
    if (loggedUser.role == 'admin' || canSupervise)
      $('#addAbsenceStatus').val(1).prop('disabled', false);
    else
      $('#addAbsenceStatus').val(-1).prop('disabled', true);
  });
  // open edit role supervisors modal and fill it with data
  $('#editSupervisorBtn').on('click', function (params) {
    // fill modal with data
    // first fill all users for selecting supervisor
    $(".select2-supervisors").select2({
      data: tmpUsers,
      placeholder: "Zaposleni",
      allowClear: true,
      multiple: true,
      // parent: $('#modalEditSupervisor'),
    });
    // fill all roles for selecting role except admin or info role
    let tmpRoles = allRoles.filter(r => r.role.toLowerCase() != 'admin' && !r.role.toLowerCase().includes('info'));
    // add text to tmpRoles
    tmpRoles.forEach(r => r.text = r.role);
    $(".select2-supervisors-roles").select2({
      data: tmpRoles,
      placeholder: "Vloga",
      // parent: $('#editSupervisorForm'),
    });
    let supervisorsIds = tmpRoles[0].supervisor_ids ? tmpRoles[0].supervisor_ids.split(', ') : [];
    $('#editSupervisorsUsers').val(supervisorsIds).trigger('change');
    // open modal
    $('#modalEditSupervisor').modal('toggle');
  });
  // on change select2-supervisors-roles, change select2-supervisors with users who are supervisors for that role
  $(".select2-supervisors-roles").on('change', function (params) {
    // debugger;
    let tmpRole = allRoles.find(r => r.id == this.value);
    let supervisorsIds = tmpRole.supervisor_ids ? tmpRole.supervisor_ids.split(', ') : [];
    $('#editSupervisorsUsers').val(supervisorsIds).trigger('change');
    // debugger;
  });
  // update supervisors for role when form is submited
  $('#editSupervisorForm').submit(function (e) {
    e.preventDefault();
    $form = $(this);
    // debugger;
    let role = $('#editSupervisorsRoles').val();
    let supervisors = $('#editSupervisorsUsers').val();
    //PUT info to route for editing supervisors for role
    debugger;
    let data = {role, supervisors};
    $.ajax({
      type: 'PUT',
      url: '/apiv2/employees/roles/'+role,
      contentType: 'application/json',
      data: JSON.stringify(data), // access in body
    }).done(function (resp) {
      if(resp.role){
        // debugger;
        // update role
        let tmpRole = allRoles.find(r => r.id == role);
        tmpRole.supervisor_ids = resp.role.supervisor_ids;
        tmpRole.supervisors = resp.role.supervisors;
        // tmpRole = resp.role;
      }
      else{
        debugger;
        if (resp.msg){
          $('#modalEditSupervisor').modal('toggle');
          $('#msgImg').attr('src','/warning_sign.png');
          $('#msg').html(resp.msg);
          $('#modalMsg').modal('toggle');
        }
        else{
          $('#modalEditSupervisor').modal('toggle');
          $('#modalError').modal('toggle');
        }
      }
    })
    .fail(function (resp) { //console.log('FAIL'); //debugger;
      $('#modalError').modal('toggle');
    }).always(function (resp) { //console.log('ALWAYS'); //debugger;
    });
  });
}

// get all absences
function getAbsences(params) {
  // debugger;
  if(!allAbsences && userId)
    absencesUserId = userId;
  // else if(!allAbsences && loggedUser.role == 'vodja projektov'){
  //   let tmp = allUsers.find(u => u.name + ' ' + u.surname == loggedUser.name + ' ' + loggedUser.surname)
  //   if(tmp)
  //     absencesUserId = tmp.id;
  // }
  let data = {absencesActivity, absencesLimit, absencesUserId, absencesUserGroups, absencesStatus, absencesDate};
  $.ajax({
    type: 'GET',
    url: '/apiv2/absences',
    contentType: 'application/x-www-form-urlencoded',
    data: data,
  }).done(function (resp) {
    if (resp.absences){
      //debugger;
      //console.log(resp);
      allAbsences = resp.absences;
      drawAbsences();
      if ($('#btnAbsencesViz').hasClass('active'))
        prepareAbsencesViz();
      else if (makeVizData)
        prepareAbsencesViz();
    }
    else{
      $('#modalError').modal('toggle');
    }
  }).fail(function (resp) {
    //console.log('FAIL');
    //debugger;
    $('#modalError').modal('toggle');
  }).always(function (resp) {
    //console.log('ALWAYS');
    //debugger;
  });
}

// draw all absences
function drawAbsences(params) {
  //debugger;
  // empty absences list and add absences from allAbsences
  $('#absenceList').empty();
  for (const absence of allAbsences) {
    let userName = absence.user_name + ' ' + absence.user_surname;
    let absenceName = absence.name ? (' - ' + absence.name) : '';
    let role = absence.role;
    let pendingNote = absence.pending_note ? absence.pending_note : '';
    let resolveNote = absence.resolve_note ? absence.resolve_note : '';
    let statusColor = absence.approved ? 'success' :  'warning';
    let statusLabel = absence.approved ? 'POTRJENO' :  'ZAVRNJENO';
    let statusVisible = (absence.approved == null) ? ' invisible' : '';
    //statusLabel = (absence.approved == null) ? '' : statusLabel;
    let date = new Date(absence.start).toLocaleDateString('sl') + ' - ' + new Date(absence.finish).toLocaleDateString('sl');
    let reason = absence.reason;
    let activityClass = absence.active ? '' : ' bg-secondary';

    let deleteBtnIcon = absence.active ? '' : '-restore';

    let historyBtn = '<button class="btn btn-outline-dark ml-2 ml-auto"><i class="fas fa-history fa-lg"></i></button>';
    let historyLinkBtn = '<a class="mypopover btn-on-stretched mr-2 mt-2 ml-auto history-popover" data-toggle="popover" data-trigger="focus" tabindex="0" data-content="ni zabeleženih sprememb" id="absenceHistory' + absence.id + '" data-original-title="" title=""><i class="fas fa-history fa-lg text-dark"></i></a>';
    let deleteBtn = `<button class="btn btn-danger ml-2"><i class="fas fa-trash` + deleteBtnIcon + ` fa-lg"></i></button>`;
    let editBtn = '<button class="btn btn-primary ml-2"><i class="fas fa-pen fa-lg"></i></button>';
    let rejectBtn = '<button class="btn btn-warning ml-2"><i class="fas fa-times fa-lg"></i></button>';
    let acceptBtn = '<button class="btn btn-success ml-2"><i class="fas fa-check fa-lg"></i></button>';
    let StatusInBtnControl = '';
    let firstElementRow = `<div class="row">
      <div class="col-md-3">
        <label id="absenceUser` + absence.id + `">` + userName + `</label>
      </div>
      <div class="col-md-3">
        <label id="absenceRole` + absence.id + `">` + role + `</label>
      </div>
      <div class="col-md-3">
        <label id="absencePendingNote` + absence.id + `">` + pendingNote + `</label>
      </div>
      <div class="col-md-3">
        <div class="d-flex">
          <h4 class="ml-auto"><span class="badge badge-pill badge-` + statusColor + statusVisible + `" id="absenceStatus` + absence.id + `">` + statusLabel + `</span></h4>
        </div>
      </div>
    </div>`;

    // activity && approved based controll buttons
    if (!absence.active){
      editBtn = '';
      rejectBtn = '';
      acceptBtn = '';
    }
    else{
      if (absence.approved == true){
        acceptBtn = '';
      }
      else if (absence.approved == false){
        rejectBtn = '';
      }
      else{

      }
    }
    // logged user based controll buttons
    // logged user can approve someone absences if users role has is_supervisor = true or if user is supervisor for that user or he is admin
    tmpUser = allUsers.find(u => u.id == absence.id_user);
    tmpRole = allRoles.find(r => r.id == tmpUser.role_id);
    if (loggedUser.role == 'admin' || tmpUser.is_supervisor || tmpRole.is_supervisor){
      // can approve, reject or edit
      // only admin can still delete or edit once its approved or rejected
      if ((absence.approved == true || absence.approved == false) && loggedUser.role != 'admin'){
        deleteBtn = '';
        editBtn = '';
        acceptBtn = '';
        rejectBtn = '';
      }
    }
    else{
      // firstElementRow = ``;
      acceptBtn = '';
      rejectBtn = '';
      // can only delete or edit his own absences
      if (loggedUser.name + ' ' + loggedUser.surname != userName){
        deleteBtn = '';
        editBtn = '';
      }
      if (absence.approved == true || absence.approved == false){
        // show only status and remove all buttons
        // StatusInBtnControl = `<h4 class="ml-auto"><span class="badge badge-pill badge-` + statusColor + `" id="absenceStatus` + absence.id + `">` + statusLabel + `</span></h4>`;
        deleteBtn = '';
        editBtn = '';
      }
    }

    let element = `<div class="list-group-item` + activityClass + `" id="absence` + absence.id + `">
      ` + firstElementRow + `
      <div class="row">
        <div class="col-md-3">
          <label id="absenceDate` + absence.id + `">` + date + `</label>
        </div>
        <div class="col-md-3">
          <label id="absenceReason` + absence.id + `">` + reason + absenceName + `</label>
        </div>
        <div class="col-md-3">
          <label id="absenceResolveNote` + absence.id + `">` + resolveNote + `</label>
        </div>
        <div class="col-md-3">
          <div class="d-flex" id="absenceControl` + absence.id + `">
            ` + historyLinkBtn + `
            ` + deleteBtn + `
            ` + editBtn + `
            ` + rejectBtn + `
            ` + acceptBtn + `
            ` + StatusInBtnControl + `
          </div>
        </div>
      </div>
    </div>`;

    $('#absenceList').append(element);
  }
}
// open delete confirm modal
function deleteAbsenceModal(id) {
  savedAbsenceId = id;
  let tmpAbsence = allAbsences.find(a => a.id == savedAbsenceId);
  if (tmpAbsence.active == true){
    $('#deleteModalMsg').html('Ste prepričani, da želite odstraniti to odsotnost?');
    $('#btnDeleteAbsence').html('Odstrani');
  }
  else{
    $('#deleteModalMsg').html('Ste prepričani, da želite ponovno dodati to odsotnost?');
    $('#btnDeleteAbsence').html('Ponovno dodaj');
  }
  $('#modalDeleteAbsence').modal('toggle');
}
// delete absence (or undelete if admin or tajnik)
function deleteAbsence() {
  let tmpAbsence = allAbsences.find(a => a.id == savedAbsenceId);
  let absenceId = tmpAbsence.id;
  let active = !tmpAbsence.active;

  let data = {absenceId, active};
  $.ajax({
    type: 'DELETE',
    url: '/absences',
    contentType: 'application/json',
    data: JSON.stringify(data), // access in body
  }).done(function (resp) {
    //console.log('SUCCESS');
    debugger;
    if (resp.success){
      if (loggedUser.role == 'admin' || loggedUser.role == 'tajnik' || loggedUser.role == 'komercialist' || loggedUser.role == 'komerciala' || loggedUser.role == 'računovodstvo'){
        //marked new active on absence and update variables //DEPENDS ON WHICH ACTIVITY TABEL IS SHOWN AND LOGGED USER ROLE
        if (absencesActivity == '0'){
          //true and false, all complaints sub are shown, update variables and  class & icon on table
          if(tmpAbsence)
          tmpAbsence.active = active;
          if (active){
            let editBtn = '<button class="btn btn-primary ml-2"><i class="fas fa-pen fa-lg"></i></button>';
            let rejectBtn = '<button class="btn btn-warning ml-2"><i class="fas fa-times fa-lg"></i></button>';
            let acceptBtn = '<button class="btn btn-success ml-2"><i class="fas fa-check fa-lg"></i></button>';
            if (resp.absence.approved == true) acceptBtn = '';
            else if (resp.absence.approved == false) rejectBtn = '';
            $('#absence' + absenceId).removeClass('bg-secondary');
            $('#absence' + absenceId).find('i.fa-trash-restore').removeClass('fa-trash-restore').addClass('fa-trash');
            $('#absenceControl' + absenceId).append(editBtn);
            $('#absenceControl' + absenceId).append(rejectBtn);
            $('#absenceControl' + absenceId).append(acceptBtn);
          }
          else{
            $('#absenceControl' + absenceId).find('.btn-primary').remove();
            $('#absenceControl' + absenceId).find('.btn-warning').remove();
            $('#absenceControl' + absenceId).find('.btn-success').remove();
            $('#absence' + absenceId).addClass('bg-secondary');
            $('#absence' + absenceId).find('i.fa-trash').removeClass('fa-trash').addClass('fa-trash-restore');
          }
        }
        else if (absencesActivity == '1' || absencesActivity == '2'){
          //true, only active absences are shown, same as any other role user // same for false, only non active absences are shown
          allAbsences = allAbsences.filter(a => a.id != absenceId);
          $('#absence' + absenceId).remove();
        }
      }
      else{
        //remove deleted absence from absences list and remove activity from absence variable
        allAbsences = allAbsences.filter(a => a.id != absenceId);
        $('#absence' + absenceId).remove();
      }
      //add new change
      if (active) addChangeSystem(6,24,null,null,null,null,null,null,null,absenceId);
      else addChangeSystem(3,24,null,null,null,null,null,null,null,absenceId);

      $('#modalDeleteAbsence').modal('toggle');
    }
    else{
      $('#modalDeleteAbsence').modal('toggle');
      if (resp.msg){
        $('#modalAddAbsence').modal('toggle');
        $('#msgImg').attr('src','/warning_sign.png');
        $('#msg').html(resp.msg);
        $('#modalMsg').modal('toggle');
      }
      else{ $('#modalError').modal('toggle'); }
    }
  }).fail(function (resp) {
    //console.log('FAIL');
    //debugger;
    $('#modalError').modal('toggle');
  }).always(function (resp) {
    //console.log('ALWAYS');
    //debugger;
  });
}
// reject absence
function rejectAbsence() {
  // reject call
  let tmpId = tmpAbsenceId;
  let absenceResolveNote = $('#absenceResolveNote').val();
  debugger;
  let data = {absenceId:tmpId, absenceStatus:false, absenceResolveNote};
  $.ajax({
    type: 'PUT',
    url: '/apiv2/absences/'+tmpId,
    contentType: 'application/json',
    data: JSON.stringify(data), // access in body
  }).done(function (resp) {
    //console.log('SUCCESS');
    debugger;
    if (resp.absence){
      $('#absenceResolveNote' + resp.absence.id).html(resp.absence.resolve_note);
      debugger;
      $('#absenceStatus' + resp.absence.id).removeClass('invisible');
      $('#absenceStatus' + resp.absence.id).removeClass('badge-success badge-warning').addClass('badge-warning');
      $('#absenceStatus' + resp.absence.id).html('ZAVRNJENO');
      $('#absenceControl' + resp.absence.id).find('.btn-warning').remove();
      $('#absenceControl' + resp.absence.id).find('.btn-success').remove();
      let acceptBtn = '<button class="btn btn-success ml-2"><i class="fas fa-check fa-lg"></i></button>';
      $('#absenceControl' + resp.absence.id).append(acceptBtn);

      // close modal
      $('#modalRejectAbsence').modal('toggle');
      // save seccussful change to database
      addChangeSystem(15,24,null,null,null,null,null,null,null,resp.absence.id);
    }
    else{
      if (resp.msg){
        $('#msgImg').attr('src','/warning_sign.png');
        $('#msg').html(resp.msg);
        $('#modalMsg').modal('toggle');
      }
      else{ $('#modalError').modal('toggle'); }
    }
  }).fail(function (resp) {
    //console.log('FAIL');
    //debugger;
    $('#modalError').modal('toggle');
  }).always(function (resp) {
    //console.log('ALWAYS');
    //debugger;
  });
}
// edit absence
function editAbsence(params) {
  debugger
}

// get all roles with their supervisors
async function fetchRoles() {
  try {
    const response = await fetch('/apiv2/employees/roles', {
      method: 'GET',
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded'
      }
    });

    if (!response.ok) {
      throw new Error('Network response was not ok');
    }

    const data = await response.json();

    if (data.roles) {
      allRoles = data.roles;
    } else {
      $('#modalError').modal('toggle');
    }
  } catch (error) {
    console.error('There was a problem with the fetch operation:', error);
    $('#modalError').modal('toggle');
  }
}
// get all info for working with absences; users with supervisors, reasons
async function fetchAbsencesInfo() {
  let data = {};
  try {
    const response = await fetch('/apiv2/absences/info', {
      method: 'GET',
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded'
      }
    });

    if (!response.ok) {
      throw new Error('Network response was not ok');
    }

    const resp = await response.json();

    if (resp.success) {
      resp.reasons.forEach(r => r.text = r.reason);
      resp.users.forEach(u => u.text = u.name + ' ' + u.surname);
      allReasons = resp.reasons;
      allUsers = resp.users;
    } else {
      $('#modalError').modal('toggle');
    }
  } catch (error) {
    console.error('There was a problem with the fetch operation:', error);
    $('#modalError').modal('toggle');
  }
}
// get all absence changes based on absence id and return them
async function fetchAbsenceChanges(absenceId) {
  // let data = {absenceId};
  try {
    const response = await fetch('/apiv2/absences/' + absenceId + '/changes', {
      method: 'GET',
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded'
      },
    });

    if (!response.ok) {
      throw new Error('Network response was not ok');
    }

    const resp = await response.json();

    if (resp.absenceChanges) {
      return resp.absenceChanges;
    } else {
      return [];
    }
  } catch (error) {
    console.error('There was a problem with the fetch operation:', error);
    return [];
  }
}


// variables
var urlParams = new URLSearchParams(window.location.search);
var userId = urlParams.get('userId');
var permissionTag = false;
let userGroups = ['konstrukterji', 'električarji', 'strojniki', 'serviserji', 'programerji', 'vodje', 'študenti', 'ostali'];
// let userGroups = ['konstrukterji', 'električarji', 'strojniki', 'varilci', 'cnc operaterji', 'serviserji', 'študenti', 'programerji', 'vodje', 'ostali'];
let allAbsences;
let allReasons;
let allUsers;
let allRoles;
let allStatus = [{ id:1, text: 'Potrjene' }, { id:2, text: 'Zavrnjene' }, { id:3, text: 'Brez statusa' }];
let absencesActivity = '1';
let absencesLimit = '20';
let absencesUserId = '';
let absencesUserGroups = '';
let absencesStatus = '';
let absencesDate = '';
let savedAbsenceId;
let allFilteredUsers;
let isSupervisorUser = false;
let isSupervisorRole = false;
let tmpAbsenceId;

// call the main function
document.addEventListener('DOMContentLoaded', main);