$(function(){
  loggedUser = {name:$('#loggedUser').html().split(" (")[0].split(" ")[0], surname:$('#loggedUser').html().split(" (")[0].split(" ")[1], role:$('#loggedUser').html().split(" (")[1].split(")")[0].toLowerCase()};
  if(loggedUser.role == 'admin' || loggedUser.role.substring(0,5) == 'vodja' || loggedUser.role == 'tajnik' || loggedUser.role == 'komercialist' || loggedUser.role == 'komerciala' || loggedUser.role == 'računovodstvo')
    permissionTag = true;
  
  $('#absenceStartNew').on('change', onStartDateNewChange);
  $('#absenceFinishNew').on('change', onFinishtDateNewChange);
  //EDIT ABSENCE FORM
  $('#editAbsenceForm').submit(function(e){
    e.preventDefault();
    $form = $(this);
    var name = absenceNameEdit.value;
    var start = absenceStartedit.value;
    var finish = absenceFinishedit.value;
    var reason = absenceReasonEdit.value;
    var id = activeAbsenceId;
    //var approved = false;

    start = start + " 07:00:00.000000";
    finish = finish + " 15:00:00.000000";
    debugger;
    $.post('/absence/update', {start, finish, reason, name, id}, function(data){
      debugger;
      if(data.success){
        start = new Date(start);
        finish = new Date(finish);
        var sd = start.getDate();
        var sm = start.getMonth()+1;
        var sy = start.getFullYear();
        var fd = finish.getDate();
        var fm = finish.getMonth()+1;
        var fy = finish.getFullYear();
        if(sd < 10) sd = '0'+sd;
        if(sm < 10) sm = '0'+sm;
        if(fd < 10) fd = '0'+fd;
        if(fm < 10) fm = '0'+fm;
        if(!data.reasonId){
          reason = allReasons.find(r=> r.id == absenceReasonEdit.value);
          reason = reason.text;
        }
        else{
          debugger;
          allReasons.push({id: data.reasonId, text: absenceReasonEdit.value})
          $(".select2-absence-reasonsedit").select2({
            data: allReasons,
            tags: permissionTag,
          });
          $(".select2-absence-reasonseNew").select2({
            data: allReasons,
            tags: permissionTag,
          });
          //$('#collapseAddAbsence').collapse("toggle");
          reason = absenceReasonEdit.value;
        }

        if(name)
          $('#absenceName'+id).html(name);
        else
          $('#absenceName'+id).html('(brez imena)');
        $('#absenceDate'+id).html(sd+'.'+sm+'.'+sy+" - "+fd+'.'+fm+'.'+fy);
        $('#absenceReason'+id).html(reason);
        
        debugger;
        //reset values
        //$('#collapseAddAbsence').collapse("toggle");
        toggleCollapse(id);
        $('#absenceNameEdit').val('');
        $('#absenceStartEdit').val('');
        $('#absenceFinishEdit').val('');
        $('#absenceReasonEdit').val(1).trigger('change');
        //ADD SYSTEM CHANGE FOR EDITING WORK ORDER
        addChangeSystem(2,24,null,null,null,null,null,null,null,id);
      }
      else{
        $('#modalError').modal('toggle');
      }
    })
  });
  
    //ADDING NEW ABSENCE FROM FORM
  $('#addAbsenceForm').submit(function(e){
    e.preventDefault();
    $form = $(this);
    var name = absenceNameNew.value;
    var start = absenceStartNew.value;
    var finish = absenceFinishNew.value;
    var reason = absenceReasonNew.value;
    var urlParams = new URLSearchParams(window.location.search);
    var worker = urlParams.get('id');
    var approved = false;
    debugger;

    start = start + " 07:00:00.000000";
    finish = finish + " 15:00:00.000000";

    $.post('/absence/add', {start, finish, worker, reason, name, approved}, function(data){
      debugger;
      if(data.success){
        start = new Date(start);
        finish = new Date(finish);
        var sd = start.getDate();
        var sm = start.getMonth()+1;
        var sy = start.getFullYear();
        var fd = finish.getDate();
        var fm = finish.getMonth()+1;
        var fy = finish.getFullYear();
        if(sd < 10) sd = '0'+sd;
        if(sm < 10) sm = '0'+sm;
        if(fd < 10) fd = '0'+fd;
        if(fm < 10) fm = '0'+fm;
        if(!data.reasonId){
          reason = allReasons.find(r=> r.id == absenceReasonNew.value);
          reason = reason.text;
        }
        else{
          debugger;
          allReasons.push({id: data.reasonId, text: absenceReasonNew.value})
          $(".select2-absence-reasonsNew").select2({
            data: allReasons,
            tags: permissionTag,
          });
          //$('#collapseAddAbsence').collapse("toggle");
          reason = absenceReasonNew.value;
        }
        if(name == '')
          name = '(brez imena)';
        var approveButton = ''
        if(loggedUser.role == 'admin' || loggedUser.role.substring(0,5) == 'vodja' || loggedUser.role == 'tajnik' || loggedUser.role == 'komercialist' || loggedUser.role == 'komerciala' || loggedUser.role == 'računovodstvo')
          approveButton = '<button class="btn btn-success mt-2"><i class="fas fa-check fa-lg"></i></button>';
        var element= `<div class="list-group-item" id="absence`+data.absenceId+`">
          <div class="row">
            <div class="col-md-3 mt-2">
              <label id="absenceName`+data.absenceId+`">`+name+`</label>
            </div>
            <div class="col-md-3 mt-2">
              <label id="absenceProject`+data.absenceId+`">(brez projekta)</label>
            </div>
            <div class="col-md-2 mt-2">
              <label id="absenceDate`+data.absenceId+`">`+sd+'.'+sm+'.'+sy+' - '+fd+'.'+fm+'.'+fy+`</label>
            </div>
            <div class="col-md-2 mt-2">
              <label id="absenceReason`+data.absenceId+`">`+reason+`</label>
            </div>
            <div class="col-md-2" id="absenceControl`+data.absenceId+`">
              `+approveButton+`
              <button class="btn btn-primary ml-2 mt-2"><i class="fas fa-pen fa-lg"></i></button>
              <button class="btn btn-danger ml-2 mt-2"><i class="fas fa-trash fa-lg"></i></button>
            </div>
          </div>
          <div class="collapse multi-collapse task-collapse" id="collapseEditAbsence`+data.absenceId+`"></div>
        </div>`;
        $('#absenceList').prepend(element);

        $('#absenceControl'+data.absenceId).find('.btn-primary').on('click', {id:data.absenceId}, onEventToggleCollapse);
        $('#absenceControl'+data.absenceId).find('.btn-danger').on('click', {id:data.absenceId, newAbsenceActiveStatus:0, absenceApproveStatus:0}, onEventDeleteAbsence);
        $('#absenceControl'+data.absenceId).find('.btn-success').on('click', {id:data.absenceId, newAbsenceApproveStatus:1, absenceActiveStatus:1}, onEventApproveAbsence);
        
        debugger;
        //reset values
        $('#collapseAddAbsence').collapse("toggle");
        $('#absenceNameNew').val('');
        $('#absenceStartNew').val('');
        $('#absenceFinishNew').val('');
        $('#absenceReasonNew').val(1).trigger('change');
        //ADD SYSTEM CHANGE FOR EDITING WORK ORDER
        addChangeSystem(7,24,null,null,null,null,null,null,null,data.absenceId);
      }
      else{
        $('#modalError').modal('toggle');
      }
    })
  })
  getAbsencesAndDraw();
  $('#changePassBtn').on('click', {id:employeeId}, onEventOpenChangePassModal);
  $('#changeRoleBtn').on('click', {id:employeeId}, onEventOpenChangeRoleModal);
  $('#toggleAddAbsenceBtn').on('click', toggleAddAbsenceCollapse);
})
function toggleAddAbsenceCollapse(){
  if(reasonsLoaded)
    $('#collapseAddAbsence').collapse("toggle");
  else{
    $.get( "/absence/reasons", function( data ) {
      allReasons = data.data;
      reasonsLoaded = true;
      $(".select2-absence-reasonsNew").select2({
        data: allReasons,
        tags: permissionTag,
      });
      $('#collapseAddAbsence').collapse("toggle");
    });
  }
}
function onEventToggleCollapse(event) {
  toggleCollapse(event.data.id);
}
function toggleCollapse(id){
  if(activeAbsenceId == 0){
    //no collapse is open, create collapse for id and open collapse
    if(reasonsLoaded)
      makeEditAbsenceCollapse(id);
    else{
      $.get( "/absence/reasons", function( data ) {
        allReasons = data.data;
        reasonsLoaded = true;
        makeEditAbsenceCollapse(id);
      });
    }
    activeAbsenceId = id;
    editCollapseOpen = true;
    $('#collapseEditAbsence'+id).collapse("toggle");

  }
  else{
    //collapse is open, close collaspe and delete its elements
    // if id == activeAbsenceId then just close collapse
    if(id == activeAbsenceId){
      $('#collapseEditAbsence'+id).collapse("toggle");
      if(editCollapseOpen)
        editCollapseOpen = false;
      else
        editCollapseOpen = true;
    }
    else{
      if(editCollapseOpen)
        $('#collapseEditAbsence'+activeAbsenceId).collapse("toggle");
      $('#collapseEditAbsence'+activeAbsenceId).empty();
      makeEditAbsenceCollapse(id);
      activeAbsenceId = id;
      editCollapseOpen = true;
      $('#collapseEditAbsence'+id).collapse("toggle");
    }
  }
}
function makeEditAbsenceCollapse(id){
  var element = `
    <div class="form-group"><label class="mt-3 mx-4 w-95" for="absenceNameEdit">Ime odsotnosti<input class="form-control" id="absenceNameEdit" type="text" name="absenceNameEdit" required=""/></label></div>
    <div class="form-group">
      <label class="mx-4 w-95" for="absenceStartEdit">Za&ccaron;etek odsotnosti
        <input class="form-control" id="absenceStartEdit" type="date" name="absenceStartEdit" required=""/>
        <div class="invalid-feedback">Datuma nista skladna!!!</div>
      </label>
    </div>
    <div class="form-group">
      <label class="mx-4 w-95" for="absenceFinishEdit">Konec odsotnosti
        <input class="form-control" id="absenceFinishEdit" type="date" name="absenceFinishEdit" required=""/>
        <div class="invalid-feedback">Datuma nista skladna!!!</div>
      </label>
    </div>
    <div class="form-group"><label class="mx-4 w-95" for="absenceReasonEdit">Razlog<select class="custom-select mr-sm-2 form-control select2-absence-reasonsEdit w-100" id="absenceReasonEdit" name="absenceReasonEdit"></select></label></div>
    <div class="d-flex justify-content-center"><button class="btn btn-success mb-3">Posodobi</button></div>
  `;
  $('#collapseEditAbsence'+id).append(element);
  $('#collapseEditAbsence'+id).find('.btn-success').on('click', {id}, onEventUpdateAbsence);

  $(".select2-absence-reasonsEdit").select2({
    data: allReasons,
    tags: permissionTag,
  });
  //put correct values into collapse
  var name = $('#absenceName'+id).html();
  var start = $('#absenceDate'+id).html().split(' - ')[0].split('. ');
  start = moment(start[2]+'-'+start[1]+'-'+start[0]).format("YYYY-MM-DD");
  var finish = $('#absenceDate'+id).html().split(' - ')[1].split('. ');
  finish = moment(finish[2]+'-'+finish[1]+'-'+finish[0]).format("YYYY-MM-DD");
  if(name != '(brez imena)')
    $('#absenceNameEdit').val(name);
  $('#absenceStartEdit').val(start);
  $('#absenceFinishEdit').val(finish);
  var reason = allReasons.find(r=> r.text == $('#absenceReason'+id).html());
  $('#absenceReasonEdit').val(reason.id).trigger('change');
}
function onEventUpdateAbsence(event) {
  updateAbsence(event.data.id);
}
function updateAbsence(id){
  
  debugger;

  var name = $('#absenceNameEdit').val();
  var start = $('#absenceStartEdit').val();
  var finish = $('#absenceFinishEdit').val();
  var reason = $('#absenceReasonEdit').val();
  //var approved = false;

  //check if dates are correct (start <= finish)
  if(new Date(start) > new Date(finish)){
    $('#absenceStartEdit').addClass('is-invalid');
    $('#absenceFinishEdit').addClass('is-invalid');
  }
  else{
    start = start + " 07:00:00.000000";
    finish = finish + " 15:00:00.000000";
    debugger;
    $.post('/absence/update', {start, finish, reason, name, id}, function(data){
      debugger;
      if(data.success){
        if(!data.reasonId){
          reason = allReasons.find(r=> r.id == absenceReasonEdit.value);
          reason = reason.text;
        }
        else{
          debugger;
          allReasons.push({id: data.reasonId, text: absenceReasonEdit.value})
          $(".select2-absence-reasonsedit").select2({
            data: allReasons,
            tags: permissionTag,
          });
          $(".select2-absence-reasonseNew").select2({
            data: allReasons,
            tags: permissionTag,
          });
          //$('#collapseAddAbsence').collapse("toggle");
          reason = absenceReasonEdit.value;
        }
  
        if(name)
          $('#absenceName'+id).html(name);
        else
          $('#absenceName'+id).html('(brez imena)');
        $('#absenceDate'+id).html($('#absenceStartEdit').val().split('-')[2]+"."+$('#absenceStartEdit').val().split('-')[1]+"."+$('#absenceStartEdit').val().split('-')[0]+
        " - "+
        $('#absenceFinishEdit').val().split('-')[2]+"."+$('#absenceFinishEdit').val().split('-')[1]+"."+$('#absenceFinishEdit').val().split('-')[0] );
        $('#absenceReason'+id).html(reason);
        
        debugger;
        //reset values
        //$('#collapseAddAbsence').collapse("toggle");
        $('#absenceStartEdit').removeClass('is-invalid');
        $('#absenceFinishEdit').removeClass('is-invalid');
        toggleCollapse(id);
        //$('#absenceNameEdit').val('');
        //$('#absenceStartEdit').val('');
        //$('#absenceFinishEdit').val('');
        //$('#absenceReasonEdit').val(1).trigger('change');
        //ADD SYSTEM CHANGE FOR EDITING WORK ORDER
        addChangeSystem(2,24,null,null,null,null,null,null,null,id);
      }
      else{
        $('#modalError').modal('toggle');
      }
    })
  }
}
function onEventApproveAbsence(event) {
  approveAbsence(event.data.id, event.data.newAbsenceApproveStatus, event.data.absenceActiveStatus);
}
function approveAbsence(id, newAbsenceApproveStatus, absenceActiveStatus){
  debugger;
  $.post('/absence/approve', {id, newAbsenceApproveStatus}, function(data){
    debugger;
    if(data.success){
      //absence is on deleted list, update approve button
      if(absenceActiveStatus == 0){
        $('#absenceControl'+id).empty();
        var approveButton = '<button class="btn btn-success mt-2"><i class="fas fa-check fa-lg"></i></button>';
        if(newAbsenceApproveStatus == 1)
          approveButton = '<button class="btn btn-success mt-2"><i class="fas fa-times fa-lg"></i></button>';
        
        var buttons = approveButton+
          `<button class="btn btn-primary ml-2 mt-2"><i class="fas fa-pen fa-lg"></i></button>
          <button class="btn btn-danger ml-2 mt-2"><i class="fas fa-trash-restore fa-lg"></i></button>`;
        $('#absenceControl'+id).append(buttons);
        $('#absenceControl'+id).find('.btn-danger').on('click', {id, newAbsenceActiveStatus:1, absenceApproveStatus:newAbsenceApproveStatus}, onEventDeleteAbsence);
        if(newAbsenceApproveStatus == 1)
          $('#absenceControl'+id).find('.btn-success').on('click', {id, newAbsenceApproveStatus:0, absenceActiveStatus:0}, onEventApproveAbsence);
        else
          $('#absenceControl'+id).find('.btn-success').on('click', {id, newAbsenceApproveStatus:1, absenceActiveStatus:0}, onEventApproveAbsence);
        //$('#absenceControl'+id).find('.btn-primary').on('click', {id:id}, onEventToggleCollapse);
      }
      //absence is either on unapproved list or approved list, remove and add to other
      else{
        var name = $('#absenceName'+id).html();
        var project = $('#absenceProject'+id).html();
        var date = $('#absenceDate'+id).html();
        var reason = $('#absenceReason'+id).html();
        $('#absence'+id).remove();
        var approveButton;
        var deleteButton = '<button class="btn btn-danger ml-2 mt-2"><i class="fas fa-trash fa-lg"></i></button>';
        if(newAbsenceApproveStatus == 1)
          approveButton = '<button class="btn btn-success mt-2"><i class="fas fa-times fa-lg"></i></button>';
        else
          approveButton = '<button class="btn btn-success mt-2"><i class="fas fa-check fa-lg"></i></button>';
        var element = `<div class="list-group-item" id="absence`+id+`">
          <div class="row">
            <div class="col-md-3 mt-2">
              <label id="absenceName`+id+`">`+name+`</label>
            </div>
            <div class="col-md-3 mt-2">
              <label id="absenceProject`+id+`">`+project+`</label>
            </div>
            <div class="col-md-2 mt-2">
              <label id="absenceDate`+id+`">`+date+`</label>
            </div>
            <div class="col-md-2 mt-2">
              <label id="absenceReason`+id+`">`+reason+`</label>
            </div>
            <div class="col-md-2" id="absenceControl`+id+`">
              `+approveButton+`
              <button class="btn btn-primary ml-2 mt-2"><i class="fas fa-pen fa-lg"></i></button>
              `+deleteButton+`
            </div>
          </div>
          <div class="collapse multi-collapse task-collapse" id="collapseEditAbsence`+id+`"></div>
        </div>`;
        if(newAbsenceApproveStatus == 0){
          $('#absenceList').prepend(element);
          $('#absenceControl'+id).find('.btn-success').on('click', {id, newAbsenceApproveStatus:1, absenceActiveStatus:1}, onEventApproveAbsence);
        }
        else{
          $('#approvedAbsenceList').prepend(element);
          $('#absenceControl'+id).find('.btn-success').on('click', {id, newAbsenceApproveStatus:0, absenceActiveStatus:1}, onEventApproveAbsence);
        }

        $('#absenceControl'+id).find('.btn-danger').on('click', {id, newAbsenceActiveStatus:0, absenceApproveStatus:newAbsenceApproveStatus}, onEventDeleteAbsence);
      }
      $('#absenceControl'+id).find('.btn-primary').on('click', {id:id}, onEventToggleCollapse);
      //ADD SYSTEM CHANGE FOR EDITING WORK ORDER
      if(newAbsenceApproveStatus == 1)
        addChangeSystem(8,24,null,null,null,null,null,null,null,id);
      else
        addChangeSystem(5,24,null,null,null,null,null,null,null,id);
    }
    else{
      $('#modalError').modal('toggle');
    }
  })
}
function onEventDeleteAbsence(event) {
  deleteAbsence(event.data.id, event.data.newAbsenceActiveStatus, event.data.absenceApproveStatus);
}
function deleteAbsence(id, newAbsenceActiveStatus, absenceApproveStatus){
  debugger;
  $.post('/absence/delete', {id, newAbsenceActiveStatus}, function(data){
    debugger;
    if(data.success){
      var name = $('#absenceName'+id).html();
      var project = $('#absenceProject'+id).html();
      var date = $('#absenceDate'+id).html();
      var reason = $('#absenceReason'+id).html();
      $('#absence'+id).remove();
      // var approveButton, deleteButton, editButton = `<button class="btn btn-primary ml-2 mt-2"><i class="fas fa-pen fa-lg"></i></button>`;
      // if(absenceApproveStatus == 1)
      //   approveButton = '<button class="btn btn-success mt-2"><i class="fas fa-times fa-lg"></i></button>';
      // else
      //   approveButton = '<button class="btn btn-success mt-2"><i class="fas fa-check fa-lg"></i></button>';
      // if(newAbsenceActiveStatus == 1)
      //   deleteButton = '<button class="btn btn-danger ml-2 mt-2"><i class="fas fa-trash fa-lg"></i></button>';
      // else
      //   deleteButton = '<button class="btn btn-danger ml-2 mt-2"><i class="fas fa-trash-restore fa-lg"></i></button>';
      
      // if (loggedUser.role != 'admin' || loggedUser.role != 'tajnik'){
      //   approveButton = '';
      //   deleteButton = '';
      //   editButton = '';
      // }
      let approveButton = ``, deleteButton = ``, editButton = `<button class="btn btn-primary ml-2 mt-2"><i class="fas fa-pen fa-lg"></i></button>`;
      let approveIcon = absenceApproveStatus == 1 ? 'times' : 'check', deleteIcon = newAbsenceActiveStatus == 1 ? 'trash' : 'trash-restore';
      if(loggedUser.role == 'admin' || loggedUser.role == 'tajnik' || loggedUser.role == 'komercialist' || loggedUser.role == 'komerciala' || loggedUser.role == 'računovodstvo' || loggedUser.role.substring(0,5) == 'vodja'){
        approveButton = `<button class="btn btn-success mt-2"><i class="fas fa-` + approveIcon + ` fa-lg"></i></button>`;
        deleteButton = `<button class="btn btn-danger ml-2 mt-2"><i class="fas fa-` + deleteIcon + ` fa-lg"></i></button>`;
      }
      else if(absenceApproveStatus == 0)
        approveButton = `<button class="btn btn-success mt-2"><i class="fas fa-` + approveIcon + ` fa-lg"></i></button>`;
      
      var element = `<div class="list-group-item" id="absence`+id+`">
        <div class="row">
          <div class="col-md-3 mt-2">
            <label id="absenceName`+id+`">`+name+`</label>
          </div>
          <div class="col-md-3 mt-2">
            <label id="absenceProject`+id+`">`+project+`</label>
          </div>
          <div class="col-md-2 mt-2">
            <label id="absenceDate`+id+`">`+date+`</label>
          </div>
          <div class="col-md-2 mt-2">
            <label id="absenceReason`+id+`">`+reason+`</label>
          </div>
          <div class="col-md-2" id="absenceControl`+id+`">
            `+approveButton+`
            `+editButton+`
            `+deleteButton+`
          </div>
        </div>
        <div class="collapse multi-collapse task-collapse" id="collapseEditAbsence`+id+`"></div>
      </div>`;
      if(newAbsenceActiveStatus == 0){
        $('#deletedAbsenceList').prepend(element);
        $('#absenceControl'+id).find('.btn-danger').on('click', {id, newAbsenceActiveStatus:1, absenceApproveStatus}, onEventDeleteAbsence);
        if(absenceApproveStatus == 1)
          $('#absenceControl'+id).find('.btn-success').on('click', {id, newAbsenceApproveStatus:0, absenceActiveStatus:newAbsenceActiveStatus}, onEventApproveAbsence);
        else
          $('#absenceControl'+id).find('.btn-success').on('click', {id, newAbsenceApproveStatus:1, absenceActiveStatus:newAbsenceActiveStatus}, onEventApproveAbsence);
      }
      else{
        if(absenceApproveStatus == 1){
          $('#approvedAbsenceList').prepend(element);
          $('#absenceControl'+id).find('.btn-success').on('click', {id, newAbsenceApproveStatus:0, absenceActiveStatus:newAbsenceActiveStatus}, onEventApproveAbsence);
        }
        else{
          $('#absenceList').prepend(element);
          $('#absenceControl'+id).find('.btn-success').on('click', {id, newAbsenceApproveStatus:1, absenceActiveStatus:newAbsenceActiveStatus}, onEventApproveAbsence);
        }
        $('#absenceControl'+id).find('.btn-danger').on('click', {id, newAbsenceActiveStatus:0, absenceApproveStatus}, onEventDeleteAbsence);
      }
      //ADD SYSTEM CHANGE FOR EDITING WORK ORDER
      if(newAbsenceActiveStatus == 0)
        addChangeSystem(3,24,null,null,null,null,null,null,null,id);
      else
        addChangeSystem(6,24,null,null,null,null,null,null,null,id);
        $('#absenceControl'+id).find('.btn-primary').on('click', {id:id}, onEventToggleCollapse);
    }
    else{
      $('#modalError').modal('toggle');
    }
  })
}
function onStartDateNewChange(){
  var start = new Date($('#absenceStartNew').val());
  var finish = new Date($('#absenceFinishNew').val());

  if(start && finish){
    if(start > finish)
      $('#absenceStartNew').val($('#absenceFinishNew').val());
  }
}
function onFinishtDateNewChange(){
  var start = new Date($('#absenceStartNew').val());
  var finish = new Date($('#absenceFinishNew').val());

  if(start && finish){
    if(start > finish)
      $('#absenceFinishNew').val($('#absenceStartNew').val());
  }
}
function getAbsencesAndDraw(params) {
  let data = {userId:employeeId};
  $.ajax({
    type: 'GET',
    url: '/absence/all',
    contentType: 'application/x-www-form-urlencoded',
    data: data, // access in body
  }).done(function (resp) {
    if (resp.success){
      // allCompSub = resp.allCompSub;
      allAbsences = resp.absences;
      drawAbsences(resp.absences);
    }
    else{ $('#modalError').modal('toggle'); }
  }).fail(function (resp) {//console.log('FAIL');//debugger;
    $('#modalError').modal('toggle');
  }).always(function (resp) {//console.log('ALWAYS');//debugger;
  });
}
function drawAbsences(absences) {
  //empty all list first them add absences to correct list based on active and approve status
  //$('#absenceList').empty();
  for (const absence of absences) {
    let absenceName = absence.name ? absence.name : '(brez imena)';
    let projectLabel = absence.project_name ? absence.project_name : '(brez projekta)';
    let startLabel = new Date(absence.start).toLocaleDateString('sl');
    let finishLabel = new Date(absence.finish).toLocaleDateString('sl');
    let reasonLabel = absence.reason;
    let approveButton = '', editButton = '', deleteButton = '';
    let approveIcon = absence.approved ? 'times' : 'check', deleteIcon = absence.active ? 'trash' : 'trash-restore';
    if (loggedUser.role == 'admin' || loggedUser.role == 'tajnik' || loggedUser.role == 'komercialist' || loggedUser.role == 'komerciala' || loggedUser.role == 'računovodstvo' || loggedUser.role.substring(0,5) == 'vodja'){
      approveButton = `<button class="btn btn-success mt-2"><i class="fas fa-`+approveIcon+` fa-lg"></i></button>`;
      editButton = `<button class="btn btn-primary ml-2 mt-2"><i class="fas fa-pen fa-lg"></i></button>`;
      deleteButton = `<button class="btn btn-danger ml-2 mt-2"><i class="fas fa-`+deleteIcon+` fa-lg"></i></button>`;
    }
    else if (absence.active && !absence.approved){
      //approveButton = `<button class="btn btn-success mt-2"><i class="fas fa-check fa-lg"></i></button>`;
      editButton = `<button class="btn btn-primary ml-2 mt-2"><i class="fas fa-pen fa-lg"></i></button>`;
      deleteButton = `<button class="btn btn-danger ml-2 mt-2"><i class="fas fa-`+deleteIcon+` fa-lg"></i></button>`;
    }
    let element = `<div class="list-group-item" id="absence` + absence.id + `">
      <div class="row">
        <div class="col-md-3 mt-2">
          <label id="absenceName` + absence.id + `">` + absenceName + `</label>
        </div>
        <div class="col-md-3 mt-2">
          <label id="absenceProject` + absence.id + `">` + projectLabel + `</label>
        </div>
        <div class="col-md-2 mt-2">
          <label id="absenceDate` + absence.id + `">` + startLabel + ` - ` + finishLabel + `</label>
        </div>
        <div class="col-md-2 mt-2">
          <label id="absenceReason` + absence.id + `">` + reasonLabel + `</label>
        </div>
        <div class="col-md-2" id="absenceControl` + absence.id + `">
          ` + approveButton + `
          ` + editButton + `
          ` + deleteButton + `
        </div>
      </div>
      <div class="collapse multi-collapse task-collapse" id="collapseEditAbsence` + absence.id + `"></div>
    </div>`;
    // <button class="btn btn-success mt-2"><i class="fas fa-`+approveIcon+` fa-lg"></i></button>
    //       <button class="btn btn-primary ml-2 mt-2"><i class="fas fa-pen fa-lg"></i></button>
    //       <button class="btn btn-danger ml-2 mt-2"><i class="fas fa-`+deleteIcon+` fa-lg"></i></button>
    if (absence.active){
      if (absence.approved)
        $('#approvedAbsenceList').append(element);
      else
        $('#absenceList').append(element);
    }
    else
      $('#deletedAbsenceList').append(element);

    //delete button integers & approve button integers
    let activeDeleteButton, activeApproveButton;
    activeDeleteButton = absence.active ? 0 : 1, approvedDeleteButton = absence.approved ? 1 : 0;
    activeApproveButton = absence.active ? 1 : 0, approvedApproveButton = absence.approved ? 0 : 1;
    if (absence.active && absence.approved) {
      activeDeleteButton = 0, approvedDeleteButton = 1, approvedApproveButton = 0, activeApproveButton = 1;
    } else if (absence.active && !absence.approved) {
      activeDeleteButton = 0, approvedDeleteButton = 0, approvedApproveButton = 1, activeApproveButton = 1;
    } else if (!absence.active && absence.approved) {
      activeDeleteButton = 1, approvedDeleteButton = 1, approvedApproveButton = 0, activeApproveButton = 0;
    } else if (!absence.active && !absence.approved) {
      activeDeleteButton = 1, approvedDeleteButton = 0, approvedApproveButton = 1, activeApproveButton = 0;
    }
    $('#absenceControl'+absence.id).find('.btn-danger').on('click', {id:absence.id, newAbsenceActiveStatus: activeDeleteButton, absenceApproveStatus: approvedDeleteButton}, onEventDeleteAbsence);
    $('#absenceControl'+absence.id).find('.btn-success').on('click', {id:absence.id, newAbsenceApproveStatus: approvedApproveButton, absenceActiveStatus: activeApproveButton}, onEventApproveAbsence);
    $('#absenceControl'+absence.id).find('.btn-primary').on('click', {id:absence.id}, onEventToggleCollapse);

  }
}
var urlParams = new URLSearchParams(window.location.search);
var activeAbsenceId = 0;
var editCollapseOpen = false;
var reasonsLoaded = false;
var allReasons;
var loggedUser;
var permissionTag = false;
let employeeId = urlParams.get('id');
let allAbsences;
//$('#absenceStartNew').on('change', onStartDateNewChange);
//$('#absenceFinishNew').on('change', onFinishtDateNewChange);