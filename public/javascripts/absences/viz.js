$(document).ready(function(){
  // on event call functions
  $('#btnAbsencesViz').on('click', function () {
    if ($('#btnAbsencesOverview').hasClass('active')){
      makeVizData = true;
      $('#absenceStatusFilter').val(1).trigger('change'); // change to approved absences
      $('#absenceCollapse').collapse('toggle');
      $('#btnAbsencesOverview').removeClass('active');
      $('#vizCollapse').collapse('toggle');
      $('#btnAbsencesViz').addClass('active');
      //drawAbsencesViz();
    }
  })
  $('#btnAbsencesOverview').on('click', function () {
    if ($('#btnAbsencesViz').hasClass('active')) {
      $('#vizCollapse').collapse('toggle');
      $('#btnAbsencesViz').removeClass('active');
      $('#absenceCollapse').collapse('toggle');
      $('#btnAbsencesOverview').addClass('active');
    }
  })
  $('#moveLeftAbsencesBtn').on('click', function(){ move(0.2); });
  $('#homeAbsencesBtn').on('click', showCurrentWeek);
  $('#moveRightAbsencesBtn').on('click', function(){ move(-0.2); });
  $('#dayAbsencesBtn').on('click', showDay);
  $('#weekAbsencesBtn').on('click', showWeek);
  $('#monthAbsencesBtn').on('click', showMonth);
  $('#lowerFontAbsencesBtn').on('click', lowerFont);
  $('#higherFontAbsencesBtn').on('click', higherFont);
  // $('#refreshAbsencesBtn').on('click', refreshTimeline);
})
function prepareAbsencesViz(params) {
  debugger;
  makeVizData = false;
  // allDrawnAbsences = allAbsences.filter(a => a == a);

  //map function for style and correction
  var minDate, maxDate;
  if(allAbsences && allAbsences.length > 0){
    minDate = allAbsences[0].start; 
    maxDate = allAbsences[0].finish;
  }
  else{
    minDate = new Date();
    maxDate = new Date();
  }
  allAbsences.forEach(item => {
    item.group = item.id_user;
    if (item.start)
      item.start = new Date(item.start).setHours(0,0,0,0);
    if (item.finish)
      item.end = new Date(item.finish).setHours(24,0,0,0);

    if (item.name){// absence with name
      item.title = item.reason + ' - ' + item.name; // hover element
      item.content = item.reason + ' - ' + item.name; // inner html text
    }
    else{// absence with no name
      item.title = item.reason; // hover element
      item.content = item.reason; // inner html text
    }

    if(item.approved == true)
      item.className = 'builder';
    else if(item.approved == false)
      item.className = 'robot';
    // if absence is either approved or reject its class stayes the same as default
    // else
    //   item.className = 'electrican';
    //FIND MIN START AND MAX END DATES 
    if (new Date(item.start) < new Date(minDate)) minDate = item.start;
    if (new Date(item.end) > new Date(maxDate)) maxDate = item.end;
    //container.id = item.id;
    //container.content = item.text;
  })
  //var today = new Date();
  if(new Date() > new Date(maxDate)) maxDate = new Date();
  //for making odd rows different colors
  allFilteredUsers.forEach((users,i)=>{
    users.content = users.text;

    if(i % 2 == 0){
      users.style =  "background: rgba(82, 226, 233, 0.2);";
      allAbsences.push({
        group : users.id,
        start : moment(minDate).add(-100, 'days').format("YYYY-MM-DD") + ' 00:00',
        end : moment(maxDate).add(100, 'days').format("YYYY-MM-DD") + ' 23:59',
        type : 'background',
        className : 'odd'
      });
    }
  })
  if (!$('#absencesVis').html()){
    // its empty, call draw function for inital drawing
    drawAbsencesViz();
  }
  else{
    // not  empty, call refresh function for emptying $('#absencesVis') element and fixing its elements
    timeline.setItems(allAbsences);
  }
}
function drawAbsencesViz(){
  $('#absencesVis').empty();
  // create a DataSet with items
  /*
  const mapEmployeesToUsers = employees.map(item => {
    const container = {};

    container.id = item.id;
    container.content = item.text;

    return container;
  })
  */
  //allUsers = mapEmployeesToUsers;
  var container = document.getElementById("absencesVis");
  let chartHeight = window.innerHeight -350 +'px';
  var options = {
    orientation: {
      axis: 'both'
    },
    locale: "sl",
    verticalScroll: true,
    maxHeight: chartHeight,
    zoomKey: 'ctrlKey',
  };

  timeline = new vis.Timeline(container, allAbsences, allFilteredUsers, options);

  var today = new Date();

  setTimeout(()=>{
    setFontSize();
    setTimeout(()=>{
      timeline.setWindow(moment().format("YYYY-MM-DD"), moment().add(7, 'days').format("YYYY-MM-DD"));
    },300);
  },1000)
}
function addDays(date, days) {
  var result = new Date(date);
  result.setDate(result.getDate() + days);
  return result;
}
function setFontSize(){
  $('.vis-panel').css('font-size', fontSize);
  if(timeline){
    //move(0.001);
    timeline.redraw();
  }
}
function move(percentage) {
  var range = timeline.getWindow();
  var interval = range.end - range.start;

  timeline.setWindow({
    start: range.start.valueOf() - interval * percentage,
    end: range.end.valueOf() - interval * percentage,
  });
}
function lowerFont(){
  $('.vis-panel').css('font-size', --fontSize);
  if(timeline){
    //move(0.001);
    timeline.redraw();
  }
}
function higherFont(){
  $('.vis-panel').css('font-size', ++fontSize);
  if(timeline){
    //move(0.001);
    timeline.redraw();
  }
}
function showDay(){
  var today = new Date();

  setTimeout(()=>{
    timeline.setWindow(moment().format("YYYY-MM-DD"), moment().add(1, 'days').format("YYYY-MM-DD"));
  },100);
}
function showWeek(){
  var today = new Date();

  setTimeout(()=>{
    timeline.setWindow(moment().format("YYYY-MM-DD"), moment().add(7, 'days').format("YYYY-MM-DD"));
  },100);
}
function showMonth(){
  var today = new Date();

  setTimeout(()=>{
    timeline.setWindow(moment().format("YYYY-MM-DD"), moment().add(30, 'days').format("YYYY-MM-DD"));
  },100);
}
function showCurrentWeek(){
  var today = new Date();
  timeline.setWindow(moment().format("YYYY-MM-DD"), moment().add(7, 'days').format("YYYY-MM-DD"));  
}
let timeline;
let makeVizData = false;
let allDrawnAbsences;
var fontSize = 14;