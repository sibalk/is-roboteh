$(function(){
  $('#addModForm').submit(function (e) {
    e.preventDefault();
    $form = $(this);
    debugger
    var modName = modNameNew.value;
    var modCategoryId = modCategoryNew.value;
    var modStatusId = modStatusNew.value;

    let data = {modName, modCategoryId, modStatusId, projectId};
    $.ajax({
      type: 'POST',
      url: '/projects/modifications',
      contentType: 'application/json',
      data: JSON.stringify(data), // access in body
    }).done(function (resp) {
      if(resp.success){
        debugger;
        resp.modification.user_name = loggedUser.name + ' ' + loggedUser.surname;
        var categoryElem = '';
        switch (resp.modification.category_id) {
          case 2: categoryElem = `<h5 class="text-center"><span class="badge badge-pill badge-purchase ml-2">NABAVA</span></h5>`; resp.modification.category = "Nabava"; break;
          case 3: categoryElem = `<h5 class="text-center"><span class="badge badge-pill badge-construction ml-2">KONSTRUKCIJA</span></h5>`; resp.modification.category = "Konstrukcija"; break;
          case 4: categoryElem = `<h5 class="text-center"><span class="badge badge-pill badge-mechanic ml-2">STROJNA IZDELAVA</span></h5>`; resp.modification.category = "Strojna izdelava"; break;
          case 5: categoryElem = `<h5 class="text-center"><span class="badge badge-pill badge-electrican ml-2">ELEKTRO IZDELAVA</span></h5>`; resp.modification.category = "Elektro izdelava"; break;
          case 6: categoryElem = `<h5 class="text-center"><span class="badge badge-pill badge-assembly ml-2">MONTAŽA</span></h5>`; resp.modification.category = "Montaža"; break;
          case 7: categoryElem = `<h5 class="text-center"><span class="badge badge-pill badge-programmer ml-2">PROGRAMIRANJE</span></h5>`; resp.modification.category = "Programiranje"; break;
          default: ''; resp.modification.category = "Brez"; break;
        }
        allModifications.push(resp.modification);
        if((modActivity == '0' || modActivity == '1') && (modStatus == 0 || resp.modification.mod_status_id == modStatus) && (modCategory == 0 || resp.modification.category_id == modCategory)){
          //mod row element add
          var date = new Date(resp.modification.date);
          var selectOptions = '';
          //var disabledProp = (loggedUser.role == 'admin' || loggedUser.role.substring(0,5) == 'vodja') ? '' : ' disabled';
          allModStatuses.forEach(s => {
            if(resp.modification.mod_status_id == s.id) selectOptions += '<option value="'+s.id+'" selected="">'+s.status+'</option>';
            else selectOptions += '<option value="'+s.id+'">'+s.status+'</option>';
          })
          var buttonsElem = `
            <a class="mypopover p-1" data-toggle="popover" data-trigger="focus" tabindex="0" data-content="ni zabeleženih sprememb" id="modHistory` + resp.modification.id + `" data-original-title="" title="">
              <i class="fas fa-history fa-lg text-dark"></i>
            </a>
            <button class="btn btn-danger ml-2 mb-n1 delete-btn" id="modButtonDelete` + resp.modification.id + `" data-toggle="tooltip" data-original-title="Odstrani">
              <i class="fas fa-trash fa-lg"></i>
            </button>
            <button class="btn btn-purchase ml-1 mb-n1 files-btn" id="modButtonFiles` + resp.modification.id + `" data-toggle="tooltip" data-original-title="Datoteke">
              <i class="fas fa-file fa-lg"></i>
            </button>
            <button class="btn btn-primary ml-1 mb-n1 edit-btn" id="modButtonEdit` + resp.modification.id + `" data-toggle="tooltip" data-original-title="Uredi">
              <i class="fas fa-pen fa-lg"></i>
            </button>`;
          var element = `<tr id="mod` + resp.modification.id + `" class="">
            <td id="modDate` + resp.modification.id + `">` + date.toLocaleDateString('sl') + `</td>
            <td id="modName` + resp.modification.id + `">` + resp.modification.modification + `</td>
            <td id="modCategory` + resp.modification.id + `">` + categoryElem + `</td>
            <td id="modAuthor` + resp.modification.id + `">` + resp.modification.user_name + `</td>
            <td id="modStatusCell` + resp.modification.id + `">
              <div class="form-group ml-auto mb-n1">
                <select class="custom-select" id="modStatus` + resp.modification.id + `">
                  ` + selectOptions + `
                </select>
              </div>
            </td>
            <td id="modButtons` + resp.modification.id + `">` + buttonsElem + `</td>
          </tr>`;
          $('#modsTableBody').append(element);
        }
        //empty values of inputs in insert row
        $('#modNameNew').val('');
        $('#modCategoryNew').val(2);
        $('#modStatusNew').val(1);
        $('#modNameNew').focus()
        //add new change
        addChangeSystem(1,32,projectId,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,resp.modification.id);
      }
      else{
        $('#modalError').modal('toggle');
      }
    }).fail(function (resp) {
      //console.log('FAIL');
      //debugger;
      $('#modalError').modal('toggle');
    }).always(function (resp) {
      //console.log('ALWAYS');
      //debugger;
    });
  })
  //call event functions
  $('#modCategoryBtnGroup').on('click', 'button', function (event) {
    changeModsCategory(this.value);
  });
  $('#modStatusBtnGroup').on('click', 'button', function (event) {
    changeModsStatus(this.value);
  });
  $('#modActivityBtnGroup').on('click', 'button', function (event) {
    changeModsActivity(this.value);
  });
  $('#btnDeleteMod').on('click', deleteModConf);
  $('#modsTableBody').on('change', 'select', function (event) {
    //debugger;
    changeModStatus(this.id.substring(9));
  });
  $('#modsTableBody').on('click', 'button', function (event) {
    //debugger;
    if ($('#'+this.id).hasClass('edit-btn')){
      //debugger;
      editMod(this.id.substring(13));
    }
    else if ($('#'+this.id).hasClass('files-btn')){
      //debugger;
      filesMod(this.id.substring(14));
    }
    else if ($('#'+this.id).hasClass('delete-btn')){
      //debugger;
      deleteMod(this.id.substring(15));
    }
    else if ($('#'+this.id).hasClass('cancel-edit-btn')){
      //debugger;
      cancelEditMod(this.id.substring(15));
    }
    else if ($('#'+this.id).hasClass('conf-edit-btn')){
      //debugger;
      editModConf(this.id.substring(17));
    }
  });
  $('#modsTableBody').on('click', 'a', function (event) {
    getModChanges(this.id.substring(10));
  });
  $('#btnCollapseMod').on('click', toggleMods);
})
function toggleMods(){
  //get mods if opened for first time, open collapse and call draw modification
  if(!modsCollapseOpen){
    if(fileCollapseOpen){
      $('#btnCollapseFile').removeClass('active');
      $('#collapseFileTab').collapse("toggle");
      fileCollapseOpen = false;
    }
    else if(taskCollapseOpen){
      $('#btnCollapseTask').removeClass('active');
      $('#collapseTableTask').collapse("toggle");
      taskCollapseOpen = false;
    }

    if(!modsLoaded){
      loadMods();
      $('#btnCollapseMod').addClass('active');
      $('#collapseModification').collapse('toggle');
      modsCollapseOpen = true;
    }
    else{
      drawMods();
      $('#btnCollapseMod').addClass('active');
      $('#collapseModification').collapse('toggle');
      modsCollapseOpen = true;
    }
  }
  else{
    $('#btnCollapseMod').removeClass('active');
    $('#collapseModification').collapse('toggle');
    modsCollapseOpen = false;
  }
}
function loadMods(){
  let data = {projectId};
  $.ajax({
    type: 'GET',
    url: '/projects/modifications',
    contentType: 'application/x-www-form-urlencoded',
    data: data, // access in body
  }).done(function (resp) {
    //console.log('SUCCESS');
    debugger;
    if(resp.success){
      allModifications = resp.modifications;
      allModStatuses = resp.modStatuses;
      modsLoaded = true;
      drawMods();
    }
    else{
      $('#modalError').modal('toggle');  
    }
  }).fail(function (resp) {
    //console.log('FAIL');
    //debugger;
    $('#modalError').modal('toggle');
  }).always(function (resp) {
    //console.log('ALWAYS');
    //debugger;
  });
  var today = new Date();
  $('#modDateNew').val(moment().format("YYYY-MM-DD"));
}
function changeModStatus(id){
  debugger;
  var modStatusId = $('#modStatus'+id).val();
  var tmp = allModifications.findIndex(m => m.id == id);
  let data = {id, modStatusId};
  $.ajax({
    type: 'PUT',
    url: '/projects/modifications',
    contentType: 'application/json',
    data: JSON.stringify(data), // access in body
  }).done(function (resp) {
    if(resp.success){
      debugger;
      //update object
      allModifications[tmp].mod_status_id = modStatusId;
      //add system change
      addChangeSystem(11,32,projectId,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,id,modStatusId);
    }
    else{
      //change status back
      $('#modStatus'+id).val(allModifications[tmp].mod_status_id);
      $('#modalError').modal('toggle');
    }
  }).fail(function (resp) {
    //debugger;
    $('#modStatus'+id).val(allModifications[tmp].mod_status_id);
    $('#modalError').modal('toggle');
  }).always(function (resp) {
    //debugger;
  });
}
function editMod(id){
  var tmpMod = allModifications.find(m => m.id == id);
  //var disableProp = (loggedUser.role != 'admin') ? 'disabled=""' : '';
  //var dateInput = `<div class="form-group ml-n2 mr-n2 mb-n2"><input class="form-control hide-calendar-icon" id="activityDateEdit`+id+`" type="date" name="activityDateEdit" `+ disableProp +` required=""> </div>`;
  //$('#activityDate'+id).append(dateInput);
  $('#modName'+id).empty();//NAME
  var nameInput = `<div class="form-group ml-n2 mr-n2 mb-n2">
    <input class="form-control" id="modNameEdit` + id + `" type="text" name="modNameEdit" required="" autocomplete="off">
  </div>`;
  $('#modName' + id).append(nameInput);
  $('#modNameEdit' + id).val(tmpMod.modification);
  $('#modCategory' + id).empty();//CATEGORY
  var categoryInput = `<div class="form-group ml-auto mb-n1">
    <select class="custom-select" id="modCategoryEdit` + id + `">
      <option value="2">Nabava</option>
      <option value="3">Konstrukticja</option>
      <option value="4">Strojna izdelava</option>
      <option value="5">Elektro izdelava</option>
      <option value="6">Montaža</option>
      <option value="7">Programiranje</option>
      <option value="1">Brez</option>
    </select>
  </div>`;
  $('#modCategory' + id).append(categoryInput);
  $('#modCategoryEdit' + id).val(tmpMod.category_id);
  $('#modButtons' + id).empty();//BUTTONS
  var editButtons = `
    <a class="mypopover p-1" data-toggle="popover" data-trigger="focus" tabindex="0" data-content="ni zabeleženih sprememb" id="modHistory` + id + `" data-original-title="" title=""><i class="fas fa-history fa-lg text-dark"></i></a>
    <button class="btn btn-danger ml-1 mb-n1 cancel-edit-btn" id="modButtonCancel` + id + `" data-toggle="tooltip" data-original-title="Prekliči"><i class="fas fa-times fa-lg"></i></button>
    <button class="btn btn-success ml-1 mb-n1 conf-edit-btn" id="modButtonEditConf` + id + `" data-toggle="tooltip" data-original-title="Potrdi"><i class="fas fa-check fa-lg"></i></button>`;
  $('#modButtons' + id).append(editButtons);
}
function cancelEditMod(id){
  debugger;
  var tmpMod = allModifications.find(m => m.id == id);
  //$('#activityDate'+id).empty();//DATE
  //$('#activityDate'+id).html(new Date(tmpActivity.date).toLocaleDateString('sl'));
  $('#modName'+id).empty();//NAME
  $('#modName'+id).html(tmpMod.modification);
  $('#modCategory'+id).empty();//CATEGORY
  var categoryElem = '';
  switch (tmpMod.category_id) {
    case 2: categoryElem = `<h5 class="text-center"><span class="badge badge-pill badge-purchase ml-2">NABAVA</span></h5>`; break;
    case 3: categoryElem = `<h5 class="text-center"><span class="badge badge-pill badge-construction ml-2">KONSTRUKCIJA</span></h5>`; break;
    case 4: categoryElem = `<h5 class="text-center"><span class="badge badge-pill badge-mechanic ml-2">STROJNA IZDELAVA</span></h5>`; break;
    case 5: categoryElem = `<h5 class="text-center"><span class="badge badge-pill badge-electrican ml-2">ELEKTRO IZDELAVA</span></h5>`; break;
    case 6: categoryElem = `<h5 class="text-center"><span class="badge badge-pill badge-assembly ml-2">MONTAŽA</span></h5>`; break;
    case 7: categoryElem = `<h5 class="text-center"><span class="badge badge-pill badge-programmer ml-2">PROGRAMIRANJE</span></h5>`; break;
    default: ''; break;
  }
  $('#modCategory' + id).append(categoryElem);
  $('#modButtons' + id).empty();//BUTTONS
  let filesColor = (tmpMod.files_count && tmpMod.files_count > 0) ? ' text-dark' : '';
  var modButtons = `
    <a class="mypopover p-1" data-toggle="popover" data-trigger="focus" tabindex="0" data-content="ni zabeleženih sprememb" id="modHistory` + id + `" data-original-title="" title=""><i class="fas fa-history fa-lg text-dark"></i></a>
    <button class="btn btn-danger ml-2 mb-n1 delete-btn" id="modButtonDelete` + id + `" data-toggle="tooltip" data-original-title="Odstrani"><i class="fas fa-trash fa-lg"></i></button>
    <button class="btn btn-purchase ml-1 mb-n1 files-btn" id="modButtonFiles` + id + `" data-toggle="tooltip" data-original-title="Datoteke"><i class="fas fa-file fa-lg` + filesColor + `"></i></button>
    <button class="btn btn-primary ml-1 mb-n1 edit-btn" id="modButtonEdit` + id + `" data-toggle="tooltip" data-original-title="Uredi"><i class="fas fa-pen fa-lg"></i></button>`;
  $('#modButtons' + id).append(modButtons);
}
function editModConf(id){
  var tmpMod = allModifications.find(m => m.id == id);
  var modName = $('#modNameEdit' + id).val();
  let modCategoryId = $('#modCategoryEdit' + id).val();
  debugger;
  if(!modName || !modCategoryId){
    //missing req inputs
    $('#msgImg').attr('src','/warning_sign.png');
    $('#msg').html('Manjkajoči podatki! Prosimo vnesite vse potrebne podatke (naziv popravka) za uspešno posodobitev popravka.');
    $('#modalMsg').modal('toggle');
  }
  else{
    //prepare add and delete responsible users -> if responsible all is true then old resp users are delete users
    debugger
    let data = {id, modName, modCategoryId};
    $.ajax({
      type: 'PUT',
      url: '/projects/modifications',
      contentType: 'application/json',
      data: JSON.stringify(data), // access in body
    }).done(function (resp) {
      if(resp.success){
        debugger;
        tmpMod.modification = resp.modification.modification;//NAME
        $('#modName'+id).empty();
        $('#modName'+id).html(tmpMod.modification);
        tmpMod.category_id = resp.modification.category_id;//CATEGORY
        $('#modCategory' + id).empty();
        var categoryElem = '';
        switch (tmpMod.category_id) {
          case 2: categoryElem = `<h5 class="text-center"><span class="badge badge-pill badge-purchase ml-2">NABAVA</span></h5>`; tmpMod.category = 'Nabava'; break;
          case 3: categoryElem = `<h5 class="text-center"><span class="badge badge-pill badge-construction ml-2">KONSTRUKCIJA</span></h5>`; tmpMod.category = 'Konstrukcija'; break;
          case 4: categoryElem = `<h5 class="text-center"><span class="badge badge-pill badge-mechanic ml-2">STROJNA IZDELAVA</span></h5>`; tmpMod.category = 'Strojna izdelava'; break;
          case 5: categoryElem = `<h5 class="text-center"><span class="badge badge-pill badge-electrican ml-2">ELEKTRO IZDELAVA</span></h5>`; tmpMod.category = 'Elektro izdelava'; break;
          case 6: categoryElem = `<h5 class="text-center"><span class="badge badge-pill badge-assembly ml-2">MONTAŽA</span></h5>`; tmpMod.category = 'Montaža'; break;
          case 7: categoryElem = `<h5 class="text-center"><span class="badge badge-pill badge-programmer ml-2">PROGRAMIRANJE</span></h5>`; tmpMod.category = 'Programiranje'; break;
          default: ''; tmpMod.category = 'Brez'; break;
        }
        $('#modCategory' + id).append(categoryElem);
        debugger;
        $('#modButtons'+id).empty();//BUTTONS
        let filesColor = (tmpMod.files_count && tmpMod.files_count > 0) ? ' text-dark' : '';
        var modButtons = `
          <a class="mypopover p-1" data-toggle="popover" data-trigger="focus" tabindex="0" data-content="ni zabeleženih sprememb" id="modHistory` + id + `" data-original-title="" title=""><i class="fas fa-history fa-lg text-dark"></i></a>
          <button class="btn btn-danger ml-2 mb-n1 delete-btn" id="modButtonDelete` + id + `" data-toggle="tooltip" data-original-title="Odstrani"><i class="fas fa-trash fa-lg"></i></button>
          <button class="btn btn-purchase ml-1 mb-n1 files-btn" id="modButtonFiles` + id + `" data-toggle="tooltip" data-original-title="Datoteke"><i class="fas fa-file fa-lg` + filesColor + `"></i></button>
          <button class="btn btn-primary ml-1 mb-n1 edit-btn" id="modButtonEdit` + id + `" data-toggle="tooltip" data-original-title="Uredi"><i class="fas fa-pen fa-lg"></i></button>`;
        $('#modButtons'+id).append(modButtons);
        //var tmp = allActivities.findIndex(a => a.id == id);
        //allActivities[tmp] = tmpActivity;
        //add new change
        addChangeSystem(2,32,projectId,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,id);
      }
      else{
        $('#modalError').modal('toggle');
      }
    }).fail(function (resp) {
      //debugger;
      $('#modalError').modal('toggle');
    }).always(function (resp) {
      //debugger;
    });
  }
}
function deleteMod(id){
  modId = id;
  var tmpMod = allModifications.find(m => m.id == modId);
  if(tmpMod.active == true){
    $('#deleteModModalMsg').html('Ste prepričani, da želite odstraniti ta popravek?');
    $('#btnDeleteMod').html('Odstrani');
  }
  else{
    $('#deleteModModalMsg').html('Ste prepričani, da želite ponovno dodati ta popravek?');
    $('#btnDeleteMod').html('Ponovno dodaj');
  }
  $('#modalDeleteMod').modal('toggle');
}
function deleteModConf(){
  var tmpMod = allModifications.find(m => m.id == modId);
  var active =  (tmpMod.active == true) ? false : true; // IF current active is true THEN false ELSE true
  
  let data = {modId, active};
  $.ajax({
    type: 'DELETE',
    url: '/projects/modifications',
    contentType: 'application/json',
    data: JSON.stringify(data), // access in body
  }).done(function (resp) {
    //console.log('SUCCESS');
    debugger;
    if(resp.success){
      tmpMod.active = active;
      if(loggedUser.role == 'admin'){
        //marked new active on activity table and update variables //DEPENDS ON WHICH ACTIVITY TABEL IS SHOWN
        if(modActivity == '0'){
          //true and false, all activities are shown, update variables and  class & icon on table
          if(active){
            $('#mod'+modId).removeClass('bg-secondary');
            $('#modButtonDelete'+modId).find('i').removeClass('fa-trash-restore');
            $('#modButtonDelete'+modId).find('i').addClass('fa-trash');
          }
          else{
            $('#mod'+modId).addClass('bg-secondary');
            $('#modButtonDelete'+modId).find('i').removeClass('fa-trash');
            $('#modButtonDelete'+modId).find('i').addClass('fa-trash-restore');
          }
        }
        else if(modActivity == '1' || modActivity == '2'){
          //true, only active activities are shown, same as any other role user // same for false, only non active activities are shown
          //allModifications = allModifications.filter(a => a.id != activityId);
          $('#mod'+modId).remove();
        }
      }
      else{
        //remove deleted activity from activity table and remove activity from variables
        //allActivities = allActivities.filter(a => a.id != activityId);
        $('#mod'+modId).remove();
      }
      //add new change
      if(active) addChangeSystem(6,32,projectId,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,modId);
      else addChangeSystem(3,32,projectId,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,modId);

      $('#modalDeleteMod').modal('toggle');
    }
    else{
      $('#modalDeleteMod').modal('toggle');
      $('#modalError').modal('toggle');
    }
  }).fail(function (resp) {
    //console.log('FAIL');
    //debugger;
    $('#modalError').modal('toggle');
  }).always(function (resp) {
    //console.log('ALWAYS');
    //debugger;
  });
}
function changeModsStatus(status){
  //update modStatus, activate correct status button
  modStatus = status;
  $('#optionModStatus0').removeClass('active stay-focus');
  $('#optionModStatus1').removeClass('active stay-focus');
  $('#optionModStatus2').removeClass('active stay-focus');
  $('#optionModStatus3').removeClass('active stay-focus');
  setTimeout(()=>{
    $('#optionModStatus'+status+'').addClass('active stay-focus');
  })
  drawMods();
}
function changeModsCategory(category){
  //update modCategory, activate correct category button
  modCategory = category;
  $('#optionModCategory0').removeClass('active stay-focus');
  $('#optionModCategory1').removeClass('active stay-focus');
  $('#optionModCategory2').removeClass('active stay-focus');
  $('#optionModCategory3').removeClass('active stay-focus');
  $('#optionModCategory4').removeClass('active stay-focus');
  $('#optionModCategory5').removeClass('active stay-focus');
  $('#optionModCategory6').removeClass('active stay-focus');
  $('#optionModCategory7').removeClass('active stay-focus');
  setTimeout(()=>{
    $('#optionModCategory'+category+'').addClass('active stay-focus');
  })
  drawMods();
}
function changeModsActivity(activity){
  //update modActivity, activate correct activity button
  modActivity = activity;
  $('#optionModActivity0').removeClass('active stay-focus');
  $('#optionModActivity1').removeClass('active stay-focus');
  $('#optionModActivity2').removeClass('active stay-focus');
  setTimeout(()=>{
    $('#optionModActivity'+activity+'').addClass('active stay-focus');
  })
  drawMods();
}
function drawMods(){
  // filter all modifications based on status, category and activity; empty modification table and draw filtered inside
  debugger;
  //category filter
  var modifications = (modCategory == 0) ? allModifications.filter(m => m) : allModifications.filter(m => m.category_id == modCategory);
  //status filter
  modifications = (modStatus == 0) ? modifications.filter(m => m) : modifications.filter(m => m.mod_status_id == modStatus);
  //activity filter
  var activity = (modActivity == 1) ? true : false;
  modifications = (modActivity == 0) ? modifications.filter(m => m) : modifications.filter(m => m.active == activity);
  debugger;
  let oddClass = '';
  $('#modsTableBody').empty();
  modifications.forEach(m => {
    var date = new Date(m.date);
    var authorLabel = m.user_name;
    var activeLabel = (m.active) ? '' : ' bg-secondary';
    var deleteIcon = (m.active) ? 'trash' : 'trash-restore';
    var selectOptions = '';
    //var disabledProp = (loggedUser.role == 'admin' || loggedUser.role.substring(0,5) == 'vodja') ? '' : ' disabled';
    var disabledProp = '';
    allModStatuses.forEach(s => {
      if(m.mod_status_id == s.id) selectOptions += '<option value="'+s.id+'" selected="">'+s.status+'</option>';
      else selectOptions += '<option value="'+s.id+'">'+s.status+'</option>';
    })
    var categoryElem = '';
    switch (m.category_id) {
      case 2: categoryElem = `<h5 class="text-center"><span class="badge badge-pill badge-purchase ml-2">NABAVA</span></h5>`; break;
      case 3: categoryElem = `<h5 class="text-center"><span class="badge badge-pill badge-construction ml-2">KONSTRUKCIJA</span></h5>`; break;
      case 4: categoryElem = `<h5 class="text-center"><span class="badge badge-pill badge-mechanic ml-2">STROJNA IZDELAVA</span></h5>`; break;
      case 5: categoryElem = `<h5 class="text-center"><span class="badge badge-pill badge-electrican ml-2">ELEKTRO IZDELAVA</span></h5>`; break;
      case 6: categoryElem = `<h5 class="text-center"><span class="badge badge-pill badge-assembly ml-2">MONTAŽA</span></h5>`; break;
      case 7: categoryElem = `<h5 class="text-center"><span class="badge badge-pill badge-programmer ml-2">PROGRAMIRANJE</span></h5>`; break;
      default: ''; break;
    }
    let filesColor = (m.files_count && m.files_count > 0) ? ' text-dark' : '';
    var buttonsElem = '';
    buttonsElem = `
      <a class="mypopover p-1" data-toggle="popover" data-trigger="focus" tabindex="0" data-content="ni zabeleženih sprememb" id="modHistory` + m.id + `" data-original-title="" title="">
        <i class="fas fa-history fa-lg text-dark"></i>
      </a>
      <button class="btn btn-danger ml-2 mb-n1 delete-btn" id="modButtonDelete` + m.id + `" data-toggle="tooltip" data-original-title="Odstrani">
        <i class="fas fa-`+deleteIcon+` fa-lg"></i>
      </button>
      <button class="btn btn-purchase ml-1 mb-n1 files-btn" id="modButtonFiles` + m.id + `" data-toggle="tooltip" data-original-title="Datoteke">
        <i class="fas fa-file fa-lg` + filesColor + `"></i>
      </button>
      <button class="btn btn-primary ml-1 mb-n1 edit-btn" id="modButtonEdit` + m.id + `" data-toggle="tooltip" data-original-title="Uredi">
        <i class="fas fa-pen fa-lg"></i>
      </button>`;
    var element = `<tr id="mod` + m.id + `" class="` + activeLabel + oddClass + `">
      <td id="modDate` + m.id + `">` + date.toLocaleDateString('sl') + `</td>
      <td id="modName` + m.id + `">` + m.modification + `</td>
      <td id="modCategory` + m.id + `">` + categoryElem + `</td>
      <td id="modAuthor` + m.id + `">` + authorLabel + `</td>
      <td id="modStatusCell` + m.id + `">
        <div class="form-group ml-auto mb-n1">
          <select class="custom-select" id="modStatus` + m.id + `" ` + disabledProp + `>
            ` + selectOptions + `
          </select>
        </div>
      </td>
      <td id="modButtons` + m.id + `">` + buttonsElem + `</td>
    </tr>
    <tr><td colspan="12" class="hidden-row` + oddClass + `">
      <div id="modFilesCollapse`+m.id+`" class="collapse">
      <div class="list-group span-absence-list mt-2 mr-3 ml-3" id="modFileList`+m.id+`">
      </div>
      <div id="addModFileRow`+m.id+`" class="mb-3 mt-3">
        <div class="text-center">
          <button class="btn btn-success">Dodaj datoteko</button>
        </div>
      </div>
    </div>
    </td></tr>`;
    $('#modsTableBody').append(element);
    $('#addModFileRow'+m.id).find('.btn-success').on('click', {id: m.id}, onEventNewModDocumentModal);
    oddClass = (oddClass == '') ? ' odd-row' : '';
  })
}
function getModChanges(id){
  debugger;
  var modId = id;
  let data = {modId};
  $.ajax({
    type: 'GET',
    url: '/projects/modifications/changes',
    contentType: 'application/json',
    data: data, // access in body
  }).done(function (resp) {
    if(resp.success){
      debugger;
      var modChanges = resp.modChanges;
      console.log('Successfully getting all the changes of selected modification.');
      var element = "";
      var fileLabel = "";
        for(var i=0; i<modChanges.length; i++){
          if(modChanges[i].id_status == 11)
            fileLabel = " na " + modChanges[i].mod_status;
          else
            fileLabel = "";
          element = new Date(modChanges[i].date).toLocaleDateString('sl') + ' ' + new Date(modChanges[i].date).toLocaleTimeString('sl').substring(0,5) + ", " + modChanges[i].name + " " + modChanges[i].surname + ', ' + modChanges[i].status + fileLabel + "<br />" + element;
        }
        //$('.mypopover').attr('data-content', element);
        $('#modHistory'+modId).attr('data-content', element);
        $('[data-toggle="popover"]').popover({ html: true });
        $('#modHistory'+modId).popover('show');
        //$('[data-toggle="popover"]').popover({trigger: "hover"}); 
    }
    else{
      $('#modalError').modal('toggle');
    }
  }).fail(function (resp) {
    //console.log('FAIL');
    //debugger;
    $('#modalError').modal('toggle');
  }).always(function (resp) {
    //console.log('ALWAYS');
    //debugger;
  });
}
function filesMod(id) {
  //$('#modFilesCollapse'+id).collapse('toggle');
  openModFiles(id);
}
//OPEN MODIFICATION FILES COLLAPSE AND FILL MOD FILES LIST
function openModFiles(id){
  //debugger;
  let collapseOpened = $('#modFilesCollapse' + id).hasClass('show');
  if (collapseOpened){//was opened, close it
    $('#modFilesCollapse'+id).collapse('toggle');
  }
  else{//get new files, empty the list and draw new files
    $('#modFileList'+id).empty();
    $.get('/projects/modifications/files', {projectModId: id}, function(data){
      let modFiles = data.data;
      //debugger;
      //collapse frame
  
      for(var i = 0; i < modFiles.length; i++){
        let deleteButton = ``;
        let downloadClassStyle = '';
        if ((modFiles[i].name + ' ' + modFiles[i].surname) == (loggedUser.name + ' ' + loggedUser.surname) || loggedUser.role == 'admin' || loggedUser.role == 'vodja projektov'){
          deleteButton = `<button class="btn btn-danger ml-auto mr-2 mt-3" id="fileButtonDelete`+modFiles[i].id+`" data-toggle="tooltip" data-original-title="Odstrani">
            <i class="fas fa-trash fa-lg"></i>
          </button>`;
        }
        else{
          downloadClassStyle = ' ml-auto';
        }
        var downloadButton = `<a class="btn btn-primary mt-3`+downloadClassStyle+`" href="/projects/sendMeDoc?filename=`+modFiles[i].path_name+`&projectId=`+projectId+`">
          <i class="fas fa-download fa-lg"></i>
        </a>`;
        var date = new Date(modFiles[i].date).toLocaleDateString('sl');
        var time = new Date(modFiles[i].date).toLocaleTimeString('sl').substring(0,5);
        var fileSrc = '/file'+modFiles[i].type+'.png'
        var listElement = `<div class="list-group-item" id="modFile`+modFiles[i].id+`">
          <div class="row">
            <div class="col-sm-3 justify-content-center d-flex">
              <div class="preview-icon">
                <img class="img-fluid bg-light image-icon w-70px" src=` + fileSrc + ` alt="ikona" />
              </div>
            </div>
            <div class="col-sm-7">
              <label class="row mt-2">`+modFiles[i].original_name+`</label>
              <label class="row mt-2 small">Naloženo: ` + date + ` ob ` + time + ` Avtor: ` + modFiles[i].name + " " + modFiles[i].surname + `</label>
            </div>
            <div class="col-sm-2">
              <div class="d-flex">
                `+deleteButton+`
                `+downloadButton+`
              </div>
            </div>
          </div>
        </div>`;
        $('#modFileList'+id).append(listElement);
        $('#modFile' + modFiles[i].id).find('.btn-danger').on('click', {id: modFiles[i].id}, onEventDeleteFile);
      }
      //allTaskFiles = files;
      $('#modFilesCollapse'+id).collapse("toggle");
      //taskFilesCollapseOpen = true;
      setTimeout(()=>{
        getFileThumbnails(modFiles, false, true);
      },500)
    });
  }
}
var modId;
var modsCollapseOpen = false;
var allModifications;
var allModStatuses;
var modsLoaded = false;
var modStatus = 0, modCategory = 0, modActivity =1;