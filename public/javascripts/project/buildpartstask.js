$(function(){
  //add onchange for taskCategoryAdd to show collapse if category is strojna izdelava else hide
  $('#taskCategoryAdd').on('change', newCategoryChange);
  $('#newBuildPartCsv').change(onNewChangeCSVInput);

  //add new build part to preview
  $('#addBPTForm').submit(function (e) {
    e.preventDefault();
    $form = $(this);
    debugger;
    var position = $('#newBPPosition').val();
    var name = $('#newBPName').val();
    var quantity = $('#newBPQuantity').val();
    var material = $('#newBPMaterial').val();
    var standard = $('#newBPStandard').val();
    var note = $('#newBPNote').val();
    var tag = $('#newBPTag').val();
    var selected = true;
    buildPartsPreview.newBuildParts.push({position, name, quantity, material, standard, note, tag, selected});
    var element = `<tr id="newBP` + counterBPPreview + `" class="">
      <td>
        <div class="form-group ml-n2 mr-n2 mb-n2">
          <div class="custom-control custom-checkbox table-custom-checkbox">
            <input class="custom-control-input" id="newBPSelected` + counterBPPreview + `" type="checkbox" checked="">
            <label class="custom-control-label" for="newBPSelected` + counterBPPreview + `"></label>
          </div>
        </div>
      </td>
      <td>
        <div class="form-group ml-n2 mr-n2 mb-n2">
          <input class="form-control" id="newBPPosition` + counterBPPreview + `" type="number" name="newBPPosition` + counterBPPreview + `" required="" value="` + position + `">
        </div>
      </td>
      <td>
        <div class="form-group ml-n2 mr-n2 mb-n2">
          <input class="form-control" id="newBPName` + counterBPPreview + `" type="text" name="newBPName` + counterBPPreview + `" required="" value="` + name + `">
        </div>
      </td>
      <td>
        <div class="form-group ml-n2 mr-n2 mb-n2">
          <input class="form-control" id="newBPQuantity` + counterBPPreview + `" type="text" name="newBPQuantity` + counterBPPreview + `" required="" value="` + quantity + `">
        </div>
      </td>
      <td>
        <div class="form-group ml-n2 mr-n2 mb-n2">
          <input class="form-control" id="newBPMaterial` + counterBPPreview + `" type="text" name="newBPMaterial` + counterBPPreview + `" required="" value="` + material + `">
        </div>
      </td>
      <td>
        <div class="form-group ml-n2 mr-n2 mb-n2">
          <input class="form-control" id="newBPStandard` + counterBPPreview + `" type="text" name="newBPStandard` + counterBPPreview + `" required="" value="` + standard + `">
        </div>
      </td>
      <td>
        <div class="form-group ml-n2 mr-n2 mb-n2">
          <input class="form-control" id="newBPNote` + counterBPPreview + `" type="text" name="newBPNote` + counterBPPreview + `" required="" value="` + note + `">
        </div>
      </td>
      <td>
        <div class="form-group ml-n2 mr-n2 mb-n2">
          <input class="form-control" id="newBPTag` + counterBPPreview + `" type="text" name="newBPTag` + counterBPPreview + `" required="" value="` + tag + `">
        </div>
      </td>
    </tr>`;
    $('#newBuildPartTableBody').prepend(element);
    $('#newBPPosition').val(++position);
    counterBPPreview++;
    $('#newBPName').val('');
    $('#newBPQuantity').val('');
    $('#newBPMaterial').val('');
    $('#newBPStandard').val('');
    $('#newBPNote').val('');
    $('#newBPTag').val('');
  })
  //call event functions
  $('#btnCnfmBPNT').on('click', buildPartsPreviewConf);
})
function newCategoryChange(){
  if ($('#taskCategoryAdd').val() == 4){
    $('#taskNameAddLabel').text('Ime opravila (Št. risbe)');
    $('#taskNoteAddLabel').text('Komentar (Ime sestava)')
    $('#newTaskBuildPartsCollapse').collapse('show');
  }
  else{
    $('#taskNameAddLabel').text('Ime opravila');
    $('#taskNoteAddLabel').text('Komentar')
    $('#newTaskBuildPartsCollapse').collapse('hide');
  }
}
function onNewChangeCSVInput(){
  debugger;
  var fileInput = $('#newBuildPartCsv').val().split("\\");
  fileInput = fileInput[fileInput.length-1];
  var fileExtention = fileInput.split('.');
  fileExtention = fileExtention[fileExtention.length-1];
  let tmpLabel = 'Št. datotek: ' + $('#newBuildPartCsv').prop('files').length + ' (';
  if ($('#newBuildPartCsv').prop('files').length != 0){
    for (let i = 0; i < $('#newBuildPartCsv').prop('files').length; i++) {
      const element = $('#newBuildPartCsv').prop('files')[i];
      if (i != 0)
        tmpLabel += ', ';
      tmpLabel += element.name;
    }
    tmpLabel += ')';
  }
  else {
    tmpLabel = '';
  }
  $('#newBuildPartCsvLabel').html(tmpLabel);
  if($('#taskNameAdd').val() == ''){
    $('#taskNameAdd').val(fileInput.split('.')[0]);
    // $('#taskBuildPartParentNumberAdd').val(fileInput.split('.')[0]);
  }
  // else if($('#taskBuildPartParentNumberAdd').val() == '')
  //   $('#taskBuildPartParentNumberAdd').val(fileInput.split('.')[0]);
  debugger;
  // testing for new naming files or lead build part
  preparseCsvToNewTaskWithBuildParts();
  // createInputFileNamesNewTask();
}
//make input values for every file for lead build part and its standard name
function createInputFileNamesNewTask() {
  $('#newTaskBPFileNames').empty();
  $('#newBPFileNames').empty();
  $('#inputBuildPartCsvLabel').html('');
  $('#buildPartStandardAdd').val('');
  if ($('#newBuildPartCsv').prop('files').length != 0){
    for (let i = 0; i < $('#newBuildPartCsv').prop('files').length; i++) {
      const element = $('#newBuildPartCsv').prop('files')[i];
      let buildpartName = '-';
      let buildpartInvalidFeedback = `<div class="invalid-feedback">Za boljšo preglednost vnesite ime sestava ( znaki -,/ itd so moteči)!</div>`;
      let invalidClass = ' is-invalid';
      //try to find the name of the file build part
      for (let j = 0; j < allResults.length; j++) {
        const result = allResults[j];
        let tmp = result.data.find(bp => bp[4] == element.name.split('.')[0])
        if(tmp){
          buildpartName = tmp[1];
          invalidClass = '';
          break;
        }
      }
      // tmpLabel += element.name;
      let divElement = `<div class="form-row">
        <div class="custom-control custom-radio w-100 mt-4">
          <input type="radio" id="buildPartParentRadio` + i + `" name="buildPartParentRadio" class="custom-control-input">
          <label class="custom-control-label form-row w-100" for="buildPartParentRadio` + i + `">
            <div class="col-md-6 mt-n4">
              <div class="form-group">
                <label class="w-100" for="buildPartParentAdd` + i + `">Ime ` + (i+1) + `. sestava
                  <input class="form-control` + invalidClass + `" id="buildPartParentAdd` + i + `" type="text" name="buildPartParentAdd` + i + `" required="" placeholder="Ime sestava" value='` + buildpartName + `' />
                  ` + buildpartInvalidFeedback + `
                </label>
              </div>
            </div>
            <div class="col-md-6 mt-n4">
              <div class="form-group">
                <label class="w-100" for="buildPartParentNumberAdd` + i + `">Št. risbe ` + (i+1) + `. sestava
                  <input class="form-control" id="buildPartParentNumberAdd` + i + `" type="text" name="buildPartParentNumberAdd` + i + `" required="" placeholder="Št. risbe sestava" value='` + element.name.split('.')[0] + `' />
                </label>
              </div>
            </div>
          </label>
        </div>
      </div>`;
    $('#newTaskBPFileNames').append(divElement);
    }
    // add on change event to first build part name (or last one)
    // problem if first build part is not the first build part in files group
    if($('#buildPartParentAdd0').val() == '-'){
      // $('#buildPartParentAdd0').on('keyup', {k:0}, onChangeBuildPartNameToNote);
      buildPartParentIndex = 0;
      $('#buildPartParentRadio' + buildPartParentIndex).prop('checked', true);
    }
    else if($('#buildPartParentAdd'+($('#newBuildPartCsv').prop('files').length -1)).val() == '-'){
      let k = $('#newBuildPartCsv').prop('files').length -1;
      // $('#buildPartParentAdd'+k).on('keyup', {k}, onChangeBuildPartNameToNote);
      buildPartParentIndex = k;
      $('#buildPartParentRadio' + buildPartParentIndex).prop('checked', true);
    }
    // disable add button if there is no name on note or name of sestav
    if ($('#taskNoteAdd').val() == '' || $('#taskNoteAdd').val() == '-' || $('#taskNoteAdd').val() == '/')
      $('#btnTaskAdd').prop('disabled', true);
    // testing
    $('#newTaskBPFileNames').on('change', onChangeBuildPartParentsNames);
    $('#newTaskBPFileNames').on('keyup', onKeyUpBuildPartParentsNames);
  }
}
function onChangeBuildPartNameToNote(params) {
  // debugger;
  console.log('key up old');
  $('#taskNoteAdd').val($('#buildPartParentAdd' + params.data.k).val());
  // (('#taskNoteAdd').val() == '') ? $('#btnTaskAdd').prop('disabled', true) : $('#btnTaskAdd').prop('disabled', false);
  if ($('#taskNoteAdd').val() == '' || $('#taskNoteAdd').val() == '-' || $('#taskNoteAdd').val() == '/'){
    if($('#buildPartParentAdd' + 0).hasClass('is-invalid') == false)
      $('#buildPartParentAdd' + 0).addClass('is-invalid')
    $('#btnTaskAdd').prop('disabled', true);
  }
  else{
    $('#btnTaskAdd').prop('disabled', false);
    $('#buildPartParentAdd' + params.data.k).removeClass('is-invalid');
  }
}
function onChangeBuildPartParentsNames(params){
  debugger;
  console.log('change');
  if($('#'+params.target.id).prop('checked')){
    buildPartParentIndex = params.target.id.substring(20);
    $('#taskNameAdd').val( $('#buildPartParentNumberAdd' + buildPartParentIndex).val() );
    $('#taskNoteAdd').val( $('#buildPartParentAdd' + buildPartParentIndex).val() );

    if ($('#taskNoteAdd').val() == '' || $('#taskNoteAdd').val() == '-' || $('#taskNoteAdd').val() == '/')
      $('#btnTaskAdd').prop('disabled', true);
    else
      $('#btnTaskAdd').prop('disabled', false);
  }
}
function onKeyUpBuildPartParentsNames(params){
  // debugger;
  console.log('new key up');
  let tempInput = $('#' + params.target.id).val();
  let tempIndex = params.target.id.substring(18);
  if (tempInput == '' || tempInput == '-' || tempInput == '/')
    $('#buildPartParentAdd' + tempIndex).addClass('is-invalid');
  else
    $('#buildPartParentAdd' + tempIndex).removeClass('is-invalid');
  // if selected change name/note as well
  if (tempIndex == buildPartParentIndex){
    $('#taskNameAdd').val( $('#buildPartParentNumberAdd' + buildPartParentIndex).val() );
    $('#taskNoteAdd').val( $('#buildPartParentAdd' + buildPartParentIndex).val() );
    
    if ($('#taskNoteAdd').val() == '' || $('#taskNoteAdd').val() == '-' || $('#taskNoteAdd').val() == '/')
      $('#btnTaskAdd').prop('disabled', true);
    else
      $('#btnTaskAdd').prop('disabled', false);
  }
}
function preparseCsvToNewTaskWithBuildParts(){//if new task has category 4 and csv file input then call this function else call regular create new task function
  $('#taskBuildPartParentAdd').removeClass("is-invalid");
  $('#taskBuildPartParentNumberAdd').removeClass("is-invalid");
  var fileInput = $('#newBuildPartCsv').val().split("\\");
  fileInput = fileInput[fileInput.length-1];
  var fileExtention = fileInput.split('.');
  fileExtention = fileExtention[fileExtention.length-1];

  if (fileExtention != 'csv'){
    alert('Datoteka, ki jo zelite sestaviti v kosovnico ni CSV datoteka.');
  }
  else{
    allResults = [];
    var config = buildConfigNewTask();
    $('#newBuildPartCsv').parse({
      config: config,
      before: function(file, inputElem)
      {
        start = now();
        console.log("Parsing file...", file);
        currentFileName = file.name.split('.')[0];
      },
      error: function(err, file)
      {
        console.log("ERROR:", err, file);
        firstError = firstError || err;
        errorCount++;
      },
      complete: function()
      {
        end = now();
        //createPreviewBuildParts(allResults);
        createInputFileNamesNewTask();
        printStats("Done with all files");
      }
    });
  }
}
function parseCsvToNewTaskWithBuildParts(){//if new task has category 4 and csv file input then call this function else call regular create new task function
  $('#taskBuildPartParentAdd').removeClass("is-invalid");
  $('#taskBuildPartParentNumberAdd').removeClass("is-invalid");
  var fileInput = $('#newBuildPartCsv').val().split("\\");
  fileInput = fileInput[fileInput.length-1];
  var fileExtention = fileInput.split('.');
  fileExtention = fileExtention[fileExtention.length-1];

  if (fileExtention != 'csv'){
    alert('Datoteka, ki jo zelite sestaviti v kosovnico ni CSV datoteka.');
  }
  else{
    allResults = [];
    var config = buildConfigNewTask();
    $('#newBuildPartCsv').parse({
      config: config,
      before: function(file, inputElem)
      {
        start = now();
        console.log("Parsing file...", file);
        currentFileName = file.name.split('.')[0];
      },
      error: function(err, file)
      {
        console.log("ERROR:", err, file);
        firstError = firstError || err;
        errorCount++;
      },
      complete: function()
      {
        end = now();
        createPreviewBuildParts(allResults);
        printStats("Done with all files");
      }
    });
  }
}
function buildConfigNewTask(){
  return {
    header: false,
    dynamicTyping: false,
    skipEmptyLines: false,
    preview: 0,
    step: false,
    encoding: 'Windows-1250',
    worker: false,
    comments: 'default',
    complete: completeFnNewTask,
    error: errorFn,
    download: inputType == "remote",
  };
}
function completeFnNewTask(results){
  end = now();

  if (results && results.errors)
  {
    if (results.errors)
    {
      errorCount = results.errors.length;
      firstError = results.errors[0];
    }
    if (results.data && results.data.length > 0)
      rowCount = results.data.length;
  }

  results.fileName = currentFileName;

  printStats("Parse complete");
  console.log("    Results:", results);
  //allParseResults.push(results);
  debugger;
  allResults.push(results);
  //createPreviewBuildParts(results);

  // icky hack
  setTimeout(enableButton, 100);
}
function createPreviewBuildParts(allGivenResults){
  debugger;
  buildPartsPreviewArray = [];
  for(let j = 0; j < allGivenResults.length; j++){
    const results = allGivenResults[j];

    if (results.errors.length > 0){
      alert('Pri branju datoteke je prišlo do napake.');
    }
    else{
      var data = results.data;
      if (data[0][0].toLocaleLowerCase() == 'poz.' && data[0][1].toLocaleLowerCase() == 'naziv' && data[0][2].toLocaleLowerCase() == 'kol.' && data[0][3].toLocaleLowerCase() == 'material' &&
      data[0][4].toLocaleLowerCase() == 'št.risbe / standard'){
        //kosovnica je postavljena v pravilnem vrstem redu (tega verjetno nikoli ne bo)
        data.shift();
        debugger;
      }
      else if ((data[data.length-1][0].toLocaleLowerCase() == 'poz.' && data[data.length-1][1].toLocaleLowerCase() == 'naziv' && data[data.length-1][2].toLocaleLowerCase() == 'kol.' &&
      data[data.length-1][3].toLocaleLowerCase() == 'material' && data[data.length-1][4].toLocaleLowerCase() == 'št.risbe / standard') 
      || 
      (data[data.length-2][0].toLocaleLowerCase() == 'poz.' && data[data.length-2][1].toLocaleLowerCase() == 'naziv' && data[data.length-2][2].toLocaleLowerCase() == 'kol.' &&
      data[data.length-2][3].toLocaleLowerCase() == 'material' && data[data.length-2][4].toLocaleLowerCase() == 'št.risbe / standard' )){
        //kosovnica ja taksa kot je v nacrtu, postavljena na glavo, taksna bo skoraj vedno
        data.pop();
        if (data[data.length-1][0].toLocaleLowerCase() == 'poz.')
          data.pop();
        debugger;
        var newBuildParts = [];
        //APPLY FILTERS FROM INPUT FORM
        // var screwCheck = $('#checkboxScrews').prop('checked'),
        // supplierCheck = $('#checkboxSuppliers').prop('checked'),
        // materialCheck = $('#checkboxMaterials').prop('checked');
        let screwCheck = false, supplierCheck = false, materialCheck = true;
        // data.forEach(d => {if ((/RT\d\d\d-\d\d-\d\d-\d\d-\d\d-\d\d-\d/).test( d[4] ) || (/RT\d\d\d\d-\d\d-\d\d-\d\d-\d\d-\d\d-\d/).test( d[4] ) || ($('#checkboxFilterSK').prop('checked') && d[4].startsWith('SK')) || (/RT\d\d\d \d\d \d\d \d\d \d\d \d\d \d/).test( d[4] )) d.selected = true; else d.selected = false})
        data.forEach(d => { (/RT\d\d\d-\d\d-\d\d-\d\d-\d\d-\d\d-\d/).test( d[4] ) || (/RT\d\d\d\d-\d\d-\d\d-\d\d-\d\d-\d\d-\d/).test( d[4] ) || (/RT\d\d\d-\d\d-\d\d-\d\d-\d\d-\d\d/).test( d[4] ) || (/RT\d\d\d\d-\d\d-\d\d-\d\d-\d\d-\d\d/).test( d[4] ) || ($('#checkboxFilterSK').prop('checked') && d[4].startsWith('SK')) || (/RT\d\d\d \d\d \d\d \d\d \d\d \d\d \d/).test( d[4] ) || (/RT\d\d\d \d\d \d\d \d\d \d\d \d\d/).test( d[4] ) ? d.selected = true : d.selected = false; })
        //(/RT\d\d\d-\d\d-\d\d-\d\d-\d\d-\d\d-\d/).test( 'RT049-01-00-00-00-00-0' )
        /*
        if (materialCheck && !supplierCheck && !screwCheck){ data = data.filter(d => d[4].startsWith('RT')); }
        else if (materialCheck && supplierCheck && !screwCheck){ data = data.filter(d => !(d[4].includes('DIN') || d[4].includes('ISO'))); }
        else if (materialCheck && !supplierCheck && screwCheck){ data = data.filter(d => d[4].startsWith('RT') || d[4].includes('DIN') || d[4].includes('ISO')); }
        else if (!materialCheck && supplierCheck && screwCheck){ data = data.filter(d => !d[4].startsWith('RT')); }
        else if (!materialCheck && supplierCheck && !screwCheck){ data = data.filter(d => !(d[4].startsWith('RT') || d[4].includes('DIN') || d[4].includes('ISO'))); }
        else if (!materialCheck && !supplierCheck && screwCheck){ data = data.filter(d => d[4].includes('DIN') || d[4].includes('ISO')); }
        else if (!materialCheck && !supplierCheck && !screwCheck){ data = []; }//same as no build parts
        else {
          //no filter, preview all build parts
        }
        */
        // if (materialCheck && !supplierCheck && !screwCheck){ data.forEach(d => { if (d[4].startsWith('RT')) d.selected = true; else d.selected = false; }) }
        // else if (materialCheck && supplierCheck && !screwCheck){ data.forEach(d => { if (!(d[4].includes('DIN') || d[4].includes('ISO'))) d.selected = true; else d.selected = false; }) }
        // else if (materialCheck && !supplierCheck && screwCheck){ data.forEach(d => { if (d[4].startsWith('RT') || d[4].includes('DIN') || d.includes('ISO')) d.selected = true; else d.selected = false; }) }
        // else if (!materialCheck && supplierCheck && screwCheck){ data.forEach(d => { if (!d[4].startsWith('RT')) d.selected = true; else d.selected = false; }) }
        // else if (!materialCheck && supplierCheck && !screwCheck){ data.forEach(d => { if (!(d[4].startsWith('RT') || d[4].includes('DIN') || d[4].includes('ISO'))) d.selected = true; else d.selected = false; }) }
        // else if (!materialCheck && !supplierCheck && screwCheck){ data.forEach(d => { if (d[4].includes('DIN') || d[4].includes('ISO')) d.selected = true; else d.selected = false; }) }
        // else if (!materialCheck && !supplierCheck && !screwCheck){ data = []; }//same as no build parts
        // else {
        //   //no filter, preview all build parts
        //   { data.forEach(d => { d.selected = true;}) }
        // }
        //add every SK build part - stands for standard build part and can be on many projects multiple times even on same project
        // if ($('#checkboxFilterSK').prop('checked')){
        //   data.forEach(d => {if (d[4].startsWith('SK')) d.selected = true});
        // }
        //parse data from csv to build parts for data base
        for(var i = 0 ; i < data.length; i++){
          if (data[i].length > 6)
            newBuildParts.unshift({position:data[i][0], name: data[i][1], quantity: data[i][2], material: data[i][3], standard: data[i][4], note: data[i][5], tag: data[i][6], selected: data[i].selected});
          else
            newBuildParts.unshift({position:data[i][0], name: data[i][1], quantity: data[i][2], material: data[i][3], standard: data[i][4], note: data[i][5], tag: '', selected: data[i].selected});
        }
        debugger;
        buildPartsPreview = {newBuildParts:newBuildParts, taskId:null, buildParts:[], fileName: results.fileName};//null for taskId means its for new task
        buildPartsPreviewArray.push({newBuildParts:newBuildParts, taskId:null, buildParts:[], fileName: results.fileName});
        //check if filter for existing build parts is on then get data and filter them out
        if($('#checkboxFilterBP').prop('checked')){
          filterExistingBuildParts();
        }
        else{
          //make modal of build parts and open it
          //buildBPPreview();
          bpIndex = 0;
          buildBPArrayPreview();
        }
      }
      else{
        //kosovnica nima naslovne vrstice, najverjetne gre za neko drugo vrsto csv datoteke --> napaka
        debugger;
        alert('Datoteka csv ni v pravilni obliki kosovnice.');
      }
    }
  }
}
function filterExistingBuildParts() {
  debugger;
  let data = {projectId};
  $.ajax({
    type: 'GET',
    url: '/buildparts/project',
    contentType: 'application/x-www-form-urlencoded',
    data: data, // access in body
  }).done(function (resp) {
    debugger;
    if (resp.success){
      allFilteringBuildParts = resp.allBuildParts;
      allFilteringBuildParts.forEach(bp => {
        var tmp = buildPartsPreview.newBuildParts.find(nbp => nbp.standard == bp.standard)
        if(tmp)
          tmp.selected = false;
      })
      debugger;
      //buildBPPreview();
      bpIndex = 0;
      buildBPArrayPreview();
    }
    else{
      $('#modalError').modal('toggle');  
    }
  }).fail(function (resp) {
    $('#modalError').modal('toggle');
  }).always(function (resp) {
  });
}
function buildBPPreview(){
  $('#newBuildPartTableBody').empty();
  for(var i = 0, l = buildPartsPreview.newBuildParts.length; i < l; i++){
    var selected = buildPartsPreview.newBuildParts[i].selected ? 'checked=""' : '';
    var element = `<tr id="newBP` + i + `" class="">
      <td>
        <div class="form-group ml-n2 mr-n2 mb-n2">
          <div class="custom-control custom-checkbox table-custom-checkbox">
            <input class="custom-control-input" id="newBPSelected`+i+`" type="checkbox" `+selected+`>
            <label class="custom-control-label" for="newBPSelected`+i+`"></label>
          </div>
        </div>
      </td>
      <td>
        <div class="form-group ml-n2 mr-n2 mb-n2">
          <input class="form-control" id="newBPPosition` + i + `" type="number" name="newBPPosition` + i + `" required="" value="`+buildPartsPreview.newBuildParts[i].position+`">
        </div>
      </td>
      <td>
        <div class="form-group ml-n2 mr-n2 mb-n2">
          <input class="form-control" id="newBPName` + i + `" type="text" name="newBPName` + i + `" required="" value="`+buildPartsPreview.newBuildParts[i].name+`">
        </div>
      </td>
      <td>
        <div class="form-group ml-n2 mr-n2 mb-n2">
          <input class="form-control" id="newBPQuantity` + i + `" type="text" name="newBPQuantity` + i + `" required="" value="`+buildPartsPreview.newBuildParts[i].quantity+`">
        </div>
      </td>
      <td>
        <div class="form-group ml-n2 mr-n2 mb-n2">
          <input class="form-control" id="newBPMaterial` + i + `" type="text" name="newBPMaterial` + i + `" required="" value="`+buildPartsPreview.newBuildParts[i].material+`">
        </div>
      </td>
      <td>
        <div class="form-group ml-n2 mr-n2 mb-n2">
          <input class="form-control" id="newBPStandard` + i + `" type="text" name="newBPStandard` + i + `" required="" value="`+buildPartsPreview.newBuildParts[i].standard+`">
        </div>
      </td>
      <td>
        <div class="form-group ml-n2 mr-n2 mb-n2">
          <input class="form-control" id="newBPNote` + i + `" type="text" name="newBPNote` + i + `" required="" value="`+buildPartsPreview.newBuildParts[i].note+`">
        </div>
      </td>
      <td>
        <div class="form-group ml-n2 mr-n2 mb-n2">
          <input class="form-control" id="newBPTag` + i + `" type="text" name="newBPTag` + i + `" required="" value="`+buildPartsPreview.newBuildParts[i].tag+`">
        </div>
      </td>
    </tr>`;
    $('#newBuildPartTableBody').prepend(element);
    counterBPPreview = i;
  }
  $('#newBPPosition').val(parseInt($('#newBPPosition'+counterBPPreview).val())+1)
  counterBPPreview++;
  setTimeout(() => {
    
    $('#modalCnfmBuildPartsWithNewTask').modal('toggle');
  }, 900);
}
function buildPartsPreviewConf(){
  //create object of build parts of only selected parts from the list, need to read values from table because values might have changed
  buildPartsPreview.buildParts = [];
  for(var i = 0, l = buildPartsPreview.newBuildParts.length; i < l; i++){
    if($('#newBPSelected'+i).prop('checked')){//build part is checked
      buildPartsPreview.buildParts.push({
        position:$('#newBPPosition'+i).val(),
        name: $('#newBPName'+i).val(),
        quantity: $('#newBPQuantity'+i).val(),
        material: $('#newBPMaterial'+i).val(),
        standard: $('#newBPStandard'+i).val(),
        note: $('#newBPNote'+i).val(),
        tag: $('#newBPTag'+i).val(),
      });
    }
  }
  if(!buildPartsPreview.taskId){//new task
    debugger;
    $('#modalCnfmBuildPartsWithNewTask').modal('toggle');
    addNewTaskStart(0);
  }
  else{//new build parts through build parts collapse, task already exists
    debugger;
    $('#modalCnfmBuildPartsWithNewTask').modal('toggle');
    addNewBuildPartsFromCSV();
  }
}
// NEW METHOD FOR ARRAY OF BUILD PARTS
function buildBPArrayPreview(){
  // for (let bpIndex = 0; bpIndex < buildPartsPreviewArray.length; bpIndex++) {
  //   const element = buildPartsPreviewArray[bpIndex];
  //   firstBuildPartPreview = bpIndex == 1 ? true : false;
  //   buildPartsPreview = buildPartsPreviewArray[bpIndex];
  //   debugger;
  //   buildBPPreview();
    
  // }
  // add names and build standards to build parts array
  if(bpIndex == 0){
    for (let i = 0; i < buildPartsPreviewArray.length; i++) {
      const bp = buildPartsPreviewArray[i];
      bp.parentName = $('#buildPartParentAdd' + i).val();
      bp.parentStandard = $('#buildPartParentNumberAdd' + i).val();
      // if(buildPartsPreviewArray[0].taskId){
      //   bp.parentName = $('#buildPartParentAdd' + i).val();
      //   bp.parentStandard = $('#buildPartParentNumberAdd' + i).val();
      // }
      // else{
  
      // }
    }
  }
  if (buildPartsPreviewArray.length > bpIndex){
    firstBuildPartPreview = bpIndex == 0 ? true : false;
    buildPartsPreview = buildPartsPreviewArray[bpIndex];
    debugger;
    buildBPPreview();
  }
  // if(buildPartsPreviewArray.length == bpIndex){
  //   $('#newTaskBPFileNames').empty();
  //   $('#newBPFileNames').empty();
  // }
}
var buildPartsPreview;
var counterBPPreview = 0;
var allFilteringBuildParts = [];
let firstBuildPartPreview = true;
let buildPartParentIndex = 0;