//IN THIS SCRIPT//
//// adding note to task, checbox of task, fixing percentage of task/project, adding/editing/deleting task, showing conflicts, form control for tasks ////

$(function(){
  $('#taskNameAdd').on('input',function(e){
    if ($('#taskNameAdd').val() == '')
      $('#taskNameAdd').addClass('is-invalid');
    else
      $('#taskNameAdd').removeClass('is-invalid');
  });
  $('#taskStartAdd').on('change', onDatesAddChange);
  $('#taskFinishAdd').on('change', onDatesAddChange);
  $('#btnDeleteTaskConf').on('click', deleteTaskConfirm);
  $('#addTaskBtnOR').on('click', function (event) {
    addNewTask(1);
  });
  $('#editTaskBtnOR').on('click', function (event) {
    updateEditTask(1);
  });
  $('#btnTaskAdd').on('click', function (event) {
    addNewTask(0);
  });
  $('#collapseAddTask').find('.close').on('click', closeCollapseAddTask);
  $('#taskAssignmentAdd').on('change', function () {
    // console.log('task select change');
    if($('#taskAssignmentAdd').val().filter(i => i == '-1').length == 1){
      // select all
      $("#taskAssignmentAdd > option").prop("selected", "selected");
      let tmp = $("#taskAssignmentAdd").val().filter(i => i != '-1');
      $("#taskAssignmentAdd").val(tmp).trigger("change");
    }
  });
})
function addTaskNote(id){
  $('#taskNoteInput').val("");
  $('#taskIdNote').val(id);
  $("#modalTaskNoteForm").modal();
  debugger;
}
function toggleTaskCheckbox(element){
  debugger;
  var taskId = parseInt(this.id.substring(15));
  var completion = this.checked ? 100 : 0;
  var oldCompletion = this.checked ? 0 : 100;
  fixTaskCompletion(taskId, completion,oldCompletion);
  debugger;
}
//FIX COMPLETION OF THE PROJECT, task percentage has changed
function fixCompletion(){
  $.post('completion', {projectId}, function(resp){
    if (resp.success == true)
      console.log("Successfully updated project percentage." );
    else if (resp.success == false)
      console.log("Unsuccessfully updated project percentage, there was no projectId" );
  })
}
//FIX Task percentage
function fixTaskCompletion(taskId, completion, oldCompletion){
  $.post('/projects/task/completion', {taskId, completion}, function(resp){
    //fix subtask count
    if (allTasks){
      var task = allTasks.find(t=>t.id == taskId);
      task.completion = completion;
    }
    //fix task display
    //$('#taskProgress'+taskId).width(completion+"%");
    $('#taskProgress'+taskId).removeClass();
    $('#taskProgress'+taskId).addClass('progress-bar w-'+completion);
    $('#taskDate'+taskId).removeClass('text-danger');
    var date = $('#taskDate'+taskId).html();
    if (date){
      date = date.split(' - ');
      if (date[1] != '/'){
        date = date[1].split('.');
        //var d = date[0]; var m = date[1]; var y = date[2];
        if (new Date() > new Date(date[2],date[1],date[0],'15') && completion < 100)
          $('#taskDate'+taskId).addClass('text-danger');
      }
    }
    if (completion == 100 && oldCompletion != completion)
      addChangeSystem(4,2,projectId,taskId);
    else
      if (oldCompletion == 100)
        addChangeSystem(5,2,projectId,taskId);
    //fix procentage of this project overall (so it can be correct value in list of all projects)
    if (completion == 100)
      fixCompletion();
  })
}
//ADD FINISHED DATE WHEN TASK WAS GIVEN 100 COMPLETION
function addFinishedDate(taskId){
  $.post('task/finished', {taskId}, function(resp){
    debugger;
  })
}
//OPEN EDIT TASK COLLAPSE
function editTask(taskId){
  //debugger;
  if (!allCategories || !allPriorities || !allWorkers){
    $.get( "/projects/workers", {projectId}, function( data ) {
      allWorkers = data.data;
      $.get( "/projects/categories", {projectId}, function( data ) {
        allCategories = data.data;
        $.get( "/projects/priorities", {projectId}, function( data ) {
          allPriorities = data.data;
          if (taskLoaded){
            makeEditTask();
            $('#collapseEditTask'+taskId).collapse("toggle");
            editTaskCollapseOpen = true;
          }
          else{
            $.get( "/projects/tasks", {projectId,categoryTask,activeTask, completionTask}, function( data ) {
              //console.log(data)
              //debugger
              allTasks = data.data;
              taskLoaded = true;
              makeEditTask();
              $('#collapseEditTask'+taskId).collapse("toggle");
              editTaskCollapseOpen = true;
            });
          }
        });
      });  
    });
  }
  else{
    if (taskLoaded){
      makeEditTask();
      $('#collapseEditTask'+taskId).collapse("toggle");
      editTaskCollapseOpen = true;
    }
    else{
      $.get( "/projects/tasks", {projectId,categoryTask,activeTask, completionTask}, function( data ) {
        //console.log(data)
        //debugger
        allTasks = data.data;
        taskLoaded = true;
        makeEditTask();
        $('#collapseEditTask'+taskId).collapse("toggle");
        editTaskCollapseOpen = true;
      });
    }
  }
}
//FILL EDIT TASK DATA (form)
function makeEditTask(){
  //debugger;
  var editTaskActivity = ''; 
  if (loggedUser.role == 'admin')
    editTaskActivity = '<div class="d-flex justify-content-center mb-2"><div class="custom-control custom-checkbox"><input class="custom-control-input" id="taskActiveEdit" type="checkbox" checked="" /><label class="custom-control-label" for="taskActiveEdit">Opravilo je aktivno</label></div></div>';
  var collapseElement = `<hr />
  <div class="form-row">
    <div class="col-md-6">
      <div class="form-group">
        <label class="w-100" for="taskNameEdit">Ime opravila
          <input class="form-control" id="taskNameEdit" type="text" name="taskNameEdit" required="" />
            <div class="invalid-feedback">Za dodajanje novega opravila je potrebno vnesti vsaj ime opravila!</div>
        </label>
      </div>
    </div>
    <div class="col-md-6">
      <div class="form-group">
        <label class="w-100" for="taskNoteEdit">Komentar
          <input class="form-control" id="taskNoteEdit" type="text" name="taskNoteEdit" />
        </label>
      </div>
    </div>
    <div class="col-md-6 d-none">
      <div class="form-group">
        <label class="w-100" for="taskDurationEdit>Število dni
          <input class="form-control" id="taskDurationEdit" type="number" name="taskDurationEdit" min="0" disabled placeholder="Se izračuna samo po vnosu obeh datumov."/>
        </label>
      </div>
    </div>
  </div>
  <div class="form-row">
    <div class="col-md-6">
      <div class="form-group">
        <label class="w-100" for="taskStartEdit">Začetek opravila
          <input class="form-control" id="taskStartEdit" type="date" name="taskStartEdit" />
            <div class="valid-feedback">Začetek opravila je znotraj datuma projekta.</div>
            <div id="taskStartEditInvalidFeedback" class="invalid-feedback">Začetek opravila je izven datuma projekta!</div>
        </label>
      </div>
    </div>
    <div class="col-md-6">
      <div class="form-group">
        <label class="w-100" for="taskFinishEdit">Konec opravila
          <input class="form-control" id="taskFinishEdit" type="date" name="taskFinishEdit" />
            <div class="valid-feedback">Konec opravila je znotraj datuma projekta.</div>
            <div id="taskFinishEditInvalidFeedback" class="invalid-feedback">Konec opravila je izven datuma projekta!</div>
        </label>
      </div>
    </div>
  </div>
  <div class="form-row">
    <div class="col-md-6">
      <div class="form-group">
        <label class="w-100" for="taskCompletionEdit">Dokončano
          <input class="form-control" id="taskCompletionEdit" type="number" name="taskCompletionEdit" min="0" max="100" />
        </label>
      </div>
    </div>
    <div class="col-md-6">
      <div class="form-group">
        <label class="w-100" for="taskAssignmentEdit">Dodeli opravilo
          <select class="custom-select mr-sm-2 form-control select2-workersEdit w-100" id="taskAssignmentEdit" name="worker"></select>
        </label>
      </div>
    </div>
  </div>
  <div class="form-row">
    <div class="col-md-6">
      <div class="form-group">
        <label class="w-100" for="taskCategoryEdit">Kategorija
          <select class="custom-select mr-sm-2 form-control select2-taskCategoryEdit w-100" id="taskCategoryEdit" name="worker"></select>
        </label>
      </div>
    </div>
    <div class="col-md-6">
      <div class="form-group">
        <label class="w-100" for="taskPriorityEdit">Prioriteta
          <select class="custom-select mr-sm-2 form-control select2-taskPriorityEdit w-100" id="taskPriorityEdit" name="worker"></select>
        </label>
      </div>
    </div>
  </div>
  <div class="d-flex justify-content-center mb-2 mt-n2">
    <div class="custom-control custom-checkbox">
      <input class="custom-control-input" id="taskWeekendEdit" type="checkbox" />
      <label class="custom-control-label" for="taskWeekendEdit">Vikend</label>
    </div>
  </div>
  `+editTaskActivity+`
  <div class="text-center">
    <button id="btnTaskEdit" class="btn btn-success">Posodobi</button>
  </div>`;
  $('#collapseEditTask'+activeTaskId).append(collapseElement);
  $('#btnTaskEdit').on('click', {or:0}, onEventUpdateEditTask);
  $('#taskStartEdit').on('change', onDatesEditChange);
  $('#taskFinishEdit').on('change', onDatesEditChange);
  $('#taskAssignmentEdit').on('change', function() {
    if($('#taskAssignmentEdit').val().filter(i => i == '-1').length == 1){
      // select all
      $("#taskAssignmentEdit > option").prop("selected", "selected");
      let tmp = $("#taskAssignmentEdit").val().filter(i => i != '-1');
      $("#taskAssignmentEdit").val(tmp).trigger("change");
    }
  })
  $('#taskNameEdit').on('input',function(e){
    if ($('#taskNameEdit').val() == '')
      $('#taskNameEdit').addClass('is-invalid');
    else
      $('#taskNameEdit').removeClass('is-invalid');
  });
  $(".select2-workersEdit").prepend('<option value="-1">Vsi</option>').select2({
    data: allWorkers,
    tags: false,
    multiple: true,
  });
  $(".select2-taskCategoryEdit").select2({
    data: allCategories,
    tags: false,
  });
  $(".select2-taskPriorityEdit").select2({
    data: allPriorities,
    tags: false,
  });  
  //$('#project_notes').val(project.notes);
  var x = allTasks.find(t => t.id == activeTaskId);
  $('#taskNameEdit').val(x.task_name);
  $('#taskNoteEdit').val(x.task_note);
  $('#taskDurationEdit').val(x.task_duration);
  if (x.task_start){
    $('#taskStartEdit').val(new Date(x.task_start).toISOString().split("T")[0]);
    onDatesEditChange();
  }
  if (x.task_finish){
    $('#taskFinishEdit').val(new Date(x.task_finish).toISOString().split("T")[0]);
    onDatesEditChange();
  }
  $('#taskCompletionEdit').val(x.completion);
  if (x.workers)
    $('#taskAssignmentEdit').val(x.workers_id.split(',')).trigger('change');
  var c = allCategories.find(c => c.text == x.category);
  var p = allPriorities.find(p => p.text == x.priority);
  $('#taskCategoryEdit').val(c.id).trigger('change');
  if ((loggedUser.role == 'konstrukter' || loggedUser.role == 'konstruktor' || (loggedUser.projectRole && loggedUser.projectRole == 'konstrukter')) && c.text == 'Strojna izdelava' && !leader)
    $('#taskCategoryEdit').prop('disabled', true);
  else
    $('#taskCategoryEdit').prop('disabled', false);
  $('#taskPriorityEdit').val(p.id).trigger('change');
  //activity of task
  if (x.active == true)
    $('#taskActiveEdit').prop("checked", true);
  else if (x.active == false){
    $('#taskActiveEdit').prop("checked", false);
    $('#collapseEditTask'+activeTaskId).removeClass('task-collapse');
    $('#collapseEditTask'+activeTaskId).addClass('task-collapse-deleted');
  }
  if (x.weekend == true)
    $('#taskWeekendEdit').prop("checked", true);
  else
    $('#taskWeekendEdit').prop("checked", false);
}
function addNewTask(or){
  debugger;
  if (!collapseAddNewTaskOpen){
    if (!allCategories || !allPriorities || !allWorkers){
      $.get( "/projects/workers", {projectId}, function( data ) {
        allWorkers = data.data;
        $.get( "/projects/categories", {projectId}, function( data ) {
          allCategories = data.data;
          $.get( "/projects/priorities", {projectId}, function( data ) {
            allPriorities = data.data;
            $(".select2-workersAdd").prepend('<option value="-1">Vsi</option>').select2({
              data: allWorkers,
              tags: false,
              multiple: true,
            });
            $(".select2-workersAdd").val(0).trigger("change");;

            $(".select2-taskCategoryAdd").select2({
              data: allCategories,
              tags: false,
            });
            $(".select2-taskPriorityAdd").select2({
              data: allPriorities,
              tags: false,
            });  
            $('#collapseAddTask').collapse("toggle");
            collapseAddNewTaskOpen = true;
            if (!taskLoaded){
              $.get( "/projects/tasks", {projectId,categoryTask,activeTask, completionTask}, function( data ) {
                //console.log(data)
                //debugger
                allTasks = data.data;
                taskLoaded = true;
              });
            }
            if(leader == false && (loggedUser.role == 'konstrukter' || loggedUser.role == 'konstruktor' || (loggedUser.projectRole && loggedUser.projectRole == 'konstrukter'))){
              //user is konstukter and is not a leader of project -> change category to strojna izdelava and disable user changing the category
              $('#taskCategoryAdd').val(4).trigger('change');
              $('#taskCategoryAdd').prop('disabled', true);
            }
          });
        });  
      });
    }
    else{
      debugger;
      $(".select2-workersAdd").select2({
        data: allWorkers,
        tags: false,
        multiple: true,
      });
      $(".select2-taskCategoryAdd").select2({
        data: allCategories,
        tags: false,
      });
      $(".select2-taskPriorityAdd").select2({
        data: allPriorities,
        tags: false,
      });
      $('#collapseAddTask').collapse("toggle");
      collapseAddNewTaskOpen = true;
      
      if(leader == false && (loggedUser.role == 'konstrukter' || loggedUser.role == 'konstruktor' || (loggedUser.projectRole && loggedUser.projectRole == 'konstrukter'))){
        //user is konstukter and is not a leader of project -> change category to strojna izdelava and disable user changing the category
        $('#taskCategoryAdd').val(4).trigger('change');
        $('#taskCategoryAdd').prop('disabled', true);
      }
    }
  }
  else{
    //debugger;
    //first check if there are any empty fields that are not allowed
    if ($('#taskNameAdd').val() == '')
      $('#taskNameAdd').addClass("is-invalid");
    else{
      if($('#taskCategoryAdd').val() == 4 && $('#newBuildPartCsv').val() != ''){
        createPreviewBuildParts(allResults);
        // if($('#taskBuildPartParentAdd').val() == '' || $('#taskBuildPartParentNumberAdd').val() == ''){
        //   $('#taskBuildPartParentAdd').addClass("is-invalid");
        //   $('#taskBuildPartParentNumberAdd').addClass("is-invalid");
        // }
        // else{
        //   //parseCsvToNewTaskWithBuildParts();
        //   createPreviewBuildParts(allResults);
        // }
      }
      else{
        addNewTaskStart(or);
      }
    }
  }
}
function addNewTaskStart(or){
  var override = or;
  //otherwise add and while adding check if there are conflicts
  var taskName = $('#taskNameAdd').val();
  var taskNote = $('#taskNoteAdd').val();
  var taskDuration = $('#taskDurationAdd').val();
  var taskStart = $('#taskStartAdd').val();
  var taskFinish = $('#taskFinishAdd').val();
  var taskCompletion = $('#taskCompletionAdd').val();
  var taskAssignment = $('#taskAssignmentAdd').val();
  taskAssignment = taskAssignment.toString();
  var taskCategory = $('#taskCategoryAdd').val();
  var taskPriority = $('#taskPriorityAdd').val();
  var taskWeekend = false;
  if (typeof $('#taskWeekendAdd').prop('checked') === 'undefined') 
    taskWeekend = true
  else
    taskWeekend = $('#taskWeekendAdd').prop('checked');
  if (!taskDuration)
    taskDuration = 0;
  if (!taskCompletion)
    taskCompletion = 0;
  else
    taskCompletion = parseInt(taskCompletion);
  if (taskStart)
    taskStart = taskStart + " 07:00:00.000000";
  if (taskFinish)
    taskFinish = taskFinish + " 15:00:00.000000";
  //build parts if task category is strojna izdelava in has csv input file
  var buildParts = [];
  let parentName = '', parentStandard = '';
  if(taskCategory == 4 && $('#newBuildPartCsv').val() != ''){
    buildParts = buildPartsPreview.buildParts;
    // parentName = $('#taskBuildPartParentAdd').val();
    // parentStandard = $('#taskBuildPartParentNumberAdd').val();
    // parentName = $('#taskBuildPartParentAdd').val();
    // parentStandard = buildPartsPreview.fileName;
    parentName = buildPartsPreview.parentName;
    parentStandard = buildPartsPreview.parentStandard;
  }
  buildParts = JSON.stringify(buildParts);
  debugger;
  $.post('/projects/addtask', {projectId, taskName, taskNote, taskDuration, taskStart, taskFinish, taskCompletion, taskAssignment, taskCategory, taskPriority, taskWeekend, override, buildParts, parentName, parentStandard}, function(resp){

    debugger;
    if (resp.conflicts){
      openConflictModal(resp.conflicts);
      //console.log(resp.conflicts);
    }
    if (override == 1){
      $('#modalConflicts').modal('toggle');
    }
    if (resp.success == true){
      $('#collapseAddTask').collapse("toggle");
      collapseAddNewTaskOpen = false;
      //categoryPriority should be created with loop or through database but since its always the same (for) i did in this way
      var categoryPriority = [{id:allCategories[1].id, text:allCategories[1].text},
      {id:allCategories[2].id, text:allCategories[2].text},
      {id:allCategories[3].id, text:allCategories[3].text},
      {id:allCategories[4].id, text:allCategories[4].text},
      {id:allCategories[5].id, text:allCategories[5].text},
      {id:allCategories[6].id, text:allCategories[6].text},
      {id:allCategories[7].id, text:allCategories[7].text},
      {id:allCategories[8].id, text:allCategories[8].text},
      {id:allCategories[9].id, text:allCategories[9].text},
      {id:allCategories[10].id, text:allCategories[10].text},
      {id:allCategories[11].id, text:allCategories[11].text},
      {id:allCategories[12].id, text:allCategories[12].text},
      {id:allCategories[13].id, text:allCategories[13].text},
      {id:allCategories[14].id, text:allCategories[14].text},
      {id:allCategories[15].id, text:allCategories[15].text},
      {id:allCategories[16].id, text:allCategories[16].text},
      {id:18, text:allCategories[0].text}];
      debugger;
      //put new task in "correct" index of allTasks
      var testTaskCategory = categoryPriority.find(cp => cp.text == allCategories.find(c => c.id == taskCategory).text).id;
      var st = 0;
      for(var i = 0; i < allTasks.length; i++){
        if (categoryPriority.find(c => c.text == allTasks[i].category).id < testTaskCategory){
          //not in right category yet, first need to get to correct category
          st++;
          debugger;
        }
        else if (categoryPriority.find(c => c.text == allTasks[i].category).id == testTaskCategory){
          //correct category, move so long as long there is date or no more dates
          if (allTasks[i].task_start && taskStart && (new Date(allTasks[i].task_start) <= new Date(taskStart)))
            st++;
          //if new task has no start put it after tasks with start
          if (allTasks[i].task_start && !taskStart)
            st++;
          debugger;
        }
        debugger;
      }
      debugger;
      //time
      var taskStartFormat = null;
      if (taskStart){
        taskStart = new Date(taskStart).toISOString();
        var sdate = taskStart.split('T')[0];
        var stime = taskStart.split('T')[1];
        taskStartFormat = {date:sdate.split('-')[2]+"."+sdate.split('-')[1]+"."+sdate.split('-')[0],
        time:stime.substring(0,5)};
      }
      else
        taskStart = null;
      var taskFinishFormat = null;
      if (taskFinish){
        taskFinish = new Date(taskFinish).toISOString();
        var fdate = taskFinish.split('T')[0];
        var ftime = taskFinish.split('T')[1];
        taskFinishFormat = {date:fdate.split('-')[2]+"."+fdate.split('-')[1]+"."+fdate.split('-')[0],
        time:ftime.substring(0,5)};
      }
      else
        taskFinish = null;
      if (!taskDuration)
        taskDuration = 0;
      //workers
      var taskWorkers = "";
      for(var i = 0; i < $('#taskAssignmentAdd').val().length; i++){
        if (i != 0)
          taskWorkers += ", ";
        taskWorkers += allWorkers.find(w => w.id == parseInt($('#taskAssignmentAdd').val()[i])).text;
      }
      if (!taskWorkers){
        taskWorkers = null;
        taskAssignment = null;
      }
      var buildPartCount = (buildPartsPreview && buildPartsPreview.buildParts) ? buildPartsPreview.buildParts.length : 0;
      var newTask = {id:resp.newTaskId, task_name: taskName, task_note: taskNote, task_duration: taskDuration, 
      task_start:taskStart, task_finish: taskFinish, active:true, 
      category:categoryPriority.find(c => c.id == testTaskCategory).text, completion:taskCompletion, 
      files_count:null, finished:null, 
      formatted_start:taskStartFormat, 
      formatted_finish:taskFinishFormat, 
      full_absence_count:null, task_absence_count:null, priority:allPriorities.find(p => p.id == taskPriority).text, 
      subtask_count:null, 
      build_parts_count:buildPartCount,
      workers: taskWorkers, workers_id: taskAssignment, weekend:taskWeekend, author: (loggedUser.name+' '+loggedUser.surname), author_id:null};
      //fit new task in i place in allTasks
      allTasks.splice(st,0,newTask);
      debugger;
      var myCurrentPage = activePage;
      var pageCounter = Math.ceil(allTasks.length/10);
      drawPagination(pageCounter);
      debugger;
      activePage = 1
      if (st == 0) st++;
      else if (st % 10 == 0){
        st++;
      }
      myCurrentPage = Math.ceil(st/10);
      drawTasks(myCurrentPage);
      debugger;
      $('#taskNameAdd').val("");
      $('#taskNoteAdd').val("");
      $('#taskDurationAdd').val("");
      $('#taskStartAdd').val("");
      $('#taskStartAdd').removeClass('is-valid');
      $('#taskStartAdd').removeClass('is-invalid');
      $('#taskFinishAdd').val("");
      $('#taskFinishAdd').removeClass('is-valid');
      $('#taskFinishAdd').removeClass('is-invalid');
      $('#taskCompletionAdd').val("0");
      $('#taskAssignmentAdd').val(0).trigger('change');
      $('#taskCategoryAdd').val(1).trigger('change');
      $('#taskPriorityAdd').val(1).trigger('change');
      if(buildPartsPreview && buildPartsPreview.buildParts){
        setTimeout(()=>{
          smoothScroll('#task'+resp.newTaskId, 100); flashAnimation('#task'+resp.newTaskId);
          toggleCollapse(resp.newTaskId, 4);
          if(++bpIndex != buildPartsPreviewArray.length){
            //first add new task id to other buildparts so they will be added to created one and not created new tasks for every build part file
            for(let i = 0; i < buildPartsPreviewArray.length; i++){
              buildPartsPreviewArray[i].taskId = resp.newTaskId;
            }
            buildBPArrayPreview();
          }
        })
        buildPartsPreview.buildParts = [];
        $('#taskBuildPartParentAdd').removeClass("is-invalid");
        $('#taskBuildPartParentNumberAdd').removeClass("is-invalid");
        $('#taskBuildPartParentAdd').val("");
        $('#taskBuildPartParentNumberAdd').val("");
        $('#newBuildPartCsvLabel').html('');
        $('#newBuildPartCsv').val('');
        $('#newTaskBPFileNames').empty();
      }
      else{
        setTimeout(()=>{
          smoothScroll('#task'+resp.newTaskId, 100); flashAnimation('#task'+resp.newTaskId);
        })
      }
      if (timelineCollapseOpen)
        refreshTimeline();
      //ADDING NEW CHANGE TO DB
      addChangeSystem(1,2,projectId,resp.newTaskId);
      //addNewProjectChange(2,1,projectId,resp.newTaskId);
      fixCompletion();
    }
    else{
      //$('#modalError').modal('toggle');
    }
  })
  debugger;
}
/*
//when data is loaded and user wants to add new task
function addTask(projectId, taskName, taskDuration, taskStart, taskFinish, taskAssignment, taskCategory, taskPriority, override){
  //taskAssignmentArrayString
  $.post('/projects/addtask', {projectId, taskName, taskDuration, taskStart, taskFinish, taskAssignment, taskCategory, taskPriority, override}, function(resp){
    
    debugger;
    
  })
}
*/
//open conflicts modal and fill modal with new conflicts
function openConflictModal(conflicts){
  debugger;
  $('#conflictsBody').empty();
  for(var i = 0; i < conflicts.length; i++){
    if (conflicts[i].length > 0){
      var conflictWorker = conflicts[i][0].name + " " +conflicts[i][0].surname;
      var listElements = ``;
      for(var j = 0; j < conflicts[i].length; j++){
        var linkStart = '';
        var linkEnd = '';
        var linkTask = '';
        if (conflicts[i][j].id_subscriber){
          //servis conflicts
          if (loggedUser.role.substring(0,5) == 'vodja' || loggedUser.role == 'admin'){
            linkStart = `<a href="/servis/?taskId=`+conflicts[i][j].id_task+`" class="alert-link">`;
            linkEnd = '</a>';
          }
          listElements += `<li class="list-group-item">
            <div>
              Servis `+linkStart+`<u>`+conflicts[i][j].task_name+`</u>`+linkEnd+` pri <u>`+conflicts[i][j].subscriber+`</u> v času <u>`+new Date(conflicts[i][j].task_start).toLocaleDateString('sl')+` - `+new Date(conflicts[i][j].task_finish).toLocaleDateString('sl')+`</u>.
            </div>
          </li>`;
        }
        else if (conflicts[i][j].id_project){
          //project conflicts
          if (loggedUser.role.substring(0,5) == 'vodja' || loggedUser.role == 'admin'){
            linkStart = `<a href="/projects/id?id=`+conflicts[i][j].id_project+`" class="alert-link">`;
            linkTask = `<a href="/projects/id?id=`+conflicts[i][j].id_project+`&taskId=`+conflicts[i][j].id_task+`" class="alert-link">`;
            linkEnd = '</a>';
          }
          listElements += `<li class="list-group-item">
            <div>
              `+linkStart+`<u>`+conflicts[i][j].project_name+`</u>`+linkEnd+` na opravilu `+linkTask+`<u>`+conflicts[i][j].task_name+`</u>`+linkEnd+` v času <u>`+new Date(conflicts[i][j].task_start).toLocaleDateString('sl')+` - `+new Date(conflicts[i][j].task_finish).toLocaleDateString('sl')+`</u>.
            </div>
          </li>`;
        }
        else{
          //absence
          if (loggedUser.role.substring(0,5) == 'vodja' || loggedUser.role == 'admin'){
            linkStart = `<a href="/employees/absence?id=`+conflicts[i][j].id_user+`" class="alert-link">`;
            linkEnd = '</a>';
          }
          listElements += `<li class="list-group-item">
            <div>
              `+linkStart+`<u>`+conflicts[i][j].project_name+`</u>`+linkEnd+` v času <u>`+new Date(conflicts[i][j].task_start).toLocaleDateString('sl')+` - `+new Date(conflicts[i][j].task_finish).toLocaleDateString('sl')+`</u> z nazivom: <u>`+conflicts[i][j].task_name+`</u>.
            </div>
          </li>`;
        }
      }
      var element = `<div><strong>`+conflictWorker+`:</strong></div>
      <ul class="list-group mb-2">
        `+listElements+`
      </ul>`;
      $('#conflictsBody').append(element);
    }
  }
  $('#modalConflicts').modal();
}
function closeCollapseAddTask(){
  $('#collapseAddTask').collapse("toggle");
  collapseAddNewTaskOpen = false;
}
function onEventUpdateEditTask(event) {
  updateEditTask(event.data.or);
}
//update task from edit form
function updateEditTask(or){
  //debugger;
  //first check if there are any empty fields that are not allowed
  if ($('#taskNameEdit').val() == '')
    $('#taskNameEdit').addClass("is-invalid")
  else{
    var override = or;
    var taskId = activeTaskId;
    var currentWorkers = allTasks.find(t => t.id == taskId).workers_id;
    //otherwise add and while adding check if there are conflicts
    var taskName = $('#taskNameEdit').val();
    var taskNote = $('#taskNoteEdit').val();
    var taskDuration = $('#taskDurationEdit').val();
    var taskStart = $('#taskStartEdit').val();
    var taskFinish = $('#taskFinishEdit').val();
    var taskCompletion = $('#taskCompletionEdit').val();
    var taskAssignment = $('#taskAssignmentEdit').val();
    taskAssignment = taskAssignment.toString();
    var taskCategory = $('#taskCategoryEdit').val();
    var taskPriority = $('#taskPriorityEdit').val();
    var taskWeekend = false;
    var taskActive;
    if (typeof $('#taskActiveEdit').prop('checked') === 'undefined') 
      taskActive = true
    else
      taskActive = $('#taskActiveEdit').prop('checked');
    if (typeof $('#taskWeekendEdit').prop('checked') === 'undefined') 
      taskWeekend = true
    else
      taskWeekend = $('#taskWeekendEdit').prop('checked');
    if (!taskDuration)
      taskDuration = 0;
    if (!taskCompletion)
      taskCompletion = 0;
    else
      taskCompletion = parseInt(taskCompletion);
    if (taskStart)
      taskStart = taskStart + " 07:00:00.000000";
    if (taskFinish)
      taskFinish = taskFinish + " 15:00:00.000000";
    debugger;
    $.post('/projects/updatetask', {projectId,taskId, taskName, taskNote, taskDuration, taskStart, taskFinish, taskCompletion, taskAssignment, currentWorkers, taskCategory, taskPriority, taskActive, taskWeekend, override}, function(resp){

      debugger;
      if (resp.conflicts){
        openConflictEditModal(resp.conflicts);
        //console.log(resp.conflicts);
      }
      if (override == 1){
        $('#modalConflictsEdit').modal('toggle');
      }
      if (resp.success == true){
        //update task in allTask && task on page
        var x = allTasks.find(t => t.id == taskId);
        //name
        x.task_name = taskName;
        x.task_note = taskNote ? taskNote : null;
        x.weekend = taskWeekend;
        $('#taskName'+taskId).html(taskName);
        $('#taskNote'+taskId).html(taskNote);
        //date
        var dateString = "";
        var taskStartFormat = null;
        if (taskStart){
          taskStart = new Date(taskStart).toISOString();
          var sdate = taskStart.split('T')[0];
          var stime = taskStart.split('T')[1];
          taskStartFormat = {date:sdate.split('-')[2]+"."+sdate.split('-')[1]+"."+sdate.split('-')[0],
          time:stime.substring(0,5)};
          x.task_start = taskStart;
          x.formatted_start = taskStartFormat;
          dateString = taskStartFormat.date + " - ";
        }
        else{
          taskStart = null;
          x.task_start = taskStart;
          x.formatted_start = taskStartFormat;
          dateString = "/ - ";
        }
        var taskFinishFormat = null;
        var dateText = '';
        if (taskFinish){
          if (new Date() > new Date(taskFinish) && taskCompletion < 100)
            dateText = 'text-danger';
          taskFinish = new Date(taskFinish).toISOString();
          var fdate = taskFinish.split('T')[0];
          var ftime = taskFinish.split('T')[1];
          taskFinishFormat = {date:fdate.split('-')[2]+"."+fdate.split('-')[1]+"."+fdate.split('-')[0],
          time:ftime.substring(0,5)};
          x.task_finish = taskFinish;
          x.formatted_finish = taskFinishFormat;
          dateString += taskFinishFormat.date ;
        }
        else{
          taskFinish = null;
          x.task_finish = taskFinish;
          x.formatted_finish = taskFinishFormat;
          dateString += "/";
        }
        if (dateString == "/ - /") dateString = "";
        $('#taskDate'+taskId).html(dateString);
        $('#taskDate'+taskId).removeClass('text-danger');
        $('#taskDate'+taskId).addClass(dateText);
        //category
        debugger;
        $('#taskBadges'+taskId).empty();
        var badge = '';
        var taskBadge = '';
        if (taskCategory == "2"){
          badge = 'badge-purchase';
          taskBadge = 'Nabava';
          x.category = taskBadge;
        }
        else if (taskCategory == "3"){
          badge = 'badge-construction';
          taskBadge = 'Konstrukcija';
          x.category = taskBadge;
        }
        else if (taskCategory == "4"){
          badge = 'badge-mechanic';
          taskBadge = 'Strojna izdelava';
          x.category = taskBadge;
        }
        else if (taskCategory == "5"){
          badge = 'badge-electrican';
          taskBadge = 'Elektro izdelava';
          x.category = taskBadge;
        }
        else if (taskCategory == "6"){
          badge = 'badge-assembly';
          taskBadge = 'Montaža';
          x.category = taskBadge;
        }
        else if (taskCategory == "7"){
          badge = 'badge-programmer';
          taskBadge = 'Programiranje';
          x.category = taskBadge;
        }
        else if (taskCategory == "8"){
          badge = 'badge-mechanic';
          taskBadge = 'Strojna montaža - Roboteh';
          x.category = taskBadge;
        }
        else if (taskCategory == "9"){
          badge = 'badge-mechanic';
          taskBadge = 'Strojna montaža - stranka';
          x.category = taskBadge;
        }
        else if (taskCategory == "10"){
          badge = 'badge-electrican';
          taskBadge = 'Elektro montaža - Roboteh';
          x.category = taskBadge;
        }
        else if (taskCategory == "11"){
          badge = 'badge-electrican';
          taskBadge = 'Elektro montaža - stranka';
          x.category = taskBadge;
        }
        else if (taskCategory == "12"){
          badge = 'badge-electrican';
          taskBadge = 'Elektro proektiranje';
          x.category = taskBadge;
        }
        else if (taskCategory == "13"){
          badge = 'badge-primary';
          taskBadge = 'Dobava';
          x.category = taskBadge;
        }
        else if (taskCategory == "14"){
          badge = 'badge-primary';
          taskBadge = 'SAT';
          x.category = taskBadge;
        }
        else if (taskCategory == "15"){
          badge = 'badge-primary';
          taskBadge = 'FAT';
          x.category = taskBadge;
        }
        else if (taskCategory == "16"){
          badge = 'badge-primary';
          taskBadge = 'Zagon';
          x.category = taskBadge;
        }
        else if (taskCategory == "17"){
          badge = 'badge-primary';
          taskBadge = 'Naklad';
          x.category = taskBadge;
        }
        else{
          x.category = "Brez";
        }
        if (x.category == 'Strojna izdelava'){
          $('#taskButtonBuildParts'+taskId).show();
          $('#taskButtonSubtask'+taskId).prop('disabled', true);
        }
        else{
          $('#taskButtonBuildParts'+taskId).hide();
          $('#taskButtonSubtask'+taskId).prop('disabled', false);
        }
        //priority
        var badgeElement = '<span class="badge badge-pill '+badge+' ml-2">'+taskBadge.toUpperCase()+'</span>';
        $('#taskBadges'+taskId).append(badgeElement);
        $('#task'+taskId).removeClass('border-low-priority');
        $('#task'+taskId).removeClass('border-medium-priority');
        $('#task'+taskId).removeClass('border-high-priority');
        if (taskPriority == "2"){
          $('#task'+taskId).addClass('border-low-priority');
          x.priority = 'nizka';
        }
        else if (taskPriority == "3"){
          $('#task'+taskId).addClass('border-medium-priority');
          x.priority = 'srednja';
        }
        else if (taskPriority == "4"){
          $('#task'+taskId).addClass('border-high-priority');
          x.priority = 'visoka';
        }
        else{
          x.priority = 'brez';
        }
        //workers
        var taskWorkers = "";
        for(var i = 0; i < $('#taskAssignmentEdit').val().length; i++){
          if (i != 0)
            taskWorkers += ", ";
          taskWorkers += allWorkers.find(w => w.id == parseInt($('#taskAssignmentEdit').val()[i])).text;
        }
        if (taskWorkers)
          $('#taskInfo'+taskId).find('.workerspopover').attr('data-content', taskWorkers);
        else
          $('#taskInfo'+taskId).find('.workerspopover').attr('data-content', " ");
        if (!taskWorkers){
          taskWorkers = null;
          taskAssignment = null;
        }
        x.workers_id = taskAssignment;
        x.workers = taskWorkers;
        x.task_duration = taskDuration;
        //absence button (if there is no start || finish || workers button is disabled)
        if (taskAssignment && taskStart && taskFinish)
          $('#taskButtonAbsence'+taskId).prop('disabled', false);
        else
          $('#taskButtonAbsence'+taskId).prop('disabled', true)
        x.active = taskActive;
        $('#task'+taskId).removeClass('bg-secondary');
        if (taskActive == false)
          $('#task'+taskId).addClass('bg-secondary');
        //completion and checkbox if 100 or opposite
        if (!x.subtask_count > 0){
          if (taskCompletion == 100)
            $('#customTaskCheck'+taskId).prop("checked", true);
          else
            $('#customTaskCheck'+taskId).prop("checked", false);
        }
        $('#taskProgress'+taskId).width(taskCompletion+"%");
        x.completion = taskCompletion;
        debugger;
        toggleCollapse(taskId, 0);
        if (timelineCollapseOpen)
          refreshTimeline();
        //ADDING NEW CHANGE TO DB
        addChangeSystem(2,2,projectId,taskId);
        //addNewProjectChange(2,2,projectId,taskId);
        fixCompletion();
      }
    })
    debugger;
  }
}
//open conflicts modal and fill modal with new conflicts
function openConflictEditModal(conflicts){
  debugger;
  $('#conflictsEditBody').empty();
  for(var i = 0; i < conflicts.length; i++){
    if (conflicts[i].length > 0){
      var conflictWorker = conflicts[i][0].name + " " +conflicts[i][0].surname;
      var listElements = ``;
      for(var j = 0; j < conflicts[i].length; j++){
        var linkStart = '';
        var linkEnd = '';
        var linkTask = '';
        debugger;
        if (conflicts[i][j].id_subscriber){
          //servis conflicts
          if (loggedUser.role.substring(0,5) == 'vodja' || loggedUser.role == 'admin'){
            linkStart = `<a href="/servis/?taskId=`+conflicts[i][j].id_task+`" class="alert-link">`;
            linkEnd = '</a>';
          }
          listElements += `<li class="list-group-item">
            <div>
              Servis `+linkStart+`<u>`+conflicts[i][j].task_name+`</u>`+linkEnd+` pri <u>`+conflicts[i][j].subscriber+`</u> v času <u>`+new Date(conflicts[i][j].task_start).toLocaleDateString('sl')+` - `+new Date(conflicts[i][j].task_finish).toLocaleDateString('sl')+`</u>.
            </div>
          </li>`;
        }
        else if (conflicts[i][j].id_project){
          //project conflicts
          if (loggedUser.role.substring(0,5) == 'vodja' || loggedUser.role == 'admin'){
            linkStart = `<a href="/projects/id?id=`+conflicts[i][j].id_project+`" class="alert-link">`;
            linkTask = `<a href="/projects/id?id=`+conflicts[i][j].id_project+`&taskId=`+conflicts[i][j].id_task+`" class="alert-link">`;
            linkEnd = '</a>';
          }
          listElements += `<li class="list-group-item">
            <div>
              `+linkStart+`<u>`+conflicts[i][j].project_name+`</u>`+linkEnd+` na opravilu `+linkTask+`<u>`+conflicts[i][j].task_name+`</u>`+linkEnd+` v času <u>`+new Date(conflicts[i][j].task_start).toLocaleDateString('sl')+` - `+new Date(conflicts[i][j].task_finish).toLocaleDateString('sl')+`</u>.
            </div>
          </li>`;
        }
        else{
          //absence
          if (loggedUser.role.substring(0,5) == 'vodja' || loggedUser.role == 'admin'){
            linkStart = `<a href="/employees/absence?id=`+conflicts[i][j].id_user+`" class="alert-link">`;
            linkEnd = '</a>';
          }
          listElements += `<li class="list-group-item">
            <div>
              `+linkStart+`<u>`+conflicts[i][j].project_name+`</u>`+linkEnd+` v času <u>`+new Date(conflicts[i][j].task_start).toLocaleDateString('sl')+` - `+new Date(conflicts[i][j].task_finish).toLocaleDateString('sl')+`</u> z nazivom: <u>`+conflicts[i][j].task_name+`</u>.
            </div>
          </li>`;
        }
      }
      var element = `<div><strong>`+conflictWorker+`:</strong></div>
      <ul class="list-group mb-2">
        `+listElements+`
      </ul>`;
      $('#conflictsEditBody').append(element);
    }
  }
  $('#modalConflictsEdit').modal();
}
function onEventDeleteTask(event) {
  deleteTask(this.id.substring(16));
}
function deleteTask(taskId){
  debugger;
  savedTaskId = taskId;
  $('#modalDeleteTask').modal('toggle');
}
function deleteTaskConfirm(){
  var taskId = savedTaskId;
  //needs to make a call to server to actually delete task (change activity to false)
  $.post('/projects/removetask', {taskId}, function(resp){
    if (resp.success){
      console.log('Successfully deleting task.');
      $('#modalDeleteTask').modal('toggle');
      if (loggedUser.role == 'admin'){
        if (taskLoaded){
          allTasks.find(t => t.id == taskId).active = false
        }
        $('#task'+taskId).addClass('bg-secondary');
      }
      else{
        if (!taskLoaded){
          $.get( "/projects/tasks", {projectId,categoryTask,activeTask, completionTask}, function( data ) {
            //console.log(data)
            //debugger
            allTasks = data.data;
            taskLoaded = true;
            fixTaskTable(taskId);
          });
        }
        else{
          fixTaskTable(taskId);
        }
      }
      fixCompletion();
      //ADDING NEW CHANGE TO DB
      addChangeSystem(3,2,projectId,taskId);
      //addNewProjectChange(2,3,projectId,taskId);
    }
    else{
      console.log('Unsuccessfully deleting task.');
      $('#modalDeleteTask').modal('toggle');
      $('#modalError').modal('toggle');
    }
  })
}
//fix table list when user deletes a task
function fixTaskTable(taskId){
  $('#task'+taskId).remove();
  var myCurrentPage = activePage;
  allTasks = allTasks.filter(t => t.id != taskId);
  var pageCounter = Math.ceil(allTasks.length/10);
  drawPagination(pageCounter);
  activePage = 1
  if (myCurrentPage > pageCounter)
    myCurrentPage = pageCounter;
  drawTasks(myCurrentPage);
  if (timelineCollapseOpen)
    refreshTimeline();
}
var collapseAddNewTaskOpen = false;
/*
$('#taskNameAdd').on('input',function(e){
  if ($('#taskNameAdd').val() == '')
    $('#taskNameAdd').addClass('is-invalid');
  else
    $('#taskNameAdd').removeClass('is-invalid');
});
*/
//new task controll check if they are correct
function onDatesAddChange(){
  $('#taskDurationAdd').val(0);
  var s = $('#pinfo_date').html().split(' - ')[0].split(".");
  s = new Date(s[2]+"."+s[1]+"."+s[0]+" 07:00");
  var f = $('#pinfo_date').html().split(' - ')[1].split(".");
  f = new Date(f[2]+"."+f[1]+"."+f[0]+" 15:00");
  if ($('#taskStartAdd').val() && $('#taskFinishAdd').val()){
    countDays();
    //check if they are in logical date order
    if (new Date($('#taskStartAdd').val()) > new Date($('#taskFinishAdd').val())){
      $('#taskStartAddInvalidFeedback').html('Datuma si časovno ne sledita!');
      $('#taskFinishAddInvalidFeedback').html('Datuma si časovno ne sledita!');
      $('#taskStartAdd').addClass('is-invalid');
      $('#taskFinishAdd').addClass('is-invalid');
      $('#btnTaskAdd').prop('disabled', true);
    }
    else{//they are, mark the problematic start or/and finish
      $('#btnTaskAdd').prop('disabled', false);
      $('#taskStartAdd').removeClass('is-invalid');
      $('#taskFinishAdd').removeClass('is-invalid');
      var newTaskStart = new Date($('#taskStartAdd').val()+" 9:00");
      var newTaskFinish = new Date($('#taskFinishAdd').val()+" 13:00");
      if (isValidDate(s) && isValidDate(f)){//Projekt ima zacetek in konec, preveri zacetek in konec opravila na obeh datumih 
        if (newTaskStart < s || newTaskStart > f){
          $('#taskStartAddInvalidFeedback').html('Začetek opravila je izven datuma projekta!');
          $('#taskStartAdd').addClass('is-invalid');
          debugger;
        }
        if (newTaskFinish < s || newTaskFinish > f){
          $('#taskFinishAddInvalidFeedback').html('Konec opravila je izven datuma projekta!');
          $('#taskFinishAdd').addClass('is-invalid');
          debugger;
        }
      }
      else if (isValidDate(s)){//projekt ima samo zacetek, preveri zacetek in konec opravila glede na zacetek projekta
        if (newTaskStart < s){
          $('#taskStartAddInvalidFeedback').html('Začetek opravila je izven datuma projekta!');
          $('#taskStartAdd').addClass('is-invalid');
          debugger;
        }
        if (newTaskFinish < s){
          $('#taskFinishAddInvalidFeedback').html('Konec opravila je izven datuma projekta!');
          $('#taskFinishAdd').addClass('is-invalid');
          debugger;
        }
      }
      else if (isValidDate(f)){
        if (newTaskStart > f){
          $('#taskStartAddInvalidFeedback').html('Začetek opravila je izven datuma projekta!');
          $('#taskStartAdd').addClass('is-invalid');
          debugger;
        }
        if (newTaskFinish > f){
          $('#taskFinishAddInvalidFeedback').html('Konec opravila je izven datuma projekta!');
          $('#taskFinishAdd').addClass('is-invalid');
          debugger;
        }
      }
    }
  }
  else if ($('#taskStartAdd').val()){
    //check if start is withing project start and finish, otherwise mark it as problematic date
    $('#btnTaskAdd').prop('disabled', false);
    $('#taskStartAdd').removeClass('is-invalid');
    $('#taskFinishAdd').removeClass('is-invalid');
    var newTaskStart = new Date($('#taskStartAdd').val()+" 9:00");
    if (isValidDate(s) && isValidDate(f)){//Projekt ima zacetek in konec, preveri zacetek in konec opravila na obeh datumih 
      if (newTaskStart < s || newTaskStart > f){
        $('#taskStartAddInvalidFeedback').html('Začetek opravila je izven datuma projekta!');
        $('#taskStartAdd').addClass('is-invalid');
        debugger;
      }
    }
    else if (isValidDate(s)){//projekt ima samo zacetek, preveri zacetek in konec opravila glede na zacetek projekta
      if (newTaskStart < s){
        $('#taskStartAddInvalidFeedback').html('Začetek opravila je izven datuma projekta!');
        $('#taskStartAdd').addClass('is-invalid');
        debugger;
      }
    }
    else if (isValidDate(f)){
      if (newTaskStart > f){
        $('#taskStartAddInvalidFeedback').html('Začetek opravila je izven datuma projekta!');
        $('#taskStartAdd').addClass('is-invalid');
        debugger;
      }
    }
  }
  else if ($('#taskFinishAdd').val()){
    //check if start is withing project start and finish, otherwise mark it as problematic date
    $('#btnTaskAdd').prop('disabled', false);
    $('#taskStartAdd').removeClass('is-invalid');
    $('#taskFinishAdd').removeClass('is-invalid');
    var newTaskFinish = new Date($('#taskFinishAdd').val()+" 13:00");
    if (isValidDate(s) && isValidDate(f)){//Projekt ima zacetek in konec, preveri zacetek in konec opravila na obeh datumih 
      if (newTaskFinish < s || newTaskFinish > f){
        $('#taskFinishAddInvalidFeedback').html('Konec opravila je izven datuma projekta!');
        $('#taskFinishAdd').addClass('is-invalid');
        debugger;
      }
    }
    else if (isValidDate(s)){//projekt ima samo zacetek, preveri zacetek in konec opravila glede na zacetek projekta
      if (newTaskFinish < s){
        $('#taskFinishAddInvalidFeedback').html('Konec opravila je izven datuma projekta!');
        $('#taskFinishAdd').addClass('is-invalid');
        debugger;
      }
    }
    else if (isValidDate(f)){
      if (newTaskFinish > f){
        $('#taskFinishAddInvalidFeedback').html('Konec opravila je izven datuma projekta!');
        $('#taskFinishAdd').addClass('is-invalid');
        debugger;
      }
    }
  }
  else{
    $('#btnTaskAdd').prop('disabled', false);
    $('#taskStartAdd').removeClass('is-invalid');
    $('#taskFinishAdd').removeClass('is-invalid');
  }
}
//edit task controll check if they are correct
function onDatesEditChange(){
  $('#taskDurationEdit').val(0);
  var s = $('#pinfo_date').html().split(' - ')[0].split(".");
  s = new Date(s[2]+"."+s[1]+"."+s[0]+" 07:00");
  var f = $('#pinfo_date').html().split(' - ')[1].split(".");
  f = new Date(f[2]+"."+f[1]+"."+f[0]+" 15:00");
  if ($('#taskStartEdit').val() && $('#taskFinishEdit').val()){
    countEditDays();
    //check if they are in logical date order
    if (new Date($('#taskStartEdit').val()) > new Date($('#taskFinishEdit').val())){
      $('#taskStartEditInvalidFeedback').html('Datuma si časovno ne sledita!');
      $('#taskFinishEditInvalidFeedback').html('Datuma si časovno ne sledita!');
      $('#taskStartEdit').addClass('is-invalid');
      $('#taskFinishEdit').addClass('is-invalid');
      $('#btnTaskEdit').prop('disabled', true);
    }
    else{//they are, mark the problematic start or/and finish
      $('#btnTaskEdit').prop('disabled', false);
      $('#taskStartEdit').removeClass('is-invalid');
      $('#taskFinishEdit').removeClass('is-invalid');
      var newTaskStart = new Date($('#taskStartEdit').val()+" 9:00");
      var newTaskFinish = new Date($('#taskFinishEdit').val()+" 13:00");
      if (isValidDate(s) && isValidDate(f)){//Projekt ima zacetek in konec, preveri zacetek in konec opravila na obeh datumih 
        if (newTaskStart < s || newTaskStart > f){
          $('#taskStartEditInvalidFeedback').html('Začetek opravila je izven datuma projekta!');
          $('#taskStartEdit').addClass('is-invalid');
          debugger;
        }
        if (newTaskFinish < s || newTaskFinish > f){
          $('#taskFinishEditInvalidFeedback').html('Konec opravila je izven datuma projekta!');
          $('#taskFinishEdit').addClass('is-invalid');
          debugger;
        }
      }
      else if (isValidDate(s)){//projekt ima samo zacetek, preveri zacetek in konec opravila glede na zacetek projekta
        if (newTaskStart < s){
          $('#taskStartEditInvalidFeedback').html('Začetek opravila je izven datuma projekta!');
          $('#taskStartEdit').addClass('is-invalid');
          debugger;
        }
        if (newTaskFinish < s){
          $('#taskFinishEditInvalidFeedback').html('Konec opravila je izven datuma projekta!');
          $('#taskFinishEdit').addClass('is-invalid');
          debugger;
        }
      }
      else if (isValidDate(f)){
        if (newTaskStart > f){
          $('#taskStartEditInvalidFeedback').html('Začetek opravila je izven datuma projekta!');
          $('#taskStartEdit').addClass('is-invalid');
          debugger;
        }
        if (newTaskFinish > f){
          $('#taskFinishEditInvalidFeedback').html('Konec opravila je izven datuma projekta!');
          $('#taskFinishEdit').addClass('is-invalid');
          debugger;
        }
      }
    }
  }
  else if ($('#taskStartEdit').val()){
    //check if start is withing project start and finish, otherwise mark it as problematic date
    $('#btnTaskEdit').prop('disabled', false);
    $('#taskStartEdit').removeClass('is-invalid');
    $('#taskFinishEdit').removeClass('is-invalid');
    var newTaskStart = new Date($('#taskStartEdit').val()+" 9:00");
    if (isValidDate(s) && isValidDate(f)){//Projekt ima zacetek in konec, preveri zacetek in konec opravila na obeh datumih 
      if (newTaskStart < s || newTaskStart > f){
        $('#taskStartEditInvalidFeedback').html('Začetek opravila je izven datuma projekta!');
        $('#taskStartEdit').addClass('is-invalid');
        debugger;
      }
    }
    else if (isValidDate(s)){//projekt ima samo zacetek, preveri zacetek in konec opravila glede na zacetek projekta
      if (newTaskStart < s){
        $('#taskStartEditInvalidFeedback').html('Začetek opravila je izven datuma projekta!');
        $('#taskStartEdit').addClass('is-invalid');
        debugger;
      }
    }
    else if (isValidDate(f)){
      if (newTaskStart > f){
        $('#taskStartEditInvalidFeedback').html('Začetek opravila je izven datuma projekta!');
        $('#taskStartEdit').addClass('is-invalid');
        debugger;
      }
    }
  }
  else if ($('#taskFinishEdit').val()){
    //check if start is withing project start and finish, otherwise mark it as problematic date
    $('#btnTaskEdit').prop('disabled', false);
    $('#taskStartEdit').removeClass('is-invalid');
    $('#taskFinishEdit').removeClass('is-invalid');
    var newTaskFinish = new Date($('#taskFinishEdit').val()+" 13:00");
    if (isValidDate(s) && isValidDate(f)){//Projekt ima zacetek in konec, preveri zacetek in konec opravila na obeh datumih 
      if (newTaskFinish < s || newTaskFinish > f){
        $('#taskFinishEditInvalidFeedback').html('Konec opravila je izven datuma projekta!');
        $('#taskFinishEdit').addClass('is-invalid');
        debugger;
      }
    }
    else if (isValidDate(s)){//projekt ima samo zacetek, preveri zacetek in konec opravila glede na zacetek projekta
      if (newTaskFinish < s){
        $('#taskFinishEditInvalidFeedback').html('Konec opravila je izven datuma projekta!');
        $('#taskFinishEdit').addClass('is-invalid');
        debugger;
      }
    }
    else if (isValidDate(f)){
      if (newTaskFinish > f){
        $('#taskFinishEditInvalidFeedback').html('Konec opravila je izven datuma projekta!');
        $('#taskFinishEdit').addClass('is-invalid');
        debugger;
      }
    }
  }
  else{
    $('#btnTaskEdit').prop('disabled', false);
    $('#taskStartEdit').removeClass('is-invalid');
    $('#taskFinishEdit').removeClass('is-invalid');
  }
}
function countDays(){
  var start = $('#taskStartAdd').val();
  var finish = $('#taskFinishAdd').val();
  var oneDay = 24*60*60*1000;
  var firstDay = new Date(start);
  var secondDay = new Date(finish);
  var diffDays = Math.round(Math.abs((firstDay.getTime() - secondDay.getTime()) / (oneDay)))+1;
  $('#taskDurationAdd').val(diffDays);
}
function countEditDays(){
  var start = $('#taskStartEdit').val();
  var finish = $('#taskFinishEdit').val();
  var oneDay = 24*60*60*1000;
  var firstDay = new Date(start);
  var secondDay = new Date(finish);
  var diffDays = Math.round(Math.abs((firstDay.getTime() - secondDay.getTime()) / (oneDay)))+1;
  $('#taskDurationEdit').val(diffDays);
}
function isValidDate(d) {
  return d instanceof Date && !isNaN(d);
}
function onEventGetTaskChanges(event) {
  getTaskChanges(this.id.substring(11));
}
function getTaskChanges(id){
  debugger;
  var taskId = id;
  let data = {taskId};
  $.ajax({
    type: 'GET',
    url: '/servis/changes',
    contentType: 'application/json',
    data: data, // access in body
  }).done(function (resp) {
    if(resp.success){
      debugger;
      var taskChanges = resp.taskChanges.filter(c => c.id_type != 20 && c.id_type != 21 && c.id_type != 22);
      // var taskChanges = resp.taskChanges.filter(c => c.id_type != 21);
      console.log('Successfully getting all the changes of selected task/service.');
      var element = "";
      var fileLabel = "";
        for(var i=0; i<taskChanges.length; i++){
          if(taskChanges[i].id_type == 7)
            fileLabel = " datoteko";
          else if(taskChanges[i].id_type == 4){
            if(taskChanges[i].subtask)
              fileLabel = " sporočilo nalogi " + taskChanges[i].subtask;
            else
              fileLabel = " sporočilo";
          }
          else if(taskChanges[i].id_type == 3)
            fileLabel = " nalogo " + taskChanges[i].subtask;
          else if(taskChanges[i].id_type == 5)
            fileLabel = " odsotnost";
          // else if (taskChanges[i].id_type == 20){
          //   fileLabel = " kos (ročno)";
          // }
          // else if (taskChanges[i].id_type == 22){
          //   fileLabel = " kos (csv)";
          // }
          else
            fileLabel = "";
          element = new Date(taskChanges[i].date).toLocaleDateString('sl') + ' ' + new Date(taskChanges[i].date).toLocaleTimeString('sl').substring(0,5) + ", " + taskChanges[i].name + " " + taskChanges[i].surname + ', ' + taskChanges[i].status + fileLabel + "<br />" + element;
        }
        //$('.mypopover').attr('data-content', element);
        $('#taskHistory'+taskId).attr('data-content', element);
        $('#taskHistory'+taskId).popover('show');
        //$('[data-toggle="popover"]').popover({trigger: "hover"}); 
    }
    else{
      $('#modalError').modal('toggle');
    }
  }).fail(function (resp) {
    //console.log('FAIL');
    //debugger;
    $('#modalError').modal('toggle');
  }).always(function (resp) {
    //console.log('ALWAYS');
    //debugger;
  });
}
//$('#taskStartAdd').on('change', onStartAddChange);
//$('#taskFinishAdd').on('change', onFinishAddChange);
var savedTaskId;