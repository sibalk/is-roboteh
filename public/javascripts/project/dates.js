//IN THIS SCRIPT//
//// project dates control, get all dates, add new date, remove date, display dates ////
$(function(){
  //get project dates and show them on page
  //debugger;
  $.get( "/projects/dates/get", {projectId}, function( data ) {
    projectDates = data.data;
    drawProjectDates();
  });
  //for add dates form
  $('#addDateform').submit(function(e){
    e.preventDefault();
    $form = $(this);
    var name = date_name.value;
    var date = date_date.value + " 15:00";
    $.post('/projects/dates/add', {name, date, projectId}, function(newDate){
      if(newDate.success){
        console.log('Successfully adding project date.');
        getProjectDates();
        $('#modalAddDate').modal('toggle');
      }
      else{
        console.log('Unsuccessfully adding project date.');
        $('#modalAddDate').modal('toggle');
        $('#modalError').modal('toggle');
      }
    })
    debugger;
  })
  //for edit dates form
  $('#editDateform').submit(function(e){
    e.preventDefault();
    $form = $(this);
    var name = date_name_edit.value;
    var date = date_date_edit.value + " 15:00";
    var dateId = activeProjectDateId;
    $.post('/projects/dates/update', {name, date, dateId}, function(data){
      if(data.success){
        console.log('Successfully updating project date.');
        getProjectDates();
        $('#modalEditDate').modal('toggle');
      }
      else{
        console.log('Unsuccessfully updating project date.');
        $('#modalEditDate').modal('toggle');
        $('#modalError').modal('toggle');
      }
    })
    debugger;
  })
  //call event functions
  $('#addProjectDateBtn').on('click', openProjectDateModal);
  $('#btnDeleteDateConf').on('click', deleteDateConfirm);
})
//function to get all dates
function getProjectDates(){
  debugger;
  if(timelineProject && showProjectDates)
    removeAllMarkers();
  $.get( "/projects/dates/get", {projectId}, function( data ) {
    projectDates = data.data;
    drawProjectDates();
  });
}
//function to add new date
function addProjectDate(){
  debugger;
}
//function do delete selected date
function deleteProjectDate(id){
  debugger;
  projectDateId = id;
  $('#modalDeleteDate').modal('toggle');
}
function deleteDateConfirm(){
  var id = projectDateId;
  $.post('/projects/dates/remove', {id}, function(data){
    if(data == "success"){
      getProjectDates();
      $('#modalDeleteDate').modal('toggle');
    }
    else{
      $('#modalDeleteDate').modal('toggle');
      $('#modalError').modal('toggle');
    }
  })
}
//function open edit selected date
function editProjectDate(id){
  debugger;
  activeProjectDateId = id;
  var tmpProjectDate = projectDates.find(pd => pd.id == id);
  $('#date_name_edit').val(tmpProjectDate.name);
  $('#date_date_edit').val(moment(tmpProjectDate.date).format("YYYY-MM-DD"));
  $('#modalEditDate').modal('toggle');
  //$.post('/projects/dates/remove', {id}, function(data){
  //  if(data == "success")
  //    getProjectDates();
  //})
}
//open modal for new date
function openProjectDateModal(){
  $('#modalAddDate').modal('toggle');
}

//draw project dates
function drawProjectDates(){
  $('#pinfo_dates').empty();
  var element = ``;
  var deleteButton = ``;
  var editButton = '';
  //chartDates = [];
  for(var i = 0; i < projectDates.length; i++){
    let dateId = projectDates[i].id;
    var date = new Date(projectDates[i].date);
    var d = date.getDate(); if(d < 10) d = '0'+d;
    var m = date.getMonth()+1; if(m < 10) m = '0'+m;
    var y = date.getFullYear();
    if(loggedUser.role == 'admin' || loggedUser.role.substring(0,5) == 'vodja' || leader){
      deleteButton = `<button class="btn btn-danger mr-2 mt-1" id="deleteDate`+projectDates[i].id+`">
        <i class="fas fa-trash fa-lg"></i>
      </button>`;
      editButton = `<button class="btn btn-primary task-button mt-1" id="editDate`+projectDates[i].id+`">
        <i class="fas fa-pen fa-lg"></i>
      </button>`;
    }
    element = `<div class="date-group-item" id="projectDate`+projectDates[i].id+`">
      <div class="row mb-2">
        <div class="col-7 col-md-7 col-lg-7 col-xl-7"><strong>`+projectDates[i].name+`</strong></div>
        <div class="col-4 col-md-2 col-lg-3 col-xl-2"><strong>`+d+"."+m+"."+y+`</strong></div>
        <div class="col-3 col-md-3 col-lg-3 col-xl-3">
          `+deleteButton+`
          `+editButton+`
        </div>
      </div>
    </div>`;
    $('#pinfo_dates').append(element);
    $('#editDate'+dateId).off('click').on('click', function (event) { editProjectDate(dateId); });
    $('#deleteDate'+dateId).off('click').on('click', function (event) { deleteProjectDate(dateId); });
    //chartDates.push(new Date(projectDates[i].date));
  }
  //if(timelineProject && showProjectDates)
    //drawProjectDatesOnTimeline();
}
var projectDates;
var chartDates = [];
var activeProjectDateId;
var projectDateId;