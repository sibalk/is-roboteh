$(document).ready(function() {
  //debugger

  //$('.modal-dialog').draggable({
  //  handle: ".modal-header"
  //});
  //var dataGet;
  var subscribers;
  loggedUser = {name:$('#loggedUser').html().split(" (")[0].split(" ")[0], surname:$('#loggedUser').html().split(" (")[0].split(" ")[1], role:$('#loggedUser').html().split(" (")[1].split(")")[0].toLowerCase()};

  $('.btnadd').click(function(){
    //grem po zaposlene
    $.get( "/apiv2/employees/", function( data ) {
      allUsers = data.users.filter(u => u.active);
      allUsers.forEach(u => u.text = u.name + " " + u.surname);

      $(".select2-leader").select2({
        data: allUsers,
        tags: true,
        dropdownParent: $('#modalAddProjectForm')
      });
      $('#project_leader').val(allUsers.find(u => u.name + ' ' + u.surname == loggedUser.name + ' ' + loggedUser.surname).id).trigger('change');
    });
    // grem po naročnike
    $.get( "/subscribers/all", function( data ) {
      subscribers = data.data;

      $(".select2-subscriber").select2({
        data: subscribers,
        tags: true,
        dropdownParent: $('#modalAddProjectForm')
      });
      changeNumberOrder();
      $("#modalAddProjectForm").modal();
    });
    // get all roles from db
    $.get( "/projects/roles", function( data ) {
      allRoles = data.data;
    });
  })
  //UPDATE THE PROJECT
  $('#editProjectform').submit(function(e){
    e.preventDefault();
    $form = $(this);
    var name = project_name_edit.value;
    var number = projectNumberPrefixEdit.value+$('#projectNumberEdit').val();
    //var number = project_number_edit.value;
    var start = project_start_edit.value;
    var finish = project_finish_edit.value;
    var subscriber = project_subscriber_edit.value;
    var notes = project_notes_edit.value;
    var active;
    if(typeof activeEdit === 'undefined')
      active = true;
    else
      active = activeEdit.checked;
    if(start)
      start = start + " 07:00:00.000000";
    if(finish)
      finish = finish + " 15:00:00.000000";
    //debugger;
    //check if subscriber is new
    if(isNaN(subscriber)){
      //debugger;
      //new subscriber, create subscriber and then update project
      $.post('/subscribers/create', {subscriber}, function(resp){
        if(resp.success){
          var subscriberName = subscriber;
          subscriber = resp.id.id;
          $.post('/projects/update', {projectId, number, name, subscriber, start, finish, notes, active}, function(resp){
            console.log('Successfully editing project.');
            //debugger;
            var temp = tableProjects.row('#'+projectId+'').data();
            temp.project_name = name;
            temp.project_number = number;
            temp.name = subscriberName;
            temp.start = start;
            temp.finish = finish;
            temp.active = active;
            tableProjects.row('#'+projectId).data(temp).invalidate().draw(false);
            //update objects in array dataGet
            var tmp = dataGet.find(p => p.id == projectId)
            tmp.project_number = number;
            tmp.project_name = name;
            tmp.name = subscriberName;
            tmp.start = start;
            tmp.finish = finish;
            tmp.active = active;
            tmp.note = notes;
            redoSortProjects();
            $('#modalEditProjectForm').modal('toggle');
            //ADDING NEW CHANGE TO DB
            addChangeSystem(2,1,projectId);
            //addNewProjectChange(1,2,projectId);
            //ADDING NEW CHANGE TO DB (NEW SUBSCRIBER)
            addChangeSystem(1,8,null,null,null,null,null,null,subscriber);
            //addSystemChange(8,1,null,null,null,subscriber);
          })
        }
        else{
          console.log('Unsuccessfully editing project.');
          $('#modalEditProjectForm').modal('toggle');
          $('#modalError').modal('toggle');
        }
      })
    }
    else{
      //debugger;
      //nothing new, update project
      $.post('/projects/update', {projectId, number, name, subscriber, start, finish, notes, active}, function(resp){
        if(resp.success){
          console.log('Successfully editing project.');
          var subscriberName = allSubscribers.find(s => s.id === parseInt(subscriber)).text;
          //debugger;
          var temp = tableProjects.row('#'+projectId+'').data();
          temp.project_name = name;
          temp.project_number = number;
          temp.name = subscriberName;
          temp.start = start;
          temp.finish = finish;
          temp.active = active;
          tableProjects.row('#'+projectId).data(temp).invalidate().draw(false);
          var tmp = dataGet.find(p => p.id == projectId)
          tmp.project_number = number;
          tmp.project_name = name;
          tmp.name = subscriberName;
          tmp.start = start;
          tmp.finish = finish;
          tmp.active = active;
          tmp.note = notes;
          redoSortProjects();
          $('#modalEditProjectForm').modal('toggle');
          //ADDING NEW CHANGE TO DB
          addChangeSystem(2,1,projectId);
          //addNewProjectChange(1,2,projectId);
        }
        else{
          console.log('Unsuccessfully editing project.');
          $('#modalEditProjectForm').modal('toggle');
          $('#modalError').modal('toggle');
        }
      })
    }

  })
  //ADD NEW PROJECT
  $('#addProjectform').submit(function(e)
  {
    e.preventDefault();
    //console.log("test");
    $form = $(this);
    var name = project_name.value;
    var number = projectNumberPrefix.value+$('#projectNumber').val();
    var start = project_start.value;
    var finish = project_finish.value;
    var subscriber = project_subscriber.value;
    //var subscriberNew = project_subscriber_new.value;
    var notes = project_notes.value;
    //console.log($form);
    if(start)
      start = start + " 07:00:00.000000";
    if(finish)
      finish = finish + " 15:00:00.000000";


    //debugger;
    //check if project number already exists
    //console.log(dataGet)
    var conflict = dataGet.find(d => d.project_number === number)
    //console.log(conflict);
    if(conflict){
      //conflict
      var alertElement = `
      <div class="alert alert-danger alert-dismissible average" role="alert">
      <button class="close" type="button" data-dismiss="alert">×</button>
      <p>Ta številka projekta je že zasedena</p>
      </div>`
      $('#messageAdd').append(alertElement); 
    }
    else{
      //no conflict with project number
      //debugger
      if(isNaN(subscriber)){
        //new subscriber, create subscriber and then add create project with new subscriber
        $.post('/subscribers/create', {subscriber}, function(resp){
          if(resp.success){
            //resp.id je id novega subscriberja
            var subscriberName = subscriber;
            subscriber = resp.id.id;
            //debugger;
            $.post('/projects/create', {number, name, subscriber, start, finish, notes}, function(resp){
              if(resp.success){
                console.log('Successfully adding project.');
                let worker = project_leader.value;
                let roleNew = allRoles.find(r => r.text.toLowerCase() == 'vodja projekta');
                roleNew = roleNew.id;
                let projectId = resp.project.id;
                //add user as project leader on created project
                let data = {worker, roleNew, projectId};
                $.ajax({
                  url: '/projects/worker/add',
                  type: 'POST',
                  data: JSON.stringify(data),
                  contentType: 'application/json',
                  success: function(resp){
                    console.log('Successfully adding worker to project.');
                  },
                  fail: function(resp){
                    console.log('Unsuccessfully adding worker to project.');
                  }
                })
                //debugger
                changeActive(projectsActive);
                //debugger
                $('#project_name').val('');
                $('#project_start').val('');
                $('#project_finish').val('');
                $('#project_notes').val('');
                $('#project_subscriber').val(1).trigger('change');
                $('#project_leader').val(allUsers.find(u => u.name + ' ' + u.surname == loggedUser.name + ' ' + loggedUser.surname).id).trigger('change');
                //toggle modal
                $('#modalAddProjectForm').modal('toggle');
                onDatesNewChange();
                changeNumberOrder();
                //ADDING NEW CHANGE TO DB
                addChangeSystem(1,1,resp.project.id);
                //addNewProjectChange(1,1,resp.id.id);
                //ADDING NEW CHANGE TO DB (NEW SUBSCRIBER)
                addChangeSystem(1,8,null,null,null,null,null,null,subscriber);
                //addSystemChange(8,1,null,null,null,subscriber);
              }
              else{
                console.log('Unsuccessfully adding project.');
                $('#modalAddProjectForm').modal('toggle');
                $('#modalError').modal('toggle');
              }
            })
          }
          else{
            console.log('Unsuccessfully adding project.');
            $('#modalAddProjectForm').modal('toggle');
            $('#modalError').modal('toggle');
          }
        })
        //debugger
      }
      else{
        //debugger
        //subscriber already exists, create project
        $.post('/projects/create', {number, name, subscriber, start, finish, notes}, function(resp){
          if(resp.success){
            console.log('Successfully adding project.');
            let worker = project_leader.value;
            let roleNew = allRoles.find(r => r.text.toLowerCase() == 'vodja projekta');
            roleNew = roleNew.id;
            let projectId = resp.project.id;
            //add user as project leader on created project
            let data = {worker, roleNew, projectId};
            $.ajax({
              url: '/projects/worker/add',
              type: 'POST',
              data: JSON.stringify(data),
              contentType: 'application/json',
              success: function(resp){
                console.log('Successfully adding worker to project.');
              },
              fail: function(resp){
                console.log('Unsuccessfully adding worker to project.');
              }
            })
            //debugger
            changeActive(projectsActive);
            $('#project_name').val('');
            $('#project_start').val('');
            $('#project_finish').val('');
            $('#project_notes').val('');
            $('#project_subscriber').val(1).trigger('change');
            $('#project_leader').val(allUsers.find(u => u.name + ' ' + u.surname == loggedUser.name + ' ' + loggedUser.surname).id).trigger('change');
            //toggle modal
            $('#modalAddProjectForm').modal('toggle');
            onDatesNewChange();
            changeNumberOrder();
            //ADDING NEW CHANGE TO DB
            addChangeSystem(1,1,resp.project.id);
            //addNewProjectChange(1,1,resp.id.id);
          }
          else{
            console.log('Unsuccessfully adding project.');
            $('#modalAddProjectForm').modal('toggle');
            $('#modalError').modal('toggle');
          }
        })
      }
    }
  });
  //onClick function for passing arguments in their functions
  $('#optionCustomSort').on('click', openCustomSortModal);
  $('#btnConfirmCustomSort').on('click', function () {
    // get custom order and save it to localStorage and call reorder/redraw of shown projects
    localStorage.setItem('customProjectSort', $('#project_subscriber_sort').val());
    localStorage.setItem('customProjectNameSort', $('#project_name_sort').val());
    // debugger;
    $('#modalCustomSubscriberSortForm').modal('toggle');
    changeActive(projectsActive);
  });
  $('#projectNumber').on('change', onNewProjectNumberChange);
  $('#projectNumberEdit').on('change', onEditProjectNumberChange);
  //$('#project_number').on('change', onNumberChange);
  //var projectNumber = $('#project_number').val();
  $('#project_start').on('change', onDatesNewChange);
  $('#project_finish').on('change', onDatesNewChange);
  //$('#project_number_edit').on('change', onProjectNumberChange);
  $('#project_start_edit').on('change', onDatesEditChange);
  $('#project_finish_edit').on('change', onDatesEditChange);
  projectStart = $('#project_start').val();
  projectFinish = $('#project_finish').val();
  $('#optionAll').on('click', { active: 0 }, onClickChangeActive);
  $('#optionTrue').on('click', { active: 1 }, onClickChangeActive);
  $('#optionFalse').on('click', { active: 2 }, onClickChangeActive);
  $('#optionAllProjects').on('click', { mode: 0 }, onClickChangeProject);
  $('#optionRTProjects').on('click', { mode: 1 }, onClickChangeProject);
  $('#optionSTRProjects').on('click', { mode: 2 }, onClickChangeProject);
  $('#optionAllCompletion').on('click', { comp: 0 }, onClickChangeCompletion);
  $('#optionUnfinished').on('click', { comp: 1 }, onClickChangeCompletion);
  $('#optionFinished').on('click', { comp: 2 }, onClickChangeCompletion);
  $('#btnMoveLeft').on('click', { number: '0.2' }, onClickMoveProject);
  $('#btnMoveRight').on('click', { number: '-0.2' }, onClickMoveProject);
  $('#btnToCSV').on('click', toCSV);
  $('#btnDeleteProject').on('click', deleteProjectConfirm);
  $('#btnOpenExcel').on('click', openExcelModal);
  $('#btnOpenPDF').on('click', openPDFModal);
  $('#projectNumberPrefix').on('change', changeNumberOrder);
  $('#projectNumberPrefixEdit').on('change', onEditProjectNumberChange);
  //for pdf
  $('input[name=customRadioPrefix]').on('change', linkPDFChange);
  $('input[name=customRadioStatus]').on('change', linkPDFChange);
  $('#checkboxCompletionPDF').on('change', linkPDFChange);
  $('#checkboxFinishPDF').on('change', linkPDFChange);
  $('#checkboxStartPDF').on('change', linkPDFChange);
  //session
  showMyProjects = sessionStorage.sessionMyProjectsSwitch == 'true' ? true : false;
  $('#showMyProjectsSwitch').prop('checked', showMyProjects);
  $('#showMyProjectsSwitch').on('click', changeToMyProjects);
  activeProject = 0; // only to get all projects because activity filter is on client side with projectActive
  projectsActive = sessionStorage.sessionProjectActive ? sessionStorage.sessionProjectActive : 1;
  $('#optionTrue').removeClass('active');
  $('#optionFalse').removeClass('active');
  $('#optionAll').removeClass('active');
  switch (projectsActive) {
    case '0': $('#optionAll').addClass('active'); break;
    case '1': $('#optionTrue').addClass('active'); break;
    case '2': $('#optionFalse').addClass('active'); break;
    default: $('#optionAll').addClass('active'); break;
  }
  // projectsActive = 1;
  projectsFilter = sessionStorage.sessionProjectsFilter ? sessionStorage.sessionProjectsFilter : 0;
  $('#optionAllProjects').removeClass('active');
  $('#optionRTProjects').removeClass('active');
  $('#optionSTRProjects').removeClass('active');
  switch (projectsFilter) {
    case '0': $('#optionAllProjects').addClass('active'); break;
    case '1': $('#optionRTProjects').addClass('active'); break;
    case '2': $('#optionSTRProjects').addClass('active'); break;
    default: $('#optionAllProjects').addClass('active'); break;
  }
  // projectsFilter = 0;
  projectsCompletion = sessionStorage.sessionProjectsCompletion ? sessionStorage.sessionProjectsCompletion : 0;
  // get all projects data and show it in table on first page load
  $('#optionAllCompletion').removeClass('active');
  $('#optionUnfinished').removeClass('active');
  $('#optionFinished').removeClass('active');
  switch (projectsCompletion) {
    case '0': $('#optionAllCompletion').addClass('active'); break;
    case '1': $('#optionUnfinished').addClass('active'); break;
    case '2': $('#optionFinished').addClass('active'); break;
    default: $('#optionAllCompletion').addClass('active'); break;
  }
  $.get("/projects/all", {activeProject}, function( data ) {
    //console.log(data)
    dataGet = data.data;
    //console.log(dataGet[2]);
    //SORT PROJECTS
    let showProjectsCount = localStorage.showProjectsCount ? localStorage.showProjectsCount : '20';
    var [sortedTmp1, sortedTmp2, tmp3] = sortProjects();
    allRTProjects = sortedTmp1, allSTRProjects = sortedTmp2;
    // dataGet = sortedTmp2.concat(sortedTmp1,tmp3);
    dataGet = sortedTmp1.concat(sortedTmp2,tmp3); // reorder dataGet as sorted list of projects, first RT, then STR and lastly every other project without RT or STR (should be none)
    // FILTER PROJECTS
    var tmp = filterSortedProjects(sortedTmp1, sortedTmp2, tmp3);
    // GET CUSTOM SORT
    // get custom sort order from localStorage and call function to reorder tmp based on custom sort order
    tmp = customSubscribersSortProjects(tmp);
    tmp = customNameSortProjects(tmp);
    projectsSort = tmp;
    tableProjects = $('#projectsTable').DataTable( {
      data: tmp,
      columns: [
          { title: "projectId", data: "id" },
          { title: "ID", data: "project_number" },
          { title: "Leto", data: "created", render: function(data, type, row){
            if(data){
              return new Date(data).getFullYear();
            }
            else
              return "/";
          } },
          { title: "Ime", data: "project_name" },
          { title: "Naročnik", data: "name" },
          { title: "Začetek", data: "start", render: function(data, type, row){
            if(data){
              return pad(new Date(data).getDate())+"."+pad(new Date(data).getMonth()+1)+"."+new Date(data).getFullYear();
            }
            else
              return "/";
          } },
          { title: "Konec", data: "finish", render: function(data, type, row){
            if(data){
              return pad(new Date(data).getDate())+"."+pad(new Date(data).getMonth()+1)+"."+new Date(data).getFullYear();
            }
            else
              return "/";
          }},
          { title: "%", data: "completion" },
          { data: "id",
            render: function ( data, type, row ) {
              if(loggedUser.role == 'admin' || loggedUser.role.substring(0,5) == 'vodja')
                return `<div class="d-flex">
                  <a class="mypopover p-1 btn-on-stretched mr-2 history-popover" data-toggle="popover" data-trigger="focus" tabindex="0" data-content="ni zabeleženih sprememb" id="projectHistory`+data+`" data-original-title="" title=""><i class="fas fa-history fa-lg text-dark"></i></a>
                  <button class="btn btn-danger btn-on-stretched w-42px btn-delete mr-2" data-toggle="tooltip" data-original-title="Odstrani"><i class="fas fa-trash fa-lg"></i></button>
                  <button class="btn btn-primary btn-on-stretched w-42px btn-edit mr-2" data-toggle="tooltip" data-original-title="Uredi"><i class="fas fa-pen fa-lg"></i></button>
                  <a class="btn btn-info stretched-link w-42px mr-2" href="/projects/id?id=` + data + `" data-toggle="tooltip" data-original-title="Več"><i class="fas fa-chevron-right fa-lg"></i></a>
                </div>`;
                // return '<button class="btn btn-danger btn-on-stretched w-42px btn-delete" data-toggle="tooltip" data-original-title="Odstrani"><i class="fas fa-trash fa-lg"></i></button>';
              else
                return `<div class="d-flex">
                  <a class="btn btn-info stretched-link w-42px" href="/projects/id?id=` + data + `" data-toggle="tooltip" data-original-title="Več"><i class="fas fa-chevron-right fa-lg"></i></a>
                </div>`;
            }},
          // { data: "id",
          //   render: function ( data, type, row ) {
          //     if(loggedUser.role == 'admin' || loggedUser.role.substring(0,5) == 'vodja')
          //       return '<button class="btn btn-primary btn-on-stretched w-42px btn-edit" data-toggle="tooltip" data-original-title="Uredi"><i class="fas fa-pen fa-lg"></i></button>';
          //     else
          //       return '<div></div>';
          //   }},
          // { data: "id",
          //   render: function ( data, type, row ) {
          //     return '<a class="btn btn-info stretched-link w-42px" href="/projects/id?id='+data+'" data-toggle="tooltip" data-original-title="Več"><i class="fas fa-chevron-right fa-lg"></i></a>';
          //   }},
          { title: "Aktivno", data: "active"},
      ],
      "language": {
        "url": "/DataTables/Slovenian.json"
      },
      "order": [],
      "lengthMenu": [[10, 20, 25, 50, -1], [10, 20, 25, 50, "All"]],
      "pageLength": parseInt(showProjectsCount),
      columnDefs: [
        {
          targets: [0,9],
          visible: false,
          searchable: false
        },
        { responsivePriority: 1, targets: 1 },
        { responsivePriority: 2, targets: -1 },
        { responsivePriority: 3, targets: 3 },
      ],
      "createdRow": function ( row, data, index ) {
        $(row).attr('id', data.id);
        $(row).css('transform', 'rotate(0)')
        $(row).removeClass('table-dark');
        if(!data.active){
          $(row).addClass('table-dark');
        }
        else if ( data.completion == 100 ) {
            //$('tr', row).addClass('table-success');
            $(row).addClass('table-success');
        }
        else if( data.finish)
          if(new Date(data.finish).getTime() < new Date().getTime())
            $(row).addClass('table-warning');
        setTimeout(()=>{$('[data-toggle="tooltip"]').tooltip();},400)
      },
      rowCallback: function(row, data, index){
        let tmp = $('select[name="projectsTable_length"]').val();
        localStorage.setItem('showProjectsCount', $('select[name="projectsTable_length"]').val());
        $(row).removeClass('table-dark');
        if(!data.active){
          $(row).addClass('table-dark');
        }
        else if ( data.completion == 100 ) {
          //$('tr', row).addClass('table-success');
          $(row).addClass('table-success');
        }
        else if( data.finish)
          if(new Date(data.finish).getTime() < new Date().getTime())
            $(row).addClass('table-warning');
        setTimeout(()=>{$('[data-toggle="tooltip"]').tooltip();},400)
        setTimeout(()=>{$('[data-toggle="popover"]').popover({ html: true });},400)
      },
      responsive: true,
      //info: false,
      //searching: false,
      //paging: false
      //dom: 'Bfrtip',
      // "dom":  "<'row'<'col-sm-12'B>>" +
      //         "<'row'<'col-sm-5'l><'col-sm-7'f>>" +
      //         "<'row'<'col-sm-12'tr>>" +
      //         "<'row'<'col-sm-5'i><'col-sm-7'p>>",
      // buttons: [
      //   {
      //     extend: 'excelHtml5',
      //     exportOptions: {
      //         columns: [ 1, 2, 3, 4, 5, 6, 7 ]
      //     }
      //   },
      //   {
      //     extend: 'pdfHtml5',
      //     exportOptions: {
      //         columns: [ 1, 2, 3, 4, 5, 6, 7 ]
      //     }
      //   }
      // ]
    });
    changeNumberOrder();
    //drawTimeline();
    setTimeout(()=>{$('[data-toggle="tooltip"]').tooltip();},400)
    setTimeout(()=>{$('[data-toggle="popover"]').popover({ html: true });},400)
    $('#projectsTable tbody').on('click', 'button', function (event) {
      //var data = tableProjects.row( this ).data();
      debugger
      //alert( 'You clicked on '+data.id+'\'s row' );
      //event.target.offsetParent.id
      if ($(event.currentTarget).hasClass('btn-edit'))
        editProject(event.currentTarget.offsetParent.id);
      else if ($(event.currentTarget).hasClass('btn-delete'))
        deleteProject(event.currentTarget.offsetParent.id);
      // else if ($(event.currentTarget).hasClass('history-popover')){
      //   debugger;
      // }
    });
    $('#projectsTable tbody').on('click', 'a', function (event) {
      //var data = tableProjects.row( this ).data();
      debugger
      //alert( 'You clicked on '+data.id+'\'s row' );
      //event.target.offsetParent.id
       if ($(event.currentTarget).hasClass('history-popover')){
        getProjectChanges(event.currentTarget.offsetParent.id);
      }
    });
  });
  /*
  function onNumberChange(){
    //console.log(dataGet)
    //debugger
    projectNumber = $('#project_number').val();
    var conflict = dataGet.find(d => d.project_number === projectNumber)
    debugger
    if(conflict){
      $('#project_number').removeClass("is-valid").addClass("is-invalid");
      $('#btnAddProject').prop("disabled", true).removeClass("ui-state-enabled").addClass("ui-state-disabled");
    }
    else{
      $('#project_number').removeClass("is-invalid").addClass("is-valid");
      $('#btnAddProject').prop("disabled", false).removeClass("ui-state-disabled").addClass("ui-state-enabled");
    }
  }
  */
});
function onClickChangeActive(event) {
  changeActive(event.data.active);
}
function onClickChangeProject(event) {
  changeProjects(event.data.mode);
}
function onClickChangeCompletion(event) {
  changeCompletion(event.data.comp);
}
function onClickMoveProject(event) {
  moveProjects(event.data.number)
}
function onDataChange(){
  //debugger
  projectStart = $('#project_start').val();
  projectFinish = $('#project_finish').val();
  if(projectStart && projectFinish){
    if(new Date(projectStart).getTime() > new Date(projectFinish).getTime()){
      $('#project_start').val(projectFinish);
    }
    else if(new Date(projectFinish).getTime() < new Date(projectStart).getTime())
      $('#project_finish').val(projectStart);
  }
}
function onDatesNewChange(){
  //debugger
  projectStart = $('#project_start').val();
  projectFinish = $('#project_finish').val();
  if(projectStart && projectFinish){
    if(new Date(projectStart).getTime() > new Date(projectFinish).getTime()){
      //$('#project_finish').val(projectStart);
      $('#project_finish').addClass('is-invalid');
      $('#project_start').addClass('is-invalid');
      $('#btnAddProject').prop('disabled', true);
    }
    else{
      $('#project_finish').removeClass('is-invalid');
      $('#project_start').removeClass('is-invalid');
      $('#btnAddProject').prop('disabled', false);
    }
  }
  else{
    $('#project_finish').removeClass('is-invalid');
    $('#project_start').removeClass('is-invalid');
    $('#btnAddProject').prop('disabled', false);
  }
}
function onFinishChange(){
  //debugger
  projectStart = $('#project_start').val();
  projectFinish = $('#project_finish').val();
  if(projectStart && projectFinish){
    if(new Date(projectFinish).getTime() < new Date(projectStart).getTime())
      $('#project_start').val(projectFinish);
  }
}
//CHANGE PROJECTS TO SEE ONLY ACTIVE OR ALL
function changeActive(seeActive){
  $('#optionAll').removeClass('active');
  $('#optionTrue').removeClass('active');
  $('#optionFalse').removeClass('active');
  activeProject = 0;
  projectsActive = seeActive;
  sessionStorage.sessionProjectActive = projectsActive;
  $.get( "/projects/all", {activeProject}, function( data ) {
    //console.log(data)
    dataGet = data.data;
    //debugger
    //SORT PROJECTS
    var [sortedTmp1, sortedTmp2, tmp3] = sortProjects();
    allRTProjects = sortedTmp1, allSTRProjects = sortedTmp2;
    // dataGet = sortedTmp2.concat(sortedTmp1,tmp3);
    dataGet = sortedTmp1.concat(sortedTmp2,tmp3);
    // FILTER PROJECTS
    var tmp = filterSortedProjects(sortedTmp1, sortedTmp2, tmp3);
    // CUSTOM SORT ON SUBSCRIBERS
    tmp = customSubscribersSortProjects(tmp);
    tmp = customNameSortProjects(tmp);
    projectsSort = tmp;
    //console.log(dataGet[2]);
    tableProjects.clear();
    tableProjects.rows.add(tmp);
    tableProjects.draw(true);
    if(seeActive == 1)
      $('#optionTrue').addClass('active');
    else if(seeActive == 2)
      $('#optionFalse').addClass('active');
    else
      $('#optionAll').addClass('active');
    $('#myPlot').empty();
    drawTimeline();
    changeNumberOrder();
  });
}
function changeCompletion(tempCompletion) {
  $('#optionAllCompletion').removeClass('active');
  $('#optionUnfinished').removeClass('active');
  $('#optionFinished').removeClass('active');
  activeProject = 0;
  projectsCompletion = tempCompletion;
  sessionStorage.sessionProjectsCompletion = projectsCompletion;
  $.get( "/projects/all", {activeProject}, function( data ) {
    //console.log(data)
    dataGet = data.data;
    //debugger
    //SORT PROJECTS
    var [sortedTmp1, sortedTmp2, tmp3] = sortProjects();
    allRTProjects = sortedTmp1, allSTRProjects = sortedTmp2;
    // dataGet = sortedTmp2.concat(sortedTmp1,tmp3);
    dataGet = sortedTmp1.concat(sortedTmp2,tmp3);
    // FILTER PROJECTS
    var tmp = filterSortedProjects(sortedTmp1, sortedTmp2, tmp3);
    // CUSTOM SORT ON SUBSCRIBERS
    tmp = customSubscribersSortProjects(tmp);
    tmp = customNameSortProjects(tmp);
    projectsSort = tmp;
    //console.log(dataGet[2]);
    tableProjects.clear();
    tableProjects.rows.add(tmp);
    tableProjects.draw(true);
    if(tempCompletion == 1)
      $('#optionUnfinished').addClass('active');
    else if(tempCompletion == 2)
      $('#optionFinished').addClass('active');
    else
      $('#optionAllCompletion').addClass('active');
    $('#myPlot').empty();
    //drawTimeline();
    changeNumberOrder();
  });
  if (tempCompletion == '0')
    $('#optionAllCompletion').addClass('active');
  else if(tempCompletion == '1')
    $('#optionUnfinished').addClass('active');
  else if(tempCompletion == '2')
    $('#optionFinished').addClass('active');
}
//CHANGE PROJECTS TO SEE ONLY ACTIVE OR ALL
function changeProjects(seeProjects){
  $('#optionAllProjects').removeClass('active');
  $('#optionRTProjects').removeClass('active');
  $('#optionSTRProjects').removeClass('active');
  activeProject = 0;
  projectsFilter = seeProjects;
  sessionStorage.sessionProjectsFilter = projectsFilter;
  $.get( "/projects/all", {activeProject}, function( data ) {
    //console.log(data)
    dataGet = data.data;
    //debugger
    //SORT PROJECTS
    var [sortedTmp1, sortedTmp2, tmp3] = sortProjects();
    allRTProjects = sortedTmp1, allSTRProjects = sortedTmp2;
    // dataGet = sortedTmp2.concat(sortedTmp1,tmp3);
    dataGet = sortedTmp1.concat(sortedTmp2,tmp3);
    // FILTER PROJECTS
    var tmp = filterSortedProjects(sortedTmp1, sortedTmp2, tmp3);
    // CUSTOM SORT ON SUBSCRIBERS
    tmp = customSubscribersSortProjects(tmp);
    tmp = customNameSortProjects(tmp);
    projectsSort = tmp;
    //console.log(dataGet[2]);
    tableProjects.clear();
    tableProjects.rows.add(tmp);
    tableProjects.draw(true);
    if(seeProjects == 1)
      $('#optionRTProjects').addClass('active');
    else if(seeProjects == 2)
      $('#optionSTRProjects').addClass('active');
    else
      $('#optionAllProjects').addClass('active');
    $('#myPlot').empty();
    //drawTimeline();
    changeNumberOrder();
  });
}
function deleteProject(id){
  //debugger;
  projectId = id;
  //open modal for delete prompt
  $('#modalDeleteProject').modal('toggle');
}
function deleteProjectConfirm(){
  $.post('/projects/delete', {projectId}, function(resp){
    if(resp.success){
      console.log('Successfully deleting project');
      $('#modalDeleteProject').modal('toggle');
      var temp = tableProjects.row('#'+projectId).data();
      temp.active = false;
      tableProjects.row('#'+projectId).data(temp).invalidate().draw(false);
      //ADDING NEW CHANGE TO DB
      addChangeSystem(3,1,projectId);
      //addNewProjectChange(1,3,projectId);
    }
    else{
      console.log('Error on deleting project');
      //console.log(resp);
      $('#modalError').modal('toggle');
    }
  })
}
/*
//CHECK IF PROJECT NUMBER IS NOT USED YET
function onProjectNumberChange(){
  $.get( "/projects/all", function( data ) {
    var allProjects = data.data;
    debugger;
    var number = $('#project_number_edit').val();
    var conflict = allProjects.find(p => p.project_number === number)
    if(conflict){
      if(conflict.project_number != projectNumber){
        $('#project_number_edit').removeClass("is-valid").addClass("is-invalid");
        $('#btnEditProject').prop("disabled", true).removeClass("ui-state-enabled").addClass("ui-state-disabled");
        //debugger;
      }
      else{
        $('#project_number_edit').removeClass("is-invalid").addClass("is-valid");
        $('#btnEditProject').prop("disabled", false).removeClass("ui-state-disabled").addClass("ui-state-enabled");
        //debugger;
      }
    }
    else{
      $('#project_number_edit').removeClass("is-invalid").addClass("is-valid");
      $('#btnEditProject').prop("disabled", false).removeClass("ui-state-disabled").addClass("ui-state-enabled");
    }
    //debugger;
  });
}
*/
//CHECK IF DATES ARE OUT OF ORDER
function onDatesEditChange(){
  var start = $('#project_start_edit').val();
  var finish = $('#project_finish_edit').val();
  if(start && finish){
    if(new Date(start).getTime() > new Date(finish).getTime()){
      //$('#project_finish_edit').val($('#project_start_edit').val());
      $('#project_start_edit').addClass('is-invalid');
      $('#project_finish_edit').addClass('is-invalid');
      $('#btnEditProject').prop('disabled', true);
    }
    else{
      $('#project_start_edit').removeClass('is-invalid');
      $('#project_finish_edit').removeClass('is-invalid');
      $('#btnEditProject').prop('disabled', false);
    }
  }
  else{
    $('#project_start_edit').removeClass('is-invalid');
    $('#project_finish_edit').removeClass('is-invalid');
    $('#btnEditProject').prop('disabled', false);
  }
}
//OPEN MODAL FOR EDIT PROJECT
function editProject(id){
  //debugger;
  projectId = id;
  $.get( "/subscribers/all", function( data ) {
    allSubscribers = data.data;
    $(".select2-subscriber-edit").select2({
      data: allSubscribers,
      tags: true,
      dropdownParent: $('#modalEditProjectForm')
    });
    project = dataGet.find(p => p.id == id);
    projectNumber = project.project_number;
    var tmpPrefix = '';
    if(project.project_number.substring(0,2) == 'RT'){
      $('#projectNumberPrefixEdit').val('RT');
      $('#projectNumberEdit').val(project.project_number.substring(2));
    }
    else if(project.project_number.substring(0,3) == 'STR'){
      $('#projectNumberPrefixEdit').val('STR');
      $('#projectNumberEdit').val(project.project_number.substring(3));
    }
    else{
      $('#projectNumberPrefixEdit').val('RT');
      $('#projectNumberEdit').val(project.project_number);
    }
    $('#project_start_edit').val('');
    $('#project_finish_edit').val('');
    $('#project_number_edit').removeClass("is-invalid");
    $('#project_number_edit').removeClass("is-valid");
    $('#btnEditProject').prop("disabled", false).removeClass("ui-state-disabled").addClass("ui-state-enabled");
    $('#project_number_edit').val(project.project_number);
    $('#project_name_edit').val(project.project_name);
    $('#project_notes_edit').val(project.notes);
    $('#project_subscriber_edit').val(allSubscribers.find(s => s.text == project.name).id).trigger('change');
    if(project.start)
      $('#project_start_edit').val(moment(project.start).format("YYYY-MM-DD"));
    if(project.finish)
      $('#project_finish_edit').val(moment(project.finish).format("YYYY-MM-DD"));
    if(project.active){
      //debugger
      $("#activeEdit").prop('checked', 1);
    }
    else{
      //debugger
      $("#activeEdit").prop('checked', 0);
    }
    onDatesEditChange();
    //debugger;
    onEditProjectNumberChange();
    $("#modalEditProjectForm").modal();
  });
}
function changeNumberOrder(){
  var tmpPrefix = $('#projectNumberPrefix').val();
  if(tmpPrefix == 'RT'){
    var firstNumberedProject = allRTProjects.find(p => !isNaN(p.number_order));
    var tmp = firstNumberedProject ? firstNumberedProject.number_order+1 : '';
    tmp = tmp+'';
    var n = firstNumberedProject ? firstNumberedProject.project_number.split(/([0-9]+)/).filter(Boolean)[1].length : 0;
    for(var i = 0, l = n - tmp.length; i < l; i++) tmp = '0'+tmp;
    $('#projectNumber').val(tmp);
  }
  else if(tmpPrefix == 'STR'){
    var firstNumberedProject = allSTRProjects.find(p => !isNaN(p.number_order));
    var tmp = firstNumberedProject ? firstNumberedProject.number_order+1 : '';
    tmp = tmp+'';
    var n = firstNumberedProject ? firstNumberedProject.project_number.split(/([0-9]+)/).filter(Boolean)[1].length : 0;
    for(var i = 0, l = n - tmp.length; i < l; i++) tmp = '0'+tmp;
    $('#projectNumber').val(tmp);
  }
  onNewProjectNumberChange();
}
function onNewProjectNumberChange(){
  $.get( "/projects/all", function( data ) {
    var allProjects = data.data;
    var tmpNumber = $('#projectNumberPrefix').val() + $('#projectNumber').val();
    if($('#projectNumberPrefix').val() == 'RT'){
      allProjects = allProjects.filter(p => p.project_number.substring(0,2) == 'RT');
    }
    else if($('#projectNumberPrefix').val() == 'STR'){
      allProjects = allProjects.filter(p => p.project_number.substring(0,3) == 'STR');
    }
    var conflictTmp = allProjects.find(c => c.project_number == tmpNumber);
    if(conflictTmp){
      $('#projectNumber').removeClass("is-valid").addClass("is-invalid");
      $('#btnAddProject').prop("disabled", true).removeClass("ui-state-enabled").addClass("ui-state-disabled");
    }
    else{
      $('#projectNumber').removeClass("is-invalid").addClass("is-valid");
      $('#btnAddProject').prop("disabled", false).removeClass("ui-state-disabled").addClass("ui-state-enabled");
    }
  });
}
function onEditProjectNumberChange(){
  $.get( "/projects/all", function( data ) {
    var allProjects = data.data;
    var tmpNumber = $('#projectNumberPrefixEdit').val() + $('#projectNumberEdit').val();
    if($('#projectNumberPrefixEdit').val() == 'RT'){
      allProjects = allProjects.filter(p => p.project_number.substring(0,2) == 'RT');
    }
    else if($('#projectNumberPrefixEdit').val() == 'STR'){
      allProjects = allProjects.filter(p => p.project_number.substring(0,3) == 'STR');
    }
    var conflictTmp = allProjects.find(c => c.project_number == tmpNumber && c.project_number != project.project_number);
    if(conflictTmp){
      $('#projectNumberEdit').removeClass("is-valid").addClass("is-invalid");
      $('#btnEditProject').prop("disabled", true).removeClass("ui-state-enabled").addClass("ui-state-disabled");
    }
    else{
      $('#projectNumberEdit').removeClass("is-invalid").addClass("is-valid");
      $('#btnEditProject').prop("disabled", false).removeClass("ui-state-disabled").addClass("ui-state-enabled");
    }
  });
}
function redoSortProjects(){
  var tmp1 = dataGet.filter(p => p.project_number.substring(0,2) == 'RT' );
  var tmp2 = dataGet.filter(p => p.project_number.substring(0,3) == 'STR' );
  var tmp3 = dataGet.filter(p => p.project_number.substring(0,3) != 'STR' && p.project_number.substring(0,2) != 'RT');
  tmp1.forEach(p => p.number_order = parseInt(p.project_number.substring(2)));
  tmp2.forEach(p => p.number_order = parseInt(p.project_number.substring(3)));
  var sortedTmp1 = tmp1.sort(function(a,b){
    if (isNaN(a.number_order))
      return 1;
    else if (isNaN(b.number_order))
      return -1;
    return b.number_order - a.number_order;
  });
  var sortedTmp2 = tmp2.sort(function(a,b){
    if (isNaN(a.number_order))
      return 1;
    else if (isNaN(b.number_order))
      return -1;
    return b.number_order - a.number_order;
  });
  allRTProjects = sortedTmp1, allSTRProjects = sortedTmp2;
  // dataGet = sortedTmp2.concat(sortedTmp1,tmp3);
  dataGet = sortedTmp1.concat(sortedTmp2,tmp3);
}
function pad(n) {return n < 10 ? "0"+n : n;}
/////////////////////////////PDF/////////////////////////////////////
function openPDFModal(){
  $('#modalPDFForm').modal('toggle');
}
function linkPDFChange() {
  let prefixOption = $('input[name=customRadioPrefix]:checked').val();
  let statusOption = $('input[name=customRadioStatus]:checked').val();
  let startCheckbox = $('#checkboxStartPDF').prop('checked');
  let finishCheckbox = $('#checkboxFinishPDF').prop('checked');
  let completionCheckbox = $('#checkboxCompletionPDF').prop('checked');
  $('#btnToPDF').prop('href', "/projects/pdfs/?showCompletion="+completionCheckbox+"&showStart="+startCheckbox+"&showFinish="+finishCheckbox+"&optionPrefix="+prefixOption+"&optionCompletion="+statusOption+"&optionActive=1")
}
////////////////////CREATE EXCEL FILE (CSV)////////////////////////
function openExcelModal(){
  $('#modalExcelForm').modal('toggle');
}
function toCSV(){
  //debugger;
  //statis radio
  var status = 1;
  if($('#radioAll')[0].checked)
    status = 0;
  else if($('#radioUnfinished')[0].checked)
    status = 1;
  else if($('#radioFinished')[0].checked)
    status = 2;
  //projects info
  var numberCheck = $('#checkboxProjectNumber')[0].checked;
  var subCheck = $('#checkboxSubscriber')[0].checked;
  var startCheck = $('#checkboxStart')[0].checked;
  var finishCheck = $('#checkboxFinish')[0].checked;
  var completionCheck = $('#checkboxCompletion')[0].checked;
  //workers info
  var leaderCheck = $('#checkboxVodja')[0].checked;
  var builderCheck = $('#checkboxKons')[0].checked;
  var mechanicCheck = $('#checkboxStro')[0].checked;
  var electricanCheck = $('#checkboxElek')[0].checked;
  var plcCheck = $('#checkboxPlc')[0].checked;
  var robotCheck = $('#checkboxRobot')[0].checked;
  var otherCheck = $('#checkboxOther')[0].checked;
  //debugger;
  $.get( "/projects/excel", {status, numberCheck, subCheck, startCheck, finishCheck, completionCheck, leaderCheck, builderCheck, mechanicCheck, electricanCheck, plcCheck, robotCheck, otherCheck}, function( data ) {
    excelData = data.data;
    var output = '';
    if(numberCheck) output += 'Št. projekta;';
    output += 'Ime projekta';
    if(subCheck) output += ';Naročnik';
    if(startCheck) output += ';Začetek';
    if(finishCheck) output += ';Konec';
    if(completionCheck) output += ';Odstotek';
    if(leaderCheck) output += ';Vodje';
    if(builderCheck) output += ';Konstrukterji';
    if(mechanicCheck) output += ';Strojniki';
    if(electricanCheck) output += ';Električarji';
    if(plcCheck) output += ';PLC prog.';
    if(robotCheck) output += ';Robot prog.';
    if(otherCheck) output += ';Ostali (nabavnik/tajnik/komercialist)';
    output += '\n';
    for(var i = 0; i < excelData.length; i++){
      var pNumber = '';
      var pSub = '';
      var pStart = '';
      var pFinish = '';
      var pCompletion = '';
      var pLeader = '';
      var pBuilder = '';
      var pMechanic = '';
      var pElectrican = '';
      var pPlc = '';
      var pRobot = '';
      var pOther = '';
      if(numberCheck)
        pNumber = '"'+excelData[i].project_number+'";';
      if(subCheck)
        pSub = ';"'+excelData[i].name+'"';
      if(startCheck){
        if(excelData[i].start){

          pStart = ';"'+new Date(excelData[i].start).toLocaleDateString('sl')+'"';
        }
        else
          pStart = ';/';
      }
      if(finishCheck){
        if(excelData[i].finish){

          pFinish = ';"'+new Date(excelData[i].finish).toLocaleDateString('sl')+'"';
        }
        else
          pFinish = ';/';
      }
      if(completionCheck)
        pCompletion = ';"'+excelData[i].completion+'"';
      if(leaderCheck)
        if(excelData[i].vodja)
          pLeader = ';"'+excelData[i].vodja+'"';
        else
          pLeader = ';/';
      if(builderCheck)
        if(excelData[i].konstrukter)
          pBuilder = ';"'+excelData[i].konstrukter+'"';
        else
          pBuilder = ';/';
      if(mechanicCheck)
        if(excelData[i].strojnik)
          pMechanic = ';"'+excelData[i].strojnik+'"';
        else
          pMechanic = ';/';
      if(electricanCheck)
        if(excelData[i].elektricar)
          pElectrican = ';"'+excelData[i].elektricar+'"';
        else
          pElectrican = ';/';
      if(plcCheck)
        if(excelData[i].plc)
          pPlc = ';"'+excelData[i].plc+'"';
        else
          pPlc = ';/';
      if(robotCheck)
        if(excelData[i].robot)
          pRobot = ';"'+excelData[i].robot+'"';
        else
          pRobot = ';/';
      if(otherCheck)
        if(excelData[i].nabavnik)
          pOther = ';"'+excelData[i].nabavnik+'"';
        else
          pOther = ';/';
      output = output + pNumber+'"'+excelData[i].project_name+'"'+pSub+pStart+pFinish+pCompletion+pLeader+pBuilder+pMechanic+pElectrican+pPlc+pRobot+pOther+'\n';
    }
    //download(filename + ".csv", output);
    download_csv(output);
    $('#modalExcelForm').modal('toggle');
  });
}
function download(filename, text) {
  var options = {
    /*action='downoad' options */
    filename: 'Seznam projektov',
    
    /* action='output' options */
    appendTo: 'body',
    
    /* general options */
    separator: ',',
    newline: '\n',
    quoteFields: true,
    excludeColumns: '',
    excludeRows: ''
  };
  filename = options.filename;
  var element = document.createElement('a');
  element.setAttribute('href', 'data:text/plain;charset=utf-8,' + encodeURIComponent(text));
  element.setAttribute('download', filename);
  
  element.style.display = 'none';
  document.body.appendChild(element);
  
  element.click();
  
  document.body.removeChild(element);
}
function download_csv(csv) {
  //console.log(csv);
  var hiddenElement = document.createElement('a');
  hiddenElement.href = 'data:text/csv;charset=utf-8,' + encodeURI(csv);
  hiddenElement.target = '_blank';
  hiddenElement.download = 'Seznam projektov.csv';
  hiddenElement.click();
}
//SHOW ONLY MY PROJECTS (projects that i am part of)
function changeToMyProjects(){
  //debugger;
  showMyProjects = showMyProjects ? false : true;
  sessionStorage.sessionMyProjectsSwitch = showMyProjects;
  changeActive(projectsActive);
}
var excelData;
var filename;
var options;
///////////////////////END OF EXCEL FILE///////////////////////////
/////////////////////////////////////////////////////////////TIMELINE//////////////////////////////////////////////////////////////////////
function drawTimeline(){
  //debugger;
  makeFirstProjectsVis();
}
// GET CHANGES
function getProjectChanges(id){
  debugger;
  var projectId = id;
  let data = {projectId};
  $.ajax({
    type: 'GET',
    url: '/projects/changes',
    contentType: 'application/json',
    data: data, // access in body
  }).done(function (resp) {
    if(resp.success){
      debugger;
      var projectChanges = resp.changes;
      console.log('Successfully getting all the changes of selected task/service.');
      var element = "";
      var fileLabel = "";
      for (const change of projectChanges) {
        element = new Date(change.date).toLocaleDateString('sl') + ' ' + new Date(change.date).toLocaleTimeString('sl').substring(0,5) + ", " + change.name + " " + change.surname + ', ' + change.status + fileLabel + "<br />" + element;
      }
      $('#projectHistory'+projectId).attr('data-content', element);
      $('#projectHistory'+projectId).popover('show');
        // for(var i=0; i<taskChanges.length; i++){
        //   if(taskChanges[i].id_type == 7)
        //     fileLabel = " datoteko";
        //   else if(taskChanges[i].id_type == 4){
        //     if(taskChanges[i].subtask)
        //       fileLabel = " sporočilo nalogi " + taskChanges[i].subtask;
        //     else
        //       fileLabel = " sporočilo";
        //   }
        //   else if(taskChanges[i].id_type == 3)
        //     fileLabel = " nalogo " + taskChanges[i].subtask;
        //   else if(taskChanges[i].id_type == 5)
        //     fileLabel = " odsotnost";
        //   else
        //     fileLabel = "";
        //   element = new Date(taskChanges[i].date).toLocaleDateString('sl') + ' ' + new Date(taskChanges[i].date).toLocaleTimeString('sl').substring(0,5) + ", " + taskChanges[i].name + " " + taskChanges[i].surname + ', ' + taskChanges[i].status + fileLabel + "<br />" + element;
        // }
        // //$('.mypopover').attr('data-content', element);
        // $('#taskHistory'+taskId).attr('data-content', element);
        // $('#taskHistory'+taskId).popover('show');
        // $('[data-toggle="popover"]').popover({trigger: "hover"}); 
    }
    else{
      $('#modalError').modal('toggle');
    }
  }).fail(function (resp) {
    //console.log('FAIL');
    //debugger;
    $('#modalError').modal('toggle');
  }).always(function (resp) {
    //console.log('ALWAYS');
    //debugger;
  });
}
// COMMON CODE TURNED TO FUNCTIONS
function sortProjects(params) {
  let tmp1 = dataGet.filter(p => p.project_number.substring(0,2) == 'RT' ); // tmp1 is list of projects with RT
  let tmp2 = dataGet.filter(p => p.project_number.substring(0,3) == 'STR' ); // tmp2 is list of projects with STR
  let tmp3 = dataGet.filter(p => p.project_number.substring(0,3) != 'STR' && p.project_number.substring(0,2) != 'RT'); // tmp3 is list of projects without RT or STR
  tmp1.forEach(p => p.number_order = parseInt(p.project_number.substring(2))); // get project number and store it in number_order for every project
  tmp2.forEach(p => p.number_order = parseInt(p.project_number.substring(3))); // get project number and store it in number_order for every project
  let sortedTmp1 = tmp1.sort(function(a,b){
    if (isNaN(a.number_order))
      return 1;
    else if (isNaN(b.number_order))
      return -1;
    return b.number_order - a.number_order;
  });
  let sortedTmp2 = tmp2.sort(function(a,b){
    if (isNaN(a.number_order))
      return 1;
    else if (isNaN(b.number_order))
      return -1;
    return b.number_order - a.number_order;
  });
  return [sortedTmp1, sortedTmp2, tmp3]
}
function filterSortedProjects(sortedTmp1, sortedTmp2, tmp3) {
  let tmp
  //filter project name
  if(projectsFilter == 1) tmp = sortedTmp1;
  else if(projectsFilter == 2) tmp = sortedTmp2;
  else tmp = sortedTmp1.concat(sortedTmp2,tmp3);
  // else tmp = sortedTmp2.concat(sortedTmp1,tmp3);
  //filter project activity
  if(projectsActive == 1) tmp = tmp.filter(p => p.active == true);
  else if(projectsActive == 2) tmp = tmp.filter(p => p.active == false);
  //filter project completion
  if(projectsCompletion == 1) tmp = tmp.filter(p => p.completion != 100);
  else if(projectsCompletion == 2) tmp = tmp.filter(p => p.completion == 100);
  // filter project by my project
  if (showMyProjects) tmp = tmp.filter(p => p.my_project)
  return tmp;
}
function openCustomSortModal() {
  $.get( "/subscribers/all", function( data ) {
    // subscribers
    allSubscribers = data.data;
    $(".select2-subscriber-multi").select2({
      data: allSubscribers,
      multiple: true,
      dropdownParent: $('#modalCustomSubscriberSortForm')
    });
    let customOrderSelection = localStorage.customProjectSort ? localStorage.customProjectSort.split(',') : [];
    // projects
    projectsSort.forEach(p => {
      p.text = p.project_number + ' - ' + p.project_name;
    });
    $(".select2-project-multi").select2({
      data: projectsSort,
      multiple: true,
      dropdownParent: $('#modalCustomSubscriberSortForm')
    });
    let customNameOrderSelection = localStorage.customProjectNameSort ? localStorage.customProjectNameSort.split(',') : [];
    $('#project_subscriber_sort').val(customOrderSelection).trigger('change');
    $('#project_name_sort').val(customNameOrderSelection).trigger('change');
    $("#modalCustomSubscriberSortForm").modal();
  });
}
function customSubscribersSortProjects(tmpProjects) {
  // console.log('uporabnik je izbral narocnike');
  let customSortSelection = localStorage.customProjectSort ? localStorage.customProjectSort.split(',') : [];
  let reorder = [];
  for (const selection of customSortSelection) {
    // console.log(selection);
    let tmp = tmpProjects.filter(p => p.id_subscriber == selection);
    reorder = reorder.concat(tmp);
    tmpProjects = tmpProjects.filter(p => p.id_subscriber != selection);
  }
  tmpProjects = reorder.concat(tmpProjects);
  // debugger;
  return tmpProjects;
}
function customNameSortProjects(tmpProjects) {
  let customNameSortSelection = localStorage.customProjectNameSort ? localStorage.customProjectNameSort.split(',') : [];
  let reorder = [];
  for (const selection of customNameSortSelection) {
    // console.log(selection);
    let tmp = tmpProjects.filter(p => p.id == selection);
    reorder = reorder.concat(tmp);
    tmpProjects = tmpProjects.filter(p => p.id != selection);
  }
  tmpProjects = reorder.concat(tmpProjects);
  // debugger;
  return tmpProjects;
}
var dataViz;
var chart;
//$('#project_start').on('change', onDataChange);
//$('#project_finish').on('change', onDataChange);
var tableProjects;
var activeProject = 0;
var projectsActive = 1;
var projectsFilter = 0;
var projectsCompletion;
var projectsSort;
var showMyProjects = false;
var loggedUser;
var projectId;
var allSubscribers;
var allUsers;
var allRoles;
var project;
var projectNumber;
var dataGet; // all projects
var allRTProjects, allSTRProjects; // all projects with RT or STR prefix seperated
//var subscribers
//var dataGet
var projectStart;
var projectFinish;
//var projectStart = $('#project_start').val();
//var projectFinish = $('#project_finish').val();
//$('#project_number_edit').on('change', onProjectNumberChange);
//$('#project_start_edit').on('change', onStartDateEditChange);
//$('#project_finish_edit').on('change', onFinishDateEditChange);