//IN THIS SCRIPT//
//// opening collaps for timeline, drawing timeline, refreshing timeline, make data for timeline  ////
$(document).ready(function(){
  //call event functions
  $('#timelineBtn').on('click', openTimeline);
  $('#moveLeftProjectBtn').on('click', function (event) {
    moveProject(0.2);
  });
  $('#moveRightProjectBtn').on('click', function (event) {
    moveProject(-0.2);
  });
  $('#homeProjectBtn').on('click', showCurrentWeekProject);
  $('#filtersProjectBtn').on('click', openFilterProjectModal);
  $('#switchDatesBtn').on('click', switchProjectDates);
  $('#dayProjectBtn').on('click', showDayProject);
  $('#weekProjectBtn').on('click', showWeekProject);
  $('#monthProjectBtn').on('click', showMonthProject);
  $('#lowerFontProjectBtn').on('click', lowerFontProject);
  $('#higherFontProjectBtn').on('click', higherFontProject);
  $('#refreshProjectBtn').on('click', refreshTimelineProject);
  $('#resizeProjectBtn').on('click', resizeTimeline);
  $('#applyFilterProjectTL').on('click', applyFilterToTasks);
})
function openTimeline(){
  debugger;
  $('#collapseTimetable').collapse("toggle");
  if(timelineCollapseOpen){
    timelineCollapseOpen = false
  }
  else{
    timelineCollapseOpen = true;
    //call function for drawing
    showProjectTasksVis();
  }
}
var timelineCollapseOpen = false;
//new
function fillSelectForProjects() {
  if (allActiveProjects){//check if select is filled with projects, if not then fill select with project
    if ($('#projectSelectVis option').length == 0){
      var tmp = allActiveProjects.filter(p => p.completion != 100)
      $(".select2-projects-vis").select2({
        data: tmp,
      });
    }
  }
  else{//get data for all active projects
    $.get( "/viz/allprojects", function( data ) {
      //sort data to get correct order of RT numbers
      allActiveProjects = data.data;
      allActiveProjectsData = data.altData;
      var tmp1 = allActiveProjects.filter(p => p.project_number.substring(0,2) == 'RT' );
      var tmp2 = allActiveProjects.filter(p => p.project_number.substring(0,3) == 'STR' );
      var tmp3 = allActiveProjects.filter(p => p.project_number.substring(0,3) != 'STR' && p.project_number.substring(0,2) != 'RT');
      tmp1.forEach(p => p.number_order = parseInt(p.project_number.substring(2)));
      tmp2.forEach(p => p.number_order = parseInt(p.project_number.substring(3)));
      var sortedTmp1 = tmp1.sort(function(a,b){
        if (isNaN(a.number_order))
          return 1;
        else if (isNaN(b.number_order))
          return -1;
        return b.number_order - a.number_order;
      });
      var sortedTmp2 = tmp2.sort(function(a,b){
        if (isNaN(a.number_order))
          return 1;
        else if (isNaN(b.number_order))
          return -1;
        return b.number_order - a.number_order;
      });
      allRTProjects = sortedTmp1, allSTRProjects = sortedTmp2;
      allActiveProjects = sortedTmp2.concat(sortedTmp1,tmp3);
      var tmp = allActiveProjects.filter(p => p.completion != 100)
      $(".select2-projects-vis").select2({
        data: tmp,
      });
    });
  }
}
function showProjectTasksVis() {
  debugger;
  var categoryTask = 0, activeTask = 1, completionTask = 0;
  $.get( "/projects/categories", function( catResp ) {
    if (catResp.success){
      allCategoriesTimeline = catResp.data;
      allCategoriesTimeline.push(allCategoriesTimeline.shift());//move category Brez to end of array
      let data = {projectId, categoryTask, activeTask, completionTask};
      $.ajax({
        type: 'GET',
        url: '/projects/tasks',
        contentType: 'application/x-www-form-urlencoded',
        data: data, // access in body
      }).done(function (resp) {
        debugger;
        if (resp.success){
          allProjectTasks = resp.data;
          //all data is collected, apply filters and show graph
          drawProjectTasks();
        }
        else{
          $('#modalError').modal('toggle');  
        }
      }).fail(function (resp) {
        $('#modalError').modal('toggle');
      }).always(function (resp) {
      });
    }
  });
}
function drawProjectTasks() {
  //apply completion
  if (radioAllTasks)
    allShownTasks = allProjectTasks;
  else if (radioUnfinishedTasks)
    allShownTasks = allProjectTasks.filter(t => t.completion != 100);
  else if (radioFinishedTasks)
  allShownTasks = allProjectTasks.filter(t => t.completion == 100);
  //apply categories
  if (!categoryBrez)
    allShownTasks = allShownTasks.filter(t => t.category != 'Brez');
  if (!categoryNabava)
    allShownTasks = allShownTasks.filter(t => t.category != 'Nabava');
  if (!categoryKonstrukcija)
    allShownTasks = allShownTasks.filter(t => t.category != 'Konstrukcija');
  if (!categoryStrojna)
    allShownTasks = allShownTasks.filter(t => t.category != 'Strojna izdelava');
  if (!categoryElektro)
    allShownTasks = allShownTasks.filter(t => t.category != 'Elektro izdelava');
  if (!categoryMontaza)
    allShownTasks = allShownTasks.filter(t => t.category != 'Montaža');
  if (!categoryProgramiranje)
    allShownTasks = allShownTasks.filter(t => t.category != 'Programiranje');
  if (!categoryStrojnaRoboteh)
    allShownTasks = allShownTasks.filter(t => t.category != 'Strojna montaža - Roboteh');
  if (!categoryStrojnaStranka)
    allShownTasks = allShownTasks.filter(t => t.category != 'Strojna montaža - stranka');
  if (!categoryElektroRoboteh)
    allShownTasks = allShownTasks.filter(t => t.category != 'Elektro montaža - Roboteh');
  if (!categoryElektroStranka)
    allShownTasks = allShownTasks.filter(t => t.category != 'Elektro montaža - stranka');
  if (!categoryElektroPro)
    allShownTasks = allShownTasks.filter(t => t.category != 'Elektro proektiranje');
  if (!categoryDobava)
    allShownTasks = allShownTasks.filter(t => t.category != 'Dobava');
  if (!categorySAT)
    allShownTasks = allShownTasks.filter(t => t.category != 'SAT');
  if (!categoryFAT)
    allShownTasks = allShownTasks.filter(t => t.category != 'FAT');
  if (!categoryZagon)
    allShownTasks = allShownTasks.filter(t => t.category != 'Zagon');
  if (!categoryNaklad)
    allShownTasks = allShownTasks.filter(t => t.category != 'Naklad');
  //find min and max date and reshape/correctify tasks so vis.js will show tasks correct way (rename task_start and task_finish to start and end; make content and title)
  var minDate, maxDate;
  allShownTasks = allShownTasks.filter(t => !(!t.task_start && !t.task_finish));
  if (allShownTasks && allShownTasks.length > 0){
    minDate = new Date(allShownTasks[0].task_start);
    maxDate = new Date(allShownTasks[0].task_finish);
  }
  else{
    minDate = new Date();
    maxDate = new Date();
  }
  allShownTasks.forEach(item => {
    item.start = item.task_start ? new Date(item.task_start) : null;
    item.end = item.task_finish ? new Date(item.task_finish) : null;
    //correct time to full day
    if (item.start && resetStartEndTime)
      item.start = new Date(item.start).setHours(0,0,0,0);
    if (item.end && resetStartEndTime)
      item.end = new Date(item.end).setHours(24,0,0,0);
      
    item.content = item.task_name;
    var workers = (item.workers) ? item.workers : 'brez delavcev';
    item.title = item.task_name + ' (' + workers + ') ' + item.completion + '%';
    if (item.category == 'Brez'){
      item.group = 1;
      item.visibleFrameTemplate = '<div class=""><div class="progress-vis progress-brez w-' + item.completion + '"></div></div>';
    }
    if (item.category == 'Nabava'){
      item.group = 2;
      item.className = 'purchase'
      item.visibleFrameTemplate = '<div class=""><div class="progress-vis progress-purchase w-' + item.completion + '"></div></div>';
    }
    if (item.category == 'Konstrukcija'){
      item.group = 3;
      item.className = 'builder';
      item.visibleFrameTemplate = '<div class=""><div class="progress-vis progress-builder w-' + item.completion + '"></div></div>';
    }
    if (item.category == 'Strojna izdelava'){
      item.group = 4;
      item.className = 'mechanic';
      item.visibleFrameTemplate = '<div class=""><div class="progress-vis progress-mechanic w-' + item.completion + '"></div></div>';
    }
    if (item.category == 'Elektro izdelava'){
      item.group = 5;
      item.className = 'electrican';
      item.visibleFrameTemplate = '<div class=""><div class="progress-vis progress-electrican w-' + item.completion + '"></div></div>';
    }
    if (item.category == 'Montaža'){
      item.group = 6;
      item.className = 'assembly';
      item.visibleFrameTemplate = '<div class=""><div class="progress-vis progress-assembly w-' + item.completion + '"></div></div>';
    }
    if (item.category == 'Programiranje'){
      item.group = 7;
      item.className = 'plc';
      item.visibleFrameTemplate = '<div class=""><div class="progress-vis progress-plc w-' + item.completion + '"></div></div>';
    }
    if (item.category == 'Strojna montaža - Roboteh'){
      item.group = 8;
      item.className = 'mechanic';
      item.visibleFrameTemplate = '<div class=""><div class="progress-vis progress-mechanic w-' + item.completion + '"></div></div>';
    }
    if (item.category == 'Strojna montaža - stranka'){
      item.group = 9;
      item.className = 'mechanic';
      item.visibleFrameTemplate = '<div class=""><div class="progress-vis progress-mechanic w-' + item.completion + '"></div></div>';
    }
    if (item.category == 'Elektro montaža - Roboteh'){
      item.group = 10;
      item.className = 'electrican';
      item.visibleFrameTemplate = '<div class=""><div class="progress-vis progress-electrican w-' + item.completion + '"></div></div>';
    }
    if (item.category == 'Elektro montaža - stranka'){
      item.group = 11;
      item.className = 'electrican';
      item.visibleFrameTemplate = '<div class=""><div class="progress-vis progress-electrican w-' + item.completion + '"></div></div>';
    }
    if (item.category == 'Elektro proektiranje'){
      item.group = 12;
      item.className = 'electrican';
      item.visibleFrameTemplate = '<div class=""><div class="progress-vis progress-electrican w-' + item.completion + '"></div></div>';
    }
    if (item.category == 'Dobava'){
      item.group = 13;
      item.className = 'purchase';
      item.visibleFrameTemplate = '<div class=""><div class="progress-vis progress-purchase w-' + item.completion + '"></div></div>';
    }
    if (item.category == 'SAT'){
      item.group = 14;
      // item.className = 'purchase';
      item.visibleFrameTemplate = '<div class=""><div class="progress-vis w-' + item.completion + '"></div></div>';
    }
    if (item.category == 'FAT'){
      item.group = 15;
      // item.className = 'purchase';
      item.visibleFrameTemplate = '<div class=""><div class="progress-vis w-' + item.completion + '"></div></div>';
    }
    if (item.category == 'Zagon'){
      item.group = 16;
      // item.className = 'purchase';
      item.visibleFrameTemplate = '<div class=""><div class="progress-vis w-' + item.completion + '"></div></div>';
    }
    if (item.category == 'Naklad'){
      item.group = 17;
      // item.className = 'purchase';
      item.visibleFrameTemplate = '<div class=""><div class="progress-vis w-' + item.completion + '"></div></div>';
    }
    if (item.completion == 100 && ((!item.start || !item.end)))
      item.className += ' completed'; 
    if (!item.start && item.end){
      item.start = item.end;
      item.end = undefined;
    }
    //FIND MIN START AND MAX END DATES 
    if (new Date(item.start) < new Date(minDate)) minDate = item.start;
    if (new Date(item.end) > new Date(maxDate)) maxDate = item.end;

  })
  allCategoriesTimeline.forEach((category,i)=>{
    category.content = category.text;
    if (i % 2 == 0){
      //debugger;
      category.style =  "background: rgba(82, 226, 233, 0.2);";
      allShownTasks.push({
        group : category.id,
        start : moment(minDate).add(-100, 'days').format("YYYY-MM-DD") + ' 00:00',
        end : moment(maxDate).add(100, 'days').format("YYYY-MM-DD") + ' 23:59',
        type : 'background',
        className : 'odd'
      });
    }
  })
  //if graph exist only redraw timeline with new data else make new graph
  if ($('#projectVis').html()){
    timelineProject.setItems(allShownTasks);
  }
  else{
    var container = document.getElementById("projectVis");
    var options = {
      visibleFrameTemplate: function (item) {
        if (item.visibleFrameTemplate) {
          return item.visibleFrameTemplate;
        }
      },
      orientation: {
        axis: 'both'
      },
      locale: "sl",
      format: {
        minorLabels: {
          millisecond:'SSS',
          second:     's',
          minute:     'HH:mm',
          hour:       'HH:mm',
          weekday:    'ddd D',
          day:        'D [KW]W',
          week:       'W',
          month:      'MMM [KW]W',
          year:       'YYYY'
        },
        majorLabels: {
          millisecond:'HH:mm:ss',
          second:     'D MMMM HH:mm [KW]W',
          minute:     'ddd D MMMM [KW]W',
          hour:       'ddd D MMMM [KW]W',
          weekday:    'MMMM YYYY [KW]W',
          day:        'MMMM YYYY [KW]W',
          week:       'MMMM YYYY [KW]W',
          month:      'YYYY',
          year:       ' '
        }
      }
    };
    timelineProject = new vis.Timeline(container, allShownTasks, allCategoriesTimeline, options);
    //var today = new Date();
    setTimeout(()=>{
      setFontSize();
      setTimeout(()=>{
        homeRangeProject = timelineProject.getWindow();
        drawProjectDatesOnTimeline();
      },300);
    },1000)
  }
}
function resizeTimeline() {
  if ($('#timelineCollapse').hasClass('row')){
    $('#timelineCollapse').removeClass('row');
    $('#timelineCollapse').addClass('full-width');
  }
  else{
    $('#timelineCollapse').removeClass('full-width');
    $('#timelineCollapse').addClass('row');
  }
}
function openFilterProjectModal() {
  $('#modalFilterProjectTasks').modal('toggle');
}
function applyFilterToTasks() {
  //filter data and if graph is drawn then redraw allShownTasks
  categoryNabava = $('#checkboxNabavaFilter').prop('checked') ? true : false;
  categoryKonstrukcija = $('#checkboxKonstrukcijaFilter').prop('checked') ? true : false;
  categoryStrojna = $('#checkboxStrIzdFilter').prop('checked') ? true : false;
  categoryElektro = $('#checkboxEleIzdFilter').prop('checked') ? true : false;
  categoryMontaza = $('#checkboxMontazaFilter').prop('checked') ? true : false;
  categoryProgramiranje = $('#checkboxProgFilter').prop('checked') ? true : false;
  categoryBrez = $('#checkboxBrezFilter').prop('checked') ? true : false;
  categoryStrojnaRoboteh = $('#checkboxStrRobotehFilter').prop('checked') ? true : false;
  categoryStrojnaStranka = $('#checkboxStrStrankaFilter').prop('checked') ? true : false;
  categoryElektroRoboteh = $('#checkboxEleRobotehFilter').prop('checked') ? true : false;
  categoryElektroStranka = $('#checkboxEleStrankaFilter').prop('checked') ? true : false;
  categoryElektroPro = $('#checkboxEleProFilter').prop('checked') ? true : false;
  categoryDobava = $('#checkboxDobavaFilter').prop('checked') ? true : false;
  categorySAT = $('#checkboxSATFilter').prop('checked') ? true : false;
  categoryFAT = $('#checkboxFATFilter').prop('checked') ? true : false;
  categoryZagon = $('#checkboxZagonFilter').prop('checked') ? true : false;
  categoryNaklad = $('#checkboxNakladFilter').prop('checked') ? true : false;

  radioAllTasks = $('#radioAllTasks').prop('checked') ? true : false;
  radioFinishedTasks = $('#radioFinishedTasks').prop('checked') ? true : false;
  radioUnfinishedTasks = $('#radioUnfinishedTasks').prop('checked') ? true : false;

  drawProjectTasks();
  $('#modalFilterProjectTasks').modal('toggle');
}
function refreshTimelineProject() {
  showProjectTasksVis();
}
function lowerFontProject(){
  $('.vis-panel').css('font-size', --fontSize);
  if(timelineProject){
    //moveProjects(0.001);
    timelineProject.redraw()
  }
}
function higherFontProject(){
  $('.vis-panel').css('font-size', ++fontSize);
  if(timelineProject){
    timelineProject.redraw()
  }
}
function moveProject(percentage) {
  var range = timelineProject.getWindow();
  var interval = range.end - range.start;

  timelineProject.setWindow({
    start: range.start.valueOf() - interval * percentage,
    end: range.end.valueOf() - interval * percentage,
  });
}
function showCurrentWeekProject(){
  //var today = new Date();
  timelineProject.setWindow({
    start: homeRangeProject.start.valueOf(),
    end: homeRangeProject.end.valueOf(),
  });
}
function showDayProject(){
  var today = new Date();

  setTimeout(()=>{
    timelineProject.setWindow(moment().format("YYYY-MM-DD"), moment().add(1, 'days').format("YYYY-MM-DD"));
  },100);
}
function showWeekProject(){
  var today = new Date();

  setTimeout(()=>{
    timelineProject.setWindow(moment().format("YYYY-MM-DD"), moment().add(7, 'days').format("YYYY-MM-DD"));
  },100);
}
function showMonthProject(){
  var today = new Date();

  setTimeout(()=>{
    timelineProject.setWindow(moment().format("YYYY-MM-DD"), moment().add(30, 'days').format("YYYY-MM-DD"));
  },100);
}
function setFontSize(){
  $('.vis-panel').css('font-size', fontSize);
  if(timelineProject){
    //moveProjects(0.001);
    timelineProject.redraw()
  }
}
function addDays(date, days) {
  var result = new Date(date);
  result.setDate(result.getDate() + days);
  return result;
}
function drawProjectDatesOnTimeline() {
  for(var i = 0, l = projectDates.length; i < l; i++){
    addNewMarker(projectDates[i].id, projectDates[i].date, projectDates[i].name);
  }
}
function removeAllMarkers() {
  for(var i = 0, l = projectDates.length; i < l; i++){
    removeMarker(projectDates[i].id);
  }
}
function addNewMarker(id, date, marker) {
  timelineProject.addCustomTime(new Date(date), id);
  timelineProject.setCustomTimeMarker(marker, id);
}
function removeMarker(id) {
  timelineProject.removeCustomTime(id);
}
function switchProjectDates() {
  if (showProjectDates){
    showProjectDates = false;
    removeAllMarkers();
    $('#switchDatesBtn').removeClass('active');
  }
  else{
    showProjectDates = true;
    drawProjectDatesOnTimeline();
    $('#switchDatesBtn').addClass('active');
  }
}
var allCategoriesTimeline;
var allProjectTasks, allShownTasks;
var timelineProject;
var categoryBrez = true, categoryNabava = true, categoryKonstrukcija = true, categoryStrojna = true, categoryElektro = true, categoryMontaza = true, categoryProgramiranje = true, categoryStrojnaRoboteh = true, categoryStrojnaStranka = true, categoryElektroRoboteh = true, categoryElektroStranka = true, categoryElektroPro = true, categoryDobava = true, categorySAT = true, categoryFAT = true, categoryZagon = true, categoryNaklad = true, radioAllTasks = false, radioUnfinishedTasks = true, radioFinishedTasks = false;
var fontSize = 14;
var homeRangeProject;
var showProjectDates = true;
var resetStartEndTime = true;