$(function(){
  window.mobiscroll4 = mobiscroll;
  $('#makeDocBPPDF').submit(function (e) {
    e.preventDefault();
    $form = $(this);
    debugger;
    var givenDate  = ($('#docBPGivenDate').val()) ? new Date($('#docBPGivenDate').val()).toLocaleDateString('sl') : '';
    var takenDate = ($('#docBPTakenDate').val()) ? new Date($('#docBPTakenDate').val()).toLocaleDateString('sl') : '';
    var viewDate = ($('#docBPDateView').val()) ? new Date($('#docBPDateView').val()).toLocaleDateString('sl') : '';
    var docBPLeader = $('#docBPLeader').val();
    var docBPTaker = $('#docBPTaker').val();
    var docBPGiver = $('#docBPGiver').val();
    var docBPProject = $('#docBPProject').val();
    var docBPCount = $('#docBPCountCheck').prop('checked') ? $('#docBPCount').val() : '';
    var givenDateCheck = $('#docBPGivenDateCheck').prop('checked');
    var takenDateCheck = $('#docBPTakenDateCheck').prop('checked');
    var viewDateCheck = $('#docBPDateViewCheck').prop('checked');

    let customSortBP = $('#customSortSwitch').prop('checked');
    let allActiveBuildParts = customSortBP ? customBuildPartsForDoc : buildPartsForDoc.filter(bp => bp.active == true);
    
    let data = {docBPLeader, docBPTaker, docBPGiver, docBPProject, givenDate, takenDate, viewDate, docBPCount, allActiveBuildParts, givenDateCheck, takenDateCheck, viewDateCheck, customSortBP};
    $.ajax({
      type: 'POST',
      url: '/projects/pdfs/buildparts/',
      contentType: 'application/json',
      data: JSON.stringify(data), // access in query
      xhrFields: {
        responseType: 'blob' 
     }
    }).done(function (resp) {
      //debugger;
      const file = new Blob([resp], {type: 'application/pdf'});
      // process to auto download it
      const fileURL = URL.createObjectURL(file);
      const link = document.createElement('a');
      link.href = fileURL;
      link.download = 'Predaja - '+$('#docBPProject').val()+'.pdf';
      link.click();

    }).fail(function (resp) {
      //console.log('FAIL');
      //debugger;
      $('#modalError').modal('toggle');
    }).always(function (resp) {
      //console.log('ALWAYS');
      //debugger;
    });
  })

  //add event call functions
  $('#btnStartSelectionBP').on('click', startSelectingBPForDoc);
  $('#btnCancelSelection').on('click', cancelSelectionBPForDoc);
  $('#btnConfSelection').on('click', confSelectionBPForDoc);
  $('#btnCancelSelection').addClass('d-none');
  $('#btnConfSelection').addClass('d-none');
  $('#customSortSwitch').parent().addClass('d-none');
  $('#btnMakeDocCSBPPDF').on('click', confCreateCustomSortBP);
})

function startSelectingBPForDoc() {
  $('#btnStartSelectionBP').addClass('d-none');
  $('#btnCancelSelection').removeClass('d-none');
  $('#btnConfSelection').removeClass('d-none');
  $('#customSortSwitch').parent().removeClass('d-none');
  // let confAndCancelButtons = `<button class="btn btn-danger ml-auto mr-2" id="btnCancelSelection" onclick="cancelSelectionBPForDoc()" data-toggle="tooltip" data-original-title="Prekliči skupno dokumentacijo"><i class="fas fa-times fa-lg"></i></button>
  // <button class="btn btn-success mr-n3" id="btnConfSelection" onclick="confSelectionBPForDoc()" data-toggle="tooltip" data-original-title="Potrdi skupno dokumentacijo"><i class="fas fa-check fa-lg"></i></button>`;
  // $('#btnCollapseMod').after(confAndCancelButtons);
  addBPSelection = true;
  drawTasks(activePage);
  debugger;
}
function confSelectionBPForDoc() {
  // get all build parts for tasks id that will be used for documentation
  buildPartsForDoc = [];
  let data = {tasksForBPDoc, buildPartsForBPDoc};
  $.ajax({
    type: 'GET',
    url: '/buildparts/multiple',
    contentType: 'application/json',
    data: data, // access in query
  }).done(function (resp) {
    if(resp.success){
      debugger;
      for (let i = 0; i < resp.buildParts.length; i++) {
        const buildPartsOfTask = resp.buildParts[i];
        buildPartsForDoc = buildPartsForDoc.concat(buildPartsOfTask);
        if (buildPartsOfTask.length == 0){
          let tmpTaskWithNoParts = allTasks.find(t => t.id == tasksForBPDoc[i]);
          if (tmpTaskWithNoParts){
            buildPartsForDoc.push({active: true, name: tmpTaskWithNoParts.task_note, standard: tmpTaskWithNoParts.task_name, note: null, sestav: false, tag: null});
          }
        }
      }
      debugger;
      let sort = localStorage.buildPartsSort ? localStorage.buildPartsSort : 'standard';
      let order = localStorage.buildPartsOrder ? localStorage.buildPartsOrder : 'desc';
      buildPartsForDoc.sort(compareValues(sort, order));
      //buildPartsForDoc = allActiveBuildParts;
      closeSelection = true;
      debugger;
      var projectName = $('#pinfo_number').html()+' '+$('#pinfo_name').html();
      var leaderName = '';
      var today = moment().format("YYYY-MM-DD");
      var user = loggedUser.name + ' ' + loggedUser.surname;
      var leaders = $('row:contains("vodja projekta")');
      if(leaders.length > 0){
        var leaderListId = leaders[0].id.match(/\d+/)[0];
        leaderName = $('#workerName'+leaderListId).html();
      }
      debugger;
      $('#docBPProject').val(projectName);
      $('#docBPLeader').val(leaderName);
      $('#docBPGiver').val(user);
      $('#docBPTaker').val('Brane Klinc');
      $('#docBPGivenDate').val(today);
      $('#docBPTakenDate').val(today);
      $('#docBPDateView').val(today);
      // if custom sort is enabled then make list and open its modal
      if ($('#customSortSwitch').prop('checked')){
        createCustomBPList();
        $('#modalBPSort').modal('toggle');
      }
      else{
        $('#modalMakeDocBPPDF').modal('toggle');
      }
      //var taskBuildParts = resp.buildParts;
      //selectionBuildParts = selectionBuildParts.concat(selectionBuildParts, taskBuildParts);
    }
    else{
      $('#modalError').modal('toggle');
    }
  }).fail(function (resp) {
    //console.log('FAIL');
    //debugger;
    $('#modalError').modal('toggle');
  }).always(function (resp) {
    //console.log('ALWAYS');
    //debugger;
  });
  // for (let i = 0; i < tasksForBPDoc.length; i++) {
  //   let id = tasksForBPDoc[i];
  //   let data = {taskId:id};
  // }
  // $('#btnCancelSelection').remove();
  // $('#btnConfSelection').remove();
  // let startSelectionButton = `<button class="btn btn-success ml-auto mr-n4" id="btnStartSelectionBP" onclick="startSelectingBPForDoc()"><i class="fas fa-pencil-ruler fa-lg"></i><i class="fas fa-arrow-right fa-lg"></i><i class="fas fa-file-pdf fa-lg"></i></button>`;
  // $('#btnCollapseMod').after(startSelectionButton);
  //debugger;
}
function cancelSelectionBPForDoc() {
  // $('#btnCancelSelection').remove();
  // $('#btnConfSelection').remove();
  // let startSelectionButton = `<button class="btn btn-success ml-auto mr-n4" id="btnStartSelectionBP" onclick="startSelectingBPForDoc()" data-toggle="tooltip" data-original-title="Predaja dokumentacije v izdelavo"><i class="fas fa-pencil-ruler fa-lg"></i><i class="fas fa-arrow-right fa-lg"></i><i class="fas fa-file-pdf fa-lg"></i></button>`;
  // $('#btnCollapseMod').after(startSelectionButton);
  $('#btnStartSelectionBP').removeClass('d-none');
  $('#btnCancelSelection').addClass('d-none');
  $('#btnConfSelection').addClass('d-none');
  $('#customSortSwitch').parent().addClass('d-none');
  addBPSelection = false;
  drawTasks(activePage);
  debugger;
}
function onEventAddBPForDoc(event) {
  addBPForDoc(event.data.id);
}
function addBPForDoc(id) {
  debugger;
  $('#selectForBPDoc'+id).prop('checked') ? tasksForBPDoc.push(id) : tasksForBPDoc = tasksForBPDoc.filter(t => t != id);
  if($('#selectForBPDoc'+id).prop('checked') && $('#collapseTaskBuildParts'+id).hasClass('show') && id == activeTaskId){
    for (const bp of allBuildParts) {
      $('#selectBPForBPDoc'+bp.id).prop('checked', true);
    }
    //remove id and its array from buildpartsForBPDoc
    buildPartsForBPDoc = buildPartsForBPDoc.filter(a => a.id != id);
  }
  else if($('#collapseTaskBuildParts'+id).hasClass('show') && id == activeTaskId){
    //check which one are already selected in buildpartsForBPDoc
    buildPartsForBPDoc = buildPartsForBPDoc.filter(a => a.id != activeTaskId);
    for (const bp of allBuildParts) {
      $('#selectBPForBPDoc'+bp.id).prop('checked', false);
    }
  }
}
function addSpecificBPForDoc(id) {
  if ($('#selectBPForBPDoc'+id).prop('checked')){
    let tmp = buildPartsForBPDoc.find(a => a.id == activeTaskId)
    if (tmp){//some build parts are already selected, add it if not full, if full remove this taskId build parts and and taskId to tasksForBPDoc
      tmp.buildParts.push(id);
      if (tmp.buildParts.length == allBuildParts.filter(bp=> bp.active).length){
        buildPartsForBPDoc = buildPartsForBPDoc.filter(a => a.id != activeTaskId);
        tasksForBPDoc.push(activeTaskId);
        $('#selectForBPDoc'+activeTaskId).prop('checked', true);
      }
    }
    else{//none parts are selected yet, create new one with active taskId and this bp id
      buildPartsForBPDoc.push({id:activeTaskId, buildParts:[id]})
    }
  }
  else{
    // if($('#selectForBPDoc'+id).prop('checked', true)){

    // }
    let tmp = buildPartsForBPDoc.find(a => a.id == activeTaskId)
    if (tmp){//some build parts are selected, remove id from selected parts, if length is 0 then remove entry from buildPartsForBPDoc
      tmp.buildParts = tmp.buildParts.filter(a => a != id)
      if (tmp.buildParts.length == 0){
        buildPartsForBPDoc = buildPartsForBPDoc.filter(a => a.id != activeTaskId);
      }
    }
    else{//all build parts were selected, task id from tasksForBPDoc needs to be removed and buildparts with all active build parts has do be added to new entry in buildPartsForBPDoc
      $('#selectForBPDoc'+activeTaskId).prop('checked', false);
      tasksForBPDoc = tasksForBPDoc.filter(t => t != activeTaskId);
      let tmpActiveBuildParts = allBuildParts.filter(bp => bp.active && bp.id != id);
      let selectedArrayBuildParts = [];
      tmpActiveBuildParts.forEach(bp => {selectedArrayBuildParts.push(bp.id)});
      buildPartsForBPDoc.push({id:activeTaskId, buildParts:selectedArrayBuildParts});
    }
  }
}
function createBPDoc(id){
  debugger;
  buildPartsForDoc = allBuildParts;
  closeSelection = false;
  var projectName = $('#pinfo_number').html()+' '+$('#pinfo_name').html();
  var leaderName = '';
  var today = moment().format("YYYY-MM-DD");
  var user = loggedUser.name + ' ' + loggedUser.surname;
  var leaders = $('row:contains("vodja projekta")');
  if(leaders.length > 0){
    var leaderListId = leaders[0].id.match(/\d+/)[0];
    leaderName = $('#workerName'+leaderListId).html();
  }
  debugger;
  $('#docBPProject').val(projectName);
  $('#docBPLeader').val(leaderName);
  $('#docBPGiver').val(user);
  $('#docBPTaker').val('Brane Klinc');
  $('#docBPGivenDate').val(today);
  $('#docBPTakenDate').val(today);
  $('#docBPDateView').val(today);
  $('#modalMakeDocBPPDF').modal('toggle');
}
function createBPCSV(id) {
  debugger;
  let csvOutput = 'Pozicija:;Opis:;Številka risbe:;Opomba:\n';
  let csvFileOutput = [['Pozicija:', 'Opis:', 'Številka risbe:', 'Opomba:']];
  let allActiveBuildParts = allBuildParts.filter(bp => bp.active == true);
  let i = 1;
  for (const buildPart of allActiveBuildParts) {
    let position = '"'+ (i++) +'"';
    let name = '"'+buildPart.name+'"';
    let standard = '"'+buildPart.standard+'"';
    let note = buildPart.note ?  '"'+buildPart.note+'"' : '';
    csvOutput = csvOutput + position + name + standard + note + '\n';
    csvFileOutput.push([position, name, standard, note]);
  }
  convertToCsv('Predaja dokumentacije.csv', csvFileOutput);
}
function convertToCsv(fName, rows) {
  var csv = '';
  for (var i = 0; i < rows.length; i++) {
      var row = rows[i];
      for (var j = 0; j < row.length; j++) {
          var val = row[j] === null ? '' : row[j].toString();
          val = val.replace(/\t/gi, " ");
          if (j > 0)
              csv += '\t';
          csv += val;
      }
      csv += '\n';
  }

  // for UTF-16
  var cCode, bArr = [];
  bArr.push(255, 254);
  for (var i = 0; i < csv.length; ++i) {
      cCode = csv.charCodeAt(i);
      bArr.push(cCode & 0xff);
      bArr.push(cCode / 256 >>> 0);
  }

  var blob = new Blob([new Uint8Array(bArr)], { type: 'text/csv;charset=UTF-16LE;' });
  if (navigator.msSaveBlob) {
      navigator.msSaveBlob(blob, fName);
  } else {
      var link = document.createElement("a");
      if (link.download !== undefined) {
          var url = window.URL.createObjectURL(blob);
          link.setAttribute("href", url);
          link.setAttribute("download", fName);
          link.style.visibility = 'hidden';
          document.body.appendChild(link);
          link.click();
          document.body.removeChild(link);
          window.URL.revokeObjectURL(url);
      }
  }
}
// create build part custom order list
function createCustomBPList () {
  $('#root').empty();
  mobiscroll4.listview('#root', {
    theme: 'ios',
    swipe: false,
    sortable: {
      handle: 'right'
    },
    enhance: true,
    backText: 'Nazaj'
  });
  let kosovnica = buildPartsForDoc.filter(bp => bp.active == true);
  kosovnica.sort(compareValues('id', 'asc'));
  let kosovnicaBrezParentID = kosovnica.filter(k => !k.parent_id);
  let kosovnicaSParentID = kosovnica.filter(k => k.parent_id);
  kosovnica = kosovnicaBrezParentID.concat(kosovnicaSParentID); // the most top id can have lower id in case of multiple file input
  for (const kos of kosovnica) {
    var element;
    var hasChildren = kosovnica.find(k => k.parent_id == kos.id);
    var childrenElement = hasChildren ? `<ul id="sestav` + kos.id + `"></ul>` : '';
    var noteLabel = kos.note ? kos.note : '';
    element = `<li id="kos` + kos.id + `">
      <div class="row">
      </div>
      <label class="col-6">` + kos.standard + `</label>
      <label class="col-6">` + kos.name + `</label>
      <p>` + noteLabel + `</p>
      ` + childrenElement+ `
    </li>`;
    if (kos.parent_id == null){
      // console.log('Kos brez starsa: ' + kos.standard);
      $('#root').append(element);
    }
    else{
      document.getElementById('sestav' + kos.parent_id) ? $('#sestav' + kos.parent_id).append(element) : $('#root').append(element);
    }
  }
  mobiscroll4.listview('#root', {
    theme: 'ios',
    swipe: false,
    sortable: {
      handle: 'right'
    },
    enhance: true,
    backText: 'Nazaj'
  });
  $('.mbsc-ic-ion-ios7-arrow-forward').append('<i class="fas fa-angle-right"></i>').removeClass('mbsc-ic-ion-ios7-arrow-forward');
  $('.mbsc-ic-ion-ios7-arrow-back').append('<i class="fas fa-angle-left"></i>').removeClass('mbsc-ic-ion-ios7-arrow-back');
  // setTimeout(() => {
  // }, 300);
}
// user has confirm custom sort build parts, call createCustomBPObject with #root and open modal for creating BP documentation pdf
function confCreateCustomSortBP(){
  $('#modalBPSort').modal('toggle');

  customBuildPartsForDoc = [];
  createCustomBPObject($('#root').children(), 'root');
  
  $('#modalMakeDocBPPDF').modal('toggle');
}
// reacreate build parts from custom sort list and make object to inject into post call for confirming bp for pdf
function createCustomBPObject (input, parentName) {
  // for of in pridobim id-je preko $('#root').children()[0].id.substring(3)
  // console.log(input)
  for (const kos of input) {
    if (kos.id){
      console.log('dodaj kos: ' + kos.id);
      let buildPart = buildPartsForDoc.find(bp => bp.id == kos.id.substring(3));
      if (buildPart) customBuildPartsForDoc.push(buildPart);
      if ($('#sestav' + kos.id.substring(3)).length){
        // kos has children, don't make empty line and call itself with its children
        console.log('zacetek podsestava ' + kos.id.substring(3));
        // customBuildPartsForDoc.push({});
        createCustomBPObject($('#sestav' + kos.id.substring(3)).children(), kos.id);
      }
    }
  }
  // end of array/sestav so make empty row/line
  console.log('prazna vrstica - konec podsestava ' + parentName);
  if (parentName != 'root')
    customBuildPartsForDoc.push({});
}
var addBPSelection = false;
let tasksForBPDoc = [];
let buildPartsForBPDoc = [];
let selectionBuildParts;
let buildPartsForDoc;
let customBuildPartsForDoc;
let closeSelection = false;