/////////////////////////////////////////////////////////////////BUILD PHASES
function onEventOpenBuildPhasesCollapse(event) {
  openBuildPhasesCollaps(event.data.id);
}
function openBuildPhasesCollaps(id){
  if (!allLocations){
    $.get('/buildparts/buildphases/locations', function(data){
      allLocations = data.locations;
      collapseBuildPhaseLogic(id);
    })
  }
  else{
    collapseBuildPhaseLogic(id);
  }
  //call event functions
  $('#btnDeleteBuildPhaseConf').on('click', deleteBuildPhaseConfirm);
}
function collapseBuildPhaseLogic(id){
  if (buildPhasesCollapseOpen){
    if (id == activeBuildPartId){
      $('#collapseBuildPhases'+id).collapse("toggle");
      buildPhasesCollapseOpen = false;
    }
    else{
      $('#collapseBuildPhases'+activeBuildPartId).collapse("toggle");
      makeBuildPhasesCollapse(id);
    }
  }
  else{
    if (id == activeBuildPartId){
      if (collapseBuildPhaseContent){
        $('#collapseBuildPhases'+activeBuildPartId).collapse("toggle");
        buildPhasesCollapseOpen = true;
        setTimeout(()=>{
          var firstBuildPhaseId;
          if (allBuildPhases && allBuildPhases.length > 0)
            firstBuildPhaseId = allBuildPhases[0].id;
          anotherSmoothScroll('#buildPartsList'+activeTaskId,'#buildPart'+activeBuildPartId); 
          if (firstBuildPhaseId)
            anotherSmoothScroll('#buildPhaseList'+activeBuildPartId,'#buildPhase'+firstBuildPhaseId); 
        },500)
      }
      else{
        makeBuildPhasesCollapse(id);
      }
    }
    else{
      //$('#collapseBuildPhases'+activeBuildPartId).collapse("toggle");
      makeBuildPhasesCollapse(id);
    }
  }
}
function makeBuildPhasesCollapse(id){
  var buildPartId = id;
  activeBuildPartId = id;
  collapseNewBuildPhaseContent = false;
  collapseNewBuildPhaseOpen = false;
  debugger;
  $.get( "/buildparts/buildphases/get", {buildPartId}, function( data ) {
    //console.log(data)
    //debugger
    debugger;
    allBuildPhases = data.buildPhases;
    allInactiveBuildPhases = [];
    //debugger;
    //collapse frame
    var addBuildPhaseButton = '';
    if (loggedUser.role.substring(0,5) == 'vodja' || loggedUser.role == 'admin' || leader || loggedUser.role == 'strojnik'|| loggedUser.role == 'varilec' || loggedUser.role == 'cnc operater'){
      addBuildPhaseButton = '<button class="btn btn-success">Dodaj fazo</button>';
    }
    var frame1 = `<hr />
    <div class="list-group d-flex ml-3 mr-3 span-absence-list" id="buildPhaseList`+buildPartId+`"></div><br />
    <div class="collapse multi-collapse task-build-phase" id="collapseNewBuildPhase`+buildPartId+`"></div>
    <div id="addBuildPhaseRow`+buildPartId+`" class="text-center">
        `+addBuildPhaseButton+`
    </div><br />`;
    $('#collapseBuildPhases'+buildPartId).empty();
    $('#collapseBuildPhases'+buildPartId).append(frame1);
    $('#addBuildPhaseRow'+buildPartId).find('button').off('click').on('click', {id: buildPartId}, onEventAddNewBuildPhase);

    var pos = 1;
    for(var i = 0; i < allBuildPhases.length; i++){
      var inactiveClass = "";
      var deleteElement = '';
      var editElement = '';
      var approveElement = '';
      var unapproveElement = '';
      var saveElement = '';
      var deleteActivity = '';
      var deleteIcon = 'trash';
      //priority
      var priorityClass = '';
      if (allBuildPhases[i].priority == 'visoka')
        priorityClass = ' border-high-priority';
      else if (allBuildPhases[i].priority == 'srednja')
        priorityClass = ' border-medium-priority';
      else if (allBuildPhases[i].priority == 'nizka')
        priorityClass = ' border-low-priority';
      if (allBuildPhases[i].active == false){
        deleteActivity = '1';
        deleteIcon = 'trash-restore'
        inactiveClass = ' bg-dark';
      }
      if (loggedUser.role.substring(0,5) == 'vodja' || loggedUser.role == 'admin' || leader || loggedUser.role == 'strojnik'|| loggedUser.role == 'varilec' || loggedUser.role == 'cnc operater'){
        deleteElement = `<button class="btn btn-danger task-button ml-2 mt-1" id="deleteBuildPhase`+allBuildPhases[i].id+`">
        <i class="fas fa-`+deleteIcon+` fa-lg" data-toggle="tooltip" data-original-title="Odstrani"></i>
        </button>`;
        editElement = `<button class="btn btn-primary task-button ml-2 mt-1" id="editBuildPhase`+allBuildPhases[i].id+`">
          <i class="fas fa-pen fa-lg" data-toggle="tooltip" data-original-title="Uredi"></i>
        </button>`;
      }
      approveElement = `<button class="btn btn-success task-button ml-2 mt-1" id="approveBuildPhase`+allBuildPhases[i].id+`">
        <i class="fas fa-check fa-lg" data-toggle="tooltip" data-original-title="Potrdi"></i>
      </button>`;
      unapproveElement = `<button class="btn btn-warning task-button ml-2 mt-1" id="unapproveBuildPhase`+allBuildPhases[i].id+`">
        <i class="fas fa-times fa-lg" data-toggle="tooltip" data-original-title="Potrdi"></i>
      </button>`;
      saveElement = `<button class="btn btn-purchase task-button ml-2 mt-1" id="saveBuildPhase`+allBuildPhases[i].id+`">
        <i class="fas fa-save fa-lg" data-toggle="tooltip" data-original-title="Shrani"></i>
      </button>`;
      //START DATUM && FINISH DATUM
      var s = new Date(allBuildPhases[i].start).toISOString();
      var sdate = s.split('T')[0];
      var stime = s.split('T')[1];
      startFormat = {date:sdate.split('-')[2]+"."+sdate.split('-')[1]+"."+sdate.split('-')[0], time:stime.substring(0,5)};
      var f = new Date(allBuildPhases[i].finish).toISOString();
      var fdate = f.split('T')[0];
      var ftime = f.split('T')[1];
      finishFormat = {date:fdate.split('-')[2]+"."+fdate.split('-')[1]+"."+fdate.split('-')[0], time:ftime.substring(0,5)};
      //WORKERS OR SUPPLIER (external manufactured)
      var workElement = `<div class="ml-2 w-20" id="buildPhaseSupplier`+allBuildPhases[i].id+`">
        <label class="p-1">`+allBuildPhases[i].supplier+`</label>
      </div>`;
      if (allBuildPhases[i].supplier == 'ROBOTEH'){
        workElement = `<div class="ml-2 w-20" id="buildPhaseSupplier`+allBuildPhases[i].id+`">
          <a class="workerspopover" data-toggle="popover" data-trigger="hover" data-content="`+allBuildPhases[i].workers+`" data-original-title="" title="">
            <i class="fas fa-user-friends fa-lg text-dark"></i>
          </a>
        </div>`;
      }
      //FINISHED - location label or input
      //var finishedLabel = '';
      var endElement = '';
      if (allBuildPhases[i].completed){
        var fed = new Date(allBuildPhases[i].finished).toISOString();
        var feddate = fed.split('T')[0];
        var fedtime = fed.split('T')[1];
        var finishedFormat = {date:feddate.split('-')[2]+"."+feddate.split('-')[1]+"."+feddate.split('-')[0], time:fedtime.substring(0,5)};
        //finishedLabel = `<label class="p-1 ml-auto w-15" id="buildPhaseFinished`+allBuildPhases[i].id+`">`+allBuildPhases[i].finished+`</label>`;
        endElement = `<div class='col-12 col-xl-6 col-lg-6 col-md-6 d-flex'>
          <div class="p-1 ml-auto d-flex w-70" id="buildPhaseEnd`+allBuildPhases[i].id+`">
            <label class="p-1 ml-auto w-50" id="buildPhaseFinished`+allBuildPhases[i].id+`">`+finishedFormat.date+`</label>
            <label class="p-1 ml-auto w-50" id="buildPhaseLocation`+allBuildPhases[i].id+`">`+allBuildPhases[i].location+`</label>
          </div>
          `+deleteElement+`
          `+editElement+`
          `+unapproveElement+`
        </div>`;
      }
      else{
        endElement = `<div class='col-12 col-xl-6 col-lg-6 col-md-6 d-flex'>
          <div class="p-1 ml-auto d-flex w-50" id="buildPhaseEnd`+allBuildPhases[i].id+`">
            <select class="custom-select mr-sm-2 form-control select2-location w-100" id="locationInput`+allBuildPhases[i].id+`" name="location"></select>
          </div>
          `+saveElement+`
          `+deleteElement+`
          `+editElement+`
          `+approveElement+`
        </div>`;
      }
      //all together
      var listElement = `<div class="list-group-item`+priorityClass+inactiveClass+`" id="buildPhase`+allBuildPhases[i].id+`">
        <div class="row">
          <div class='col-12 col-xl-6 col-lg-6 col-md-6 d-flex'>
            <label class="p-1 w-4" id="buildPhasePos`+allBuildPhases[i].id+`">`+pos+`.</label>
            <label class="p-1 w-20" id="buildPhaseType`+allBuildPhases[i].id+`">`+allBuildPhases[i].build_phase_type+`</label>
            `+workElement+`
            <label class="p-1 w-40" id="buildPhaseDate`+allBuildPhases[i].id+`">`+startFormat.date+' - '+finishFormat.date+`</label>
          </div>
          `+endElement+`
        </div>
      </div>`;
      if (allBuildPhases[i].active == false){
        if (loggedUser.role == 'admin'){
          pos++;
          $('#buildPhaseList'+buildPartId).append(listElement);
          $('#unapproveBuildPhase'+allBuildPhases[i].id).on('click', {id: allBuildPhases[i].id}, onEventUnapproveBuildPhase);
          $('#approveBuildPhase'+allBuildPhases[i].id).on('click', {id: allBuildPhases[i].id}, onEventApproveBuildPhase);
          $('#editBuildPhase'+allBuildPhases[i].id).on('click', {buildPhaseId: allBuildPhases[i].id}, onEventOpenEditBuildPhase);
          $('#saveBuildPhase'+allBuildPhases[i].id).on('click', {id: allBuildPhases[i].id}, onEventSaveBuildPhaseLocation);
          $('#deleteBuildPhase'+allBuildPhases[i].id).on('click', {id: allBuildPhases[i].id, activity: deleteActivity}, onEventDeleteBuildPhase);
        }
        var id = allBuildPhases[i].id;
        allInactiveBuildPhases.push(allBuildPhases[i]);
        allBuildPhases = allBuildPhases.filter(s => s.id != id);
        i--;
      }
      else{
        pos++;
        $('#buildPhaseList'+buildPartId).append(listElement);
        $('#unapproveBuildPhase'+allBuildPhases[i].id).on('click', {id: allBuildPhases[i].id}, onEventUnapproveBuildPhase);
        $('#approveBuildPhase'+allBuildPhases[i].id).on('click', {id: allBuildPhases[i].id}, onEventApproveBuildPhase);
        $('#editBuildPhase'+allBuildPhases[i].id).on('click', {buildPhaseId: allBuildPhases[i].id}, onEventOpenEditBuildPhase);
        $('#saveBuildPhase'+allBuildPhases[i].id).on('click', {id: allBuildPhases[i].id}, onEventSaveBuildPhaseLocation);
        $('#deleteBuildPhase'+allBuildPhases[i].id).on('click', {id: allBuildPhases[i].id, activity: deleteActivity}, onEventDeleteBuildPhase);
      }
      // $('#unapproveBuildPhase'+allBuildPhases[i].id).off('click').on('click', {id: allBuildPhases[i].id}, onEventUnapproveBuildPhase);
      // $('#approveBuildPhase'+allBuildPhases[i].id).off('click').on('click', {id: allBuildPhases[i].id}, onEventApproveBuildPhase);
      // $('#editBuildPhase'+allBuildPhases[i].id).off('click').on('click', {buildPhaseId: allBuildPhases[i].id}, onEventOpenEditBuildPhase);
      // $('#saveBuildPhase'+allBuildPhases[i].id).off('click').on('click', {id: allBuildPhases[i].id}, onEventSaveBuildPhaseLocation);
      // $('#deleteBuildPhase'+allBuildPhases[i].id).off('click').on('click', {id: allBuildPhases[i].id, activity: deleteActivity}, onEventDeleteBuildPhase);
    }
    $(".select2-location").select2({
      data: allLocations,
      tags: true,
    });
    for(var i = 0; i < allBuildPhases.length; i++){
      if (allBuildPhases[i].completed == false)
        $('#locationInput'+allBuildPhases[i].id).val(allBuildPhases[i].location_id).trigger('change');
    }
    //$('[data-toggle="popover"]').popover({trigger: "hover"});
    $('#collapseBuildPhases'+buildPartId).collapse("toggle");
    $('[data-toggle="popover"]').popover();
    $('[data-toggle="tooltip"]').tooltip();
    buildPhasesCollapseOpen = true;
    collapseBuildPhaseContent = true;
    setTimeout(()=>{
      var firstBuildPhaseId;
      if (allBuildPhases && allBuildPhases.length > 0)
        firstBuildPhaseId = allBuildPhases[0].id;
      anotherSmoothScroll('#buildPartsList'+activeTaskId,'#buildPart'+activeBuildPartId); 
      if (firstBuildPhaseId)
        anotherSmoothScroll('#buildPhaseList'+activeBuildPartId,'#buildPhase'+firstBuildPhaseId); 
    },500)
  });
}
function onEventAddNewBuildPhase(event) {
  addNewBuildPhase(event.data.id);
}
//add new phase or open collapse for adding new phase
function addNewBuildPhase(id){

  debugger;
  if (!collapseNewBuildPhaseContent){
    debugger;
    var element = `<div class="row d-flex justify-content-end">
      <button class="close mr-4" type="button" aria-label="Close"><span aria-label="true">x</span></button>
    </div>
    <div class="form-row">
      <div class="col-md-6">
        <label class="w-100" for="buildPhaseTypeAdd">Tip
          <select class="custom-select mr-sm-2 form-control select2-phase-type w-100" id="buildPhaseTypeAdd`+id+`" name="phaseType"></select>
          <div class="invalid-feedback">Vnesite tip!</div>
        </label>
      </div>
      <div class="col-md-6">
        <label class="w-100" for="buildPhasePriorityAdd">Prioriteta
          <select class="custom-select mr-sm-2 form-control select2-priority w-100" id="buildPhasePriorityAdd`+id+`" name="phasepriority"></select>
        </label>
      </div>
    </div>
    <div class="form-row">
      <div class="col-md-6">
        <label class="w-100" for="buildPhaseStartAdd">Začetek
          <input class="form-control" id="buildPhaseStartAdd`+id+`" type="date" name="buildPhaseStartAdd" required="">
          <div class="invalid-feedback">Vnesite začetek!</div>
        </label>
      </div>
      <div class="col-md-6">
        <label class="w-100" for="buildPhaseFinishAdd">Konec
          <input class="form-control" id="buildPhaseFinishAdd`+id+`" type="date" name="buildPhaseFinishAdd" required="">
          <div class="invalid-feedback">Vnesite konec!</div>
        </label>
      </div>
    </div>
    <div class="form-row">
      <div class="col-md-6">
        <label class="w-100" for="buildPhaseSupplierAdd">Izvajalec
          <select class="custom-select mr-sm-2 form-control select2-phase-supplier w-100" id="buildPhaseSupplierAdd`+id+`" name="phasesupplier"></select>
          <div class="invalid-feedback">Vnesite izvajalca!</div>
        </label>
      </div>
      <div class="col-md-6">
        <label class="w-100" for="buildPhaseWorkersAdd">Zadolženi za fazo
          <select class="custom-select mr-sm-2 form-control select2-phase-worker w-100" id="buildPhaseWorkersAdd`+id+`" name="phaseworker"></select>
        </label>
      </div>
    </div>`;
    $('#collapseNewBuildPhase'+id).append(element);
    $('#collapseNewBuildPhase'+id).find('.close').on('click', {id}, onEventCloseCollapseNewBuildPhase);
    //fix dates to today inputs
    $('#buildPhaseStartAdd'+id).val(formatDate(new Date()));
    $('#buildPhaseFinishAdd'+id).val(formatDate(new Date()));
    //$('#buildPhaseWorkersAdd').prop('disabled', true);
    // $('#buildPhaseStartAdd'+id).change(function() {
    //   var s = new Date($('#buildPhaseStartAdd'+id).val());
    //   var f = new Date($('#buildPhaseFinishAdd'+id).val());
    //   if (s > f){
    //     $('#buildPhaseFinishAdd'+id).val($('#buildPhaseStartAdd'+id).val())
    //   }
    // });
    // $('#buildPhaseFinishAdd'+id).change(function() {
    //   var s = new Date($('#buildPhaseStartAdd'+id).val());
    //   var f = new Date($('#buildPhaseFinishAdd'+id).val());
    //   if (s > f){
    //     $('#buildPhaseStartAdd'+id).val($('#buildPhaseFinishAdd'+id).val())
    //   }
    // });
    $('#buildPhaseSupplierAdd'+id).change(function() {
      if ($("#buildPhaseSupplierAdd"+id+" option:selected" ).text() == 'ROBOTEH'){
        $('#buildPhaseWorkersAdd'+id).prop('disabled', false);
      }
      else{
        $('#buildPhaseWorkersAdd'+id).prop('disabled', true);
      }
    });
    // add permission to strojniki, varilci, cnc operaterji to add new supplier or new type
    let currentPermission = (loggedUser.role == 'strojnik'|| loggedUser.role == 'varilec' || loggedUser.role == 'cnc operater') ? true : permissionTag;
    //users(strojniki,serviserji,delavci,študenti)
    if (!allPhaseSuppliers || !allPhaseTypes || !allPhaseWorkers){
      $.get('/buildparts/buildphases/phasedata', function(data){
        allPhaseSuppliers = data.suppliers;
        allPhaseTypes = data.types;
        allPhaseWorkers = data.workers;
        allPriorities = data.priorities;
        $(".select2-phase-type").select2({
          data: allPhaseTypes,
          tags: currentPermission,
        });
        $(".select2-phase-supplier").select2({
          data: allPhaseSuppliers,
          tags: currentPermission,
        });
        $(".select2-phase-worker").select2({
          data: allPhaseWorkers,
          multiple: true,
        });
        $(".select2-priority").select2({
          data: allPriorities,
        });
      })
    }
    else{
      $(".select2-phase-type").select2({
        data: allPhaseTypes,
        tags: currentPermission,
      });
      $(".select2-phase-supplier").select2({
        data: allPhaseSuppliers,
        tags: currentPermission,
      });
      $(".select2-phase-worker").select2({
        data: allPhaseWorkers,
        multiple: true,
      });
      $(".select2-priority").select2({
        data: allPriorities,
      });
    }
    collapseNewBuildPhaseContent = true;
    $('#addBuildPhaseRow'+id).find('button').html('Potrdi');
  }
  if (!collapseNewBuildPhaseOpen){
    debugger;
    $('#collapseNewBuildPhase'+id).collapse("toggle");
    collapseNewBuildPhaseOpen = true;
    //change button text to approve instead of adding
    $('#addBuildPhaseRow'+id).find('button').html('Potrdi'); 
  }
  else{
    //collapse is open and user wants do add new phase
    debugger;
    if (($("#buildPhaseSupplierAdd"+id+" option:selected").text() == 'ROBOTEH' && $('#buildPhaseWorkersAdd'+id).val().length > 0) || ($("#buildPhaseSupplierAdd"+id+" option:selected").text() != 'ROBOTEH')){
      $('#buildPhaseWorkersAdd'+id).removeClass('is-invalid');
      //var worker = $('#absenceWorker').val();
      //var reason = $('#absenceReason').val();
      var buildPartId = id;
      var type = $('#buildPhaseTypeAdd'+id).val();
      var start = $('#buildPhaseStartAdd'+id).val() + " 07:00:00.000000";
      var finish = $('#buildPhaseFinishAdd'+id).val() + " 15:00:00.000000";
      var supplier = $('#buildPhaseSupplierAdd'+id).val();
      var workers = $('#buildPhaseWorkersAdd'+id).val().toString();
      var priority = $('#buildPhasePriorityAdd'+id).val();
      //no empty inputs, add new build phase
      debugger;
      $.post('/buildparts/buildphases/add', {buildPartId,type,start,finish,supplier,workers,priority}, function(data){
        debugger;
        if (data.success){
          console.log('Successfully adding new build phase.');
          var tmpPhaseType, tmpSupplier;
          if (data.typeId){
            tmpPhaseType = {id:data.typeId, text:type};
            allPhaseTypes.push(tmpPhaseType);
            $(".select2-phase-type").select2({
              data: allPhaseTypes,
              tags: permissionTag,
            });
          }
          else{
            tmpPhaseType = allPhaseTypes.find(pt => pt.id == type);
          }
          if (data.supplierId){
            tmpSupplier = {id:data.supplierId, text:supplier};
            allPhaseSuppliers.push(tmpSupplier);
            $(".select2-phase-supplier").select2({
              data: allPhaseSuppliers,
              tags: permissionTag,
            });
          }
          else{
            tmpSupplier = allPhaseSuppliers.find(ps => ps.id == supplier);
          }
          var tmpWorkersId = null;
          var tmpWorkersNames = null;
          if ($("#buildPhaseSupplierAdd"+id+" option:selected").text() == 'ROBOTEH'){
            var d = $('#buildPhaseWorkersAdd'+id).select2('data');
            tmpWorkersId = $('#buildPhaseWorkersAdd'+id).val().toString();
            for(var i = 0; i < d.length; i++){
              if (i == 0)
                tmpWorkersNames = d[i].text;
              else
                tmpWorkersNames += ', '+d[i].text;
            }
          }
          //get position for new build phase
          //var newPos = $('#buildPhasePos'+allBuildPhases[allBuildPhases.length-1].id).html();
          //newPos = parseInt(t.substring(0,t.length-1))+1;
          //make new build phase
          var tmpBuildPhase = {
            id: data.buildPhase.id,
            active: true,
            build_phase_type: tmpPhaseType.text,
            build_phase_type_id: tmpPhaseType.id,
            build_supplier_id: tmpSupplier.id,
            completed: false,
            created: new Date().toISOString(),
            finish: new Date(finish).toISOString(),
            finished: null,
            location: null,
            location_id: null,
            priority: $('#buildPhasePriorityAdd'+id+' option:selected').text(),
            priority_id: $('#buildPhasePriorityAdd'+id).val(),
            start: new Date(start).toISOString(),
            supplier: tmpSupplier.text,
            workers: tmpWorkersNames,
            workers_id: tmpWorkersId
          }
          allBuildPhases.push(tmpBuildPhase);
          //create new element to add on list
          //priority
          var priorityClass = '';
          var deleteActivity = '';
          var deleteIcon = 'trash';
          if (tmpBuildPhase.priority == 'visoka')
            priorityClass = 'border-high-priority';
          else if (tmpBuildPhase.priority == 'srednja')
            priorityClass = 'border-medium-priority';
          else if (tmpBuildPhase.priority == 'nizka')
            priorityClass = 'border-low-priority';
          //START DATUM && FINISH DATUM
          var s = new Date(tmpBuildPhase.start).toISOString();
          var sdate = s.split('T')[0];
          var stime = s.split('T')[1];
          startFormat = {date:sdate.split('-')[2]+"."+sdate.split('-')[1]+"."+sdate.split('-')[0], time:stime.substring(0,5)};
          var f = new Date(tmpBuildPhase.finish).toISOString();
          var fdate = f.split('T')[0];
          var ftime = f.split('T')[1];
          finishFormat = {date:fdate.split('-')[2]+"."+fdate.split('-')[1]+"."+fdate.split('-')[0], time:ftime.substring(0,5)};
          //WORKERS OR SUPPLIER (external manufactured)
          var workElement = `<div class="ml-2 w-20" id="buildPhaseSupplier`+tmpBuildPhase.id+`">
            <label class="p-1">`+tmpBuildPhase.supplier+`</label>
          </div>`;
          if (tmpBuildPhase.supplier == 'ROBOTEH'){
            workElement = `<div class="ml-2 w-20" id="buildPhaseSupplier`+tmpBuildPhase.id+`">
              <a class="workerspopover" data-toggle="popover" data-trigger="hover" data-content="`+tmpBuildPhase.workers+`" data-original-title="" title="">
                <i class="fas fa-user-friends fa-lg text-dark"></i>
              </a>
            </div>`;
          }
          var deleteElement = '';
          var editElement = '';
          var approveElement = '';
          var saveElement = '';
          if (loggedUser.role.substring(0,5) == 'vodja' || loggedUser.role == 'admin' || leader || loggedUser.role == 'strojnik'|| loggedUser.role == 'varilec' || loggedUser.role == 'cnc operater'){
            deleteElement = `<button class="btn btn-danger task-button ml-2 mt-1" id="deleteBuildPhase`+tmpBuildPhase.id+`">
              <i class="fas fa-`+deleteIcon+` fa-lg" data-toggle="tooltip" data-original-title="Odstrani"></i>
            </button>`;
            editElement = `<button class="btn btn-primary task-button ml-2 mt-1" id="editBuildPhase`+tmpBuildPhase.id+`">
              <i class="fas fa-pen fa-lg" data-toggle="tooltip" data-original-title="Uredi"></i>
            </button>`;
          }
          approveElement = `<button class="btn btn-success task-button ml-2 mt-1" id="approveBuildPhase`+tmpBuildPhase.id+`">
            <i class="fas fa-check fa-lg" data-toggle="tooltip" data-original-title="Potrdi"></i>
          </button>`;
          saveElement = `<button class="btn btn-purchase task-button ml-2 mt-1" id="saveBuildPhase`+tmpBuildPhase.id+`">
            <i class="fas fa-save fa-lg" data-toggle="tooltip" data-original-title="Shrani"></i>
          </button>`;
          var endElement = `<div class='col-12 col-xl-6 col-lg-6 col-md-6 d-flex'>
            <div class="p-1 ml-auto d-flex w-50" id="buildPhaseEnd`+tmpBuildPhase.id+`">
              <select class="custom-select mr-sm-2 form-control select2-location w-100" id="locationInput`+tmpBuildPhase.id+`" name="location"></select>
            </div>
            `+saveElement+`
            `+deleteElement+`
            `+editElement+`
            `+approveElement+`
          </div>`;
          var listElement = `<div class="list-group-item `+priorityClass+`" id="buildPhase`+tmpBuildPhase.id+`">
            <div class="row">
              <div class='col-12 col-xl-6 col-lg-6 col-md-6 d-flex'>
                <label class="p-1 w-4" id="buildPhasePos`+tmpBuildPhase.id+`">`+allBuildPhases.length+`.</label>
                <label class="p-1 w-20" id="buildPhaseType`+tmpBuildPhase.id+`">`+tmpBuildPhase.build_phase_type+`</label>
                `+workElement+`
                <label class="p-1 w-40" id="buildPhaseDate`+tmpBuildPhase.id+`">`+startFormat.date+' - '+finishFormat.date+`</label>
              </div>
              `+endElement+`
            </div>
          </div>`;
          $('#buildPhaseList'+buildPartId).append(listElement);
          $('#approveBuildPhase'+tmpBuildPhase.id).off('click').on('click', {id: tmpBuildPhase.id}, onEventApproveBuildPhase);
          $('#editBuildPhase'+tmpBuildPhase.id).off('click').on('click', {buildPhaseId: tmpBuildPhase.id}, onEventOpenEditBuildPhase);
          $('#saveBuildPhase'+tmpBuildPhase.id).off('click').on('click', {id: tmpBuildPhase.id}, onEventSaveBuildPhaseLocation);
          $('#deleteBuildPhase'+tmpBuildPhase.id).off('click').on('click', {id: tmpBuildPhase.id, activity: deleteActivity}, onEventDeleteBuildPhase);
          $(".select2-location").select2({
            data: allLocations,
            tags: permissionTag,
          });
          for(var i = 0; i < allBuildPhases.length; i++){
            if (allBuildPhases[i].completed == false)
              $('#locationInput'+allBuildPhases[i].id).val(allBuildPhases[i].location_id).trigger('change');
          }
          $('[data-toggle="popover"]').popover();
          $('[data-toggle="tooltip"]').tooltip();
          $('#buildPhaseList'+buildPartId).animate({ scrollTop: $('#buildPhaseList'+buildPartId).prop("scrollHeight")}, 1000);
          $('#collapseNewBuildPhase'+buildPartId).collapse('toggle');
          $('#addBuildPhaseRow'+id).find('button').html('Dodaj fazo');
          collapseNewBuildPhaseOpen = false;
          //change button to black cuz now build part has phases
          if ($('#phasesBuildPart'+buildPartId).find('i').css('color') == 'rgb(255, 255, 255)'){
            //change button to black
            //$('#phasesBuildPart'+buildPartId).find('img').attr('src', '/pointerDownBlack.png');
            $('#phasesBuildPart'+buildPartId).find('i').addClass('text-dark');
            //remove checkbox on build part
            $('#buildPartCheckbox'+buildPartId).empty()
          }
          //fix build part completion
          var numberOfCompleted = 0;
          for(var i=0; i<allBuildPhases.length; i++){
            if (allBuildPhases[i].completed)
            numberOfCompleted++;
          }
          //calc new completion of build part
          var completion = Math.round((numberOfCompleted/allBuildPhases.length)*100)
          if (numberOfCompleted == 0 || allBuildPhases.length == 0)
            completion = 0;
          fixBuildPartCompletion(buildPartId,completion);
          //animation to the new element in build phases list
          setTimeout(()=>{
            anotherSmoothScroll('#buildPhaseList'+activeBuildPartId,'#buildPhase'+tmpBuildPhase.id); 
            flashAnimation('#buildPhase'+tmpBuildPhase.id);
          },500)
          //ADDING CHANGE TO DB
          addChangeSystem(1,21,projectId,activeTaskId,null,null,null,null,null,null,null,null,activeBuildPartId,tmpBuildPhase.id);
        }
        else{
          console.log('Unsuccessfully adding new build phase!');
          //console.log(data);
          $('#modalError').modal('toggle');
        }
      })
    }
    else{
      $('#buildPhaseWorkersAdd'+id).addClass('is-invalid');
    }     
  }
}
function onEventCloseCollapseNewBuildPhase(event) {
  closeCollapseNewBuildPhase(event.data.id);
}
function closeCollapseNewBuildPhase(id){
  $('#collapseNewBuildPhase'+id).collapse("toggle");
  collapseNewBuildPhaseOpen = false;
  $('#addBuildPhaseRow'+id).find('button').html('Dodaj fazo');
}
function onEventOpenEditBuildPhase(event) {
  openEditBuildPhase(event.data.buildPhaseId);
}
//OPEN EDIT BUILD PHASE MODAL
function openEditBuildPhase(buildPhaseId){
  debugger;
  
  if (!allPhaseSuppliers || !allPhaseTypes || !allPhaseWorkers){
    $.get('/buildparts/buildphases/phasedata', function(data){
      allPhaseSuppliers = data.suppliers;
      allPhaseTypes = data.types;
      allPhaseWorkers = data.workers;
      allPriorities = data.priorities;
      $(".select2-phase-type").select2({
        data: allPhaseTypes,
        tags: permissionTag,
      });
      $(".select2-phase-supplier").select2({
        data: allPhaseSuppliers,
        tags: permissionTag,
      });
      $(".select2-phase-worker").select2({
        data: allPhaseWorkers,
        multiple: true,
      });
      $(".select2-priority").select2({
        data: allPriorities,
      });
      fillEditBuildPhaseModal(buildPhaseId);
    })
  }
  else{
    fillEditBuildPhaseModal(buildPhaseId);
  }
}
//FILL EDIT MODAL DATA FOR BUILD PHASE
function fillEditBuildPhaseModal(buildPhaseId){
  var tmpBuildPhase = allBuildPhases.find(bp => bp.id == buildPhaseId);
  if (!tmpBuildPhase)
    tmpBuildPhase = allInactiveBuildPhases.find(ibp => ibp.id == buildPhaseId);
  $('#buildPhaseStartEdit').val(formatDate(tmpBuildPhase.start));
  $('#buildPhaseFinishEdit').val(formatDate(tmpBuildPhase.finish));
  $('#buildPhaseTypeEdit').val(tmpBuildPhase.build_phase_type_id).trigger('change');
  $('#buildPhasePriorityEdit').val(tmpBuildPhase.priority_id).trigger('change');
  $('#buildPhaseSupplierEdit').val(tmpBuildPhase.build_supplier_id).trigger('change');
  if (tmpBuildPhase.workers_id)
    $('#buildPhaseWorkersEdit').val(tmpBuildPhase.workers_id.split(',')).trigger('change');
  else
    $('#buildPhaseWorkersEdit').val([]).trigger('change');
  if (tmpBuildPhase.supplier == 'ROBOTEH')
    $('#buildPhaseWorkersEdit').prop('disabled', false);
  else
    $('#buildPhaseWorkersEdit').prop('disabled', true);
  if (firstBuildPhaseEdit){
    firstBuildPhaseEdit = false;
    // $('#buildPhaseStartEdit').change(onChangePhaseStartEdit);
    // $('#buildPhaseFinishEdit').change(onChangePhaseFinishEdit);
    $('#buildPhaseSupplierEdit').change(onChangePhaseSupplierEdit);
  }
  $('#buildPhaseAdditionalEdit').empty();
  if (tmpBuildPhase.completed){
    var additionalRow = `<div class="col-md-6">
      <label class="w-75" for="buildPhaseFinishedEdit">Končano
        <input class="form-control" id="buildPhaseFinishedEdit" type="date" name="buildPhaseFinishedEdit" required="">
        <div class="invalid-feedback">Vnesite končni datum!</div>
      </label>
      <button class="btn btn-success task-button ml-1" id="randomBuildPhaseFinishedEdit">
        <i class="fas fa-dice-three fa-lg" data-toggle="tooltip" data-original-title="Faze"></i>
      </button>
    </div>
    <div class="col-md-6">
      <label class="w-100" for="buildPhaseLocationEdit">Lokacija
        <select class="custom-select mr-sm-2 form-control select2-location w-100" id="buildPhaseLocationEdit" name="location"></select>
        <div class="invalid-feedback">Vnesite lokacijo!</div>
      </label>
    </div>`;
    $('#buildPhaseAdditionalEdit').append(additionalRow);
    $(".select2-location").select2({
      data: allLocations,
      tags: true,
    });
    for(var i = 0; i < allBuildPhases.length; i++){
      if (allBuildPhases[i].completed == false)
        $('#locationInput'+allBuildPhases[i].id).val(allBuildPhases[i].location_id).trigger('change');
    }
    $('#buildPhaseFinishedEdit').val(formatDate(tmpBuildPhase.finished));
    $('#buildPhaseLocationEdit').val(tmpBuildPhase.location_id).trigger('change');
  }
  var button = `<button class="btn btn-success" id="btnEditBuildPhase">Posodobi</button>`;
  $('#editBuildPhaseButtonBody').empty();
  $('#editBuildPhaseButtonBody').append(button);
  $('#btnEditBuildPhase').off('click').on('click', {buildPhaseId}, onEventEditBuildPhase);
  $('#randomBuildPhaseFinishedEdit').on('click', function () {
    var date = new Date($('#buildPhaseFinishEdit').val());
    date.setDate(date.getDate() + (Math.floor(Math.random() * 7) - 3));
    var startDate = new Date($('#buildPhaseStartEdit').val());
    if(startDate > date){
      // random date is before start so set start otherwise it is impossible date for finished
      $('#buildPhaseFinishedEdit').val($('#buildPhaseStartEdit').val());
    }
    else{
      // set random date as finished date
      $('#buildPhaseFinishedEdit').val(moment(date).format("YYYY-MM-DD"));
    }
  })
  $("#modalEditBuildPhase").modal('toggle');
}
function onEventEditBuildPhase(event) {
  editBuildPhase(event.data.buildPhaseId);
}
//EDIT BUILD PHASE
function editBuildPhase(buildPhaseId){
  debugger;
  if (($("#buildPhaseSupplierEdit option:selected").text() == 'ROBOTEH' && $('#buildPhaseWorkersEdit').val().length > 0) || ($("#buildPhaseSupplierEdit option:selected").text() != 'ROBOTEH')){
    var type = $('#buildPhaseTypeEdit').val();
    var start = $('#buildPhaseStartEdit').val() + " 07:00:00.000000";
    var finish = $('#buildPhaseFinishEdit').val() + " 15:00:00.000000";
    var supplier = $('#buildPhaseSupplierEdit').val();
    var workers = $('#buildPhaseWorkersEdit').val().toString();
    var priority = $('#buildPhasePriorityEdit').val();
    var finished = null;
    var location = null;
    if ($('#buildPhaseFinishedEdit').val()){
      finished = $('#buildPhaseFinishedEdit').val() +' '+ new Date().toISOString().split('T')[1].split('Z')[0];
      location = $('#buildPhaseLocationEdit').val();
    }

    //post to update build phase
    $.post('/buildparts/buildphases/update', {buildPhaseId, type, start, finish, supplier, workers, priority, finished, location}, function(data){
      debugger;
      if (data.success){
        console.log('Successfully updating build phase.');
        var tmpPhaseType, tmpSupplier, tmpLocation;
        if (data.typeId){
          tmpPhaseType = {id:data.typeId, text:type};
          allPhaseTypes.push(tmpPhaseType);
          $(".select2-phase-type").select2({
            data: allPhaseTypes,
            tags: permissionTag,
          });
        }
        else{
          tmpPhaseType = allPhaseTypes.find(pt => pt.id == type);
        }
        if (data.supplierId){
          tmpSupplier = {id:data.supplierId, text:supplier};
          allPhaseSuppliers.push(tmpSupplier);
          $(".select2-phase-supplier").select2({
            data: allPhaseSuppliers,
            tags: permissionTag,
          });
        }
        else{
          tmpSupplier = allPhaseSuppliers.find(ps => ps.id == supplier);
        }
        if (data.locationId){
          tmpLocation = {id:data.locationId, text:location};
          allLocations.push(tmpLocation);
          $(".select2-location").select2({
            data: allLocations,
            tags: permissionTag,
          });
        }
        else{
          if (location)
            tmpLocation = allLocations.find(pt => pt.id == location);
        }
        var tmpPriority = {id:$('#buildPhasePriorityEdit').val(), text:$('#buildPhasePriorityEdit option:selected').text()};
        var tmpWorkersId = null;
        var tmpWorkersNames = null;
        if ($("#buildPhaseSupplierEdit option:selected").text() == 'ROBOTEH'){
          var d = $('#buildPhaseWorkersEdit').select2('data');
          tmpWorkersId = $('#buildPhaseWorkersEdit').val().toString();
          for(var i = 0; i < d.length; i++){
            if (i == 0)
              tmpWorkersNames = d[i].text;
            else
              tmpWorkersNames += ', '+d[i].text;
          }
        }
        //START DATUM && FINISH DATUM
        var s = new Date(data.buildPhase.start).toISOString();
        var sdate = s.split('T')[0];
        var stime = s.split('T')[1];
        startFormat = {date:sdate.split('-')[2]+"."+sdate.split('-')[1]+"."+sdate.split('-')[0], time:stime.substring(0,5)};
        var f = new Date(data.buildPhase.finish).toISOString();
        var fdate = f.split('T')[0];
        var ftime = f.split('T')[1];
        finishFormat = {date:fdate.split('-')[2]+"."+fdate.split('-')[1]+"."+fdate.split('-')[0], time:ftime.substring(0,5)};
        var tmpBuildPhase = allBuildPhases.find(bp => bp.id == buildPhaseId);
        if (!tmpBuildPhase)
          tmpBuildPhase = allInactiveBuildPhases.find(ibp => ibp.id == buildPhaseId);
        $('#buildPhaseType'+buildPhaseId).html(tmpPhaseType.text);
        $('#buildPhaseDate'+buildPhaseId).html(startFormat.date+' - '+finishFormat.date);
        if (tmpSupplier.text == 'ROBOTEH'){
          var workElement = `<a class="workerspopover" data-toggle="popover" data-trigger="hover" data-content="`+tmpWorkersNames+`" data-original-title="" title="">
            <i class="fas fa-user-friends fa-lg text-dark"></i>
          </a>`;
          $('#buildPhaseSupplier'+buildPhaseId).empty();
          $('#buildPhaseSupplier'+buildPhaseId).append(workElement);
        }
        else{
          var workElement = `<label class="p-1">`+tmpSupplier.text+`</label>`;
          $('#buildPhaseSupplier'+buildPhaseId).empty();
          $('#buildPhaseSupplier'+buildPhaseId).append(workElement);
        }
        $('#buildPhase'+tmpBuildPhase.id).removeClass('border-low-priority');
        $('#buildPhase'+tmpBuildPhase.id).removeClass('border-medium-priority');
        $('#buildPhase'+tmpBuildPhase.id).removeClass('border-high-priority');
        tmpBuildPhase.priority_id = tmpPriority.id;
        tmpBuildPhase.priority = tmpPriority.text;
        if (tmpBuildPhase.priority == 'visoka')
          $('#buildPhase'+tmpBuildPhase.id).addClass('border-high-priority');
        else if (tmpBuildPhase.priority == 'srednja')
          $('#buildPhase'+tmpBuildPhase.id).addClass('border-medium-priority');
        else if (tmpBuildPhase.priority == 'nizka')
          $('#buildPhase'+tmpBuildPhase.id).addClass('border-low-priority');
        if (tmpLocation){
          tmpBuildPhase.location_id = tmpLocation.id;
          tmpBuildPhase.location = tmpLocation.text;
          $('#buildPhaseLocation'+buildPhaseId).html(tmpLocation.text);
          var fed = new Date(data.buildPhase.finished).toISOString();
          var feddate = fed.split('T')[0];
          var fedtime = fed.split('T')[1];
          finishedFormat = {date:feddate.split('-')[2]+"."+feddate.split('-')[1]+"."+feddate.split('-')[0], time:fedtime.substring(0,5)};
          tmpBuildPhase.finished = fed;
          $('#buildPhaseFinished'+buildPhaseId).html(finishedFormat.date);
        }
        tmpBuildPhase.build_phase_type_id = tmpPhaseType.id;
        tmpBuildPhase.build_phase_type = tmpPhaseType.text;
        tmpBuildPhase.build_supplier_id = tmpSupplier.id;
        tmpBuildPhase.supplier = tmpSupplier.text;
        tmpBuildPhase.start = s;
        tmpBuildPhase.finish = f;
        tmpBuildPhase.workers_id = tmpWorkersId;
        tmpBuildPhase.workers = tmpWorkersNames;
        $('[data-toggle="popover"]').popover();
        $("#modalEditBuildPhase").modal('toggle');
        //ADDING CHANGE TO DB
        addChangeSystem(2,21,projectId,activeTaskId,null,null,null,null,null,null,null,null,activeBuildPartId,tmpBuildPhase.id);
      }
      else{
        console.log('Unsuccessfully updating build phase.');
        //alert('Napaka pri posodabljanju.');
        $('#modalError').modal('toggle');
      }
    })
  }
  else{
    $('#buildPhaseWorkersEdit').addClass('is-invalid');
  }
}
function onEventApproveBuildPhase(event) {
  approveBuildPhase(event.data.id);
}
//APPROVE BUILD PHASE
function approveBuildPhase(id){
  var buildPartId = activeBuildPartId;
  var location = $('#locationInput'+id).val();
  if(!location){
    alert('Izberi lokacijo kosa');
  }
  else{
    $.post('/buildparts/buildphases/finish', {id, location}, function(resp){
      if (resp.success){
        console.log('Successfully updating location!');
        var tmpLocation;
        if (resp.locationId){
          allLocations.push({id:resp.locationId, text:location});
          $(".select2-location").select2({
            data: allLocations,
          });
          tmpLocation = {id:resp.locationId, text:location};
        }
        else
          tmpLocation = allLocations.find(l => l.id == location);
        var d = new Date().toISOString();
        var tmpBuildPhase = allBuildPhases.find(bp => bp.id == id);
        if (tmpBuildPhase){
          //tmpBuildPhase.completed = true;
          //tmpBuildPhase.finished = d;
          //tmpBuildPhase.location = tmpLocation.text;
          //tmpBuildPhase.location_id = tmpLocation.id;
          //build phase is active and need to update build part completion
          var numberOfCompleted = 0;
          for(var i=0; i<allBuildPhases.length; i++){
            if (allBuildPhases[i].completed)
            numberOfCompleted++;
          }
          numberOfCompleted++;
          //calc new completion of build part
          var completion = Math.round((numberOfCompleted/allBuildPhases.length)*100)
          if (numberOfCompleted == 0 || allBuildPhases.length == 0)
            completion = 0;
          fixBuildPartCompletion(buildPartId,completion);
        }
        else{
          //build phase is inactive and build part completion stays same
          tmpBuildPhase = allInactiveBuildPhases.find(ibp => ibp.id == id);
        }
        tmpBuildPhase.completed = true;
        tmpBuildPhase.finished = d;
        tmpBuildPhase.location = tmpLocation.text;
        tmpBuildPhase.location_id = tmpLocation.id;
        var fed = new Date(tmpBuildPhase.finished).toISOString();
        var feddate = fed.split('T')[0];
        var fedtime = fed.split('T')[1];
        finishedFormat = {date:feddate.split('-')[2]+"."+feddate.split('-')[1]+"."+feddate.split('-')[0], time:fedtime.substring(0,5)};
        tmpBuildPhase.finished = fed;
        //$('#buildPhaseFinished'+buildPhaseId).html(finishedFormat.date);
        //add unapprove element
        var unapproveElement = `<button class="btn btn-warning task-button ml-2 mt-1" id="unapproveBuildPhase`+tmpBuildPhase.id+`">
          <i class="fas fa-times fa-lg" data-toggle="tooltip" data-original-title="Potrdi"></i>
        </button>`;
        //fix end element of build phase on page
        var fixEndElement = `<label class="p-1 ml-auto w-50" id="buildPhaseFinished`+tmpBuildPhase.id+`">`+finishedFormat.date+`</label>
        <label class="p-1 ml-auto w-50" id="buildPhaseLocation`+tmpBuildPhase.id+`">`+tmpBuildPhase.location+`</label>`;
        $('#buildPhaseEnd'+id).empty();
        $('#buildPhaseEnd'+id).append(fixEndElement);
        $('#buildPhaseEnd'+id).width('70%');
        $('#approveBuildPhase'+id).remove();
        $('#saveBuildPhase'+id).remove();
        $('#editBuildPhase'+id).after(unapproveElement);
        $('#unapproveBuildPhase'+tmpBuildPhase.id).off('click').on('click', {id: tmpBuildPhase.id}, onEventUnapproveBuildPhase);
        $('[data-toggle="tooltip"]').tooltip();
        //ADDING CHANGE TO DB
        addChangeSystem(4,21,projectId,activeTaskId,null,null,null,null,null,null,null,null,activeBuildPartId,tmpBuildPhase.id);
      }
      else{
        console.log('Unsuccessfully updating location!');
        $('#modalError').modal('toggle');
      }
    })
  }
}
function onEventUnapproveBuildPhase(event) {
  unapproveBuildPhase(event.data.id);
}
//APPROVE BUILD PHASE
function unapproveBuildPhase(id){
  var buildPartId = activeBuildPartId;
  $.post('/buildparts/buildphases/unfinish', {id}, function(resp){
    if (resp.success){
      console.log('Successfully undo approve!');
      var tmpBuildPhase = allBuildPhases.find(bp => bp.id == id);
      if (tmpBuildPhase){
        //tmpBuildPhase.completed = false;
        //tmpBuildPhase.location = null;
        //tmpBuildPhase.location_id = null;
        //build phase is active and need to update build part completion
        var numberOfCompleted = 0;
        for(var i=0; i<allBuildPhases.length; i++){
          if (allBuildPhases[i].completed)
          numberOfCompleted++;
        }
        numberOfCompleted--;
        //calc new completion of build part
        var completion = Math.round((numberOfCompleted/allBuildPhases.length)*100)
        if (numberOfCompleted == 0 || allBuildPhases.length == 0)
          completion = 0;
        fixBuildPartCompletion(buildPartId,completion);
      }
      else{
        //build phase is inactive and build part completion stays same
        tmpBuildPhase = allInactiveBuildPhases.find(ibp => ibp.id == id);
      }
      tmpBuildPhase.completed = false;
      tmpBuildPhase.location = null;
      tmpBuildPhase.location_id = null;
      //add approve & save element
      var approveElement = `<button class="btn btn-success task-button ml-2 mt-1" id="approveBuildPhase`+tmpBuildPhase.id+`">
        <i class="fas fa-check fa-lg" data-toggle="tooltip" data-original-title="Potrdi"></i>
      </button>`;
      var saveElement = `<button class="btn btn-purchase task-button ml-2 mt-1" id="saveBuildPhase`+tmpBuildPhase.id+`">
        <i class="fas fa-save fa-lg" data-toggle="tooltip" data-original-title="Shrani"></i>
      </button>`;
      //fix end element of build phase on page
      var fixEndElement = `<select class="custom-select mr-sm-2 form-control select2-location w-100" id="locationInput`+tmpBuildPhase.id+`" name="location"></select>`;
      $('#buildPhaseEnd'+id).empty();
      $('#buildPhaseEnd'+id).append(fixEndElement);
      $('#buildPhaseEnd'+id).width('50%');
      $('#unapproveBuildPhase'+id).remove();
      $('#deleteBuildPhase'+id).before(saveElement);
      $('#saveBuildPhase'+tmpBuildPhase.id).off('click').on('click', {id: tmpBuildPhase.id}, onEventSaveBuildPhaseLocation);
      $('#editBuildPhase'+id).after(approveElement);
      $('#approveBuildPhase'+tmpBuildPhase.id).off('click').on('click', {id: tmpBuildPhase.id}, onEventApproveBuildPhase);
      $('[data-toggle="tooltip"]').tooltip();
      $(".select2-location").select2({
        data: allLocations,
        tags: permissionTag,
      });
      for(var i = 0; i < allBuildPhases.length; i++){
        if (allBuildPhases[i].completed == false)
          $('#locationInput'+allBuildPhases[i].id).val(allBuildPhases[i].location_id).trigger('change');
      }
      //ADDING CHANGE TO DB
      addChangeSystem(5,21,projectId,activeTaskId,null,null,null,null,null,null,null,null,activeBuildPartId,tmpBuildPhase.id);
    }
    else{
      console.log('Unsuccessfully undo approve!');
      $('#modalError').modal('toggle');
    }
  })
}
function onEventSaveBuildPhaseLocation(event) {
  saveBuildPhaseLocation(event.data.id);
}
//SAVE BUILD PHASE LOCATION
function saveBuildPhaseLocation(id){
  var buildPartId = activeBuildPartId;
  var location = $('#locationInput'+id).val();
  $.post('/buildparts/buildphases/location', {id, location}, function(resp){
    if (resp.success){
      console.log('Successfully saving location!');
      var tmpLocation;
      if (resp.locationId){
        allLocations.push({id:resp.locationId, text:location});
        $(".select2-location").select2({
          data: allLocations,
        });
        tmpLocation = {id:resp.locationId, text:location};
      }
      else
        tmpLocation = allLocations.find(l => l.id == location);
      var tmpBuildPhase = allBuildPhases.find(bp => bp.id == id);
      tmpBuildPhase.location = tmpLocation.text;
      tmpBuildPhase.location_id = tmpLocation.id;
      $('#saveBuildPhase'+id).addClass('active');
      //ADDING CHANGE TO DB
      addChangeSystem(9,21,projectId,activeTaskId,null,null,null,null,null,null,null,null,activeBuildPartId,tmpBuildPhase.id);
    }
    else{
      console.log('Unsuccessfully updating location!');
      $('#modalError').modal('toggle');
    }
  })
}
function onEventDeleteBuildPhase(event) {
  deleteBuildPhase(event.data.id, event.data.activity);
}
//DELETE SELECTED Build phase
function deleteBuildPhase(id, activity){
  var active = false;
  if (activity && activity == 1)
    active = true;
  savedBuildPhaseId = id;
  savedBuildPhaseActive = active;
  savedBuildPhaseActivity = activity;
  if (active){
    $('#btnDeleteBuildPhaseConf').html('Ponovno dodaj');
    $('#deleteBuildPhaseModalMsg').html('Ste prepričani, da želite ponovno dodati izbrisano fazo?');
  }
  else{
    $('#btnDeleteBuildPhaseConf').html('Odstrani');
    $('#deleteBuildPhaseModalMsg').html('Ste prepričani, da želite odstraniti izbrano fazo?');
  }
  $('#modalDeleteBuildPhase').modal('toggle');
}
function deleteBuildPhaseConfirm(){
  //debugger;
  var taskId = activeTaskId;
  var buildPartId = activeBuildPartId;
  var id = savedBuildPhaseId;
  var active = savedBuildPhaseActive;
  var activity = savedBuildPhaseActivity;
  //not sure why i need taskId for deletin subtask
  $.post('/buildparts/buildphases/delete', {id, active}, function(resp){
    if (resp.success){
      console.log('Successfully deleting build phase.');
      $('#modalDeleteBuildPhase').modal('toggle');
      //debugger
      var count = allBuildPhases.length;
      if (loggedUser.role == 'admin'){
        if (activity && activity == 1){
          //resurection
          $('#buildPhase'+id).removeClass('bg-dark');
          //$('#deleteBuildPhase'+id).off('click');
          $('#deleteBuildPhase'+id).off('click').on('click', {id}, onEventDeleteBuildPhase);
          //$('#deleteBuildPhase'+id).find('img').attr('src', '/delete.png');
          $('#deleteBuildPhase'+id).find('i').removeClass('fa-trash-restore');
          $('#deleteBuildPhase'+id).find('i').addClass('fa-trash');
          var tmp = allInactiveBuildPhases.find(s => s.id == id);
          allBuildPhases.push(tmp);
          allInactiveBuildPhases = allInactiveBuildPhases.filter(s => s.id != id);
          //ADDING CHANGE TO DB
          addChangeSystem(6,21,projectId,taskId,null,null,null,null,null,null,null,null,activeBuildPartId,id);
          //ADDING NEW CHANGE TO DB
          //addNewProjectChange(3,6,projectId,taskId,id);
        }
        else{
          //deleting
          $('#buildPhase'+id).addClass('bg-dark');
          //$('#deleteBuildPhase'+id).off('click');
          $('#deleteBuildPhase'+id).off('click').on('click', {id, activity: 1}, onEventDeleteBuildPhase);
          //$('#deleteBuildPhase'+id).find('img').attr('src', '/undelete1.png');
          $('#deleteBuildPhase'+id).find('i').removeClass('fa-trash');
          $('#deleteBuildPhase'+id).find('i').addClass('fa-trash-restore');
          var tmp = allBuildPhases.find(s => s.id == id);
          allInactiveBuildPhases.push(tmp);
          allBuildPhases = allBuildPhases.filter(s => s.id != id);
          //ADDING CHANGE TO DB
          addChangeSystem(3,21,projectId,taskId,null,null,null,null,null,null,null,null,activeBuildPartId,id);
          //ADDING NEW CHANGE TO DB
          //addNewProjectChange(3,3,projectId,taskId,id);
        }
      }
      else{
        $('#buildPhase'+id).remove();
        var tmp = allBuildPhases.find(s => s.id == id);
        allInactiveBuildPhases.push(tmp);
        allBuildPhases = allBuildPhases.filter(s => s.id != id);
        //ADDING CHANGE TO DB
        addChangeSystem(3,21,projectId,taskId,null,null,null,null,null,null,null,null,activeBuildPartId,id);
        //ADDING NEW CHANGE TO DB
        //addNewProjectChange(3,3,projectId,taskId,id);
      }
      var numberOfCompleted = 0;
      for(var i=0; i<allBuildPhases.length; i++){
        if (allBuildPhases[i].completed)
        numberOfCompleted++;
      }
      //calc new completion of task
      var completion = Math.round((numberOfCompleted/allBuildPhases.length)*100)
      if (numberOfCompleted == 0 || allBuildPhases.length == 0)
        completion = 0;
  
      // if (allBuildPhases.length == 0){
      //   //change button to white so it can be seen from button it has no subtasks
      //   //$('#phasesBuildPart'+buildPartId).find('img').attr('src', '/pointerDown.png');
      //   $('#phasesBuildPart'+buildPartId).find('i').css('color','#ffffff');
      //   //add checkbox so it can be checked as task
      //   var checkboxElement = `<input class="custom-control-input" id="customBuildPartCheck`+buildPartId+`" type="checkbox" />
      //   <label class="custom-control-label" for="customBuildPartCheck`+buildPartId+`"> </label>`;
      //   $('#buildPartCheckbox'+buildPartId).append(checkboxElement);
      //   $('#customBuildPartCheck'+buildPartId).on('change', toggleBuildPartCheckbox);
      // }
      // //console.log(completion);
      // //post new completion to fix in db
      // //fixTaskCompletion(taskId, completion);
      // //FIX TASK COMPLETION FOR BUILD PARTS
      // fixBuildPartCompletion(buildPartId,completion);
      /*
      if (allTasks){
        var task = allTasks.find(t => t.id == taskId);
        task.build_parts_count = allBuildParts.length;
      }
      */
      var tmpBuildPart = allBuildParts.find(bp => bp.id == activeBuildPartId);
      tmpBuildPart.build_phases_count = allBuildPhases.length;
      //DOES build parts HAVE INFO ON PHASES COUNT? YES
      if (allBuildPhases.length == 0){
        //change button to white so it can be seen from button it has no build phases
        //$('#phasesBuildPart'+buildPartId).find('img').attr('src', '/pointerDown.png');
        $('#phasesBuildPart'+buildPartId).find('i').removeClass('text-dark');
        //add checkbox so it can be checked as task
        var checkboxElement = `<input class="custom-control-input" id="customBuildPartCheck`+buildPartId+`" type="checkbox" />
        <label class="custom-control-label" for="customBuildPartCheck`+buildPartId+`"> </label>`;
        $('#buildPartCheckbox'+buildPartId).append(checkboxElement);
        $('#customBuildPartCheck'+buildPartId).on('change', toggleBuildPartCheckbox);
      }
      else{
        if (count < allBuildPhases.length){
          //$('#phasesBuildPart'+buildPartId).find('img').attr('src', '/pointerDownBlack.png');
          $('#phasesBuildPart'+buildPartId).find('i').addClass('text-dark');
          $('#buildPartCheckbox'+buildPartId).empty();
        }
      }
      fixBuildPartCompletion(buildPartId,completion);
    }
    else{
      console.log('Unsuccessfully deleting build phase.');
      $('#modalDeleteBuildPhase').modal('toggle');
      $('#modalError').modal('toggle');
    }
  })
}
function onChangePhaseStartEdit(){
  var s = new Date($('#buildPhaseStartEdit').val());
  var f = new Date($('#buildPhaseFinishEdit').val());
  if (s > f){
    $('#buildPhaseFinishEdit').val($('#buildPhaseStartEdit').val())
  }
}
function onChangePhaseFinishEdit(){
  var s = new Date($('#buildPhaseStartEdit').val());
  var f = new Date($('#buildPhaseFinishEdit').val());
  if (s > f){
    $('#buildPhaseStartEdit').val($('#buildPhaseFinishEdit').val())
  }
}
//if Roboteh then choose workers otherwise its disabled
function onChangePhaseSupplierEdit(){
  if ($("#buildPhaseSupplierEdit option:selected" ).text() == 'ROBOTEH'){
    $('#buildPhaseWorkersEdit').prop('disabled', false);
  }
  else{
    $('#buildPhaseWorkersEdit').prop('disabled', true);
  }
}