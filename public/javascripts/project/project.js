//IN THIS SCRIPT//
//// control over collaps, drawing tasks, drawing page, changing category and activity of tasks, pdf ////

$(function(){
  loggedUser = {name:$('#loggedUser').html().split(" (")[0].split(" ")[0], surname:$('#loggedUser').html().split(" (")[0].split(" ")[1], role:$('#loggedUser').html().split(" (")[1].split(")")[0].toLowerCase()};
  let workersLength = $('#workersList').children().length;
  for (let i = 0; i < workersLength; i++) {
    let workerId = $('#workersList').children()[i].id.substring(13);
    if ($('#workerInfo' + workerId).text() == (loggedUser.name + ' ' + loggedUser.surname)) {
      if($('#workerRole' + workerId).text().substring(0,5) == 'vodja'){
        leader = true;
        break;
      }
      debugger;
      loggedUser.projectRole = $('#workerRole' + workerId).text();
    }
    
  }
  $('[data-toggle="popover"]').popover({ html: true });
  $('[data-toggle="tooltip"]').tooltip({trigger : 'hover'});
  $('[rel="tooltip"]').on('click', function () {
    $(this).tooltip('hide')
  })
  $.fn.modal.Constructor.prototype.enforceFocus = function() {};
  //if ($('#msg').html() != ''){
  //  $("#modalMsg").modal();
  //}
  if (completionTask == 0)
    $('#optionAllCompletion').addClass('active stay-focus');
  else if (completionTask == 1)
    $('#optionUncomplete').addClass('active stay-focus');
  else if (completionTask == 2)
    $('#optionComplete').addClass('active stay-focus');
  setTimeout(() => {
    if ($('#taskId').html() == '2'){
      //task
      selectedTaskId = urlParams.get('taskId');
      setTimeout(()=>{
        smoothScroll('#task'+selectedTaskId, 100); flashAnimation('#task'+selectedTaskId);
      })
    }
    else if ($('#taskId').html() == '3'){
      //subtask
      selectedTaskId = urlParams.get('taskId');
      selectedSubtaskId = urlParams.get('subtaskId');
      setTimeout(()=>{
        smoothScroll('#task'+selectedTaskId, 100); 
        setTimeout(()=>{ 
          toggleCollapse(selectedTaskId,1);
          setTimeout(()=>{ 
            anotherSmoothScroll('#subtaskList'+selectedTaskId,'#subtask'+selectedSubtaskId); 
            flashAnimation('#subtask'+selectedSubtaskId);
          },500);
        },500);
      },100);
    }
    else if ($('#taskId').html() == '4'){
      //note
      selectedTaskId = urlParams.get('taskId');
      selectedSubtaskId = urlParams.get('subtaskId');
      selectedNoteId = urlParams.get('noteId');
      if (selectedSubtaskId){
        setTimeout(()=>{
          smoothScroll('#task'+selectedTaskId, 100); 
          setTimeout(()=>{ 
            toggleCollapse(selectedTaskId,1);
            setTimeout(()=>{ 
              anotherSmoothScroll('#subtaskList'+selectedTaskId,'#subtask'+selectedSubtaskId); 
              //flashAnimation('#subtask'+selectedSubtaskId);
              setTimeout(()=>{
                openMessagesModal(selectedSubtaskId,2);
                setTimeout(()=>{
                  flashAnimation('#message'+selectedNoteId);
                },500)
              },200);
            },500);
          },500);
        },100);
      }
      else{
        setTimeout(()=>{
          smoothScroll('#task'+selectedTaskId, 100); 
          setTimeout(()=>{
            openMessagesModal(selectedTaskId,1);
            setTimeout(()=>{
              flashAnimation('#message'+selectedNoteId);
            },500)
          },200);
        })
      }
    }
    else if ($('#taskId').html() == '5'){
      //absence
    }
    else if ($('#taskId').html() == '7'){
      //file
      selectedTaskId = urlParams.get('taskId');
      selectedFileId = urlParams.get('fileId');
      if (selectedTaskId){
        //task's file, it has taskId
        setTimeout(()=>{
          smoothScroll('#task'+selectedTaskId, 100); 
          setTimeout(()=>{ 
            toggleCollapse(selectedTaskId,2);
            setTimeout(()=>{ 
              anotherSmoothScroll('#fileList'+selectedTaskId,'#taskFile'+selectedFileId); 
              flashAnimation('#taskFile'+selectedFileId);
            },500);
          },100);
        },100);
      }
      else{
        //project's file, general project file
        setTimeout(()=>{
          toggleFileCollapse();
          setTimeout(()=>{
            smoothScroll('#fileList',130);
            anotherSmoothScroll('#fileList','#file'+selectedFileId); 
            flashAnimation('#file'+selectedFileId);
          },500)
        })
      }
    }
  }, 200);
  //anotherSmoothScroll('#fileList'+selectedTaskId,'#taskFile'+selectedFileId);
  //loggedUser = {name:$('#loggedUser').html().split(" (")[0].split(" ")[0], surname:$('#loggedUser').html().split(" (")[0].split(" ")[1], role:$('#loggedUser').html().split(" (")[1].split(")")[0]};
  if (loggedUser.role == 'admin' || loggedUser.role.substring(0,5) == 'vodja' || loggedUser.role == 'tajnik' || loggedUser.role == 'komercialist' || loggedUser.role == 'komerciala' || loggedUser.role == 'računovodstvo')
    permissionTag = true;
  
  //UPDATE EDIT PROJECT
  $('#editProjectform').submit(function(e){
    e.preventDefault();
    $form = $(this);
    
    var name = $('#project_name').val();
    var number = $('#projectNumberPrefix').val()+$('#projectNumber').val();
    var start = $('#project_start').val();
    var finish = $('#project_finish').val();
    var subscriber = $('#project_subscriber').val();
    var notes = $('#project_notes').val();
    var active;
    if (typeof $('#project_active').prop('checked') === 'undefined') 
      active = true;
    else
      active = $('#project_active').prop('checked');
    if (start)
      start = start + " 07:00:00.000000";
    if (finish)
      finish = finish + " 15:00:00.000000";

    if (isNaN(subscriber)){
      debugger;
      //new subscriber, create subscriber and then update project
      $.post('/subscribers/create', {subscriber}, function(resp){
        var subscriberName = subscriber;
        subscriber = resp.id.id;
        $.post('/projects/update', {projectId, number, name, subscriber, start, finish, notes, active}, function(resp){
          if (resp.success){
            console.log('Successfully editing project.');
            debugger;
            $('#pinfo_name').html(name);
            $('#pinfo_number').html(number);
            $('#pinfo_notes').html(notes);
            $('#pinfo_subscriber').html(subscriberName);
            var result1 = '/';
            var result2 = '/';
            //var result = pad(dateobj.getDate())+"/"+pad(dateobj.getMonth()+1)+"/"+dateobj.getFullYear();
            if (start && finish){
              result1 = start.split(' ')[0].split('-')[2] + '.' + start.split(' ')[0].split('-')[1] + '.' + start.split(' ')[0].split('-')[0];
              result2 = finish.split(' ')[0].split('-')[2] + '.' + finish.split(' ')[0].split('-')[1] + '.' + finish.split(' ')[0].split('-')[0];
            }
            else if (start && !finish)
              result1 = start.split(' ')[0].split('-')[2] + '.' + start.split(' ')[0].split('-')[1] + '.' + start.split(' ')[0].split('-')[0];
            else if (!start && finish)
              result2 = finish.split(' ')[0].split('-')[2] + '.' + finish.split(' ')[0].split('-')[1] + '.' + finish.split(' ')[0].split('-')[0];
            
            $('#pinfo_date').html(result1+' - '+result2);
            //close collapse for edit Project
            $('#collapseEditProject').collapse("toggle");
            //ADDING NEW CHANGE TO DB
            addChangeSystem(2,1,projectId);
            //addNewProjectChange(1,2,projectId);
            //ADDING NEW CHANGE TO DB (NEW SUBSCRIBER)
            addChangeSystem(1,8,null,null,null,null,null,null,subscriber);
            //addSystemChange(8,1,null,null,null,subscriber);
          }
          else{
            console.log('Unsuccessfully editing project.');
            $('#modalError').modal('toggle');
          }
        })
      })
    }
    else{
      debugger;
      //nothing new, update project
      $.post('/projects/update', {projectId, number, name, subscriber, start, finish, notes, active}, function(resp){
        if (resp.success){
          console.log('Successfully editing project.');
          var subscriberName = allSubscribers.find(s => s.id === parseInt(subscriber)).text;
          debugger;
          $('#pinfo_name').html(name);
          $('#pinfo_number').html(number);
          $('#pinfo_notes').html(notes);
          $('#pinfo_subscriber').html(subscriberName);
          var result1 = '/'
          var result2 = '/';
          //var result = pad(dateobj.getDate())+"/"+pad(dateobj.getMonth()+1)+"/"+dateobj.getFullYear();
          if (start && finish){
            result1 = start.split(' ')[0].split('-')[2] + '.' + start.split(' ')[0].split('-')[1] + '.' + start.split(' ')[0].split('-')[0];
            result2 = finish.split(' ')[0].split('-')[2] + '.' + finish.split(' ')[0].split('-')[1] + '.' + finish.split(' ')[0].split('-')[0];
          }
          else if (start && !finish)
            result1 = start.split(' ')[0].split('-')[2] + '.' + start.split(' ')[0].split('-')[1] + '.' + start.split(' ')[0].split('-')[0];
          else if (!start && finish)
            result2 = finish.split(' ')[0].split('-')[2] + '.' + finish.split(' ')[0].split('-')[1] + '.' + finish.split(' ')[0].split('-')[0];
          
          $('#pinfo_date').html(result1+' - '+result2);
          //close collapse for edit Project
          $('#collapseEditProject').collapse("toggle");
          //ADDING NEW CHANGE TO DB
          addChangeSystem(2,1,projectId);
          //addNewProjectChange(1,2,projectId);
        }
        else{
          console.log('Unsuccessfully editing project.');
          $('#modalError').modal('toggle');
        }
      })
    }
    
  })
  //LOCALSTORAGE
  //filter build parts
  filterBuildPartsNewTask = localStorage.filterBuildPartsNewTask ? localStorage.filterBuildPartsNewTask : 'false';
  if(filterBuildPartsNewTask == 'true')
    $('#checkboxFilterBP').prop('checked', true);
  else
    $('#checkboxFilterBP').prop('checked', false);
  //show n tasks
  showNTasks = localStorage.showNTasks ? localStorage.showNTasks : '10';
  if(showNTasks != '10')
  $('#selectNumberTasks').val(showNTasks);
  //event functions
  $('#project_start').on('change', projectDatesChange);
  $('#project_finish').on('change', projectDatesChange);
  $('#projectNumber').on('change', projectNumberChange);
  $('#projectNumberPrefix').on('change', projectNumberChange);
  $('#checkboxFilterBP').on('change', onChangeFilterBuildPartsNewTask);
  $('#selectNumberTasks').on('change',onChangeSelectShowNTasks);
  //set variables with session ones if they exist
  activeTask = sessionStorage.sessionActiveTasks ? parseInt(sessionStorage.sessionActiveTasks) : 1;
  categoryTask = sessionStorage.sessionCategoryTasks ? parseInt(sessionStorage.sessionCategoryTasks) : 0;
  completionTask = sessionStorage.sessionCompletionTasks ? parseInt(sessionStorage.sessionCompletionTasks) : 1;
  showMyInputTasks = sessionStorage.sessionMyInputSwitch == 'true' ? true : false;
  showMyTasks = sessionStorage.sessionMyTasksSwitch == 'true' ? true : false;
  $('#showMyInputTasksSwitch').prop('checked', showMyInputTasks);
  $('#showMyTasksSwitch').prop('checked', showMyTasks);

  $('#optionAll').removeClass('active stay-focus');
  $('#optionTrue').removeClass('active stay-focus');
  $('#optionFalse').removeClass('active stay-focus');
  switch (activeTask) {
    case 0: $('#optionAll').addClass('active stay-focus'); break;
    case 1: $('#optionTrue').addClass('active stay-focus'); break;
    case 2: $('#optionFalse').addClass('active stay-focus'); break;
    default: $('#optionTrue').addClass('active stay-focus'); break;
  }
  $('#optionAllCompletion').removeClass('active stay-focus');
  $('#optionUncomplete').removeClass('active stay-focus');
  $('#optionComplete').removeClass('active stay-focus');
  switch (completionTask) {
    case 0: $('#optionAllCompletion').addClass('active stay-focus'); break;
    case 1: $('#optionUncomplete').addClass('active stay-focus'); break;
    case 2: $('#optionComplete').addClass('active stay-focus'); break;
    default: $('#optionUncomplete').addClass('active stay-focus'); break;
  }

  changeTaskTable(categoryTask);
  //on event call functions
  $('#taskPages').on('click', 'li', changePage);
  $('#editProjectBtn').on('click', function (event) {
    editProjectCollapse(projectId);
  });
  $('#categoryBtnGroup').on('click', 'button', function (event) {
    changeTaskTable(this.value);
  });
  $('#completionBtnGroup').on('click', 'button', function (event) {
    changeCompletion(this.value);
  });
  $('#activityBtnGroup').on('click', 'button', function (event) {
    changeActive(this.value);
  });
  $('#showMyInputTasksSwitch').on('click', changeToMyInputTasks);
  $('#showMyTasksSwitch').on('click', changeToMyTasks);
  // tabs inside textarea
  $('.text-area').on('keydown', function(e) {
    if (e.keyCode == 9){ // [TAB] key
      e.preventDefault();
      var start = this.selectionStart;
      var end = this.selectionEnd;

      this.value = this.value.substring(0, start) + '\t' + this.value.substring(end);
      this.selectionStart = this.selectionEnd = start + 1;
    }
    if (e.which == 13) { // [ENTER] key
      e.preventDefault();  // We will add newline ourselves.

      var start = this.selectionStart;
      var currentLine = this.value.slice(0, start).split('\n').pop();
      var newlineIndent = '\n' + currentLine.match(/^\s*/)[0];

      if (!document.execCommand('insertText', false, newlineIndent)) {
          // Add fallback for Firefox browser:
      }
    }
  })
  
  //add todays date
  if(leader){
    $('#taskStartAdd').val(moment().format("YYYY-MM-DD"));
  }

  // fix html escaped style
  $('#pinfo_notes').html(unescapeHtml($('#pinfo_notes').html()));
})
//OPEN EDIT PROJECT COLLAPSE
function editProjectCollapse(projectId){
  //debugger;
  //edit project is closed
  if (!editProjectCollapseOpen){
    if (!allSubscribers){
      $.get( "/subscribers/all", function( data ) {
        allSubscribers = data.data;
        fillEditProjectData();
        $('#collapseEditProject').collapse("toggle");
        editProjectCollapseOpen = true;
      });
    }
    else{
      fillEditProjectData();
      $('#collapseEditProject').collapse("toggle");
      editProjectCollapseOpen = true;
    }
  }
  //edit project is open
  else{
    $('#collapseEditProject').collapse("toggle");
    editProjectCollapseOpen = false;
  }
}
function fillEditProjectData(){
  $(".select2-subscriber").select2({
    data: allSubscribers,
    tags: true,
  });
  $('#projectNumber').removeClass("is-invalid");
  $('#projectNumber').removeClass("is-valid");
  $('#btnEditProject').prop("disabled", false).removeClass("ui-state-disabled").addClass("ui-state-enabled");
  debugger;
  var tmp = $('#pinfo_number').html();
  if (tmp.substring(0,2) == 'RT'){
    $('#projectNumberPrefix').val('RT');
    $('#projectNumber').val(tmp.substring(2));
  }
  else if (tmp.substring(0,3) == 'STR'){
    $('#projectNumberPrefix').val('STR');
    $('#projectNumber').val(tmp.substring(3));
  }
  else{
    $('#projectNumberPrefix').val('RT');
    $('#projectNumber').val(tmp);
  }
  //$('#project_number').val($('#pinfo_number').html());
  $('#project_name').val($('#pinfo_name').html());
  var x = allSubscribers.find(s => s.text == unescapeHtml($("#pinfo_subscriber").html()));
  $('#project_subscriber').val(x.id).trigger('change');
  var date = $('#pinfo_date').html().split(" - ");
  var s = date[0];
  var f = date[1];
  $('#project_start').val(s.split('.')[2]+'-'+s.split('.')[1]+'-'+s.split('.')[0]);
  $('#project_finish').val(f.split('.')[2]+'-'+f.split('.')[1]+'-'+f.split('.')[0]);
  $('#project_notes').val($('#pinfo_notes').html());
  projectNumberChange();
}
function onEventToggleCollapse(event) {
  toggleCollapse(event.data.taskId, event.data.type);
}
//OPEN EDIT TASK COLLAPSE
function toggleCollapse(taskId, type){
  buildPhasesCollapseOpen = false;
  collapseBuildPhaseContent = false;
  collapseNewBuildPhaseContent = false;
  collapseNewBuildPhaseOpen = false;
  //debugger;
  //if == 0 then there is no open collapse or open is one of this task's collapse
  if (activeTaskId == 0){
    activateTask(taskId, type);
  }
  //there is open collapse
  //close collapse, empty it and open this collapse
  else{
    if (editTaskCollapseOpen){
      $('#collapseEditTask'+activeTaskId).collapse("toggle");
      editTaskCollapseOpen = false;
      $('#collapseEditTask'+activeTaskId).empty();
      //it was open, want to close collapse edit
      if (type == 0 && taskId == activeTaskId)
        activeTaskId = 0;
      else
        activateTask(taskId, type);
    }
    else if (subtaskCollapseOpen){
      $('#collapseSubtasks'+activeTaskId).collapse("toggle");
      subtaskCollapseOpen = false;
      $('#collapseSubtasks'+activeTaskId).empty();
      //it was open, want to close collapse subtasks
      if (type == 1 && taskId == activeTaskId)
        activeTaskId = 0;
      else
        activateTask(taskId, type);
    }
    else if (taskFilesCollapseOpen){
      $('#collapseTaskFiles'+activeTaskId).collapse("toggle");
      taskFilesCollapseOpen = false;
      $('#collapseTaskFiles'+activeTaskId).empty();
      //it was open, want to close collapse files
      if (type == 2 && taskId == activeTaskId)
        activeTaskId = 0;
      else
        activateTask(taskId, type);
    }
    else if (taskAbsenceCollapseOpen){
      $('#collapseTaskAbsence'+activeTaskId).collapse("toggle");
      taskAbsenceCollapseOpen = false;
      $('#collapseTaskAbsence'+activeTaskId).empty();
      //it was open, want to close collapse absence
      if (type == 3 && taskId == activeTaskId)
        activeTaskId = 0;
      else
        activateTask(taskId, type);
    }
    else if (taskBuildPartsCollapseOpen){
      $('#collapseTaskBuildParts'+activeTaskId).collapse("toggle");
      taskBuildPartsCollapseOpen = false;
      $('#collapseTaskBuildParts'+activeTaskId).empty();
      //it was open, want to close collapse build parts
      if (type == 4 && taskId == activeTaskId)
        activeTaskId = 0;
      else
        activateTask(taskId, type);
    }
  }
}
function activateTask(taskId, type){
  activeTaskId = taskId;
  if (type == 0)
    editTask(taskId);
  else if (type == 1)
    openSubtasks(taskId);
  else if (type == 2)
    openTaskFiles(taskId);
  else if (type == 3)
    openTaskAbsence(taskId);
  else if (type == 4)
    openTaskBuildParts(taskId);
}
//CHANGE CATEGORY FOR ALL TASKS
function changeTaskTable(category){
  //debugger;
  $('#option0').removeClass('active stay-focus');
  $('#option1').removeClass('active stay-focus');
  $('#option2').removeClass('active stay-focus');
  $('#option3').removeClass('active stay-focus');
  $('#option4').removeClass('active stay-focus');
  $('#option5').removeClass('active stay-focus');
  $('#option6').removeClass('active stay-focus');
  $('#option7').removeClass('active stay-focus');
  setTimeout(()=>{
    $('#option'+category+'').addClass('active stay-focus');
  })
  categoryTask = category;
  sessionStorage.sessionCategoryTasks = category;
  if ($('#optionAll').hasClass('active'))
    activeTask = 0;
  else if ($('#optionTrue').hasClass('active'))
    activeTask = 1;
  else if ($('#optionFalse').hasClass('active'))
    activeTask = 2;
  $.get( "/projects/tasks", {projectId, categoryTask, activeTask, completionTask, showMyInputTasks, showMyTasks}, function( data ) {
    //console.log(data)
    //debugger
    allTasks = data.data;
    //shownTasks = data.data.splice(0,10);
    taskLoaded = true;
    //drawPagination(Math.ceil(allTasks.length/10));
    if(showNTasks == '-1')
      drawPagination(1);
    else
      drawPagination(Math.ceil(allTasks.length/parseInt(showNTasks)));
    drawTasks(1);
    if (timelineCollapseOpen)
      refreshTimeline();
    //$('[data-toggle="popover"]').popover({trigger: "hover"});
  });
}
//CHANGE TASKS TO SEE ONLY ACTIVE OR ALL
function changeActive(seeActive){
  //debugger;
  $('#optionAll').removeClass('active stay-focus');
  $('#optionTrue').removeClass('active stay-focus');
  $('#optionFalse').removeClass('active stay-focus');
  activeTask = seeActive;
  sessionStorage.sessionActiveTasks = seeActive;
  if ($('#option0').hasClass('active'))
    categoryTask = 0;
  else if ($('#option1').hasClass('active'))
    categoryTask = 1;
  else if ($('#option2').hasClass('active'))
    categoryTask = 2;
  else if ($('#option3').hasClass('active'))
    categoryTask = 3;
  else if ($('#option4').hasClass('active'))
    categoryTask = 4;
  else if ($('#option5').hasClass('active'))
    categoryTask = 5;
  else if ($('#option6').hasClass('active'))
    categoryTask = 6;
  else if ($('#option7').hasClass('active'))
    categoryTask = 7;
  $.get( "/projects/tasks", {projectId, categoryTask, activeTask, completionTask, showMyInputTasks, showMyTasks}, function( data ) {
    //console.log(data)
    allTasks = data.data;
    //shownTasks = data.data.splice(0,10);
    taskLoaded = true;
    //debugger
    if(showNTasks == '-1')
      drawPagination(1);
    else
      drawPagination(Math.ceil(allTasks.length/parseInt(showNTasks)));
    drawTasks(1);
    if (seeActive == 1)
      $('#optionTrue').addClass('active stay-focus');
    else if (seeActive == 2)
      $('#optionFalse').addClass('active stay-focus');
    else
      $('#optionAll').addClass('active stay-focus');
    if (timelineCollapseOpen)
      refreshTimeline();
    //$('[data-toggle="popover"]').popover({trigger: "hover"});
  });
}
//CHANGE TASK TO SEE ALL/UNFINISHED/FINISHED
function changeCompletion(seeCompletion){
  $('#optionAllCompletion').removeClass('active stay-focus');
  $('#optionUncomplete').removeClass('active stay-focus');
  $('#optionComplete').removeClass('active stay-focus');
  completionTask = seeCompletion;
  sessionStorage.sessionCompletionTasks = seeCompletion;
  setTimeout(()=>{
    if (seeCompletion == 1)
      $('#optionUncomplete').addClass('active stay-focus');
    else if (seeCompletion == 2)
      $('#optionComplete').addClass('active stay-focus');
    else
      $('#optionAllCompletion').addClass('active stay-focus');
  })
  changeTaskTable(categoryTask);
}
//REMOVE/EMPTY TASK LIST AND DRAW/SHOW NEEDED TASKS
function drawTasks(page){
  $('#pagePrev').removeClass('disabled');
  $('#pageNext').removeClass('disabled');
  if (page == 1)
    $('#pagePrev').addClass('disabled');
  if ($('#page'+page).next().attr('id') == 'pageNext')
    $('#pageNext').addClass('disabled');
  $('#page'+activePage).removeClass("active");
  $('#page'+page).addClass("active");
  activePage = page;
  //calculate which task to show/draw
  let limit = 10, start = 0;
  if(showNTasks == '-1'){
    start = 0;
    limit = allTasks.length;
  }
  else{
    limit = page * parseInt(showNTasks);
    start = limit - parseInt(showNTasks);
    limit = Math.min(limit, allTasks.length);
  }

  // debugger;
  //first empty task list and reset collapse and active task id
  $('#taskList').empty();
  // debugger;
  editProjectCollapseOpen = false;
  editTaskCollapseOpen = false;
  subtaskCollapseOpen = false;
  taskFilesCollapseOpen = false;
  taskAbsenceCollapseOpen = false;
  taskBuildPartsCollapseOpen = false;
  activeTaskId = 0;
  var deleteTaskClass;
  var myTask = false;
  //draw 10 tasks
  for(var i = start; i < limit; i++){
    myTask = false;
    var id = allTasks[i].id;
    deleteTaskClass = '';
    if ((loggedUser.role == 'admin' && allTasks[i].active == false))
      deleteTaskClass = ' bg-secondary';
    //check if has no subtasks -> then add checkbox
    //if has no subtasks and completion == 100 then checked else not checked
    //checkbox
    var checkboxElement = '';
    var noteElement = '';
    var isChecked = '';
    let taskNote = allTasks[i].task_note ? allTasks[i].task_note : '';
    var category = allTasks[i].category.toUpperCase();
    var categoryClass = ''; 
    if (category == 'NABAVA')
      categoryClass = 'badge-purchase';
    else if (category == 'KONSTRUKCIJA')
      categoryClass = 'badge-construction';
    else if (category == 'STROJNA IZDELAVA')
      categoryClass = 'badge-mechanic';
    else if (category == 'ELEKTRO IZDELAVA')
      categoryClass = 'badge-electrican';
    else if (category == 'MONTAŽA')
      categoryClass = 'badge-assembly';
    else if (category == 'PROGRAMIRANJE')
      categoryClass = 'badge-programmer';
    else if (category == 'STROJNA MONTAŽA - ROBOTEH')
      categoryClass = 'badge-mechanic';
    else if (category == 'STROJNA MONTAŽA - STRANKA')
      categoryClass = 'badge-mechanic';
    else if (category == 'ELEKTRO MONTAŽA - ROBOTEH')
      categoryClass = 'badge-electrican';
    else if (category == 'ELEKTRO MONTAŽA - STRANKA')
      categoryClass = 'badge-electrican';
    else if (category == 'ELEKTRO PROEKTIRANJE')
      categoryClass = 'badge-electrican';
    else if (category == 'DOBAVA')
      categoryClass = 'badge-primary';
    else if (category == 'SAT')
      categoryClass = 'badge-primary';
    else if (category == 'FAT')
      categoryClass = 'badge-primary';
    else if (category == 'ZAGON')
      categoryClass = 'badge-primary';
    else if (category == 'NAKLAD')
      categoryClass = 'badge-primary';
    else if (category == 'BREZ')
      category = '';
    //debugger;
    //messages icon
    let commentsIcon = '<i class="far fa-comments fa-lg"></i>';
    if(allTasks[i].notes_count && allTasks[i].notes_count > 0)
      commentsIcon = '<i class="fas fa-comments fa-lg"></i>';
    //workers
    var taskWorkers = [];
    if (allTasks[i].workers){
      taskWorkers = allTasks[i].workers.split(", ");
      var tmp = taskWorkers.find(w => w == (loggedUser.name +" "+ loggedUser.surname))
      if (tmp)
        myTask = true;
    }
    if (leader || myTask || loggedUser.role == 'admin' || loggedUser.role.substring(0,5) == 'vodja' || (((loggedUser.role == 'konstrukter' || loggedUser.role == 'konstruktor') || (loggedUser.projectRole && loggedUser.projectRole == 'konstrukter')) && allTasks[i].category == 'Strojna izdelava')){
      if (!allTasks[i].subtask_count && !allTasks[i].build_parts_count){
        if (allTasks[i].completion == 100)
          isChecked = 'checked = ""';
        checkboxElement = `<input class="custom-control-input" id="customTaskCheck`+id+`" type="checkbox" `+isChecked+` />
        <label class="custom-control-label" for="customTaskCheck`+id+`"> </label>`;
      }
      else if (allTasks[i].category == 'Strojna izdelava'){
        if (allTasks[i].completion == 100)
          isChecked = 'checked = ""';
        checkboxElement = `<input class="custom-control-input" id="customTaskCheck`+id+`" type="checkbox" `+isChecked+` />
        <label class="custom-control-label" for="customTaskCheck`+id+`"> </label>`;
      }
      noteElement = `<button class="invisible-button ml-2 btn-on-stretched">` + commentsIcon + `</button>`;
    }
    else{
      noteElement = `<button class="invisible-button ml-2 btn-on-stretched" disabled>` + commentsIcon + `</button>`;
    }
    //date
    var date = '';
    if (allTasks[i].formatted_start && allTasks[i].formatted_finish)
      date = allTasks[i].formatted_start.date + " - " + allTasks[i].formatted_finish.date;
    else if (allTasks[i].formatted_start)
      date = allTasks[i].formatted_start.date + " - /";
    else if (allTasks[i].formatted_finish)
      date = "/ - " + allTasks[i].formatted_finish.date;
    //red date text (task should be finished by now)
    var todayDate = new Date();
    var tmpDate = new Date(allTasks[i].task_finish);
    var dateText = '';
    if (todayDate > tmpDate && allTasks[i].completion < 100)
      dateText = 'text-danger';
    //priority
    var priorityClass = '';
    if (allTasks[i].priority == 'visoka')
      priorityClass = 'border-high-priority';
    else if (allTasks[i].priority == 'srednja')
      priorityClass = 'border-medium-priority';
    else if (allTasks[i].priority == 'nizka')
      priorityClass = 'border-low-priority';
    //workers
    var workers = "";
    if (allTasks[i].workers)
      workers = allTasks[i].workers;

    //control buttons
    //delete button
    var deleteControlButton = '';
    if (loggedUser.role == 'admin' || loggedUser.role.substring(0,5) == 'vodja' || leader || (allTasks[i].author == (loggedUser.name + ' ' + loggedUser.surname)))
      deleteControlButton = `<button class="btn btn-danger ml-2 btn-on-stretched" id="taskButtonDelete`+id+`" data-toggle="tooltip" data-original-title="Odstrani">
        <i class="fas fa-trash fa-lg"></i>
      </button>`;
    //edit button
    var editControlButton = '';
    if (loggedUser.role == 'admin' || loggedUser.role.substring(0,5) == 'vodja' || leader || (((loggedUser.role == 'konstrukter' || loggedUser.role == 'konstruktor') || (loggedUser.projectRole && loggedUser.projectRole == 'konstrukter')) && allTasks[i].category == 'Strojna izdelava')){
      editControlButton = `<button class="btn btn-primary ml-1 btn-on-stretched" id='taskButtonEdit`+id+`' data-toggle="tooltip" data-original-title="Uredi">
        <i class="fas fa-pen fa-lg"></i>
      </button>`;
    }
    //build part button
    let selectForBuildPartDocRow = ``;
    var buildPartControlButton = '';
    let buildPartsButtonClass = 'btn-on-stretched', subtaskButtonClass = 'stretched-link';
    var mechanicalTaskDisable = '';
    var mechanicalTaskStyle = 'd-none';
    if  (allTasks[i].category == 'Strojna izdelava'){
      mechanicalTaskStyle = '';
      if((allTasks[i].build_parts_count && allTasks[i].build_parts_count > 0) || (loggedUser.role == 'konstrukter'|| (loggedUser.projectRole && loggedUser.projectRole == 'konstrukter'))){
        buildPartsButtonClass = 'stretched-link';
        subtaskButtonClass = 'btn-on-stretched';
        if(allTasks[i].build_parts_count > 0)
          mechanicalTaskDisable = ' disabled=""';
      }
    }
    if (loggedUser.role.substring(0,5) == 'vodja' || loggedUser.role == 'admin' || leader || myTask || loggedUser.role == 'strojnik' || loggedUser.role == 'cnc operater' || loggedUser.role == 'strojni monter' || loggedUser.role == 'varilec' || (((loggedUser.role == 'konstrukter' || loggedUser.role == 'konstruktor') || (loggedUser.projectRole && loggedUser.projectRole == 'konstrukter')) && allTasks[i].category == 'Strojna izdelava')){
      buildPartControlButton = `<button class="btn btn-success ml-1 `+buildPartsButtonClass+` `+mechanicalTaskStyle+`" id='taskButtonBuildParts`+id+`' data-toggle="tooltip" data-original-title="Kosovnica">
        <i class="fas fa-pencil-ruler fa-lg"></i>
      </button>`;
      if (allTasks[i].build_parts_count && allTasks[i].build_parts_count > 0)
        buildPartControlButton = `<button class="btn btn-success ml-1 `+buildPartsButtonClass+` `+mechanicalTaskStyle+`" id='taskButtonBuildParts`+id+`' data-toggle="tooltip" data-original-title="Kosovnica">
          <i class="fas fa-pencil-ruler fa-lg text-dark"></i>
        </button>`;
    }
    if (addBPSelection && allTasks[i].category == 'Strojna izdelava') {
      selectForBuildPartDocRow = `<div class='row'>
          <div class="custom-control custom-switch ml-auto mt-1 mb-n2 mr-3 btn-on-stretched"><input class="custom-control-input" id="selectForBPDoc`+id+`" type="checkbox"><label class="custom-control-label" for="selectForBPDoc`+id+`">Vključi v dokumentacijo</label></div>
      </div>`;
    }
    //absence button
    var absenceControlButton = '';
      if (loggedUser.role.substring(0,5) == 'vodja' || loggedUser.role == 'admin' || leader || myTask || (((loggedUser.role == 'konstrukter' || loggedUser.role == 'konstruktor') || (loggedUser.projectRole && loggedUser.projectRole == 'konstrukter')) && allTasks[i].category == 'Strojna izdelava')){
      absenceControlButton = `<button class="btn btn-programmer ml-1 btn-on-stretched" id='taskButtonAbsence`+id+`' data-toggle="tooltip" data-original-title="Odsotnosti">
        <i class="fas fa-clock fa-lg"></i>
      </button>`;
      //debugger;
      if (allTasks[i].full_absence_count && allTasks[i].full_absence_count > 0)
        absenceControlButton = `<button class="btn btn-programmer ml-1 btn-on-stretched" id='taskButtonAbsence`+id+`' data-toggle="tooltip" data-original-title="Odsotnosti">
          <i class="fas fa-clock fa-lg text-dark"></i>
        </button>`;
      if (allTasks[i].task_absence_count && allTasks[i].task_absence_count > 0)
        absenceControlButton = `<button class="btn btn-programmer ml-1 btn-on-stretched" id='taskButtonAbsence`+id+`' data-toggle="tooltip" data-original-title="Odsotnosti">
          <i class="fas fa-clock fa-lg text-dark"></i>
        </button>`;
      if (!allTasks[i].workers || !allTasks[i].task_start || !allTasks[i].task_finish)
        absenceControlButton = `<button class="btn btn-programmer ml-1 btn-on-stretched" disabled id='taskButtonAbsence`+id+`' data-toggle="tooltip" data-original-title="Odsotnosti">
          <i class="fas fa-clock fa-lg"></i>
        </button>`;
    }
    //files button
    var filesControlButton = '';
    if (loggedUser.role.substring(0,5) == 'vodja' || loggedUser.role == 'admin' || leader || myTask || (((loggedUser.role == 'konstrukter' || loggedUser.role == 'konstruktor')|| (loggedUser.projectRole && loggedUser.projectRole == 'konstrukter')) && allTasks[i].category == 'Strojna izdelava')){
      filesControlButton = `<button class="btn btn-purchase ml-1 btn-on-stretched" id='taskButtonFile`+id+`' data-toggle="tooltip" data-original-title="Datoteke">
        <i class="fas fa-file fa-lg"></i>
      </button>`;
      if (allTasks[i].files_count && allTasks[i].files_count > 0)
        filesControlButton = `<button class="btn btn-purchase ml-1 btn-on-stretched" id='taskButtonFile`+id+`' data-toggle="tooltip" data-original-title="Datoteke">
          <i class="fas fa-file fa-lg text-dark"></i>
        </button>`;
    }
    //subtask button
    var subtaskControlButton = '';
    //if (loggedUser.role == 'vodja' || loggedUser.role == 'vodja projektov' || loggedUser.role == 'vodja strojnikov' || loggedUser.role == 'vodja CNC obdelave' || loggedUser.role == 'vodja električarjev' || loggedUser.role == 'vodja programerjev' || loggedUser.role == 'admin' || leader || myTask){
    if (true){
      subtaskControlButton = `<button class="btn btn-info ml-1 `+subtaskButtonClass+`" `+mechanicalTaskDisable+` id='taskButtonSubtask`+id+`' data-toggle="tooltip" data-original-title="Naloge">
        <i class="fas fa-check-square fa-lg"></i>
      </button>`;
      if (allTasks[i].subtask_count && allTasks[i].subtask_count > 0)
      subtaskControlButton = `<button class="btn btn-info ml-1 `+subtaskButtonClass+`" `+mechanicalTaskDisable+` id='taskButtonSubtask`+id+`' data-toggle="tooltip" data-original-title="Naloge">
        <i class="fas fa-check-square fa-lg text-dark"></i>
      </button>`;
    }
    //putting everything together into one element
    var element = `<div class="list-group-item `+priorityClass+deleteTaskClass+`" id="task`+id+`">
      <div class="row">
        <div class="col-md-6">
          <span id="taskName`+id+`" class="font-weight-bold btn-on-stretched">`+allTasks[i].task_name+`</span>
          <small id="taskNote`+id+`" class="form-text text-muted mt-n2 btn-on-stretched">` + taskNote + `</small>
        </div>
        <div class="col-md-6 d-flex">
          <div class="checkbox-xl custom-control custom-checkbox table-custom-checkbox btn-on-stretched" id="taskCheckbox`+id+`">
            `+checkboxElement+`
          </div>
          `+noteElement+`
          <div class="ml-2" id="taskInfo`+id+`"><a class="workerspopover btn-on-stretched" data-toggle="popover" data-trigger="hover" data-content="`+workers+`" data-original-title="" title=""><i class="fas fa-user-friends fa-lg text-dark"></i></a></div>
          <div class="progress ml-2 w-80">
            <div class="progress-bar w-`+allTasks[i].completion+`" id="taskProgress`+id+`"></div>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="d-flex ml-3">
          <div class="`+dateText+` btn-on-stretched" id="taskDate`+id+`">`+date+`</div>
          <div id="taskBadges`+id+`">
            <span class="badge badge-pill `+categoryClass+` ml-2">`+category+`</span>
          </div>
        </div>
        <div class="d-flex ml-auto mr-3">
          <div class="ml-auto" id="taskButtons`+id+`">
            <a class="mypopover p-1 btn-on-stretched" data-toggle="popover" data-trigger="focus" tabindex="0" data-content="ni zabeleženih sprememb" id="taskHistory`+id+`" data-original-title="" title=""><i class="fas fa-history fa-lg text-dark"></i></a>
            `+deleteControlButton+`
            `+buildPartControlButton+`
            `+absenceControlButton+`
            `+filesControlButton+`
            `+subtaskControlButton+`
            `+editControlButton+`
          </div>
        </div>
      </div>
      `+selectForBuildPartDocRow+`
      <div class="collapse multi-collapse task-collapse btn-on-stretched" id="collapseEditTask`+id+`"></div>
      <div class="collapse multi-collapse task-collapse btn-on-stretched" id="collapseSubtasks`+id+`"></div>
      <div class="collapse multi-collapse task-collapse btn-on-stretched" id="collapseTaskFiles`+id+`"></div>
      <div class="collapse multi-collapse task-collapse btn-on-stretched" id="collapseTaskAbsence`+id+`"></div>
      <div class="collapse multi-collapse task-collapse btn-on-stretched" id="collapseTaskBuildParts`+id+`"></div>
    </div>`;
    //debugger;
    $('#taskList').append(element);
    $('#customTaskCheck'+id).on('change', toggleTaskCheckbox);
    $('#task'+id).find('.invisible-button').on('click', {id, type: 1}, onClickOpenMessagesModal);
    $('#taskButtonEdit'+id).on('click', {taskId: id, type: 0}, onEventToggleCollapse);
    $('#taskButtonSubtask'+id).on('click', {taskId: id, type: 1}, onEventToggleCollapse);
    $('#taskButtonFile'+id).on('click', {taskId: id, type: 2}, onEventToggleCollapse);
    $('#taskButtonAbsence'+id).on('click', {taskId: id, type: 3}, onEventToggleCollapse);
    $('#taskButtonBuildParts'+id).on('click', {taskId: id, type: 4}, onEventToggleCollapse);
    $('#taskHistory'+id).on('click', onEventGetTaskChanges);
    $('#taskButtonDelete'+id).on('click', onEventDeleteTask);
    $('#selectForBPDoc'+id).on('click', {id}, onEventAddBPForDoc);
    if(addBPSelection && allTasks[i].category == 'Strojna izdelava'){
      let tmp = tasksForBPDoc.find(t => t == id)
      if(tmp) $('#selectForBPDoc'+id).prop('checked', true)
    }
  }
  //popover
  $('[data-toggle="popover"]').popover({ html: true });
  $('[data-toggle="tooltip"]').tooltip({trigger : 'hover'});
  $('[rel="tooltip"]').on('click', function () {
    $(this).tooltip('hide')
  })
}
//CHANGE PAGE OF TASKS LIST
function changePage(event){
  let page = this.value;
  if (page == 0){
    $('#'+this.id).hasClass('next') ? page = 'next' : page = 'prev';
  }
  //debugger
  //page is prev or next
  if (isNaN(page)){
    //check if task are loaded
    if (page == 'next')
      page = activePage + 1;
    else if (page == 'prev')
      page = activePage - 1;
    if (taskLoaded){
      drawTasks(page);
    }
    else{
      $.get( "/projects/tasks", {projectId, categoryTask, activeTask, completionTask, showMyInputTasks, showMyTasks}, function( data ) {
        allTasks = data.data;
        taskLoaded = true;
        debugger
        drawTasks(page);
      });
    }
  }
  //page is number
  else{
    //page isnt active otherwise do nothing
    if (!$('#page'+page).hasClass('active')){
      debugger;
      if (taskLoaded){
        drawTasks(page);
      }
      else{
        $.get( "/projects/tasks", {projectId, categoryTask, activeTask, completionTask, showMyInputTasks, showMyTasks}, function( data ) {
          allTasks = data.data;
          taskLoaded = true;
          debugger
          drawTasks(page);
        });
      }
    }
  }
}
//DRAW PAGINATION
function drawPagination(pages){
  //debugger;
  $('#taskPages').empty();
  var missingPages = '';
  for(var i = 2; i <= pages; i++){
    missingPages += '<li class="page-item" value="'+i+'" id="page'+i+'"><a class="page-link" href="#collapseTableTask">'+i+'</a></li>';
  }
  var pagesElement = `<ul class="pagination pagination-sm">
  <li class="page-item disabled" id="pagePrev"><a class="page-link" href="#collapseTableTask">«</a></li>
  <li class="page-item active" value="1" id="page1"><a class="page-link" href="#collapseTableTask">1</a></li>
    `+missingPages+`
    <li class="page-item next" value="0" id="pageNext"><a class="page-link" href="#collapseTableTask">»</a></li>
  </ul>`;
  $('#taskPages').append(pagesElement);
}
function projectDatesChange(){
  debugger;
  var start = $('#project_start').val();
  var finish = $('#project_finish').val();
  if (start && finish){
    if (new Date($('#project_start').val()) > new Date($('#project_finish').val())){
      //$('#project_finish').val($('#project_start').val());
      $('#project_start').addClass('is-invalid');
      $('#project_finish').addClass('is-invalid');
      $('#btnEditProject').prop('disabled', true);
    }
    else{
      $('#project_start').removeClass('is-invalid');
      $('#project_finish').removeClass('is-invalid');
      $('#btnEditProject').prop('disabled', false);
    }
  }
  else{
    $('#project_start').removeClass('is-invalid');
    $('#project_finish').removeClass('is-invalid');
    $('#btnEditProject').prop('disabled', false);
  }
}
function projectFinishChange(){
  debugger;
  var start = $('#project_start').val();
  var finish = $('#project_finish').val();
  if (start && finish){
    if (new Date($('#project_start').val()) >= new Date($('#project_finish').val())){
      $('#project_start').val($('#project_finish').val());
    }
  }
}
/*
function projectNumberChange(){
  var currentNumber = $('#pinfo_number').html();
  var newNumber = $('#project_number').val();
  $('#project_number').removeClass('is-valid');
  $('#project_number').removeClass('is-invalid');
  if (newNumber){
    $.get('/projects/all', {projectId}, function(resp){
      var allProjects = resp.data;
      var conflict = allProjects.find(p => p.project_number == newNumber);
      if (conflict && (newNumber != currentNumber)){
        $('#project_number').addClass('is-invalid');
        $('#btnEditProject').prop('disabled', true)
      }
      else{
        $('#project_number').addClass('is-valid');
        $('#btnEditProject').prop('disabled', false)
      }
    })
  }
}
*/
function projectNumberChange(){
  var currentNumber = $('#pinfo_number').html();
  var tmpNumber = $('#projectNumberPrefix').val()+$('#projectNumber').val();
  $.get( "/projects/all", function( data ) {
    var allProjects = data.data;
    if ($('#projectNumberPrefix').val() == 'RT'){
      allProjects = allProjects.filter(p => p.project_number.substring(0,2) == 'RT');
    }
    else if ($('#projectNumberPrefix').val() == 'STR'){
      allProjects = allProjects.filter(p => p.project_number.substring(0,3) == 'STR');
    }
    var conflictTmp = allProjects.find(c => c.project_number == tmpNumber && c.project_number != currentNumber);
    if (conflictTmp){
      $('#projectNumber').removeClass("is-valid").addClass("is-invalid");
      $('#btnEditProject').prop('disabled', true);
      //$('#btnEditProject').prop("disabled", true).removeClass("ui-state-enabled").addClass("ui-state-disabled");
    }
    else{
      $('#projectNumber').removeClass("is-invalid").addClass("is-valid");
      $('#btnEditProject').prop('disabled', false);
      //$('#btnEditProject').prop("disabled", false).removeClass("ui-state-disabled").addClass("ui-state-enabled");
    }
  });
}
function onChangeFilterBuildPartsNewTask(event) {
  if ($('#checkboxFilterBP').prop('checked') == true){
    filterBuildPartsNewTask = 'true';
    localStorage.setItem('filterBuildPartsNewTask', filterBuildPartsNewTask);
  }
  else{
    filterBuildPartsNewTask = 'false';
    localStorage.setItem('filterBuildPartsNewTask', filterBuildPartsNewTask);
  }
}
function onChangeFilterBuildPartsNewBP(event) {
  if ($('#checkboxFilterBPAdd').prop('checked') == true){
    filterBuildPartsNewTask = 'true';
    localStorage.setItem('filterBuildPartsNewTask', filterBuildPartsNewTask);
  }
  else{
    filterBuildPartsNewTask = 'false';
    localStorage.setItem('filterBuildPartsNewTask', filterBuildPartsNewTask);
  }
}
function onChangeSelectShowNTasks(event) {
  let tmp = $('#selectNumberTasks').val();
  if(showNTasks != tmp){
    debugger
    showNTasks = tmp;
    localStorage.showNTasks = tmp;
    if(showNTasks == '-1')
      drawPagination(1);
    else
      drawPagination(Math.ceil(allTasks.length/parseInt(showNTasks)));
    drawTasks(1)
  }
}
//SHOW ONLY MY TASKS(tasks that i have created)
function changeToMyInputTasks(){
  //debugger;
  showMyInputTasks = showMyInputTasks ? false : true;
  sessionStorage.sessionMyInputSwitch = showMyInputTasks;
  
  $.get( "/projects/tasks", {projectId, categoryTask, activeTask, completionTask, showMyInputTasks, showMyTasks}, function( data ) {
    //console.log(data)
    debugger
    allTasks = data.data;
    //shownTasks = data.data.splice(0,10);
    taskLoaded = true;
    //drawPagination(Math.ceil(allTasks.length/10));
    if(showNTasks == '-1')
      drawPagination(1);
    else
      drawPagination(Math.ceil(allTasks.length/parseInt(showNTasks)));
    drawTasks(1);
    //$('[data-toggle="popover"]').popover({trigger: "hover"});
  });
}
//SHOW ONLY MY TASKS(tasks that i have created)
function changeToMyTasks(){
  //debugger;
  showMyTasks = showMyTasks ? false : true;
  sessionStorage.sessionMyTasksSwitch = showMyTasks;
  
  $.get( "/projects/tasks", {projectId, categoryTask, activeTask, completionTask, showMyInputTasks, showMyTasks}, function( data ) {
    //console.log(data)
    debugger
    allTasks = data.data;
    //shownTasks = data.data.splice(0,10);
    taskLoaded = true;
    //drawPagination(Math.ceil(allTasks.length/10));
    if(showNTasks == '-1')
      drawPagination(1);
    else
      drawPagination(Math.ceil(allTasks.length/parseInt(showNTasks)));
    drawTasks(1);
    //$('[data-toggle="popover"]').popover({trigger: "hover"});
  });
}
//SMOOTH SCROLL ANIMATION TO THE JQUERY SELECTOR
/*
function smoothScroll(selector, diff){
  if (!diff)
    diff = 0;
  $("html, body").animate({ scrollTop: $(selector).offset().top-diff }, 1000);
}
*/
/*
function smoothScroll (selector) {
  document.querySelector(selector).scrollIntoView({
    behavior: 'smooth'
  });
}
*/
//FLASH ANIMATION WITH FADE OUT AND IN
/*
function flashAnimation(element){
  $(element).delay(100).fadeOut().fadeIn().fadeOut().fadeIn().fadeOut().fadeIn()
}
*/
var urlParams = new URLSearchParams(window.location.search);
var projectId = urlParams.get('id');
var selectedTaskId;
var selectedSubtaskId;
var selectedNoteId;
var selectedFileId;
var selectedAbsenceId;
var leader = false;
//var tid;
var taskLoaded = false;
//tables//
var allTasks;
var allSubtasks;
var allAbsence;
var allReasons;
var allWorkersTable;
//tables//
var shownTasks;
var loggedUser;
var categoryTask = 0;
var activeTask = 1;
var activePage;
//0 = all, 1 = unfinished, 2 = finished
var completionTask = 1;
//for select2 --> (id, text)//
var allSubscribers;
var allWorkers;
var allCategories;
var allPriorities;
//for select2 --> (id, text)//
var editProjectCollapseOpen = false;
var editTaskCollapseOpen = false;
var subtaskCollapseOpen = false;
var taskFilesCollapseOpen = false;
var taskAbsenceCollapseOpen = false;
var taskBuildPartsCollapseOpen = false;
var activeTaskId = 0;
var imageLoaded = true;
var showMyInputTasks = false;
var showMyTasks = false;
let filterBuildPartsNewTask, showNTasks, sessionCategoryTasks, sessionCompletionTasks, sessionActiveTasks, sessionMyInputSwitch, sessionMyTasksSwitch;