//IN THIS SCRIPT//
//// opening build parts, add/edit/delete for one build part, adding multiple add the same time through csv ////

$(function(){
  $('#buildPartStandardEdit').on('keyup', onChangeStandardEditInput);
  $('#btnDeleteBuildPartConf').on('click', deleteBuildPartConfirm);
})
//OPEN TASK ABSENCE COLLAPSE
function openTaskBuildParts(taskId){
  //debugger;
  if (taskLoaded){
    makeTaskBuildParts(taskId);
  }
  else{
    $.get( "/projects/tasks", {projectId,categoryTask,activeTask, completionTask}, function( data ) {
      //console.log(data)
      //debugger
      allTasks = data.data;
      taskLoaded = true;
      makeTaskBuildParts(taskId);
    });
  }

  //$('#collapseTaskAbsence'+taskId).collapse("toggle");
  //taskAbsenceCollapseOpen = true;
  
}
//FILL ABSENCE LIST
function makeTaskBuildParts(taskId){
  //debugger;
  collapseNewTaskBuildPartsOpen = false;
  collapseNewTaskBuildPartsContent = false;
  //var task = allTasks.find(t => t.id == taskId);
  $.get('/buildparts/get', {taskId}, function(data){
    //debugger;
    allBuildParts = data.buildParts;
    allInactiveBuildParts = [];
    //frame for build parts list
    var addBuildPartButton ='', invisbleButton = '', makeDocBPButton = '', makeCSVBPButton = '';
    if (loggedUser.role.substring(0,5) == 'vodja' || loggedUser.role == 'admin' || leader || (loggedUser.role == 'konstrukter' || loggedUser.role == 'konstruktor' || (loggedUser.projectRole && loggedUser.projectRole == 'konstrukter'))){
      invisbleButton = '<button class="btn btn-success invisible">Dodaj kos</button>';
      addBuildPartButton = '<button id="btnAddBuildPart'+taskId+'" class="btn btn-success ml-3">Dodaj kos</button>';
      makeDocBPButton = '<button id="btnCreateBPDoc'+taskId+'" class="btn btn-info mr-3" data-toggle="tooltip" data-original-title="Ustvari dokumentacijo"><i class="fas fa-download fa-lg"></i></button>';
      makeCSVBPButton = '<button id="btnCreateBPCSV'+taskId+'" class="btn btn-info mr-3" data-toggle="tooltip" data-original-title="Ustvari CSV"><i class="fas fa-file-csv fa-lg"></i></button>';
    }
    let sort = localStorage.buildPartsSort ? localStorage.buildPartsSort : 'standard';
    let order = localStorage.buildPartsOrder ? localStorage.buildPartsOrder : 'desc';
    let sortSelects = '', orderSelects = '';
    switch (sort) {
      case 'id': sortSelects = `<option value="id" selected="">Id</option><option value="standard">Št. risbe</option>`; break;
      //case 'position': sortSelects = `<option value="id">Id</option><option value="position" selected="">Pozicija</option><option value="standard">Št. risbe</option>`; break;
      case 'standard': sortSelects = `<option value="id">Id</option><option value="standard" selected="">Št. risbe</option>`; break;
      default: sortSelects = `<option value="id">Id</option><option value="standard" selected="">Št. risbe</option>`; break;
    }
    orderSelects = order == 'desc' ? `<option value="desc" selected="">padajoče</option><option value="asc">naraščajoče</option>` : `<option value="desc">padajoče</option><option value="asc" selected="">naraščajoče</option>`;
    
    var frame1 = `<hr />
      <div class="d-flex">
        <i class="fas fa-sort fa-lg mt-2 ml-3 mr-1"></i>
        <select class="form-control w-125px" id="buildPartSortSelect">
          `+sortSelects+`
        </select>
        <i class="fas fa-sort-amount-down fa-lg mt-2 ml-3 mr-1"></i>
        <select class="form-control w-125px" id="buildPartOrderSelect">
          `+orderSelects+`
        </select>
      </div>
      <div class="list-group d-flex ml-3 mr-3 span-absence-list" id="buildPartsList`+taskId+`"></div><br />
      <div class="collapse multi-collapse task-collapse" id="collapseNewTaskBuildPart`+taskId+`"></div>
      <div id="addBuildPartRow`+taskId+`" class="d-flex justify-content-between">
        `+invisbleButton+`
        `+addBuildPartButton+`
        <div>
          `+makeCSVBPButton+`
          `+makeDocBPButton+`
        </div>
      </div><br />`;
    $('#collapseTaskBuildParts'+taskId).append(frame1);
    $('#buildPartSortSelect').on('change', onChangeSortBuildParts);
    $('#buildPartOrderSelect').on('change', onChangeSortBuildParts);
    $('#btnAddBuildPart'+taskId).on('click', function (event) {
      addNewTaskBuildPart(taskId);
    });
    $('#btnCreateBPDoc'+taskId).on('click', function (event) {
      createBPDoc(taskId);
    });
    $('#btnCreateBPCSV'+taskId).on('click', function (event) {
      createBPCSV(taskId);
    });

    //apply sorting
    allBuildParts.sort(compareValues(sort, order));
    //append build parts in frame list
    for(var i = 0; i < allBuildParts.length; i++){
      //variables
      let position = allBuildParts[i].position ? allBuildParts[i].position : '',
      name = allBuildParts[i].name ? allBuildParts[i].name : '',
      quantity = allBuildParts[i].quantity ? allBuildParts[i].quantity : '',
      material = allBuildParts[i].material ? allBuildParts[i].material : '',
      standard = allBuildParts[i].standard ? allBuildParts[i].standard : '',
      note = allBuildParts[i].note ? allBuildParts[i].note : '',
      tag = allBuildParts[i].tag ? allBuildParts[i].tag : '';
      //control, activity, buttons
      var checkedCB = '';
      var checkboxElement = '';
      if (!allBuildParts[i].build_phases_count){
        if (allBuildParts[i].completion == 100)
          checkedCB = 'checked=""';
        checkboxElement = `<input class="custom-control-input" id="customBuildPartCheck`+allBuildParts[i].id+`" type="checkbox" `+checkedCB+` />
          <label class="custom-control-label" for="customBuildPartCheck`+allBuildParts[i].id+`"> </label>`;
      }
      var deleteElement = '';
      var editElement = '';
      var phaseElement = '';
      var deleteActivity = '';
      var deleteIcon = 'trash';
      var deleteTaskClass = '';
      if (allBuildParts[i].active == false){
        deleteActivity = '1';
        deleteIcon = 'trash-restore';
        deleteTaskClass = 'bg-secondary';
      }
      // add date if exist
      if (allBuildParts[i].date){
        tag = new Date(allBuildParts[i].date).toLocaleString('sl');
      }
      //make delete and edit button
      if (loggedUser.role.substring(0,5) == 'vodja' || loggedUser.role == 'admin' || leader || loggedUser.role == 'konstrukter' || loggedUser.role == 'konstruktor' || (loggedUser.projectRole && loggedUser.projectRole == 'konstrukter')){
        deleteElement = `<button class="btn btn-danger task-button mr-2 mt-1" id="deleteBuildPart`+allBuildParts[i].id+`">
          <i class="fas fa-`+deleteIcon+` fa-lg" data-toggle="tooltip" data-original-title="Odstrani"></i>
        </button>`;
        editElement = `<button class="btn btn-primary task-button mr-2 mt-1" id="editBuildPart`+allBuildParts[i].id+`">
          <i class="fas fa-pen fa-lg" data-toggle="tooltip" data-original-title="Uredi"></i>
        </button>`;
      }
      //make phases button
      if (allBuildParts[i].build_phases_count && allBuildParts[i].build_phases_count > 0){
        phaseElement = `<button class="btn btn-success task-button mr-2 mt-1" id="phasesBuildPart`+allBuildParts[i].id+`">
          <i class="fas fa-angle-down fa-lg text-dark" data-toggle="tooltip" data-original-title="Faze"></i>
        </button>`;
      }
      else{
        phaseElement = `<button class="btn btn-success task-button mr-2 mt-1" id="phasesBuildPart`+allBuildParts[i].id+`">
          <i class="fas fa-angle-down fa-lg" data-toggle="tooltip" data-original-title="Faze"></i>
        </button>`;
      }
      var progressBar = `<div class="progress ml-2 w-80" id="buildPartProgressControl`+allBuildParts[i].id+`"><div class="progress-bar w-`+allBuildParts[i].completion+`" id="buildPartProgress`+allBuildParts[i].id+`"></div></div>`;

      let selectBPForBuildPartDocRow = '';
      if (addBPSelection && allBuildParts[i].active) {
        selectBPForBuildPartDocRow = `<div class='row'>
            <div class="custom-control custom-switch ml-auto mt-1 mb-n2 mr-3 btn-on-stretched"><input class="custom-control-input" id="selectBPForBPDoc`+allBuildParts[i].id+`" type="checkbox"><label class="custom-control-label" for="selectBPForBPDoc`+allBuildParts[i].id+`">Vključi v dokumentacijo</label></div>
        </div>`;
      }

      var element = `<div class="list-group-item `+deleteTaskClass+`" id="buildPart`+allBuildParts[i].id+`">
        <div class="row">
          <div class="col-md-1">
            <div id="buildPartPosition`+allBuildParts[i].id+`">`+position+`</div>
          </div>
          <div class="col-md-4">
            <div class="ml-2" id="buildPartName`+allBuildParts[i].id+`">`+name+`</div>
          </div>
          <div class="col-md-3">
            <div class="ml-2" id="buildPartStandard`+allBuildParts[i].id+`">`+standard+`</div>
          </div>
          <div class="col-md-4 d-flex">
            <div class="ml-auto" id="buildPartQuantity`+allBuildParts[i].id+`">`+quantity+`</div>
            `+progressBar+`
          </div>
        </div>
        <div class="row">
          <div class="col-md-3 d-flex">
            <div class="mt-3" id="buildPartMaterial`+allBuildParts[i].id+`">`+material+`</div>
          </div>
          <div class="col-md-3">
            <div class="ml-2 mt-3" id="buildPartNote`+allBuildParts[i].id+`">`+note+`</div>
          </div>
          <div class="col-md-2 mt-3 mb-n3">
            <small class="ml-2" id="buildPartTag`+allBuildParts[i].id+`">`+tag+`</small>
          </div>
          <div class="col-md-4 d-flex">
            <div class="ml-auto d-flex" id="buildPartControl`+allBuildParts[i].id+`">
              <div class="custom-control custom-checkbox ml-auto mt-2" id="buildPartCheckbox`+allBuildParts[i].id+`">
                `+checkboxElement+`
              </div>
              `+deleteElement+`
              `+phaseElement+`
              `+editElement+`
            </div>
          </div>
        </div>
        `+selectBPForBuildPartDocRow+`
        <div class="collapse multi-collapse task-build-phase" id="collapseBuildPhases`+allBuildParts[i].id+`"></div>
      </div>`;
      let tmpBuildPartId = allBuildParts[i].id;
      if (allBuildParts[i].active == false){
        if (loggedUser.role == 'admin'){
          $('#buildPartsList'+taskId).append(element);
          $('#customBuildPartCheck'+allBuildParts[i].id).on('change', toggleBuildPartCheckbox);
          $('#deleteBuildPart'+allBuildParts[i].id).on('click', {id: allBuildParts[i].id, activity: deleteActivity}, onEventDeleteBuildPart);
          $('#editBuildPart'+allBuildParts[i].id).on('click', {id: allBuildParts[i].id}, onEventOpenEditBuildPart);
          $('#phasesBuildPart'+allBuildParts[i].id).on('click', {id: allBuildParts[i].id}, onEventOpenBuildPhasesCollapse);
          $('#selectBPForBPDoc'+allBuildParts[i].id).on('click', function (event) {
            addSpecificBPForDoc(tmpBuildPartId);
          })
          //if (allBuildParts[i].mark_screw)
          //  $('#phasesBuildPart'+allBuildParts[i].id).hide();
        }
        // var id = allBuildParts[i].id;
        // allInactiveBuildParts.push(allBuildParts[i]);
        // allBuildParts = allBuildParts.filter(s => s.id != id);
        // i--;
      }
      else{
        $('#buildPartsList'+taskId).append(element);
        $('#customBuildPartCheck'+allBuildParts[i].id).on('change', toggleBuildPartCheckbox);
        $('#deleteBuildPart'+allBuildParts[i].id).on('click', {id: allBuildParts[i].id, activity: deleteActivity}, onEventDeleteBuildPart);
        $('#editBuildPart'+allBuildParts[i].id).on('click', {id: allBuildParts[i].id}, onEventOpenEditBuildPart);
        $('#phasesBuildPart'+allBuildParts[i].id).on('click', {id: allBuildParts[i].id}, onEventOpenBuildPhasesCollapse);
        $('#selectBPForBPDoc'+allBuildParts[i].id).on('click', function (event) {
          addSpecificBPForDoc(tmpBuildPartId);
        })
        //if (allBuildParts[i].mark_screw)
        //  $('#phasesBuildPart'+allBuildParts[i].id).hide();
      }
      if (addBPSelection && allBuildParts[i].active) {
        if($('#selectForBPDoc'+taskId).prop('checked')){
          $('#selectBPForBPDoc'+allBuildParts[i].id).prop('checked', true);
        }
        else{
          //check if task id exist in buildPartsForBPDoc and build part id is in it then mark it as selected
          let tmp = buildPartsForBPDoc.find(bp => bp.id == activeTaskId);
          if(tmp && tmp.buildParts.find(bpi => bpi == allBuildParts[i].id)){
            $('#selectBPForBPDoc'+allBuildParts[i].id).prop('checked', true);
          }
        }
      }
    }
    $('[data-toggle="tooltip"]').tooltip();
    $('#collapseTaskBuildParts'+taskId).collapse("toggle");
    taskBuildPartsCollapseOpen = true;
    setTimeout(()=>{
      smoothScroll('#buildPartsList'+taskId, 100);
    },500)
  })
}
//////////////////////////////////////DELETE BUILD PART
function onEventDeleteBuildPart(event) {
  deleteBuildPart(event.data.id, event.data.activity);
}
//DELETE SELECTED Build part
function deleteBuildPart(id, activity){
  var active = false;
  if (activity && activity == 1)
  active = true;
  savedBuildPartId = id;
  savedBuildPartActive = active;
  savedBuildPartActivity = activity;
  if (active){
    $('#btnDeleteBuildPartConf').html('Ponovno dodaj');
    $('#deleteBuildPartModalMsg').html('Ste prepričani, da želite ponovno dodati izbrisan kos?');
  }
  else{
    $('#btnDeleteBuildPartConf').html('Odstrani');
    $('#deleteBuildPartModalMsg').html('Ste prepričani, da želite odstraniti izbrani kos?');
  }
  $('#modalDeleteBuildPart').modal('toggle');
}
function deleteBuildPartConfirm(){
  var taskId = activeTaskId;
  var id = savedBuildPartId;
  var active = savedBuildPartActive;
  var activity = savedBuildPartActivity;
  //debugger;
  //not sure why i need taskId for deletin subtask
  $.post('/buildparts/delete', {id, active}, function(resp){
    //debugger
    if (resp.success){
      console.log('Successfully deleting build part.');
      $('#modalDeleteBuildPart').modal('toggle');
      if (loggedUser.role == 'admin'){
        if (activity && activity == 1){
          //resurection
          $('#buildPart'+id).removeClass('bg-secondary');
          $('#deleteBuildPart'+id).off('click').on('click', {id}, onEventDeleteBuildPart);
          //$('#deleteBuildPart'+id).find('img').attr('src', '/delete.png');
          $('#deleteBuildPart'+id).find('i').removeClass('fa-trash-restore');
          $('#deleteBuildPart'+id).find('i').addClass('fa-trash');
          // var tmp = allInactiveBuildParts.find(s => s.id == id);
          var tmp = allBuildParts.find(s => s.id == id);
          tmp.active = active;
          // allBuildParts.push(tmp);
          // allInactiveBuildParts = allInactiveBuildParts.filter(s => s.id != id);
          //ADDING NEW CHANGE TO DB
          addChangeSystem(6,20,projectId,taskId,null,null,null,null,null,null,null,null,id);
          //addNewProjectChange(3,6,projectId,taskId,id);
        }
        else{
          //deleting
          $('#buildPart'+id).addClass('bg-secondary');
          $('#deleteBuildPart'+id).off('click').on('click', {id, activity: 1}, onEventDeleteBuildPart);
          //$('#deleteBuildPart'+id).find('img').attr('src', '/undelete1.png');
          $('#deleteBuildPart'+id).find('i').removeClass('fa-trash');
          $('#deleteBuildPart'+id).find('i').addClass('fa-trash-restore');
          var tmp = allBuildParts.find(s => s.id == id);
          tmp.active = active;
          // allInactiveBuildParts.push(tmp);
          // allBuildParts = allBuildParts.filter(s => s.id != id);
          //ADDING NEW CHANGE TO DB
          addChangeSystem(3,20,projectId,taskId,null,null,null,null,null,null,null,null,id);
          //addNewProjectChange(3,3,projectId,taskId,id);
        }
      }
      else{
        $('#buildPart'+id).remove();
        var tmp = allBuildParts.find(s => s.id == id);
        tmp.active = active;
        // allInactiveBuildParts.push(tmp);
        // allBuildParts = allBuildParts.filter(s => s.id != id);
        //ADDING NEW CHANGE TO DB
        addChangeSystem(3,20,projectId,taskId,null,null,null,null,null,null,null,null,id);
        //addNewProjectChange(3,3,projectId,taskId,id);
      }
      var tmp = allBuildParts.filter(bp => bp.completion == 100 && bp.mark_screw == false && bp.active == true);
      var all = allBuildParts.filter(bp => bp.mark_screw == false && bp.active == true);

      //calc new completion of task
      var taskCompletion = Math.round((tmp.length/all.length)*100)
      if (taskCompletion == 0 || allBuildParts.length == 0)
        taskCompletion = 0;
      debugger;
      fixTaskCompletion(activeTaskId,taskCompletion);
      if (allTasks){
        var task = allTasks.find(t => t.id == taskId);
        task.build_parts_count = allBuildParts.length;
      }
      if (allBuildParts.length == 0){
        //change button to white so it can be seen from button it has no build parts
        //$('#taskButtonBuildParts'+taskId).find('img').attr('src', '/buildWhite.png');
        $('#taskButtonBuildParts'+taskId).find('i').removeClass('text-dark');
        //add checkbox so it can be checked as task
        var checkboxElement = `<input class="custom-control-input" id="customTaskCheck`+taskId+`" type="checkbox" />
        <label class="custom-control-label" for="customTaskCheck`+taskId+`"> </label>`;
        $('#taskCheckbox'+taskId).append(checkboxElement);
        $('#customTaskCheck'+taskId).on('click', toggleTaskCheckbox);
      }
      else{
        //$('#taskButtonBuildParts'+taskId).find('img').attr('src', '/buildBlack.png');
        $('#taskButtonBuildParts'+taskId).find('i').addClass('text-dark');
        $('#taskCheckbox'+taskId).empty();
      }
    }
    else{
      console.log('Unsuccessfully deleting build part.');
      $('#modalDeleteBuildPart').modal('toggle');
      $('#modalError').modal('toggle');
    }
  })
}
///////////////////////////////ADD NEW BUILD PART
//open collapse for adding new build part
function addNewTaskBuildPart(taskId){
  debugger;
  if (!collapseNewTaskBuildPartsContent){
    debugger;
    let filterChecked = (filterBuildPartsNewTask == 'true') ? ' checked=""' : '';
    let unusedElements = `<div class="custom-control custom-checkbox">
      <input class="custom-control-input" id="checkboxScrewsAdd" type="checkbox" name="customCheckBuildType" />
      <label class="custom-control-label" for="checkboxScrewsAdd">Vijaki</label>
    </div>
    <div class="custom-control custom-checkbox">
      <input class="custom-control-input" id="checkboxSuppliersAdd" type="checkbox" name="customCheckBuildType" />
      <label class="custom-control-label" for="checkboxSuppliersAdd">Dobavitelji</label>
    </div>
    <div class="custom-control custom-checkbox">
      <input class="custom-control-input" id="checkboxMaterialsAdd" type="checkbox" name="customCheckBuildType" checked="" />
      <label class="custom-control-label" for="checkboxMaterialsAdd">Risbe</label>
    </div>`;
    var element = `<div class="row d-flex justify-content-end">
      <button class="close mr-4" type="button" aria-label="Close"><span aria-label="true">x</span></button>
    </div>
    <div class="input-group mt-1 mb-1">
      <div class="custom-file">
        <label class="custom-file-label" for="inputBuildPartCsv" aria-describedby="inputBuildPartCsvAddon" id="inputBuildPartCsvLabel">Izberi datoteko (csv)</label>
        <input type="file" class="custom-file-input card-report" id="inputBuildPartCsv" label="test" accept=".csv" multiple>
        <div class="invalid-feedback">Vnesite csv kosovnice!</div>
      </div>
    </div>
    <div class="d-flex justify-content-around mb-2">
      <div class="custom-control custom-switch">
        <input class="custom-control-input" id="checkboxFilterBPAdd" type="checkbox" `+filterChecked+`>
        <label class="custom-control-label" for="checkboxFilterBPAdd">Filtriraj obstoječe</label>
      </div>
      <div class="custom-control custom-switch">
        <input class="custom-control-input" id="checkboxFilterSKAdd" type="checkbox" checked="">
        <label class="custom-control-label" for="checkboxFilterSKAdd">Standardni kosi</label>
      </div>
    </div>
    <div class="form-row">
      <div class="col-md-6">
        <label class="w-100" for="buildPartNameAdd">Naziv
          <input class="form-control" id="buildPartNameAdd" type="text" name="buildPartNameAdd" required="">
          <div class="invalid-feedback">Vnesite naziv!</div>
        </label>
      </div>
      <div class="col-md-6">
        <label class="w-100" for="buildPartStandardAdd">Št.risbe/standard
          <input class="form-control" id="buildPartStandardAdd" type="text" name="buildPartStandardAdd" required="" autocomplete="off">
          <div id="buildPartStandardAddInvalid" class="invalid-feedback">Vnesite št.risbe/standard!</div>
          <div id="buildPartStandardAddValid" class="valid-feedback">Pravilen format.</div>
        </label>
      </div>
    </div>
    <div class="form-row">
      <div class="col-md-3">
        <label class="w-100" for="buildPartQuantityAdd">Količina
          <input class="form-control" id="buildPartQuantityAdd" type="number" value='1' name="buildPartQuantityAdd" required="">
          <div class="invalid-feedback">Vnesite količino!</div>
        </label>
      </div>
      <div class="col-md-9">
        <label class="w-100" for="buildPartNoteAdd">Opomba<input class="form-control" id="buildPartNoteAdd" type="text" name="buildPartNoteAdd">
        </label>
      </div>
    </div>
    <div class="d-flex justify-content-center mb-2 ml-n3"><div class="custom-control custom-checkbox"><input class="custom-control-input" id="buildPartSestavAdd" type="checkbox"><label class="custom-control-label" for="buildPartSestavAdd">Sestav</label></div></div>
    <div class="" id="newBPFileNames">
    </div>`;
    $('#collapseNewTaskBuildPart'+taskId).append(element);
    $('#collapseNewTaskBuildPart'+taskId).find('.close').on('click', {id: taskId}, onEventCloseCollapseNewTaskBuildPart);
    if (allBuildParts && allBuildParts.length > 0)
    $('#buildPartPositionAdd').val(allBuildParts[0].position+1);
    //add on change function to csv input
    $('#inputBuildPartCsv').change(onChangeCSVInput);
    $('#checkboxFilterBPAdd').on('change', onChangeFilterBuildPartsNewBP);
    $('#buildPartStandardAdd').on('keyup', onChangeStandardAddInput);
    //reasons&materials
    /*
    if (!allMaterials || !allStandards){
      $.get('/buildparts/standards', function(data){
        allStandards = data.standards;
        allMaterials = data.materials;
        $(".select2-standard-add").select2({
          data: allStandards,
          tags: permissionTag
        });
        $(".select2-material-add").select2({
          data: allMaterials,
          tags: permissionTag
        });
      })
    }
    else{
      $(".select2-standard-add").select2({
        data: allStandards,
        tags: permissionTag
      });
      $(".select2-material-add").select2({
        data: allMaterials,
        tags: permissionTag
      });
    }
    */
    collapseNewTaskBuildPartsContent = true;
  }
  if (!collapseNewTaskBuildPartsOpen){
    debugger;
    $('#collapseNewTaskBuildPart'+taskId).collapse("toggle");
    collapseNewTaskBuildPartsOpen = true;
    $('#btnAddBuildPart'+taskId).html('Potrdi');
    smoothScroll('#collapseNewTaskBuildPart'+taskId, 100);
  }
  else{
    debugger;
    if ($('#inputBuildPartCsv').val() != ''){
      //parseCsvToBuildParts();
      creteBuildPartsFromCsv(allResults);
    }
    else if ($('#buildPartNameAdd').val() != '' && $('#buildPartStandardAdd').val() != '' && $('#buildPartPositionAdd').val() != '' && $('#buildPartQuantityAdd').val() != ''){
      $('#buildPartNameAdd').removeClass('is-invalid');
      $('#buildPartStandardAdd').removeClass('is-invalid');
      $('#buildPartPositionAdd').removeClass('is-invalid');
      $('#buildPartQuantityAdd').removeClass('is-invalid');
      //$('#buildPartMaterialAdd').removeClass('is-invalid');
      //var start = $('#absenceStart').val() + " 07:00:00.000000";
      //var finish = $('#absenceFinish').val() + " 15:00:00.000000";
      //var worker = $('#absenceWorker').val();
      //var reason = $('#absenceReason').val();
      var taskId = activeTaskId;
      var name = $('#buildPartNameAdd').val();
      var standard = $('#buildPartStandardAdd').val();
      var position = $('#buildPartPositionAdd').val();
      var quantity = $('#buildPartQuantityAdd').val();
      var material = $('#buildPartMaterialAdd').val();
      var note = $('#buildPartNoteAdd').val();
      var tag = $('#buildPartTagAdd').val();
      let sestav = $('#buildPartSestavAdd').prop('checked');
      //no empty inputs, add new absence
      debugger;
      $.post('/buildparts/add', {taskId,name,standard,position,quantity,material,note,tag,sestav}, function(data){
        debugger;
        if (data.success){
          console.log('Successfully adding new build part.')
          console.log(data);
          /*
          var tmpStandard, tmpMaterial;
          if (data.materialId){
            tmpMaterial = {id:data.materialId, text:material, description:null,material:true,supplier:false,screw_quality:false};
            allMaterials.push(tmpMaterial);
            $(".select2-material-add").select2({
              data: allMaterials,
              tags: permissionTag
            });
          }
          else{
            tmpMaterial = allMaterials.find(m => m.id == material);
          }
          if (data.standardId){
            tmpStandard = {id:data.standardId, text:standard};
            allStandards.push(tmpStandard);
            $(".select2-standard-add").select2({
              data: allStandards,
              tags: permissionTag
            });
          }
          else{
            tmpStandard = allStandards.find(s => s.id == standard);
          }
          */
          //make new build part variable and create new element on page
          var tmpBuildPart = {
            id: data.buildPart.id,
            position: data.buildPart.position,
            name: data.buildPart.name,
            quantity: data.buildPart.quantity,
            material: data.buildPart.material,
            //mark: tmpMaterial.text,
            //mark_id: tmpMaterial.id,
            //mark_material: tmpMaterial.material,
            //mark_supplier: tmpMaterial.supplier,
            //mark_screw: tmpMaterial.screw_quality,
            standard: data.buildPart.standard,
            //standard: tmpStandard.text,
            //standard_id: tmpStandard.id,
            note: data.buildPart.note,
            tag: data.buildPart.tag,
            active: true,
            completion: 0
          }
          if(allBuildParts)
            allBuildParts.unshift(tmpBuildPart);
          onChangeSortBuildParts();
          //reset inputs
          $('#buildPartNameAdd').val('');
          $('#buildPartStandardAdd').val('');
          $('#buildPartPositionAdd').val(parseInt(position)+1);
          $('#buildPartQuantityAdd').val(1);
          $('#buildPartMaterialAdd').val('');
          $('#buildPartNoteAdd').val('');
          $('#buildPartTagAdd').val('');
          //close collapse for adding new build part
          $('#collapseNewTaskBuildPart'+taskId).collapse("toggle");
          $('#btnAddBuildPart'+taskId).html('Dodaj kos');
          collapseNewTaskBuildPartsOpen = false;
          if(allBuildParts){
            //FIX TASK COMPLETION
            var tmp = allBuildParts.filter(bp => bp.completion == 100);
            var all = allBuildParts.filter(bp => bp);
            //calc new completion of task
            var taskCompletion = Math.round((tmp.length/all.length)*100)
            if (taskCompletion == 0 || allBuildParts.length == 0)
              taskCompletion = 0;
            debugger;
            fixTaskCompletion(activeTaskId,taskCompletion);
            //change button to black cuz now task has subtasks
            //$('#taskButtonBuildParts'+activeTaskId).find('img').attr('src', '/buildBlack.png');
            $('#taskButtonBuildParts'+activeTaskId).find('i').css('color', '#000000');
            //remove checkbox on task
            $('#taskCheckbox'+activeTaskId).empty();
            $('[data-toggle="tooltip"]').tooltip();
          }
          //scroll animation
          smoothScroll('#buildPartsList'+activeTaskId, 100);
          anotherSmoothScroll('#buildPartsList'+activeTaskId,'#buildPart'+tmpBuildPart.id);
          flashAnimation('#buildPart'+tmpBuildPart.id);
          //ADDING CHANGE TO DB
          addChangeSystem(1,20,projectId,taskId,null,null,null,null,null,null,null,null,tmpBuildPart.id);
        }
        else{
          console.log('Unsuccessfully adding new build part.')
          $('#modalError').modal('toggle');
        }
      })
    }
    else{
      if ($('#buildPartNameAdd').val() == '')
      $('#buildPartNameAdd').addClass('is-invalid');
      if ($('#buildPartStandardAdd').val() == ''){
        $('#buildPartStandardAddInvalid').html('Vnesite št.risbe/standard!');
        $('#buildPartStandardAdd').removeClass('is-valid').addClass('is-invalid');
      }
      if ($('#buildPartPositionAdd').val() == '')
        $('#buildPartPositionAdd').addClass('is-invalid');
      if ($('#buildPartQuantityAdd').val() == '')
        $('#buildPartQuantityAdd').addClass('is-invalid');
      // if ($('#buildPartMaterialAdd').val() == '')
      //   $('#buildPartMaterialAdd').addClass('is-invalid');
    }     
  }
}
function onChangeStandardAddInput(event) {
  debugger;
  //$('#buildPartStandardAdd').removeClass('is-valid').addClass('is-invalid');
  let input = $('#buildPartStandardAdd').val();
  if((/RT\d\d\d-\d\d-\d\d-\d\d-\d\d-\d\d-\d/).test( input ) || (/RT\d\d\d \d\d \d\d \d\d \d\d \d\d \d/).test( input )){
    $('#buildPartStandardAdd').removeClass('is-invalid').addClass('is-valid');
  }
  else{
    if((/RT\d\d\d-\d\d-\d\d-\d\d-\d\d-\d\d-/).test( input )){
      $('#buildPartStandardAddInvalid').html('Pravilen format je: RTXXX-XX-XX-XX-XX-XX-<strong>X</strong>');
    }
    else if((/RT\d\d\d-\d\d-\d\d-\d\d-\d\d-\d\d/).test( input )){
      $('#buildPartStandardAddInvalid').html('Pravilen format je: RTXXX-XX-XX-XX-XX-XX<strong>-X</strong>');
    }
    else if((/RT\d\d\d-\d\d-\d\d-\d\d-\d\d-\d/).test( input )){
      $('#buildPartStandardAddInvalid').html('Pravilen format je: RTXXX-XX-XX-XX-XX-X<strong>X-X</strong>');
    }
    else if((/RT\d\d\d-\d\d-\d\d-\d\d-\d\d-/).test( input )){
      $('#buildPartStandardAddInvalid').html('Pravilen format je: RTXXX-XX-XX-XX-XX-<strong>XX-X</strong>');
    }
    else if((/RT\d\d\d-\d\d-\d\d-\d\d-\d\d/).test( input )){
      $('#buildPartStandardAddInvalid').html('Pravilen format je: RTXXX-XX-XX-XX-XX<strong>-XX-X</strong>');
    }
    else if((/RT\d\d\d-\d\d-\d\d-\d\d-\d/).test( input )){
      $('#buildPartStandardAddInvalid').html('Pravilen format je: RTXXX-XX-XX-XX-X<strong>X-XX-X</strong>');
    }
    else if((/RT\d\d\d-\d\d-\d\d-\d\d-/).test( input )){
      $('#buildPartStandardAddInvalid').html('Pravilen format je: RTXXX-XX-XX-XX-<strong>XX-XX-X</strong>');
    }
    else if((/RT\d\d\d-\d\d-\d\d-\d\d/).test( input )){
      $('#buildPartStandardAddInvalid').html('Pravilen format je: RTXXX-XX-XX-XX<strong>-XX-XX-X</strong>');
    }
    else if((/RT\d\d\d-\d\d-\d\d-\d/).test( input )){
      $('#buildPartStandardAddInvalid').html('Pravilen format je: RTXXX-XX-XX-X<strong>X-XX-XX-X</strong>');
    }
    else if((/RT\d\d\d-\d\d-\d\d-/).test( input )){
      $('#buildPartStandardAddInvalid').html('Pravilen format je: RTXXX-XX-XX-<strong>XX-XX-XX-X</strong>');
    }
    else if((/RT\d\d\d-\d\d-\d\d/).test( input )){
      $('#buildPartStandardAddInvalid').html('Pravilen format je: RTXXX-XX-XX<strong>-XX-XX-XX-X</strong>');
    }
    else if((/RT\d\d\d-\d\d-\d/).test( input )){
      $('#buildPartStandardAddInvalid').html('Pravilen format je: RTXXX-XX-X<strong>X-XX-XX-XX-X</strong>');
    }
    else if((/RT\d\d\d-\d\d-/).test( input )){
      $('#buildPartStandardAddInvalid').html('Pravilen format je: RTXXX-XX-<strong>XX-XX-XX-XX-X</strong>');
    }
    else if((/RT\d\d\d-\d\d/).test( input )){
      $('#buildPartStandardAddInvalid').html('Pravilen format je: RTXXX-XX<strong>-XX-XX-XX-XX-X</strong>');
    }
    else if((/RT\d\d\d-\d/).test( input )){
      $('#buildPartStandardAddInvalid').html('Pravilen format je: RTXXX-X<strong>X-XX-XX-XX-XX-X</strong>');
    }
    else if((/RT\d\d\d-/).test( input )){
      $('#buildPartStandardAddInvalid').html('Pravilen format je: RTXXX-<strong>XX-XX-XX-XX-XX-X</strong>');
    }
    else if((/RT\d\d\d/).test( input )){
      $('#buildPartStandardAddInvalid').html('Pravilen format je: RTXXX<strong>-XX-XX-XX-XX-XX-X</strong>');
    }
    else if((/RT\d\d/).test( input )){
      $('#buildPartStandardAddInvalid').html('Pravilen format je: RTXX<strong>X-XX-XX-XX-XX-XX-X</strong>');
    }
    else if((/RT\d/).test( input )){
      $('#buildPartStandardAddInvalid').html('Pravilen format je: RTX<strong>XX-XX-XX-XX-XX-XX-X</strong>');
    }
    else if((/RT/).test( input )){
      $('#buildPartStandardAddInvalid').html('Pravilen format je: RT<strong>XXX-XX-XX-XX-XX-XX-X</strong>');
    }
    else if((/R/).test( input )){
      $('#buildPartStandardAddInvalid').html('Pravilen format je: R<strong>TXXX-XX-XX-XX-XX-XX-X</strong>');
    }
    else
      $('#buildPartStandardAddInvalid').html('Pravilen format je: <strong>RTXXX-XX-XX-XX-XX-XX-X</strong>');
    $('#buildPartStandardAdd').removeClass('is-valid').addClass('is-invalid');
  }
}
function onChangeStandardEditInput(event) {
  debugger;
  //$('#buildPartStandardAdd').removeClass('is-valid').addClass('is-invalid');
  let input = $('#buildPartStandardEdit').val();
  if((/RT\d\d\d-\d\d-\d\d-\d\d-\d\d-\d\d-\d/).test( input ) || (/RT\d\d\d \d\d \d\d \d\d \d\d \d\d \d/).test( input )){
    $('#buildPartStandardEdit').removeClass('is-invalid').addClass('is-valid');
  }
  else{
    if((/RT\d\d\d-\d\d-\d\d-\d\d-\d\d-\d\d-/).test( input )){
      $('#buildPartStandardEditInvalid').html('Pravilen format je: RTXXX-XX-XX-XX-XX-XX-<strong>X</strong>');
    }
    else if((/RT\d\d\d-\d\d-\d\d-\d\d-\d\d-\d\d/).test( input )){
      $('#buildPartStandardEditInvalid').html('Pravilen format je: RTXXX-XX-XX-XX-XX-XX<strong>-X</strong>');
    }
    else if((/RT\d\d\d-\d\d-\d\d-\d\d-\d\d-\d/).test( input )){
      $('#buildPartStandardEditInvalid').html('Pravilen format je: RTXXX-XX-XX-XX-XX-X<strong>X-X</strong>');
    }
    else if((/RT\d\d\d-\d\d-\d\d-\d\d-\d\d-/).test( input )){
      $('#buildPartStandardEditInvalid').html('Pravilen format je: RTXXX-XX-XX-XX-XX-<strong>XX-X</strong>');
    }
    else if((/RT\d\d\d-\d\d-\d\d-\d\d-\d\d/).test( input )){
      $('#buildPartStandardEditInvalid').html('Pravilen format je: RTXXX-XX-XX-XX-XX<strong>-XX-X</strong>');
    }
    else if((/RT\d\d\d-\d\d-\d\d-\d\d-\d/).test( input )){
      $('#buildPartStandardEditInvalid').html('Pravilen format je: RTXXX-XX-XX-XX-X<strong>X-XX-X</strong>');
    }
    else if((/RT\d\d\d-\d\d-\d\d-\d\d-/).test( input )){
      $('#buildPartStandardEditInvalid').html('Pravilen format je: RTXXX-XX-XX-XX-<strong>XX-XX-X</strong>');
    }
    else if((/RT\d\d\d-\d\d-\d\d-\d\d/).test( input )){
      $('#buildPartStandardEditInvalid').html('Pravilen format je: RTXXX-XX-XX-XX<strong>-XX-XX-X</strong>');
    }
    else if((/RT\d\d\d-\d\d-\d\d-\d/).test( input )){
      $('#buildPartStandardEditInvalid').html('Pravilen format je: RTXXX-XX-XX-X<strong>X-XX-XX-X</strong>');
    }
    else if((/RT\d\d\d-\d\d-\d\d-/).test( input )){
      $('#buildPartStandardEditInvalid').html('Pravilen format je: RTXXX-XX-XX-<strong>XX-XX-XX-X</strong>');
    }
    else if((/RT\d\d\d-\d\d-\d\d/).test( input )){
      $('#buildPartStandardEditInvalid').html('Pravilen format je: RTXXX-XX-XX<strong>-XX-XX-XX-X</strong>');
    }
    else if((/RT\d\d\d-\d\d-\d/).test( input )){
      $('#buildPartStandardEditInvalid').html('Pravilen format je: RTXXX-XX-X<strong>X-XX-XX-XX-X</strong>');
    }
    else if((/RT\d\d\d-\d\d-/).test( input )){
      $('#buildPartStandardEditInvalid').html('Pravilen format je: RTXXX-XX-<strong>XX-XX-XX-XX-X</strong>');
    }
    else if((/RT\d\d\d-\d\d/).test( input )){
      $('#buildPartStandardEditInvalid').html('Pravilen format je: RTXXX-XX<strong>-XX-XX-XX-XX-X</strong>');
    }
    else if((/RT\d\d\d-\d/).test( input )){
      $('#buildPartStandardEditInvalid').html('Pravilen format je: RTXXX-X<strong>X-XX-XX-XX-XX-X</strong>');
    }
    else if((/RT\d\d\d-/).test( input )){
      $('#buildPartStandardEditInvalid').html('Pravilen format je: RTXXX-<strong>XX-XX-XX-XX-XX-X</strong>');
    }
    else if((/RT\d\d\d/).test( input )){
      $('#buildPartStandardEditInvalid').html('Pravilen format je: RTXXX<strong>-XX-XX-XX-XX-XX-X</strong>');
    }
    else if((/RT\d\d/).test( input )){
      $('#buildPartStandardEditInvalid').html('Pravilen format je: RTXX<strong>X-XX-XX-XX-XX-XX-X</strong>');
    }
    else if((/RT\d/).test( input )){
      $('#buildPartStandardEditInvalid').html('Pravilen format je: RTX<strong>XX-XX-XX-XX-XX-XX-X</strong>');
    }
    else if((/RT/).test( input )){
      $('#buildPartStandardEditInvalid').html('Pravilen format je: RT<strong>XXX-XX-XX-XX-XX-XX-X</strong>');
    }
    else if((/R/).test( input )){
      $('#buildPartStandardEditInvalid').html('Pravilen format je: R<strong>TXXX-XX-XX-XX-XX-XX-X</strong>');
    }
    else
      $('#buildPartStandardEditInvalid').html('Pravilen format je: <strong>RTXXX-XX-XX-XX-XX-XX-X</strong>');
    $('#buildPartStandardEdit').removeClass('is-valid').addClass('is-invalid');
  }
}
function onEventCloseCollapseNewTaskBuildPart(event) {
  closeCollapseNewTaskBuildPart(event.data.id);
}
function closeCollapseNewTaskBuildPart(taskId){
  $('#collapseNewTaskBuildPart'+taskId).collapse("toggle");
  collapseNewTaskBuildPartsOpen = false;
  $('#btnAddBuildPart'+taskId).html('Dodaj kos');
}
//////////////////////////EDIT BUILD PARTS
function onEventOpenEditBuildPart(event) {
  openEditBuildPart(event.data.id);
}
//open modal edit build part
function openEditBuildPart(buildPartId){
  debugger;
  /*
  if (!allMaterials || !allStandards){
    $.get('/buildparts/standards', function(data){
      allStandards = data.standards;
      allMaterials = data.materials;
      $(".select2-standard-add").select2({
        data: allStandards,
        tags: permissionTag
      });
      $(".select2-material-add").select2({
        data: allMaterials,
        tags: permissionTag
      });
      fillEditBuildPartModal(buildPartId);
    })
  }
  else{
  }
  */
  fillEditBuildPartModal(buildPartId);
}
//fill edit build part modal
function fillEditBuildPartModal(buildPartId){
  var tmpBuildPart = allBuildParts.find(bp => bp.id == buildPartId); 
  if (!tmpBuildPart)
    tmpBuildPart = allInactiveBuildParts.find(ibp => ibp.id == buildPartId);
  isNaN(tmpBuildPart.quantity) ? $('#buildPartQuantityEdit').prop('type', 'text') : $('#buildPartQuantityEdit').prop('type', 'number');

  $('#buildPartNameEdit').val(tmpBuildPart.name);
  $('#buildPartStandardEdit').val(tmpBuildPart.standard);
  $('#buildPartPositionEdit').val(tmpBuildPart.position);
  $('#buildPartQuantityEdit').val(tmpBuildPart.quantity);
  $('#buildPartMaterialEdit').val(tmpBuildPart.material);
  $('#buildPartNoteEdit').val(tmpBuildPart.note);
  $('#buildPartTagEdit').val(tmpBuildPart.tag);
  // var button = `<button class="btn btn-success" id="btnEditBuildPart">Posodobi</button>`;
  // $('#editBuildPartButtonBody').empty();
  // $('#editBuildPartButtonBody').append(button);
  $('#btnEditBuildPart').off('click').on('click', {id: buildPartId}, onEventEditBuildPart);
  /*
  $( "#buildPartEditInput" ).on( "keydown", function(event) {
    if (event.which == 13) 
       editSubtask(subtaskId);
  });
  */
  $("#modalEditBuildPart").modal('toggle');
}
function onEventEditBuildPart(event) {
  editBuildPart(event.data.id);
}
//if all required val post edit build part
function editBuildPart(buildPartId){
  debugger;
  if ($('#buildPartNameEdit').val() != '' && $('#buildPartStandardEdit').val() != '' && $('#buildPartPositionEdit').val() != '' && $('#buildPartQuantityEdit').val() != ''){
    $('#buildPartNameEdit').removeClass('is-invalid');
    $('#buildPartStandardEdit').removeClass('is-invalid');
    $('#buildPartPositionedit').removeClass('is-invalid');
    $('#buildPartQuantityEdit').removeClass('is-invalid');
    $('#buildPartMaterialEdit').removeClass('is-invalid');
    
    //var taskId = activeTaskId;
    var name = $('#buildPartNameEdit').val();
    var standard = $('#buildPartStandardEdit').val();
    var position = $('#buildPartPositionEdit').val();
    var quantity = $('#buildPartQuantityEdit').val();
    var material = $('#buildPartMaterialEdit').val();
    var note = $('#buildPartNoteEdit').val();
    var tag = $('#buildPartTagEdit').val();
    //no empty inputs, edit build part
    debugger;
    $.post('/buildparts/edit', {buildPartId,name,standard,position,quantity,material,note,tag}, function(data){
      debugger;
      if (data.success){
        console.log('Successfully editing selected build part.')
        console.log(data);
        /*
        var tmpStandard, tmpMaterial;
        if (data.materialId){
          tmpMaterial = {id:data.materialId, text:material, description:null,material:false,supplier:false,screw_quality:false};
          allMaterials.push(tmpMaterial);
          $(".select2-material-add").select2({
            data: allMaterials,
            tags: permissionTag
          });
        }
        else{
          tmpMaterial = allMaterials.find(m => m.id == material);
        }
        if (data.standardId){
          tmpStandard = {id:data.standardId, text:standard};
          allStandards.push(tmpStandard);
          $(".select2-standard-add").select2({
            data: allStandards,
            tags: permissionTag
          });
        }
        else{
          tmpStandard = allStandards.find(s => s.id == standard);
        }
        */
        //FIX VALUES IN OBJECT ARRAY
        var tmpBuildPart = allBuildParts.find(bp => bp.id == buildPartId);
        if (!tmpBuildPart)
          tmpBuildPart = allInactiveBuildParts.find(ibp => ibp.id = buildPartId);
        var wasScrew = tmpBuildPart.mark_screw;
        tmpBuildPart.name = name;
        tmpBuildPart.position = position;
        tmpBuildPart.quantity = quantity;
        tmpBuildPart.note = note;
        tmpBuildPart.tag = tag;
        tmpBuildPart.material = material;
        //tmpBuildPart.mark = tmpMaterial.text;
        //tmpBuildPart.mark_id = tmpMaterial.id;
        //tmpBuildPart.mark_material = tmpMaterial.material;
        //tmpBuildPart.mark_supplier = tmpMaterial.supplier;
        //tmpBuildPart.mark_screw = tmpMaterial.screw_quality;
        tmpBuildPart.standard = standard;
        //tmpBuildPart.standard = tmpStandard.text;
        //tmpBuildPart.standard_id = tmpStandard.id
        /*
        if (wasScrew && !tmpBuildPart.mark_screw){
          $('#buildPartProgressControl65').css({'width':'80%'})
          var checkboxElement = `<input class="custom-control-input" id="customBuildPartCheck`+buildPartId+`" type="checkbox"  onchange="toggleBuildPartCheckbox(this)" />
          <label class="custom-control-label" for="customBuildPartCheck`+buildPartId+`"> </label>`;
          $('#buildPartCheckbox'+buildPartId).append(checkboxElement);
          $('#phasesBuildPart'+buildPartId).show();
          //tmpBuildPart.completion = 0;
        }
        else{
          $('#buildPartProgressControl65').css({'width':'0%'})
          $('#buildPartCheckbox'+buildPartId).empty();
          $('#phasesBuildPart'+buildPartId).show();
        }
        */
        //FIX VALUES ON PAGE
        $('#buildPartPosition'+buildPartId).html(tmpBuildPart.position);
        $('#buildPartName'+buildPartId).html(tmpBuildPart.name);
        $('#buildPartStandard'+buildPartId).html(tmpBuildPart.standard);
        $('#buildPartQuantity'+buildPartId).html(tmpBuildPart.quantity);
        $('#buildPartMaterial'+buildPartId).html(tmpBuildPart.material);
        $('#buildPartNote'+buildPartId).html(tmpBuildPart.note);
        $('#buildPartTag'+buildPartId).html(tmpBuildPart.tag);
        //fix completion of task
        var tmp = allBuildParts.filter(bp => bp.completion == 100);
        var all = allBuildParts.filter(bp => bp);
        //calc new completion of task
        var taskCompletion = Math.round((tmp.length/all.length)*100)
        if (taskCompletion == 0 || allBuildParts.length == 0)
          taskCompletion = 0;
        debugger;
        fixTaskCompletion(activeTaskId,taskCompletion);
        /*
        if (tmpBuildPart.mark_screw)
          $('#phasesBuildPart'+buildPartId).hide();
        else
          $('#phasesBuildPart'+buildPartId).show();
        */
        //close modal for editing selected build part
        $('#modalEditBuildPart').modal("toggle");
        //ADDING CHANGE TO DB
        addChangeSystem(2,20,projectId,activeTaskId,null,null,null,null,null,null,null,null,tmpBuildPart.id);
      }
      else{
        console.log('Unsuccessfully editing build part.')
        //console.log(data);
        $('#modalError').modal('toggle');
      }
    })
  }
  else{
    if ($('#buildPartNameEdit').val() == '')
    $('#buildPartNameEdit').addClass('is-invalid');
    if ($('#buildPartStandardEdit').val() == ''){
      $('#buildPartStandardEditInvalid').html('Vnesite št.risbe/standard!');
      $('#buildPartStandardEdit').addClass('is-invalid');
    }
    if ($('#buildPartPositionEdit').val() == '')
      $('#buildPartPositionEdit').addClass('is-invalid');
    if ($('#buildPartQuantityEdit').val() == '')
      $('#buildPartQuantityEdit').addClass('is-invalid');
    if ($('#buildPartMaterialEdit').val() == '')
      $('#buildPartMaterialEdit').addClass('is-invalid');
  }  
}
function toggleBuildPartCheckbox(element){
  var buildPartId = parseInt(this.id.match(/\d+/g)[0]);
  debugger;
  var completion;
  if (this.checked){
    completion = 100;
    //ADDING CHANGE TO DB
    addChangeSystem(4,20,projectId,activeTaskId,null,null,null,null,null,null,null,null,buildPartId);
    //ADDING NEW CHANGE TO DB
    //addNewProjectChange(2,4,projectId,taskId);
  }
  else{
    completion = 0;
    //ADDING CHANGE TO DB
    addChangeSystem(5,20,projectId,activeTaskId,null,null,null,null,null,null,null,null,buildPartId);
    //ADDING NEW CHANGE TO DB
    //addNewProjectChange(2,5,projectId,taskId);
  }
  fixBuildPartCompletion(buildPartId,completion);
  //fixTaskCompletion(taskId, completion);
  debugger;
}
//FIX build part percentage --> and then if complete then call fixTaskCompletion
function fixBuildPartCompletion(buildPartId, completion){
  debugger
  $.post('/buildparts/completion', {buildPartId, completion}, function(resp){
    //fix subtask count
    debugger;
    if (resp.success){
      var buildPart, oldCompletion;
      if (allBuildParts){
        buildPart = allBuildParts.find(t=>t.id == buildPartId);
        oldCompletion = buildPart.completion;
        buildPart.completion = completion;
      }
      //fix task display
      //$('#buildPartProgress'+buildPartId).width(completion+"%");
      $('#buildPartProgress'+buildPartId).removeClass();
      $('#buildPartProgress'+buildPartId).addClass('progress-bar w-'+completion);
      
      //fix task completion --> build part is finished and changes task's overall completion
      if (completion == 100 || (oldCompletion == 100 && completion < 100)){
        //TODO count all completed build parts for completion of active task
        var tmp = allBuildParts.filter(bp => bp.completion == 100 && bp.active == true);
        var all = allBuildParts.filter(bp => bp.active == true);
        //calc new completion of task
        var taskCompletion = Math.round((tmp.length/all.length)*100)
        if (taskCompletion == 0 || allBuildParts.length == 0)
          taskCompletion = 0;
        debugger;
        fixTaskCompletion(activeTaskId,taskCompletion);
      }
    }
    else{

    }
  })
}
 //help functions and control on input fn
function formatDate(date){
  var d = new Date(date);
  const offset = d.getTimezoneOffset()
  d = new Date(d.getTime() + (offset*60*1000));
  return d.toISOString().split('T')[0]
}
//////////////////////////////SORT FUNCTIONS
function onChangeSortBuildParts(event) {
  debugger
  let sort = $('#buildPartSortSelect').val();
  let order = $('#buildPartOrderSelect').val()
  localStorage.setItem('buildPartsSort', sort);
  localStorage.setItem('buildPartsOrder', order);
  allBuildParts.sort(compareValues(sort, order));
  redrawBuildParts();
}
function redrawBuildParts() {
  $('#buildPartsList'+activeTaskId).empty();
  //append build parts in frame list
  for(var i = 0; i < allBuildParts.length; i++){
    //variables
    let position = allBuildParts[i].position ? allBuildParts[i].position : '',
    name = allBuildParts[i].name ? allBuildParts[i].name : '',
    quantity = allBuildParts[i].quantity ? allBuildParts[i].quantity : '',
    material = allBuildParts[i].material ? allBuildParts[i].material : '',
    standard = allBuildParts[i].standard ? allBuildParts[i].standard : '',
    note = allBuildParts[i].note ? allBuildParts[i].note : '',
    tag = allBuildParts[i].tag ? allBuildParts[i].tag : '';
    //control, activity, buttons
    var checkedCB = '';
    var checkboxElement = '';
    if (!allBuildParts[i].build_phases_count){
      if (allBuildParts[i].completion == 100)
        checkedCB = 'checked=""';
      checkboxElement = `<input class="custom-control-input" id="customBuildPartCheck`+allBuildParts[i].id+`" type="checkbox" `+checkedCB+` />
        <label class="custom-control-label" for="customBuildPartCheck`+allBuildParts[i].id+`"> </label>`;
    }
    var deleteElement = '';
    var editElement = '';
    var phaseElement = '';
    var deleteActivity = '';
    var deleteIcon = 'trash';
    var deleteTaskClass = '';
    if (allBuildParts[i].active == false){
      deleteActivity = '1';
      deleteIcon = 'trash-restore';
      deleteTaskClass = 'bg-secondary';
    }
    //make delete and edit button
    if (loggedUser.role.substring(0,5) == 'vodja' || loggedUser.role == 'admin' || leader || loggedUser.role == 'konstrukter' || loggedUser.role == 'konstruktor' || (loggedUser.projectRole && loggedUser.projectRole == 'konstrukter')){
      deleteElement = `<button class="btn btn-danger task-button mr-2 mt-1" id="deleteBuildPart`+allBuildParts[i].id+`">
        <i class="fas fa-`+deleteIcon+` fa-lg" data-toggle="tooltip" data-original-title="Odstrani"></i>
      </button>`;
      editElement = `<button class="btn btn-primary task-button mr-2 mt-1" id="editBuildPart`+allBuildParts[i].id+`">
        <i class="fas fa-pen fa-lg" data-toggle="tooltip" data-original-title="Uredi"></i>
      </button>`;
    }
    //make phases button
    if (allBuildParts[i].build_phases_count && allBuildParts[i].build_phases_count > 0){
      phaseElement = `<button class="btn btn-success task-button mr-2 mt-1" id="phasesBuildPart`+allBuildParts[i].id+`">
        <i class="fas fa-angle-down fa-lg text-dark" data-toggle="tooltip" data-original-title="Faze"></i>
      </button>`;
    }
    else{
      phaseElement = `<button class="btn btn-success task-button mr-2 mt-1" id="phasesBuildPart`+allBuildParts[i].id+`">
        <i class="fas fa-angle-down fa-lg" data-toggle="tooltip" data-original-title="Faze"></i>
      </button>`;
    }
    var progressBar = `<div class="progress ml-2 w-80" id="buildPartProgressControl`+allBuildParts[i].id+`"><div class="progress-bar w-`+allBuildParts[i].completion+`" id="buildPartProgress`+allBuildParts[i].id+`"></div></div>`;
    var element = `<div class="list-group-item `+deleteTaskClass+`" id="buildPart`+allBuildParts[i].id+`">
      <div class="row">
        <div class="col-md-1">
          <div id="buildPartPosition`+allBuildParts[i].id+`">`+position+`</div>
        </div>
        <div class="col-md-4">
          <div class="ml-2" id="buildPartName`+allBuildParts[i].id+`">`+name+`</div>
        </div>
        <div class="col-md-3">
          <div class="ml-2" id="buildPartStandard`+allBuildParts[i].id+`">`+standard+`</div>
        </div>
        <div class="col-md-4 d-flex">
          <div class="ml-auto" id="buildPartQuantity`+allBuildParts[i].id+`">`+quantity+`</div>
          `+progressBar+`
        </div>
      </div>
      <div class="row">
        <div class="col-md-3 d-flex">
          <div class="mt-3" id="buildPartMaterial`+allBuildParts[i].id+`">`+material+`</div>
        </div>
        <div class="col-md-3">
          <div class="ml-2 mt-3" id="buildPartNote`+allBuildParts[i].id+`">`+note+`</div>
        </div>
        <div class="col-md-2">
          <div class="ml-2 mt-3" id="buildPartTag`+allBuildParts[i].id+`">`+tag+`</div>
        </div>
        <div class="col-md-4 d-flex">
          <div class="ml-auto d-flex" id="buildPartControl`+allBuildParts[i].id+`">
            <div class="custom-control custom-checkbox ml-auto mt-2" id="buildPartCheckbox`+allBuildParts[i].id+`">
              `+checkboxElement+`
            </div>
            `+deleteElement+`
            `+phaseElement+`
            `+editElement+`
          </div>
        </div>
      </div>
      <div class="collapse multi-collapse task-build-phase" id="collapseBuildPhases`+allBuildParts[i].id+`"></div>
    </div>`;
    if (allBuildParts[i].active == false){
      if (loggedUser.role == 'admin'){
        $('#buildPartsList'+activeTaskId).append(element);
        $('#customBuildPartCheck'+allBuildParts[i].id).on('change', toggleBuildPartCheckbox);
        $('#deleteBuildPart'+allBuildParts[i].id).on('click', {id: allBuildParts[i].id, activity: deleteActivity}, onEventDeleteBuildPart);
        $('#editBuildPart'+allBuildParts[i].id).on('click', {id: allBuildParts[i].id}, onEventOpenEditBuildPart);
        $('#phasesBuildPart'+allBuildParts[i].id).on('click', {id: allBuildParts[i].id}, onEventOpenBuildPhasesCollapse);
        //if (allBuildParts[i].mark_screw)
        //  $('#phasesBuildPart'+allBuildParts[i].id).hide();
      }
      // var id = allBuildParts[i].id;
      // allInactiveBuildParts.push(allBuildParts[i]);
      // allBuildParts = allBuildParts.filter(s => s.id != id);
      // i--;
    }
    else{
      $('#buildPartsList'+activeTaskId).append(element);
      $('#customBuildPartCheck'+allBuildParts[i].id).on('change', toggleBuildPartCheckbox);
      $('#deleteBuildPart'+allBuildParts[i].id).on('click', {id: allBuildParts[i].id, activity: deleteActivity}, onEventDeleteBuildPart);
      $('#editBuildPart'+allBuildParts[i].id).on('click', {id: allBuildParts[i].id}, onEventOpenEditBuildPart);
      $('#phasesBuildPart'+allBuildParts[i].id).on('click', {id: allBuildParts[i].id}, onEventOpenBuildPhasesCollapse);
      //if (allBuildParts[i].mark_screw)
      //  $('#phasesBuildPart'+allBuildParts[i].id).hide();
    }
  }
}
/////////////////////////PARSE CSV
function creteBuildPartsFromCsv(allGivenResults){
  debugger;
  buildPartsPreviewArray = [];
  //var newBuildParts = [];
  for (let j = 0; j < allGivenResults.length; j++) {
    const results = allGivenResults[j];
    
    if (results.errors.length > 0){
      alert('Pri branju datoteke je prišlo do napake.');
    }
    else{
      var data = results.data;
      if (data[0][0].toLocaleLowerCase() == 'poz.' && data[0][1].toLocaleLowerCase() == 'naziv' && data[0][2].toLocaleLowerCase() == 'kol.' && data[0][3].toLocaleLowerCase() == 'material' &&
      data[0][4].toLocaleLowerCase() == 'št.risbe / standard' && data[0][5].toLocaleLowerCase() == 'opomba'){
        //kosovnica je postavljena v pravilnem vrstem redu (tega verjetno nikoli ne bo)
        data.shift();
        debugger;
      }
      else if ((data[data.length-1][0].toLocaleLowerCase() == 'poz.' && data[data.length-1][1].toLocaleLowerCase() == 'naziv' && data[data.length-1][2].toLocaleLowerCase() == 'kol.' &&
      data[data.length-1][3].toLocaleLowerCase() == 'material' && data[data.length-1][4].toLocaleLowerCase() == 'št.risbe / standard' && data[data.length-1][5].toLocaleLowerCase() == 'opomba') 
      || 
      (data[data.length-2][0].toLocaleLowerCase() == 'poz.' && data[data.length-2][1].toLocaleLowerCase() == 'naziv' && data[data.length-2][2].toLocaleLowerCase() == 'kol.' &&
      data[data.length-2][3].toLocaleLowerCase() == 'material' && data[data.length-2][4].toLocaleLowerCase() == 'št.risbe / standard' && data[data.length-2][5].toLocaleLowerCase() == 'opomba')){
        //kosovnica ja taksa kot je v nacrtu, postavljena na glavo, taksna bo skoraj vedno
        data.pop();
        if (data[data.length-1][0].toLocaleLowerCase() == 'poz.')
          data.pop();
        debugger;
        
        var newBuildParts = [];
        //let currentFileBuildParts = [];
        
        // var screwCheck = $('#checkboxScrewsAdd').prop('checked'), supplierCheck = $('#checkboxSuppliersAdd').prop('checked'), materialCheck = $('#checkboxMaterialsAdd').prop('checked');
        let screwCheck = false, supplierCheck = false, materialCheck = true;
        //(/RT\d\d\d-\d\d-\d\d-\d\d-\d\d-\d\d-\d/).test( 'RT049-01-00-00-00-00-0' )
        data.forEach(d => {if ((/RT\d\d\d-\d\d-\d\d-\d\d-\d\d-\d\d-\d/).test( d[4] ) || (/RT\d\d\d\d-\d\d-\d\d-\d\d-\d\d-\d\d-\d/).test( d[4] ) || (/RT\d\d\d-\d\d-\d\d-\d\d-\d\d-\d\d/).test( d[4] ) || (/RT\d\d\d\d-\d\d-\d\d-\d\d-\d\d-\d\d/).test( d[4] ) || ($('#checkboxFilterSKAdd').prop('checked') && d[4].startsWith('SK')) || (/RT\d\d\d \d\d \d\d \d\d \d\d \d\d \d/).test( d[4] ) || (/RT\d\d\d \d\d \d\d \d\d \d\d \d\d/).test( d[4] )) d.selected = true; else d.selected = false})
        // if (materialCheck && !supplierCheck && !screwCheck){ data.forEach(d => { if (d[4].startsWith('RT')) d.selected = true; else d.selected = false; }) }
        // else if (materialCheck && supplierCheck && !screwCheck){ data.forEach(d => { if (!(d[4].includes('DIN') || d[4].includes('ISO'))) d.selected = true; else d.selected = false; }) }
        // else if (materialCheck && !supplierCheck && screwCheck){ data.forEach(d => { if (d[4].startsWith('RT') || d[4].includes('DIN') || d.includes('ISO')) d.selected = true; else d.selected = false; }) }
        // else if (!materialCheck && supplierCheck && screwCheck){ data.forEach(d => { if (!d[4].startsWith('RT')) d.selected = true; else d.selected = false; }) }
        // else if (!materialCheck && supplierCheck && !screwCheck){ data.forEach(d => { if (!(d[4].startsWith('RT') || d[4].includes('DIN') || d[4].includes('ISO'))) d.selected = true; else d.selected = false; }) }
        // else if (!materialCheck && !supplierCheck && screwCheck){ data.forEach(d => { if (d[4].includes('DIN') || d[4].includes('ISO')) d.selected = true; else d.selected = false; }) }
        // else if (!materialCheck && !supplierCheck && !screwCheck){ data = []; }//same as no build parts
        // else {
        //   //no filter, preview all build parts
        //   { data.forEach(d => { d.selected = true;}) }
        // }
        //add every SK build part - stands for standard build part and can be on many projects multiple times even on same project
        // if ($('#checkboxFilterSKAdd').prop('checked')){
        //   data.forEach(d => {if (d[4].startsWith('SK')) d.selected = true});
        // }
        //for()
        for(var i = 0 ; i < data.length; i++){
          if (data[i].length > 6)
            newBuildParts.unshift({position:data[i][0], name: data[i][1], quantity: data[i][2], material: data[i][3], standard: data[i][4], note: data[i][5], tag: data[i][6], selected: data[i].selected});
          else
            newBuildParts.unshift({position:data[i][0], name: data[i][1], quantity: data[i][2], material: data[i][3], standard: data[i][4], note: data[i][5], tag: '', selected: data[i].selected});
        }
        debugger;
        //var postData = {newBuildParts:newBuildParts, newStandards:newStandards, newMaterials:newMaterials, newStandardsIndexs:newStandardsIndexs, newMaterialsIndexs:newMaterialsIndexs, taskId:activeTaskId};
        buildPartsPreview = {newBuildParts:newBuildParts, taskId:activeTaskId, buildParts:[], fileName: results.fileName};//null for taskId means its for new task
        buildPartsPreviewArray.push({newBuildParts:newBuildParts, taskId:activeTaskId, buildParts:[], fileName: results.fileName});
        // //check if filter for existing build parts is on then get data and filter them out
        // if($('#checkboxFilterBPAdd').prop('checked')){
        //   filterExistingBuildParts();
        // }
        // else{
        //   //make modal of build parts and open it
        //   buildBPPreview();
        // }
      }
      else{
        //kosovnica nima naslovne vrstice, najverjetne gre za neko drugo vrsto csv datoteke --> napaka
        debugger;
        alert('Datoteka csv ni v pravilni obliki kosovnice.');
      }
    }
  }
  //check if filter for existing build parts is on then get data and filter them out
  if($('#checkboxFilterBPAdd').prop('checked')){
    filterExistingBuildParts();
  }
  else{
    //make modal of build parts and open it
    //buildBPPreview();
    bpIndex = 0;
    buildBPArrayPreview();
  }
}
function addNewBuildPartsFromCSV(){
  debugger;
  // let parentName = $('#buildPartNameAdd').val();
  // let parentStandard = $('#buildPartStandardAdd').val();
  // let parentName = '-';
  // let parentStandard = buildPartsPreview.fileName;
  let parentName = buildPartsPreview.parentName;
  let parentStandard = buildPartsPreview.parentStandard;
  var tmp = {taskId:buildPartsPreview.taskId, buildParts:buildPartsPreview.buildParts, parentName, parentStandard};
  $.ajax({
    url: '/buildparts/csv',
    type: 'POST',
    data: JSON.stringify(tmp),
    contentType: 'application/json',
    dataType: 'json',
    success: function(res) {
      if (res.success){
        $('#buildPartNameAdd').removeClass('is-invalid');
        $('#buildPartStandardAdd').removeClass('is-invalid');
        $('#buildPartNameAdd').val('');
        $('#buildPartStandardAdd').val('');
        $('#buildPartSestavAdd').prop('checked', false);
        $('#inputBuildPartCsvLabel').html('');
        $('#inputBuildPartCsv').val('');
        var taskId = activeTaskId;
        var buildParts = res.buildParts;
        console.log('Successfully creating new build parts from csv.');
        
        //empty build parts because i return all build parts, new and existing
        $('#buildPartsList'+taskId).empty();
        //append new build parts
        for(var i = 0; i < buildParts.length; i++){
          //variables
          var position = '', name = '', quantity = '', material = '', standard = '', note = '', tag = '';
          if (buildParts[i].position)
            position = buildParts[i].position;
          if (buildParts[i].name)
            name = buildParts[i].name;
          if (buildParts[i].quantity)
            quantity = buildParts[i].quantity;
          if (buildParts[i].material)
            material = buildParts[i].material;
          if (buildParts[i].standard)
            standard = buildParts[i].standard;
          if (buildParts[i].note)
            note = buildParts[i].note;
          if (buildParts[i].tag)
            tag = buildParts[i].tag;
          //control, activity, buttons
          var checkedCB = '';
          var checkboxElement = '';
          if (!buildParts[i].build_phases_count){
            if (buildParts[i].completion == 100)
              checkedCB = 'checked=""';
            checkboxElement = `<input class="custom-control-input" id="customBuildPartCheck`+buildParts[i].id+`" type="checkbox" `+checkedCB+` />
              <label class="custom-control-label" for="customBuildPartCheck`+buildParts[i].id+`"> </label>`;
          }
          var deleteElement = '';
          var editElement = '';
          var phaseElement = '';
          var deleteActivity = '';
          var deleteIcon = 'trash';
          var deleteTaskClass = '';
          if (buildParts[i].active == false){
            deleteActivity = '1';
            deleteIcon = 'trash-restore';
            deleteTaskClass = 'bg-secondary';
          }
          //make delete and edit button
          if (loggedUser.role.substring(0,5) == 'vodja' || loggedUser.role == 'admin' || leader || loggedUser.role == 'konstrukter' || loggedUser.role == 'konstruktor' || (loggedUser.projectRole && loggedUser.projectRole == 'konstrukter')){
            deleteElement = `<button class="btn btn-danger task-button mr-2 mt-1" id="deleteBuildPart`+buildParts[i].id+`">
              <i class="fas fa-`+deleteIcon+` fa-lg" data-toggle="tooltip" data-original-title="Odstrani"></i>
            </button>`;
            editElement = `<button class="btn btn-primary task-button mr-2 mt-1" id="editBuildPart`+buildParts[i].id+`">
              <i class="fas fa-pen fa-lg" data-toggle="tooltip" data-original-title="Uredi"></i>
            </button>`;
          }
          //make phases button
          if (buildParts[i].build_phases_count && buildParts[i].build_phases_count > 0){
            phaseElement = `<button class="btn btn-success task-button mr-2 mt-1" id="phasesBuildPart`+buildParts[i].id+`">
              <i class="fas fa-angle-down fa-lg text-dark" data-toggle="tooltip" data-original-title="Faze"></i>
            </button>`;
          }
          else{
            phaseElement = `<button class="btn btn-success task-button mr-2 mt-1" id="phasesBuildPart`+buildParts[i].id+`">
              <i class="fas fa-angle-down fa-lg" data-toggle="tooltip" data-original-title="Faze"></i>
            </button>`;
          }
          //if (buildParts[i].mark_material){
          //}
          var progressBar = `<div class="progress ml-2 w-80" id="buildPartProgressControl`+buildParts[i].id+`"><div class="progress-bar w-`+buildParts[i].completion+`" id="buildPartProgress`+buildParts[i].id+`"></div></div>`;
          var element = `<div class="list-group-item `+deleteTaskClass+`" id="buildPart`+buildParts[i].id+`">
            <div class="row">
              <div class="col-md-1">
                <div id="buildPartPosition`+buildParts[i].id+`">`+position+`</div>
              </div>
              <div class="col-md-4">
                <div class="ml-2" id="buildPartName`+buildParts[i].id+`">`+name+`</div>
              </div>
              <div class="col-md-3">
                <div class="ml-2" id="buildPartStandard`+buildParts[i].id+`">`+standard+`</div>
              </div>
              <div class="col-md-4 d-flex">
                <div class="ml-auto" id="buildPartQuantity`+buildParts[i].id+`">`+quantity+`</div>
                `+progressBar+`
              </div>
            </div>
            <div class="row">
              <div class="col-md-3 d-flex">
                <div class="mt-3" id="buildPartMaterial`+buildParts[i].id+`">`+material+`</div>
              </div>
              <div class="col-md-3">
                <div class="ml-2 mt-3" id="buildPartNote`+buildParts[i].id+`">`+note+`</div>
              </div>
              <div class="col-md-2">
                <div class="ml-2 mt-3" id="buildPartTag`+buildParts[i].id+`">`+tag+`</div>
              </div>
              <div class="col-md-4 d-flex">
                <div class="ml-auto d-flex" id="buildPartControl`+buildParts[i].id+`">
                  <div class="custom-control custom-checkbox ml-auto mt-2" id="buildPartCheckbox`+buildParts[i].id+`">
                    `+checkboxElement+`
                  </div>
                  `+deleteElement+`
                  `+phaseElement+`
                  `+editElement+`
                </div>
              </div>
            </div>
            <div class="collapse multi-collapse task-build-phase" id="collapseBuildPhases`+buildParts[i].id+`"></div>
          </div>`;
          if (buildParts[i].active == false){
            if (loggedUser.role == 'admin'){
              $('#buildPartsList'+taskId).append(element);
              $('#customBuildPartCheck'+buildParts[i].id).on('change', toggleBuildPartCheckbox);
              $('#deleteBuildPart'+buildParts[i].id).on('click', {id: buildParts[i].id, activity: deleteActivity}, onEventDeleteBuildPart);
              $('#editBuildPart'+buildParts[i].id).on('click', {id: buildParts[i].id}, onEventOpenEditBuildPart);
              $('#phasesBuildPart'+buildParts[i].id).on('click', {id: buildParts[i].id}, onEventOpenBuildPhasesCollapse);
            }
            var id = buildParts[i].id;
            allInactiveBuildParts.push(buildParts[i]);
            buildParts = buildParts.filter(s => s.id != id);
            i--;
          }
          else{
            $('#buildPartsList'+taskId).append(element);
            $('#customBuildPartCheck'+buildParts[i].id).on('change', toggleBuildPartCheckbox);
            $('#deleteBuildPart'+buildParts[i].id).on('click', {id: buildParts[i].id, activity: deleteActivity}, onEventDeleteBuildPart);
            $('#editBuildPart'+buildParts[i].id).on('click', {id: buildParts[i].id}, onEventOpenEditBuildPart);
            $('#phasesBuildPart'+buildParts[i].id).on('click', {id: buildParts[i].id}, onEventOpenBuildPhasesCollapse);
          }
        }
        allBuildParts = buildParts;
        if (allTasks){
          var tmpTask = allTasks.find(t => t.id == taskId);
          tmpTask.build_parts_count = allBuildParts.length;
        }
        //close collapse for adding new build part
        if($('#collapseNewTaskBuildPart'+taskId).hasClass("show")) $('#collapseNewTaskBuildPart'+taskId).collapse("toggle");
        $('[data-toggle="tooltip"]').tooltip();
        collapseNewTaskBuildPartsOpen = false;
        //FIX TASK COMPLETION
        var tmp = allBuildParts.filter(bp => bp.completion == 100 && bp.mark_screw == false);
        var all = allBuildParts.filter(bp => bp.mark_screw == false);
        //calc new completion of task
        var taskCompletion = Math.round((tmp.length/all.length)*100)
        if (taskCompletion == 0 || allBuildParts.length == 0)
          taskCompletion = 0;
        debugger;
        fixTaskCompletion(activeTaskId,taskCompletion);
        //change button to black cuz now task has subtasks
        //$('#taskButtonBuildParts'+activeTaskId).find('img').attr('src', '/buildBlack.png');
        $('#taskButtonBuildParts'+activeTaskId).find('i').css('color','#000000');
        //remove checkbox on task
        $('#taskCheckbox'+activeTaskId).empty();
        buildPartsPreview.buildParts = [];
        buildPartsPreview.taskId = null;
        $('#btnAddBuildPart'+taskId).html('Dodaj kos');
        // //add animation to top of the list of build part
        // setTimeout(()=>{
        //   smoothScroll('#buildPartsList'+activeTaskId, 100);
        // },500)
        $('#newTaskBPFileNames').empty();
        $('#newBPFileNames').empty();
        if(++bpIndex != buildPartsPreviewArray.length){
          buildBPArrayPreview();
        }
      }
      else{
        console.log('Unsuccessfully creating new build parts from csv.');
        console.log(res.error);
      }
    }
  })
}
function onChangeCSVInput(){
  debugger;
  var fileInput = $('#inputBuildPartCsv').val().split("\\");
  fileInput = fileInput[fileInput.length-1];
  var fileExtention = fileInput.split('.');
  fileExtention = fileExtention[fileExtention.length-1];
  let fileName = fileInput.split('.')[0];
  let tmpLabel = 'Št. datotek: ' + $('#inputBuildPartCsv').prop('files').length + ' (';
  if ($('#inputBuildPartCsv').prop('files').length != 0){
    for (let i = 0; i < $('#inputBuildPartCsv').prop('files').length; i++) {
      const element = $('#inputBuildPartCsv').prop('files')[i];
      if (i != 0)
        tmpLabel += ', ';
      tmpLabel += element.name;
    }
    tmpLabel += ')';
  }
  else {
    tmpLabel = '';
  }
  $('#inputBuildPartCsvLabel').html(tmpLabel);
  $('#buildPartStandardAdd').val(fileName);
  $('#buildPartSestavAdd').prop('checked', true);
  preparseCsvToBuildParts();
  debugger;
}
//make input values for every file for lead build part and its standard name
function createInputFileNames() {
  $('#newTaskBPFileNames').empty();
  $('#newBPFileNames').empty();
  $('#newBuildPartCsvLabel').html('');
  $('#newBuildPartCsv').val('');
  if ($('#inputBuildPartCsv').prop('files').length != 0){
    for (let i = 0; i < $('#inputBuildPartCsv').prop('files').length; i++) {
      const element = $('#inputBuildPartCsv').prop('files')[i];
      let buildpartName = '-';
      //try to find the name of the file build part
      for (let j = 0; j < allResults.length; j++) {
        const result = allResults[j];
        let tmp = allResults[0].data.find(bp => bp[4] == element.name.split('.')[0])
        if(tmp){
          buildpartName = tmp[1];
          break;
        }
      }
      // tmpLabel += element.name;
      let divElement = `<div class="form-row">
      <div class="col-md-6">
        <div class="form-group">
          <label class="w-100" for="buildPartParentAdd` + i + `">Ime ` + (i+1) + `. sestava
            <input class="form-control" id="buildPartParentAdd` + i + `" type="text" name="buildPartParentAdd` + i + `" required="" placeholder="Ime sestava" value='` + buildpartName + `' />
          </label>
        </div>
      </div>
      <div class="col-md-6">
        <div class="form-group">
          <label class="w-100" for="buildPartParentNumberAdd` + i + `">Št. risbe ` + (i+1) + `. sestava
            <input class="form-control" id="buildPartParentNumberAdd` + i + `" type="text" name="buildPartParentNumberAdd` + i + `" required="" placeholder="Št. risbe sestava" value='` + element.name.split('.')[0] + `' />
          </label>
        </div>
      </div>
    </div>`;
    $('#newBPFileNames').append(divElement);
    }
    
  }
}
function preparseCsvToBuildParts() {
  var fileInput = $('#inputBuildPartCsv').val().split("\\");
  fileInput = fileInput[fileInput.length-1];
  var fileExtention = fileInput.split('.');
  fileExtention = fileExtention[fileExtention.length-1];

  if (fileExtention != 'csv'){
    alert('Datoteka, ki jo zelite sestaviti v kosovnico ni CSV datoteka.');
  }
  else{
    $('#buildPartNameAdd').removeClass('is-invalid');
    $('#buildPartStandardAdd').removeClass('is-invalid');
    allResults = [];
    var config = buildConfig();
    $('#inputBuildPartCsv').parse({
      config: config,
      before: function(file, inputElem)
      {
        start = now();
        console.log("Parsing file...", file);
        currentFileName = file.name.split('.')[0];
      },
      error: function(err, file)
      {
        console.log("ERROR:", err, file);
        firstError = firstError || err;
        errorCount++;
      },
      complete: function()
      {
        end = now();
        debugger;
        //creteBuildPartsFromCsv(allResults);
        createInputFileNames();
        printStats("Done with all files");
      }
    });
  }
}
function parseCsvToBuildParts(){
  var fileInput = $('#inputBuildPartCsv').val().split("\\");
  fileInput = fileInput[fileInput.length-1];
  var fileExtention = fileInput.split('.');
  fileExtention = fileExtention[fileExtention.length-1];

  if (fileExtention != 'csv'){
    alert('Datoteka, ki jo zelite sestaviti v kosovnico ni CSV datoteka.');
  }
  else{
    if($('#buildPartNameAdd').val() == '' || $('#buildPartStandardAdd').val() == ''){
      $('#buildPartNameAdd').addClass('is-invalid');
      $('#buildPartStandardAdd').addClass('is-invalid');
      $('#buildPartMaterialAdd').removeClass('is-invalid');
    }
    else{
      $('#buildPartNameAdd').removeClass('is-invalid');
      $('#buildPartStandardAdd').removeClass('is-invalid');
      allResults = [];
      var config = buildConfig();
      $('#inputBuildPartCsv').parse({
        config: config,
        before: function(file, inputElem)
        {
          start = now();
          console.log("Parsing file...", file);
          currentFileName = file.name.split('.')[0];
        },
        error: function(err, file)
        {
          console.log("ERROR:", err, file);
          firstError = firstError || err;
          errorCount++;
        },
        complete: function()
        {
          end = now();
          debugger;
          creteBuildPartsFromCsv(allResults);
          printStats("Done with all files");
        }
      });
    }
  }
}
function buildConfig(){
  return {
    header: false,
    dynamicTyping: false,
    skipEmptyLines: false,
    preview: 0,
    step: false,
    encoding: 'Windows-1250',
    worker: false,
    comments: 'default',
    complete: completeFn,
    error: errorFn,
    download: inputType == "remote",
  };
}
function bfrFrstChnk(chunk){
  var rows = chunk.split( /\r\n|\r|\n/ );
  var headings = rows[0].toUpperCase();
  rows[0] = headings;
  return rows.join("\r\n");
}
function printStats(msg){
  if (msg)
    console.log(msg);
  console.log("       Time:", (end-start || "(Unknown; your browser does not support the Performance API)"), "ms");
  console.log("  Row count:", rowCount);
  if (stepped)
    console.log("    Stepped:", stepped);
  console.log("     Errors:", errorCount);
  if (errorCount)
    console.log("First error:", firstError);
}
function stepFn(results, parser){
  stepped++;
  if (results)
  {
    if (results.data)
      rowCount += results.data.length;
    if (results.errors)
    {
      errorCount += results.errors.length;
      firstError = firstError || results.errors[0];
    }
  }
}
function completeFn(results){
  end = now();

  if (results && results.errors)
  {
    if (results.errors)
    {
      errorCount = results.errors.length;
      firstError = results.errors[0];
    }
    if (results.data && results.data.length > 0)
      rowCount = results.data.length;
  }

  results.fileName = currentFileName;

  printStats("Parse complete");
  console.log("    Results:", results);
  //allParseResults.push(results);
  debugger;
  allResults.push(results);
  debugger;
  //creteBuildPartsFromCsv(results);

  // icky hack
  setTimeout(enableButton, 100);
}
function errorFn(err, file){
  end = now();
  console.log("ERROR:", err, file);
  enableButton();
}
function now(){
  return typeof window.performance !== 'undefined' ? window.performance.now() : 0;
}
function enableButton(){
	$('#submit').prop('disabled', false);
}
var inputType = "string";
var stepped = 0, rowCount = 0, errorCount = 0, firstError;
var start, end;
var firstRun = true;
var maxUnparseLength = 10000;
var collapseNewTaskAbsenceOpen = false;
var collapseNewTaskAbsenceContent = false;
var collapseNewTaskBuildPartsOpen = false;
var collapseNewTaskBuildPartsContent = false;
var permissionTag = false;
var allBuildParts;
var allInactiveBuildParts;
//var allStandards;
//var allMaterials;
var allBuildPhases;
var allInactiveBuildPhases;
var buildPhasesCollapseOpen = false;
var collapseBuildPhaseContent = false;
var activeBuildPartId;
var allLocations;
var allPhaseTypes;
var allPhaseWorkers;
var allPhaseSuppliers;
var collapseNewBuildPhaseContent = false;
var collapseNewBuildPhaseOpen = false;
var firstBuildPhaseEdit = true;
//for delete prompt
var savedBuildPartId, savedBuildPartActivity, savedBuildPartActive;
var savedBuildPhaseId, savedBuildPhaseActivity, savedBuildPhaseActive;
let allParseResults = [];
let allResults = [];
//for multiple files preview
let buildPartsPreviewArray = [];
let bpIndex = 0;
let currentFileName;
//let currentFileNameArray = [];