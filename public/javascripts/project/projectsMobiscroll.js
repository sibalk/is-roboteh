$(document).ready(function() {
  // theme for calander
  mobiscroll.setOptions({
    theme: 'material',
    themeVariant: 'light'
  });
  //on click calls for buttons etc
  $('#btnTimelineMobiscroll').click(drawFirstTimelineMobiscroll);
  $('#filterResourcesBtn').on('click', () => {
    filterResources(true, false);
  });
  // on click .filter-category-parent change their children
  $('.filter-category-parent').on('change', function() {
    let checked = $(this).prop('checked');
    $('.filter-category-child').prop('checked', checked);
  });
  // on click .filter-category-child change their parent if all are checked
  $('.filter-category-child').on('change', function() {
    let checked = $('.filter-category-child').length === $('.filter-category-child:checked').length;
    $('.filter-category-parent').prop('checked', checked);
  });
});
// get projects from api
async function getProjects(completed, start, finish) {
  try {
    const params = new URLSearchParams({ active: 1, completed});
    if (start !== undefined) params.append(start);
    if (finish !== undefined) params.append(finish);

    const response = await fetch(`/apiv2/projects?${params}`, {
      method: 'GET',
      headers: {
        'Content-Type': 'application/json'
      },
    });
    const data = await response.json();
    return data.projects;
  }
  catch (error) {
    console.error('Error:', error);
  }
}
// get tasks from api
async function getTasks(start, finish, completion) {
  try {
    const params = new URLSearchParams({ start, end: finish });
    if (completion !== undefined) params.append('completion', completion);
    if (projectId !== undefined) params.append('projectId', projectId);
    const response = await fetch(`/apiv2/tasks?${params}`, {
      method: 'GET',
      headers: {
        'Content-Type': 'application/json'
      },
    });
    const data = await response.json();
    return data.tasks;
  }
  catch (error) {
    console.error('Error:', error);
  }
}
// call getProjectsMobiscroll in function for first draw of timeline mobiscroll
async function drawFirstTimelineMobiscroll() {
  if (!$('#projectsMobiscroll').html()) {
    projectsMobiscroll = await getProjects(projectsFilterCompletion);
    // filter projects to active and with start and finish
    projectsMobiscroll = projectsMobiscroll.filter(p => p.active && p.start && p.finish);
    // filter projects to RT, STR and sorted them by their number
    let projectsRT = projectsMobiscroll.filter(p => p.project_number.substring(0,2) === 'RT');
    let projectsSTR = projectsMobiscroll.filter(p => p.project_number.substring(0,3) === 'STR');
    let projectsOther = projectsMobiscroll.filter(p => p.project_number.substring(0,2) !== 'RT' && p.project_number.substring(0,3) !== 'STR');
    // sort projects by their number in reverse order so newer projects are on top
    let sortedProjectsRT = projectsRT = projectsRT.sort((a, b) => b.project_number.localeCompare(a.project_number));
    let sortedProjectsSTR = projectsSTR = projectsSTR.sort((a, b) => b.project_number.localeCompare(a.project_number));
    // join RT, STR and other projects
    projectsMobiscroll = sortedProjectsRT.concat(sortedProjectsSTR, projectsOther);
    // prepare timezone for mobiscroll
    moment.tz.add('Europe/Ljubljana|CET CEST|-10 -20|01010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010|-19RC0 3IP0 WM0 1fA0 1cM0 1cM0 1rc0 Qo0 1vmo0 1cM0 1cM0 1fA0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1fA0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1fA0 1o00 11A0 1o00 11A0 1o00 11A0 1qM0 WM0 1qM0 WM0 1qM0 11A0 1o00 11A0 1o00 11A0 1qM0 WM0 1qM0 WM0 1qM0 WM0 1qM0 11A0 1o00 11A0 1o00 11A0 1qM0 WM0 1qM0 WM0 1qM0 11A0 1o00 11A0 1o00 11A0 1o00 11A0 1qM0 WM0 1qM0 WM0 1qM0 11A0 1o00 11A0 1o00 11A0 1qM0 WM0 1qM0 WM0 1qM0 11A0 1o00 11A0 1o00 11A0 1o00 11A0 1qM0 WM0 1qM0 WM0 1qM0 11A0 1o00 11A0 1o00 11A0 1qM0 WM0 1qM0 WM0 1qM0 WM0 1qM0 11A0 1o00 11A0 1o00|12e5')
    mobiscroll.momentTimezone.moment = moment;
    // prepare color type for holidays and weekends
    colorType = [{ slot: 1, allDay: 'true', start: new Date('2024-01-01'), end: new Date('2024-01-01'), cssClass: 'md-dots-bg' },{ slot: 1, allDay: 'true', start: new Date('2024-01-02'), end: new Date('2024-01-02'), cssClass: 'md-dots-bg' },{ slot: 1, allDay: 'true', start: new Date('2024-02-08'), end: new Date('2024-02-08'), cssClass: 'md-dots-bg' },{ slot: 1, allDay: 'true', start: new Date('2024-04-01'), end: new Date('2024-04-01'), cssClass: 'md-dots-bg' },{ slot: 1, allDay: 'true', start: new Date('2024-04-27'), end: new Date('2024-04-27'), cssClass: 'md-dots-bg' },{ slot: 1, allDay: 'true', start: new Date('2024-05-01'), end: new Date('2024-05-02'), cssClass: 'md-dots-bg' },{ slot: 1, allDay: 'true', start: new Date('2024-05-19'), end: new Date('2024-05-19'), cssClass: 'md-dots-bg' },{ slot: 1, allDay: 'true', start: new Date('2024-06-25'), end: new Date('2024-06-25'), cssClass: 'md-dots-bg' },{ slot: 1, allDay: 'true', start: new Date('2024-08-15'), end: new Date('2024-08-15'), cssClass: 'md-dots-bg' },{ slot: 1, allDay: 'true', start: new Date('2024-10-31'), end: new Date('2024-11-1 21:00'), cssClass: 'md-dots-bg' },{ slot: 1, allDay: 'true', start: new Date('2024-12-25'), end: new Date('2024-12-26')},{ slot: 1, allDay: 'true', start: new Date('2025-01-01'), end: new Date('2025-01-02'), cssClass: 'md-dots-bg' },{ slot: 1, allDay: 'true', start: new Date('2023-07-17'), end: new Date('2023-08-16'), cssClass: 'md-stripes-bg' },{ slot: 1, allDay: 'true', start: new Date('2023-04-24'), end: new Date('2023-04-28'), cssClass: 'md-rect-bg' },{ slot: 1, allDay: 'true', start: new Date('2023-12-27'), end: new Date('2023-12-29'), cssClass: 'md-rect-bg' },{ slot: 1, allDay: 'true', start: new Date('2023-10-30'), end: new Date('2023-10-30'), cssClass: 'md-rect-bg' },{ date: moment().format('YYYY-MM-DD'), background: 'cornsilk' },{background: '#a5ceff4d', slot: 1, recurring: { repeat: 'weekly', weekDays: 'SU,SA' }, recurringException: [moment().format('YYYY-MM-DD')]}];
    // prepare resources for mobiscroll, every project is a resource
    projectsResource = projectsMobiscroll.map(p => { return { id: p.id, name: p.project_number + '-' + p.subscriber + '-' + p.project_name } });
    // add properties tp projectsMobiscroll for data
    projectsMobiscroll = projectsMobiscroll.map(p => {
      // p.start = new Date(p.start);
      // p.end = new Date(p.finish);
      p.end = p.finish;
      p.allDay = true;
      p.name = p.project_number + '-' + p.subscriber + '-' + p.project_name;
      p.title = p.project_number + '-' + p.subscriber + '-' + p.project_name;
      p.resource = p.id;
      p.color = p.completion == 100 ? '#4baf4f' : 'royalblue';
      return p;
    });
    // draw mobiscroll timeline on #projectsMobiscroll
    drawMobiscrollTimeline(projectsMobiscroll);

    // prepare projects for select2 for filtering
    projectsSelect = projectsMobiscroll.map(p => { return { id: p.id, text: p.project_number + '-' + p.subscriber + '-' + p.project_name } });
    $(".select2-exclusion").select2({
      data: projectsSelect,
      tags: true,
      multiple: true,
      dropdownParent: $('#modalFilterTimeline'),
    });
  }
}
// draw mobiscroll timeline
function drawMobiscrollTimeline(projects) {
  mobiscrollTimeline = mobiscroll.eventcalendar('#projectsMobiscroll', {
    clickToCreate: false,
    dragToCreate: false,
    dragToMove: false,
    dragToResize: false,
    timezonePlugin: mobiscroll.momentTimezone,
    dataTimezone: 'Europe/Ljubljana',
    displayTimezone: 'Europe/Ljubljana',
    colors: colorType,
    resources: projectsResource,
    data: projectsMobiscroll,
    view: {
      timeline: {
        type: 'month',
        size: 12,
        weekNumbers: true
      }
    },
    locale: sloLocal,
    onPageLoading: function (event, timeline) {
      filterResources(false, true);
    },
    onPageLoaded: function (args) {
      setTimeout(() => {
        let startDate = args.firstDay;
        let endDate = args.lastDay; 
        // let endDate = new Date(args.lastDay.getFullYear(), args.lastDay.getMonth(), args.lastDay.getDate(), 0);
        // set range button text
        rangeButton = $('#custom-date-range-text');
        rangeButton.text(moment(startDate).format('D. MMMM YYYY') + ' - ' + moment(endDate).format('D. MMMM YYYY'));
  
        // range picker
        myRange = mobiscroll.datepicker('#custom-date-range', {
          select: 'range',
          display: 'anchored',
          showOverlay: false,
          touchUI: true,
          buttons: [],
          locale: sloLocal,
          firstDay: 1,
          onClose: function (args, inst) {
            let date = inst.getVal();
            if (date[0] && date[1]){
              if(date[0].getTime() !== startDate.getTime() || date[1].getTime() !== endDate.getTime()){
                // navigate to start date
                mobiscrollTimeline.navigate(date[0]);
                // change displaz option to custom
                selectRange.setOptions({data: [ { text: 'Dan', value: 'day', disabled: false }, { text: 'Teden', value: 'week', disabled: false }, { text: 'Teden (razširjeno)', value: 'weekExt', disabled: false }, { text: 'Teden', value: 'week', disabled: false }, { text: '2 tedna', value: '2week', disabled: false }, { text: '4 Tedne', value: 'weekMonth', disabled: false }, { text: 'Mesec', value: 'month', disabled: false }, { text: '2 meseca', value: '2month', disabled: false }, { text: 'Pol leta', value: '6month', disabled: false }, { text: 'Leto', value: 'year', disabled: false }, { text: 'Ročno', value: 'custom', disabled: false }]});
                setTimeout(() => {
                  selectRange.setVal('custom');
                }, 200);
              }
              startDate = date[0];
              endDate = date[1];
              // set calendar view
              mobiscrollTimeline.setOptions({
                refDate: startDate,
                view: {
                  timeline: {
                    type: 'day',
                    size: getNrDays(startDate, endDate),
                    eventList: true
                  }
                },
                colors: [
                  { slot: 1, allDay: 'true', start: new Date('2024-01-01'), end: new Date('2024-01-01'), cssClass: 'md-dots-bg' },{ slot: 1, allDay: 'true', start: new Date('2024-01-02'), end: new Date('2024-01-02'), cssClass: 'md-dots-bg' },{ slot: 1, allDay: 'true', start: new Date('2024-02-08'), end: new Date('2024-02-08'), cssClass: 'md-dots-bg' },{ slot: 1, allDay: 'true', start: new Date('2024-04-01'), end: new Date('2024-04-01'), cssClass: 'md-dots-bg' },{ slot: 1, allDay: 'true', start: new Date('2024-04-27'), end: new Date('2024-04-27'), cssClass: 'md-dots-bg' },{ slot: 1, allDay: 'true', start: new Date('2024-05-01'), end: new Date('2024-05-02'), cssClass: 'md-dots-bg' },{ slot: 1, allDay: 'true', start: new Date('2024-05-19'), end: new Date('2024-05-19'), cssClass: 'md-dots-bg' },{ slot: 1, allDay: 'true', start: new Date('2024-06-25'), end: new Date('2024-06-25'), cssClass: 'md-dots-bg' },{ slot: 1, allDay: 'true', start: new Date('2024-08-15'), end: new Date('2024-08-15'), cssClass: 'md-dots-bg' },{ slot: 1, allDay: 'true', start: new Date('2024-10-31'), end: new Date('2024-11-1 21:00'), cssClass: 'md-dots-bg' },{ slot: 1, allDay: 'true', start: new Date('2024-12-25'), end: new Date('2024-12-26')},{ slot: 1, allDay: 'true', start: new Date('2025-01-01'), end: new Date('2025-01-02'), cssClass: 'md-dots-bg' },{ slot: 1, allDay: 'true', start: new Date('2023-07-17'), end: new Date('2023-08-16'), cssClass: 'md-stripes-bg' },{ slot: 1, allDay: 'true', start: new Date('2023-04-24'), end: new Date('2023-04-28'), cssClass: 'md-rect-bg' },{ slot: 1, allDay: 'true', start: new Date('2023-12-27'), end: new Date('2023-12-29'), cssClass: 'md-rect-bg' },{ slot: 1, allDay: 'true', start: new Date('2023-10-30'), end: new Date('2023-10-30'), cssClass: 'md-rect-bg' },
                  {background: '#a5ceff4d',slot: 1,recurring: { repeat: 'weekly', weekDays: 'SU,SA' },recurringException: [moment().format('YYYY-MM-DD')]},
                  { date: moment().format('YYYY-MM-DD'), background: 'cornsilk' },
                ],
              });
            }
          }
        });
        myRange.setVal([startDate, endDate]);
      }, 500);
    },
    //custom event look
    renderScheduleEvent: function (data) {
      var ev = data.original;
      let colorClass = 'royalblue';
      let iconType = 'star';
      // based on category add color and icon if not its a project, stays blue and gets a star icon
      if (ev.category) {
        if (ev.category.toLowerCase() == 'nabava') {
          colorClass = 'nabava';
          iconType = 'shopping-cart';
        } else if (ev.category.toLowerCase() == 'konstrukcija') {
          colorClass = 'konstrukcija';
          iconType = 'pencil-ruler';
        } else if (ev.category.toLowerCase() == 'strojna izdelava' || ev.category.toLowerCase() == 'strojna montaža - roboteh' || ev.category.toLowerCase() == 'strojna montaža - stranke') {
          colorClass = 'strojna-izdelava';
          iconType = 'cog';
        } else if (ev.category.toLowerCase() == 'elektro izdelava' || ev.category.toLowerCase() == 'elektro montaža - roboteh' || ev.category.toLowerCase() == 'elektro montaža - stranke') {
          colorClass = 'elektro-izdelava';
          iconType = 'bolt';
        } else if (ev.category.toLowerCase() == 'montaža') {
          colorClass = 'montaza';
          iconType = 'wrench';
        } else if (ev.category.toLowerCase() == 'programiranje') {
          colorClass = 'programiranje';
          iconType = 'laptop-code';
        } else if (ev.category.toLowerCase() == 'brez') {
          colorClass = 'brez';
          iconType = 'slash';
        }
        else {
          colorClass = 'royalblue';
          iconType = 'tasks';
        }
      }
      let icon = '<span class="mbsc-icon mbsc-font-icon ' + colorClass + '-ozadje"><i class="fas fa-' + iconType + ' text-white"></i></span>';
      let completionClass = ev.completion == 100 ? ' md-stripped-bg-compl' : '';
      return `<div class="mbsc-schedule-event-background mbsc-timeline-event-background mbsc-material ` + colorClass + `-ozadje"></div>
          <div class="mbsc-schedule-event-background mbsc-timeline-event-background mbsc-material omili-ozadje` + completionClass + `"></div>
          <div class="mbsc-schedule-event-inner mbsc-material md-timeline-template-event">
          <div class="md-timeline-template-event-cont">
            `+ icon +`
          <span class="md-timeline-template-title ml-2 mr-2">` + ev.title + `</span></div></div>`;
    },
    // custom header
    renderHeader: function () {
      return `<div id="custom-date-range-space" class="col-md-4 col-12">
          <div id="custom-date-range" class="w-210px">
            <button mbsc-button data-variant="flat" class="mbsc-calendar-button">
              <span id="custom-date-range-text" class="mbsc-calendar-title"></span>
            </button>
          </div>
        </div>
        <div class="col-md-8 col-12 row ml-0">
          <div class="md-shift-header-controls">
          <button class="btn btn-primary mt-2 mr-2" id="filterResources" data-toggle="tooltip" data-original-title="Filter zaposlenih">
            <i class="fas fa-filter fa-lg"></i>
          </button>
          <button class="btn btn-primary mt-2 mr-2 btn-on-stretched" id="refreshData" data-toggle="tooltip" data-original-title="Osveži">
            <i class="fas fa-sync fa-lg"></i>
          </button>
          <label class="md-shift-cal-view m-2">
          <input mbsc-input id="shift-management-view" data-dropdown="true" data-input-style="box" />
          </label>
          <select id="shift-management-select">
          <option value="day">Dan</option>
          <option value="week">Teden</option>
          <option value="weekExt">Teden (razširjeno)</option>
          <option value="2week">2 tedna</option>
          <option value="weekMonth">4 tedne</option>
          <option value="month">Mesec</option>
          <option value="2month">2 meseca</option>
          <option value="6month">Pol leta</option>
          <option value="year">Leto</option>
          <option value="custom">Ročno</option>
          </select>
          </div>
          <div mbsc-calendar-prev></div>
          <div mbsc-calendar-today></div>
          <div mbsc-calendar-next></div>
        </div>`;
    },
  });
  // filter button
  $('#filterResources').on('click', () => {
    // appendChekboxes();
    $('#modalFilterTimeline').modal('toggle');
  });
  // refresh button
  $('#refreshData').on('click', () => {
    filterResources(true);
  });
  // switching views (day,week,2weeks,month)
  selectRange = mobiscroll.select('#shift-management-select', {
    inputElement: document.getElementById('shift-management-view'),
    touchUi: false,
    onChange: function (event, inst) {
      if (event.value == 'day') {
        // myRange.setVal([]);
        setTimeout(() => {
          selectRange.setOptions({data: [ { text: 'Dan', value: 'day' }, { text: 'Teden', value: 'week' }, { text: 'Teden (razširjeno)', value: 'weekExt' }, { text: '2 tedna', value: '2week' }, { text: '4 Tedne', value: 'weekMonth' }, { text: 'Mesec', value: 'month' }, { text: '2 meseca', value: '2month' }, { text: 'Pol leta', value: '6month' }, { text: 'Leto', value: 'year' }, { text: 'Ročno', value: 'custom', disabled: true }]});
        }, 500);
        mobiscrollTimeline.setOptions({
          view: {
            timeline: {
              type: 'day',
              eventList: true,
              weekNumbers: true,
            }
          },
          colors: [
            { slot: 1, allDay: 'true', start: new Date('2024-01-01'), end: new Date('2024-01-01'), cssClass: 'md-dots-bg' },{ slot: 1, allDay: 'true', start: new Date('2024-01-02'), end: new Date('2024-01-02'), cssClass: 'md-dots-bg' },{ slot: 1, allDay: 'true', start: new Date('2024-02-08'), end: new Date('2024-02-08'), cssClass: 'md-dots-bg' },{ slot: 1, allDay: 'true', start: new Date('2024-04-01'), end: new Date('2024-04-01'), cssClass: 'md-dots-bg' },{ slot: 1, allDay: 'true', start: new Date('2024-04-27'), end: new Date('2024-04-27'), cssClass: 'md-dots-bg' },{ slot: 1, allDay: 'true', start: new Date('2024-05-01'), end: new Date('2024-05-02'), cssClass: 'md-dots-bg' },{ slot: 1, allDay: 'true', start: new Date('2024-05-19'), end: new Date('2024-05-19'), cssClass: 'md-dots-bg' },{ slot: 1, allDay: 'true', start: new Date('2024-06-25'), end: new Date('2024-06-25'), cssClass: 'md-dots-bg' },{ slot: 1, allDay: 'true', start: new Date('2024-08-15'), end: new Date('2024-08-15'), cssClass: 'md-dots-bg' },{ slot: 1, allDay: 'true', start: new Date('2024-10-31'), end: new Date('2024-11-1 21:00'), cssClass: 'md-dots-bg' },{ slot: 1, allDay: 'true', start: new Date('2024-12-25'), end: new Date('2024-12-26')},{ slot: 1, allDay: 'true', start: new Date('2025-01-01'), end: new Date('2025-01-02'), cssClass: 'md-dots-bg' },{ slot: 1, allDay: 'true', start: new Date('2023-07-17'), end: new Date('2023-08-16'), cssClass: 'md-stripes-bg' },{ slot: 1, allDay: 'true', start: new Date('2023-04-24'), end: new Date('2023-04-28'), cssClass: 'md-rect-bg' },{ slot: 1, allDay: 'true', start: new Date('2023-12-27'), end: new Date('2023-12-29'), cssClass: 'md-rect-bg' },{ slot: 1, allDay: 'true', start: new Date('2023-10-30'), end: new Date('2023-10-30'), cssClass: 'md-rect-bg' },
            {background: '#a5ceff4d',slot: 1,recurring: { repeat: 'weekly', weekDays: 'SU,SA' }},
          ],
        });
      }
      else if (event.value == '2week') {
        // myRange.setVal([]);
        setTimeout(() => {
          selectRange.setOptions({data: [ { text: 'Dan', value: 'day' }, { text: 'Teden', value: 'week' }, { text: 'Teden (razširjeno)', value: 'weekExt' }, { text: '2 tedna', value: '2week' }, { text: '4 Tedne', value: 'weekMonth' }, { text: 'Mesec', value: 'month' }, { text: '2 meseca', value: '2month' }, { text: 'Pol leta', value: '6month' }, { text: 'Leto', value: 'year' }, { text: 'Ročno', value: 'custom', disabled: true }]});
        }, 500);
        mobiscrollTimeline.setOptions({
          view: {
            timeline: {
              type: 'week',
              timeCellStep: 1440,
              timeLabelStep: 1440,
              startDay: 1,
              endDay: 0,
              size: 2,
              weekNumbers: true,
            }
          },
          colors: [
            { slot: 1, allDay: 'true', start: new Date('2024-01-01'), end: new Date('2024-01-01'), cssClass: 'md-dots-bg' },{ slot: 1, allDay: 'true', start: new Date('2024-01-02'), end: new Date('2024-01-02'), cssClass: 'md-dots-bg' },{ slot: 1, allDay: 'true', start: new Date('2024-02-08'), end: new Date('2024-02-08'), cssClass: 'md-dots-bg' },{ slot: 1, allDay: 'true', start: new Date('2024-04-01'), end: new Date('2024-04-01'), cssClass: 'md-dots-bg' },{ slot: 1, allDay: 'true', start: new Date('2024-04-27'), end: new Date('2024-04-27'), cssClass: 'md-dots-bg' },{ slot: 1, allDay: 'true', start: new Date('2024-05-01'), end: new Date('2024-05-02'), cssClass: 'md-dots-bg' },{ slot: 1, allDay: 'true', start: new Date('2024-05-19'), end: new Date('2024-05-19'), cssClass: 'md-dots-bg' },{ slot: 1, allDay: 'true', start: new Date('2024-06-25'), end: new Date('2024-06-25'), cssClass: 'md-dots-bg' },{ slot: 1, allDay: 'true', start: new Date('2024-08-15'), end: new Date('2024-08-15'), cssClass: 'md-dots-bg' },{ slot: 1, allDay: 'true', start: new Date('2024-10-31'), end: new Date('2024-11-1 21:00'), cssClass: 'md-dots-bg' },{ slot: 1, allDay: 'true', start: new Date('2024-12-25'), end: new Date('2024-12-26')},{ slot: 1, allDay: 'true', start: new Date('2025-01-01'), end: new Date('2025-01-02'), cssClass: 'md-dots-bg' },{ slot: 1, allDay: 'true', start: new Date('2023-07-17'), end: new Date('2023-08-16'), cssClass: 'md-stripes-bg' },{ slot: 1, allDay: 'true', start: new Date('2023-04-24'), end: new Date('2023-04-28'), cssClass: 'md-rect-bg' },{ slot: 1, allDay: 'true', start: new Date('2023-12-27'), end: new Date('2023-12-29'), cssClass: 'md-rect-bg' },{ slot: 1, allDay: 'true', start: new Date('2023-10-30'), end: new Date('2023-10-30'), cssClass: 'md-rect-bg' },
            {background: '#a5ceff4d',slot: 1,recurring: { repeat: 'weekly', weekDays: 'SU,SA' },recurringException: [moment().format('YYYY-MM-DD')]},
            { date: moment().format('YYYY-MM-DD'), background: 'cornsilk' },
          ],
        });
      }
      else if (event.value == 'week') {
        // myRange.setVal([]);
        setTimeout(() => {
          selectRange.setOptions({data: [ { text: 'Dan', value: 'day' }, { text: 'Teden', value: 'week' }, { text: 'Teden (razširjeno)', value: 'weekExt' }, { text: '2 tedna', value: '2week' }, { text: '4 Tedne', value: 'weekMonth' }, { text: 'Mesec', value: 'month' }, { text: '2 meseca', value: '2month' }, { text: 'Pol leta', value: '6month' }, { text: 'Leto', value: 'year' }, { text: 'Ročno', value: 'custom', disabled: true }]});
        }, 500);
        mobiscrollTimeline.setOptions({
          view: {
            timeline: {
              type: 'week',
              eventList: false,
              startDay: 1,
              endDay: 0,
              timeCellStep: 1440,
              timeLabelStep: 1440,
              weekNumbers: true,
            }
          },
          colors: [
            { slot: 1, allDay: 'true', start: new Date('2024-01-01'), end: new Date('2024-01-01'), cssClass: 'md-dots-bg' },{ slot: 1, allDay: 'true', start: new Date('2024-01-02'), end: new Date('2024-01-02'), cssClass: 'md-dots-bg' },{ slot: 1, allDay: 'true', start: new Date('2024-02-08'), end: new Date('2024-02-08'), cssClass: 'md-dots-bg' },{ slot: 1, allDay: 'true', start: new Date('2024-04-01'), end: new Date('2024-04-01'), cssClass: 'md-dots-bg' },{ slot: 1, allDay: 'true', start: new Date('2024-04-27'), end: new Date('2024-04-27'), cssClass: 'md-dots-bg' },{ slot: 1, allDay: 'true', start: new Date('2024-05-01'), end: new Date('2024-05-02'), cssClass: 'md-dots-bg' },{ slot: 1, allDay: 'true', start: new Date('2024-05-19'), end: new Date('2024-05-19'), cssClass: 'md-dots-bg' },{ slot: 1, allDay: 'true', start: new Date('2024-06-25'), end: new Date('2024-06-25'), cssClass: 'md-dots-bg' },{ slot: 1, allDay: 'true', start: new Date('2024-08-15'), end: new Date('2024-08-15'), cssClass: 'md-dots-bg' },{ slot: 1, allDay: 'true', start: new Date('2024-10-31'), end: new Date('2024-11-1 21:00'), cssClass: 'md-dots-bg' },{ slot: 1, allDay: 'true', start: new Date('2024-12-25'), end: new Date('2024-12-26')},{ slot: 1, allDay: 'true', start: new Date('2025-01-01'), end: new Date('2025-01-02'), cssClass: 'md-dots-bg' },{ slot: 1, allDay: 'true', start: new Date('2023-07-17'), end: new Date('2023-08-16'), cssClass: 'md-stripes-bg' },{ slot: 1, allDay: 'true', start: new Date('2023-04-24'), end: new Date('2023-04-28'), cssClass: 'md-rect-bg' },{ slot: 1, allDay: 'true', start: new Date('2023-12-27'), end: new Date('2023-12-29'), cssClass: 'md-rect-bg' },{ slot: 1, allDay: 'true', start: new Date('2023-10-30'), end: new Date('2023-10-30'), cssClass: 'md-rect-bg' },
            {background: '#a5ceff4d',slot: 1,recurring: { repeat: 'weekly', weekDays: 'SU,SA' },recurringException: [moment().format('YYYY-MM-DD')]},
            { date: moment().format('YYYY-MM-DD'), background: 'cornsilk' },
          ],
        });
      }
      else if (event.value == 'weekExt') {
        // myRange.setVal([]);
        setTimeout(() => {
          selectRange.setOptions({data: [ { text: 'Dan', value: 'day' }, { text: 'Teden', value: 'week' }, { text: 'Teden (razširjeno)', value: 'weekExt' }, { text: '2 tedna', value: '2week' }, { text: '4 Tedne', value: 'weekMonth' }, { text: 'Mesec', value: 'month' }, { text: '2 meseca', value: '2month' }, { text: 'Pol leta', value: '6month' }, { text: 'Leto', value: 'year' }, { text: 'Ročno', value: 'custom', disabled: true }]});
        }, 500);
        mobiscrollTimeline.setOptions({
          view: {
            timeline: {
              type: 'week',
              eventList: false,
              startDay: 1,
              endDay: 0,
              weekNumbers: true,
            }
          },
          colors: [
            { slot: 1, allDay: 'true', start: new Date('2024-01-01'), end: new Date('2024-01-01'), cssClass: 'md-dots-bg' },{ slot: 1, allDay: 'true', start: new Date('2024-01-02'), end: new Date('2024-01-02'), cssClass: 'md-dots-bg' },{ slot: 1, allDay: 'true', start: new Date('2024-02-08'), end: new Date('2024-02-08'), cssClass: 'md-dots-bg' },{ slot: 1, allDay: 'true', start: new Date('2024-04-01'), end: new Date('2024-04-01'), cssClass: 'md-dots-bg' },{ slot: 1, allDay: 'true', start: new Date('2024-04-27'), end: new Date('2024-04-27'), cssClass: 'md-dots-bg' },{ slot: 1, allDay: 'true', start: new Date('2024-05-01'), end: new Date('2024-05-02'), cssClass: 'md-dots-bg' },{ slot: 1, allDay: 'true', start: new Date('2024-05-19'), end: new Date('2024-05-19'), cssClass: 'md-dots-bg' },{ slot: 1, allDay: 'true', start: new Date('2024-06-25'), end: new Date('2024-06-25'), cssClass: 'md-dots-bg' },{ slot: 1, allDay: 'true', start: new Date('2024-08-15'), end: new Date('2024-08-15'), cssClass: 'md-dots-bg' },{ slot: 1, allDay: 'true', start: new Date('2024-10-31'), end: new Date('2024-11-1 21:00'), cssClass: 'md-dots-bg' },{ slot: 1, allDay: 'true', start: new Date('2024-12-25'), end: new Date('2024-12-26')},{ slot: 1, allDay: 'true', start: new Date('2025-01-01'), end: new Date('2025-01-02'), cssClass: 'md-dots-bg' },{ slot: 1, allDay: 'true', start: new Date('2023-07-17'), end: new Date('2023-08-16'), cssClass: 'md-stripes-bg' },{ slot: 1, allDay: 'true', start: new Date('2023-04-24'), end: new Date('2023-04-28'), cssClass: 'md-rect-bg' },{ slot: 1, allDay: 'true', start: new Date('2023-12-27'), end: new Date('2023-12-29'), cssClass: 'md-rect-bg' },{ slot: 1, allDay: 'true', start: new Date('2023-10-30'), end: new Date('2023-10-30'), cssClass: 'md-rect-bg' },
            {background: '#a5ceff4d',slot: 1,recurring: { repeat: 'weekly', weekDays: 'SU,SA' },recurringException: [moment().format('YYYY-MM-DD')]},
            { date: moment().format('YYYY-MM-DD'), background: 'cornsilk' },
          ],
        });
      }
      else if (event.value == 'weekMonth') {
        // myRange.setVal([]);
        setTimeout(() => {
          selectRange.setOptions({data: [ { text: 'Dan', value: 'day' }, { text: 'Teden', value: 'week' }, { text: 'Teden (razširjeno)', value: 'weekExt' }, { text: '2 tedna', value: '2week' }, { text: '4 Tedne', value: 'weekMonth' }, { text: 'Mesec', value: 'month' }, { text: '2 meseca', value: '2month' }, { text: 'Pol leta', value: '6month' }, { text: 'Leto', value: 'year' }, { text: 'Ročno', value: 'custom', disabled: true }]});
        }, 500);
        mobiscrollTimeline.setOptions({
          view: {
            timeline: {
              type: 'week',
              eventList: false,
              size: 4,
              startDay: 1,
              endDay: 0,
              timeCellStep: 1440,
              timeLabelStep: 1440,
              weekNumbers: true,
            }
          },
          colors: [
            { slot: 1, allDay: 'true', start: new Date('2024-01-01'), end: new Date('2024-01-01'), cssClass: 'md-dots-bg' },{ slot: 1, allDay: 'true', start: new Date('2024-01-02'), end: new Date('2024-01-02'), cssClass: 'md-dots-bg' },{ slot: 1, allDay: 'true', start: new Date('2024-02-08'), end: new Date('2024-02-08'), cssClass: 'md-dots-bg' },{ slot: 1, allDay: 'true', start: new Date('2024-04-01'), end: new Date('2024-04-01'), cssClass: 'md-dots-bg' },{ slot: 1, allDay: 'true', start: new Date('2024-04-27'), end: new Date('2024-04-27'), cssClass: 'md-dots-bg' },{ slot: 1, allDay: 'true', start: new Date('2024-05-01'), end: new Date('2024-05-02'), cssClass: 'md-dots-bg' },{ slot: 1, allDay: 'true', start: new Date('2024-05-19'), end: new Date('2024-05-19'), cssClass: 'md-dots-bg' },{ slot: 1, allDay: 'true', start: new Date('2024-06-25'), end: new Date('2024-06-25'), cssClass: 'md-dots-bg' },{ slot: 1, allDay: 'true', start: new Date('2024-08-15'), end: new Date('2024-08-15'), cssClass: 'md-dots-bg' },{ slot: 1, allDay: 'true', start: new Date('2024-10-31'), end: new Date('2024-11-1 21:00'), cssClass: 'md-dots-bg' },{ slot: 1, allDay: 'true', start: new Date('2024-12-25'), end: new Date('2024-12-26')},{ slot: 1, allDay: 'true', start: new Date('2025-01-01'), end: new Date('2025-01-02'), cssClass: 'md-dots-bg' },{ slot: 1, allDay: 'true', start: new Date('2023-07-17'), end: new Date('2023-08-16'), cssClass: 'md-stripes-bg' },{ slot: 1, allDay: 'true', start: new Date('2023-04-24'), end: new Date('2023-04-28'), cssClass: 'md-rect-bg' },{ slot: 1, allDay: 'true', start: new Date('2023-12-27'), end: new Date('2023-12-29'), cssClass: 'md-rect-bg' },{ slot: 1, allDay: 'true', start: new Date('2023-10-30'), end: new Date('2023-10-30'), cssClass: 'md-rect-bg' },
            {background: '#a5ceff4d',slot: 1,recurring: { repeat: 'weekly', weekDays: 'SU,SA' },recurringException: [moment().format('YYYY-MM-DD')]},
            { date: moment().format('YYYY-MM-DD'), background: 'cornsilk' },
          ],
        });
      }
      else if (event.value == 'month'){
        // myRange.setVal([]);
        setTimeout(() => {
          selectRange.setOptions({data: [ { text: 'Dan', value: 'day' }, { text: 'Teden', value: 'week' }, { text: 'Teden (razširjeno)', value: 'weekExt' }, { text: '2 tedna', value: '2week' }, { text: '4 Tedne', value: 'weekMonth' }, { text: 'Mesec', value: 'month' }, { text: '2 meseca', value: '2month' }, { text: 'Pol leta', value: '6month' }, { text: 'Leto', value: 'year' }, { text: 'Ročno', value: 'custom', disabled: true }]});
        }, 500);
        mobiscrollTimeline.setOptions({
          view: {
            timeline: {
              type: 'month',
              size: 1,
              weekNumbers: true,
            }
          },
          colors: [
            { slot: 1, allDay: 'true', start: new Date('2024-01-01'), end: new Date('2024-01-01'), cssClass: 'md-dots-bg' },{ slot: 1, allDay: 'true', start: new Date('2024-01-02'), end: new Date('2024-01-02'), cssClass: 'md-dots-bg' },{ slot: 1, allDay: 'true', start: new Date('2024-02-08'), end: new Date('2024-02-08'), cssClass: 'md-dots-bg' },{ slot: 1, allDay: 'true', start: new Date('2024-04-01'), end: new Date('2024-04-01'), cssClass: 'md-dots-bg' },{ slot: 1, allDay: 'true', start: new Date('2024-04-27'), end: new Date('2024-04-27'), cssClass: 'md-dots-bg' },{ slot: 1, allDay: 'true', start: new Date('2024-05-01'), end: new Date('2024-05-02'), cssClass: 'md-dots-bg' },{ slot: 1, allDay: 'true', start: new Date('2024-05-19'), end: new Date('2024-05-19'), cssClass: 'md-dots-bg' },{ slot: 1, allDay: 'true', start: new Date('2024-06-25'), end: new Date('2024-06-25'), cssClass: 'md-dots-bg' },{ slot: 1, allDay: 'true', start: new Date('2024-08-15'), end: new Date('2024-08-15'), cssClass: 'md-dots-bg' },{ slot: 1, allDay: 'true', start: new Date('2024-10-31'), end: new Date('2024-11-1 21:00'), cssClass: 'md-dots-bg' },{ slot: 1, allDay: 'true', start: new Date('2024-12-25'), end: new Date('2024-12-26')},{ slot: 1, allDay: 'true', start: new Date('2025-01-01'), end: new Date('2025-01-02'), cssClass: 'md-dots-bg' },{ slot: 1, allDay: 'true', start: new Date('2023-07-17'), end: new Date('2023-08-16'), cssClass: 'md-stripes-bg' },{ slot: 1, allDay: 'true', start: new Date('2023-04-24'), end: new Date('2023-04-28'), cssClass: 'md-rect-bg' },{ slot: 1, allDay: 'true', start: new Date('2023-12-27'), end: new Date('2023-12-29'), cssClass: 'md-rect-bg' },{ slot: 1, allDay: 'true', start: new Date('2023-10-30'), end: new Date('2023-10-30'), cssClass: 'md-rect-bg' },
            {background: '#a5ceff4d',slot: 1,recurring: { repeat: 'weekly', weekDays: 'SU,SA' },recurringException: [moment().format('YYYY-MM-DD')]},
            { date: moment().format('YYYY-MM-DD'), background: 'cornsilk' },
          ],
        });
      }
      else if (event.value == '2month'){
        // myRange.setVal([]);
        setTimeout(() => {
          selectRange.setOptions({data: [ { text: 'Dan', value: 'day' }, { text: 'Teden', value: 'week' }, { text: 'Teden (razširjeno)', value: 'weekExt' }, { text: '2 tedna', value: '2week' }, { text: '4 Tedne', value: 'weekMonth' }, { text: 'Mesec', value: 'month' }, { text: '2 meseca', value: '2month' }, { text: 'Pol leta', value: '6month' }, { text: 'Leto', value: 'year' }, { text: 'Ročno', value: 'custom', disabled: true }]});
        }, 500);
        mobiscrollTimeline.setOptions({
          view: {
            timeline: {
              type: 'month',
              size: 2,
              weekNumbers: true,
            }
          },
          colors: [
            { slot: 1, allDay: 'true', start: new Date('2024-01-01'), end: new Date('2024-01-01'), cssClass: 'md-dots-bg' },{ slot: 1, allDay: 'true', start: new Date('2024-01-02'), end: new Date('2024-01-02'), cssClass: 'md-dots-bg' },{ slot: 1, allDay: 'true', start: new Date('2024-02-08'), end: new Date('2024-02-08'), cssClass: 'md-dots-bg' },{ slot: 1, allDay: 'true', start: new Date('2024-04-01'), end: new Date('2024-04-01'), cssClass: 'md-dots-bg' },{ slot: 1, allDay: 'true', start: new Date('2024-04-27'), end: new Date('2024-04-27'), cssClass: 'md-dots-bg' },{ slot: 1, allDay: 'true', start: new Date('2024-05-01'), end: new Date('2024-05-02'), cssClass: 'md-dots-bg' },{ slot: 1, allDay: 'true', start: new Date('2024-05-19'), end: new Date('2024-05-19'), cssClass: 'md-dots-bg' },{ slot: 1, allDay: 'true', start: new Date('2024-06-25'), end: new Date('2024-06-25'), cssClass: 'md-dots-bg' },{ slot: 1, allDay: 'true', start: new Date('2024-08-15'), end: new Date('2024-08-15'), cssClass: 'md-dots-bg' },{ slot: 1, allDay: 'true', start: new Date('2024-10-31'), end: new Date('2024-11-1 21:00'), cssClass: 'md-dots-bg' },{ slot: 1, allDay: 'true', start: new Date('2024-12-25'), end: new Date('2024-12-26')},{ slot: 1, allDay: 'true', start: new Date('2025-01-01'), end: new Date('2025-01-02'), cssClass: 'md-dots-bg' },{ slot: 1, allDay: 'true', start: new Date('2023-07-17'), end: new Date('2023-08-16'), cssClass: 'md-stripes-bg' },{ slot: 1, allDay: 'true', start: new Date('2023-04-24'), end: new Date('2023-04-28'), cssClass: 'md-rect-bg' },{ slot: 1, allDay: 'true', start: new Date('2023-12-27'), end: new Date('2023-12-29'), cssClass: 'md-rect-bg' },{ slot: 1, allDay: 'true', start: new Date('2023-10-30'), end: new Date('2023-10-30'), cssClass: 'md-rect-bg' },
            {background: '#a5ceff4d',slot: 1,recurring: { repeat: 'weekly', weekDays: 'SU,SA' },recurringException: [moment().format('YYYY-MM-DD')]},
            { date: moment().format('YYYY-MM-DD'), background: 'cornsilk' },
          ],
        });
      }
      else if (event.value == '6month'){
        // myRange.setVal([]);
        setTimeout(() => {
          selectRange.setOptions({data: [ { text: 'Dan', value: 'day' }, { text: 'Teden', value: 'week' }, { text: 'Teden (razširjeno)', value: 'weekExt' }, { text: '2 tedna', value: '2week' }, { text: '4 Tedne', value: 'weekMonth' }, { text: 'Mesec', value: 'month' }, { text: '2 meseca', value: '2month' }, { text: 'Pol leta', value: '6month' }, { text: 'Leto', value: 'year' }, { text: 'Ročno', value: 'custom', disabled: true }]});
        }, 500);
        mobiscrollTimeline.setOptions({
          view: {
            timeline: {
              type: 'month',
              size: 6,
              weekNumbers: true,
            }
          },
          colors: [
            { slot: 1, allDay: 'true', start: new Date('2024-01-01'), end: new Date('2024-01-01'), cssClass: 'md-dots-bg' },{ slot: 1, allDay: 'true', start: new Date('2024-01-02'), end: new Date('2024-01-02'), cssClass: 'md-dots-bg' },{ slot: 1, allDay: 'true', start: new Date('2024-02-08'), end: new Date('2024-02-08'), cssClass: 'md-dots-bg' },{ slot: 1, allDay: 'true', start: new Date('2024-04-01'), end: new Date('2024-04-01'), cssClass: 'md-dots-bg' },{ slot: 1, allDay: 'true', start: new Date('2024-04-27'), end: new Date('2024-04-27'), cssClass: 'md-dots-bg' },{ slot: 1, allDay: 'true', start: new Date('2024-05-01'), end: new Date('2024-05-02'), cssClass: 'md-dots-bg' },{ slot: 1, allDay: 'true', start: new Date('2024-05-19'), end: new Date('2024-05-19'), cssClass: 'md-dots-bg' },{ slot: 1, allDay: 'true', start: new Date('2024-06-25'), end: new Date('2024-06-25'), cssClass: 'md-dots-bg' },{ slot: 1, allDay: 'true', start: new Date('2024-08-15'), end: new Date('2024-08-15'), cssClass: 'md-dots-bg' },{ slot: 1, allDay: 'true', start: new Date('2024-10-31'), end: new Date('2024-11-1 21:00'), cssClass: 'md-dots-bg' },{ slot: 1, allDay: 'true', start: new Date('2024-12-25'), end: new Date('2024-12-26')},{ slot: 1, allDay: 'true', start: new Date('2025-01-01'), end: new Date('2025-01-02'), cssClass: 'md-dots-bg' },{ slot: 1, allDay: 'true', start: new Date('2023-07-17'), end: new Date('2023-08-16'), cssClass: 'md-stripes-bg' },{ slot: 1, allDay: 'true', start: new Date('2023-04-24'), end: new Date('2023-04-28'), cssClass: 'md-rect-bg' },{ slot: 1, allDay: 'true', start: new Date('2023-12-27'), end: new Date('2023-12-29'), cssClass: 'md-rect-bg' },{ slot: 1, allDay: 'true', start: new Date('2023-10-30'), end: new Date('2023-10-30'), cssClass: 'md-rect-bg' },
            {background: '#a5ceff4d',slot: 1,recurring: { repeat: 'weekly', weekDays: 'SU,SA' },recurringException: [moment().format('YYYY-MM-DD')]},
            { date: moment().format('YYYY-MM-DD'), background: 'cornsilk' },
          ],
        });
      }
      else if (event.value == 'custom'){
        var date = myRange.getVal();
        mobiscrollTimeline.setOptions({
          colors: [
            { slot: 1, allDay: 'true', start: new Date('2024-01-01'), end: new Date('2024-01-01'), cssClass: 'md-dots-bg' },{ slot: 1, allDay: 'true', start: new Date('2024-01-02'), end: new Date('2024-01-02'), cssClass: 'md-dots-bg' },{ slot: 1, allDay: 'true', start: new Date('2024-02-08'), end: new Date('2024-02-08'), cssClass: 'md-dots-bg' },{ slot: 1, allDay: 'true', start: new Date('2024-04-01'), end: new Date('2024-04-01'), cssClass: 'md-dots-bg' },{ slot: 1, allDay: 'true', start: new Date('2024-04-27'), end: new Date('2024-04-27'), cssClass: 'md-dots-bg' },{ slot: 1, allDay: 'true', start: new Date('2024-05-01'), end: new Date('2024-05-02'), cssClass: 'md-dots-bg' },{ slot: 1, allDay: 'true', start: new Date('2024-05-19'), end: new Date('2024-05-19'), cssClass: 'md-dots-bg' },{ slot: 1, allDay: 'true', start: new Date('2024-06-25'), end: new Date('2024-06-25'), cssClass: 'md-dots-bg' },{ slot: 1, allDay: 'true', start: new Date('2024-08-15'), end: new Date('2024-08-15'), cssClass: 'md-dots-bg' },{ slot: 1, allDay: 'true', start: new Date('2024-10-31'), end: new Date('2024-11-1 21:00'), cssClass: 'md-dots-bg' },{ slot: 1, allDay: 'true', start: new Date('2024-12-25'), end: new Date('2024-12-26')},{ slot: 1, allDay: 'true', start: new Date('2025-01-01'), end: new Date('2025-01-02'), cssClass: 'md-dots-bg' },{ slot: 1, allDay: 'true', start: new Date('2023-07-17'), end: new Date('2023-08-16'), cssClass: 'md-stripes-bg' },{ slot: 1, allDay: 'true', start: new Date('2023-04-24'), end: new Date('2023-04-28'), cssClass: 'md-rect-bg' },{ slot: 1, allDay: 'true', start: new Date('2023-12-27'), end: new Date('2023-12-29'), cssClass: 'md-rect-bg' },{ slot: 1, allDay: 'true', start: new Date('2023-10-30'), end: new Date('2023-10-30'), cssClass: 'md-rect-bg' },
            {background: '#a5ceff4d',slot: 1,recurring: { repeat: 'weekly', weekDays: 'SU,SA' },recurringException: [moment().format('YYYY-MM-DD')]},
            { date: moment().format('YYYY-MM-DD'), background: 'cornsilk' },
          ],
        });
      }
      else {
        // myRange.setVal([]);
        setTimeout(() => {
          selectRange.setOptions({data: [ { text: 'Dan', value: 'day' }, { text: 'Teden', value: 'week' }, { text: 'Teden (razširjeno)', value: 'weekExt' }, { text: '2 tedna', value: '2week' }, { text: '4 Tedne', value: 'weekMonth' }, { text: 'Mesec', value: 'month' }, { text: '2 meseca', value: '2month' }, { text: 'Pol leta', value: '6month' }, { text: 'Leto', value: 'year' }, { text: 'Ročno', value: 'custom', disabled: true }]});
        }, 500);
        mobiscrollTimeline.setOptions({
          view: {
            timeline: {
              type: 'month',
              size: 12,
              weekNumbers: true,
            }
          },
          colors: [
            { slot: 1, allDay: 'true', start: new Date('2024-01-01'), end: new Date('2024-01-01'), cssClass: 'md-dots-bg' },{ slot: 1, allDay: 'true', start: new Date('2024-01-02'), end: new Date('2024-01-02'), cssClass: 'md-dots-bg' },{ slot: 1, allDay: 'true', start: new Date('2024-02-08'), end: new Date('2024-02-08'), cssClass: 'md-dots-bg' },{ slot: 1, allDay: 'true', start: new Date('2024-04-01'), end: new Date('2024-04-01'), cssClass: 'md-dots-bg' },{ slot: 1, allDay: 'true', start: new Date('2024-04-27'), end: new Date('2024-04-27'), cssClass: 'md-dots-bg' },{ slot: 1, allDay: 'true', start: new Date('2024-05-01'), end: new Date('2024-05-02'), cssClass: 'md-dots-bg' },{ slot: 1, allDay: 'true', start: new Date('2024-05-19'), end: new Date('2024-05-19'), cssClass: 'md-dots-bg' },{ slot: 1, allDay: 'true', start: new Date('2024-06-25'), end: new Date('2024-06-25'), cssClass: 'md-dots-bg' },{ slot: 1, allDay: 'true', start: new Date('2024-08-15'), end: new Date('2024-08-15'), cssClass: 'md-dots-bg' },{ slot: 1, allDay: 'true', start: new Date('2024-10-31'), end: new Date('2024-11-1 21:00'), cssClass: 'md-dots-bg' },{ slot: 1, allDay: 'true', start: new Date('2024-12-25'), end: new Date('2024-12-26')},{ slot: 1, allDay: 'true', start: new Date('2025-01-01'), end: new Date('2025-01-02'), cssClass: 'md-dots-bg' },{ slot: 1, allDay: 'true', start: new Date('2023-07-17'), end: new Date('2023-08-16'), cssClass: 'md-stripes-bg' },{ slot: 1, allDay: 'true', start: new Date('2023-04-24'), end: new Date('2023-04-28'), cssClass: 'md-rect-bg' },{ slot: 1, allDay: 'true', start: new Date('2023-12-27'), end: new Date('2023-12-29'), cssClass: 'md-rect-bg' },{ slot: 1, allDay: 'true', start: new Date('2023-10-30'), end: new Date('2023-10-30'), cssClass: 'md-rect-bg' },
            {background: '#a5ceff4d',slot: 1,recurring: { repeat: 'weekly', weekDays: 'SU,SA' },recurringException: [moment().format('YYYY-MM-DD')]},
            { date: moment().format('YYYY-MM-DD'), background: 'cornsilk' },
          ],
        });
      }
    }
  });
  setTimeout(() => {
    selectRange.setVal('year');
  }, 500);
}
// returns the number of days between two dates
function getNrDays(start, end) {
  return Math.round(Math.abs((end.setHours(0) - start.setHours(0)) / (24 * 60 * 60 * 1000))) + 1;
}
// get all data for mobiscroll (for now just projects, will add tasks later)
function getData(start, end){

}
// filter resources on project timeline
async function filterResources(refreshData, moveTimeline){
  // if moveTimeline is true then FILTERING PROJECTS doesnt need to execute
  if (!moveTimeline){
    // FILTERING PROJECTS
    // reset resources on timeline first in order to show it properly after filtering; if refreshData is true then no need to reset resources
    if (refreshData)
      mobiscrollTimeline.setOptions({resources: []});
    // get value from project radios
    projectsFilterCompletion = $('input[name=customRadioCompleted]:checked').val();
    projectsFilterPrefix = $('input[name=customRadioPrefix]:checked').val();

    // get all projects
    projectsMobiscroll = await getProjects(projectsFilterCompletion);
    // filter projects to active and with start and finish
    projectsMobiscroll = projectsMobiscroll.filter(p => p.active && p.start && p.finish);
    // filter projects to RT, STR and sorted them by their number
    let projectsRT = projectsMobiscroll.filter(p => p.project_number.substring(0,2) === 'RT');
    let projectsSTR = projectsMobiscroll.filter(p => p.project_number.substring(0,3) === 'STR');
    let projectsOther = projectsMobiscroll.filter(p => p.project_number.substring(0,2) !== 'RT' && p.project_number.substring(0,3) !== 'STR');
    // sort projects by their number in reverse order so newer projects are on top
    let sortedProjectsRT = projectsRT = projectsRT.sort((a, b) => b.project_number.localeCompare(a.project_number));
    let sortedProjectsSTR = projectsSTR = projectsSTR.sort((a, b) => b.project_number.localeCompare(a.project_number));
    
    // join RT, STR and other projects depending on prefix filter
    // filter projects by prefix
    if (projectsFilterPrefix == 1) projectsMobiscroll = sortedProjectsRT;
    else if (projectsFilterPrefix == 2) projectsMobiscroll = sortedProjectsSTR;
    else projectsMobiscroll = sortedProjectsRT.concat(sortedProjectsSTR, projectsOther);

    // filter exclusion projects if select not empty and radio customRadioExclusion is 0 if 1 then show only selected projects
    let exclusionProjects = $('#filterProjectsExclusion').val().map(x=>+x);
    if (exclusionProjects.length > 0 && $('input[name=customRadioExclusion]:checked').val() == 0) projectsMobiscroll = projectsMobiscroll.filter(p => !exclusionProjects.includes(p.id));
    else if (exclusionProjects.length > 0 && $('input[name=customRadioExclusion]:checked').val() == 1) projectsMobiscroll = projectsMobiscroll.filter(p => exclusionProjects.includes(p.id));

    // prepare resources for mobiscroll, every project is a resource
    projectsResource = projectsMobiscroll.map(p => { return { id: p.id, name: p.project_number + '-' + p.subscriber + '-' + p.project_name } });
    // add properties tp projectsMobiscroll for data, can also refresh timeline
    projectsMobiscroll = projectsMobiscroll.map(p => {
      p.end = p.finish;
      p.allDay = true;
      p.name = p.project_number + '-' + p.subscriber + '-' + p.project_name;
      p.title = p.project_number + '-' + p.subscriber + '-' + p.project_name;
      p.resource = p.id;
      p.color = p.completion == 100 ? '#4baf4f' : 'royalblue';
      return p;
    });
    // set resources on timeline, if refreshData is true then no need to set resources
    if (refreshData)
      mobiscrollTimeline.setOptions({resources: projectsResource});
    // set data on timeline
    mobiscrollTimeline.setEvents(projectsMobiscroll);
  }

  // FILTERING TASKS
  // check if any task categeory is selected
  if ($('#checkboxFilterCategory').find('.custom-control-input:checked').length > 0){
    // get all tasks inside current time frame
    let tmpS = moment(mobiscrollTimeline._firstDay).add(-1, 'days').format('YYYY-MM-DDTHH:mm:ss');
    let tmpE = moment(mobiscrollTimeline._lastDay).add(1, 'days').format('YYYY-MM-DDTHH:mm:ss');

    // add completion filter for tasks
    let completionFilter = $('input[name=tasksRadioCompleted]:checked').val();

    let tasks = await getTasks(tmpS, tmpE, completionFilter);
    // filter tasks to active and with start and finish
    tasks = tasks.filter(t => t.active && t.task_start && t.task_finish);
    // filter tasks based on selected categories
    let checkboxNabava = $('#checkboxNabava').is(':checked');
    let checkboxKonstrukcija = $('#checkboxKonstrukcija').is(':checked');
    let checkboxStrIzd = $('#checkboxStrIzd').is(':checked');
    let checkboxEleIzd = $('#checkboxEleIzd').is(':checked');
    let checkboxMontaza = $('#checkboxMontaza').is(':checked');
    let checkboxProg = $('#checkboxProg').is(':checked');
    let checkboxBrez = $('#checkboxBrez').is(':checked');
    let checkboxStrMonRoboteh = $('#checkboxStrMonRoboteh').is(':checked');
    let checkboxStrMonStranke = $('#checkboxStrMonStranke').is(':checked');
    let checkboxEleMonRoboteh = $('#checkboxEleMonRoboteh').is(':checked');
    let checkboxEleMonStranke = $('#checkboxEleMonStranke').is(':checked');
    let checkboxElePro = $('#checkboxElePro').is(':checked');
    let checkboxDobava = $('#checkboxDobava').is(':checked');
    let checkboxSAT = $('#checkboxSAT').is(':checked');
    let checkboxFAP = $('#checkboxFAP').is(':checked');
    let checkboxZagon = $('#checkboxZagon').is(':checked');
    let checkboxNaklad = $('#checkboxNaklad').is(':checked');

    // add filter for weekend, if checked then show only weekend tasks otherwise all tasks
    $('#filterTasksSuper').is(':checked') ? tasks = tasks.filter(t => t.weekend == true) : tasks = tasks;
    
    // filter tasks based on selected categories
    tasksMobiscroll = [];
    if (checkboxNabava) tasksMobiscroll = tasksMobiscroll.concat(tasks.filter(t => t.category.toLowerCase() == 'nabava'));
    if (checkboxKonstrukcija) tasksMobiscroll = tasksMobiscroll.concat(tasks.filter(t => t.category.toLowerCase() == 'konstrukcija'));
    if (checkboxStrIzd) tasksMobiscroll = tasksMobiscroll.concat(tasks.filter(t => t.category.toLowerCase() == 'strojna izdelava'));
    if (checkboxEleIzd) tasksMobiscroll = tasksMobiscroll.concat(tasks.filter(t => t.category.toLowerCase() == 'elektronska izdelava'));
    if (checkboxMontaza) tasksMobiscroll = tasksMobiscroll.concat(tasks.filter(t => t.category.toLowerCase() == 'montaža'));
    if (checkboxProg) tasksMobiscroll = tasksMobiscroll.concat(tasks.filter(t => t.category.toLowerCase() == 'programiranje'));
    if (checkboxBrez) tasksMobiscroll = tasksMobiscroll.concat(tasks.filter(t => t.category.toLowerCase() == 'brez'));
    if (checkboxStrMonRoboteh) tasksMobiscroll = tasksMobiscroll.concat(tasks.filter(t => t.category.toLowerCase() == 'strojna montaža - roboteh'));
    if (checkboxStrMonStranke) tasksMobiscroll = tasksMobiscroll.concat(tasks.filter(t => t.category.toLowerCase() == 'strojna montaža - stranka'));
    if (checkboxEleMonRoboteh) tasksMobiscroll = tasksMobiscroll.concat(tasks.filter(t => t.category.toLowerCase() == 'elektro montaža - roboteh'));
    if (checkboxEleMonStranke) tasksMobiscroll = tasksMobiscroll.concat(tasks.filter(t => t.category.toLowerCase() == 'elektro montaža - stranka'));
    if (checkboxElePro) tasksMobiscroll = tasksMobiscroll.concat(tasks.filter(t => t.category.toLowerCase() == 'elektro proektiranje'));
    if (checkboxDobava) tasksMobiscroll = tasksMobiscroll.concat(tasks.filter(t => t.category.toLowerCase() == 'dobava'));
    if (checkboxSAT) tasksMobiscroll = tasksMobiscroll.concat(tasks.filter(t => t.category.toLowerCase() == 'SAT'));
    if (checkboxFAP) tasksMobiscroll = tasksMobiscroll.concat(tasks.filter(t => t.category.toLowerCase() == 'FAT'));
    if (checkboxZagon) tasksMobiscroll = tasksMobiscroll.concat(tasks.filter(t => t.category.toLowerCase() == 'zagon'));
    if (checkboxNaklad) tasksMobiscroll = tasksMobiscroll.concat(tasks.filter(t => t.category.toLowerCase() == 'naklad'));

    // prepare tasks for data, they need to have resource and color
    tasksMobiscroll = tasksMobiscroll.map(t => {
      t.start = t.task_start;
      t.end = t.task_finish;
      t.allDay = true;
      t.name = t.task_name;
      t.title = t.task_name;
      t.resource = t.project_id;
      return t;
    });
    // add tasks to data projectsMobiscroll and show them on timeline
    joinData = projectsMobiscroll.concat(tasksMobiscroll);
    mobiscrollTimeline.setEvents(joinData);
  }

  // close modal for filtering
  $('#modalFilterTimeline').modal('hide');

}
// variable for projects visualization through mobiscroll
var projectsMobiscroll = [], tasksMobiscroll = [], datesMobiscroll = [], joinData = [];
var projectsFilterCompletion = 0; // 0 for not completed, 1 for completed, 2 or empty for all
var projectsFilterPrefix = 0; // 0 for all projects, 1 for RT projects, 2 for STR projects
var mobiscrollTimeline, colorType;
var projectsResource = [];
var projectsSelect;
var timelineRange, rangeButton, selectRange;
var sloLocal = {
  setText: "Potrdi",
  cancelText: "Prekliči",
  clearText: "Izbriši",
  closeText: "Zapri",
  selectedText: "{count} izbrano",
  dateFormat: "DD.MM.YYYY",
  dateFormatLong: "DDD, D. MMM. YYYY.",
  dateWheelFormat: "|DDD D MMM|",
  dayNames: ["Nedelja", "Ponedeljek", "Torek", "Sreda", "Četrtek", "Petek", "Sobota"],
  dayNamesShort: ["Ned", "Pon", "Tor", "Sre", "Čet", "Pet", "Sob"],
  dayNamesMin: ["Ne", "Po", "To", "Sr", "Če", "Pe", "So"],
  monthNames: ["Januar", "Februar", "Marec", "April", "Maj", "Junij", "Julij", "Avgust", "September", "Oktober", "November", "December"],
  monthNamesShort: ["Jan", "Feb", "Mar", "Apr", "Maj", "Jun", "Jul", "Avg", "Sep", "Okt", "Nov", "Dec"],
  timeFormat: "H:mm",
  nowText: "Zdaj",
  pmText: "pm",
  amText: "am",
  firstDay: 0,
  dateText: "Datum",
  timeText: "Čas",
  todayText: "Danes",
  prevMonthText: "Prejšnji mesec",
  nextMonthText: "Naslednji mesec",
  prevYearText: "Prejšnje leto",
  nextYearText: "Naslednje leto",
  eventText: "Dogodek",
  eventsText: "Dogodki",
  allDayText: "Ves dan",
  noEventsText: "Brez dogodkov",
  moreEventsText: "Še {count}",
  rangeStartLabel: "Začetek",
  rangeEndLabel: "Konec",
  rangeStartHelp: "Izberite",
  rangeEndHelp: "Izberite",
  filterEmptyText: "Brez rezultata",
  filterPlaceholderText: "Išči",
  weekText: '{count}. teden',
};