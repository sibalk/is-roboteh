//IN THIS SCRIPT//
//// opening modal for adding worker, adding worker, deleting worker ////

$(function(){
  //ADD WORKER TO THE PROJECT
  $('#addWorkerform').submit(function(e){
    e.preventDefault();
    $form = $(this);
    var worker = userAdd.value;
    var roleNew = roleAdd.value;
    if(isNaN(roleNew)){
      debugger;
      //new role, create role and then add worker with new role
      $.post('/employees/addprojectrole', {roleNew}, function(data){
        var roleName = roleNew;
        roleNew = data.id.id;
        allRoles.push({id:roleNew, text:roleName});
        debugger;
        $.post('/projects/worker/add', {worker, roleNew, projectId}, function(resp){
          $('#modalAddWorker').modal('toggle');
          var selectedWorker = allUsers.find(u => u.id === parseInt(worker))
          debugger
          if(allWorkers){
            var tmp = allWorkers.find(w => w.id == selectedWorker.id);
            if(!tmp){
              allWorkers.push({id:selectedWorker.id, text:selectedWorker.text});
              $(".select2-workersAdd").select2({
                data: allWorkers,
                tags: false,
                multiple: true,
              });
            }
            if(allWorkersTable){
              allWorkersTable.push({id_wp:resp.id.id, id:selectedWorker.id, workRole:roleName, role:null, name:selectedWorker.text.split(" ")[0], surname:selectedWorker.text.split(" ")[1]});
            }
          }
          let tmpRole = roleName.toLowerCase();
          var tmpImg = getIconImageName(tmpRole);
          //add to the list of workers on project
          var element = `<row class="list-group-item flex-column" id=projectWorker`+resp.id.id+`>
            <div class="row">
              <div class="col-12 col-sm-12 col-md-5 d-flex">
                <div class="mr-4" id="workerImg`+resp.id.id+`">
                  <img class="img-fluid w-50px" src="/`+tmpImg+`.png" alt="ikona">
                </div>
                <div class="ml-3 mt-1" id="workerInfo`+resp.id.id+`">
                  <div class="mb-1 row" id="workerName`+resp.id.id+`">`+selectedWorker.text+`</div>
                </div>
              </div>
              <div class="col-4 col-sm-4 col-md-3 mt-1 text-center">
                <div class="" id="workerRole`+resp.id.id+`">`+roleName+`</div>
              </div>
              <div class="col-12 col-sm-8 col-md-4 d-flex">
                <div class="ml-auto mt-2" id="workerControl`+resp.id.id+`">
                  <button class="btn btn-danger" data-toggle="tooltip" data-original-title="Odstrani">
                    <i class="fas fa-trash fa-lg"></i>
                  </button>
                  <a class="btn btn-programmer ml-3" href="/employees/absence?id=`+selectedWorker.id+`" data-toggle="tooltip" data-original-title="Odsotnosti">
                    <i class="fas fa-clock fa-lg"></i>
                  </a>
                  <a class="btn btn-info ml-3" href="/employees/tasks?userId=`+selectedWorker.id+`" data-toggle="tooltip" data-original-title="Opravila"> 
                    <i class="fas fa-angle-rught fa-lg"></i>
                  </a>
                  <a class="btn btn-primary ml-3" href="/employees/userId?userId=`+selectedWorker.id+`" data-toggle="tooltip" data-original-title="Projekti">
                    <i class="fas fa-angle-double-right fa-lg"></i>
                  </a>
                </div>
              </div>
            </div>
          </row>`;
          $('#workersList').append(element);
          //if I ever add tableWorkers + redo
          /*
          tableWorkers.row.add({
            id_wp: resp.id.id, 
            name: selectedWorker.text.split(' ')[0], 
            surname: selectedWorker.text.split(' ')[1], 
            workRole: roleName,
            id: worker
          }).draw(false)
          */
        })
      })
    }
    else{
      debugger;
      $.post('/projects/worker/add', {worker, roleNew, projectId}, function(resp){
        $('#modalAddWorker').modal('toggle');
        var selectedRole = allRoles.find(r => r.id === parseInt(roleNew))
        var selectedWorker = allUsers.find(u => u.id === parseInt(worker))
        debugger
        if(allWorkers){
          var tmp = allWorkers.find(w => w.id == selectedWorker.id);
          if(!tmp){
            allWorkers.push({id:selectedWorker.id, text:selectedWorker.text});
            $(".select2-workersAdd").select2({
              data: allWorkers,
              tags: false,
              multiple: true,
            });
          }
          if(allWorkersTable){
            allWorkersTable.push({id_wp:resp.id.id, id:selectedWorker.id, workRole:selectedRole.text, role:null, name:selectedWorker.text.split(" ")[0], surname:selectedWorker.text.split(" ")[1]});
          }
        }
        let tmpRole = selectedRole.text.toLowerCase();
        var tmpImg = getIconImageName(tmpRole);
        //add to the list of workers on project
        var element = `<row class="list-group-item flex-column" id=projectWorker`+resp.id.id+`>
          <div class="row">
            <div class="col-12 col-sm-12 col-md-5 d-flex">
              <div class="mr-4" id="workerImg`+resp.id.id+`">
                <img class="img-fluid w-50px" src="/`+tmpImg+`.png" alt="ikona">
              </div>
              <div class="ml-3 mt-1" id="workerInfo`+resp.id.id+`">
                <div class="mb-1 row" id="workerName`+resp.id.id+`">`+selectedWorker.text+`</div>
              </div>
            </div>
            <div class="col-4 col-sm-4 col-md-3 mt-1 text-center">
              <div class="" id="workerRole`+resp.id.id+`">`+selectedRole.text+`</div>
            </div>
            <div class="col-12 col-sm-8 col-md-4 d-flex">
              <div class="ml-auto mt-2" id="workerControl`+resp.id.id+`">
                <button class="btn btn-danger" data-toggle="tooltip" data-original-title="Odstrani">
                  <i class="fas fa-trash fa-lg"></i>
                </button>
                <a class="btn btn-programmer ml-3" href="/employees/absence?id=`+selectedWorker.id+`" data-toggle="tooltip" data-original-title="Odsotnosti"> 
                  <i class="fas fa-clock fa-lg"></i>
                </a>
                <a class="btn btn-info ml-3" href="/employees/tasks?userId=`+selectedWorker.id+`" data-toggle="tooltip" data-original-title="Opravila"> 
                  <i class="fas fa-angle-right fa-lg"></i>
                </a>
                <a class="btn btn-primary ml-3" href="/employees/userId?userId=`+selectedWorker.id+`" data-toggle="tooltip" data-original-title="Projekti">
                  <i class="fas fa-angle-double-right fa-lg"></i>
                </a>
              </div>
            </div>
          </div>
        </row>`;
        $('#workersList').append(element);
        //if I ever add tableWorkers + redo
        /*
        tableWorkers.row.add({
          id_wp: resp.id.id, 
          name: selectedWorker.text.split(' ')[0], 
          surname: selectedWorker.text.split(' ')[1], 
          workRole: selectedRole.text,
          id: worker
        }).draw(false)
        */
      })
    }
  })
  //call event functions
  $('#workersList').on('click', 'button', function (event) {
    //debugger
    deleteWorker(this.parentElement.id.substring(13));
    //deleteWorker(worker.id_wp)
  });
  $('#addWorkerBtn').on('click', openWorkerModal);
  $('#btnDeleteWorkerConf').on('click', deleteWorkerConfirm);
})

//OPEN MODAL TO ADD WORKER TO PROJECT
function openWorkerModal(){
  debugger;
  if(!allUsers || !allRoles){
    $.get('/projects/users', function(resp){
      allUsers = resp.data
      debugger
      $(".select2-users").select2({
        data: allUsers,
        tags: false,
        dropdownParent: $('#modalAddWorker'),
      });
      $.get('/projects/roles', function(resp){
        allRoles = resp.data
        debugger
        $(".select2-roles").select2({
          data: allRoles,
          tags: false,
          dropdownParent: $('#modalAddWorker'),
        });
        var tmp = allUsers[0].role;
        tmp = allRoles.find(r => r.text == tmp);
        if(tmp){
          $('#roleAdd').val(tmp.id).trigger('change')
        }
        $('#userAdd').on('change', changeUserAddRole);
        $("#modalAddWorker").modal();
      })
    })
  }
  else{
    $("#modalAddWorker").modal();
  }
}
//REMOVE WORKER FROM PROJECT
function deleteWorker(id){
  savedWorkerId = id;
  $("#modalDeleteWorker").modal('toggle');
}
function deleteWorkerConfirm(){
  var id = savedWorkerId;
  //debugger;
  //console.log(tableWorkers.row(id))
  $.post('/projects/worker/remove', {id}, function(resp){
    if(resp.success){
      console.log('Successfully deleting worker from project.');
      $("#modalDeleteWorker").modal('toggle');
      var workerName = $('#workerName'+id).html();
      debugger
      if(allWorkers){
        var deletingWorker = allWorkers.find(w => w.text == workerName);
        if(!allWorkersTable){
          $.get('/projects/worker/get', {projectId}, function(resp){
            allWorkersTable = resp.data;
            var tmp = allWorkersTable.find(w => w.id == deletingWorker.id);
            if(!tmp){
              allWorkers = allWorkers.filter(w => w.id != deletingWorker.id)
              $(".select2-workersAdd").select2({
                data: allWorkers,
                tags: false,
                multiple: true,
              });
            }
          })  
        }
        else{
          allWorkersTable = allWorkersTable.filter(w => w.id_wp != id);
          var tmp = allWorkersTable.find(w => w.id == deletingWorker.id);
          if(!tmp){
            allWorkers = allWorkers.filter(w => w.id != deletingWorker.id)
            $(".select2-workersAdd").select2({
              data: allWorkers,
              tags: false,
              multiple: true,
            });
          }
        }
      }
      $('#projectWorker'+id).remove();
    }
    else{
      console.log('Unsuccessfully deleting worker from project.');
      $("#modalDeleteWorker").modal('toggle');
      $('#modalError').modal('toggle');
    }
  })
}
function changeUserAddRole(){
  var selectedWorker = $('#userAdd').val();
  var tmp = allUsers.find(u => u.id == selectedWorker).role;
  tmp = allRoles.find(r => r.text == tmp);
  if(tmp){
    $('#roleAdd').val(tmp.id).trigger('change');
  }
  else if(allUsers.find(u => u.id == selectedWorker).role == 'vodja projektov'){
    tmp = allRoles.find(r => r.text == 'vodja projekta');
    $('#roleAdd').val(tmp.id).trigger('change');
  }
}
function getIconImageName(role){
  if(role == 'strojnik')
    return 'mechanic64';
  else if(role == 'električar')
    return 'electrican64';
  else if(role == 'konstruktor' || role == 'konstrukter')
    return 'builder64';
  else if(role == 'programer plc-jev')
    return 'plc64';
  else if(role == 'programer robotov')
    return 'robot64';
  else if(role == 'vodja' || role == 'vodja projekta' || role == 'vodja projektov')
    return 'manager64';
  else if(role == 'vodja strojnikov')
    return 'managerMech64';
  else if(role == 'vodja električarjev')
    return 'managerElec64';
  else if(role == 'vodja CNC obdelave')
    return 'managerCNC64';
  else if(role == 'vodja programerjev')
    return 'managerPLC64';
  else if(role == 'nabavnik' || role == 'komercialist' || role == 'komercijalist' || role == 'tajnik' || role == 'tajnica' || role == 'komerciala' || role == 'računovodstvo')
    return 'secratery64';
  else if(role == 'varilec')
    return 'welder02';
  else if(role == 'serviser')
    return 'serviser'
  else if(role == 'cnc' || role == 'cnc operater')
    return 'cnc64';
  else if(role == 'student' || role == 'študent')
    return 'student03';
  else return 'worker64';
}
var allUsers;
var allRoles;
var savedWorkerId;