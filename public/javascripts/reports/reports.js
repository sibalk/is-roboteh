//IN THIS SCRIPT//
//// refresh changes body, drawing changes in body, draw changes based on input in control section ////

$(function(){
	setTimeout(()=>{

		//get user info from page
		loggedUser = {name:$('#loggedUser').html().split(" (")[0].split(" ")[0], surname:$('#loggedUser').html().split(" (")[0].split(" ")[1], role:$('#loggedUser').html().split(" (")[1].split(")")[0].toLowerCase()};
		activeUserRole = loggedUser.role;
		$.get( "/projects/users", function( data ) {
			allUsers = data.data;
			$(".select2-users").select2({
				data: allUsers,
			});
			$(".select2-new-report-users").select2({
				data: allUsers,
			});
			$(".select2-edit-report-users").select2({
				data: allUsers,
			});
			if(loggedUser.role == 'admin' || loggedUser.role == 'tajnik' || loggedUser.role == 'komercialist'  || loggedUser.role == 'komerciala' || loggedUser.role == 'računovodstvo' || loggedUser.role.substring(0,5) == 'vodja'){
				userId = allUsers.find(u => u.text == (loggedUser.name+" "+loggedUser.surname)).id;
				reportsUserId = userId;
				$('#userSelect').val(userId).trigger('change');
				//$('#newReportUser').val(userId).trigger('change');
			}
		});
		$.get( "/reports/types", function( data ) {
			allTypes = data.data;
			$(".select2-new-report-type").select2({
				data: allTypes,
				placeholder: "Tip poročila",
				tags: true
			});
			allTypes.unshift({id:0, text: "Vsi"});
			$(".select2-type").select2({
				data: allTypes,
				placeholder: "Tip poročila",
			});
		});
		$.get( "/reports/projects", function( data ) {
			allProjects = data.data;
			$(".select2-new-report-projects").select2({
				data: allProjects,
				search: false,
			});
			allProjects.unshift({id:0, text: "Vsi"});
			$(".select2-projects").select2({
				data: allProjects,
				search: false,
			});
		});
		setTimeout(()=>{
			$('#report_start').val(moment().format("YYYY-MM-DD"));
			$('#report_finish').val(moment().format("YYYY-MM-DD"));
			$('#newReportDate').val(moment().format("YYYY-MM-DD"));
	
			getReports();
		},100);
		$('#newReportOtherForm').hide();
		$('#newReportRadioOther').on('change', changeRadioOtherNew);
		$('#newReportRadioProject').on('change', changeRadioProjectNew);
		$('#editReportOtherForm').hide();
		$('#editReportRadioOther').on('change', changeRadioOtherEdit);
		$('#editReportRadioProject').on('change', changeRadioProjectEdit);
		$('#inputReportOtherForm').hide();
		$('#radioOther').on('change', changeRadioOtherInput);
		$('#radioProject').on('change', changeRadioProjectInput);
		$('#userSelect').on('change', changeNewReportUser);
		$('#newReportStart').on('change', calcNewTime);
		$('#newReportFinish').on('change', calcNewTime);
		$('#editReportStart').on('change', calcEditTime);
		$('#editReportFinish').on('change', calcEditTime);
		//add new report
		$('#addNewReportForm').submit(function(e){
			e.preventDefault();
			$form = $(this);
			
			//debugger;
			var userId = null;
			if(loggedUser.role == 'admin' || loggedUser.role == 'tajnik' || loggedUser.role == 'komercialist'  || loggedUser.role == 'komerciala' || loggedUser.role == 'računovodstvo' || loggedUser.role.substring(0,5) == 'vodja')
				userId = newReportUser.value;
			var date = newReportDate.value;
			var time = newReportTime.value;
			var start = newReportStart.value;
			var finish = newReportFinish.value;
			var projectId = newReportProject.value;
			var other = newReportOther.value;
			var description = newReportDescription.value;
			var type = newReportType.value;
			var radio = $('input[name=customRadio]:checked', '#addNewReportForm').val();
			if(!time){
				//alert('Opravljen čas je brez pravilnega vnosa')
				//$('#newReportTime').addClass('is-invalid');
			}
			else{
				//$('#newReportTime').removeClass('is-invalid');
				if(isNaN(type)){
					//new type, add new type and on success create new report
					$.post('/reports/types', {type}, function(resp){
						if(resp.success){
							console.log("Uspešno dodajanje novega tipa poročila.");
							var tmpType = ({id:resp.type.id,text:type});
							type = tmpType.id;
							$.post('/reports/reports', {userId,date,time,start,finish,projectId,other,description,type,radio}, function(resp){
								if(resp.success){
									console.log("Uspešno dodajanje novega poročila.");
									//add new report to list
									//project and other prepare
									if(radio == '0'){
										tmpProject = allProjects.find(p => p.id == projectId);
										tmpProjectName = tmpProject.text.split(' - ')[1];
										tmpProjectNumber = tmpProject.text.split(' - ')[0];
										other == null; 
									}
									else{
										tmpProject = {id:null,text:null};
										tmpProjectName = null;
										tmpProjectNumber = null;
									}
									//type
									tmpReportType = tmpType;
									//created
									var today = new Date().toISOString();
									//formatted time
									var formatted_time = {hours:time.split(':')[0], minutes:time.split(':')[1]};
									//formatted date
									var newDate = date.split('-');
									var formatted_date = {date:newDate[2]+'.'+newDate[1]+'.'+newDate[0],time:'00:00'}
									//together
									var newReport = ({
										id: resp.report.id,
										project_id: tmpProject.id,
										project_name: tmpProjectName,
										project_number: tmpProjectNumber,
										subscriber_id: null,
										name: null,
										date: date,
										time: time,
										start: start,
										finish: finish,
										description: description,
										report_type_id: tmpReportType.id,
										report_type: tmpReportType.text,
										location: null,
										files: null,
										active: true,
										ma: false,
										created: today,
										other: other,
										formatted_date: formatted_date,
										formatted_time: formatted_time,
									})
									allReports.unshift(newReport);
									var tmpInfo = '';
									if(newReport.project_id)
										tmpInfo = newReport.project_number+' - '+newReport.project_name;
									else
										tmpInfo = newReport.other;
									var reportElement = `<div class="card card-report mb-3" id="report`+newReport.id+`">
										<div class="card-header">
											<div class="alert-dissmisable">
												<div class="close text-dark" id="reportTime`+newReport.id+`">`+newReport.formatted_time.hours+`h `+newReport.formatted_time.minutes+`m</div>
												<div id="reportInfo`+newReport.id+`">`+tmpInfo+`</div>
											</div>
										</div>
										<div class="card-body">
											<h5 class="card-title pre-wrap" id="reportDescription`+newReport.id+`">`+newReport.description+`</h5>
											<p class="card-text" id="reportType`+newReport.id+`">`+newReport.report_type+`</p>
										</div>
										<div class="card-subtitle ml-auto mr-2 mt-n4" id="reportDate`+newReport.id+`">`+newReport.formatted_date.date+`</div>
									</div>`;
									if(loggedUser.role == 'admin' || loggedUser.role == 'tajnik' || loggedUser.role == 'komercialist' || loggedUser.role == 'komerciala' || loggedUser.role == 'računovodstvo' || loggedUser.role.substring(0,5) == 'vodja'){
										if(userId == reportsUserId)
											$('#reportBody').prepend(reportElement);	
									}
									else
										$('#reportBody').prepend(reportElement);
									//add on click event to new report
									$('#report'+newReport.id).on('mouseup', {id:newReport.id}, onClickEditReport);
									$('#modalAddNewReport').modal('toggle');
									$('#newReportDescription').val('');
									$('#newReportOther').val('');
									addChangeSystem(1,19,null,null,null,null,null,null,null,null,null,null,null,null,null,newReport.id);
								}
								else
									console.log("Pri dodajanju novega poročila je prišlo do napake.");
								//fix select2 for report type
								allTypes.shift();
								allTypes.push(tmpType);
								$(".select2-new-report-type").select2({
									data: allTypes,
									placeholder: "Tip poročila",
									tags: true
								});
								allTypes.unshift({id:0, text: "Vsi"});
								$(".select2-type").select2({
									data: allTypes,
									placeholder: "Tip poročila",
									tags: true
								});
							})
						}
						else
							console.log("Pri dodajanju novega tipa poročila je prišlo do napake.");
					})
				}
				else{
					//no new type, create new report
					$.post('/reports/reports', {userId,date,time,start,finish,projectId,other,description,type,radio}, function(resp){
						if(resp.success){
							console.log("Uspešno dodajanje novega poročila.");
							//add new report to list
							//project and other prepare
							if(radio == '0'){
								tmpProject = allProjects.find(p => p.id == projectId);
								tmpProjectName = tmpProject.text.split(' - ')[1];
								tmpProjectNumber = tmpProject.text.split(' - ')[0];
								other == null; 
							}
							else{
								tmpProject = {id:null,text:null};
								tmpProjectName = null;
								tmpProjectNumber = null;
							}
							//type
							tmpReportType = allTypes.find(t => t.id == type);
							//created
							var today = new Date().toISOString();
							//formatted time
							var formatted_time = {hours:time.split(':')[0], minutes:time.split(':')[1]};
							//formatted date
							var newDate = date.split('-');
							var formatted_date = {date:newDate[2]+'.'+newDate[1]+'.'+newDate[0],time:'00:00'}
							//together
							var newReport = ({
								id: resp.report.id,
								project_id: tmpProject.id,
								project_name: tmpProjectNumber,
								project_number: tmpProjectNumber,
								subscriber_id: null,
								name: null,
								date: date,
								time: time,
								start: start,
								finish: finish,
								description: description,
								report_type_id: tmpReportType.id,
								report_type: tmpReportType.text,
								location: null,
								files: null,
								active: true,
								ma: false,
								created: today,
								other: other,
								formatted_date: formatted_date,
								formatted_time: formatted_time,
							})
							allReports.unshift(newReport);
							var tmpInfo = '';
							if(newReport.project_id)
								tmpInfo = newReport.project_number+' - '+newReport.project_name;
							else
								tmpInfo = newReport.other;
							var reportElement = `<div class="card card-report mb-3" id="report`+newReport.id+`">
								<div class="card-header">
									<div class="alert-dissmisable">
										<div class="close text-dark" id="reportTime`+newReport.id+`" >`+newReport.formatted_time.hours+`h `+newReport.formatted_time.minutes+`m</div>
										<div id="reportInfo`+newReport.id+`">`+tmpInfo+`</div>
									</div>
								</div>
								<div class="card-body">
									<h5 class="card-title pre-wrap" id="reportDescription`+newReport.id+`">`+newReport.description+`</h5>
									<p class="card-text" id="reportType`+newReport.id+`">`+newReport.report_type+`</p>
								</div>
								<div class="card-subtitle ml-auto mr-2 mt-n4" id="reportDate`+newReport.id+`">`+newReport.formatted_date.date+`</div>
							</div>`;
							if(loggedUser.role == 'admin' || loggedUser.role == 'tajnik' || loggedUser.role == 'komercialist' || loggedUser.role == 'komerciala' || loggedUser.role == 'računovodstvo' || loggedUser.role.substring(0,5) == 'vodja'){
								if(userId == reportsUserId)
									$('#reportBody').prepend(reportElement);	
							}
							else
								$('#reportBody').prepend(reportElement);
							//add on click event to new report
							$('#report'+newReport.id).on('mouseup', {id:newReport.id}, onClickEditReport);
							$('#modalAddNewReport').modal('toggle');
							$('#newReportDescription').val('');
							$('#newReportOther').val('');
							addChangeSystem(1,19,null,null,null,null,null,null,null,null,null,null,null,null,null,newReport.id);
						}
						else
							console.log("Pri dodajanju novega poročila je prišlo do napake.");
					})
				}
			}
		})
		//edit report
		$('#editReportForm').submit(function(e){
			e.preventDefault();
			$form = $(this);
			
			//debugger;
			var userId = null;
			if(loggedUser.role == 'admin' || loggedUser.role == 'tajnik' || loggedUser.role == 'komercialist' || loggedUser.role == 'komerciala' || loggedUser.role == 'računovodstvo' || loggedUser.role.substring(0,5) == 'vodja')
				userId = editReportUser.value;
			var date = editReportDate.value;
			var time = editReportTime.value;
			var start = editReportStart.value;
			var finish = editReportFinish.value;
			var projectId = editReportProject.value;
			var other = editReportOther.value;
			var description = editReportDescription.value;
			var type = editReportType.value;
			var radio = $('input[name=customRadioEdit]:checked', '#editReportForm').val();
			if(!time){
				//alert('Opravljen čas je brez pravilnega vnosa')
				//$('#editReportTime').addClass('is-invalid');
			}
			else{
				//$('#editReportTime').removeClass('is-invalid');
				if(isNaN(type)){
					//new type, add new type and on success create new report
					$.post('/reports/types', {type}, function(resp){
						if(resp.success){
							console.log("Uspešno dodajanje novega tipa poročila.");
							var tmpType = ({id:resp.type.id,text:type});
							type = tmpType.id;
							$.post('/reports/edit', {reportId,userId,date,time,start,finish,projectId,other,description,type,radio}, function(resp){
								if(resp.success){
									console.log("Uspešno posodobitev poročila.");
									//add new report to list
									//project and other prepare
									if(radio == '0'){
										tmpProject = allProjects.find(p => p.id == projectId);
										tmpProjectName = tmpProject.text.split(' - ')[1];
										tmpProjectNumber = tmpProject.text.split(' - ')[0];
										other == null; 
									}
									else{
										tmpProject = {id:null,text:null};
										tmpProjectName = null;
										tmpProjectNumber = null;
									}
									//type
									tmpReportType = tmpType;
									//created
									var today = new Date().toISOString();
									//formatted time
									var formatted_time = {hours:time.split(':')[0], minutes:time.split(':')[1]};
									//formatted date
									var newDate = date.split('-');
									var formatted_date = {date:newDate[2]+'.'+newDate[1]+'.'+newDate[0],time:'00:00'}
									//together
									tmpReport = allReports.find(r => r.id == reportId);
									tmpReport.project_id = tmpProject.id;
									tmpReport.project_name = tmpProjectName;
									tmpReport.project_number = tmpProjectNumber;
									tmpReport.date = date;
									tmpReport.time = time;
									tmpReport.start = start;
									tmpReport.finish = finish;
									tmpReport.description = description;
									tmpReport.report_type_id = tmpReportType.id;
									tmpReport.report_type = tmpReportType.text;
									tmpReport.other = other;
									tmpReport.formatted_date = formatted_date;
									tmpReport.formatted_time = formatted_time;
									var tmpInfo = '';
									if(tmpReport.project_id)
										tmpInfo = tmpReport.project_number+' - '+tmpReport.project_name;
									else
										tmpInfo = tmpReport.other;
									$('#reportTime'+reportId).html(tmpReport.formatted_time.hours+'h '+tmpReport.formatted_time.minutes+'m');
									$('#reportInfo'+reportId).html(tmpInfo);
									$('#reportDescription'+reportId).html(tmpReport.description);
									$('#reportType'+reportId).html(tmpReport.report_type);
									$('#reportDate'+reportId).html(tmpReport.formatted_date.date);
									$('#modalEditReport').modal('toggle');
									addChangeSystem(2,19,null,null,null,null,null,null,null,null,null,null,null,null,null,reportId);
								}
								else
									console.log("Pri dodajanju novega poročila je prišlo do napake.");
								//fix select2 for report type
								allTypes.shift();
								allTypes.push(tmpType);
								$(".select2-new-report-type").select2({
									data: allTypes,
									placeholder: "Tip poročila",
									tags: true
								});
								allTypes.unshift({id:0, text: "Vsi"});
								$(".select2-type").select2({
									data: allTypes,
									placeholder: "Tip poročila",
									tags: true
								});
							})
						}
						else
							console.log("Pri dodajanju novega tipa poročila je prišlo do napake.");
					})
				}
				else{
					//no new type, create new report
					$.post('/reports/edit', {reportId,userId,date,time,start,finish,projectId,other,description,type,radio}, function(resp){
						if(resp.success){
							console.log("Uspešno posodobitev poročila.");
							//add new report to list
							//project and other prepare
							if(radio == '0'){
								tmpProject = allProjects.find(p => p.id == projectId);
								tmpProjectName = tmpProject.text.split(' - ')[1];
								tmpProjectNumber = tmpProject.text.split(' - ')[0];
								other == null; 
							}
							else{
								tmpProject = {id:null,text:null};
								tmpProjectName = null;
								tmpProjectNumber = null;
							}
							//type
							tmpReportType = allTypes.find(t => t.id == type);
							//created
							var today = new Date().toISOString();
							//formatted time
							var formatted_time = {hours:time.split(':')[0], minutes:time.split(':')[1]};
							//formatted date
							var newDate = date.split('-');
							var formatted_date = {date:newDate[2]+'.'+newDate[1]+'.'+newDate[0],time:'00:00'}
							//together
							tmpReport = allReports.find(r => r.id == reportId);
							tmpReport.project_id = tmpProject.id;
							tmpReport.project_name = tmpProjectName;
							tmpReport.project_number = tmpProjectNumber;
							tmpReport.date = date;
							tmpReport.time = time;
							tmpReport.start = start;
							tmpReport.finish = finish;
							tmpReport.description = description;
							tmpReport.report_type_id = tmpReportType.id;
							tmpReport.report_type = tmpReportType.text;
							tmpReport.other = other;
							tmpReport.formatted_date = formatted_date;
							tmpReport.formatted_time = formatted_time;
							var tmpInfo = '';
							if(tmpReport.project_id)
								tmpInfo = tmpReport.project_number+' - '+tmpReport.project_name;
							else
								tmpInfo = tmpReport.other;
							$('#reportTime'+reportId).html(tmpReport.formatted_time.hours+'h '+tmpReport.formatted_time.minutes+'m');
							$('#reportInfo'+reportId).html(tmpInfo);
							$('#reportDescription'+reportId).html(tmpReport.description);
							$('#reportType'+reportId).html(tmpReport.report_type);
							$('#reportDate'+reportId).html(tmpReport.formatted_date.date);
							$('#modalEditReport').modal('toggle');
							addChangeSystem(2,19,null,null,null,null,null,null,null,null,null,null,null,null,null,reportId);
						}
						else
							console.log("Pri dodajanju novega poročila je prišlo do napake.");
					})
				}
			}
		})
		//get reports based on search input
		$('#getReportsForm').submit(function(e){
			e.preventDefault();
			$form = $(this);
	
			var userId = null;
			if(loggedUser.role == 'admin' || loggedUser.role == 'tajnik' || loggedUser.role == 'komercialist' || loggedUser.role == 'komerciala' || loggedUser.role == 'računovodstvo' || loggedUser.role.substring(0,5) == 'vodja')
				userId = userSelect.value;
			var dateStart = report_start.value;
			var dateFinish = report_finish.value;
			var radio = $('input[name=customRadioInput]:checked', '#getReportsForm').val();
			var projectId = project_select.value;
			var other = otherInput.value;
			var reportType = report_type.value;
			$.get( "/reports/reports",{userId,dateStart,dateFinish,radio,projectId,other,reportType}, function( resp ) {
				if(resp.success){
					if(loggedUser.role == 'admin' || loggedUser.role == 'tajnik' || loggedUser.role == 'komercialist' || loggedUser.role == 'komerciala' || loggedUser.role == 'računovodstvo' || loggedUser.role.substring(0,5) == 'vodja')
						reportsUserId = userId;
					//debugger;
					console.log('Uspešno pridobivanje podatkov');
					allReports = resp.data;
					drawReports();
				}
			});
		})
	},200)
	//event handlers
	$('#monthlyReportsButton').on('click', monthlyReportMode);
	$('#dailyReportsButton').on('click', dailyReportMode);
	$('#addReportButton').on('click', addNewReport);
	$('#btnDeleteReport').on('click', deleteReportConfirm);
	$('#reportDeleteButton').on('click', openDeleteModal);
})
function drawReports(){
	$('#reportBody').empty();
	for(var i = 0; i < allReports.length; i++){
		var tmpInfo = '';
		if(allReports[i].project_id)
			tmpInfo = allReports[i].project_number+' - '+allReports[i].project_name;
		else
			tmpInfo = allReports[i].other;
		var activeStyle = '';
		if(activeUserRole == 'admin' && allReports[i].active == false){
			activeStyle = 'bg-secondary';
		}
		var reportElement = `<div class="card card-report mb-3 `+activeStyle+`" id="report`+allReports[i].id+`">
			<div class="card-header">
				<div class="alert-dissmisable">
					<div class="close text-dark" id="reportTime`+allReports[i].id+`">`+allReports[i].formatted_time.hours+`h `+allReports[i].formatted_time.minutes+`m</div>
					<div id="reportInfo`+allReports[i].id+`">`+tmpInfo+`</div>
				</div>
			</div>
			<div class="card-body">
				<h5 class="card-title pre-wrap" id="reportDescription`+allReports[i].id+`">`+allReports[i].description+`</h5>
				<p class="card-text" id="reportType`+allReports[i].id+`">`+allReports[i].report_type+`</p>
			</div>
			<div class="card-subtitle ml-auto mr-2 mt-n4" id="reportDate`+allReports[i].id+`">`+allReports[i].formatted_date.date+`</div>
		</div>`;
		if(allReports[i].active == true)
			$('#reportBody').append(reportElement);
		else if(activeUserRole == 'admin')
			$('#reportBody').append(reportElement);
		//add on click event to drawn report
		$('#report'+allReports[i].id).on('mouseup', {id:allReports[i].id}, onClickEditReport);
	}
}
function getReports(){
	$.get( "/reports/reports",{userId}, function( data ) {
		allReports = data.data;
		drawReports();
	});
}
function addNewReport(){
	$('#modalAddNewReport').modal('toggle');
}
function onClickEditReport(event) {
	if(event.which == 1)
		editReport(event.data.id);
}
function editReport(id){
	reportId = id;
	tmpReport = allReports.find(r => r.id == reportId);
	if(loggedUser.role == 'admin' || loggedUser.role == 'tajnik' || loggedUser.role == 'komercialist' || loggedUser.role == 'komerciala' || loggedUser.role == 'računovodstvo' || loggedUser.role.substring(0,5) == 'vodja'){
		$('#editReportUser').val(reportsUserId).trigger('change');
	}
	var date = tmpReport.formatted_date.date.split('.');
	$('#editReportDate').val(date[2]+'-'+date[1]+'-'+date[0]);
	$('#editReportTime').val(tmpReport.time);
	$('#editReportStart').val(tmpReport.start);
	$('#editReportFinish').val(tmpReport.finish);
	$('#editReportType').val(tmpReport.report_type_id).trigger('change');
	$('#editReportDescription').val(tmpReport.description);
	if(tmpReport.project_id){
		$('#editReportProject').val(tmpReport.project_id).trigger('change');
		$('#editReportOther').val('');
		$('#editReportProjectForm').show();
		$('#editReportOtherForm').hide();
		$('#editReportOther').prop('required', false);
		$('#editReportRadioProject').prop('checked', true);
		$('#editReportRadioOther').prop('checked', false);
	}
	else{
		$('#editReportOther').val(tmpReport.other);
		$('#editReportProjectForm').hide();
		$('#editReportOtherForm').show();
		$('#editReportOther').prop('required', true);
		$('#editReportRadioProject').prop('checked', false);
		$('#editReportRadioOther').prop('checked', true);
	}
	calcEditTime();
	//change icon to delete or undelete
	if(tmpReport.active){
		$('#reportDeleteButton').find('i').removeClass('fa-trash-restore');
		$('#reportDeleteButton').find('i').addClass('fa-trash');
	}
	else{
		$('#reportDeleteButton').find('i').removeClass('fa-trash');
		$('#reportDeleteButton').find('i').addClass('fa-trash-restore');
	}
	$('#modalEditReport').modal('toggle');
}
function changeRadioOtherNew(){
	//console.log('test menjava na drugo');
	$('#newReportProjectForm').hide();
	$('#newReportOtherForm').show();
	$('#newReportOther').prop('required', true);
}
function changeRadioProjectNew(){
	//console.log('test menjava na projekt');
	$('#newReportProjectForm').show();
	$('#newReportOtherForm').hide();
	$('#newReportOther').prop('required', false);
}
function changeRadioOtherInput(){
	//console.log('test menjava na drugo');
	$('#inputReportProjectForm').hide();
	$('#inputReportOtherForm').show();
}
function changeRadioProjectInput(){
	//console.log('test menjava na drugo');
	$('#inputReportOtherForm').hide();
	$('#inputReportProjectForm').show();
}
function changeRadioOtherEdit(){
	//console.log('test menjava na drugo');
	$('#editReportProjectForm').hide();
	$('#editReportOtherForm').show();
	$('#editReportOther').prop('required', true);
}
function changeRadioProjectEdit(){
	//console.log('test menjava na drugo');
	$('#editReportOtherForm').hide();
	$('#editReportProjectForm').show();
	$('#editReportOther').prop('required', false);
}
function changeNewReportUser(){
	$('#newReportUser').val($('#userSelect').val()).trigger('change');
}
function calcNewTime(){
	var start = $('#newReportStart').val();
	var finish = $('#newReportFinish').val();
	if(start && finish){
		var ary1=start.split(':'),ary2=finish.split(':');
		var minsdiff=parseInt(ary2[0],10)*60+parseInt(ary2[1],10)-parseInt(ary1[0],10)*60-parseInt(ary1[1],10);
		//console.log(String(100+Math.floor(minsdiff/60)).substr(1)+':'+String(100+minsdiff%60).substr(1));
		$('#newReportTime').val(String(100+Math.floor(minsdiff/60)).substr(1)+':'+String(100+minsdiff%60).substr(1));
		if(minsdiff < 0){
			$('#newReportTime').addClass('is-invalid');
			$('#btnAddReport').prop('disabled', true);
		}
		else{
			$('#newReportTime').removeClass('is-invalid');
			$('#btnAddReport').prop('disabled', false);
		}
	}
	else{
		$('#newReportTime').val('');
		$('#newReportTime').addClass('is-invalid');
		$('#btnAddReport').prop('disabled', true);
	}
}
function calcEditTime(){
	var start = $('#editReportStart').val();
	var finish = $('#editReportFinish').val();
	if(start && finish){
		var ary1=start.split(':'),ary2=finish.split(':');
		var minsdiff=parseInt(ary2[0],10)*60+parseInt(ary2[1],10)-parseInt(ary1[0],10)*60-parseInt(ary1[1],10);
		//console.log(String(100+Math.floor(minsdiff/60)).substr(1)+':'+String(100+minsdiff%60).substr(1));
		$('#editReportTime').val(String(100+Math.floor(minsdiff/60)).substr(1)+':'+String(100+minsdiff%60).substr(1));
		if(minsdiff < 0){
			$('#editReportTime').addClass('is-invalid');
			$('#btnEditReport').prop('disabled', true);
		}
		else{
			$('#editReportTime').removeClass('is-invalid');
			$('#btnEditReport').prop('disabled', false);
		}
	}
	else{
		$('#editReportTime').val('');
		$('#editReportTime').addClass('is-invalid');
		$('#btnEditReport').prop('disabled', true);
	}
}
//for delete prompt
function openDeleteModal(){
	var tmpReport = allReports.find(r => r.id == reportId)
	if(tmpReport.active){
		$('#btnDeleteReport').html('Odstrani');
		$('#deleteModalMsg').html('Ste prepričani, da želite odstraniti to poročilo?');
	}
	else{
		$('#btnDeleteReport').html('Ponovno dodaj');
		$('#deleteModalMsg').html('Ste prepričani, da želite ponovno dodati izbrisano poročilo?');
	}
	$('#modalEditReport').modal('toggle');
	$('#modalDeleteReport').modal('toggle');
}
//delete call to server
function deleteReportConfirm(){
	var tmpReport = allReports.find(r => r.id == reportId);
	var active = tmpReport.active;
	if(active == true) active = false; else active = true;
	//ajax call to server & on success do:
	$.post('/reports/delete', {reportId,active}, function(resp){
		if(resp.success){
			console.log('Successfully deleting report.');
			if(activeUserRole == 'admin'){
				//open edit modal back again and fix button icon for delete
				if(active == true){
					//$('#reportDeleteButton').find('img').attr('src', '/delete.png');
					$('#reportDeleteButton').find('i').removeClass('fa-trash-restore');
					$('#reportDeleteButton').find('i').addClass('fa-trash');
					$('#report'+reportId).removeClass('bg-secondary');
					addChangeSystem(6,19,null,null,null,null,null,null,null,null,null,null,null,null,null,reportId);
				}
				else{
					//$('#reportDeleteButton').find('img').attr('src', '/undelete1.png');
					$('#reportDeleteButton').find('i').removeClass('fa-trash');
					$('#reportDeleteButton').find('i').addClass('fa-trash-restore');
					$('#report'+reportId).addClass('bg-secondary');
					addChangeSystem(3,19,null,null,null,null,null,null,null,null,null,null,null,null,null,reportId);
				}
				$('#modalDeleteReport').modal('toggle');
				$('#modalEditReport').modal('toggle');
			}
			else{
				//close delete prompt modal and remove deleted report from list of reports
				$('#modalDeleteReport').modal('toggle');
				$('#report'+reportId).remove();
				addChangeSystem(3,19,null,null,null,null,null,null,null,null,null,null,null,null,null,reportId);
			}
			//fix active on local side
			tmpReport.active = active;
		}
		else{
			console.log('Unsuccessfully deleting report.');
		}
	})
}
//////////////////////////////////for monthly reports
function monthlyReportMode(){
	$('#searchReportsButton').prop('disabled', true);
	$('#addReportButton').prop('disabled', true);
	$('#dailyReportsButton').removeClass('active');
	$('#monthlyReportsButton').addClass('active');
	$('#reportBody').empty();
	$(pdfMonthButton).empty();
	$.get( "/reports/monthly", function( data ) {
		var dates = data.dates;
		var frame = `<div class="list-group" id="monthlyList">
		</div>`;
		$('#reportBody').append(frame);
		for(var i = 0; i < data.dates.length; i++){
			var monthName;
			switch (data.dates[i].month) {
				case 2: monthName = 'Februar'; break;
				case 3: monthName = 'Marec'; break;
				case 4: monthName = 'April'; break;
				case 5: monthName = 'Maj'; break;
				case 6: monthName = 'Junij'; break;
				case 7: monthName = 'Julij'; break;
				case 8: monthName = 'Avgust'; break;
				case 9: monthName = 'September'; break;
				case 10: monthName = 'Oktober'; break;
				case 11: monthName = 'November'; break;
				case 12: monthName = 'December'; break;
				default: monthName = 'Januar'; break;
			}
			var element = `<div href="#" class="list-group-item list-group-item-linkable list-group-item-action">`+monthName+` `+data.dates[i].year+`</div>`;
			$('#monthlyList').append(element);
			$('#monthlyList').find('div').last().on('mouseup', {month: data.dates[i].month, year: data.dates[i].year}, onClickSelectedMonth);
		}
		debugger
	});
}
function onClickSelectedMonth(event) {
	if(event.which == 1)
		selectedMonth(event.data.month, event.data.year, event.data.userId);
}
function selectedMonth(month,year,userId){
	debugger;
	activeMonth = month;
	activeYear = year;
	pdfUser = userId ? userId : null;
	var monthName;
	switch (month) {
		case 2: monthName = 'Februar'; break;
		case 3: monthName = 'Marec'; break;
		case 4: monthName = 'April'; break;
		case 5: monthName = 'Maj'; break;
		case 6: monthName = 'Junij'; break;
		case 7: monthName = 'Julij'; break;
		case 8: monthName = 'Avgust'; break;
		case 9: monthName = 'September'; break;
		case 10: monthName = 'Oktober'; break;
		case 11: monthName = 'November'; break;
		case 12: monthName = 'December'; break;
		default: monthName = 'Januar'; break;
	}
	pdfHeader = monthName+` `+year;
	$.get( "/reports/monthly", {month, year, userId}, function( data ) {
		if(data.success){
			$('#reportBody').empty();
			console.log('Success selectedMonth');
			var users = data.users;
			var reports = data.reports;
			if(users){
				var frame = `<div class="list-group" id="usersList">
				</div>`;
				$('#reportBody').append(frame);
				for(var i = 0; i < data.users.length; i++){
					var element = `<div href="#" class="list-group-item list-group-item-linkable list-group-item-action">`+data.users[i].surname+` `+data.users[i].name+`</div>`;
					$('#usersList').append(element);
					$('#usersList').find('div').last().on('mouseup', {month: activeMonth, year: activeYear, userId: data.users[i].user_id}, onClickSelectedMonth);
				}
				debugger
			}
			else if(reports){
				monthlyReports = reports;
				var pdfButton = `<a class="btn btn-info" id="PDFMonthBtn" href="/reports/pdfreport?userId=`+userId+`&month=`+month+`&year=`+year+`"><i class="fas fa-download fa-lg"></i></button>`;
				$(pdfMonthButton).append(pdfButton);
				//$('#PDFMonthBtn').on('click', toPdfMonth);
				var frame = `<div class="list-group" id="reportsList">
				</div>`;
				$('#reportBody').append(frame);
				var lastDay, lastDayTime;
				for(var i = 0; i < data.reports.length; i++){
					var currentDay = data.reports[i].date;
					if(lastDay && lastDay == currentDay){
						//same day as last iteration, add time and description
						var start = data.reports[i].start;
						var finish = data.reports[i].finish;
						var info = '';
						if(data.reports[i].project_id)
							info = data.reports[i].project_name;
						else
							info = data.reports[i].other;
						var newTime =  new Date("0000-01-01 " + data.reports[i].time);
						lastDayTime.setMinutes(newTime.getMinutes() + lastDayTime.getMinutes());
						lastDayTime.setHours(newTime.getHours() + lastDayTime.getHours());
						$('#reportTime'+currentDay.split('T')[0]).html(elapsedTime.getHours()+'h '+elapsedTime.getMinutes()+'m');
						var newElement = `<p class="card-text" id="report`+data.reports[i].id+`">`+start.substring(0,5)+' - '+finish.substring(0,5)+': '+info+`</p>`;
						$('#reportBody'+currentDay.split('T')[0]).append(newElement);
					}
					else{
						//new day, create new date card
						lastDay = currentDay;
						var start = data.reports[i].start;
						var finish = data.reports[i].finish;
						var info = '';
						if(data.reports[i].project_id)
							info = data.reports[i].project_name;
						else
							info = data.reports[i].other;
						var elapsedTime = new Date("0000-01-01 " + data.reports[i].time);
						lastDayTime = elapsedTime;
						var reportElement = `<div class="card mb-3" id="report`+currentDay.split('T')[0]+`">
							<div class="card-header">
								<div class="alert-dissmisable"><div class="close text-dark" id="reportTime`+currentDay.split('T')[0]+`">`+elapsedTime.getHours()+`h `+elapsedTime.getMinutes()+`m</div>
								<div id="reportDate`+currentDay.split('T')[0]+`">`+new Date(data.reports[i].date).toLocaleDateString('sl')+`</div>
								</div>
							</div>
							<div class="card-body" id="reportBody`+currentDay.split('T')[0]+`">
								<p class="card-text" id="report`+data.reports[i].id+`">`+start.substring(0,5)+' - '+finish.substring(0,5)+': '+info+`</p>
							</div>
						</div>`;
						$('#reportsList').append(reportElement);
					}
					//var element = `<div href="#" class="list-group-item list-group-item-linkable list-group-item-action" onclick='selectedMonth(`+activeMonth+`,`+activeYear+`,`+data.users[i].user_id+`)'>`+data.users[i].surname+` `+data.users[i].name+`</div>`;
					//$('#usersList').append(element);
				}
			}
		}
		else{
			console.log('Failed selectedMonth');
		}
	});
}
function dailyReportMode(){
	$('#searchReportsButton').prop('disabled', false);
	$('#addReportButton').prop('disabled', false);
	$('#dailyReportsButton').addClass('active');
	$('#monthlyReportsButton').removeClass('active');
	$(pdfMonthButton).empty();
	//getReports();
	drawReports();
}
var reportId;
var reportsUserId = 0;
var ctrlChanges;
var userId;
var loggedUser;
var allUsers;
var allTypes;
var allStatus;
var allProjects;
var allReports;
var activeUserRole;
var reverseChanges = false;
var activeMonth;
var activeYear;
var monthlyReports, pdfHeader,pdfUser; //variables used for pdf