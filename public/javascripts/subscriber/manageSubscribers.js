$(function(){
  loggedUser = { name: $('#loggedUser').html().split(" (")[0].split(" ")[0], surname: $('#loggedUser').html().split(" (")[0].split(" ")[1], role: $('#loggedUser').html().split(" (")[1].split(")")[0].toLowerCase() };
  if($('#msg').html() != ''){
    $("#modalMsg").modal();
  }
  debugger;
  $.get("/subscribers/allinfo", function( data ) {
    //console.log(data)
    allSubscribers = data.data;
    //console.log(dataGet[2]);

    tableSubscribers = $('#subscribersTable').DataTable( {
      data: allSubscribers,
      columns: [
          { title: "subscribersId", data: "id" },
          { title: "Naziv", data: "name" },
          { title: "Ikona", data: "icon" },
          { title: "Slika ikone", data: "icon",
          render: function ( data, type, row ) {
            if(data)
              return '<img class="img-fluid w-50px" src="/uploads/images/'+data+'" alt="ikona" />';
            else
              return '';
          }},
          { data: "id",
          render: function ( data, type, row ) {
            if(loggedUser.role == 'admin' || loggedUser.role.substring(0,5) == 'vodja')
              return '<button class="btn btn-primary btn-edit">Uredi</button>';
            else return '<div></div>';
          }},
      ],
      "language": {
        "url": "/DataTables/Slovenian.json"
      },
      order: [[1, "asc"]],
      columnDefs: [
        {
          targets: [0],
          visible: false,
          searchable: false
        },
      ],
      "createdRow": function ( row, data, index ) {
        $(row).attr('id', data.id);
      },
      //responsive: true,
      //info: false,
      //searching: false,
      scrollY: "500px",
      paging: false,
    });
  });
  //AJAX CREATE NEW SUBSCRIBER
  $("#addSubForm").submit(function (e) {
    //event.preventDefault();
    e.preventDefault();
  
    var file = document.getElementById("imageNew").files[0];
    var formData = new FormData();
    formData.append("imageNew", file);
    formData.append("subscriberName", $('#subscriberName').val());
    //console.log(formData);
    setTimeout(disableFunction, 1);
    debugger;
    $.ajax({
      url: '/subscribers/fileUpload',
      type: 'POST',
      data: formData,
      contentType: false,
      processData: false,
      success: function(res) {
        //console.log('success');
        $('#btnAddSub').attr('disabled',false);
        $('#btnEditSub').attr('disabled',false);
        $('#imageNew').val('');
        //addSystemChange(23,1,workOrderId,workOrderFileId);
        //addChangeSystem(1,23,null,null,null,null,null,null,null,null,null,workOrderId,null,null,null,null,null,workOrderFileId);
        $('#modalSubscriberAddForm').modal('toggle')
        //add new file to the list of all files to this work order
        tableSubscribers.row.add({
          id: res.subscriber.id, 
          name: res.subscriber.name, 
          icon: res.subscriber.icon,
        }).draw(false)
        allSubscribers.push({id: res.subscriber.id, name: res.subscriber.name, icon: res.subscriber.icon, active:true});
        //$('#fileList'+activeTaskId).prepend(listElement);
        //prepare modalMsg for info about success
        $('#msgImg').attr('src','/ok_sign.png');
        $('#msg').html(res.message);
        $('#modalMsg').modal('toggle');
      },
      error: function(res) {
        //console.log('error');
        $('#btnAddSub').attr('disabled',false);
        $('#btnEditSub').attr('disabled',false);
        $('#imageNew').val('');
        $('#modalSubscriberAddForm').modal('toggle')
        //prepare modalMsg for info about success
        $('#msgImg').attr('src','/warning_sign.png');
        $('#msg').html(res.responseJSON.message);
        $('#modalMsg').modal('toggle');
      }
    })
  });
  //AJAX UPDATE SUBSCRIBER
  $("#editSubForm").submit(function (e) {
    //event.preventDefault();
    e.preventDefault();
  
    var file = document.getElementById("imageEdit").files[0];
    var formData = new FormData();
    formData.append("imageEdit", file);
    formData.append("subscriberNameEdit", $('#subscriberNameEdit').val());
    formData.append("subscriberIdEdit", $('#subscriberIdEdit').val());
    //console.log(formData);
    setTimeout(disableFunction, 1);
    debugger;
    $.ajax({
      url: '/subscribers/fileUploadEdit',
      type: 'POST',
      data: formData,
      contentType: false,
      processData: false,
      success: function(res) {
        //console.log('success');
        $('#btnAddSub').attr('disabled',false);
        $('#btnEditSub').attr('disabled',false);
        $('#imageEdit').val('');
        //addSystemChange(23,1,workOrderId,workOrderFileId);
        //addChangeSystem(1,23,null,null,null,null,null,null,null,null,null,workOrderId,null,null,null,null,null,workOrderFileId);
        $('#modalSubscriberEditForm').modal('toggle')
        //add new file to the list of all files to this work order
        var tmp = tableSubscribers.row('#'+$('#subscriberIdEdit').val()).data();
        tmp.name = res.subscriber.name;
        tmp.icon = res.subscriber.icon;
        tableSubscribers.row('#'+$('#subscriberIdEdit').val()).data(tmp).invalidate().draw(false);
        //$('#fileList'+activeTaskId).prepend(listElement);
        //prepare modalMsg for info about success
        $('#msgImg').attr('src','/ok_sign.png');
        $('#msg').html(res.message);
        $('#modalMsg').modal('toggle');
      },
      error: function(res) {
        //console.log('error');
        $('#btnAddSub').attr('disabled',false);
        $('#btnEditSub').attr('disabled',false);
        $('#imageEdit').val('');
        $('#modalSubscriberEditForm').modal('toggle')
        //prepare modalMsg for info about success
        $('#msgImg').attr('src','/warning_sign.png');
        $('#msg').html(res.responseJSON.message);
        $('#modalMsg').modal('toggle');
      }
    })
  });
  $('#AddSubBtn').on('click', newSubscriberModal);
  setTimeout(()=>{
    $('#subscribersTable tbody').on('click', 'button', function (event) {
      //var data = tableEmployees.row( this ).data();
      debugger
      //alert( 'You clicked on '+data.id+'\'s row' );
      //event.target.offsetParent.id
      if ($(event.currentTarget).hasClass('btn-edit'))
        editSubscriber(event.target.offsetParent.parentElement.id);
    });
  },700);
})
function disableFunction(){
  $('#btnAddDoc').attr('disabled','disabled');
  $('#btnAddSub').attr('disabled','disabled');
  $('#btnEditSub').attr('disabled','disabled');
}
function newSubscriberModal(){
  debugger
  $("#modalSubscriberAddForm").modal();
}
function newDocumentModal(){
  debugger
  $("#modalDocumentAddForm").modal();
}
function editSubscriber(id){
  $('#subscriberIdEdit').val(id);
  var sub = allSubscribers.find(a => a.id == id);
  $('#subscriberNameEdit').val(sub.name);
  debugger
  $("#modalSubscriberEditForm").modal();
}
var loggedUser;
var allSubscribers;
var tableSubscribers;