$(function () {
  $.get("/feedback/all", function (data) {
    debugger;
    tableFeedback = $('#feedbackTable').DataTable({
      data: data.data,
      columns: [
        { title: "Id", data: "id" },
        { title: "Odziv", data: "feedback" },
        {
          title: "Obravnavano", data: "handled",
          render: function (data, type, row) {
            var id = row['id'];
            if (data) {
              return `<div class="custom-control custom-checkbox table-custom-checkbox" id=` + id + `>
                <input class="custom-control-input" id="customTaskCheck`+ id + `" type="checkbox" checked ="" onchange="toggleFeedbackCheckbox(this)" />
                <label class="custom-control-label" for="customTaskCheck`+ id + `"> </label>
              </div>`;
            }
            else {
              return `<div class="custom-control custom-checkbox table-custom-checkbox" id=` + id + `>
                <input class="custom-control-input" id="customTaskCheck`+ id + `" type="checkbox" onchange="toggleFeedbackCheckbox(this)" />
                <label class="custom-control-label" for="customTaskCheck`+ id + `"> </label>
              </div>`;
            }
          }
        },
        { title: "Ustvarjeno", data: "created" }
      ],
      info: true,
      searching: true,
      paging: true,
    })
  })
})
function toggleFeedbackCheckbox(element) {
  feedbackId = element.parentElement.id;
  var handled;
  if (element.checked) {
    handled = true;
  }
  else {
    handled = false;
  }
  debugger;
  $.post('/feedback/handle', { feedbackId, handled }, function (resp) {
    debugger;
  })
}
var feedbackId;
var allFeedbacks;
var tableFeedback;