//IN THIS SCRIPT//
//// refresh changes body, drawing changes in body, draw changes based on input in control section ////
$(function(){
	setTimeout(()=>{

		//get user info from page
		loggedUser = {name:$('#loggedUser').html().split(" (")[0].split(" ")[0], surname:$('#loggedUser').html().split(" (")[0].split(" ")[1], role:$('#loggedUser').html().split(" (")[1].split(")")[0].toLowerCase()};
		activeUserRole = loggedUser.role;
		//UPORABNIKI
		$.get( "/projects/users", function( data ) {
			allUsers = data.data;
			allUsers.unshift({id:0, text: " "});
			$(".select2-users").select2({
				data: allUsers,
			});
			allUsers.shift();
			allUsers = data.data;
			$(".select2-new-users").select2({
				data: allUsers,
				multiple: true,
			});
			$(".select2-new-user").select2({
				data: allUsers,
			});
			$(".select2-edit-report-users").select2({
				data: allUsers,
			});
			userId = allUsers.find(u => u.text == (loggedUser.name+" "+loggedUser.surname)).id;
			$('#woWorkerNew').val(userId).trigger('change');
			/*
			if(loggedUser.role == 'admin' || loggedUser.role == 'tajnik' || loggedUser.role.substring(0,5) == 'vodja'){
				userId = allUsers.find(u => u.text == (loggedUser.name+" "+loggedUser.surname)).id;
				$('#woWorkerNew').val(userId).trigger('change');
				//$('#woUserSelect').val(userId).trigger('change');
				//$('#newReportUser').val(userId).trigger('change');
			}
			*/
		});
		//TIPI
		$.get( "/workorders/types", function( data ) {
			allTypes = data.data;
			allTypes.unshift({id:0, text: " "});
			$(".select2-type").select2({
				data: allTypes,
				search: false,
				placeholder: "Izberi tip",
			});
			allTypes.shift();
			$(".select2-new-type").select2({
				data: allTypes,
				placeholder: "Tip"
			});
			//allTypes.unshift({id:0, text: "Vsi"});
		});
		//NAROČNIKI
		$.get( "/subscribers/all", function( data ) {
			allSubscribers = data.data;
			allSubscribers.unshift({id:0, text: " "});
			$(".select2-new-subscribers").select2({
				data: allSubscribers,
				search: false,
				placeholder: "Izberi naročnika",
			});
		});
		//PROJEKTI
		$.get( "/reports/projects", function( data ) {
			allProjects = data.data;
			allProjects.unshift({id:0, text: " "});
			$(".select2-new-projects").select2({
				data: allProjects,
				search: false,
			});
			$(".select2-projects").select2({
				data: allProjects,
				search: false,
				placeholder: "Izberi projekt",
			});
			$(".select2-subscribers").select2({
				data: allProjects,
				search: false,
				placeholder: "Izberi naročnika",
			});
		});
		setTimeout(()=>{
			
			$('#report_start').val(moment().format("YYYY-MM-DD"));
			$('#report_finish').val(moment().format("YYYY-MM-DD"));
			$('#woDateNew').val(moment().format("YYYY-MM-DD"));
			getWorkOrders();
	
		},100);

		//ADD NEW WORK ORDER
		$('#addNewWorkOrderForm').submit(function(e){
			e.preventDefault();
			$form = $(this);
			
			//debugger;
			var userId = null;
			if(loggedUser.role == 'admin' || loggedUser.role == 'tajnik' || loggedUser.role == 'komercialist' || loggedUser.role == 'komerciala' || loggedUser.role == 'računovodstvo' || loggedUser.role.substring(0,5) == 'vodja')
				userId = woWorkerNew.value;
			var number = woNumberNew.value;
			var type = woTypeNew.value;
			var date = woDateNew.value;
			var location = woLocationNew.value;
			var subscriberId = woSubscriberNew.value;
			var projectId = woProjectNew.value;
			var description = woDescriptionNew.value;
			var workersId = $('#woWorkersNew').val();
			workersId = workersId.toString();
			var vehicle = woVehicleNew.value;
			var arrival = woArrivalNew.value;
			var departure = woDepartureNew.value;
			var representative = woRepresentativeNew.value;
			$.post('/workorders/add', {userId,number,type,date,location,subscriberId,projectId,description,workersId,vehicle,arrival,departure,representative}, function(resp){
				if(resp.success){
					var workOrderId = resp.woId;
					console.log("Uspešno dodajanje novega delovnega naloga.");
					tmpSubscriber = {id:null,text:null};
					tmpProject = {id:null,text:null};
					if(subscriberId != 0){
						tmpSubscriber = allSubscribers.find(s => s.id == subscriberId);
					}
					if(projectId != 0)
						tmpProject = allProjects.find(p => p.id == projectId);
					tmpType = allTypes.find(t => t.id == type);
					//formatted date
					var newDate = date.split('-');
					var formatted_date = {date:newDate[2]+'.'+newDate[1]+'.'+newDate[0],time:'00:00'}
					//formatted time
					var formatted_arrival = {hours:arrival.split(':')[0], minutes:arrival.split(':')[1]};
					var formatted_departure = {hours:departure.split(':')[0], minutes:departure.split(':')[1]};
					var newWorkOrder = ({
						id: workOrderId,
						subscriber_id: tmpSubscriber.id,
						subscriber_name: tmpSubscriber.text,
						project_id: tmpProject.id,
						project_name: tmpProject.text,
						number: number,
						date: date,
						location: location,
						description: description,
						vehicle: vehicle,
						arrival: arrival,
						departure: departure,
						work_order_type_id: tmpType.id,
						work_order_type: tmpType.text,
						representative: representative,
						user_id: woWorkerNew.value,
						formatted_date: formatted_date,
						formatted_arrival: formatted_arrival,
						formatted_departure: formatted_departure,
					})
					allWorkOrders.unshift(newWorkOrder);
					var woElement = `<div class="card bg-light card-report mb-3" id="workOrder`+workOrderId+`" onclick="selectWorkOrder(`+workOrderId+`)">
						<div class="card-header">
							<div class="alert-dissmisable">
								<div class="close" id="workOrderDate`+workOrderId+`" style="color:#3E3F3A;">`+newWorkOrder.formatted_date.date+`</div>
								<div id="workOrderNumber`+workOrderId+`">`+number+`</div>
							</div>
						</div>
					</div>`;
					$('#workOrderBody').prepend(woElement);
					$('#workOrderNewCollapse').toggle('collapse')
					if(workersId){
						var addWorkers = workersId;
						$.post('/workorders/workers', {workOrderId,addWorkers}, function(respWorkers){
							if(respWorkers.success)
								console.log('Uspešno dodajanje delavcev na delovni nalog.');
							else
								console.log('Neuspešno dodajanje delavcev na delovni nalog!');
						})
					}
				}
				else
					console.log("Pri dodajanju novega delovnega naloga je prišlo do napake.");
			})
		})
		//EDIT WORK ORDER
		$('#editWorkOrderForm').submit(function(e){
			e.preventDefault();
			$form = $(this);
			
			//debugger;
			var userId = null;
			if(loggedUser.role == 'admin' || loggedUser.role == 'tajnik' || loggedUser.role == 'komercialist' || loggedUser.role == 'komerciala' || loggedUser.role == 'računovodstvo' || loggedUser.role.substring(0,5) == 'vodja')
				userId = woWorkerEdit.value;
			var number = woNumberEdit.value;
			var type = woTypeEdit.value;
			var date = woDateEdit.value;
			var location = woLocationEdit.value;
			var subscriberId = woSubscriberEdit.value;
			var projectId = woProjectEdit.value;
			var description = woDescriptionEdit.value;
			var workersId = $('#woWorkersEdit').val();
			workersId = workersId.toString();
			var vehicle = woVehicleEdit.value;
			var arrival = woArrivalEdit.value;
			var departure = woDepartureEdit.value;
			var representative = woRepresentativeEdit.value;
			$.post('/workorders/edit', {workOrderId,userId,number,type,date,location,subscriberId,projectId,description,workersId,vehicle,arrival,departure,representative}, function(resp){
				if(resp.success){
					console.log("Uspešno posodobitev delovnega naloga.");
					tmpSubscriber = {id:null,text:null};
					tmpProject = {id:null,text:null};
					if(subscriberId != 0){
						tmpSubscriber = allSubscribers.find(s => s.id == subscriberId);
					}
					if(projectId != 0)
						tmpProject = allProjects.find(p => p.id == projectId);
					tmpType = allTypes.find(t => t.id == type);
					//formatted date
					var newDate = date.split('-');
					var formatted_date = {date:newDate[2]+'.'+newDate[1]+'.'+newDate[0],time:'00:00'}
					//formatted time
					var formatted_arrival = {hours:arrival.split(':')[0], minutes:arrival.split(':')[1]};
					var formatted_departure = {hours:departure.split(':')[0], minutes:departure.split(':')[1]};
					tmpWorkOrder = allWorkOrders.find(wo => wo.id == workOrderId);
					tmpWorkOrder.subscriber_id = tmpSubscriber.id;
					tmpWorkOrder.subscriber_name = tmpSubscriber.text;
					tmpWorkOrder.project_id = tmpProject.id;
					tmpWorkOrder.project_name = tmpProject.text;
					tmpWorkOrder.number = number;
					tmpWorkOrder.date = date;
					tmpWorkOrder.location = location;
					tmpWorkOrder.description = description;
					tmpWorkOrder.vehicle = vehicle;
					tmpWorkOrder.arrival = arrival;
					tmpWorkOrder.departure = departure;
					tmpWorkOrder.work_order_type_id = tmpType.id;
					tmpWorkOrder.work_order_type = tmpType.text;
					tmpWorkOrder.formatted_date = formatted_date;
					tmpWorkOrder.formatted_arrival = formatted_arrival;
					tmpWorkOrder.formatted_departure = formatted_departure;
					
					if(workersId){
						editWorkers = $('#woWorkersEdit').val();
						var addWorkers = [];
						var deleteWorkers = [];
						for(var i = 0; i < workOrderWorkers.length; i++){
							tmpWorker = editWorkers.find(w => w == workOrderWorkers[i].worker_id);
							if(!tmpWorker)
								deleteWorkers.push(workOrderWorkers[i].id)
						}
						for(var i = 0; i < editWorkers.length; i++){
							tmpWorker = workOrderWorkers.find(wow => wow.worker_id == editWorkers[i])
							if(!tmpWorker)
								addWorkers.push(parseInt(editWorkers[i]));
						}
						//console.log("Novi delavci: ");
						//console.log(addWorkers);
						//console.log("Izbrisani zapisi delavcev: ");
						//console.log(deleteWorkers);
						if(addWorkers.length > 0 || deleteWorkers.length > 0){
							addWorkers = addWorkers.toString();
							deleteWorkers = deleteWorkers.toString();
							$.post('/workorders/workers', {workOrderId,addWorkers,deleteWorkers}, function(respWorkers){
								if(respWorkers.success){
									console.log('Uspešno posodabljanje delavcev na delovni nalog.');
									closeEditOnSuccess();
								}
								else
									console.log('Neuspešno posodabljane delavcev na delovni nalog!');
							})
						}
						else{
							closeEditOnSuccess();
						}
					}
				}
				else
					console.log("Pri posodabljanju delovnega naloga je prišlo do napake.");
			})
		})
		//get work orders based on search input
		$('#getWorkOrdersForm').submit(function(e){
			e.preventDefault();
			$form = $(this);
	
			var userId = null;
			if(loggedUser.role == 'admin' || loggedUser.role == 'tajnik' || loggedUser.role == 'komercialist' || loggedUser.role == 'komerciala' || loggedUser.role == 'računovodstvo' || loggedUser.role.substring(0,5) == 'vodja')
				userId = woUserSelect.value;
			var dateStart = woStartSelect.value;
			var dateFinish = woFinishSelect.value;
			var projectId = woProjectSelect.value;
			var subscriberId = woSubscriberSelect.value;
			var woType = woTypeSelect.value;
			$.get( "/workorders/get",{userId,dateStart,dateFinish,projectId,subscriberId,woType}, function( resp ) {
				if(resp.success){
					//debugger;
					console.log('Uspešno pridobivanje podatkov');
					allWorkOrders = resp.data;
					$('#workOrderName').html('Seznam iskanih delovnih nalog:');
					drawWorkOrders();
				}
			});
		})
	},200)
})
function closeEditOnSuccess(){
	tmpWorkOrder = allWorkOrders.find(wo => wo.id == workOrderId);
	tmpSerialNumber = tmpWorkOrder.id + '/' + tmpWorkOrder.formatted_date.date.split('.')[2];
	$('#workOrderNumber').html(tmpWorkOrder.number + ' (' + tmpSerialNumber + ')');
	//$('#workOrderNumber').html(tmpWorkOrder.number)
	$('#workOrderDate'+workOrderId).html(tmpWorkOrder.formatted_date.date);
	$('#workOrderDate').html(tmpWorkOrder.formatted_date.date)
	$('#workOrderType').html(tmpWorkOrder.work_order_type)
	$('#workOrderLocation').html(tmpWorkOrder.location)
	if(tmpWorkOrder.project_id)
		$('#workOrderProject').html(tmpWorkOrder.project_name)
	else
		$('#workOrderProject').html('')
	if(tmpWorkOrder.subscriber_id)
		$('#workOrderSubscriber').html(tmpWorkOrder.subscriber_name)
	else
		$('#workOrderSubscriber').html('')
	$('#workOrderDescription').html(tmpWorkOrder.description)
	$('#workOrderVehicle').html(tmpWorkOrder.vehicle)
	$('#workOrderArrival').html(tmpWorkOrder.formatted_arrival.hours+":"+tmpWorkOrder.formatted_arrival.minutes)
	$('#workOrderDeparture').html(tmpWorkOrder.formatted_departure.hours+":"+tmpWorkOrder.formatted_departure.minutes)
	$('#workOrderRepresentative').html(tmpWorkOrder.representative)
	tmpUser = allUsers.find(u => u.id == tmpWorkOrder.user_id)
	$('#workOrderUser').html(tmpUser.text)
	$.get( "/workorders/workers",{workOrderId}, function( data ) {
		if(data.success){
			workOrderWorkers = data.data;
			var workers = '';
			for(var i = 0; i < workOrderWorkers.length; i++){
				workers += workOrderWorkers[i].name + ' ' + workOrderWorkers[i].surname;
				if(i+1 != workOrderWorkers.length)
					workers += ', ';
			}
			$('#workOrderWorkers').html(workers);
		}
	});
	$('#workOrderEdit').toggle('collapse');
	$('#workOrderInfo').toggle('collapse');
}
function drawReports(){
	$('#reportBody').empty();
	for(var i = 0; i < allReports.length; i++){
		var tmpInfo = '';
		if(allReports[i].project_id)
			tmpInfo = allReports[i].project_number+' - '+allReports[i].project_name;
		else
			tmpInfo = allReports[i].other;
		var reportElement = `<div class="card text-white bg-primary card-report mb-3" id="report`+allReports[i].id+`" onclick="editReport(`+allReports[i].id+`)">
			<div class="card-header">
				<div class="alert-dissmisable">
					<div class="close" id="reportTime`+allReports[i].id+`">`+allReports[i].formatted_time.hours+`h `+allReports[i].formatted_time.minutes+`m</div>
					<div id="reportInfo`+allReports[i].id+`">`+tmpInfo+`</div>
				</div>
			</div>
			<div class="card-body">
				<h5 class="card-title" style="white-space: pre-wrap;" id="reportDescription`+allReports[i].id+`">`+allReports[i].description+`</h5>
				<p class="card-text" id="reportType`+allReports[i].id+`">`+allReports[i].report_type+`</p>
			</div>
			<div class="card-subtitle ml-auto mr-2 mt-n4" id="reportDate`+allReports[i].id+`">`+allReports[i].formatted_date.date+`</div>
		</div>`;
		$('#reportBody').append(reportElement);
	}
}
function drawWorkOrders(){
	$('#workOrderBody').empty();
	for(var i = 0; i < allWorkOrders.length; i++){
		var element = `<div class="card bg-light card-report mb-3" id="workOrder`+allWorkOrders[i].id+`" onclick="selectWorkOrder(`+allWorkOrders[i].id+`)">
			<div class="card-header">
				<div class="alert-dissmisable">
					<div class="close" id="workOrderDate`+allWorkOrders[i].id+`" style="color:#3E3F3A;">`+allWorkOrders[i].formatted_date.date+`</div>
					<div id="workOrderNumber`+allWorkOrders[i].id+`">`+allWorkOrders[i].number+`</div>
				</div>
			</div>
		</div>`;
		$('#workOrderBody').append(element);
	}
}
function getWorkOrders(){
	$.get( "/workorders/get", function( data ) {
		allWorkOrders = data.data;
	});
}
function addNewWorkOrder(){
	$('#workOrderNewCollapse').toggle('collapse')
}
function closeWorkOrderNew(){
	$('#workOrderNewCollapse').toggle('collapse')
}
function editReport(id){
	reportId = id;
	tmpReport = allReports.find(r => r.id == reportId);
	if(loggedUser.role == 'admin' || loggedUser.role == 'tajnik' || loggedUser.role == 'komercialist' || loggedUser.role == 'komerciala' || loggedUser.role == 'računovodstvo' || loggedUser.role.substring(0,5) == 'vodja'){
		$('#editReportUser').val(reportsUserId).trigger('change');
	}
	var date = tmpReport.formatted_date.date.split('.');
	$('#editReportDate').val(date[2]+'-'+date[1]+'-'+date[0]);
	$('#editReportTime').val(tmpReport.formatted_time.hours+':'+tmpReport.formatted_time.minutes+':00');
	$('#editReportType').val(tmpReport.report_type_id).trigger('change');
	$('#editReportDescription').val(tmpReport.description);
	if(tmpReport.project_id){
		$('#editReportProject').val(tmpReport.project_id).trigger('change');
		$('#editReportOther').val('');
		$('#editReportProjectForm').show();
		$('#editReportOtherForm').hide();
		$('#editReportOther').prop('required', false);
		$('#editReportRadioProject').prop('checked', true);
		$('#editReportRadioOther').prop('checked', false);
	}
	else{
		$('#editReportOther').val(tmpReport.other);
		$('#editReportProjectForm').hide();
		$('#editReportOtherForm').show();
		$('#editReportOther').prop('required', true);
		$('#editReportRadioProject').prop('checked', false);
		$('#editReportRadioOther').prop('checked', true);
	}
	$('#modalEditReport').modal('toggle');
}
function changeRadioOtherNew(){
	//console.log('test menjava na drugo');
	$('#newReportProjectForm').hide();
	$('#newReportOtherForm').show();
	$('#newReportOther').prop('required', true);
}
function changeRadioProjectNew(){
	//console.log('test menjava na projekt');
	$('#newReportProjectForm').show();
	$('#newReportOtherForm').hide();
	$('#newReportOther').prop('required', false);
}
function changeRadioOtherInput(){
	//console.log('test menjava na drugo');
	$('#inputReportProjectForm').hide();
	$('#inputReportOtherForm').show();
}
function changeRadioProjectInput(){
	//console.log('test menjava na drugo');
	$('#inputReportOtherForm').hide();
	$('#inputReportProjectForm').show();
}
function changeRadioOtherEdit(){
	//console.log('test menjava na drugo');
	$('#editReportProjectForm').hide();
	$('#editReportOtherForm').show();
	$('#editReportOther').prop('required', true);
}
function changeRadioProjectEdit(){
	//console.log('test menjava na drugo');
	$('#editReportOtherForm').hide();
	$('#editReportProjectForm').show();
	$('#editReportOther').prop('required', false);
}
function changeNewReportUser(){
	$('#newReportUser').val($('#userSelect').val()).trigger('change');
}
function selectWorkOrder(id){
	//console.log(id);
	workOrderId = id;
	workOrderExpenses = null;
	workOrderMaterials = null;
	tmpWorkOrder = allWorkOrders.find(wo => wo.id == id);
	tmpSerialNumber = tmpWorkOrder.id + '/' + tmpWorkOrder.formatted_date.date.split('.')[2];
	$('#workOrderNumber').html(tmpWorkOrder.number + ' (' + tmpSerialNumber + ')');
	$('#workOrderDate').html(tmpWorkOrder.formatted_date.date)
	$('#workOrderType').html(tmpWorkOrder.work_order_type)
	$('#workOrderLocation').html(tmpWorkOrder.location)
	if(tmpWorkOrder.project_id)
		$('#workOrderProject').html(tmpWorkOrder.project_name)
	else
		$('#workOrderProject').html('')
	if(tmpWorkOrder.subscriber_id)
		$('#workOrderSubscriber').html(tmpWorkOrder.subscriber_name)
	else
		$('#workOrderSubscriber').html('')
	$('#workOrderDescription').html(tmpWorkOrder.description)
	$('#workOrderVehicle').html(tmpWorkOrder.vehicle)
	$('#workOrderArrival').html(tmpWorkOrder.formatted_arrival.hours+":"+tmpWorkOrder.formatted_arrival.minutes)
	$('#workOrderDeparture').html(tmpWorkOrder.formatted_departure.hours+":"+tmpWorkOrder.formatted_departure.minutes)
	$('#workOrderRepresentative').html(tmpWorkOrder.representative)
	tmpUser = allUsers.find(u => u.id == tmpWorkOrder.user_id)
	$('#workOrderUser').html(tmpUser.text)
	$.get( "/workorders/workers",{workOrderId}, function( data ) {
		if(data.success){
			workOrderWorkers = data.data;
			var workers = '';
			for(var i = 0; i < workOrderWorkers.length; i++){
				workers += workOrderWorkers[i].name + ' ' + workOrderWorkers[i].surname;
				if(i+1 != workOrderWorkers.length)
					workers += ', ';
			}
			$('#workOrderWorkers').html(workers);
		}
	});
	$.get( "/workorders/expenses",{workOrderId}, function( data ) {
		if(data.success){
			workOrderExpenses = data.data[0];
			if(workOrderExpenses){
				$('#timeQuantity').html(workOrderExpenses.time_quantity);
				$('#timePrice').html(workOrderExpenses.time_price);
				$('#timeSum').html(workOrderExpenses.time_sum);
				$('#distanceQuantity').html(workOrderExpenses.distance_quantity);
				$('#distancePrice').html(workOrderExpenses.distance_price);
				$('#distanceSum').html(workOrderExpenses.distance_sum);
				$('#materialQuantity').html(workOrderExpenses.material_quantity);
				$('#materialPrice').html(workOrderExpenses.material_price);
				$('#materialSum').html(workOrderExpenses.material_sum);
				$('#otherQuantity').html(workOrderExpenses.other_quantity);
				$('#otherPrice').html(workOrderExpenses.other_price);
				$('#otherSum').html(workOrderExpenses.other_sum);
			}
			else{
				$('#timeQuantity').html('');
				$('#timePrice').html('');
				$('#timeSum').html('');
				$('#distanceQuantity').html('');
				$('#distancePrice').html('');
				$('#distanceSum').html('');
				$('#materialQuantity').html('');
				$('#materialPrice').html('');
				$('#materialSum').html('');
				$('#otherQuantity').html('');
				$('#otherPrice').html('');
				$('#otherSum').html('');
			}
		}
	});
	$.get( "/workorders/materials",{workOrderId}, function( data ) {
		if(data.success){
			workOrderMaterials = data.data;
			$('#materialBody').empty()
			var i;
			for(i = 0; i < workOrderMaterials.length; i++){
				var element = `<tr>
					<td scope="row">`+(i+1)+`.</td>
					<td>`+workOrderMaterials[i].material_name+`</td>
					<td>`+workOrderMaterials[i].quantity+`</td>
					<td>`+workOrderMaterials[i].price+`</td>
					<td>`+workOrderMaterials[i].sum+`</td>
				</tr>`;
				$('#materialBody').append(element);
			}
			var element = `<tr>
				<td scope="row">`+(i+1)+`.</td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
			</tr>`;
			$('#materialBody').append(element);
		}
	});
	$('#workOrderInfo').toggle('collapse');
	$('#workOrderList').toggle('collapse');
	//$('#workOrderInfo').show();
}
function closeWorkOrderInfo(){
	$('#workOrderInfo').toggle('collapse');
	$('#workOrderList').toggle('collapse');
	//$('#workOrderInfo').show();
}
function openWorkOrderEdit(){
	tmpWorkOrder = allWorkOrders.find(wo => wo.id == workOrderId);
	$('#woNumberEdit').val(tmpWorkOrder.number)
	var date = tmpWorkOrder.formatted_date.date.split('.');
	$('#woDateEdit').val(date[2]+'-'+date[1]+'-'+date[0]);
	$('#woTypeEdit').val(tmpWorkOrder.work_order_type_id).trigger('change');
	$('#woLocationEdit').val(tmpWorkOrder.location)
	if(tmpWorkOrder.project_id)
		$('#woProjectEdit').val(tmpWorkOrder.project_id).trigger('change');
	else
		$('#woProjectEdit').val(0).trigger('change');
	if(tmpWorkOrder.subscriber_id)
		$('#woSubscriberEdit').val(tmpWorkOrder.subscriber_id).trigger('change');
	else
		$('#woSubscriberEdit').val(0).trigger('change');
	$('#woDescriptionEdit').val(tmpWorkOrder.description)
	$('#woVehicleEdit').val(tmpWorkOrder.vehicle)
	$('#woArrivalEdit').val(tmpWorkOrder.formatted_arrival.hours+":"+tmpWorkOrder.formatted_arrival.minutes)
	$('#woDepartureEdit').val(tmpWorkOrder.formatted_departure.hours+":"+tmpWorkOrder.formatted_departure.minutes)
	$('#woRepresentativeEdit').val(tmpWorkOrder.representative)
	$('#woWorkerEdit').val(tmpWorkOrder.user_id).trigger('change');
	var workers = [];
	for(var i = 0; i < workOrderWorkers.length; i++){
		workers.push(workOrderWorkers[i].worker_id);
	}
	$('#woWorkersEdit').val(workers).trigger('change')

	//open&close collapses
	$('#workOrderInfo').toggle('collapse');
	$('#workOrderEdit').toggle('collapse');
}
function closeWorkOrderEdit(){
	$('#workOrderEdit').toggle('collapse');
	$('#workOrderInfo').toggle('collapse');
}
function openDeleteModal(){
	$('#modalDeleteWorkOrder').modal('toggle');
}
function deleteWorkOrder(){
	//console.log("delete work order "+workOrderId);
	$.post('/workorders/delete', {workOrderId}, function(resp){
		if(resp.success){
			console.log('Uspešno odstranitev delovnega naloga.');
			$('#modalDeleteWorkOrder').modal('toggle');
			$('#workOrderInfo').toggle('collapse');
			$('#workOrder'+workOrderId).remove();
			$('#workOrderList').toggle('collapse');
			deleteIndex = allWorkOrders.findIndex(wo => wo.id == workOrderId);
			allWorkOrders.splice(deleteIndex,1);
		}
		else
			console.log('Neuspešna odstranitev delovnega naloga!');
	})
}
function calcNewTime(){
	var start = $('#newReportStart').val();
	var finish = $('#newReportFinish').val();
	if(start && finish){
		var ary1=start.split(':'),ary2=finish.split(':');
		var minsdiff=parseInt(ary2[0],10)*60+parseInt(ary2[1],10)-parseInt(ary1[0],10)*60-parseInt(ary1[1],10);
		//console.log(String(100+Math.floor(minsdiff/60)).substr(1)+':'+String(100+minsdiff%60).substr(1));
		$('#newReportTime').val(String(100+Math.floor(minsdiff/60)).substr(1)+':'+String(100+minsdiff%60).substr(1));
	}
}
function calcEditTime(){
	var start = $('#editReportStart').val();
	var finish = $('#editReportFinish').val();
	if(start && finish){
		var ary1=start.split(':'),ary2=finish.split(':');
		var minsdiff=parseInt(ary2[0],10)*60+parseInt(ary2[1],10)-parseInt(ary1[0],10)*60-parseInt(ary1[1],10);
		//console.log(String(100+Math.floor(minsdiff/60)).substr(1)+':'+String(100+minsdiff%60).substr(1));
		$('#editReportTime').val(String(100+Math.floor(minsdiff/60)).substr(1)+':'+String(100+minsdiff%60).substr(1));
	}
}
var workOrderId;
var workOrderWorkers;
var workOrderExpenses;
var workOrderMaterials;
var reportId;
var reportsUserId = 0;
var ctrlChanges;
var userId;
var loggedUser;
var allWorkOrders;
var allUsers;
var allTypes;
var allStatus;
var allProjects;
var allSubscribers;
var allReports;
var activeUserRole;
var reverseChanges = false;