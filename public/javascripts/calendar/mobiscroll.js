$(function(){
  // on page load 
  formatDate = mobiscroll.util.datetime.formatDate;
  loggedUser = {name:$('#loggedUser').html().split(" (")[0].split(" ")[0], surname:$('#loggedUser').html().split(" (")[0].split(" ")[1], role:$('#loggedUser').html().split(" (")[1].split(")")[0].toLowerCase()};
  //get all users
  // $.get( "/employees/all", {activeUser}, function( data ) {
  //   //console.log(data)
  //   allUsers = data.data;
  // });\
  if (loggedUser.role.substring(0,5) == 'vodja'){
    $('#eventPopup').removeClass('active');
    $('#taskPopup').addClass('active');
  }
  if (loggedUser.role.includes('info')){
    clickToCreateVar = false;
    dragToCreateVar = false;
    dragToMoveVar = false;
    dragToResizeVar = false;
  }

  //resize resources depending on which logged role user is
  if (loggedUser.role.includes('elektr')){
     dbRobotehResources[0].children.forEach(c => c.collapsed = ((c.id != 'električar') && (c.id != 'vodja Električarjev')));
  }
  if (loggedUser.role.includes('program')){
    dbRobotehResources[0].children.forEach(c => c.collapsed = ((c.id != 'plc') && (c.id != 'robot')));
  }
  if (loggedUser.role.includes('konstr')){
    dbRobotehResources[0].children.forEach(c => c.collapsed = ((c.id != 'konstrukter') && (c.id != 'konstruktor')));
  }
  if (loggedUser.role.includes('stroj') || loggedUser.role.includes('varil') || loggedUser.role.includes('CNC')){
    dbRobotehResources[0].children.forEach(c => c.collapsed = ((c.id != 'vodja Strojnikov') && (c.id != 'vodja CNC Obdelave') && (c.id != 'strojnik') && (c.id != 'strojni Monter') && (c.id != 'varilec') && (c.id != 'CNC operater')));
  }


  // $.get( "/apiv2/events", {}, function( data ) {
  //   //console.log(data)
  //   events = data.events;
  //   for (const event of events) {
  //     event.editable = event.my_event;
  //     // event.start = event.og_start;
  //     event.iconType = event.editable ? 'lock-open' : 'lock';
  //     if(loggedUser.role == 'admin')
  //       event.editable = true;
  //   }

  // });
  $.get( "/apiv2/employees", {}, function( data ) {
    //console.log(data)
    users = data.users;
    for (const user of users) {
      if ( user.name + ' ' + user.surname == loggedUser.name + ' ' + loggedUser.surname) loggedUser.id = user.id;
      if (user.role.toLowerCase().includes('info') || !user.active)
        continue;
      if (user.role.toLowerCase().includes('plc')){
        dbRobotehResources[0].children[0].children.push({ id: 'user' + user.id, name: user.name + ' ' + user.surname, role: user.role, color: user.color })
      }
      else if (user.role.toLowerCase().includes('robot')){
        dbRobotehResources[0].children[1].children.push({ id: 'user' + user.id, name: user.name + ' ' + user.surname, role: user.role, color: user.color })
      }
      else if (user.role.toLowerCase().includes('serviser')){
        dbRobotehResources[0].children[2].children.push({ id: 'user' + user.id, name: user.name + ' ' + user.surname, role: user.role, color: user.color })
      }
      else if (user.role.toLowerCase().includes('konstr')){
        dbRobotehResources[0].children[3].children.push({ id: 'user' + user.id, name: user.name + ' ' + user.surname, role: user.role, color: user.color })
      }
      else if (user.role.toLowerCase().includes('vodja pro')){
        dbRobotehResources[0].children[6].children.push({ id: 'user' + user.id, name: user.name + ' ' + user.surname, role: user.role, color: user.color })
      }
      else if (user.role.toLowerCase().includes('vodja stro')){
        dbRobotehResources[0].children[7].children.push({ id: 'user' + user.id, name: user.name + ' ' + user.surname, role: user.role, color: user.color })
      }
      else if (user.role.toLowerCase().includes('vodja cnc')){
        dbRobotehResources[0].children[8].children.push({ id: 'user' + user.id, name: user.name + ' ' + user.surname, role: user.role, color: user.color })
      }
      else if (user.role.toLowerCase().includes('vodja elek')){
        dbRobotehResources[0].children[9].children.push({ id: 'user' + user.id, name: user.name + ' ' + user.surname, role: user.role, color: user.color })
      }
      else if (user.role.toLowerCase().includes('strojnik')){
        dbRobotehResources[0].children[4].children.push({ id: 'user' + user.id, name: user.name + ' ' + user.surname, role: user.role, color: user.color })
      }
      else if (user.role.toLowerCase().includes('elektri')){
        dbRobotehResources[0].children[5].children.push({ id: 'user' + user.id, name: user.name + ' ' + user.surname, role: user.role, color: user.color })
      }
      else if (user.role.toLowerCase().includes('tajni') || user.role.toLowerCase().includes('komercialist')){
        dbRobotehResources[0].children[10].children.push({ id: 'user' + user.id, name: user.name + ' ' + user.surname, role: user.role, color: user.color })
      }
      else if (user.role.toLowerCase().includes('admin')){
        dbRobotehResources[0].children[11].children.push({ id: 'user' + user.id, name: user.name + ' ' + user.surname, role: user.role, color: user.color })
      }
      else if (user.role.toLowerCase().includes('strojni mon')){
        dbRobotehResources[0].children[12].children.push({ id: 'user' + user.id, name: user.name + ' ' + user.surname, role: user.role, color: user.color })
      }
      else if (user.role.toLowerCase().includes('cnc operater')){
        dbRobotehResources[0].children[13].children.push({ id: 'user' + user.id, name: user.name + ' ' + user.surname, role: user.role, color: user.color })
      }
      else if (user.role.toLowerCase().includes('varil')){
        dbRobotehResources[0].children[14].children.push({ id: 'user' + user.id, name: user.name + ' ' + user.surname, role: user.role, color: user.color })
      }
      else if (user.role.toLowerCase().includes('tudent')){
        dbRobotehResources[0].children[15].children.push({ id: 'user' + user.id, name: user.name + ' ' + user.surname, role: user.role, color: user.color })
      }
      else {
        dbRobotehResources[0].children[16].children.push({ id: 'user' + user.id, name: user.name + ' ' + user.surname, role: user.role, color: user.color })
      }
    }
    // make checkboxes for each user and their role for filtering
    appendChekboxes();
    initCalendar();

    // call function to create select2 workers for task creation
    // debugger;
    let workers = users.filter(w => w.active && w.role.toLowerCase().substring(0,4) != 'info');
    for (const worker of workers) {
      worker.text = worker.name + ' ' + worker.surname;
    }
    $(".select2-workers").select2({
      data: users.filter(w => w.active && w.role.toLowerCase().substring(0,4) != 'info'),
      tags: true,
      multiple: true,
      dropdownParent: $('#work-order-popup'),
    });
    $("#absenceWorker").select2({
      data: users.filter(w => w.active && w.role.toLowerCase().substring(0,4) != 'info'),
      tags: false,
      multiple: false,
      dropdownParent: $('#work-order-popup'),
    });
  });
  // get all reasons for absences
  $.get( "/apiv2/absences/reasons", {}, function( data ) {
    //console.log(data)
    reasons = data.reasons;
    // for (const reason of reasons) {
    //   reason.text = reason.reason;
    // }
    $(".select2-reasons").select2({
      data: reasons,
      // tags: false,
      // multiple: false,
      minimumResultsForSearch: Infinity,
      dropdownParent: $('#work-order-popup'),
    });
  });
  // prepare/setup calendar/timeline and all it's componentes (color picker, date&time picker)
  // theme for calander
  mobiscroll.setOptions({
    theme: 'material',
    themeVariant: 'light'
  });
  // calendar setup
  // initCalendar();
  let disabledLabel = loggedUser.role == 'admin' || loggedUser.role.substring(0,5) == 'vodja' ? '' : ' disabled';
  // popup for adding and editing setup
  popup = mobiscroll.popup('#work-order-popup', {
    // headerText: 'header vrstica koda',
    headerText: `<ul class="nav nav-tabs">
      <li class="nav-item col-4 pl-1">
        <a class="nav-link active" id="eventTab" data-toggle="tab" href="#eventPopup">
          <h5 class="font-weight-bold" id="eventsPopupHeader">Dogodek</h5>
        </a>
      </li>
      <li class="nav-item col-4 pl-1">
        <a class="nav-link ` + disabledLabel + `" id="taskTab" data-toggle="tab" href="#taskPopup">
          <h5 class="font-weight-bold" id="tasksPopupHeader">Opravilo</h5>
        </a>
      </li>
      <li class="nav-item col-4 pl-1">
        <a class="nav-link disabled" id="absenceTab" data-toggle="tab" href="#absencePopup">
          <h5 class="font-weight-bold" id="tasksPopupHeader">Odsotnost</h5>
        </a>
      </li>
    </ul>`,
    display: 'bottom',
    contentPadding: false,
    fullScreen: true,
    scrollLock: false,
    onInit: function (event, inst) {

    },
    onOpen: function (event, inst) {
      // on change of tab in case of editing event/task user probably wants to transform to other one
      $('#taskTab').on('click', function(){
        if (tempEditEvent && Object.keys(tempEditEvent).length === 0 && tempEditEvent.constructor === Object){
          // tempEditEvent is empty
          return;
        }
        // check if object is event -> then try filling the data of new task based on event
        if(tempEditEvent.id.substring(0,5) == 'event'){
          // console.log('user wants to tranform event to task');
          debugger;

          let projectNumberInput = locationInput.value.match(/(RT\d*|STR\d*)/g);
          if(projectNumberInput && projectNumberInput.length >= 1){
            let tmpProject = projects.find(p => p.project_number == locationInput.value.match(/(RT\d*|STR\d*)/g)[0]);
            if(tmpProject){
              $('#taskProject').val(tmpProject.id).trigger('change');
            }
          }
          else $('#taskProject').val(0).trigger('change');
          notesTextarea.value ? $('#taskName').val(notesTextarea.value) : $('#taskName').val('');
          $('#taskStart').val(new Date(tempEditEvent.start).toISOString().substring(0,10));
          $('#taskFinish').val(new Date(tempEditEvent.end).toISOString().substring(0,10));
          let workers = [];
          for (const user of tempEditEvent.resource) {
            workers.push(user.substring(4));
          }
          $('#taskWorkers').val(workers).trigger('change');
          $('#taskNote').val(tempEditEvent.title);
        }
      })
      $('#eventTab').on('click', function(){
        if (tempEditEvent && Object.keys(tempEditEvent).length === 0 && tempEditEvent.constructor === Object){
          // tempEditEvent is empty
          return;
        }
        if(tempEditEvent.id.substring(0,4) == 'task'){
          // console.log('user wants to tranform task to event');
          debugger;
          titleInput.value = tempEditEvent.subscriber ? tempEditEvent.subscriber : titleInput.value;
          titleInput.value = tempEditEvent.sub_name ? tempEditEvent.sub_name : titleInput.value;
          locationInput.value = tempEditEvent.project_name ? tempEditEvent.project_number + ' - ' + tempEditEvent.project_name : locationInput.value;
          notesTextarea.value = tempEditEvent.task_name;
          
          mobiscroll.getInst(allDaySwitch).checked = true;
          tempEditEvent.allDay ? $('#workOrderStart').prop('type', 'date') : $('#workOrderStart').prop('type', 'datetime-local');
          tempEditEvent.allDay ? $('#workOrderEnd').prop('type', 'date') : $('#workOrderEnd').prop('type', 'datetime-local');
          tempEditEvent.allDay ? $('#workOrderStart').val(moment(tempEditEvent.start).format("YYYY-MM-DD")) : $('#workOrderStart').val(moment(tempEditEvent.start).format("YYYY-MM-DDTHH:mm"));
          tempEditEvent.allDay ? $('#workOrderEnd').val(moment(tempEditEvent.end).format("YYYY-MM-DD")) : $('#workOrderEnd').val(moment(tempEditEvent.end).format("YYYY-MM-DDTHH:mm"));
          // setCheckboxes(tempEditEvent.resource);
          let workers = [];
          for (const user of tempEditEvent.resource) {
            workers.push(user.substring(4));
          }
          $('#eventWorkers').val(workers).trigger('change');
          selectColor(tempEditEvent.color || getResource(tempEditEvent.resource).color, true);
          
          if(titleInput.value) $('#work-order-title').parent().parent().find('.mbsc-label-floating').addClass('mbsc-label-floating-active');
          if(locationInput.value) $('#work-order-location').parent().parent().find('.mbsc-label-floating').addClass('mbsc-label-floating-active');
          if(notesTextarea.value) $('#work-order-notes').parent().parent().find('.mbsc-label-floating').addClass('mbsc-label-floating-active');
        }
      })
    },
    onClose: function () {
      if (deleteEvent) {
        calendar.removeEvent(tempEvent);
      } else if (restoreEvent) {
        calendar.updateEvent(oldEvent);
      }
    },
    responsive: {
      medium: {
        display: 'anchored',
        width: 520,
        fullScreen: false,
        touchUi: false
      }
    }
  });
  // delete on tooltip event calls delete event for task or event based on id
  deleteTooltipButton.addEventListener('click', () => {
    // debugger;
    tempEvent.id.substring(0,4) === 'task' ? deleteTaskButton.click() : deleteButton.click();
    tooltip.close();
  })
  // popup update title on opening popup for editing
  titleInput.addEventListener('input', function (ev) {
    // update current event's title
    tempEvent.title = ev.target.value;
  });
  
  // popup update location on opening popup for editing
  locationInput.addEventListener('input', function (ev) {
    // update current event's title
    tempEvent.location = ev.target.value;
  });
  
  // popup update notes on opening popup for editing
  notesTextarea.addEventListener('change', function (ev) {
    // update current event's title
    tempEvent.notes = ev.target.value;
  });
  
  document.querySelectorAll('input[name=event-status]').forEach(function (elm) {
    elm.addEventListener('change', function () {
        // update current event's free property
        tempEvent.free = mobiscroll.getInst(freeSegmented).checked;
    });
  });

  // event on unvalidating selected event
  unvalidButton.addEventListener('click', function (args) {
    // var tmpE = args.event;

    // var data = {eventId: tempEvent.id, activity: false};
    tempEvent.unvalid = !tempEvent.unvalid;
    let data = {eventId: tempEvent.real_id, start: tempEvent.start, end: tempEvent.end, title: tempEvent.title, resource: tempEvent.resource, location: tempEvent.location, notes: tempEvent.notes, allDay : tempEvent.allDay, color: tempEvent.color, unvalid: tempEvent.unvalid};
    $.ajax({
      type: 'PUT',
      url: '/apiv2/events/'+tempEvent.real_id,
      contentType: 'application/json',
      data: JSON.stringify(data), // access in body
    }).done(function (resp) {
      calendar.removeEvent(tempEvent);
      setTimeout(() => {
        calendar.addEvent(tempEvent);
      }, 200);
      popup.close();
      if (isMobileDevice()){
        $('.navbar').show();
        $('.notification-bar').show();
      }
    

    }).fail(function (resp) {
      //console.log('FAIL');
      //debugger;
      popup.close();
      if (isMobileDevice()){
        $('.navbar').show();
        $('.notification-bar').show();
      }
      $('#errorModalMsg').text('Napaka pri posodobitvi dogodka v podatkovni bazi. Osvežite stran in ob ponovni napaki obvestite admninistratorja RIS-a.')
      $('#modalError').modal('toggle');
    })
    
  });
  
  // event on delete selected event
  deleteButton.addEventListener('click', function () {
    // var tmpE = args.event;

    var data = {eventId: tempEvent.real_id, activity: false};
    $.ajax({
      type: 'DELETE',
      url: '/apiv2/events/'+tempEvent.real_id,
      contentType: 'application/json',
      data: JSON.stringify(data), // access in body
    }).done(function (resp) {
      // console.log('SUCCESS');
      // delete current event on button click and successful delete in database
      calendar.removeEvent(tempEvent);
      popup.close();
      if (isMobileDevice()){
        $('.navbar').show();
        $('.notification-bar').show();
      }
    
      // save a local reference to the deleted event
      var deletedEvent = tempEvent;
    
      mobiscroll.snackbar({        
        button: {
          action: function () {
            // undo delete - update active to true and add event back to calendar on success
            var data = {eventId: deletedEvent.real_id, activity: true};
            $.ajax({
              type: 'DELETE',
              url: '/apiv2/events/'+deletedEvent.real_id,
              contentType: 'application/json',
              data: JSON.stringify(data), // access in body
            }).done(function (resp) {
              // console.log('SUCCESS');
              // delete current event on button click and successful delete in database
              calendar.addEvent(deletedEvent);

            }).fail(function (resp) {
              //console.log('FAIL');
              //debugger;
              popup.close();
              if (isMobileDevice()){
                $('.navbar').show();
                $('.notification-bar').show();
              }
              $('#errorModalMsg').text('Napaka pri razveljavitvi brisanju dogodka v podatkovni bazi. Osvežite stran in ob ponovni napaki obvestite admninistratorja RIS-a.')
              $('#modalError').modal('toggle');
            })
          },
          text: 'Razveljavi'
        },
        message: 'Dogodek je odstranjen'
      });

    }).fail(function (resp) {
      //console.log('FAIL');
      //debugger;
      popup.close();
      if (isMobileDevice()){
        $('.navbar').show();
        $('.notification-bar').show();
      }
      $('#errorModalMsg').text('Napaka pri brisanju dogodka v podatkovni bazi. Osvežite stran in ob ponovni napaki obvestite admninistratorja RIS-a.')
      $('#modalError').modal('toggle');
    })
    
  });
  // event on delete selected event
  deleteTaskButton.addEventListener('click', function () {
    // var tmpE = args.event;

    var data = {eventId: tempEvent.real_id, active: false};
    $.ajax({
      type: 'DELETE',
      url: '/apiv2/tasks/'+tempEvent.real_id,
      contentType: 'application/json',
      data: JSON.stringify(data), // access in body
    }).done(function (resp) {
      // console.log('SUCCESS');
      // delete current event on button click and successful delete in database
      calendar.removeEvent(tempEvent);
      popup.close();
      if (isMobileDevice()){
        $('.navbar').show();
        $('.notification-bar').show();
      }
    
      // save a local reference to the deleted event
      var deletedEvent = tempEvent;
    
      mobiscroll.snackbar({        
        button: {
          action: function () {
            // undo delete - update active to true and add event back to calendar on success
            var data = {eventId: deleteEvent.real_id, active: true};
            $.ajax({
              type: 'DELETE',
              url: '/apiv2/tasks/'+deleteEvent.real_id,
              contentType: 'application/json',
              data: JSON.stringify(data), // access in body
            }).done(function (resp) {
              // console.log('SUCCESS');
              // delete current event on button click and successful delete in database
              calendar.addEvent(deletedEvent);

            }).fail(function (resp) {
              //console.log('FAIL');
              //debugger;
              popup.close();
              if (isMobileDevice()){
                $('.navbar').show();
                $('.notification-bar').show();
              }
              $('#errorModalMsg').text('Napaka pri razveljavitvi brisanju opravila v podatkovni bazi. Osvežite stran in ob ponovni napaki obvestite admninistratorja RIS-a.')
              $('#modalError').modal('toggle');
            })
          },
          text: 'Razveljavi'
        },
        message: 'Opravilo je odstranjeno'
      });

    }).fail(function (resp) {
      //console.log('FAIL');
      //debugger;
      popup.close();
      if (isMobileDevice()){
        $('.navbar').show();
        $('.notification-bar').show();
      }
      $('#errorModalMsg').text('Napaka pri brisanju opravila v podatkovni bazi. Osvežite stran in ob ponovni napaki obvestite admninistratorja RIS-a.')
      $('#modalError').modal('toggle');
    })
    
  });
  // colorpicker setuop
  colorPicker = mobiscroll.popup('#demo-event-color', {
    display: 'bottom',
    contentPadding: false,
    showArrow: false,
    showOverlay: false,
    buttons: [
      'cancel',
      {
        text: 'Set',
        keyCode: 'enter',
        handler: function (ev) {
          setSelectedColor();
        },
        cssClass: 'mbsc-popup-button-primary'
      }
    ],
    responsive: {
      medium: {
        display: 'anchored',
        anchor: document.getElementById('event-color-cont'),
        buttons: [],
      }
    }
  });

  // event for selecting color in colorpicker
  colorSelect.addEventListener('click', function () {
    selectColor(tempEvent.color || getResource(tempEvent.resource).color || '#1a73e8s');
    colorPicker.open();
  });
  
  // adding call events to each color in popup for colorpicker
  colorElms.forEach(function (elm) {
    elm.addEventListener('click', function () {
      tempColor = elm.getAttribute('data-value');
      selectColor(tempColor);

      if (!colorPicker.s.buttons.length) {
        setSelectedColor();
      }
    });
  });
  
  // event for switching to all day
  allDaySwitch.addEventListener('change', function () {
    var checked = this.checked
    // change range settings based on the allDay
    // range.setOptions({
    //     controls: checked ? ['date'] : ['datetime'],
    //     responsive: checked ? datePickerResponsive : datetimePickerResponsive
    // });

    // update current event's allDay property
    checked ? $('#workOrderStart').prop('type', 'date') : $('#workOrderStart').prop('type', 'datetime-local');
    checked ? $('#workOrderEnd').prop('type', 'date') : $('#workOrderEnd').prop('type', 'datetime-local');
    checked ? $('#workOrderStart').val(moment(tempEvent.start).format("YYYY-MM-DD")) : $('#workOrderStart').val(moment(tempEvent.start).format("YYYY-MM-DDT07:00"));
    checked ? $('#workOrderEnd').val(moment(tempEvent.end).format("YYYY-MM-DD")) : $('#workOrderEnd').val(moment(tempEvent.end).format("YYYY-MM-DDT15:00"));
    tempEvent.allDay = checked;
  });
  let tooltipButtons = isSimpleMobileCheck() ? ['cancel'] : [];
  tooltip = mobiscroll.popup('#custom-event-tooltip-popup', {
    scrollLock: false,
    display: 'anchored',
    touchUi: false,
    showOverlay: false,
    contentPadding: false,
    closeOnOverlayClick: false,
    buttons: tooltipButtons,
    cancelText: 'Zapri',
    width: 350,
    onInit: function () {
      tooltip.addEventListener('mouseenter', function (e) {
        if (timer) {
          clearTimeout(timer);
          timer = null;
        }
      });

      tooltip.addEventListener('mouseleave', function () {
        timer = setTimeout(function () {
          tooltip.close();
        }, 200);
      });
    }
  });
  // appendChekboxes();
  // GET all projects for select2 (task creation and project select to append to correct one)
  let data = {active:1};
  // adding project tasks
  $.ajax({
    type: 'GET',
    url: '/apiv2/projects',
    contentType: 'application/x-www-form-urlencoded',
    data: data, // access in body
  }).done(function (resp) {
    projects = resp.projects; // add filter active if no active as query parameter in request
    for (const project of projects) {
      project.text = project.project_number + ' - ' + project.project_name;
    }
    $(".select2-project").prepend('<option></option>').select2({
      data: projects,
      tags: false,
      placeholder: "Izberi projekt",
      allowClear: false,
      dropdownParent: $('#work-order-popup'),
    });
  }).fail(function (resp) {
    //console.log('FAIL');
    //debugger;
    
    // $('#modalError').modal('toggle');
  })
  // GET all categories for select2 (task creation and editing)
  $.ajax({
    type: 'GET',
    url: '/apiv2/categories',
    contentType: 'application/x-www-form-urlencoded',
    data: [],
  }).done(function (resp) {
    categories = resp.data;
    $(".select2-category").select2({
      data: categories,
      tags: false,
      minimumResultsForSearch: Infinity,
      allowClear: false,
      dropdownParent: $('#work-order-popup'),
    });
    // make checkboxes for categories for filtering
    appendCategoryChekboxes();
  }).fail(function (resp) {
    //console.log('FAIL');
    //debugger;
    
    // $('#modalError').modal('toggle');
  })

  // addTaskBtnOR on click get all data and call addNewTask
  $('#addTaskBtnOR').on('click', function(){
    let projectId = $('#taskProject').val();
    let finish = $('#taskFinish').val();
    let start = $('#taskStart').val();
    let workers = $('#taskWorkers').val();
    let name = $('#taskName').val();
    let category = $('#taskCategory').val();
    let notes = $("#taskNote").val();
    let duration = start && finish ? calcDurationDays(start, finish) : 0;
    // console.log('konflikti override, vseeno klici funkcijo za dodajanje opravila');
    let data = {start, finish, duration, name, workers: workers.toString(), projectId, notes, category};
    addNewTask(data);
  })
  // editTaskBtnOR on click get all data and call addNewTask
  $('#editTaskBtnOR').on('click', function(){
    let projectId = $('#taskProject').val();
    let finish = $('#taskFinish').val();
    let start = $('#taskStart').val();
    let workers = $('#taskWorkers').val();
    let name = $('#taskName').val();
    let category = $('#taskCategory').val();
    let notes = $("#taskNote").val();
    let duration = start && finish ? calcDurationDays(start, finish) : 0;
    // console.log('konflikti override, vseeno klici funkcijo za dodajanje opravila');
    let data = {start, finish, duration, name, workers: workers.toString(), projectId, notes, category};
    editTask(data);
  })
  // dragTaskBtnOR on click get data and update event
  $('#dragTaskBtnOR').on('click', function(){
    debugger;
    // user wants to override, update with drag&drop anyway
    // console.log('vseeno posodobi opravilo preko drag akcije');
    let tmpStart = tmpDragEvent.start.toISOString().substring(0,10);
    let tmpEnd = tmpDragEvent.end.toISOString().substring(0,10);
    let duration = calcDurationDays(tmpStart, tmpEnd);
    let workers = tmpDragEvent.resource.map(r => r.substring(4));
    let data = {start: tmpStart, finish: tmpEnd, duration, name: tmpDragEvent.task_name, workers: workers.toString(), projectId: tmpDragEvent.project_id, notes: tmpDragEvent.task_note, category: tmpDragEvent.category_id, weekend: tmpDragEvent.weekend, priority: tmpDragEvent.priority_id, active: tmpDragEvent.active};
    $.ajax({
      type: 'PUT',
      url: '/apiv2/tasks/'+tmpDragEvent.real_id,
      contentType: 'application/json',
      data: JSON.stringify(data), // access in body
    }).done(function (resp) {
      
      $('#modalDragConflicts').modal('toggle');
    }).fail(function (resp) {
      //console.log('FAIL');
      //debugger;
      $('#errorModalMsg').text('Napaka pri posodobitvi podatkov v podatkovni bazi. Osvežite stran in ob ponovni napaki obvestite admninistratorja RIS-a.')
      $('#modalError').modal('toggle');
    })
  })
  // on modalDragConflicts close so the event who was not overrided will be put back to its place
  $('#modalDragConflicts').on('click', function(args){
    if (args.target.id == 'modalDragConflicts'){
      // debugger;
      calendar.updateEvent(tmpOldDragEvent);
    }
  })
  $('#closeDragModal').on('click', function(args){
    debugger;
    calendar.updateEvent(tmpOldDragEvent);
  })
  $("#modalDragConflicts").on('keydown', function ( e ) {
    var key = e.which || e.keyCode;
    if (key == 27) {
      // debugger;
      calendar.updateEvent(tmpOldDragEvent);
    }
  });
  setTimeout(() => {
    $('.mbsc-calendar-controls.mbsc-material').addClass('row');
  }, 300);
  // set range value if previously selected custom range
  setTimeout(() => {
    localStorage.calendarSelectRange && localStorage.calendarSelectRange == 'custom' && localStorage.calendarRangeStart && localStorage.calendarRangeEnd ? selectRange.setOptions({data: [ { text: 'Dan', value: 'day', disabled: false }, { text: 'Teden', value: 'week', disabled: false }, { text: 'Teden (razširjeno)', value: 'weekExt', disabled: false }, { text: '2 tedna', value: '2week', disabled: false }, { text: '4 Tedne', value: 'weekMonth', disabled: false }, { text: 'Mesec', value: 'month', disabled: false }, { text: '2 meseca', value: '2month', disabled: false }, { text: 'Pol leta', value: '6month', disabled: false }, { text: 'Leto', value: 'year', disabled: false }, { text: 'Ročno', value: 'custom', disabled: false }]}) : selectRange.setOptions({data: [ { text: 'Dan', value: 'day', disabled: false }, { text: 'Teden', value: 'week', disabled: false }, { text: 'Teden (razširjeno)', value: 'weekExt', disabled: false }, { text: '2 tedna', value: '2week', disabled: false }, { text: '4 Tedne', value: 'weekMonth', disabled: false }, { text: 'Mesec', value: 'month', disabled: false }, { text: '2 meseca', value: '2month', disabled: false }, { text: 'Pol leta', value: '6month', disabled: false }, { text: 'Leto', value: 'year', disabled: false }, { text: 'Ročno', value: 'custom', disabled: true }]});
    if (localStorage.calendarSelectRange && localStorage.calendarSelectRange == 'custom' && localStorage.calendarRangeStart && localStorage.calendarRangeEnd)
      setTimeout(() => { selectRange.setVal(localStorage.calendarSelectRange); }, 200);
    else if (localStorage.calendarSelectRange && localStorage.calendarSelectRange == 'day')
      setTimeout(() => { selectRange.setVal(localStorage.calendarSelectRange); }, 200);
    else if (localStorage.calendarSelectRange && localStorage.calendarSelectRange == '2week')
      setTimeout(() => { selectRange.setVal(localStorage.calendarSelectRange); }, 200);
    else if (localStorage.calendarSelectRange && localStorage.calendarSelectRange == 'week')
      setTimeout(() => { selectRange.setVal(localStorage.calendarSelectRange); }, 200);
    else if (localStorage.calendarSelectRange && localStorage.calendarSelectRange == 'weekExt')
      setTimeout(() => { selectRange.setVal(localStorage.calendarSelectRange); }, 200);
    else if (localStorage.calendarSelectRange && localStorage.calendarSelectRange == 'weekMonth')
      setTimeout(() => { selectRange.setVal(localStorage.calendarSelectRange); }, 200);
    else if (localStorage.calendarSelectRange && localStorage.calendarSelectRange == 'month')
      setTimeout(() => { selectRange.setVal(localStorage.calendarSelectRange); }, 200);
    else if (localStorage.calendarSelectRange && localStorage.calendarSelectRange == '2month')
      setTimeout(() => { selectRange.setVal(localStorage.calendarSelectRange); }, 200);
    else if (localStorage.calendarSelectRange && localStorage.calendarSelectRange == '6month')
      setTimeout(() => { selectRange.setVal(localStorage.calendarSelectRange); }, 200);
    else if (localStorage.calendarSelectRange && localStorage.calendarSelectRange == 'year')
      setTimeout(() => { selectRange.setVal(localStorage.calendarSelectRange); }, 200);
    else
      setTimeout(() => { selectRange.setVal('2week'); }, 200);
  }, 500);
  // setting interval for refreshing data when 5 minutes has passed since last data update
  setInterval(() => {
    refreshTimerCount++;
    if (refreshTimerCount >= 300){
      let tmpS = moment(calendar._firstDay).add(-1, 'days').format("YYYY-MM-DDTHH:mm");
      let tmpE = moment(calendar._lastDay).add(1, 'days').format("YYYY-MM-DDTHH:mm");
      getData(tmpS,tmpE);
      console.log('refresh data');
      refreshTimerCount = 0;
    }
  }, 1000);
})
// returns the formatted date
function getFormattedRange(start, end) {
  return formatDate('MMM D, YYYY', new Date(start)) + (end && getNrDays(start, end) > 1 ? (' - ' + formatDate('MMM D, YYYY', new Date(end))) : '');
}
// returns the number of days between two dates
function getNrDays(start, end) {
  return Math.round(Math.abs((end.setHours(0) - start.setHours(0)) / (24 * 60 * 60 * 1000))) + 1;
}
// function to change from event to tasks and other way around
function changeDataOnCalendar(){
  showProjectTasks = $('#changeToProjectTasks').prop('checked');
  showEvents = $('#changeToEvents').prop('checked');
  showMyStuff = $('#changeToMyStuff').prop('checked');
  showAbsences = $('#changeToAbsences').prop('checked');

  let tmpS = moment(calendar._firstDay).add(-1, 'days').format("YYYY-MM-DDTHH:mm");
  let tmpE = moment(calendar._lastDay).add(1, 'days').format("YYYY-MM-DDTHH:mm");

  allData = [];
  getData(tmpS, tmpE);
  // getTasks(tmpS, tmpE);
  // getEvents(tmpS, tmpE);
  // if (showProjectTasks){
  //   // calendar.setOptions({clickToCreate: false, dragToCreate: false});
    
  // }
  // if (showEvents){
  //   // calendar.setOptions({clickToCreate: true, dragToCreate: true});
  // }
  // console.log('switcharoo')
}
// get and return events
function getEvents(start, end){
  $.get( "/apiv2/events", {start, end}, function( data ) {
    //console.log(data)
    events = data.events;
    for (const event of events) {
      event.editable = event.my_event;
      // event.start = event.og_start;
      event.iconType = event.editable ? 'lock-open' : 'lock';
      if (loggedUser.role == 'admin')
        event.editable = true;
      // hacking allDay because there is/was bug where events with allDay set to true overlap with eachother
      // if (event.allDay == true) {
      //   event.all_day = event.allDay == true ? true : false;
      //   event.allDay = false;
      //   event.start = moment(event.start).format("YYYY-MM-DDT00:00");
      //   event.end = moment(event.end).format("YYYY-MM-DDT23:00");
      // }
    }
    // add it to allData so both tasks and events can be seen
    if (showEvents){
      allData = allData.concat(events);
    }
    // filter my creation/stuff if enabled
    if ( showMyStuff ) {
      allData = allData.filter(d => d.owner_name == loggedUser.name + ' ' + loggedUser.surname);
    }
    calendar.setEvents(allData);
    // calendar.setEvents(events.filter(e => ( new Date(e.start) <= new Date(calendar._firstDay) && new Date(e.end) >= new Date(calendar._lastDay) ) 
    // || ( new Date(e.end) >= new Date(calendar._firstDay) && new Date(e.end) <= new Date(calendar._lastDay) ) 
    // || ( new Date(e.start) >= new Date(calendar._firstDay) && new Date(e.start) <= new Date(calendar._lastDay) )));
  });
}
// get and return tasks
function getTasks(start, end){
  let data = {active:1, start, end};
  // adding project tasks
  $.ajax({
    type: 'GET',
    url: '/apiv2/tasks',
    contentType: 'application/x-www-form-urlencoded',
    data: data, // access in body
  }).done(function (resp) {
    projectTasks = resp.tasks.filter(t => t.workers);
    for (const task of projectTasks) {
      // color se bo rihtala preko category
      task.color = 'royalblue';
      task.allDay = true;
      task.editable = false; 
      task.iconType = 'lock';
      if (task.project_id){
        task.title = task.project_number + ' - ' + task.project_name;
        task.location = task.subscriber;
      }
      else{
        task.title = 'Servis - Storitev';
        task.location = task.sub_name;
      }
      task.notes = task.task_name;
      task.resource = [];
      task.unvalid = false;
      task.start = task.task_start;
      task.end = task.task_finish;
      task.owner_name = task.author;
      task.owner = task.author.id;
      let resource = task.workers_id.split(',');
      for (const tmp of resource) {
        task.resource.push('user' + tmp);
      }
    }
    // add it to allData so both tasks and events can be seen
    if (showProjectTasks){
      allData = allData.concat(projectTasks);
    }
    // filter my creation/stuff if enabled
    if ( showMyStuff ) {
      allData = allData.filter(d => d.owner_name == loggedUser.name + ' ' + loggedUser.surname);
    }
    calendar.setEvents(allData);
  }).fail(function (resp) {
    //console.log('FAIL');
    //debugger;
    
    $('#modalError').modal('toggle');
  })
}
// get and return tasks
function getData(start, end){
  let data = {getTasks: showProjectTasks, getEvents: showEvents, getAbsences: showAbsences, start, end};
  // adding project tasks
  $.ajax({
    type: 'GET',
    url: '/apiv2/tasks/data',
    contentType: 'application/x-www-form-urlencoded',
    data: data, // access in body
  }).done(function (resp) {
    projectTasks = resp.tasks;
    events = resp.events;
    absences = resp.absences;
    // proccess tasks
    projectTasks = resp.tasks.filter(t => t.workers && filterCategories.find(f => f == t.category_id));
    for (const task of projectTasks) {
      // color se bo rihtala preko category
      task.real_id = task.id;
      task.id = 'task_' + task.id;
      task.color = 'royalblue';
      task.allDay = true;
      // debugger;
      (loggedUser.role.toLowerCase() == 'admin' || task.author == loggedUser.name + ' ' + loggedUser.surname) ? task.editable = true : task.editable = false;
      task.iconType = 'lock';
      if (task.project_id){
        task.title = task.project_number + ' - ' + task.project_name;
        task.location = task.subscriber;
      }
      else{
        task.title = 'Servis - Storitev';
        task.location = task.sub_name;
      }
      task.notes = task.task_name;
      task.resource = [];
      task.unvalid = false;
      task.start = task.task_start;
      task.end = task.task_finish;
      task.owner_name = task.author;
      task.owner = task.author_id;
      let resource = task.workers_id.split(',');
      for (const tmp of resource) {
        task.resource.push('user' + tmp);
      }
    }
    // proccess events
    for (const event of events) {
      event.real_id = event.id;
      event.id = 'event_' + event.id;
      event.editable = event.my_event;
      // event.start = event.og_start;
      event.iconType = event.editable ? 'lock-open' : 'lock';
      if (loggedUser.role == 'admin')
        event.editable = true;
    }
    // proccess absences
    for (const absence of absences) {
      absence.real_id = absence.id;
      absence.id = 'absence_' + absence.id;
      absence.title = absence.reason;
      absence.location = absence.reason.toUpperCase();
      absence.notes = absence.reason.toUpperCase();
      // TODO - add edit control for absences
      absence.editable = false;
      absence.iconType = (absence.reason.toLowerCase() == 'bolniška' || absence.reason.toLowerCase() == 'krvodajalska' ) ? 'clinic-medical' : 'umbrella-beach';

      absence.resource = ['user' + absence.id_user];
      absence.end = moment(absence.finish).add(1, 'days').format("YYYY-MM-DD");
      absence.start = moment(absence.start).format("YYYY-MM-DD");
      absence.allDay = true;
      absence.color =  (absence.reason.toLowerCase() == 'bolniška' || absence.reason.toLowerCase() == 'krvodajalska' ) ? '#a71201' : '#f0ad4e';
      absence.owner_name = absence.author;
      absence.owner = absence.author_id;
    }
    
    allData = projectTasks.concat(events);
    allData = allData.concat(absences);
    // filter my creation/stuff if enabled
    if ( showMyStuff ) {
      allData = allData.filter(d => d.owner_name == loggedUser.name + ' ' + loggedUser.surname);
    }
    calendar.setEvents(allData);
    refreshTimerCount = 0;
    $('#refreshData').blur();

  }).fail(function (resp) {
    //console.log('FAIL');
    //debugger;
    
    $('#modalError').modal('toggle');
  })
}

// setup the calendar - first init of calendar, in a function because need to wait for getting all resources and data/events
function initCalendar() {
  moment.tz.add('Europe/Belgrade|CET CEST|-10 -20|01010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010|-19RC0 3IP0 WM0 1fA0 1cM0 1cM0 1rc0 Qo0 1vmo0 1cM0 1cM0 1fA0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1fA0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1fA0 1o00 11A0 1o00 11A0 1o00 11A0 1qM0 WM0 1qM0 WM0 1qM0 11A0 1o00 11A0 1o00 11A0 1qM0 WM0 1qM0 WM0 1qM0 WM0 1qM0 11A0 1o00 11A0 1o00 11A0 1qM0 WM0 1qM0 WM0 1qM0 11A0 1o00 11A0 1o00 11A0 1o00 11A0 1qM0 WM0 1qM0 WM0 1qM0 11A0 1o00 11A0 1o00 11A0 1qM0 WM0 1qM0 WM0 1qM0 11A0 1o00 11A0 1o00 11A0 1o00 11A0 1qM0 WM0 1qM0 WM0 1qM0 11A0 1o00 11A0 1o00 11A0 1qM0 WM0 1qM0 WM0 1qM0 WM0 1qM0 11A0 1o00 11A0 1o00|12e5')
  mobiscroll.momentTimezone.moment = moment;

  // get saved view option if it exist in localStorage
  let viewType;
  let colorType = localStorage.calendarSelectRange && localStorage.calendarSelectRange == 'day' ?
  [{ slot: 1, allDay: 'true', start: new Date('2025-01-01'), end: new Date('2025-01-01'), cssClass: 'md-dots-bg' },{ slot: 1, allDay: 'true', start: new Date('2025-01-02'), end: new Date('2025-01-02'), cssClass: 'md-dots-bg' },{ slot: 1, allDay: 'true', start: new Date('2025-02-08'), end: new Date('2025-02-08'), cssClass: 'md-dots-bg' },{ slot: 1, allDay: 'true', start: new Date('2025-04-21'), end: new Date('2025-04-21'), cssClass: 'md-dots-bg' },{ slot: 1, allDay: 'true', start: new Date('2025-04-27'), end: new Date('2025-04-27'), cssClass: 'md-dots-bg' },{ slot: 1, allDay: 'true', start: new Date('2025-05-01'), end: new Date('2025-05-02'), cssClass: 'md-dots-bg' },{ slot: 1, allDay: 'true', start: new Date('2025-05-19'), end: new Date('2025-05-19'), cssClass: 'md-dots-bg' },{ slot: 1, allDay: 'true', start: new Date('2025-06-25'), end: new Date('2025-06-25'), cssClass: 'md-dots-bg' },{ slot: 1, allDay: 'true', start: new Date('2025-08-15'), end: new Date('2025-08-15'), cssClass: 'md-dots-bg' },{ slot: 1, allDay: 'true', start: new Date('2025-10-31'), end: new Date('2025-11-1 21:00'), cssClass: 'md-dots-bg' },{ slot: 1, allDay: 'true', start: new Date('2025-12-25'), end: new Date('2025-12-26'), cssClass: 'md-dots-bg' },{ slot: 1, allDay: 'true', start: new Date('2026-01-01'), end: new Date('2025-01-02'), cssClass: 'md-dots-bg' },{ slot: 1, allDay: 'true', start: new Date('2023-07-17'), end: new Date('2023-08-16'), cssClass: 'md-stripes-bg' },{ slot: 1, allDay: 'true', start: new Date('2023-04-24'), end: new Date('2023-04-28'), cssClass: 'md-rect-bg' },{ slot: 1, allDay: 'true', start: new Date('2023-12-27'), end: new Date('2023-12-29'), cssClass: 'md-rect-bg' },{ slot: 1, allDay: 'true', start: new Date('2023-10-30'), end: new Date('2023-10-30'), cssClass: 'md-rect-bg' },{background: '#a5ceff4d', slot: 1, recurring: { repeat: 'weekly', weekDays: 'SU,SA' }}]
  :
  [{ slot: 1, allDay: 'true', start: new Date('2025-01-01'), end: new Date('2025-01-01'), cssClass: 'md-dots-bg' },{ slot: 1, allDay: 'true', start: new Date('2025-01-02'), end: new Date('2025-01-02'), cssClass: 'md-dots-bg' },{ slot: 1, allDay: 'true', start: new Date('2025-02-08'), end: new Date('2025-02-08'), cssClass: 'md-dots-bg' },{ slot: 1, allDay: 'true', start: new Date('2025-04-21'), end: new Date('2025-04-21'), cssClass: 'md-dots-bg' },{ slot: 1, allDay: 'true', start: new Date('2025-04-27'), end: new Date('2025-04-27'), cssClass: 'md-dots-bg' },{ slot: 1, allDay: 'true', start: new Date('2025-05-01'), end: new Date('2025-05-02'), cssClass: 'md-dots-bg' },{ slot: 1, allDay: 'true', start: new Date('2025-05-19'), end: new Date('2025-05-19'), cssClass: 'md-dots-bg' },{ slot: 1, allDay: 'true', start: new Date('2025-06-25'), end: new Date('2025-06-25'), cssClass: 'md-dots-bg' },{ slot: 1, allDay: 'true', start: new Date('2025-08-15'), end: new Date('2025-08-15'), cssClass: 'md-dots-bg' },{ slot: 1, allDay: 'true', start: new Date('2025-10-31'), end: new Date('2025-11-1 21:00'), cssClass: 'md-dots-bg' },{ slot: 1, allDay: 'true', start: new Date('2025-12-25'), end: new Date('2025-12-26'), cssClass: 'md-dots-bg' },{ slot: 1, allDay: 'true', start: new Date('2026-01-01'), end: new Date('2026-01-02'), cssClass: 'md-dots-bg' },{ slot: 1, allDay: 'true', start: new Date('2023-07-17'), end: new Date('2023-08-16'), cssClass: 'md-stripes-bg' },{ slot: 1, allDay: 'true', start: new Date('2023-04-24'), end: new Date('2023-04-28'), cssClass: 'md-rect-bg' },{ slot: 1, allDay: 'true', start: new Date('2023-12-27'), end: new Date('2023-12-29'), cssClass: 'md-rect-bg' },{ slot: 1, allDay: 'true', start: new Date('2023-10-30'), end: new Date('2023-10-30'), cssClass: 'md-rect-bg' },{ date: moment().format('YYYY-MM-DD'), background: 'cornsilk' },{background: '#a5ceff4d', slot: 1, recurring: { repeat: 'weekly', weekDays: 'SU,SA' }, recurringException: [moment().format('YYYY-MM-DD')]}];
  let refDateSave = localStorage.calendarSelectRange && localStorage.calendarSelectRange == 'custom' && localStorage.calendarRangeStart && localStorage.calendarRangeEnd ? new Date(localStorage.calendarRangeStart) : new Date();
  if (localStorage.calendarSelectRange){
    if(localStorage.calendarSelectRange == 'day'){
      viewType = { timeline: { type: 'day', eventList: true, weekNumbers: true,}};
    }
    else if (localStorage.calendarSelectRange == '2week'){
      viewType = { timeline: { type: 'week', timeCellStep: 1440, timeLabelStep: 1440, startDay: 1, endDay: 0, size: 2, weekNumbers: true, }};
    }
    else if (localStorage.calendarSelectRange == 'week'){
      viewType = { timeline: { type: 'week', eventList: false, startDay: 1, endDay: 0, timeCellStep: 1440, timeLabelStep: 1440, weekNumbers: true, }};
    }
    else if (localStorage.calendarSelectRange == 'weekExt'){
      viewType = { timeline: { type: 'week', eventList: false, startDay: 1, endDay: 0, weekNumbers: true, }};
    }
    else if (localStorage.calendarSelectRange == 'weekMonth'){
      viewType = { timeline: { type: 'week', eventList: false, size: 4, startDay: 1, endDay: 0, timeCellStep: 1440, timeLabelStep: 1440, weekNumbers: true, }};
    }
    else if (localStorage.calendarSelectRange == 'month'){
      viewType = { timeline: { type: 'month', size: 1, weekNumbers: true, }};
    }
    else if (localStorage.calendarSelectRange == '2month'){
      viewType = { timeline: { type: 'month', size: 2, weekNumbers: true, }};
    }
    else if (localStorage.calendarSelectRange == '6month'){
      viewType = { timeline: { type: 'month', size: 6, weekNumbers: true, }};
    }
    else if (localStorage.calendarSelectRange == 'year'){
      viewType = { timeline: { type: 'month', size: 12, weekNumbers: true, }};
    }
    else if (localStorage.calendarSelectRange == 'custom'){
      if (localStorage.calendarRangeStart && localStorage.calendarRangeEnd){
        viewType = { timeline: { 
          type: 'day',
          size: getNrDays(new Date(localStorage.calendarRangeStart), new Date(localStorage.calendarRangeEnd)),
          eventList: true,
          weekNumbers: true,
        }};
      }
      else{
        viewType = { timeline: {
          type: 'week',
          timeCellStep: 1440,
          timeLabelStep: 1440,
          startDay: 1,
          endDay: 0,
          size: 2,
          weekNumbers: true,
        }};
      }
    }
    else {
      viewType = { timeline: {
        type: 'week',
        timeCellStep: 1440,
        timeLabelStep: 1440,
        startDay: 1,
        endDay: 0,
        size: 2,
        weekNumbers: true,
      }};
    }
  }
  else{
    viewType = { timeline: {
      type: 'week',
      timeCellStep: 1440,
      timeLabelStep: 1440,
      startDay: 1,
      endDay: 0,
      size: 2,
      weekNumbers: true,
    }};
  }

  calendar = mobiscroll.eventcalendar('#calendarOverview', {
    clickToCreate: clickToCreateVar,
    dragToCreate: dragToCreateVar,
    dragToMove: dragToMoveVar,
    dragToResize: dragToResizeVar,
    dragTimeStep: 60,
    timezonePlugin: mobiscroll.momentTimezone,
    dataTimezone: 'Europe/Belgrade',
    displayTimezone: 'Europe/Belgrade',
    view: viewType,
    refDate: refDateSave,
    selectedDate: refDateSave,
    locale: sloLocal,
    data: [],
    onPageLoading: function (event, calendar) {
      // var year = event.firstDay.getFullYear();
      // var month = event.firstDay.getMonth();
      let tmpS = moment(calendar._firstDay).add(-1, 'days').format("YYYY-MM-DDTHH:mm");
      let tmpE = moment(calendar._lastDay).add(1, 'days').format("YYYY-MM-DDTHH:mm");
      
      allData = [];
      getData(tmpS, tmpE);
      // if (showProjectTasks){
      //   getTasks(tmpS, tmpE);
      // }
      // if (showEvents){
      //   getEvents(tmpS, tmpE);
      // }
    },
    onPageLoaded: function (args) {
      setTimeout(() => {
        startDate = args.firstDay;
        end = args.lastDay;
        endDate = new Date(end.getFullYear(), end.getMonth(), end.getDate() - 1, 0);
        // set button text
        rangeButton = document.getElementById('custom-date-range-text');
        rangeButton.innerText = getFormattedRange(startDate, endDate);

        // Range picker
        // myRange for selectiong range on displaying calendar timeline
        myRange = mobiscroll.datepicker('#custom-date-range', {
          select: 'range',
          display: 'anchored',
          showOverlay: false,
          touchUi: true,
          buttons: [],
          firstDay: 1,
          locale: sloLocal,
          onClose: function (args, inst) {
            var date = inst.getVal();
            if (date[0] && date[1]) {
              if (date[0].getTime() !== startDate.getTime() || date[1].getTime() !== endDate.getTime()) {
                // navigate the calendar
                calendar.navigate(date[0]);
                // change display option to custom
                selectRange.setOptions({data: [ { text: 'Dan', value: 'day', disabled: false }, { text: 'Teden', value: 'week', disabled: false }, { text: 'Teden (razširjeno)', value: 'weekExt', disabled: false }, { text: 'Teden', value: 'week', disabled: false }, { text: '2 tedna', value: '2week', disabled: false }, { text: '4 Tedne', value: 'weekMonth', disabled: false }, { text: 'Mesec', value: 'month', disabled: false }, { text: '2 meseca', value: '2month', disabled: false }, { text: 'Pol leta', value: '6month', disabled: false }, { text: 'Leto', value: 'year', disabled: false }, { text: 'Ročno', value: 'custom', disabled: false }]});
                setTimeout(() => {
                  selectRange.setVal('custom');
                  localStorage.setItem('calendarSelectRange', 'custom');
                  localStorage.setItem('calendarRangeStart', date[0]);
                  localStorage.setItem('calendarRangeEnd', date[1]);
                }, 200);
              }
              startDate = date[0];
              endDate = date[1];
              // set calendar view
              calendar.setOptions({
                refDate: startDate,
                view: {
                  timeline: {
                    type: 'day',
                    size: getNrDays(startDate, endDate),
                    eventList: true
                  }
                },
                colors: [
                  { slot: 1, allDay: 'true', start: new Date('2025-01-01'), end: new Date('2025-01-01'), cssClass: 'md-dots-bg' },{ slot: 1, allDay: 'true', start: new Date('2025-01-02'), end: new Date('2025-01-02'), cssClass: 'md-dots-bg' },{ slot: 1, allDay: 'true', start: new Date('2025-02-08'), end: new Date('2025-02-08'), cssClass: 'md-dots-bg' },{ slot: 1, allDay: 'true', start: new Date('2025-04-21'), end: new Date('2025-04-21'), cssClass: 'md-dots-bg' },{ slot: 1, allDay: 'true', start: new Date('2025-04-27'), end: new Date('2025-04-27'), cssClass: 'md-dots-bg' },{ slot: 1, allDay: 'true', start: new Date('2025-05-01'), end: new Date('2025-05-02'), cssClass: 'md-dots-bg' },{ slot: 1, allDay: 'true', start: new Date('2025-05-19'), end: new Date('2025-05-19'), cssClass: 'md-dots-bg' },{ slot: 1, allDay: 'true', start: new Date('2025-06-25'), end: new Date('2025-06-25'), cssClass: 'md-dots-bg' },{ slot: 1, allDay: 'true', start: new Date('2025-08-15'), end: new Date('2025-08-15'), cssClass: 'md-dots-bg' },{ slot: 1, allDay: 'true', start: new Date('2025-10-31'), end: new Date('2025-11-1 21:00'), cssClass: 'md-dots-bg' },{ slot: 1, allDay: 'true', start: new Date('2025-12-25'), end: new Date('2025-12-26'), cssClass: 'md-dots-bg' },{ slot: 1, allDay: 'true', start: new Date('2026-01-01'), end: new Date('2026-01-02'), cssClass: 'md-dots-bg' },{ slot: 1, allDay: 'true', start: new Date('2023-07-17'), end: new Date('2023-08-16'), cssClass: 'md-stripes-bg' },{ slot: 1, allDay: 'true', start: new Date('2023-04-24'), end: new Date('2023-04-28'), cssClass: 'md-rect-bg' },{ slot: 1, allDay: 'true', start: new Date('2023-12-27'), end: new Date('2023-12-29'), cssClass: 'md-rect-bg' },{ slot: 1, allDay: 'true', start: new Date('2023-10-30'), end: new Date('2023-10-30'), cssClass: 'md-rect-bg' },
                  {background: '#a5ceff4d',slot: 1,recurring: { repeat: 'weekly', weekDays: 'SU,SA' },recurringException: [moment().format('YYYY-MM-DD')]},
                  { date: moment().format('YYYY-MM-DD'), background: 'cornsilk' },
                ],
              });
            } else {
              // myRange.setVal([startDate, endDate])
            }
          }
        });
        myRange.setVal([startDate, endDate]);
      }, 500);
    },
    resources: dbRobotehResources,
    colors: colorType,
    marked: [
      new Date(2022, 2, 15),
      new Date(2022, 2, 22),
      {
        start: new Date(2022, 2, 23),
        end: new Date(2022, 2, 24),
        color: 'red'
      },
      {
        color: 'green',
        recurring: { repeat: 'yearly', month: 12, day: 24 }
      }
    ],
    extendDefaultEvent: function () {
      return {
        title: '',
        location: '',
        iconType: 'lock-open',
        // color: '#1a73e8',
      };
    },
    onCellClick: function (args, inst) {
      if (isSimpleMobileCheck()){
        if (!timer) {
          timer = setTimeout(function () {
            tooltip.close();
          }, 200);
        }
      }
    },
    onEventClick: function (args) {
      (isSimpleMobileCheck()) ? onEventMobiscrollHoverIn(args) : onEventMobiscrollClick(args);
    },
    onEventUpdated: function (args) {
      // debugger
      if (args.event.id.substring(0, 4) == 'task'){
        let tmpE = args.event;
        tmpDragEvent = tmpE;
        tmpOldDragEvent = args.oldEvent;
        let tmpStart = args.event.start.toISOString().substring(0,10);
        let tmpEnd = args.event.end.toISOString().substring(0,10);
        let duration = calcDurationDays(tmpStart, tmpEnd);
        let workers = tmpE.resource.map(r => r.substring(4)).join(',');
        let data = {start: tmpStart, finish: tmpEnd, duration, name: tmpE.task_name, workers: workers, projectId: tmpE.project_id, notes: tmpE.task_note, category: tmpE.category_id, weekend: tmpE.weekend, priority: tmpE.priority_id, active: tmpE.active};
        debugger;
        // check conflicts
        $.ajax({
          type: 'GET',
          url: '/apiv2/tasks/conflicts',
          contentType: 'application/json',
          data: data, // access in body
        }).done(function (resp) {
          //console.log('SUCCESS');
          // if conflicts then open modal with conflicts otherwise add new task
          if (resp.conflicts.length > 0){
            // open modal with conflicts
            openDragConflictModal(resp.conflicts);
          }
          else{
            // update event, no conflicts
            // console.log('ni konfliktov, posodobi opravilo preko drag akcije');
            $.ajax({
              type: 'PUT',
              url: '/apiv2/tasks/'+tmpE.real_id,
              contentType: 'application/json',
              data: JSON.stringify(data), // access in body
            }).done(function (resp) {
              
            }).fail(function (resp) {
              //console.log('FAIL');
              //debugger;
              $('#errorModalMsg').text('Napaka pri posodobitvi podatkov v podatkovni bazi. Osvežite stran in ob ponovni napaki obvestite admninistratorja RIS-a.')
              $('#modalError').modal('toggle');
            })
          }
        }).fail(function (resp) {
          //console.log('FAIL');
          //debugger;
          $('#errorModalMsg').text('Napaka pri pridobivanju podaktov za konflikte. Osvežite stran in ob ponovni napaki obvestite admninistratorja RIS-a.')
          $('#modalError').modal('toggle');
        })
      }
      else if (args.event.id.substring(0, 5) == 'event'){
        if ( args.oldEvent.resource != args.event.resource && !(loggedUser.role.toLowerCase() == 'admin' ||  loggedUser.role.toLowerCase().includes('vodja')) ){
          // user does not have permission to create events for other users
          // a way around this is to create own event and then drag to other user and this make resource as it was before
          args.event.resource = args.oldEvent.resource;
          // and update event on calendar so user knows it was corrected
          calendar.updateEvent(args.event);
        }
        // popup.close();
        // // store temporary event
        // tempEvent = args.event;
        // createAddPopup(args.target);
        var tmpE = args.event;
        var data = {eventId: tmpE.id, start: tmpE.start, end: tmpE.end, title: tmpE.title, resource: tmpE.resource, location: tmpE.location, notes: tmpE.notes, allDay: tmpE.allDay, color: tmpE.color};
        $.ajax({
          type: 'PUT',
          url: '/apiv2/events/'+tmpE.real_id,
          contentType: 'application/json',
          data: JSON.stringify(data), // access in body
        }).done(function (resp) {
          // console.log('SUCCESS');
  
        }).fail(function (resp) {
          //console.log('FAIL');
          //debugger;
          // popup.close();
          $('#errorModalMsg').text('Napaka pri posodobitvi podatkov v podatkovni bazi. Osvežite stran in ob ponovni napaki obvestite admninistratorja RIS-a.')
          $('#modalError').modal('toggle');
        })
        // console.log('krneki');
        // update to new start and end because of draging element
      }
      else if (args.event.id.substring(0, 7) == 'absence'){
        // do nothing for now, no drag and drop for absences
      }
    },
    onEventCreated: function (args) {
      // popup.close();
      // // store temporary event
      // tempEvent = args.event;
      // if(args.action == 'click'){
      //   var tmp = new Date(tempEvent.start);
      //   tmp.setHours(23,59);
      //   tempEvent.end = tmp;
      // }
      // createAddPopup(args.target);
      if (loggedUser.role == 'admin' || loggedUser.role.includes('vodja')){
        // console.log('imam dovoljenje');
        popup.close();
        if (isMobileDevice()){
          $('.navbar').show();
          $('.notification-bar').show();
        }
        // store temporary event
        tempEvent = args.event;
        tempEvent.allDay = true;
        if(args.action == 'click'){
          var tmp = new Date(tempEvent.start);
          tmp.setHours(7,0);
          tempEvent.start = tmp.toISOString();
          tmp.setHours(15,0);
          tempEvent.end = tmp.toISOString();
        }
        createAddPopup(args.target);
      }
      else if (('user' + users.find(u => u.name == loggedUser.name && u.surname == loggedUser.surname).id) == args.event.resource){
        // console.log('imam dovoljenje');
        popup.close();
        if (isMobileDevice()){
          $('.navbar').show();
          $('.notification-bar').show();
        }
        // store temporary event
        tempEvent = args.event;
        tempEvent.allDay = true;
        if(args.action == 'click'){
          var tmp = new Date(tempEvent.start);
          tmp.setHours(7,0);
          tempEvent.start = tmp.toISOString();
          tmp.setHours(15,0);
          tempEvent.end = tmp.toISOString();
        }
        createAddPopup(args.target);
      }
      else{
        // console.log('nimam dovoljenje');
        calendar.removeEvent(args.event);
      }
      
    },
    onEventDeleted: function (args) {
      mobiscroll.snackbar({
        button: {
          action: function () {
            calendar.addEvent(args.event);
          },
          text: 'Razveljavi'
        },
        message: 'Dogodek je odstranjen'
      });
    },
    // custom on hover event
    onEventHoverIn: function (args, inst) {
      if (!(isSimpleMobileCheck())){
        onEventMobiscrollHoverIn(args);
      }
    },
    // custom hover out event
    onEventHoverOut: function (args) {
      if (!timer) {
        timer = setTimeout(function () {
          tooltip.close();
        }, 200);
      }
    },
    // custom event look
    renderScheduleEvent: function (data) {
      var ev = data.original;
      var color = data.color ? data.color: '#1a73e8';
      let colorClass = 'royalblue';
      switch (color) {
        case '#ffeb3c': colorClass = 'rumena'; break;
        case '#ff9900': colorClass = 'oranzna'; break;
        case '#f44437': colorClass = 'oranzna-rdeca'; break;
        case '#ea1e63': colorClass = 'crimson'; break;
        case '#9c26b0': colorClass = 'darkmagenta'; break;
        case '#3f51b5': colorClass = 'mediumblue'; break;
        case '#5ac8fa': colorClass = 'deepskyblue'; break;
        case '#009788': colorClass = 'seagreen'; break;
        case '#4baf4f': colorClass = 'limegreen'; break;
        case '#7e5d4e': colorClass = 'saddlebrown'; break;
        case '#fc03f4': colorClass = 'roza-poweranger'; break;
        case '#000000': colorClass = 'odsotnost'; break;
        case '#a71201': colorClass = 'bolniska'; break;
        case '#f0ad4e': colorClass = 'dopust'; break;
        default: colorClass = 'royalblue'; break;
      }
      let icon = ev.my_event ? '<span class="mbsc-icon mbsc-font-icon ' + colorClass + '-ozadje"><i class="fas fa-' + ev.iconType + ' text-white"></i></span>' : '<span class="mbsc-icon mbsc-font-icon ' + colorClass + '-ozadje"><i class="fas fa-' + ev.iconType + ' text-white"></i></span>';

      if (ev.unvalid){
        color = '#514d4d';
        icon = '<span class="mbsc-icon mbsc-font-icon ' + colorClass + '-ozadje"><i class="fas fa-times text-white"></i></span>'
      }
      // for project tasks
      if (ev.category){
        if (ev.category.toLowerCase() == 'nabava'){
          colorClass = 'nabava';
          icon = '<span class="mbsc-icon mbsc-font-icon ' + colorClass + '-ozadje"><i class="fas fa-' + 'shopping-cart' + ' text-white"></i></span>';
        }
        else if (ev.category.toLowerCase() == 'konstrukcija'){
          colorClass = 'konstrukcija';
          icon = '<span class="mbsc-icon mbsc-font-icon ' + colorClass + '-ozadje"><i class="fas fa-' + 'pencil-ruler' + ' text-white"></i></span>';
        }
        else if (ev.category.toLowerCase() == 'strojna izdelava' || ev.category.toLowerCase() == 'strojna montaža - roboteh' || ev.category.toLowerCase() == 'strojna montaža - stranke'){
          colorClass = 'strojna-izdelava';
          icon = '<span class="mbsc-icon mbsc-font-icon ' + colorClass + '-ozadje"><i class="fas fa-' + 'cog' + ' text-white"></i></span>';
        }
        else if (ev.category.toLowerCase() == 'elektro izdelava' || ev.category.toLowerCase() == 'elektro montaža - roboteh' || ev.category.toLowerCase() == 'elektro montaža - stranke'){
          colorClass = 'elektro-izdelava';
          icon = '<span class="mbsc-icon mbsc-font-icon ' + colorClass + '-ozadje"><i class="fas fa-' + 'bolt' + ' text-white"></i></span>';
        }
        else if (ev.category.toLowerCase() == 'montaža'){
          colorClass = 'montaza';
          icon = '<span class="mbsc-icon mbsc-font-icon ' + colorClass + '-ozadje"><i class="fas fa-' + 'wrench' + ' text-white"></i></span>';
        }
        else if (ev.category.toLowerCase() == 'programiranje'){
          colorClass = 'programiranje';
          icon = '<span class="mbsc-icon mbsc-font-icon ' + colorClass + '-ozadje"><i class="fas fa-' + 'laptop-code' + ' text-white"></i></span>';
        }
        else if (ev.category.toLowerCase() == 'brez'){
          colorClass = 'brez';
          icon = '<span class="mbsc-icon mbsc-font-icon ' + colorClass + '-ozadje"><i class="fas fa-' + 'slash' + ' text-white"></i></span>';
        }
        else {
          colorClass = 'royalblue';
          icon = '<span class="mbsc-icon mbsc-font-icon ' + colorClass + '-ozadje"><i class="fas fa-tasks text-white"></i></span>';
        }

        let timeColorLabel = ev.allDay != true ? '<span class="md-timeline-template-time ml-2 ' + colorClass + '-barva">' + data.start + '-' + data.end + '</span>' : '';
        let completionClass = ev.completion == 100 ? ' md-stripped-bg-compl' : '';
        return `<div class="mbsc-schedule-event-background mbsc-timeline-event-background mbsc-material ` + colorClass + `-ozadje"></div>
          <div class="mbsc-schedule-event-background mbsc-timeline-event-background mbsc-material omili-ozadje` + completionClass + `"></div>
          <div class="mbsc-schedule-event-inner mbsc-material md-timeline-template-event">
          <div class="md-timeline-template-event-cont">
            `+ icon +`
            ` +timeColorLabel + `
          <span class="md-timeline-template-title ml-2 mr-2">` + ev.title + `</span></div></div>`;
      }
      // for absences
      else if (ev.reason){
        let timeColorLabel = ev.allDay != true ? '<span class="md-timeline-template-time ml-2 ' + colorClass + '-barva">' + data.start + '-' + data.end + '</span>' : '';
        return `<div class="mbsc-schedule-event-background mbsc-timeline-event-background mbsc-material ` + colorClass + `-ozadje"></div>
          <div class="mbsc-schedule-event-background mbsc-timeline-event-background mbsc-material omili-ozadje"></div>
          <div class="mbsc-schedule-event-inner mbsc-material md-timeline-template-event">
          <div class="md-timeline-template-event-cont">
            ` + icon + `
            ` + timeColorLabel + `
          <span class="md-timeline-template-title ml-2 mr-2">` + ev.title + `</span></div></div>`;
      }
      else{
        // events
        let timeColorLabel = ev.allDay != true ? '<span class="md-timeline-template-time ml-2 ' + colorClass + '-barva">' + data.start + '-' + data.end + '</span>' : '';
        let completionClass = ev.completion ? ' md-stripped-bg-compl' : '';
        return `<div class="mbsc-schedule-event-background mbsc-timeline-event-background mbsc-material ` + colorClass + `-ozadje"></div>
          <div class="mbsc-schedule-event-background mbsc-timeline-event-background mbsc-material omili-ozadje` + completionClass + `"></div>
          <div class="mbsc-schedule-event-inner mbsc-material md-timeline-template-event">
          <div class="md-timeline-template-event-cont">
            ` + icon + `
            ` + timeColorLabel + `
          <span class="md-timeline-template-title ml-2 mr-2">` + ev.title + `</span></div></div>`;
      }

    },
    // custom resources
    renderResource: function (resource) {
      if (resource.img){
        return '<div class="employee-shifts-cont ml-n2">' +
            '<div class="employee-shifts-name">' + resource.name + '</div>' +
            '<img class="employee-shifts-avatar" src="' + resource.img + '"/>' +
            '</div>';
      }
      else{
        return '<div class="ml-n2">' +
            '<div class="">' + resource.name + '</div>' +
            '</div>';
      }
    },
    // custom header
    renderHeader: function () {
      return `<div id="custom-date-range-space" class="col-md-4 col-12">
          <div id="custom-date-range" class="w-210px">
            <button mbsc-button data-variant="flat" class="mbsc-calendar-button">
              <span id="custom-date-range-text" class="mbsc-calendar-title"></span>
            </button>
          </div>
        </div>
        <div class="col-md-8 col-12 row ml-0">
          <div class="md-shift-header-controls">
          <button class="btn btn-primary mt-2 mr-2" id="filterResources" data-toggle="tooltip" data-original-title="Filter zaposlenih">
            <i class="fas fa-filter fa-lg"></i>
          </button>
          <button class="btn btn-primary mt-2 mr-2 btn-on-stretched" id="refreshData" data-toggle="tooltip" data-original-title="Osveži">
            <i class="fas fa-sync fa-lg"></i>
          </button>
          <div class="custom-control custom-switch mt-2 mr-2">
            <input type="checkbox" class="custom-control-input" id="changeToMyStuff">
            <label class="custom-control-label" for="changeToMyStuff">Moji vnosi</label>
          </div>
          <div class="custom-control custom-switch mt-2 mr-2">
            <input type="checkbox" class="custom-control-input" id="changeToEvents" checked="">
            <label class="custom-control-label" for="changeToEvents">Dogodki</label>
          </div>
          <div class="custom-control custom-switch mt-2 mr-2">
            <input type="checkbox" class="custom-control-input" id="changeToProjectTasks" checked="">
            <label class="custom-control-label" for="changeToProjectTasks">Projektana opravila</label>
          </div>
          <div class="custom-control custom-switch mt-2">
            <input type="checkbox" class="custom-control-input" id="changeToAbsences" checked="">
            <label class="custom-control-label" for="changeToAbsences">Odsotnosti</label>
          </div>
          <label class="md-shift-cal-view m-2">
          <input mbsc-input id="shift-management-view" data-dropdown="true" data-input-style="box" />
          </label>
          <select id="shift-management-select">
          <option value="day">Dan</option>
          <option value="week">Teden</option>
          <option value="weekExt">Teden (razširjeno)</option>
          <option value="2week">2 tedna</option>
          <option value="weekMonth">4 tedne</option>
          <option value="month">Mesec</option>
          <option value="2month">2 meseca</option>
          <option value="6month">Pol leta</option>
          <option value="year">Leto</option>
          <option value="custom">Ročno</option>
          </select>
          </div>
          <div mbsc-calendar-prev></div>
          <div mbsc-calendar-today></div>
          <div mbsc-calendar-next></div>
        </div>`;
    },
  });
  // refresh button
  $('#refreshData').on('click', refreshData);
  // filter button
  $('#filterResources').on('click', () => {
    // appendChekboxes();
    $('#modalFilterResources').modal('toggle')
  });
  $('#filterResourcesBtn').on('click', filterResources);
  $('.resource-parent').on('change', function () {
    // debugger;
    let parent = this.id.substring(19);
    $('.filter-resource-child-' + parent + '-checkbox').prop('checked', this.checked)
  });
  $('.resource-child').on('change', function () {
    // debugger;
    if ($(this).prop('checked') == false){
      $(this).parent().parent().find(':checkbox').first().prop('checked', false);
    }
    else if($(this).parent().parent().find('.resource-child:checkbox').length == $(this).parent().parent().find('.resource-child:checked').length){
      $(this).parent().parent().find(':checkbox').first().prop('checked', true);
    }
  });
  // switch on change
  $('#changeToProjectTasks').on('change', changeDataOnCalendar);
  $('#changeToEvents').on('change', changeDataOnCalendar);
  $('#changeToMyStuff').on('change', changeDataOnCalendar);
  $('#changeToAbsences').on('change', changeDataOnCalendar);
  // switching views (day,week,2weeks,month)
  selectRange = mobiscroll.select('#shift-management-select', {
    inputElement: document.getElementById('shift-management-view'),
    touchUi: false,
    onChange: function (event, inst) {
      if (event.value == 'day') {
        // myRange.setVal([]);
        setTimeout(() => {
          selectRange.setOptions({data: [ { text: 'Dan', value: 'day' }, { text: 'Teden', value: 'week' }, { text: 'Teden (razširjeno)', value: 'weekExt' }, { text: '2 tedna', value: '2week' }, { text: '4 Tedne', value: 'weekMonth' }, { text: 'Mesec', value: 'month' }, { text: '2 meseca', value: '2month' }, { text: 'Pol leta', value: '6month' }, { text: 'Leto', value: 'year' }, { text: 'Ročno', value: 'custom', disabled: true }]});
          localStorage.setItem('calendarSelectRange', 'day');
        }, 500);
        calendar.setOptions({
          view: {
            timeline: {
              type: 'day',
              eventList: true,
              weekNumbers: true,
            }
          },
          colors: [
            { slot: 1, allDay: 'true', start: new Date('2025-01-01'), end: new Date('2025-01-01'), cssClass: 'md-dots-bg' },{ slot: 1, allDay: 'true', start: new Date('2025-01-02'), end: new Date('2025-01-02'), cssClass: 'md-dots-bg' },{ slot: 1, allDay: 'true', start: new Date('2025-02-08'), end: new Date('2025-02-08'), cssClass: 'md-dots-bg' },{ slot: 1, allDay: 'true', start: new Date('2025-04-21'), end: new Date('2025-04-21'), cssClass: 'md-dots-bg' },{ slot: 1, allDay: 'true', start: new Date('2025-04-27'), end: new Date('2025-04-27'), cssClass: 'md-dots-bg' },{ slot: 1, allDay: 'true', start: new Date('2025-05-01'), end: new Date('2025-05-02'), cssClass: 'md-dots-bg' },{ slot: 1, allDay: 'true', start: new Date('2025-05-19'), end: new Date('2025-05-19'), cssClass: 'md-dots-bg' },{ slot: 1, allDay: 'true', start: new Date('2025-06-25'), end: new Date('2025-06-25'), cssClass: 'md-dots-bg' },{ slot: 1, allDay: 'true', start: new Date('2025-08-15'), end: new Date('2025-08-15'), cssClass: 'md-dots-bg' },{ slot: 1, allDay: 'true', start: new Date('2025-10-31'), end: new Date('2025-11-1 21:00'), cssClass: 'md-dots-bg' },{ slot: 1, allDay: 'true', start: new Date('2025-12-25'), end: new Date('2025-12-26'), cssClass: 'md-dots-bg' },{ slot: 1, allDay: 'true', start: new Date('2026-01-01'), end: new Date('2026-01-02'), cssClass: 'md-dots-bg' },{ slot: 1, allDay: 'true', start: new Date('2023-07-17'), end: new Date('2023-08-16'), cssClass: 'md-stripes-bg' },{ slot: 1, allDay: 'true', start: new Date('2023-04-24'), end: new Date('2023-04-28'), cssClass: 'md-rect-bg' },{ slot: 1, allDay: 'true', start: new Date('2023-12-27'), end: new Date('2023-12-29'), cssClass: 'md-rect-bg' },{ slot: 1, allDay: 'true', start: new Date('2023-10-30'), end: new Date('2023-10-30'), cssClass: 'md-rect-bg' },
            {background: '#a5ceff4d',slot: 1,recurring: { repeat: 'weekly', weekDays: 'SU,SA' }},
          ],
        });
      }
      else if (event.value == '2week') {
        // myRange.setVal([]);
        setTimeout(() => {
          selectRange.setOptions({data: [ { text: 'Dan', value: 'day' }, { text: 'Teden', value: 'week' }, { text: 'Teden (razširjeno)', value: 'weekExt' }, { text: '2 tedna', value: '2week' }, { text: '4 Tedne', value: 'weekMonth' }, { text: 'Mesec', value: 'month' }, { text: '2 meseca', value: '2month' }, { text: 'Pol leta', value: '6month' }, { text: 'Leto', value: 'year' }, { text: 'Ročno', value: 'custom', disabled: true }]});
          localStorage.setItem('calendarSelectRange', '2week');
        }, 500);
        calendar.setOptions({
          view: {
            timeline: {
              type: 'week',
              timeCellStep: 1440,
              timeLabelStep: 1440,
              startDay: 1,
              endDay: 0,
              size: 2,
              weekNumbers: true,
            }
          },
          colors: [
            { slot: 1, allDay: 'true', start: new Date('2025-01-01'), end: new Date('2025-01-01'), cssClass: 'md-dots-bg' },{ slot: 1, allDay: 'true', start: new Date('2025-01-02'), end: new Date('2025-01-02'), cssClass: 'md-dots-bg' },{ slot: 1, allDay: 'true', start: new Date('2025-02-08'), end: new Date('2025-02-08'), cssClass: 'md-dots-bg' },{ slot: 1, allDay: 'true', start: new Date('2025-04-21'), end: new Date('2025-04-21'), cssClass: 'md-dots-bg' },{ slot: 1, allDay: 'true', start: new Date('2025-04-27'), end: new Date('2025-04-27'), cssClass: 'md-dots-bg' },{ slot: 1, allDay: 'true', start: new Date('2025-05-01'), end: new Date('2025-05-02'), cssClass: 'md-dots-bg' },{ slot: 1, allDay: 'true', start: new Date('2025-05-19'), end: new Date('2025-05-19'), cssClass: 'md-dots-bg' },{ slot: 1, allDay: 'true', start: new Date('2025-06-25'), end: new Date('2025-06-25'), cssClass: 'md-dots-bg' },{ slot: 1, allDay: 'true', start: new Date('2025-08-15'), end: new Date('2025-08-15'), cssClass: 'md-dots-bg' },{ slot: 1, allDay: 'true', start: new Date('2025-10-31'), end: new Date('2025-11-1 21:00'), cssClass: 'md-dots-bg' },{ slot: 1, allDay: 'true', start: new Date('2025-12-25'), end: new Date('2025-12-26'), cssClass: 'md-dots-bg' },{ slot: 1, allDay: 'true', start: new Date('2026-01-01'), end: new Date('2026-01-02'), cssClass: 'md-dots-bg' },{ slot: 1, allDay: 'true', start: new Date('2023-07-17'), end: new Date('2023-08-16'), cssClass: 'md-stripes-bg' },{ slot: 1, allDay: 'true', start: new Date('2023-04-24'), end: new Date('2023-04-28'), cssClass: 'md-rect-bg' },{ slot: 1, allDay: 'true', start: new Date('2023-12-27'), end: new Date('2023-12-29'), cssClass: 'md-rect-bg' },{ slot: 1, allDay: 'true', start: new Date('2023-10-30'), end: new Date('2023-10-30'), cssClass: 'md-rect-bg' },
            {background: '#a5ceff4d',slot: 1,recurring: { repeat: 'weekly', weekDays: 'SU,SA' },recurringException: [moment().format('YYYY-MM-DD')]},
            { date: moment().format('YYYY-MM-DD'), background: 'cornsilk' },
          ],
        });
      }
      else if (event.value == 'week') {
        // myRange.setVal([]);
        setTimeout(() => {
          selectRange.setOptions({data: [ { text: 'Dan', value: 'day' }, { text: 'Teden', value: 'week' }, { text: 'Teden (razširjeno)', value: 'weekExt' }, { text: '2 tedna', value: '2week' }, { text: '4 Tedne', value: 'weekMonth' }, { text: 'Mesec', value: 'month' }, { text: '2 meseca', value: '2month' }, { text: 'Pol leta', value: '6month' }, { text: 'Leto', value: 'year' }, { text: 'Ročno', value: 'custom', disabled: true }]});
          localStorage.setItem('calendarSelectRange', 'week');
        }, 500);
        calendar.setOptions({
          view: {
            timeline: {
              type: 'week',
              eventList: false,
              startDay: 1,
              endDay: 0,
              timeCellStep: 1440,
              timeLabelStep: 1440,
              weekNumbers: true,
            }
          },
          colors: [
            { slot: 1, allDay: 'true', start: new Date('2025-01-01'), end: new Date('2025-01-01'), cssClass: 'md-dots-bg' },{ slot: 1, allDay: 'true', start: new Date('2025-01-02'), end: new Date('2025-01-02'), cssClass: 'md-dots-bg' },{ slot: 1, allDay: 'true', start: new Date('2025-02-08'), end: new Date('2025-02-08'), cssClass: 'md-dots-bg' },{ slot: 1, allDay: 'true', start: new Date('2025-04-21'), end: new Date('2025-04-21'), cssClass: 'md-dots-bg' },{ slot: 1, allDay: 'true', start: new Date('2025-04-27'), end: new Date('2025-04-27'), cssClass: 'md-dots-bg' },{ slot: 1, allDay: 'true', start: new Date('2025-05-01'), end: new Date('2025-05-02'), cssClass: 'md-dots-bg' },{ slot: 1, allDay: 'true', start: new Date('2025-05-19'), end: new Date('2025-05-19'), cssClass: 'md-dots-bg' },{ slot: 1, allDay: 'true', start: new Date('2025-06-25'), end: new Date('2025-06-25'), cssClass: 'md-dots-bg' },{ slot: 1, allDay: 'true', start: new Date('2025-08-15'), end: new Date('2025-08-15'), cssClass: 'md-dots-bg' },{ slot: 1, allDay: 'true', start: new Date('2025-10-31'), end: new Date('2025-11-1 21:00'), cssClass: 'md-dots-bg' },{ slot: 1, allDay: 'true', start: new Date('2025-12-25'), end: new Date('2025-12-26'), cssClass: 'md-dots-bg' },{ slot: 1, allDay: 'true', start: new Date('2026-01-01'), end: new Date('2026-01-02'), cssClass: 'md-dots-bg' },{ slot: 1, allDay: 'true', start: new Date('2023-07-17'), end: new Date('2023-08-16'), cssClass: 'md-stripes-bg' },{ slot: 1, allDay: 'true', start: new Date('2023-04-24'), end: new Date('2023-04-28'), cssClass: 'md-rect-bg' },{ slot: 1, allDay: 'true', start: new Date('2023-12-27'), end: new Date('2023-12-29'), cssClass: 'md-rect-bg' },{ slot: 1, allDay: 'true', start: new Date('2023-10-30'), end: new Date('2023-10-30'), cssClass: 'md-rect-bg' },
            {background: '#a5ceff4d',slot: 1,recurring: { repeat: 'weekly', weekDays: 'SU,SA' },recurringException: [moment().format('YYYY-MM-DD')]},
            { date: moment().format('YYYY-MM-DD'), background: 'cornsilk' },
          ],
        });
      }
      else if (event.value == 'weekExt') {
        // myRange.setVal([]);
        setTimeout(() => {
          selectRange.setOptions({data: [ { text: 'Dan', value: 'day' }, { text: 'Teden', value: 'week' }, { text: 'Teden (razširjeno)', value: 'weekExt' }, { text: '2 tedna', value: '2week' }, { text: '4 Tedne', value: 'weekMonth' }, { text: 'Mesec', value: 'month' }, { text: '2 meseca', value: '2month' }, { text: 'Pol leta', value: '6month' }, { text: 'Leto', value: 'year' }, { text: 'Ročno', value: 'custom', disabled: true }]});
          localStorage.setItem('calendarSelectRange', 'weekExt');
        }, 500);
        calendar.setOptions({
          view: {
            timeline: {
              type: 'week',
              eventList: false,
              startDay: 1,
              endDay: 0,
              weekNumbers: true,
            }
          },
          colors: [
            { slot: 1, allDay: 'true', start: new Date('2025-01-01'), end: new Date('2025-01-01'), cssClass: 'md-dots-bg' },{ slot: 1, allDay: 'true', start: new Date('2025-01-02'), end: new Date('2025-01-02'), cssClass: 'md-dots-bg' },{ slot: 1, allDay: 'true', start: new Date('2025-02-08'), end: new Date('2025-02-08'), cssClass: 'md-dots-bg' },{ slot: 1, allDay: 'true', start: new Date('2025-04-21'), end: new Date('2025-04-21'), cssClass: 'md-dots-bg' },{ slot: 1, allDay: 'true', start: new Date('2025-04-27'), end: new Date('2025-04-27'), cssClass: 'md-dots-bg' },{ slot: 1, allDay: 'true', start: new Date('2025-05-01'), end: new Date('2025-05-02'), cssClass: 'md-dots-bg' },{ slot: 1, allDay: 'true', start: new Date('2025-05-19'), end: new Date('2025-05-19'), cssClass: 'md-dots-bg' },{ slot: 1, allDay: 'true', start: new Date('2025-06-25'), end: new Date('2025-06-25'), cssClass: 'md-dots-bg' },{ slot: 1, allDay: 'true', start: new Date('2025-08-15'), end: new Date('2025-08-15'), cssClass: 'md-dots-bg' },{ slot: 1, allDay: 'true', start: new Date('2025-10-31'), end: new Date('2025-11-1 21:00'), cssClass: 'md-dots-bg' },{ slot: 1, allDay: 'true', start: new Date('2025-12-25'), end: new Date('2025-12-26'), cssClass: 'md-dots-bg' },{ slot: 1, allDay: 'true', start: new Date('2026-01-01'), end: new Date('2026-01-02'), cssClass: 'md-dots-bg' },{ slot: 1, allDay: 'true', start: new Date('2023-07-17'), end: new Date('2023-08-16'), cssClass: 'md-stripes-bg' },{ slot: 1, allDay: 'true', start: new Date('2023-04-24'), end: new Date('2023-04-28'), cssClass: 'md-rect-bg' },{ slot: 1, allDay: 'true', start: new Date('2023-12-27'), end: new Date('2023-12-29'), cssClass: 'md-rect-bg' },{ slot: 1, allDay: 'true', start: new Date('2023-10-30'), end: new Date('2023-10-30'), cssClass: 'md-rect-bg' },
            {background: '#a5ceff4d',slot: 1,recurring: { repeat: 'weekly', weekDays: 'SU,SA' },recurringException: [moment().format('YYYY-MM-DD')]},
            { date: moment().format('YYYY-MM-DD'), background: 'cornsilk' },
          ],
        });
      }
      else if (event.value == 'weekMonth') {
        // myRange.setVal([]);
        setTimeout(() => {
          selectRange.setOptions({data: [ { text: 'Dan', value: 'day' }, { text: 'Teden', value: 'week' }, { text: 'Teden (razširjeno)', value: 'weekExt' }, { text: '2 tedna', value: '2week' }, { text: '4 Tedne', value: 'weekMonth' }, { text: 'Mesec', value: 'month' }, { text: '2 meseca', value: '2month' }, { text: 'Pol leta', value: '6month' }, { text: 'Leto', value: 'year' }, { text: 'Ročno', value: 'custom', disabled: true }]});
          localStorage.setItem('calendarSelectRange', 'weekMonth');
        }, 500);
        calendar.setOptions({
          view: {
            timeline: {
              type: 'week',
              eventList: false,
              size: 4,
              startDay: 1,
              endDay: 0,
              timeCellStep: 1440,
              timeLabelStep: 1440,
              weekNumbers: true,
            }
          },
          colors: [
            { slot: 1, allDay: 'true', start: new Date('2025-01-01'), end: new Date('2025-01-01'), cssClass: 'md-dots-bg' },{ slot: 1, allDay: 'true', start: new Date('2025-01-02'), end: new Date('2025-01-02'), cssClass: 'md-dots-bg' },{ slot: 1, allDay: 'true', start: new Date('2025-02-08'), end: new Date('2025-02-08'), cssClass: 'md-dots-bg' },{ slot: 1, allDay: 'true', start: new Date('2025-04-21'), end: new Date('2025-04-21'), cssClass: 'md-dots-bg' },{ slot: 1, allDay: 'true', start: new Date('2025-04-27'), end: new Date('2025-04-27'), cssClass: 'md-dots-bg' },{ slot: 1, allDay: 'true', start: new Date('2025-05-01'), end: new Date('2025-05-02'), cssClass: 'md-dots-bg' },{ slot: 1, allDay: 'true', start: new Date('2025-05-19'), end: new Date('2025-05-19'), cssClass: 'md-dots-bg' },{ slot: 1, allDay: 'true', start: new Date('2025-06-25'), end: new Date('2025-06-25'), cssClass: 'md-dots-bg' },{ slot: 1, allDay: 'true', start: new Date('2025-08-15'), end: new Date('2025-08-15'), cssClass: 'md-dots-bg' },{ slot: 1, allDay: 'true', start: new Date('2025-10-31'), end: new Date('2025-11-1 21:00'), cssClass: 'md-dots-bg' },{ slot: 1, allDay: 'true', start: new Date('2025-12-25'), end: new Date('2025-12-26'), cssClass: 'md-dots-bg' },{ slot: 1, allDay: 'true', start: new Date('2026-01-01'), end: new Date('2026-01-02'), cssClass: 'md-dots-bg' },{ slot: 1, allDay: 'true', start: new Date('2023-07-17'), end: new Date('2023-08-16'), cssClass: 'md-stripes-bg' },{ slot: 1, allDay: 'true', start: new Date('2023-04-24'), end: new Date('2023-04-28'), cssClass: 'md-rect-bg' },{ slot: 1, allDay: 'true', start: new Date('2023-12-27'), end: new Date('2023-12-29'), cssClass: 'md-rect-bg' },{ slot: 1, allDay: 'true', start: new Date('2023-10-30'), end: new Date('2023-10-30'), cssClass: 'md-rect-bg' },
            {background: '#a5ceff4d',slot: 1,recurring: { repeat: 'weekly', weekDays: 'SU,SA' },recurringException: [moment().format('YYYY-MM-DD')]},
            { date: moment().format('YYYY-MM-DD'), background: 'cornsilk' },
          ],
        });
      }
      else if (event.value == 'month'){
        // myRange.setVal([]);
        setTimeout(() => {
          selectRange.setOptions({data: [ { text: 'Dan', value: 'day' }, { text: 'Teden', value: 'week' }, { text: 'Teden (razširjeno)', value: 'weekExt' }, { text: '2 tedna', value: '2week' }, { text: '4 Tedne', value: 'weekMonth' }, { text: 'Mesec', value: 'month' }, { text: '2 meseca', value: '2month' }, { text: 'Pol leta', value: '6month' }, { text: 'Leto', value: 'year' }, { text: 'Ročno', value: 'custom', disabled: true }]});
          localStorage.setItem('calendarSelectRange', 'month');
        }, 500);
        calendar.setOptions({
          view: {
            timeline: {
              type: 'month',
              size: 1,
              weekNumbers: true,
            }
          },
          colors: [
            { slot: 1, allDay: 'true', start: new Date('2025-01-01'), end: new Date('2025-01-01'), cssClass: 'md-dots-bg' },{ slot: 1, allDay: 'true', start: new Date('2025-01-02'), end: new Date('2025-01-02'), cssClass: 'md-dots-bg' },{ slot: 1, allDay: 'true', start: new Date('2025-02-08'), end: new Date('2025-02-08'), cssClass: 'md-dots-bg' },{ slot: 1, allDay: 'true', start: new Date('2025-04-21'), end: new Date('2025-04-21'), cssClass: 'md-dots-bg' },{ slot: 1, allDay: 'true', start: new Date('2025-04-27'), end: new Date('2025-04-27'), cssClass: 'md-dots-bg' },{ slot: 1, allDay: 'true', start: new Date('2025-05-01'), end: new Date('2025-05-02'), cssClass: 'md-dots-bg' },{ slot: 1, allDay: 'true', start: new Date('2025-05-19'), end: new Date('2025-05-19'), cssClass: 'md-dots-bg' },{ slot: 1, allDay: 'true', start: new Date('2025-06-25'), end: new Date('2025-06-25'), cssClass: 'md-dots-bg' },{ slot: 1, allDay: 'true', start: new Date('2025-08-15'), end: new Date('2025-08-15'), cssClass: 'md-dots-bg' },{ slot: 1, allDay: 'true', start: new Date('2025-10-31'), end: new Date('2025-11-1 21:00'), cssClass: 'md-dots-bg' },{ slot: 1, allDay: 'true', start: new Date('2025-12-25'), end: new Date('2025-12-26'), cssClass: 'md-dots-bg' },{ slot: 1, allDay: 'true', start: new Date('2026-01-01'), end: new Date('2026-01-02'), cssClass: 'md-dots-bg' },{ slot: 1, allDay: 'true', start: new Date('2023-07-17'), end: new Date('2023-08-16'), cssClass: 'md-stripes-bg' },{ slot: 1, allDay: 'true', start: new Date('2023-04-24'), end: new Date('2023-04-28'), cssClass: 'md-rect-bg' },{ slot: 1, allDay: 'true', start: new Date('2023-12-27'), end: new Date('2023-12-29'), cssClass: 'md-rect-bg' },{ slot: 1, allDay: 'true', start: new Date('2023-10-30'), end: new Date('2023-10-30'), cssClass: 'md-rect-bg' },
            {background: '#a5ceff4d',slot: 1,recurring: { repeat: 'weekly', weekDays: 'SU,SA' },recurringException: [moment().format('YYYY-MM-DD')]},
            { date: moment().format('YYYY-MM-DD'), background: 'cornsilk' },
          ],
        });
      }
      else if (event.value == '2month'){
        // myRange.setVal([]);
        setTimeout(() => {
          selectRange.setOptions({data: [ { text: 'Dan', value: 'day' }, { text: 'Teden', value: 'week' }, { text: 'Teden (razširjeno)', value: 'weekExt' }, { text: '2 tedna', value: '2week' }, { text: '4 Tedne', value: 'weekMonth' }, { text: 'Mesec', value: 'month' }, { text: '2 meseca', value: '2month' }, { text: 'Pol leta', value: '6month' }, { text: 'Leto', value: 'year' }, { text: 'Ročno', value: 'custom', disabled: true }]});
          localStorage.setItem('calendarSelectRange', '2month');
        }, 500);
        calendar.setOptions({
          view: {
            timeline: {
              type: 'month',
              size: 2,
              weekNumbers: true,
            }
          },
          colors: [
            { slot: 1, allDay: 'true', start: new Date('2025-01-01'), end: new Date('2025-01-01'), cssClass: 'md-dots-bg' },{ slot: 1, allDay: 'true', start: new Date('2025-01-02'), end: new Date('2025-01-02'), cssClass: 'md-dots-bg' },{ slot: 1, allDay: 'true', start: new Date('2025-02-08'), end: new Date('2025-02-08'), cssClass: 'md-dots-bg' },{ slot: 1, allDay: 'true', start: new Date('2025-04-21'), end: new Date('2025-04-21'), cssClass: 'md-dots-bg' },{ slot: 1, allDay: 'true', start: new Date('2025-04-27'), end: new Date('2025-04-27'), cssClass: 'md-dots-bg' },{ slot: 1, allDay: 'true', start: new Date('2025-05-01'), end: new Date('2025-05-02'), cssClass: 'md-dots-bg' },{ slot: 1, allDay: 'true', start: new Date('2025-05-19'), end: new Date('2025-05-19'), cssClass: 'md-dots-bg' },{ slot: 1, allDay: 'true', start: new Date('2025-06-25'), end: new Date('2025-06-25'), cssClass: 'md-dots-bg' },{ slot: 1, allDay: 'true', start: new Date('2025-08-15'), end: new Date('2025-08-15'), cssClass: 'md-dots-bg' },{ slot: 1, allDay: 'true', start: new Date('2025-10-31'), end: new Date('2025-11-1 21:00'), cssClass: 'md-dots-bg' },{ slot: 1, allDay: 'true', start: new Date('2025-12-25'), end: new Date('2025-12-26'), cssClass: 'md-dots-bg' },{ slot: 1, allDay: 'true', start: new Date('2026-01-01'), end: new Date('2026-01-02'), cssClass: 'md-dots-bg' },{ slot: 1, allDay: 'true', start: new Date('2023-07-17'), end: new Date('2023-08-16'), cssClass: 'md-stripes-bg' },{ slot: 1, allDay: 'true', start: new Date('2023-04-24'), end: new Date('2023-04-28'), cssClass: 'md-rect-bg' },{ slot: 1, allDay: 'true', start: new Date('2023-12-27'), end: new Date('2023-12-29'), cssClass: 'md-rect-bg' },{ slot: 1, allDay: 'true', start: new Date('2023-10-30'), end: new Date('2023-10-30'), cssClass: 'md-rect-bg' },
            {background: '#a5ceff4d',slot: 1,recurring: { repeat: 'weekly', weekDays: 'SU,SA' },recurringException: [moment().format('YYYY-MM-DD')]},
            { date: moment().format('YYYY-MM-DD'), background: 'cornsilk' },
          ],
        });
      }
      else if (event.value == '6month'){
        // myRange.setVal([]);
        setTimeout(() => {
          selectRange.setOptions({data: [ { text: 'Dan', value: 'day' }, { text: 'Teden', value: 'week' }, { text: 'Teden (razširjeno)', value: 'weekExt' }, { text: '2 tedna', value: '2week' }, { text: '4 Tedne', value: 'weekMonth' }, { text: 'Mesec', value: 'month' }, { text: '2 meseca', value: '2month' }, { text: 'Pol leta', value: '6month' }, { text: 'Leto', value: 'year' }, { text: 'Ročno', value: 'custom', disabled: true }]});
          localStorage.setItem('calendarSelectRange', '6month');
        }, 500);
        calendar.setOptions({
          view: {
            timeline: {
              type: 'month',
              size: 6,
              weekNumbers: true,
            }
          },
          colors: [
            { slot: 1, allDay: 'true', start: new Date('2025-01-01'), end: new Date('2025-01-01'), cssClass: 'md-dots-bg' },{ slot: 1, allDay: 'true', start: new Date('2025-01-02'), end: new Date('2025-01-02'), cssClass: 'md-dots-bg' },{ slot: 1, allDay: 'true', start: new Date('2025-02-08'), end: new Date('2025-02-08'), cssClass: 'md-dots-bg' },{ slot: 1, allDay: 'true', start: new Date('2025-04-21'), end: new Date('2025-04-21'), cssClass: 'md-dots-bg' },{ slot: 1, allDay: 'true', start: new Date('2025-04-27'), end: new Date('2025-04-27'), cssClass: 'md-dots-bg' },{ slot: 1, allDay: 'true', start: new Date('2025-05-01'), end: new Date('2025-05-02'), cssClass: 'md-dots-bg' },{ slot: 1, allDay: 'true', start: new Date('2025-05-19'), end: new Date('2025-05-19'), cssClass: 'md-dots-bg' },{ slot: 1, allDay: 'true', start: new Date('2025-06-25'), end: new Date('2025-06-25'), cssClass: 'md-dots-bg' },{ slot: 1, allDay: 'true', start: new Date('2025-08-15'), end: new Date('2025-08-15'), cssClass: 'md-dots-bg' },{ slot: 1, allDay: 'true', start: new Date('2025-10-31'), end: new Date('2025-11-1 21:00'), cssClass: 'md-dots-bg' },{ slot: 1, allDay: 'true', start: new Date('2025-12-25'), end: new Date('2025-12-26'), cssClass: 'md-dots-bg' },{ slot: 1, allDay: 'true', start: new Date('2026-01-01'), end: new Date('2026-01-02'), cssClass: 'md-dots-bg' },{ slot: 1, allDay: 'true', start: new Date('2023-07-17'), end: new Date('2023-08-16'), cssClass: 'md-stripes-bg' },{ slot: 1, allDay: 'true', start: new Date('2023-04-24'), end: new Date('2023-04-28'), cssClass: 'md-rect-bg' },{ slot: 1, allDay: 'true', start: new Date('2023-12-27'), end: new Date('2023-12-29'), cssClass: 'md-rect-bg' },{ slot: 1, allDay: 'true', start: new Date('2023-10-30'), end: new Date('2023-10-30'), cssClass: 'md-rect-bg' },
            {background: '#a5ceff4d',slot: 1,recurring: { repeat: 'weekly', weekDays: 'SU,SA' },recurringException: [moment().format('YYYY-MM-DD')]},
            { date: moment().format('YYYY-MM-DD'), background: 'cornsilk' },
          ],
        });
      }
      else if (event.value == 'custom'){
        var date = myRange.getVal();
        localStorage.setItem('calendarSelectRange', 'custom');
        localStorage.setItem('calendarRangeStart', date[0]);
        localStorage.setItem('calendarRangeEnd', date[1]);
        calendar.setOptions({
          colors: [
            { slot: 1, allDay: 'true', start: new Date('2025-01-01'), end: new Date('2025-01-01'), cssClass: 'md-dots-bg' },{ slot: 1, allDay: 'true', start: new Date('2025-01-02'), end: new Date('2025-01-02'), cssClass: 'md-dots-bg' },{ slot: 1, allDay: 'true', start: new Date('2025-02-08'), end: new Date('2025-02-08'), cssClass: 'md-dots-bg' },{ slot: 1, allDay: 'true', start: new Date('2025-04-21'), end: new Date('2025-04-21'), cssClass: 'md-dots-bg' },{ slot: 1, allDay: 'true', start: new Date('2025-04-27'), end: new Date('2025-04-27'), cssClass: 'md-dots-bg' },{ slot: 1, allDay: 'true', start: new Date('2025-05-01'), end: new Date('2025-05-02'), cssClass: 'md-dots-bg' },{ slot: 1, allDay: 'true', start: new Date('2025-05-19'), end: new Date('2025-05-19'), cssClass: 'md-dots-bg' },{ slot: 1, allDay: 'true', start: new Date('2025-06-25'), end: new Date('2025-06-25'), cssClass: 'md-dots-bg' },{ slot: 1, allDay: 'true', start: new Date('2025-08-15'), end: new Date('2025-08-15'), cssClass: 'md-dots-bg' },{ slot: 1, allDay: 'true', start: new Date('2025-10-31'), end: new Date('2025-11-1 21:00'), cssClass: 'md-dots-bg' },{ slot: 1, allDay: 'true', start: new Date('2025-12-25'), end: new Date('2025-12-26'), cssClass: 'md-dots-bg' },{ slot: 1, allDay: 'true', start: new Date('2026-01-01'), end: new Date('2026-01-02'), cssClass: 'md-dots-bg' },{ slot: 1, allDay: 'true', start: new Date('2023-07-17'), end: new Date('2023-08-16'), cssClass: 'md-stripes-bg' },{ slot: 1, allDay: 'true', start: new Date('2023-04-24'), end: new Date('2023-04-28'), cssClass: 'md-rect-bg' },{ slot: 1, allDay: 'true', start: new Date('2023-12-27'), end: new Date('2023-12-29'), cssClass: 'md-rect-bg' },{ slot: 1, allDay: 'true', start: new Date('2023-10-30'), end: new Date('2023-10-30'), cssClass: 'md-rect-bg' },
            {background: '#a5ceff4d',slot: 1,recurring: { repeat: 'weekly', weekDays: 'SU,SA' },recurringException: [moment().format('YYYY-MM-DD')]},
            { date: moment().format('YYYY-MM-DD'), background: 'cornsilk' },
          ],
        });
      }
      else {
        // myRange.setVal([]);
        setTimeout(() => {
          selectRange.setOptions({data: [ { text: 'Dan', value: 'day' }, { text: 'Teden', value: 'week' }, { text: 'Teden (razširjeno)', value: 'weekExt' }, { text: '2 tedna', value: '2week' }, { text: '4 Tedne', value: 'weekMonth' }, { text: 'Mesec', value: 'month' }, { text: '2 meseca', value: '2month' }, { text: 'Pol leta', value: '6month' }, { text: 'Leto', value: 'year' }, { text: 'Ročno', value: 'custom', disabled: true }]});
          localStorage.setItem('calendarSelectRange', 'year');
        }, 500);
        calendar.setOptions({
          view: {
            timeline: {
              type: 'month',
              size: 12,
              weekNumbers: true,
            }
          },
          colors: [
            { slot: 1, allDay: 'true', start: new Date('2025-01-01'), end: new Date('2025-01-01'), cssClass: 'md-dots-bg' },{ slot: 1, allDay: 'true', start: new Date('2025-01-02'), end: new Date('2025-01-02'), cssClass: 'md-dots-bg' },{ slot: 1, allDay: 'true', start: new Date('2025-02-08'), end: new Date('2025-02-08'), cssClass: 'md-dots-bg' },{ slot: 1, allDay: 'true', start: new Date('2025-04-21'), end: new Date('2025-04-21'), cssClass: 'md-dots-bg' },{ slot: 1, allDay: 'true', start: new Date('2025-04-27'), end: new Date('2025-04-27'), cssClass: 'md-dots-bg' },{ slot: 1, allDay: 'true', start: new Date('2025-05-01'), end: new Date('2025-05-02'), cssClass: 'md-dots-bg' },{ slot: 1, allDay: 'true', start: new Date('2025-05-19'), end: new Date('2025-05-19'), cssClass: 'md-dots-bg' },{ slot: 1, allDay: 'true', start: new Date('2025-06-25'), end: new Date('2025-06-25'), cssClass: 'md-dots-bg' },{ slot: 1, allDay: 'true', start: new Date('2025-08-15'), end: new Date('2025-08-15'), cssClass: 'md-dots-bg' },{ slot: 1, allDay: 'true', start: new Date('2025-10-31'), end: new Date('2025-11-1 21:00'), cssClass: 'md-dots-bg' },{ slot: 1, allDay: 'true', start: new Date('2025-12-25'), end: new Date('2025-12-26'), cssClass: 'md-dots-bg'},{ slot: 1, allDay: 'true', start: new Date('2026-01-01'), end: new Date('2026-01-02'), cssClass: 'md-dots-bg' },{ slot: 1, allDay: 'true', start: new Date('2023-07-17'), end: new Date('2023-08-16'), cssClass: 'md-stripes-bg' },{ slot: 1, allDay: 'true', start: new Date('2023-04-24'), end: new Date('2023-04-28'), cssClass: 'md-rect-bg' },{ slot: 1, allDay: 'true', start: new Date('2023-12-27'), end: new Date('2023-12-29'), cssClass: 'md-rect-bg' },{ slot: 1, allDay: 'true', start: new Date('2023-10-30'), end: new Date('2023-10-30'), cssClass: 'md-rect-bg' },
            {background: '#a5ceff4d',slot: 1,recurring: { repeat: 'weekly', weekDays: 'SU,SA' },recurringException: [moment().format('YYYY-MM-DD')]},
            { date: moment().format('YYYY-MM-DD'), background: 'cornsilk' },
          ],
        });
      }
    }
  });
}

function createAddPopup(elm) {
  // empty temp edit event
  tempEditEvent = {};
  // hide delete button inside add popup
  deleteButton.style.display = 'none';
  unvalidButton.style.display = 'none';
  deleteTaskButton.style.display = 'none';
  deleteAbsenceButton.style.display = 'none';

  deleteEvent = true;
  restoreEvent = false;

  // set popup header text and buttons for adding
  popup.setOptions({
      // headerText: 'Nov dogodek',
      buttons: [
        // 'cancel',
        {
          text: sloLocal.cancelText,
          handler: function () {
            popup.close();
            if (isMobileDevice()){
              $('.navbar').show();
              $('.notification-bar').show();
            }
          }
        },
        {
          text: sloLocal.setText,
          keyCode: 'enter',
          handler: function () {
            // debugger;
            if ($("#eventPopup").hasClass('active')){
              if ($('#workOrderStart').val() == '' || $('#workOrderEnd').val() == '' || $('#eventWorkers').val().length == 0){
                $('#workOrderStart').val() == '' ? $('#workOrderStart').addClass('is-invalid') : $('#workOrderStart').removeClass('is-invalid');
                $('#workOrderEnd').val() == '' ? $('#workOrderEnd').addClass('is-invalid') : $('#workOrderEnd').removeClass('is-invalid');
                $('#eventWorkers').val().length == 0 ? $('#eventWorkers').addClass('is-invalid') : $('#eventWorkers').removeClass('is-invalid');
              }
              else{
                $('#workOrderStart').removeClass('is-invalid');
                $('#workOrderEnd').removeClass('is-invalid');
                $('#eventWorkers').removeClass('is-invalid');
                /////////////////////////////////////////////////////////////////// USER IS ADDING EVENT
                // was used for adding users to event but the list was too long and it was not user friendly -> changed to select2
                // var resources = [];
                // this if is when adding event on category wants to be added as well, if not desired in this way remove/comment it
                // if ( !/\d/.test(tempEvent.resource) ) 
                //   resources.push(tempEvent.resource);
                // $('#work-order-popup').find('.mbsc-row input:checked').each(function() {
                //   resources.push($(this).val());
                // });
                // tempEvent.resource = resources;
                let workers = $('#eventWorkers').val();
                workers.forEach((worker, index) => { workers[index] = 'user' + worker; });
                tempEvent.resource = workers;
                tempEvent.editable = true;
                tempEvent.color = tempEvent.color ? tempEvent.color : rgb2hex($('#event-color').css('background-color'));
  
                tempEvent.start = $('#workOrderStart').val().length == 10 ? $('#workOrderStart').val()+'T07:00' : $('#workOrderStart').val();
                tempEvent.end = $('#workOrderEnd').val().length == 10 ? $('#workOrderEnd').val()+'T15:00' : $('#workOrderEnd').val();
                
  
                // create event in database and if success then add it to calendar otherwise close popup and open error modal with correct error message
                let data = {start: tempEvent.start, end: tempEvent.end, title: tempEvent.title, resource: tempEvent.resource, location: tempEvent.location, notes: tempEvent.notes, allDay : tempEvent.allDay, color: tempEvent.color};
                $.ajax({
                  type: 'POST',
                  url: '/apiv2/events',
                  contentType: 'application/json',
                  data: JSON.stringify(data), // access in body
                }).done(function (resp) {
                  //console.log('SUCCESS');
                  tempEvent.real_id = resp.event.id;
                  tempEvent.id = 'event_' + resp.event.id;
                  tempEvent.owner = resp.event.owner;
                  tempEvent.owner_name = loggedUser.name + ' ' + loggedUser.surname;
                  tempEvent.created = new Date();
                  // tempEvent.all_day = tempEvent.allDay == true ? true : false;
                  // tempEvent.allDay = false;
                  // tempEvent.start = tempEvent.all_day ? moment(tempEvent.start).format("YYYY-MM-DDT00:00") : moment(tempEvent.start).format("YYYY-MM-DDTHH:mm");
                  // tempEvent.end = tempEvent.all_day ? moment(tempEvent.end).format("YYYY-MM-DDT23:00") : moment(tempEvent.end).format("YYYY-MM-DDTHH:mm");
                  calendar.updateEvent(tempEvent);
                  deleteEvent = false;
  
                  // navigate the calendar to the correct view
                  calendar.navigate(tempEvent.start);
  
                  popup.close();
                  if (isMobileDevice()){
                    $('.navbar').show();
                    $('.notification-bar').show();
                  }
                }).fail(function (resp) {
                  //console.log('FAIL');
                  //debugger;
                  popup.close();
                  if (isMobileDevice()){
                    $('.navbar').show();
                    $('.notification-bar').show();
                  }
                  $('#errorModalMsg').text('Napaka pri kreiranju v podatkovni bazi. Osvežite stran in ob ponovni napaki obvestite admninistratorja RIS-a.')
                  $('#modalError').modal('toggle');
                })
              }
            }
            else if ($("#taskPopup").hasClass('active')){
              ///////////////////////////////////////////////////////////////////////////////// USER IS ADDING TASK
              // check if required inputs are not empty
              if( $('#taskProject').val() == null || $('#taskName').val() == '' || $('#taskStart').val() == '' || $('#taskFinish').val() == '' || $('#taskWorkers').val().length == 0){
                // at least one required input is empty
                $('#taskProject').val() == null ? $('#taskProject').addClass('is-invalid') : $('#taskProject').removeClass('is-invalid');
                $('#taskName').val() == '' ? $('#taskName').addClass('is-invalid') : $('#taskName').removeClass('is-invalid');
                $('#taskStart').val() == '' ? $('#taskStart').addClass('is-invalid') : $('#taskStart').removeClass('is-invalid');
                $('#taskFinish').val() == '' ? $('#taskFinish').addClass('is-invalid') : $('#taskFinish').removeClass('is-invalid');
                $('#taskWorkers').val().length == 0 ? $('#taskWorkers').addClass('is-invalid') : $('#taskWorkers').removeClass('is-invalid');
              }
              else{
                $('#taskProject').removeClass('is-invalid');
                $('#taskName').removeClass('is-invalid');
                $('#taskStart').removeClass('is-invalid');
                $('#taskFinish').removeClass('is-invalid');
                $('#taskWorkers').removeClass('is-invalid');
                // required inputs are ok, add new event (try, might get back with conflicts)
                // first check if there are conflict then add if there are non otherwise user must override conflicts
                let projectId = $('#taskProject').val();
                let finish = $('#taskFinish').val();
                let start = $('#taskStart').val();
                let workers = $('#taskWorkers').val();
                let name = $('#taskName').val();
                let category = $('#taskCategory').val();
                let notes = $("#taskNote").val();
                let duration = start && finish ? calcDurationDays(start, finish) : 0;

                let data = {start, finish, workers: workers.toString(), projectId};
                $.ajax({
                  type: 'GET',
                  url: '/apiv2/tasks/conflicts',
                  contentType: 'application/json',
                  data: data, // access in body
                }).done(function (resp) {
                  //console.log('SUCCESS');
                  // if conflicts then open modal with conflicts otherwise add new task
                  if (resp.conflicts.length > 0){
                    // open modal with conflicts
                    openConflictModal(resp.conflicts);
                  }
                  else{
                    // add new task
                    // console.log('ni konfliktov, klici funkcijo za dodajanje opravila')
                    let data = {start, finish, duration, name, workers: workers.toString(), projectId, notes, category};
                    addNewTask(data);
                  }
                }).fail(function (resp) {
                  //console.log('FAIL');
                  //debugger;
                  popup.close();
                  if (isMobileDevice()){
                    $('.navbar').show();
                    $('.notification-bar').show();
                  }
                  $('#errorModalMsg').text('Napaka pri pridobivanju podaktov za konflikte. Osvežite stran in ob ponovni napaki obvestite admninistratorja RIS-a.')
                  $('#modalError').modal('toggle');
                })
              }
            }
          },
          cssClass: 'mbsc-popup-button-primary'
        }
      ]
  });
  // fix header text
  // $('#eventsPopupHeader').text('Nov dogodek');
  // $('#tasksPopupHeader').text('Novo opravilo');

  // TASK DATA FILL
  // add correct user to taskWorkers
  debugger;
  let eventWorkers = [];
  if (tempEvent.resource.substring(0,4) == 'user'){
    $('#taskWorkers').val(tempEvent.resource.substring(4)).trigger('change');
    $('#eventWorkers').val(tempEvent.resource.substring(4)).trigger('change');
    $('#absenceWorker').val(tempEvent.resource.substring(4)).trigger('change');
    eventWorkers = tempEvent.resource;
    // try to predict which category fits the role of selected user
    let user = users.find(u => u.id == tempEvent.resource.substring(4));
    if (user){
      let role = user.role.toLowerCase();
      let tmpCategory = categories.find(c => c.name.toLowerCase().substring(0,6) == role.substring(0,6))
      let category = tmpCategory ? tmpCategory.id : 1;
      $('#taskCategory').val(category).trigger('change');
    }
  }
  else{
    let workers = [];
    if (tempEvent.resource == 'employees'){
      for (const user of users) {
        workers.push(user.id);
        eventWorkers.push('user'+user.id);
      }
      $('#taskCategory').val(1).trigger('change');
    }
    else{
      let tmpUsers = users.filter(u => u.role.toLowerCase().includes(tempEvent.resource.toLowerCase()));
      for (const user of tmpUsers) {
        workers.push(user.id);
        eventWorkers.push('user'+user.id);
      }
      // try to predict which category fits the parent role
      let tmpCategory = categories.find(c => c.name.toLowerCase().substring(0,6) == tempEvent.resource.substring(0,6));
      let category = tmpCategory ? tmpCategory.id : 1;
      // special case for category 7 Programiranje because parent role name is not the same as category name, its either plc or robot
      if(tempEvent.resource == 'plc' || tempEvent.resource == 'robot') category = 7;
      $('#taskCategory').val(category).trigger('change');
    }
    $('#taskWorkers').val(workers).trigger('change');
    $('#eventWorkers').val(workers).trigger('change');
  }
  if(localStorage.calendarSelectRange == 'custom') tempEvent.end = new Date(tempEvent.end).addHours(-3).toISOString(); // hacky solution
  $('#taskStart').val(moment(tempEvent.start).format("YYYY-MM-DD"));
  $('#taskFinish').val(moment(tempEvent.end).format("YYYY-MM-DD"));
  $('#taskProject').prop('disabled', false);
  $('#taskProject').val(0).trigger('change');
  // $('#taskCategory').val(1).trigger('change');
  $('#taskName').val('');
  $('#taskNote').val('');
  $('#taskProject').removeClass('is-invalid');
  $('#taskName').removeClass('is-invalid');
  $('#taskStart').removeClass('is-invalid');
  $('#taskFinish').removeClass('is-invalid');
  $('#taskWorkers').removeClass('is-invalid');
  $('#eventWorkers').removeClass('is-invalid');

  // ABSENCE DATA FILL
  $('#absenceStart').val(moment(tempEvent.start).format("YYYY-MM-DD"));
  $('#absenceFinish').val(moment(tempEvent.end).format("YYYY-MM-DD"));
  $('#absenceWorker').prop('disabled', true);

  // EVENT DATA FILL
  // fill popup with a new event data
  mobiscroll.getInst(titleInput).value = tempEvent.title;
  mobiscroll.getInst(locationInput).value = '';
  mobiscroll.getInst(notesTextarea).value = '';
  mobiscroll.getInst(allDaySwitch).checked = tempEvent.allDay;

  //test custom datetime
  tempEvent.allDay ? $('#workOrderStart').prop('type', 'date') : $('#workOrderStart').prop('type', 'datetime-local');
  tempEvent.allDay ? $('#workOrderEnd').prop('type', 'date') : $('#workOrderEnd').prop('type', 'datetime-local');
  tempEvent.allDay ? $('#workOrderStart').val(moment(tempEvent.start).format("YYYY-MM-DD")) : $('#workOrderStart').val(moment(tempEvent.start).format("YYYY-MM-DDTHH:mm"));
  tempEvent.allDay ? $('#workOrderEnd').val(moment(tempEvent.end).format("YYYY-MM-DD")) : $('#workOrderEnd').val(moment(tempEvent.end).format("YYYY-MM-DDTHH:mm"));
  // range.setVal([tempEvent.start, tempEvent.end]);
  // range.setOptions({
  //     controls: tempEvent.allDay ? ['date'] : ['datetime'],
  //     responsive: tempEvent.allDay ? datePickerResponsive : datetimePickerResponsive
  // });
  // range.setVal([tempEvent.start, tempEvent.end]);
  // setCheckboxes(tempEvent.resource);
  // setCheckboxes(eventWorkers);
  // set resource color
  selectColor(getResource(tempEvent.resource).color || '#1a73e8', true);

  // set anchor for the popup
  popup.setOptions({ anchor: elm });

  popup.open();
  if (isMobileDevice()){
    $('.navbar').hide();
    $('.notification-bar').hide();
  }
  refreshTimerCount = 0;
  setTimeout(() => {
    $('#taskPopup').hasClass('active') ? $('#taskTab').click() : $('#eventTab').click();
  }, 200);
}

function createEditPopup(args) {
  var ev = args.event;
  tempEditEvent = ev;
  debugger;
  // show delete button inside edit popup
  (tempEditEvent.id.substring(0,5) == 'event') ? deleteButton.style.display = 'block' : deleteButton.style.display = 'none';
  (tempEditEvent.id.substring(0,5) == 'event') ? unvalidButton.style.display = 'block' : unvalidButton.style.display = 'none';
  (ev.id.substring(0,4) == 'task') ? deleteTaskButton.style.display = 'block' : deleteTaskButton.style.display = 'none';
  (ev.id.substring(0,4) == 'absence') ? deleteAbsenceButton.style.display = 'block' : deleteAbsenceButton.style.display = 'none';

  deleteEvent = false;
  restoreEvent = true;

  // set popup header text and buttons for editing
  popup.setOptions({
      // headerText: 'Uredi dogodek',
      buttons: [
        // 'cancel',
        {
          text: sloLocal.cancelText,
          handler: function () {
            popup.close();
            if (isMobileDevice()){
              $('.navbar').show();
              $('.notification-bar').show();
            }
          }
        },
        {
          text: sloLocal.setText,
          keyCode: 'enter',
          handler: function () {
            // check if it is event or task and get correct id
            if ($('#taskPopup').hasClass('active')){
              // TODO add input control
              if( $('#taskProject').val() == null || $('#taskName').val() == '' || $('#taskStart').val() == '' || $('#taskFinish').val() == '' || $('#taskWorkers').val().length == 0){
                // at least one required input is empty
                $('#taskProject').val() == null ? $('#taskProject').addClass('is-invalid') : $('#taskProject').removeClass('is-invalid');
                $('#taskName').val() == '' ? $('#taskName').addClass('is-invalid') : $('#taskName').removeClass('is-invalid');
                $('#taskStart').val() == '' ? $('#taskStart').addClass('is-invalid') : $('#taskStart').removeClass('is-invalid');
                $('#taskFinish').val() == '' ? $('#taskFinish').addClass('is-invalid') : $('#taskFinish').removeClass('is-invalid');
                $('#taskWorkers').val().length == 0 ? $('#taskWorkers').addClass('is-invalid') : $('#taskWorkers').removeClass('is-invalid');
                $('#eventWorkers').val().length == 0 ? $('#eventWorkers').addClass('is-invalid') : $('#eventWorkers').removeClass('is-invalid');
              }
              else{
                // either user is editing task or wants to transform event to task
                if ( ev.id.substring(0,4) == 'task'){
                  // console.log('posodabljam opravilo');
                  // HERE TODO
                  // check if there are conflicts
                  let projectId = $('#taskProject').val();
                  let finish = $('#taskFinish').val();
                  let start = $('#taskStart').val();
                  let workers = $('#taskWorkers').val();
                  let name = $('#taskName').val();
                  let category = $('#taskCategory').val();
                  let notes = $("#taskNote").val();
                  let duration = start && finish ? calcDurationDays(start, finish) : 0;
                  let data = {start, finish, workers: workers.toString(), projectId};
                  $.ajax({
                    type: 'GET',
                    url: '/apiv2/tasks/conflicts',
                    contentType: 'application/json',
                    data: data, // access in body
                  }).done(function (resp) {
                    //console.log('SUCCESS');
                    // if conflicts then open modal with conflicts otherwise add new task
                    if (resp.conflicts.length > 0){
                      // open modal with conflicts
                      openConflictModal(resp.conflicts, true);
                    }
                    else{
                      // no conflicts -> edit task
                      // console.log('ni konfliktov, klici funkcijo za urejanje opravila')
                      let data = {start, finish, duration, name, workers: workers.toString(), projectId, notes, category, weekend: tempEditEvent.weekend, priority: tempEditEvent.priority_id, active: tempEditEvent.active};
                      editTask(data);
                    }
                  }).fail(function (resp) {
                    //console.log('FAIL');
                    //debugger;
                    popup.close();
                    if (isMobileDevice()){
                      $('.navbar').show();
                      $('.notification-bar').show();
                    }
                    $('#errorModalMsg').text('Napaka pri pridobivanju podaktov za konflikte. Osvežite stran in ob ponovni napaki obvestite admninistratorja RIS-a.')
                    $('#modalError').modal('toggle');
                  })
                }
                else{
                  // console.log('pretvarjam dogodek v opravilo');
                  // TODO check if there are conflicts and then delete eventd and add new task HERE
                  transformEventToTask(ev);
                }
              }
            }
            else if ($('#eventPopup').hasClass('active')){
              // either user is editing event or wants to transform task to event
              if ( ev.id.substring(0,5) == 'event'){
                // console.log('posodabljam dogodek');

                // var resources = [];
                // $('#work-order-popup').find('.mbsc-row input:checked').each(function() {
                //   resources.push($(this).val());
                // });
                let workers = $('#eventWorkers').val();
                workers.forEach((worker, index) => { workers[index] = 'user' + worker; });
                ev.start = $('#workOrderStart').val().length == 10 ? $('#workOrderStart').val()+'T07:00' : $('#workOrderStart').val();
                ev.end = $('#workOrderEnd').val().length == 10 ? $('#workOrderEnd').val()+'T15:00' : $('#workOrderEnd').val();
                // tempEditEvent = ev;
                // update event with the new properties on save button click and send it to database first
                let data = {eventId: ev.real_id, start: new Date(ev.start), end: new Date(ev.end), title: titleInput.value, resource: workers, location: locationInput.value, notes: notesTextarea.value, allDay : mobiscroll.getInst(allDaySwitch).checked, color: ev.color, unvalid: ev.unvalid};
                editEvent(data);
              }
              else{
                // console.log('pretvarjam opravilo v dogodek');
                // TODO add new event and delete task -> call transformTaskToEvent
                transformTaskToEvent(ev);
              }
            }
          },
          cssClass: 'mbsc-popup-button-primary'
        }
      ]
  });
  //check if task or event
  if (ev.id.substring(0,4) == 'task'){
    // console.log('urejam opravilo');
    // $('#taskTab').click();

    // fix header text
    // $('#eventsPopupHeader').text('Pretvori v dogodek');
    // $('#tasksPopupHeader').text('Uredi opravilo');

    $('#taskProject').prop('disabled', true);

    $('#taskProject').val(ev.project_id).trigger('change');
    $('#taskName').val(ev.task_name);
    $('#taskStart').val(moment(ev.task_start).format("YYYY-MM-DD"));
    $('#taskFinish').val(moment(ev.task_finish).format("YYYY-MM-DD"));
    $('#taskCategory').val(ev.category_id).trigger('change');
    $('#taskWorkers').val(ev.workers_id.split(',')).trigger('change');
    $('#taskNote').val(ev.task_note);
    // debugger;
  }
  else if (ev.id.substring(0,5) == 'event'){
    // console.log('urejam dogodek');
    $('#taskProject').prop('disabled', false);
    // $('#eventTab').click();
    // fix header text
    // $('#eventsPopupHeader').text('Uredi dogodek');
    // $('#tasksPopupHeader').text('Pretvori v opravilo');
  
    // fill popup with the selected event data
    mobiscroll.getInst(titleInput).value = ev.title || '';
    mobiscroll.getInst(locationInput).value = ev.location || '';
    mobiscroll.getInst(notesTextarea).value = ev.notes || '';
    mobiscroll.getInst(allDaySwitch).checked = ev.allDay || false;
    selectColor(ev.color || getResource(ev.resource).color, true);
    //unvalid button
    unvalidButton.innerHTML = ev.unvalid ? 'Označi kot veljavno' : 'Označi kot neveljavno';
    //test custom datetime
    ev.allDay ? $('#workOrderStart').prop('type', 'date') : $('#workOrderStart').prop('type', 'datetime-local');
    ev.allDay ? $('#workOrderEnd').prop('type', 'date') : $('#workOrderEnd').prop('type', 'datetime-local');
    ev.allDay ? $('#workOrderStart').val(moment(ev.start).format("YYYY-MM-DD")) : $('#workOrderStart').val(moment(ev.start).format("YYYY-MM-DDTHH:mm"));
    ev.allDay ? $('#workOrderEnd').val(moment(ev.end).format("YYYY-MM-DD")) : $('#workOrderEnd').val(moment(ev.end).format("YYYY-MM-DDTHH:mm"));
    let workers = [];
    for (const user of ev.resource) {
      workers.push(user.substring(4));
    }
    $('#eventWorkers').val(workers).trigger('change');
    // setCheckboxes(ev.resource);
  }
  else if (ev.id.substring(0,7) == 'absence'){
    $('#absenceStart').val(moment(ev.start).format("YYYY-MM-DD"));
    $('#absenceFinish').val(moment(ev.finish).format("YYYY-MM-DD"));
    $('#absenceWorker').val(ev.id_user).trigger('change');
    // debugger;
  }
  // set anchor for the popup
  popup.setOptions({ anchor: args.domEvent.currentTarget });
  // open only if event or task for now, absence will be added later based on the feedback
  if (ev.id.substring(0,4) == 'task' || ev.id.substring(0,5) == 'event'){
    popup.open();
  }
  if (isMobileDevice()){
    $('.navbar').hide();
    $('.notification-bar').hide();
  }
  refreshTimerCount = 0;
  setTimeout(() => {
    if (ev.id.substring(0,4) == 'task'){
      $('#eventsPopupHeader').text('dogodek');
      $('#tasksPopupHeader').text('opravilo');
      $('#taskTab').removeClass('active');
      $('#taskTab').click();
      $('#taskStart').val(moment(ev.task_start).format("YYYY-MM-DD"));
      $('#taskFinish').val(moment(ev.task_finish).format("YYYY-MM-DD"));
    }
    else if (ev.id.substring(0,5) == 'event'){
      $('#eventsPopupHeader').text('dogodek');
      $('#tasksPopupHeader').text('opravilo');
      $('#eventTab').removeClass('active');
      $('#eventTab').click();
    }
    else if (ev.id.substring(0,7) == 'absence'){
      $('#absenceTab').click();
    }
  }, 200);
}

function setCheckboxes(resources) {
  var checkboxes = document.getElementsByClassName('work-order-checkbox');

  for (var i = 0; i < checkboxes.length; i++) {
      var checkbox = checkboxes[i];
      // mobiscroll.getInst(checkbox).checked = resources.indexOf(checkbox.getAttribute('data-value')) !== -1;
      if (Array.isArray(resources)){
        mobiscroll.getInst(checkbox).checked = (resources.find(r => r == checkbox.dataset.value) != undefined);
      }
      else{
        mobiscroll.getInst(checkbox).checked = (checkbox.dataset.value == resources);
      }
  }
}

function appendChekboxes() {
  // var checkboxes = '<div class="mbsc-grid mbsc-no-padding"><div class="mbsc-row">';
  let checkboxesFilterResources = '<div class="mbsc-grid mbsc-no-padding"><div class="mbsc-row mt-2">';

  for (var i = 0; i < dbRobotehResources.length; ++i) {
      for (var j = 0; j < dbRobotehResources[i].children.length; ++j) {
          var resource = dbRobotehResources[i].children[j];
          // checkboxes += '<div class="mbsc-col-sm-4"><div class="mbsc-form-group-title">' + resource.name + '</div>';
          checkboxesFilterResources += `<div class="mbsc-col-sm-4 mb-3">
            <div class="mbsc-form-group-title text-info text-uppercase bg-light h5">
              <div class="custom-control custom-checkbox table-custom-checkbox btn-on-stretched" id="resourceCheckbox`+resource.id+`">
                <input class="custom-control-input resource-parent filter-resource-` + resource.id + `-checkbox" id="customResourceCheck`+resource.id+`" type="checkbox" checked='' />
                <label class="custom-control-label" for="customResourceCheck`+resource.id+`">` + resource.name + `</label>
              </div>
            </div>`;

          for (var k = 0; k < resource.children.length; ++k) {
              var r = resource.children[k];
              // checkboxes += '<label class="work-order-checkbox-label"><input class="work-order-checkbox" data-value="' + r.id + '" type="checkbox" mbsc-checkbox data-theme="material" data-label="' + r.name + '"/></label>';
              checkboxesFilterResources += `<div class="custom-control custom-checkbox table-custom-checkbox btn-on-stretched ml-4" id="resourceChildCheckbox`+r.id+`" data-value="` + r.id + `">
                <input class="custom-control-input resource-child filter-resource-child-` + resource.id + `-checkbox" id="customResourceCheck`+r.id+`" data-value="` + r.id + `" type="checkbox" checked='' />
                <label class="custom-control-label" for="customResourceCheck`+r.id+`">` + r.name + `</label>
              </div>`;
          }
          // checkboxes += '</div>';
          checkboxesFilterResources += '</div>';
      }
  }
  // checkboxes += '</div></div>';
  checkboxesFilterResources += '</div></div>';

  // resourceCont.innerHTML = checkboxes;
  filterResourcesBody.innerHTML = checkboxesFilterResources;
  // mobiscroll.enhance(resourceCont);
  mobiscroll.enhance(filterResourcesBody);
}
function appendCategoryChekboxes() {
  let checkboxesFilterCategories = '<div class="mbsc-grid mbsc-no-padding"><div class="mbsc-row mt-2">';
  for (var i = 0; i < categories.length; ++i) {
    var category = categories[i];
    checkboxesFilterCategories += `<div class="mbsc-col-sm-4 mb-3">
      <div class="mbsc-form-group-title text-info text-uppercase bg-light h5">
        <div class="custom-control custom-checkbox table-custom-checkbox btn-on-stretched" id="categoryCheckbox`+category.id+`">
          <input class="custom-control-input category-parent filter-category-` + category.id + `-checkbox" id="customCategoryCheck`+category.id+`" type="checkbox" checked='' />
          <label class="custom-control-label" for="customCategoryCheck`+category.id+`">` + category.name + `</label>
        </div>
      </div>`;
    checkboxesFilterCategories += '</div>';
  }
  checkboxesFilterCategories += '</div></div>';
  filterTasksBody.innerHTML = checkboxesFilterCategories;
  mobiscroll.enhance(filterTasksBody);
}

function selectColor(color, setColor) {
  var selectedElm = document.querySelector('.crud-color-c.selected');
  var newSelected = document.querySelector('.crud-color-c[data-value="' + color + '"]');

  if (selectedElm) {
      selectedElm.classList.remove('selected')
  }
  if (newSelected) {
      newSelected.classList.add('selected')
  }
  if (setColor) {
      pickedColor.style.background = color;
  }
}

function setSelectedColor() {
  tempEvent.color = tempColor;
  pickedColor.style.background = tempColor;
  colorPicker.close();
}

function getResource(res) {
  // let p = robotehResources[0].children[0].children.find(p => {return p.id == res});
  // let r = robotehResources[0].children[1].children.find(r => {return r.id == res});
  // let c = robotehResources[1].children[0].children.find(c => {return c.id == res});
  // dbRobotehResources[0].children.find(p => {return p.id == res}) ||
  if (res == 'employees')
    return {id:'employees', name: 'Zaposleni'};
  else
    return dbRobotehResources[0].children.find(p => {return p.id == res})
      || dbRobotehResources[0].children[0].children.find(p => {return p.id == res})
      || dbRobotehResources[0].children[1].children.find(r => {return r.id == res})
      || dbRobotehResources[0].children[2].children.find(c => {return c.id == res})
      || dbRobotehResources[0].children[3].children.find(c => {return c.id == res})
      || dbRobotehResources[0].children[4].children.find(c => {return c.id == res})
      || dbRobotehResources[0].children[5].children.find(c => {return c.id == res})
      || dbRobotehResources[0].children[6].children.find(c => {return c.id == res})
      || dbRobotehResources[0].children[7].children.find(c => {return c.id == res})
      || dbRobotehResources[0].children[8].children.find(c => {return c.id == res})
      || dbRobotehResources[0].children[9].children.find(c => {return c.id == res})
      || dbRobotehResources[0].children[10].children.find(c => {return c.id == res})
      || dbRobotehResources[0].children[11].children.find(c => {return c.id == res})
      || dbRobotehResources[0].children[12].children.find(c => {return c.id == res})
      || dbRobotehResources[0].children[13].children.find(c => {return c.id == res})
      || dbRobotehResources[0].children[14].children.find(c => {return c.id == res})
      || dbRobotehResources[0].children[15].children.find(c => {return c.id == res})
      || dbRobotehResources[0].children[16].children.find(c => {return c.id == res})
      || dbRobotehResources[0].children[17].children.find(c => {return c.id == res});
}

//open conflicts modal and fill modal with new conflicts
function openConflictModal(conflicts, editConflicts){
  debugger;
  editConflicts ? $('#editConflictsBody').empty() : $('#conflictsBody').empty();
  for (const userConflicts of conflicts) {
    if (userConflicts.length == 0) 
      continue;
    let workerName = userConflicts[0].name + ' ' + userConflicts[0].surname;
    let listElements = '';
    for (const conflict of userConflicts) {
      let linkStart = '';
      let linkEnd = '';
      let linkTask = '';
      if (conflict.id_project){ // project task
        if (loggedUser.role.toLowerCase().substring(0,5) == 'vodja' || loggedUser.role.toLowerCase() == 'admin'){
          linkStart = `<a href="/projects/id?id=` + conflict.id_project + `" class="alert-link">`;
          linkTask = `<a href="/projects/id?id=` + conflict.id_project + `&taskId=` + conflict.id_task + `" class="alert-link">`;
          linkEnd = '</a>';
        }
        listElements += `<li class="list-group-item">
          <div>
            ` + linkStart + `<u>` + conflict.project_name + `</u>` + linkEnd + ` na opravilu ` + linkTask + `<u>` + conflict.task_name + `</u>` + linkEnd + ` v času <u>` + new Date(conflict.task_start).toLocaleDateString('sl')+` - `+new Date(conflict.task_finish).toLocaleDateString('sl')+`</u>.
          </div>
        </li>`;
      }
      else if (conflict.id_subscriber){ // subscriber task
        //servis conflicts
        if (loggedUser.role.toLowerCase().substring(0,5) == 'vodja' || loggedUser.role.toLowerCase() == 'admin'){
          linkStart = `<a href="/servis/?taskId=` + conflict.id_task + `" class="alert-link">`;
          linkEnd = '</a>';
        }
        listElements += `<li class="list-group-item">
          <div>
            Servis ` + linkStart + `<u>` + conflict.task_name + `</u>` + linkEnd + ` pri <u>` + conflict.subscriber + `</u> v času <u>` + new Date(conflict.task_start).toLocaleDateString('sl') + ` - ` + new Date(conflict.task_finish).toLocaleDateString('sl') + `</u>.
          </div>
        </li>`;
      }
      else{ // absence
        if (loggedUser.role.toLowerCase().substring(0,5) == 'vodja' || loggedUser.role.toLowerCase() == 'admin'){
          linkStart = `<a href="/employees/absence?id=` + conflict.id_user + `" class="alert-link">`;
          linkEnd = '</a>';
        }
        listElements += `<li class="list-group-item">
          <div>
            ` + linkStart + `<u>` + conflict.project_name + `</u>` + linkEnd + ` v času <u>` + new Date(conflict.task_start).toLocaleDateString('sl') + ` - ` + new Date(conflict.task_finish).toLocaleDateString('sl') + `</u>.
          </div>
        </li>`;
      }
    }
    var element = `<div><strong>` + workerName + `:</strong></div>
      <ul class="list-group mb-2">
        ` + listElements + `
      </ul>`;
    editConflicts ? $('#editConflictsBody').append(element) : $('#conflictsBody').append(element);
  }
  editConflicts ? $('#modalEditConflicts').modal() : $('#modalConflicts').modal();
}
// open conflicts modal for drag and drop action and fill modal with new conflicts
function openDragConflictModal(conflicts){
  debugger;
  $('#editDragConflictsBody').empty();
  for (const userConflicts of conflicts) {
    if (userConflicts.length == 0) 
      continue;
    let workerName = userConflicts[0].name + ' ' + userConflicts[0].surname;
    let listElements = '';
    for (const conflict of userConflicts) {
      let linkStart = '';
      let linkEnd = '';
      let linkTask = '';
      if (conflict.id_project){ // project task
        if (loggedUser.role.toLowerCase().substring(0,5) == 'vodja' || loggedUser.role.toLowerCase() == 'admin'){
          linkStart = `<a href="/projects/id?id=` + conflict.id_project + `" class="alert-link">`;
          linkTask = `<a href="/projects/id?id=` + conflict.id_project + `&taskId=` + conflict.id_task + `" class="alert-link">`;
          linkEnd = '</a>';
        }
        listElements += `<li class="list-group-item">
          <div>
            ` + linkStart + `<u>` + conflict.project_name + `</u>` + linkEnd + ` na opravilu ` + linkTask + `<u>` + conflict.task_name + `</u>` + linkEnd + ` v času <u>` + new Date(conflict.task_start).toLocaleDateString('sl')+` - `+new Date(conflict.task_finish).toLocaleDateString('sl')+`</u>.
          </div>
        </li>`;
      }
      else if (conflict.id_subscriber){ // subscriber task
        //servis conflicts
        if (loggedUser.role.toLowerCase().substring(0,5) == 'vodja' || loggedUser.role.toLowerCase() == 'admin'){
          linkStart = `<a href="/servis/?taskId=` + conflict.id_task + `" class="alert-link">`;
          linkEnd = '</a>';
        }
        listElements += `<li class="list-group-item">
          <div>
            Servis ` + linkStart + `<u>` + conflict.task_name + `</u>` + linkEnd + ` pri <u>` + conflict.subscriber + `</u> v času <u>` + new Date(conflict.task_start).toLocaleDateString('sl') + ` - ` + new Date(conflict.task_finish).toLocaleDateString('sl') + `</u>.
          </div>
        </li>`;
      }
      else{ // absence
        if (loggedUser.role.toLowerCase().substring(0,5) == 'vodja' || loggedUser.role.toLowerCase() == 'admin'){
          linkStart = `<a href="/employees/absence?id=` + conflict.id_user + `" class="alert-link">`;
          linkEnd = '</a>';
        }
        listElements += `<li class="list-group-item">
          <div>
            ` + linkStart + `<u>` + conflict.project_name + `</u>` + linkEnd + ` v času <u>` + new Date(conflict.task_start).toLocaleDateString('sl') + ` - ` + new Date(conflict.task_finish).toLocaleDateString('sl') + `</u>.
          </div>
        </li>`;
      }
    }
    var element = `<div><strong>` + workerName + `:</strong></div>
      <ul class="list-group mb-2">
        ` + listElements + `
      </ul>`;
    $('#editDragConflictsBody').append(element);
  }
  $('#modalDragConflicts').modal();
}
function calcDurationDays(start, finish){
  let firstDay = new Date(start);
  let lastDay = new Date(finish);
  let oneDay = 24*60*60*1000;
  return Math.round(Math.abs((firstDay.getTime() - lastDay.getTime()) / (oneDay)))+1;
}
// add new project task
function addNewTask() {
  debugger;
  let projectId = $('#taskProject').val();
  let finish = $('#taskFinish').val();
  let start = $('#taskStart').val();
  let workers = $('#taskWorkers').val();
  let name = $('#taskName').val();
  let category = $('#taskCategory').val();
  let notes = $("#taskNote").val();
  let duration = start && finish ? calcDurationDays(start, finish) : 0;
  let data = {start, finish, duration, name, workers: workers.toString(), projectId, notes, category};
  let eventId = tempEvent.id.substring(0, 5) == 'event' ? tempEvent.id.substring(6) : '';
  debugger;
  $.ajax({
    type: 'POST',
    url: '/apiv2/tasks',
    contentType: 'application/json',
    data: JSON.stringify(data), // access in body
  }).done(function (resp) {
    //console.log('SUCCESS');
    debugger;
    tempEvent.real_id = resp.task.id;
    tempEvent.id = 'task_' + resp.task.id;
    tempEvent.owner = resp.task.author_id;
    tempEvent.owner_name = loggedUser.name + ' ' + loggedUser.surname;
    tempEvent.created = new Date();
    resp.task.resource = [];
    tempEvent.author_id = resp.task.author_id;
    tempEvent.author = resp.task.author;
    tempEvent.owner = resp.task.author_id;
    tempEvent.owner_name = resp.task.author;
    tempEvent.start = resp.task.task_start;
    tempEvent.end = resp.task.task_finish;
    tempEvent.task_start = resp.task.task_start;
    tempEvent.task_finish = resp.task.task_finish;
    tempEvent.category = resp.task.category;
    tempEvent.category_id = resp.task.category_id;
    tempEvent.workers = resp.task.workers;
    tempEvent.workers_id = resp.task.workers_id;
    tempEvent.notes = resp.task.task_name;
    tempEvent.project_id = resp.task.project_id;
    tempEvent.project_name = resp.task.project_name;
    tempEvent.project_number = resp.task.project_number;
    tempEvent.task_note = resp.task.task_note;
    tempEvent.task_name = resp.task.task_name;
    tempEvent.active = resp.task.active;
    tempEvent.sub_id = resp.task.sub_id;
    tempEvent.sub_name = resp.task.sub_name;
    tempEvent.weekend = resp.task.weekend;
    tempEvent.priority = resp.task.priority;
    tempEvent.priority_id = resp.task.priority_id;
    tempEvent.editable = true;
    if (resp.task.project_id){
      tempEvent.title = resp.task.project_number + ' - ' + resp.task.project_name;
      tempEvent.location = resp.task.subscriber;
    }
    else{
      tempEvent.title = 'Servis - Storitev';
      tempEvent.location = resp.task.sub_name;
    }
    let resource = resp.task.workers_id.split(',');
    for (const tmp of resource) {
      resp.task.resource.push('user' + tmp);
    }
    tempEvent.resource = resp.task.resource;
    
    calendar.updateEvent(tempEvent);
    deleteEvent = false;
    // navigate the calendar to the correct view
    calendar.navigate(tempEvent.start);

    popup.close();
    if (isMobileDevice()){
      $('.navbar').show();
      $('.notification-bar').show();
    }
    if ($('#modalConflicts').hasClass('show')){
      $('#modalConflicts').modal('toggle');
    }

    // delete event if it was converted from event
    if (eventId){
      var dataEvent = {eventId, activity: false};
      $.ajax({
        type: 'DELETE',
        url: '/apiv2/events/'+eventId,
        contentType: 'application/json',
        data: JSON.stringify(dataEvent), // access in body
      }).done(function (resp) {
        // console.log('SUCCESS');
      }).fail(function (resp) {
        //console.log('FAIL');
        //debugger;
        $('#errorModalMsg').text('Napaka pri brisanju dogodka v podatkovni bazi. Osvežite stran in ob ponovni napaki obvestite admninistratorja RIS-a.')
        $('#modalError').modal('toggle');
      })
    }

  }).fail(function (resp) {
    //console.log('FAIL');
    //debugger;
    popup.close();
    if (isMobileDevice()){
      $('.navbar').show();
      $('.notification-bar').show();
    }
    $('#errorModalMsg').text('Napaka pri kreiranju v podatkovni bazi. Osvežite stran in ob ponovni napaki obvestite admninistratorja RIS-a.');
    $('#modalError').modal('toggle');
  })
}
// give data and edit event
function editEvent(data) {
  $.ajax({
    type: 'PUT',
    url: '/apiv2/events/'+tempEditEvent.real_id,
    contentType: 'application/json',
    data: JSON.stringify(data), // access in body
  }).done(function (resp) {
    //console.log('SUCCESS');
    tempEditEvent.resource = resp.event.resource;
    calendar.updateEvent(tempEditEvent);

    // navigate the calendar to the correct view
    calendar.navigate(new Date(tempEditEvent.start));;

    restoreEvent = false;
    popup.close();
    if (isMobileDevice()){
      $('.navbar').show();
      $('.notification-bar').show();
    }

  }).fail(function (resp) {
    //console.log('FAIL');
    //debugger;
    popup.close();
    if (isMobileDevice()){
      $('.navbar').show();
      $('.notification-bar').show();
    }
    $('#errorModalMsg').text('Napaka pri posodobitvi podatkov v podatkovni bazi. Osvežite stran in ob ponovni napaki obvestite admninistratorja RIS-a.')
    $('#modalError').modal('toggle');
  })
}
// give data and edit event
function editTask(data) {
  // let data = {start, finish, duration, name, workers: workers.toString(), projectId, notes, category, weekend: tempEditEvent.weekend, priority: tempEditEvent.priority_id, active: tempEditEvent.active};
  data.weekend = data.weekend ? data.weekend : tempEditEvent.weekend;
  data.priority = data.priority ? data.priority : tempEditEvent.priority_id;
  data.active = data.active ? data.active : tempEditEvent.active;
  $.ajax({
    type: 'PUT',
    url: '/apiv2/tasks/'+tempEditEvent.real_id,
    contentType: 'application/json',
    data: JSON.stringify(data), // access in body
  }).done(function (resp) {
    //console.log('SUCCESS');

    resp.task.resource = [];
    // tempEditEvent.author_id = resp.task.author_id;
    // tempEditEvent.author = resp.task.author;
    // tempEditEvent.owner = resp.task.author_id;
    // tempEditEvent.owner_name = resp.task.author;
    tempEditEvent.start = resp.task.task_start;
    tempEditEvent.end = resp.task.task_finish;
    tempEditEvent.category = resp.task.category;
    tempEditEvent.category_id = resp.task.category_id;
    tempEditEvent.workers = resp.task.workers;
    tempEditEvent.workers_id = resp.task.workers_id;
    tempEditEvent.notes = resp.task.task_name;
    tempEditEvent.project_id = resp.task.project_id;
    tempEditEvent.project_name = resp.task.project_name;
    tempEditEvent.project_number = resp.task.project_number;
    tempEditEvent.sub_id = resp.task.sub_id;
    tempEditEvent.sub_name = resp.task.sub_name;
    // tempEditEvent.weekend = resp.task.weekend;
    // tempEditEvent.priority = resp.task.priority;
    // tempEditEvent.priority_id = resp.task.priority_id;
    if (resp.task.project_id){
      tempEditEvent.title = resp.task.project_number + ' - ' + resp.task.project_name;
      tempEditEvent.location = resp.task.subscriber;
    }
    else{
      tempEditEvent.title = 'Servis - Storitev';
      tempEditEvent.location = resp.task.sub_name;
    }
    let resource = resp.task.workers_id.split(',');
    for (const tmp of resource) {
      resp.task.resource.push('user' + tmp);
    }
    tempEditEvent.resource = resp.task.resource;
    
    calendar.updateEvent(tempEditEvent);

    // navigate the calendar to the correct view
    calendar.navigate(new Date(tempEditEvent.start));;

    restoreEvent = false;
    popup.close();
    if (isMobileDevice()){
      $('.navbar').show();
      $('.notification-bar').show();
    }
    if ($('#modalEditConflicts').hasClass('show')){
      $('#modalEditConflicts').modal('toggle');
    }

  }).fail(function (resp) {
    //console.log('FAIL');
    //debugger;
    popup.close();
    if (isMobileDevice()){
      $('.navbar').show();
      $('.notification-bar').show();
    }
    $('#errorModalMsg').text('Napaka pri posodobitvi podatkov v podatkovni bazi. Osvežite stran in ob ponovni napaki obvestite admninistratorja RIS-a.')
    $('#modalError').modal('toggle');
  })
}
// transform task to event -> first add new event and on success delete task
function transformTaskToEvent(currentEvent) {
  // debugger;
  // let resources = [];
  // $('#work-order-popup').find('.mbsc-row input:checked').each(function() {
  //   resources.push($(this).val());
  // });
  // currentEvent.resource = resources;
  let workers = $('#eventWorkers').val();
  workers.forEach((worker, index) => { workers[index] = 'user' + worker; });
  currentEvent.resource = workers;
  currentEvent.editable = true;
  currentEvent.title = titleInput.value;
  currentEvent.location = locationInput.value;
  currentEvent.color = currentEvent.color ? currentEvent.color : rgb2hex($('#event-color').css('background-color'));
  currentEvent.start = $('#workOrderStart').val().length == 10 ? $('#workOrderStart').val()+'T07:00' : $('#workOrderStart').val();
  currentEvent.end = $('#workOrderEnd').val().length == 10 ? $('#workOrderEnd').val()+'T15:00' : $('#workOrderEnd').val();
  let taskId = currentEvent.real_id; // use taskId to delete correct task (mark as unactive)
  // debugger;
  // testing case first add new event and then delete current task
  // create event in database and if success then add it to calendar otherwise close popup and open error modal with correct error message
  let data = {start: currentEvent.start, end: currentEvent.end, title: currentEvent.title, resource: currentEvent.resource, location: currentEvent.location, notes: currentEvent.notes, allDay : currentEvent.allDay, color: currentEvent.color};
  $.ajax({
    type: 'POST',
    url: '/apiv2/events',
    contentType: 'application/json',
    data: JSON.stringify(data), // access in body
  }).done(function (resp) {
    //console.log('SUCCESS');
    currentEvent.real_id = resp.event.id;
    currentEvent.id = 'event_' + resp.event.id;
    currentEvent.owner = resp.event.owner;
    currentEvent.owner_name = loggedUser.name + ' ' + loggedUser.surname;
    currentEvent.created = new Date();
    currentEvent.category_id = null;
    currentEvent.category = null;
    currentEvent.category_id = null;
    currentEvent.task_duration = null;
    currentEvent.task_start = null;
    currentEvent.task_finish = null;
    currentEvent.task_name = null;
    currentEvent.task_note = null;
    calendar.updateEvent(currentEvent);
    deleteEvent = false;

    // navigate the calendar to the correct view
    calendar.navigate(currentEvent.start);

    popup.close();
    if (isMobileDevice()){
      $('.navbar').show();
      $('.notification-bar').show();
    }

    // now delete current task since it was converted to event
    var data = {eventId: taskId, activity: false};
    $.ajax({
      type: 'DELETE',
      url: '/apiv2/tasks/'+taskId,
      contentType: 'application/json',
      data: JSON.stringify(data), // access in body
    }).done(function (resp) {
      // console.log('SUCCESS');

    }).fail(function (resp) {
      //console.log('FAIL');
      //debugger;
      // popup.close();
      $('#errorModalMsg').text('Napaka pri brisanju opravila v podatkovni bazi. Osvežite stran in ob ponovni napaki obvestite admninistratorja RIS-a.')
      $('#modalError').modal('toggle');
    })

  }).fail(function (resp) {
    //console.log('FAIL');
    //debugger;
    popup.close();
    if (isMobileDevice()){
      $('.navbar').show();
      $('.notification-bar').show();
    }
    $('#errorModalMsg').text('Napaka pri kreiranju v podatkovni bazi. Osvežite stran in ob ponovni napaki obvestite admninistratorja RIS-a.')
    $('#modalError').modal('toggle');
  })
}
// transform task to event -> first add new event and on success delete task
function transformEventToTask(currentEvent) {
  debugger;

  let projectId = $('#taskProject').val();
  let finish = $('#taskFinish').val();
  let start = $('#taskStart').val();
  let workers = $('#taskWorkers').val();
  let name = $('#taskName').val();
  let category = $('#taskCategory').val();
  let notes = $("#taskNote").val();
  let duration = start && finish ? calcDurationDays(start, finish) : 0;
  // console.log('konflikti override, vseeno klici funkcijo za dodajanje opravila');
  // let data = {start, finish, duration, name, workers: workers.toString(), projectId, notes, category};
  let data = {start, finish, workers: workers.toString(), projectId};
  $.ajax({
    type: 'GET',
    url: '/apiv2/tasks/conflicts',
    contentType: 'application/json',
    data: data, // access in body
  }).done(function (resp) {
    //console.log('SUCCESS');
    // if conflicts then open modal with conflicts otherwise add new task
    if (resp.conflicts.length > 0){
      // open modal with conflicts
      openConflictModal(resp.conflicts);
    }
    else{
      // add new task
      // console.log('ni konfliktov, klici funkcijo za dodajanje opravila')
      let data = {start, finish, duration, name, workers: workers.toString(), projectId, notes, category};
      addNewTask(data);
    }
  }).fail(function (resp) {
    //console.log('FAIL');
    //debugger;
    popup.close();
    if (isMobileDevice()){
      $('.navbar').show();
      $('.notification-bar').show();
    }
    $('#errorModalMsg').text('Napaka pri pridobivanju podaktov za konflikte. Osvežite stran in ob ponovni napaki obvestite admninistratorja RIS-a.')
    $('#modalError').modal('toggle');
  })

}
// on event click function
function onEventMobiscrollClick(args){
  oldEvent = { ...args.event };
  tempEvent = args.event;
  debugger;

  if (!popup.isVisible()) {
    if (args.event.editable)
      createEditPopup(args);
  }
}
// on event hover function
function onEventMobiscrollHoverIn(args, inst){
  var event = args.event;
  tempEvent = args.event;

  $('#tooltip-title').html('Lokacija: ');
  $('#tooltip-notes').html('Opravilo: ');
    
  // var resource = doctors.find(function (dr) { return dr.id === event.resource });
  var eventTime = formatDate('DD .MM. YYYY', new Date(event.start)) + ' - ' + formatDate('DD. MM. YYYY', new Date(event.end));
  var userLabel = '';
  for (const userId of event.resource) {
    let user = users.find(u => u.id == userId.substring(4));
    if (user) userLabel += ', ' + user.name + ' ' + user.surname;
  }

  currentEvent = event;

  tooltipHeader.style.backgroundColor = event.color;
  // console.log(event.color);
  if (event.color == '#ffeb3c'){
    tooltipHeader.style.color = '#444';
  }
  else
    tooltipHeader.style.color = '#eee';
  tooltipLoc.innerHTML = event.location ? event.location : '|';
  tooltipTime.innerHTML = eventTime;
  tooltipTitle.innerHTML = event.title;
  tooltipNotes.innerHTML = event.notes;
  tooltipUsers.innerHTML = userLabel.substring(2);
  tooltipOwner.innerHTML = event.owner ? event.owner_name + ' (' + new Date(event.created).toLocaleString('sl') + ')' : 'Ni pravega podatka';
  $('#tooltip-title').removeClass('display-none');

  clearTimeout(timer);
  timer = null;

  tooltip.setOptions({ anchor: args.domEvent.target });
  $('#tooltip-changes-button').off();
  $('#tooltip-event-subtasks').off();
  $('#tooltip-event-buildparts').off();
  $('#custom-task-check').off();
  $('#tooltipHistory').html('');
  $('#tooltip-event-progress').removeClass();
  $('#tooltipSubtasks').empty();
  $('#tooltipBuildparts').empty();
  (loggedUser.role.toLowerCase() == 'admin' || loggedUser.name + ' ' + loggedUser.surname == tempEvent.owner_name) ? $('#tooltip-event-delete').removeClass('d-none') : $('#tooltip-event-delete').addClass('d-none');
  (loggedUser.role.toLowerCase() == 'admin' || loggedUser.name + ' ' + loggedUser.surname == tempEvent.owner_name || event.resource.find(r => r == 'user'+loggedUser.id)) ? $('#custom-task-check').prop('disabled', false) : $('#custom-task-check').prop('disabled', true);
  if(loggedUser.role.toLowerCase().substring(0,4) == 'info'){
    for (let worker of tempEvent.resource) {
      if (users.find( u => u.id == worker.substring(4) && u.role.toLowerCase().includes('elektri') && loggedUser.role.toLowerCase() == 'info elektro')) {$('#custom-task-check').prop('disabled', false); break; }
    }
  }
  // customize tooltip for task
  if (event.id.substring(0,4) == 'task'){
    $('#tooltip-changes-button').on('click', {taskId: event.real_id, eventId: null}, getChanges);
    $('#tooltip-event-subtasks').on('click', getSubstasks);
    $('#tooltip-event-buildparts').on('click', getBuildParts);
    $('#tooltip-event-shortcut').removeClass('d-none');
    $('#tooltip-event-shortcut').prop("href", '/projects/id?id=' + event.project_id + '&taskId=' + event.real_id + '&type=2');
    ( event.category != 'Strojna izdelava' && event.subtask_count > 0 ) ? $('#tooltip-event-subtasks').removeClass('d-none') : $('#tooltip-event-subtasks').addClass('d-none');
    ( event.category == 'Strojna izdelava' && event.build_parts_count > 0 ) ? $('#tooltip-event-buildparts').removeClass('d-none') : $('#tooltip-event-buildparts').addClass('d-none');
    ( event.category != 'Strojna izdelava' && event.subtask_count > 0 || event.category == 'Strojna izdelava' && event.build_parts_count > 0 ) ? $('#tooltip-event-checkbox').addClass('d-none') : $('#tooltip-event-checkbox').removeClass('d-none');
    event.completion == 100 ? $('#custom-task-check').prop("checked", true) : $('#custom-task-check').prop("checked", false);
    // if ( event.category != 'Strojna izdelava' && event.subtask_count > 0 || event.category == 'Strojna izdelava' && event.build_parts_count > 0 ) $('#custom-task-check').on('click', {taskId: event.real_id, eventId: null}, patchTaskCompletion);
    $('#custom-task-check').on('click', {taskId: event.real_id, eventId: null}, patchTaskCompletion);
    $('#tooltip-event-progress').addClass('progress-bar w-' + event.completion);
  }
  // customize tooltip for absence
  else if (event.id.substring(0,7) == 'absence'){
    $('#tooltip-changes-button').on('click', {taskId: null, eventId: null, absenceId: event.real_id}, getChanges);
    $('#tooltip-event-subtasks').addClass('d-none');
    $('#tooltip-event-shortcut').addClass('d-none');
    $('#tooltip-event-buildparts').addClass('d-none');
    $('#tooltip-event-checkbox').addClass('d-none');
    $('#tooltip-event-progress').parent().addClass('d-none');
    $('#tooltip-event-delete').addClass('d-none');
    // custom names for absences
    $('#tooltip-notes').html('Tip/Razlog: ');
    $('#tooltip-title').html('');
    tooltipTitle.innerHTML = '';

    // hide tooltip-title if it is empty
    $('#tooltip-title').addClass('display-none');
    
    // event.completion ? $('#tooltip-event-progress').addClass('progress-bar w-100') : $('#tooltip-event-progress').addClass('progress-bar w-0');
    // event.completion ? $('#custom-task-check').prop("checked", true) : $('#custom-task-check').prop("checked", false);
  }
  // customize tooltip for event
  else{
    $('#tooltip-changes-button').on('click', {taskId: null, eventId: event.real_id}, getChanges);
    $('#custom-task-check').on('click', {taskId: null, eventId: event.real_id}, patchEventCompletion);
    $('#tooltip-event-subtasks').addClass('d-none');
    $('#tooltip-event-shortcut').addClass('d-none');
    $('#tooltip-event-buildparts').addClass('d-none');
    $('#tooltip-event-checkbox').removeClass('d-none');
    event.completion ? $('#tooltip-event-progress').addClass('progress-bar w-100') : $('#tooltip-event-progress').addClass('progress-bar w-0');
    event.completion ? $('#custom-task-check').prop("checked", true) : $('#custom-task-check').prop("checked", false);
  }
  // event.id.substring(0,4) == 'task' ? $('#tooltip-changes-button').on('click', {taskId: event.real_id, eventId: null}, getChanges) : $('#tooltip-changes-button').on('click', {taskId: null, eventId: event.real_id}, getChanges);
  // $('#tooltip-changes-button').on('click', {taskId: null, event: null}, getChanges);
  tooltip.open();
  refreshTimerCount = 0;
}
// detect touch device
function isTouchDevice() {
  return (('ontouchstart' in window) ||
     (navigator.maxTouchPoints > 0) ||
     (navigator.msMaxTouchPoints > 0));
}
// detect mobile device
function isMobileDevice() {
  let check = false;
  (function(a){if(/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino/i.test(a)||/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(a.substr(0,4))) check = true;})(navigator.userAgent||navigator.vendor||window.opera);
  return check;
};
// simple mobile check
function isSimpleMobileCheck(){
  return /Mobi/.test(navigator.userAgent)
}
// function to get subtasks for selected project task
function getSubstasks(event){
  // debugger;
  $('#tooltipSubtasks').empty();
  $('#tooltipBuildparts').empty();
  let data = {taskId: tempEvent.real_id};
  $.ajax({
    type: 'GET',
    url: '/apiv2/subtasks',
    contentType: 'application/json',
    data: data, // access in query
  }).done(function (resp) {
    if(resp.subtasks){
      for (const subtask of resp.subtasks) {
        if (!subtask.active) continue;
        let subtaskCheck = subtask.completed ? ' checked=""' : '';
        let subtaskElement = `<div class="list-group-item p-2" id="subtask` + subtask.id + `">
          <div class="row">
            <div class="col-10">
              <label class="mr-auto w-90" id="subtaskName` + subtask.id + `">` + subtask.name + `</label>
            </div>
            <div class="col-1 d-flex">
              <div class="custom-control custom-checkbox ml-auto mt-2">
                <input class="custom-control-input" id="customSubCheck` + subtask.id + `" type="checkbox"` + subtaskCheck + `>
                <label class="custom-control-label" for="customSubCheck` + subtask.id + `"> </label>
              </div>
            </div>
          </div>
        </div>`;
        $('#tooltipSubtasks').append(subtaskElement);
        ((loggedUser.role.toLowerCase() == 'admin' || loggedUser.name + ' ' + loggedUser.surname == tempEvent.owner_name || tempEvent.resource.find(r => r == 'user'+loggedUser.id))) ? $('#customSubCheck' + subtask.id).prop('disabled', false) : $('#customSubCheck' + subtask.id).prop('disabled', true);
        if(loggedUser.role.toLowerCase().substring(0,4) == 'info'){
          for (let worker of tempEvent.resource) {
            if (users.find( u => u.id == worker.substring(4) && u.role.toLowerCase().includes('elektri') && loggedUser.role.toLowerCase() == 'info elektro')) {$('#customSubCheck' + subtask.id).prop('disabled', false); break; }
          }
        }
        $('#customSubCheck' + subtask.id).on('click', {subtaskId: subtask.id}, patchSubtaskCompletion);
      }
      // debugger;
    }
    else{
      $('#modalError').modal('toggle');
    }
  }).fail(function (resp) {
    //console.log('FAIL');
    //debugger;
    $('#modalError').modal('toggle');
  }).always(function (resp) {
    //console.log('ALWAYS');
    //debugger;
  });
}
// function to get buildparts for selected project task
function getBuildParts(event){
  // debugger;
  $('#tooltipSubtasks').empty();
  $('#tooltipBuildparts').empty();
  let data = {taskId: tempEvent.real_id};
  $.ajax({
    type: 'GET',
    url: '/apiv2/buildparts',
    contentType: 'application/json',
    data: data, // access in query
  }).done(function (resp) {
    if(resp.buildparts){
      for (const buildpart of resp.buildparts) {
        if (!buildpart.active) continue;
        let buildpartCheck = buildpart.completion == 100 ? ' checked=""' : '';
        let buildpartElement = `<div class="list-group-item p-2" id="buildpart` + buildpart.id + `">
          <div class="row">
            <div class="col-10">
              <label class="mr-auto w-90" id="buildpartName` + buildpart.id + `">` + buildpart.name + `</label>
              <small id="buildpartStandard" class="form-text text-muted mt-n4 btn-on-stretched">` + buildpart.standard + `</small>
            </div>
            <div class="col-1 d-flex">
              <div class="custom-control custom-checkbox ml-auto mt-2">
                <input class="custom-control-input" id="customBuildPartCheck` + buildpart.id + `" type="checkbox"` + buildpartCheck + `>
                <label class="custom-control-label" for="customBuildPartCheck` + buildpart.id + `"> </label>
              </div>
            </div>
          </div>
        </div>`;
        $('#tooltipBuildparts').append(buildpartElement);
        ((loggedUser.role.toLowerCase() == 'admin' || loggedUser.name + ' ' + loggedUser.surname == tempEvent.owner_name || tempEvent.resource.find(r => r == 'user'+loggedUser.id))) ? $('#customBuildPartCheck' + buildpart.id).prop('disabled', false) : $('#customBuildPartCheck' + buildpart.id).prop('disabled', true);
        if (buildpart.build_phases_count && buildpart.build_phases_count > 0) $('#customBuildPartCheck' + buildpart.id).prop('disabled', true);
        $('#customBuildPartCheck' + buildpart.id).on('click', {buildpartId: buildpart.id}, patchBuildpartCompletion);
      }
      // debugger;
    }
    else{
      $('#modalError').modal('toggle');
    }
  }).fail(function (resp) {
    //console.log('FAIL');
    //debugger;
    $('#modalError').modal('toggle');
  }).always(function (resp) {
    //console.log('ALWAYS');
    //debugger;
  });
}
// PATCH event completion
function patchEventCompletion(event){
  // debugger;
  let completion = $('#custom-task-check').prop('checked');
  var data = {eventId: tempEvent.real_id, completion};
  $.ajax({
    type: 'PATCH',
    url: '/apiv2/events/'+tempEvent.real_id,
    contentType: 'application/json',
    data: JSON.stringify(data), // access in body
  }).done(function (resp) {
    // console.log('SUCCESS');
    $('#tooltip-event-progress').removeClass();
    completion ? $('#tooltip-event-progress').addClass('progress-bar w-100') : $('#tooltip-event-progress').addClass('progress-bar w-0');
    tempEvent.completion = completion;
    calendar.updateEvent(tempEvent);
  }).fail(function (resp) {
    //console.log('FAIL');
    //debugger;
    $('#errorModalMsg').text('Napaka pri posodobitvi končnosti dogodka v podatkovni bazi. Osvežite stran in ob ponovni napaki obvestite admninistratorja RIS-a.')
    $('#modalError').modal('toggle');
  })
}
// PATCH task completion
function patchTaskCompletion(event){
  // debugger;
  let completion = $('#custom-task-check').prop('checked');
  var data = {taskId: tempEvent.real_id, completion};
  $.ajax({
    type: 'PATCH',
    url: '/apiv2/tasks/'+tempEvent.real_id,
    contentType: 'application/json',
    data: JSON.stringify(data), // access in body
  }).done(function (resp) {
    // console.log('SUCCESS');
    $('#tooltip-event-progress').removeClass();
    completion ? $('#tooltip-event-progress').addClass('progress-bar w-100') : $('#tooltip-event-progress').addClass('progress-bar w-0');
    tempEvent.completion = resp.task.completion;
    // console.log(resp)
    calendar.updateEvent(tempEvent);
  }).fail(function (resp) {
    //console.log('FAIL');
    //debugger;
    $('#errorModalMsg').text('Napaka pri posodobitvi končnosti opravila v podatkovni bazi. Osvežite stran in ob ponovni napaki obvestite admninistratorja RIS-a.')
    $('#modalError').modal('toggle');
  })
}
// PATCH subtask completion
function patchSubtaskCompletion(event){
  // debugger;
  // console.log(event.data.subtaskId);
  let completion = $('#customSubCheck' + event.data.subtaskId).prop('checked');
  var data = {subtaskId: event.data.subtaskId, completion};
  $.ajax({
    type: 'PATCH',
    url: '/apiv2/subtasks/'+event.data.subtaskId,
    contentType: 'application/json',
    data: JSON.stringify(data), // access in body
  }).done(function (resp) {
    // console.log('SUCCESS');
    // console.log(resp);
    // debugger;
    let taskCompletion = Math.round(document.getElementById('tooltipSubtasks').querySelectorAll('input[type="checkbox"]:checked').length / document.getElementById('tooltipSubtasks').querySelectorAll('input[type="checkbox"]').length * 100);
    tempEvent.completion = taskCompletion;
    $('#tooltip-event-progress').removeClass();
    $('#tooltip-event-progress').addClass('progress-bar w-' + taskCompletion);
    calendar.updateEvent(tempEvent);
  }).fail(function (resp) {
    //console.log('FAIL');
    //debugger;
    $('#errorModalMsg').text('Napaka pri posodobitvi končnosti naloge v podatkovni bazi. Osvežite stran in ob ponovni napaki obvestite admninistratorja RIS-a.')
    $('#modalError').modal('toggle');
  })
}
// PATCH buildpart completion
function patchBuildpartCompletion(event){
  debugger;
  let completion = $('#customBuildPartCheck' + event.data.buildpartId).prop('checked');
  var data = {buildpartId: event.data.buildpartId, completion};
  $.ajax({
    type: 'PATCH',
    url: '/apiv2/buildparts/'+event.data.buildpartId,
    contentType: 'application/json',
    data: JSON.stringify(data), // access in body
  }).done(function (resp) {
    // console.log('SUCCESS');
    // console.log(resp);
    // debugger;
    let taskCompletion = Math.round(document.getElementById('tooltipBuildparts').querySelectorAll('input[type="checkbox"]:checked').length / document.getElementById('tooltipBuildparts').querySelectorAll('input[type="checkbox"]').length * 100);
    tempEvent.completion = taskCompletion;
    $('#tooltip-event-progress').removeClass();
    $('#tooltip-event-progress').addClass('progress-bar w-' + taskCompletion);
    calendar.updateEvent(tempEvent);
  }).fail(function (resp) {
    //console.log('FAIL');
    //debugger;
    $('#errorModalMsg').text('Napaka pri posodobitvi končnosti kosa v podatkovni bazi. Osvežite stran in ob ponovni napaki obvestite admninistratorja RIS-a.')
    $('#modalError').modal('toggle');
  })
}
// function to get right changes, depending what information is in tooltip ( task or event )
function getChanges(params){
  // debugger;
  // params.data.taskId ? getTaskChanges(params.data.taskId) : getEventChanges(params.data.eventId);
  // check params if it has taskId or eventId or absenceId and call the right function for getting changes
  if (params.data.taskId) getTaskChanges(params.data.taskId);
  else if (params.data.eventId) getEventChanges(params.data.eventId);
  else if (params.data.absenceId) getAbsenceChanges(params.data.absenceId);
}
// getting task history and return them
function getTaskChanges(id) {
  // debugger;
  let data = {id};
  $.ajax({
    type: 'GET',
    url: '/apiv2/tasks/'+id+'/changes',
    contentType: 'application/json',
    data: data, // access in body
  }).done(function (resp) {
    if(resp.success){
      debugger;
      // console.log(resp.taskChanges);
      let changesLabel = '';
      for (const change of resp.taskChanges) {
        if (!change.notes){
          // console.log(change.name + ' ' + change.surname + ' je ' + moment(change.date).format('D. M. YYYY HH:mm') + ' ' + change.status);
          let typeAddition = change.type == 'nalogo' ? ' ' + change.type + ' ' + change.subtask :  '';
          if (change.type == 'kosovnico') {
            typeAddition = change.status == 'odstranil/a končanje' ? ' kosa ' + change.buildpart : ' kos ' + change.buildpart ;
          }
          changesLabel = '<hr></hr>' + change.name + ' ' + change.surname + ' je ' + moment(change.date).format('D. M. YYYY HH:mm') + ' ' + change.status + typeAddition + '<br />' + changesLabel;
        }
        else{
          let indexOfUsers = change.notes.indexOf('zadolženi:');
          let changesNotesLabel = change.notes.substring(10);
          if (indexOfUsers != '-1'){
            changesNotesLabel = change.notes.substring( 10, indexOfUsers + 11);
            let changedUsersLabel = '';
            let changedUsers = change.notes.substring(indexOfUsers + 11).split(',');
            for (const changedUser of changedUsers) {
              let user = users.find(u => u.id == changedUser)
              if(user) changedUsersLabel += ', ' + user.name + ' ' + user.surname;
            
            }
            changesNotesLabel += changedUsersLabel.substring(2);
          }
          // console.log(change.name + ' ' + change.surname + ' je ' + moment(change.date).format('D. M. YYYY HH:mm') + ' ' + change.status + changesNotesLabel);
          changesLabel = '<hr></hr>' + change.name + ' ' + change.surname + ' je ' + moment(change.date).format('D. M. YYYY HH:mm') + ' ' + change.status + changesNotesLabel + changesLabel;
        }
        $('#tooltipHistory').html(changesLabel);
      }
    }
    else{
      $('#modalError').modal('toggle');
    }
  }).fail(function (resp) {
    //console.log('FAIL');
    //debugger;
    $('#modalError').modal('toggle');
  }).always(function (resp) {
    //console.log('ALWAYS');
    //debugger;
  });
}
// getting event history
function getEventChanges(id) {
  // debugger;
  let data = {id};
  $.ajax({
    type: 'GET',
    url: '/apiv2/events/'+id+'/changes',
    contentType: 'application/json',
    data: data, // access in body
  }).done(function (resp) {
    if(resp.success){
      // debugger;
      // console.log(resp.eventChanges);
      let changesLabel = '';
      for (const change of resp.eventChanges) {
        if (!change.notes){
          // console.log(change.name + ' ' + change.surname + ' je ' + moment(change.date).format('D. M. YYYY HH:mm') + ' ' + change.status);
          changesLabel = '<hr></hr>' + change.name + ' ' + change.surname + ' je ' + moment(change.date).format('D. M. YYYY HH:mm') + ' ' + change.status + '<br />' + changesLabel;
        }
        else{
          let indexOfUsers = change.notes.indexOf('zadolženi:');
          let changesNotesLabel = change.notes.substring(10);
          if (indexOfUsers != '-1'){
            changesNotesLabel = change.notes.substring( 10, indexOfUsers + 11 );
            let changedUsersLabel = '';
            let changedUsers = change.notes.substring(indexOfUsers + 11).split(',');
            for (const changedUser of changedUsers) {
              let user = users.find(u => u.id == changedUser.substring(4))
              if(user) changedUsersLabel += ', ' + user.name + ' ' + user.surname;
            
            }
            changesNotesLabel += changedUsersLabel.substring(2);
          }
          // console.log(change.name + ' ' + change.surname + ' je ' + moment(change.date).format('D. M. YYYY HH:mm') + ' ' + change.status + changesNotesLabel);
          changesLabel = '<hr></hr>' + change.name + ' ' + change.surname + ' je ' + moment(change.date).format('D. M. YYYY HH:mm') + ' ' + change.status + changesNotesLabel + changesLabel;
        }
        $('#tooltipHistory').html(changesLabel);
      }
    }
    else{
      $('#modalError').modal('toggle');
    }
  }).fail(function (resp) {
    //console.log('FAIL');
    //debugger;
    $('#modalError').modal('toggle');
  }).always(function (resp) {
    //console.log('ALWAYS');
    //debugger;
  });
}
// getting absence history
function getAbsenceChanges(id) {
  // debugger;
  let data = {id};
  $.ajax({
    type: 'GET',
    url: '/apiv2/absences/'+id+'/changes',
    contentType: 'application/json',
    data: data, // access in body
  }).done(function (resp) {
    if(resp.absenceChanges){
      // debugger;
      // console.log(resp.absenceChanges);
      let changesLabel = '';
      for (const change of resp.absenceChanges) {
        if (!change.notes){
          // console.log(change.name + ' ' + change.surname + ' je ' + moment(change.date).format('D. M. YYYY HH:mm') + ' ' + change.status);
          changesLabel = '<hr></hr>' + change.name + ' ' + change.surname + ' je ' + moment(change.date).format('D. M. YYYY HH:mm') + ' ' + change.status + '<br />' + changesLabel;
        }
        else{
          let indexOfUsers = change.notes.indexOf('zadolženi:');
          let changesNotesLabel = change.notes.substring(10);
          if (indexOfUsers != '-1'){
            changesNotesLabel = change.notes.substring( 10, indexOfUsers + 11 );
            let changedUsersLabel = '';
            let changedUsers = change.notes.substring(indexOfUsers + 11).split(',');
            for (const changedUser of changedUsers) {
              let user = users.find(u => u.id == changedUser.substring(4))
              if(user) changedUsersLabel += ', ' + user.name + ' ' + user.surname;
            
            }
            changesNotesLabel += changedUsersLabel.substring(2);
          }
          // console.log(change.name + ' ' + change.surname + ' je ' + moment(change.date).format('D. M. YYYY HH:mm') + ' ' + change.status + changesNotesLabel);
          changesLabel = '<hr></hr>' + change.name + ' ' + change.surname + ' je ' + moment(change.date).format('D. M. YYYY HH:mm') + ' ' + change.status + changesNotesLabel + changesLabel;
        }
        $('#tooltipHistory').html(changesLabel);
      }
    }
    else{
      $('#modalError').modal('toggle');
    }
  }).fail(function (resp) {
    //console.log('FAIL');
    //debugger;
    $('#modalError').modal('toggle');
  }).always(function (resp) {
    //console.log('ALWAYS');
    //debugger;
  });
}
// function for refreshing data on button click
function refreshData(){
  let tmpS = moment(calendar._firstDay).add(-1, 'days').format("YYYY-MM-DDTHH:mm");
  let tmpE = moment(calendar._lastDay).add(1, 'days').format("YYYY-MM-DDTHH:mm");
  getData(tmpS, tmpE);
}
// function for filtering resources
function filterResources(){
  calendar.setOptions({resources: []});
  let newDbRobotehResources = [{
    id: 'employees',
    name: 'Zaposleni',
    collapsed: false,
    children: []
  }];
  let resources = [];
  // filtering resources - users
  $('#filterResourcesBody').find('.resource-child:checked').each(function() {
    let user = users.find(u => u.id == this.id.substring(23));
    if (user){
      if (user.role.toLowerCase().includes('plc')){
        let tmp = newDbRobotehResources[0].children.find(c => c.id == 'plc');
        if (tmp) tmp.children.push({ id: 'user' + user.id, name: user.name + ' ' + user.surname, role: user.role, color: user.color });
        else newDbRobotehResources[0].children.push({ id: 'plc', name: 'Programerji PLC-jev', collapsed: false, img: '/plc64.png', children: [{ id: 'user' + user.id, name: user.name + ' ' + user.surname, role: user.role, color: user.color }] });
      }
      else if (user.role.toLowerCase().includes('robot')){
        let tmp = newDbRobotehResources[0].children.find(c => c.id == 'robot');
        if (tmp) tmp.children.push({ id: 'user' + user.id, name: user.name + ' ' + user.surname, role: user.role, color: user.color });
        else newDbRobotehResources[0].children.push({ id: 'robot', name: 'Programerji robotov', collapsed: false, img: '/robot64.png', children: [{ id: 'user' + user.id, name: user.name + ' ' + user.surname, role: user.role, color: user.color }] });
      }
      else if (user.role.toLowerCase().includes('serviser')){
        let tmp = newDbRobotehResources[0].children.find(c => c.id == 'serviser');
        if (tmp) tmp.children.push({ id: 'user' + user.id, name: user.name + ' ' + user.surname, role: user.role, color: user.color });
        else newDbRobotehResources[0].children.push({ id: 'serviser', name: 'Serviserji', collapsed: false, img: '/serviser.png', children: [{ id: 'user' + user.id, name: user.name + ' ' + user.surname, role: user.role, color: user.color }] });
      }
      else if (user.role.toLowerCase().includes('konstr')){
        let tmp = newDbRobotehResources[0].children.find(c => c.id == 'konstrukter');
        if (tmp) tmp.children.push({ id: 'user' + user.id, name: user.name + ' ' + user.surname, role: user.role, color: user.color });
        else newDbRobotehResources[0].children.push({ id: 'konstrukter', name: 'Konstrukterji', collapsed: false, img: '/builder64.png', children: [{ id: 'user' + user.id, name: user.name + ' ' + user.surname, role: user.role, color: user.color }] });
      }
      else if (user.role.toLowerCase().includes('strojnik')){
        let tmp = newDbRobotehResources[0].children.find(c => c.id == 'strojnik');
        if (tmp) tmp.children.push({ id: 'user' + user.id, name: user.name + ' ' + user.surname, role: user.role, color: user.color  });
        else newDbRobotehResources[0].children.push({ id: 'strojnik', name: 'Strojniki', collapsed: false, img: '/mechanic64.png', children: [{ id: 'user' + user.id, name: user.name + ' ' + user.surname, role: user.role, color: user.color  }] });
      }
      else if (user.role.toLowerCase().includes('elektri')){
        let tmp = newDbRobotehResources[0].children.find(c => c.id == 'električar');
        if (tmp) tmp.children.push({ id: 'user' + user.id, name: user.name + ' ' + user.surname, role: user.role,  color: user.color });
        else newDbRobotehResources[0].children.push({ id: 'električar', name: 'Električarji', collapsed: false, img: '/electrican64.png', children: [{ id: 'user' + user.id, name: user.name + ' ' + user.surname, role: user.role,  color: user.color }] });
      }
      else if (user.role.toLowerCase().includes('vodja pro')){
        let tmp = newDbRobotehResources[0].children.find(c => c.id == 'vodja Projektov');
        if (tmp) tmp.children.push({ id: 'user' + user.id, name: user.name + ' ' + user.surname, role: user.role, color: user.color });
        else newDbRobotehResources[0].children.push({ id: 'vodja Projektov', name: 'Vodje projektov', collapsed: false, img: '/manager64.png', children: [{ id: 'user' + user.id, name: user.name + ' ' + user.surname, role: user.role, color: user.color }] });
      }
      else if (user.role.toLowerCase().includes('vodja stroj')){
        let tmp = newDbRobotehResources[0].children.find(c => c.id == 'vodja Strojnikov');
        if (tmp) tmp.children.push({ id: 'user' + user.id, name: user.name + ' ' + user.surname, role: user.role, color: user.color });
        else newDbRobotehResources[0].children.push({ id: 'vodja Strojnikov', name: 'Vodje strojnikov', collapsed: false, img: '/managerMech64.png', children: [{ id: 'user' + user.id, name: user.name + ' ' + user.surname, role: user.role, color: user.color }] });
      }
      else if (user.role.toLowerCase().includes('vodja cnc')){
        let tmp = newDbRobotehResources[0].children.find(c => c.id == 'vodja CNC Obdelave');
        if (tmp) tmp.children.push({ id: 'user' + user.id, name: user.name + ' ' + user.surname, role: user.role, color: user.color });
        else newDbRobotehResources[0].children.push({ id: 'vodja CNC Obdelave', name: 'Vodje CNC obdelave', collapsed: false, img: '/managerCNC64.png', children: [{ id: 'user' + user.id, name: user.name + ' ' + user.surname, role: user.role, color: user.color }] });
      }
      else if (user.role.toLowerCase().includes('vodja ele')){
        let tmp = newDbRobotehResources[0].children.find(c => c.id == 'vodja Električarjev');
        if (tmp) tmp.children.push({ id: 'user' + user.id, name: user.name + ' ' + user.surname, role: user.role, color: user.color });
        else newDbRobotehResources[0].children.push({ id: 'vodja Električarjev', name: 'Vodje električarjev', collapsed: false, img: '/managerElec64.png', children: [{ id: 'user' + user.id, name: user.name + ' ' + user.surname, role: user.role, color: user.color }] });
      }
      else if (user.role.toLowerCase().includes('tajnik') || user.role.toLowerCase().includes('komercialist') || user.role.toLowerCase().includes('komerciala')|| user.role.toLowerCase().includes('računovodstvo')){
        let tmp = newDbRobotehResources[0].children.find(c => c.id == 'tajnik');
        if (tmp) tmp.children.push({ id: 'user' + user.id, name: user.name + ' ' + user.surname, role: user.role , color: user.color });
        else newDbRobotehResources[0].children.push({ id: 'tajnik', name: 'Uprava', collapsed: false, img: '/secratery64.png', children: [{ id: 'user' + user.id, name: user.name + ' ' + user.surname, role: user.role , color: user.color }] });
      }
      else if (user.role.toLowerCase().includes('admin')){
        let tmp = newDbRobotehResources[0].children.find(c => c.id == 'admin');
        if (tmp) tmp.children.push({ id: 'user' + user.id, name: user.name + ' ' + user.surname, role: user.role , color: user.color });
        else newDbRobotehResources[0].children.push({ id: 'admin', name: 'Admini', collapsed: false, img: '/admin64.png', children: [{ id: 'user' + user.id, name: user.name + ' ' + user.surname, role: user.role , color: user.color }] });
      }
      else if (user.role.toLowerCase().includes('strojni mon')){
        let tmp = newDbRobotehResources[0].children.find(c => c.id == 'strojni Monter');
        if (tmp) tmp.children.push({ id: 'user' + user.id, name: user.name + ' ' + user.surname, role: user.role , color: user.color });
        else newDbRobotehResources[0].children.push({ id: 'strojni Monter', name: 'Strojni monterji', collapsed: false, img: '/mechanic64.png', children: [{ id: 'user' + user.id, name: user.name + ' ' + user.surname, role: user.role , color: user.color }] });
      }
      else if (user.role.toLowerCase().includes('cnc operater')){
        let tmp = newDbRobotehResources[0].children.find(c => c.id == 'CNC Operater');
        if (tmp) tmp.children.push({ id: 'user' + user.id, name: user.name + ' ' + user.surname, role: user.role , color: user.color });
        else newDbRobotehResources[0].children.push({ id: 'CNCOperater', name: 'CNC operaterji', collapsed: false, img: '/cnc64.png', children: [{ id: 'user' + user.id, name: user.name + ' ' + user.surname, role: user.role , color: user.color }] });
      }
      else if (user.role.toLowerCase().includes('varil')){
        let tmp = newDbRobotehResources[0].children.find(c => c.id == 'varilec');
        if (tmp) tmp.children.push({ id: 'user' + user.id, name: user.name + ' ' + user.surname, role: user.role , color: user.color });
        else newDbRobotehResources[0].children.push({ id: 'varilec', name: 'Varilci', collapsed: false, img: '/welder01.png', children: [{ id: 'user' + user.id, name: user.name + ' ' + user.surname, role: user.role , color: user.color }] });
      }
      else if (user.role.toLowerCase().includes('tudent')){
        let tmp = newDbRobotehResources[0].children.find(c => c.id == 'student');
        if (tmp) tmp.children.push({ id: 'user' + user.id, name: user.name + ' ' + user.surname, role: user.role , color: user.color });
        else newDbRobotehResources[0].children.push({ id: 'student', name: 'Študenti', collapsed: false, img: '/student01.png', children: [{ id: 'user' + user.id, name: user.name + ' ' + user.surname, role: user.role , color: user.color }] });
      }
    }
  });
  // filtering resources - categories
  filterCategories = [];
  $('#filterTasksBody').find('.category-parent:checked').each(function() {
    // debugger;
    // store id of checked category in array of filtered categories
    filterCategories.push(this.id.substring(19));
  });
  // fix resources as new filtered resources
  calendar.setOptions({resources: newDbRobotehResources});
  // fix tasks as new filtered tasks
  getData(moment(calendar._firstDay).add(-1, 'days').format("YYYY-MM-DDTHH:mm"), moment(calendar._lastDay).add(1, 'days').format("YYYY-MM-DDTHH:mm"));
  // close modal
  $('#modalFilterResources').modal('toggle');
}
const rgb2hex = (rgb) => `#${rgb.match(/^rgb\((\d+),\s*(\d+),\s*(\d+)\)$/).slice(1).map(n => parseInt(n, 10).toString(16).padStart(2, '0')).join('')}`
var loggedUser;
var allUsers;
var activeUser = 1;
var users;
var events;
var reasons;
var formatDate;
var clickToCreateVar = true;
var dragToCreateVar = true;
var dragToMoveVar = true;
var dragToResizeVar = true;
var refreshTimerCount = 0;
var calendar,
    popup,
    range,
    oldEvent,
    tempEvent = {},
    tempEditEvent = {},
    deleteEvent,
    restoreEvent,
    colorPicker,
    tempColor,
    timer,
    myRange,
    rangeButton,
    firstDay,
    lastDay,
    selectRange,
    titleInput = document.getElementById('work-order-title'),
    locationInput = document.getElementById('work-order-location'),
    notesTextarea = document.getElementById('work-order-notes'),
    deleteButton = document.getElementById('work-order-delete'),
    unvalidButton = document.getElementById('work-order-unvalid'),
    allDaySwitch = document.getElementById('event-all-day'),
    colorSelect = document.getElementById('event-color-picker'),
    pickedColor = document.getElementById('event-color'),
    colorElms = document.querySelectorAll('.crud-color-c'),
    resourceCont = document.getElementById('work-order-resources'),
    filterResourcesBody = document.getElementById('filterResourcesBody'),
    filterTasksBody = document.getElementById('filterTasksBody'),
    tooltip = document.getElementById('custom-event-tooltip-popup');
    tooltipHeader = document.getElementById('tooltip-event-header'),
    tooltipData = document.getElementById('tooltip-event-name-age'),
    tooltipTime = document.getElementById('tooltip-event-time'),
    tooltipTitle = document.getElementById('tooltip-event-title'),
    tooltipNotes = document.getElementById('tooltip-event-notes'),
    tooltipUsers = document.getElementById('tooltip-event-users'),
    tooltipOwner = document.getElementById('tooltip-event-owner'),
    tooltipLoc = document.getElementById('tooltip-event-location')
    taskNameInput = document.getElementById('taskName'),
    taskStartInput = document.getElementById('taskStart'),
    taskFinishInput = document.getElementById('taskFinish'),
    taskCategoryInput = document.getElementById('taskCategory'),
    taskWorkersInput = document.getElementById('taskWorkers'),
    taskNoteInput = document.getElementById('taskNote'),
    taskProjectInput = document.getElementById('taskProject'),
    deleteTaskButton = document.getElementById('task-delete');
    deleteTooltipButton = document.getElementById('tooltip-event-delete');
    deleteAbsenceButton = document.getElementById('absence-delete');

var dbRobotehResources = [
  {
    id: 'employees',
    name: 'Zaposleni',
    collapsed: false,
    children: [
      { id: 'plc', name: 'Programerji PLC-jev', collapsed: false, img: '/plc64.png', children: [] },
      { id: 'robot', name: 'Programerji robotov', collapsed: false, img: '/robot64.png', children: [] },
      { id: 'serviser', name: 'Serviserji', collapsed: true, img: '/serviser.png', children: [] },
      { id: 'konstrukter', name: 'Konstrukterji', collapsed: true, img: '/builder64.png', children: [] },
      { id: 'strojnik', name: 'Strojniki', collapsed: true, img: '/mechanic64.png', children: [] },
      { id: 'električar', name: 'Električarji', collapsed: true, img: '/electrican64.png', children: [] },
      { id: 'vodja Projektov', name: 'Vodje projektov', collapsed: true, img: '/manager64.png', children: [] },
      { id: 'vodja Strojnikov', name: 'Vodje strojnikov', collapsed: true, img: '/managerMech64.png', children: [] },
      { id: 'vodja CNC Obdelave', name: 'Vodje CNC obdelave', collapsed: true, img: '/managerCNC64.png', children: [] },
      { id: 'vodja Električarjev', name: 'Vodja električarjev', collapsed: true, img: '/managerElec64.png', children: [] },
      { id: 'tajnik', name: 'Uprava', collapsed: true, img: '/secratery64.png', children: [] },
      { id: 'admin', name: 'Administratorji', collapsed: true, img: '/admin64.png', children: [] },
      { id: 'strojni Monter', name: 'Strojni monterji', collapsed: true, img: '/mechanic64.png', children: [] },
      { id: 'CNC operater', name: 'CNC operaterji', collapsed: true, img: '/cnc64.png', children: [] }, 
      { id: 'varilec', name: 'Varilci', collapsed: true, img: '/welder01.png', children: [] },
      { id: 'student', name: 'Študenti', collapsed: true, img: '/student01.png', children: [] },
      { id: 'ostalo', name: 'Ostalo', collapsed: true, img: '/worker64.png', children: [] },
    ]
  },
];
var sloLocal = {
  setText: "Potrdi",
  cancelText: "Prekliči",
  clearText: "Izbriši",
  closeText: "Zapri",
  selectedText: "{count} izbrano",
  dateFormat: "DD.MM.YYYY",
  dateFormatLong: "DDD, D. MMM. YYYY.",
  dateWheelFormat: "|DDD D MMM|",
  dayNames: ["Nedelja", "Ponedeljek", "Torek", "Sreda", "Četrtek", "Petek", "Sobota"],
  dayNamesShort: ["Ned", "Pon", "Tor", "Sre", "Čet", "Pet", "Sob"],
  dayNamesMin: ["Ne", "Po", "To", "Sr", "Če", "Pe", "So"],
  monthNames: ["Januar", "Februar", "Marec", "April", "Maj", "Junij", "Julij", "Avgust", "September", "Oktober", "November", "December"],
  monthNamesShort: ["Jan", "Feb", "Mar", "Apr", "Maj", "Jun", "Jul", "Avg", "Sep", "Okt", "Nov", "Dec"],
  timeFormat: "H:mm",
  nowText: "Zdaj",
  pmText: "pm",
  amText: "am",
  firstDay: 0,
  dateText: "Datum",
  timeText: "Čas",
  todayText: "Danes",
  prevMonthText: "Prejšnji mesec",
  nextMonthText: "Naslednji mesec",
  prevYearText: "Prejšnje leto",
  nextYearText: "Naslednje leto",
  eventText: "Dogodek",
  eventsText: "Dogodki",
  allDayText: "Ves dan",
  noEventsText: "Brez dogodkov",
  moreEventsText: "Še {count}",
  rangeStartLabel: "Začetek",
  rangeEndLabel: "Konec",
  rangeStartHelp: "Izberite",
  rangeEndHelp: "Izberite",
  filterEmptyText: "Brez rezultata",
  filterPlaceholderText: "Išči",
  weekText: '{count}. teden',
};
var absences;
var projectTasks;
var projects;
var categories;
var filterCategories = [1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17];
var allData;
var showProjectTasks = true;
var showEvents = true;
var showAbsences = true;
var showMyStuff = false;
var tmpDragEvent;
var tmpOldDragEvent;