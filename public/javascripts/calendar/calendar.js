$(function(){
  //$('[data-toggle="tooltip"]').tooltip()
  $('[data-toggle="tooltip"]').tooltip({trigger : 'hover'});
  $('[rel="tooltip"]').on('click', function () {
    $(this).tooltip('hide')
  })
})
function toggleSelectedDay(id){
  var selected = false;
  if($('#'+id).hasClass('calendar-day-selected'))
    selected = true;
  if(selected){
    $('#'+id).removeClass('calendar-day-selected');
    $('#'+id).addClass('border-dark');
  }
  else{
    $('#'+id).removeClass('border-dark');
    $('#'+id).addClass('calendar-day-selected');
  }
}
function toggleTask(e){
  console.log('toggle opravilo');
  //e.stopPropagation();
}
function toggleWorker(id){
  var selected = false;
  if($('#'+id).hasClass('active'))
    selected = true;
  if(selected){
    $('#'+id).removeClass('active');
  }
  else{
    $('#'+id).addClass('active');
  }
}
function addDays(date, days) {
  var result = new Date(date);
  //console.log(result);
  //console.log(date);
  var newResult = result.setDate(result.getDate() + (parseInt(days)-1));
  //console.log(newResult);
  var formattedResult = (result.getFullYear() + "-" + ("0"+(result.getMonth()+1)).slice(-2) + "-" + ("0" + result.getDate()).slice(-2));
  //console.log(formattedResult);
  return formattedResult;
}
function getMonday(d) {
  d = new Date(d);
  var day = d.getDay(),
      diff = d.getDate() - day + (day == 0 ? -6:1); // adjust when day is sunday
  return new Date(d.setDate(diff));
}
//var
var date;
var allTasks;
var allUsers;
var userRoles;
var workRoles;
var priorities;
var categories;