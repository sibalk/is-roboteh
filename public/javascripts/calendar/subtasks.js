//IN THIS SCRIPT//
//// adding note to subtask, adding/deleting subtask, subtask checkbox, filling collapse for subtask ////

$(function(){
  $('#btnOverrideSubtask').on('click', subtaskOverride);
  setTimeout(() => {
    if(loggedUser.role.substring(0,5) == 'vodja' || loggedUser.role.substring(0,4) == 'info' || loggedUser.role == 'tajnik' || loggedUser.role == 'komercialist' || loggedUser.role == 'komerciala' || loggedUser.role == 'računovodstvo' || loggedUser.role == 'admin'){
      $('#expiredList').on('change', 'input', function (event) {
        debugger
        if ($('#'+this.id).hasClass('subtask-check')){
          toggleSubtaskCheckbox(this);
        }
      })
    }
    else{
      $('#workersList').on('change', 'input', function (event) {
        debugger
        if ($('#'+this.id).hasClass('subtask-check')){
          toggleSubtaskCheckbox(this);
        }
      })
    }
  }, 200);
})
function toggleSubtaskCheckbox(element){
  var subtaskId = parseInt(element.id.match(/\d+/g)[0]);
  var completed = element.checked;
  var taskId = activeTaskId;
  var oldCompletion = tmpTask.completion;
  if((completed == true) && ($('#taskWorkorders').attr('data-content') == 'brez delovnega naloga') && (allSubtasks.filter(s => s.completed == false).length <= 1) && !tmpTask.id_project){
    //console.log('warning! no work order! add new work order to service');
    overrideSubtaskId = subtaskId;
    $('#customCheck'+subtaskId).prop('checked', false);
    $('#btnOverrideTask').hide();
    $('#btnOverrideSubtask').show();
    $('#modalWarningWO').modal('toggle');
  }
  else{
    $.post('/projects/subtask', {subtaskId, completed}, function(data){
      //debugger;
      //calculate completion via subtask
      var temp = allSubtasks.find(s => s.id == subtaskId)
      if(temp){
        //fix completion in allSubtasks
        temp.completed = completed;
        //calc task completion
        var numberOfCompleted = 0;
        for(var i=0; i<allSubtasks.length; i++){
          if(allSubtasks[i].completed)
          numberOfCompleted++;
        }
        var completion = Math.round((numberOfCompleted/allSubtasks.length)*100)
        if(numberOfCompleted == '0' || allSubtasks.length == '0')
          completion = 0;
        //console.log(completion);
        fixTaskCompletion(taskId, completion, oldCompletion);
        //debugger;
      }
      //ADDING NEW CHANGE TO DB
      if(completed){
        if(!tmpTask.id_project)
          addChangeSystem(4,10,null,taskId,subtaskId,null,null,null,tmpTask.id_subscriber);
        else
          addChangeSystem(4,3,projectId,taskId,subtaskId);
      }
      else{
        if(!tmpTask.id_project)
          addChangeSystem(5,10,null,taskId,subtaskId,null,null,null,tmpTask.id_subscriber);
        else
          addChangeSystem(5,3,projectId,taskId,subtaskId);
      }
    })
  }
}
function subtaskOverride(){
  var subtaskId = overrideSubtaskId;
  var completed = true;
  $('#customCheck'+subtaskId).prop('checked', true);
  var taskId = activeTaskId;
  var oldCompletion = tmpTask.completion;
  $.post('/projects/subtask', {subtaskId, completed}, function(data){
    $('#modalWarningWO').modal('toggle');
    //debugger;
    //calculate completion via subtask
    var temp = allSubtasks.find(s => s.id == subtaskId)
    if(temp){
      //fix completion in allSubtasks
      temp.completed = completed;
      //calc task completion
      var numberOfCompleted = 0;
      for(var i=0; i<allSubtasks.length; i++){
        if(allSubtasks[i].completed)
        numberOfCompleted++;
      }
      var completion = Math.round((numberOfCompleted/allSubtasks.length)*100)
      if(numberOfCompleted == '0' || allSubtasks.length == '0')
        completion = 0;
      //console.log(completion);
      fixTaskCompletion(taskId, completion, oldCompletion);
      //debugger;
    }
    //ADDING NEW CHANGE TO DB
    if(completed){
      if(!tmpTask.id_project)
        addChangeSystem(4,10,null,taskId,subtaskId,null,null,null,tmpTask.id_subscriber);
      else
        addChangeSystem(4,3,projectId,taskId,subtaskId);
    }
  })
}
var overrideSubtaskId;