//IN THIS SCRIPT//
//// adding note to task, checbox of task, fixing percentage of task/project, adding/editing/deleting task, showing conflicts, form control for tasks ////
$(function(){
  $('#btnOverrideTask').on('click', taskOverride);
  if(loggedUser.role.substring(0,5) == 'vodja' || loggedUser.role.substring(0,4) == 'info' || loggedUser.role == 'tajnik' || loggedUser.role == 'komercialist' || loggedUser.role == 'komerciala' || loggedUser.role == 'računovodstvo' || loggedUser.role == 'admin'){
    $('#expiredList').on('change', 'input', function (event) {
      debugger
      if ($('#'+this.id).hasClass('task-check')){
        toggleTaskCheckbox(this);
      }
    })
  }
  else{
    $('#workersList').on('change', 'input', function (event) {
      debugger
      if ($('#'+this.id).hasClass('task-check')){
        toggleTaskCheckbox(this);
      }
    })
  }
})
function toggleTaskCheckbox(element){
  var taskId = parseInt(element.id.match(/\d+/g)[0]);
  debugger;
  var completion = element.checked ? 100 : 0;
  var oldCompletion = element.checked ? 0 : 100;
  if((element.checked == true) && ($('#taskWorkorders').attr('data-content') == 'brez delovnega naloga' && !tmpTask.id_project)){
    overrideTaskId = taskId;
    $('#customTaskCheck'+taskId).prop('checked', false);
    $('#btnOverrideTask').show();
    $('#btnOverrideSubtask').hide();
    $('#modalWarningWO').modal('toggle');
  }
  else
    fixTaskCompletion(taskId, completion, oldCompletion);
  debugger;
}
function taskOverride(){
  var taskId = overrideTaskId;
  debugger;
  $('#customTaskCheck'+taskId).prop('checked', true);
  $('#modalWarningWO').modal('toggle');
  var completion = 100, oldCompletion = 0;
  fixTaskCompletion(taskId, completion,oldCompletion);
  debugger;
}
//FIX COMPLETION OF THE PROJECT, task percentage has changed
function fixCompletion(id){
  var projectId = id;
  $.post('/projects/completion', {projectId}, function(resp){
    if(resp.success == true)
      console.log("Successfully updated project percentage." );
    else if(resp.success == false)
      console.log("Unsuccessfully updated project percentage, there was no projectId" );
  })
}
//FIX Task percentage
function fixTaskCompletion(taskId, completion, oldCompletion){
  $.post('/projects/task/completion', {taskId, completion}, function(resp){
    //fix subtask count
    if(activeTasks){
      var task = activeTasks.find(t=>t.id == taskId);
      task.completion = completion;
    }
    //fix task display
    $('#taskProgress'+taskId).width(completion+"%");
    $('#taskDate'+taskId).removeClass('text-danger');
    var date = $('#taskDate'+taskId).html();
    if(date){
      date = date.split(' - ');
      if(date[1] != '/'){
        date = date[1].split('.');
        //var d = date[0]; var m = date[1]; var y = date[2];
        if(new Date() > new Date(date[2],date[1],date[0],'15') && completion < 100)
          $('#taskDate'+taskId).addClass('text-danger');
      }
    }
    //fix procentage of this project overall (so it can be correct value in list of all projects)
    //check if project task or service and add new change to system changes
    if(completion == 100 && oldCompletion != completion){
      if(!tmpTask.id_project)
        addChangeSystem(4,9,null,taskId,null,null,null,null,tmpTask.id_subscriber);
      else
        addChangeSystem(4,2,projectId,taskId);
    }
    else{
      if(oldCompletion == 100){
        if(!tmpTask.id_project)
          addChangeSystem(5,9,null,taskId,null,null,null,null,tmpTask.id_subscriber);
        else
          addChangeSystem(5,2,projectId,taskId);
      }
    }
    if(tmpTask.id_project)
      fixCompletion(selectedProject);
  })
}
//ADD FINISHED DATE WHEN TASK WAS GIVEN 100 COMPLETION
function addFinishedDate(taskId){
  $.post('task/finished', {taskId}, function(resp){
    debugger;
  })
}

function isValidDate(d) {
  return d instanceof Date && !isNaN(d);
}
//$('#taskStartAdd').on('change', onStartAddChange);
//$('#taskFinishAdd').on('change', onFinishAddChange);
var overrideTaskId;