$(function(){
  projectId = selectedProject;
  taskId = selectedTask;
  activeTask = taskId;
  $('#dateInput').on('change', changeDate);
  $('#projectSelect').on('change', changeDate);
  $('#customSwitch1').on('change', switcher);
  $('#weekCount').on('change', counter);
  $('footer').hide();
  loggedUser = {name:$('#loggedUser').html().split(" (")[0].split(" ")[0], surname:$('#loggedUser').html().split(" (")[0].split(" ")[1], role:$('#loggedUser').html().split(" (")[1].split(")")[0].toLowerCase()};
  if(loggedUser.role == 'info'){
    $('#notificationBar').hide();
    $('#dateInput').hide();
    $('#dateInputLabel').hide();
  }
  //get all users
  $.get( "/employees/all", {activeUser}, function( data ) {
    //console.log(data)
    allUsers = data.data;
  });
  //get all projects active
  $.get( "/reports/projects", {activeProject}, function( data ) {
    //console.log(data)
    allProjects = data.data;
    allProjects.unshift({id:0,text:'Vsi projekti'});
    $(".select2-projects").select2({
      data: allProjects,
      tags: true,
    });
  });
  //date = $('#dateInput').val();
  //date = '2019-04-17';
  date = new Date();
  $('#dateInput').val(convertDateFormat(date));
  //date = new Date();
  $.get( "/calendar/tasks", {date, selectedProject, seeAll, weekCount, category}, function( data ) {
    //console.log(data)
    activeTasks = data.activeTasks;
    expiredTasks = data.expiredTasks;
    drawCalendar();
    //drawTasks();
    setTimeout(()=>{
      drawProjects();
      fillExpired();
    },200)
  });
  //add on event call functions
  $('#prevWeekBtn').on('click', prevWeek);
  $('#nextWeekBtn').on('click', nextWeek);
  $('#fullScreenBtn').on('click', toggleFullScreen);
  $('#optionWeek').on('click', function (event) {
    changeView(0);
  });
  $('#optionMonth').on('click', function (event) {
    changeView(1);
  });
  $('#resetInputBtn').on('click', resetInput);
  $('#workersList').on('click', 'li', function(event) {
    //debugger
    toggleWorker(this.id.substring(6));
  })
  // add on event call functions
  // if(loggedUser.role.substring(0,5) == 'vodja' || loggedUser.role.substring(0,4) == 'info' || loggedUser.role == 'tajnik' || loggedUser.role == 'admin'){
  //   $('#expiredList').on('change', 'input', function (event) {
  //     debugger
  //     if ($('#'+this.id).hasClass('task-check')){
  //       toggleTaskCheckbox(this);
  //     }
  //     if ($('#'+this.id).hasClass('subtask-check')){
  //       toggleSubtaskCheckbox(this);
  //     }
  //   })
  // }
  // else{
  //   $('#workersList').on('change', 'input', function (event) {
  //     debugger
  //     if ($('#'+this.id).hasClass('task-check')){
  //       toggleTaskCheckbox(this);
  //     }
  //     if ($('#'+this.id).hasClass('subtask-check')){
  //       toggleSubtaskCheckbox(this);
  //     }
  //   })
  // }
})
function toggleSelectedDay(id){
  var selected = false;
  if($('#'+id).hasClass('calendar-day-selected'))
    selected = true;
  if(selected){
    $('#'+id).removeClass('calendar-day-selected');
    $('#'+id).addClass('border-dark');
  }
  else{
    $('#'+id).removeClass('border-dark');
    $('#'+id).addClass('calendar-day-selected');
  }
}
function toggleTask(e){
  //console.log('toggle opravilo');
  //e.stopPropagation();
}
function toggleWorker(id){
  if(selectedWorker != 0){
    $('#worker'+selectedWorker).removeClass('active');
  }
  if(selectedWorker == id)
    id = 0;
  selectedWorker = id;
  $('#worker'+id).addClass('active');
  selectedProject = 0;
  $('#projectSelect').val(0).trigger('change');
  /*
  drawCalendar();
  setTimeout(()=>{
    if(selectedProject == 0)
      drawProjects();
    else
      drawTasks();
  },200);
  if(selectedTask == 0);
    fillExpired();
  */
}
function addDays(date, days) {
  var result = new Date(date);
  //console.log(result);
  //console.log(date);
  var newResult = result.setDate(result.getDate() + (parseInt(days)-1));
  //console.log(newResult);
  var formattedResult = (result.getFullYear() + "-" + ("0"+(result.getMonth()+1)).slice(-2) + "-" + ("0" + result.getDate()).slice(-2));
  //console.log(formattedResult);
  return formattedResult;
}
function getMonday(d) {
  d = new Date(d);
  var day = d.getDay(),
      diff = d.getDate() - day + (day == 0 ? -6:1); // adjust when day is sunday
  return new Date(d.setDate(diff));
}
//
function fillExpired(){
  //$( "#scrollList" ).stop();
  $('#expiredList').empty();
  for(var i = 0; i < expiredTasks.length; i++){
    var projectName;
    if(expiredTasks[i].project) projectName = expiredTasks[i].project_number+' - '+expiredTasks[i].project; else projectName = expiredTasks[i].subscriber;
    var start = '', finish = '';
    if(expiredTasks[i].task_start) start = new Date(expiredTasks[i].task_start).toLocaleDateString('sl');
    if(expiredTasks[i].task_finish) finish = new Date(expiredTasks[i].task_finish).toLocaleDateString('sl');
    var expiredCategory = '';
    if(expiredTasks[i].category == 'Nabava') expiredCategory = '<span class="badge badge-purchase ml-2">Nabava</span>';
    else if(expiredTasks[i].category == 'Konstrukcija') expiredCategory = '<span class="badge badge-construction ml-2">Konstrukcija</span>';
    else if(expiredTasks[i].category == 'Strojna izdelava') expiredCategory = '<span class="badge badge-mechanic ml-2">Strojna izdelava</span>';
    else if(expiredTasks[i].category == 'Elektro izdelava') expiredCategory = '<span class="badge badge-electrican ml-2">Elektro izdelava</span>';
    else if(expiredTasks[i].category == 'Programiranje') expiredCategory = '<span class="badge badge-programmer ml-2">Programiranje</span>';
    else if(expiredTasks[i].category == 'Montaža') expiredCategory = '<span class="badge badge-assembly ml-2">Montaža</span>';
    if(expiredTasks[i].subscriber) expiredCategory = '<span class="badge badge-info ml-2">Servis</span>';
    var element = `<li class="list-group-item flex-column" id="expiredTask`+expiredTasks[i].id+`">
      <div class="d-flex w-100 justify-content-between">
        <div class="h5 mb-1 font-italic">`+projectName+`</div>
      </div>
      <div class="d-flex w-100 justify-content-between">
        <div class="h5 mb-1">`+expiredTasks[i].task_name+`</div>
      </div>
      <div class="d-flex w-100 justify-content-start">
        <div class="h6 mb-1 mr-2">`+start+` - `+finish+`</div>
      </div>
      <div class="d-flex justify-content-start flex-wrap">
        <div class="mb-1 mr-2">`+expiredTasks[i].workers+`</div>
      </div>
      <div class="d-flex w-100 justify-content-start">`+expiredCategory+`</div>
    </li>`;
    if(expiredTasks[i].workers_id)
      var search = expiredTasks[i].workers_id.split(',').find(id => id == selectedWorker);
    if(search || selectedWorker == 0){
      //console.log('drawing task of worker')
      if((expiredTasks[i].category == 'Konstrukcija' || expiredTasks[i].category == 'Nabava') && loggedUser.role == 'info'){
        //console.log('skipping expired task with category Konstrukcija and Nabava for info role');
      }
      else
        $('#expiredList').append(element);
    }
  }
  hiddenScroll = $('#scrollList').prop('scrollHeight') - $('#scrollList').height() + 15;
  //animateList();
}
function onEventDrawTask(event) {
  drawTask(event.data.id);
}
//draw chosen task info inside expired list
function drawTask(id){
  selectedTask = id;
  activeTaskId = id;
  taskId = id;
  tmpTask = activeTasks.find(t => t.id == taskId);
  //console.log(tmpTask);
  if(loggedUser.role.substring(0,5) == 'vodja' || loggedUser.role.substring(0,4) == 'info' || loggedUser.role == 'tajnik' || loggedUser.role == 'komercialist' || loggedUser.role == 'komerciala' || loggedUser.role == 'računovodstvo' || loggedUser.role == 'admin')
    $('#expiredList').empty();
  else
    $('#workersList').empty();
  $.get('/projects/subtasks', {taskId}, function(data){
    allSubtasks = data.data;
    var hasSubtasks = false;
    if(allSubtasks.length > 0){
      hasSubtasks = true;
    }
    //var id = taskId;
    var checkboxElement = '';
    var noteElement = '';
    //priority
    var priorityClass = '';
    if(tmpTask.priority == 'visoka')
      priorityClass = 'border-high-priority';
    else if(tmpTask.priority == 'srednja')
      priorityClass = 'border-medium-priority';
    else if(tmpTask.priority == 'nizka')
      priorityClass = 'border-low-priority';
    //note
    var noteElement = `<button class="invisible-button ml-2"><i class="fas fa-comments fa-lg"></i></button>`;
    //checkbox
    var isChecked = '';
    if(!hasSubtasks){
      if(tmpTask.completion == 100)
        isChecked = 'checked = ""';
      checkboxElement = `<input class="custom-control-input task-check" id="customTaskCheck`+id+`" type="checkbox" `+isChecked+` />
      <label class="custom-control-label" for="customTaskCheck`+id+`"> </label>`;
    }
    //workers
    var workers = "";
    if(tmpTask.workers)
      workers = tmpTask.workers;
    //date
    var date = '';
    if(tmpTask.formatted_start && tmpTask.formatted_finish)
      date = tmpTask.formatted_start.date + " - " + tmpTask.formatted_finish.date;
    else if(tmpTask.formatted_start)
      date = tmpTask.formatted_start.date + " - /";
    else if(tmpTask.formatted_finish)
      date = "/ - " + tmpTask.formatted_finish.date;
    //red date text (task should be finished by now)
    var todayDate = new Date();
    var tmpDate = new Date(tmpTask.task_finish);
    var dateText = '';
    if(todayDate > tmpDate && tmpTask.completion < 100)
      dateText = 'text-danger';
    //category
    var category = tmpTask.category.toUpperCase();
    var categoryClass = ''; 
    if(category == 'NABAVA')
      categoryClass = 'badge-purchase';
    else if(category == 'KONSTRUKCIJA')
      categoryClass = 'badge-construction';
    else if(category == 'STROJNA IZDELAVA')
      categoryClass = 'badge-mechanic';
    else if(category == 'ELEKTRO IZDELAVA')
      categoryClass = 'badge-electrican';
    else if(category == 'MONTAŽA')
      categoryClass = 'badge-assembly';
    else if(category == 'PROGRAMIRANJE')
      categoryClass = 'badge-programmer';
    else if(category == 'BREZ')
      category = '';
    var workorderElem = '';
    if(!tmpTask.id_project){
      //workorders
      var workorders = "brez delovnega naloga";
      var workorderColor = ' text-danger';
      if(tmpTask.workorders_id){
        workorders = tmpTask.workorders;
        workorderColor = ' text-success';
      }
      workorderElem = `<a class="p-1 ml-auto" data-toggle="popover" data-trigger="hover" tabindex="0" data-content="`+workorders+`" id="taskWorkorders" href="/workorders" data-original-title="" title="">
        <i class="icon-dn fa-lg`+workorderColor+`"></i>
      </a>`;  
    }
    //put everything together
    var element = `<div class="list-group-item `+priorityClass+`" id="task`+id+`">
      <div class="row">
        <div class="col-md-12">
          <div id="taskName`+id+`">`+tmpTask.task_name+`</div>
        </div>
        <div class="col-md-12 d-flex">
          <div class="progress w-100">
            <div class="progress-bar w-` + tmpTask.completion + `" id="taskProgress`+id+`"></div>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-md-12 d-flex">
          <div class="`+dateText+`" id="taskDate`+id+`">`+date+`</div>
          `+workorderElem+`
        </div>
        <div class="col-md-12 d-flex">
          <div class="mr-auto" id="taskBadges`+id+`">
            <span class="badge badge-pill `+categoryClass+` ml-2">`+category+`</span>
          </div>
          <div class="custom-control custom-checkbox table-custom-checkbox ml-auto" id="taskCheckbox`+id+`">
            `+checkboxElement+`
          </div>
          `+noteElement+`
          <div class="ml-2" id="taskInfo`+id+`">
            <a class="workerspopover" data-toggle="popover" data-trigger="hover" data-content="`+workers+`" data-original-title="" title="">
              <i class="fas fa-user-friends fa-lg text-dark"></i>
            </a>
          </div>
        </div>
      </div>
    </div>
    <div class="list-group d-flex" id="subtaskList`+id+`"></div>
    </div>`;
    //debugger;
    if(loggedUser.role.substring(0,5) == 'vodja' || loggedUser.role.substring(0,4) == 'info' || loggedUser.role == 'tajnik' || loggedUser.role == 'komercialist' || loggedUser.role == 'komerciala' || loggedUser.role == 'računovodstvo' || loggedUser.role == 'admin'){
      $('#expiredList').append(element);
      $('#task'+id).find('.invisible-button').on('click', {id, type: 1}, onClickOpenMessagesModal);
      // $('#customTaskCheck'+id).on('change', function (event) {
      //   toggleTaskCheckbox(this);
      // });
    }
    else{
      $('#workersList').append(element);
      // $('#customTaskCheck'+id).on('change', function (event) {
      //   toggleTaskCheckbox(this);
      // });
    }
    for(var i = 0; i < allSubtasks.length; i++){
      var checkedCB = '';
      if(allSubtasks[i].completed)
        checkedCB = 'checked=""';
      var listElement = `<div class="list-group-item" id="subtask`+allSubtasks[i].id+`">
        <div class="row">
          <div class='col-8 col-xl-8 col-lg-8 col-md-8'>
            <label class="p-1 mr-auto w-100" id="subtaskName`+allSubtasks[i].id+`">`+allSubtasks[i].name+`</label>
          </div>
          <div class='col-4 col-xl-4 col-lg-4 col-md-4 d-flex'>
            <div class="custom-control custom-checkbox ml-auto mt-2">
              <input class="custom-control-input subtask-check" id="customCheck`+allSubtasks[i].id+`" type="checkbox" `+checkedCB+` />
              <label class="custom-control-label" for="customCheck`+allSubtasks[i].id+`"> </label>
            </div>
            <button class="invisible-button mt-2"><i class="fas fa-comments fa-lg"></i></button>
          </div>
        </div>
      </div>`;
      if(allSubtasks[i].active == true){
        $('#subtaskList'+taskId).append(listElement);
        $('#subtask'+allSubtasks[i].id).find('.invisible-button').on('click', {id: allSubtasks[i].id, type: 2}, onClickOpenMessagesModal);
      }
    }
    $('[data-toggle="popover"]').popover({trigger: "hover"});
  })
}
//function to call to draw a task
function drawTasks(){
  selectedTask = 0;
  //projectList = [];
  servisPresent = false;
  for(var i = 0; i < activeTasks.length; i++){
    var index;
    var duration = countDays(activeTasks[i].task_start, activeTasks[i].task_finish);
    var workingTaskDay = new Date(activeTasks[i].task_start);
    //draw task only if task have both start and finish
    if(activeTasks[i].task_start && activeTasks[i].task_finish){
      var categoryBorder = 'primary';
      if(activeTasks[i].category == 'Nabava') categoryBorder = 'purchase';
      else if(activeTasks[i].category == 'Konstrukcija') categoryBorder = 'construction';
      else if(activeTasks[i].category == 'Strojna izdelava') categoryBorder = 'mechanic';
      else if(activeTasks[i].category == 'Elektro izdelava') categoryBorder = 'electric';
      else if(activeTasks[i].category == 'Montaža') categoryBorder = 'assembly';
      else if(activeTasks[i].category == 'Programiranje') categoryBorder = 'programming';
      else if(activeTasks[i].category == 'Brez') categoryBorder = 'dark';
      var taskName = activeTasks[i].task_name;
      var taskWorkers = '';
      if(activeTasks[i].workers) taskWorkers = activeTasks[i].workers;
      var taskNote = '';
      if(activeTasks[i].subscriber){
        taskNote = activeTasks[i].subscriber;
        servisPresent = true;
      } 
      //if(i==0) taskNote = 'Preizkus opombe!';
      var collapseName = 'task-'+activeTasks[i].id;
      var tmp = projectList.find(p => p.id == activeTasks[i].id_project);
      if(activeTasks[i].id_project){
        if(tmp){
          //get index and fix border color
          index = projectList.findIndex(p => p.id == tmp.id);
          //console.log('found index: '+index);
        }
        else{
          //add project id, name to the list
          index = projectList.length;
          //console.log('new index: '+index);
          projectList.push({id:activeTasks[i].id_project, name: activeTasks[i].project, number: activeTasks[i].project_number});
        }
      }
      var noteElement = '';
      if(selectedProject == 0){
        categoryBorder = 'project'+index;
        if(activeTasks[i].subscriber){
          categoryBorder = 'dark';
          noteElement = `<h6 class="card-subtitle mb-1 text-muted">`+taskNote+`</h6>`;
        }
      }
      var taskElement = `<div class="card border-`+categoryBorder+` mb-1 w-100">
      <div class="card-header p-1"><a class="collapsed d-block calendar-text" id="heading-collapsed" href="#`+collapseName+`" aria-expanded="true" aria-controls="`+collapseName+`">`+taskName+`</a></div>
      <div class="collapse" id="`+collapseName+`" aria-labelledby="heading-collapsed">
      <div class="card-body p-1">
      <p class="card-text">`+taskWorkers+`</p>
      `+noteElement+`
      </div>
      </div>
      </div>`;
      //console.log('--------------NOVO OPRAVILO----------')
      var search = '';
      if(activeTasks[i].workers_id)
      search = activeTasks[i].workers_id.split(',').find(id => id == selectedWorker);
      if(search || selectedWorker == 0){
        //console.log('drawing task of worker')
        
        if((activeTasks[i].category == 'Konstrukcija' || activeTasks[i].category == 'Nabava') && loggedUser.role == 'info'){
          //console.log('not drawing task with Konstrukcija and Nabava category for info role');
        }
        else{
          //add project to projectList if not in it else get index of and color task based on project index
          for(var j = 0; j < duration; j++){
            if((workingTaskDay.getDay() == 6 || workingTaskDay.getDay() == 0) && activeTasks[i].weekend == false){
              //ne rišem
              //debugger;
            }
            else{
              var dateName = workingTaskDay.getFullYear()+'-'+(workingTaskDay.getMonth()+1)+'-'+workingTaskDay.getDate();
              //console.log('drawing task '+taskName+' on day: '+dateName);
              $('#body-'+dateName).append(taskElement);
              $('#body-'+dateName).find('.card').last().on('click', {id:activeTasks[i].id}, onEventDrawTask);
            }
            workingTaskDay = new Date(workingTaskDay.setDate(workingTaskDay.getDate()+1))
          }
        }
      }

    }
    else{
      //console.log('----OPRAVILO NIMA STARTA ALI FINISHA-----')
    }
  }
  drawLegend();
  //if(selectedProject == 0)
  //else
  //  $('#legend').empty();
}
function drawProjects() {
  //selectedProject == 0
  selectedTask = 0;
  servisPresent = false;
  for(var i = 0; i < activeTasks.length; i++){
    var index;
    var duration = countDays(activeTasks[i].task_start, activeTasks[i].task_finish);
    var workingTaskDay = new Date(activeTasks[i].task_start);
    //draw task only if task have both start and finish
    if(activeTasks[i].task_start && activeTasks[i].task_finish){
      var collapseName = 'project-'+activeTasks[i].id_project;
      var tmp = projectList.find(p => p.id == activeTasks[i].id_project);
      if(activeTasks[i].id_project){
        if(tmp){
          //get index and fix border color
          index = projectList.findIndex(p => p.id == tmp.id);
          //console.log('found index: '+index);
        }
        else{
          //add project id, name to the list
          index = projectList.length;
          //console.log('new index: '+index);
          projectList.push({id:activeTasks[i].id_project, name: activeTasks[i].project, number: activeTasks[i].project_number});
        }
      }
      //prepare project element and task part for body
      var categoryBorder = 'project'+index;
      var taskName = activeTasks[i].task_name;
      var projectName = activeTasks[i].project_number+' - '+activeTasks[i].project;
      var taskElement = `<p class="card-text mb-1">`+taskName+`</p>`;
      var search = '';
      if(activeTasks[i].workers_id)
      search = activeTasks[i].workers_id.split(',').find(id => id == selectedWorker);
      if(search || selectedWorker == 0){
        //console.log('drawing task of worker')
        if((activeTasks[i].category == 'Konstrukcija' || activeTasks[i].category == 'Nabava') && loggedUser.role == 'info'){
          //console.log('not drawing task with Konstrukcija and Nabava category for info role');
        }
        else{
          //add project to projectList if not in it else get index of and color task based on project index
          for(var j = 0; j < duration; j++){
            var dateName = workingTaskDay.getFullYear()+'-'+(workingTaskDay.getMonth()+1)+'-'+workingTaskDay.getDate();
            //console.log('drawing task '+taskName+' on day: '+dateName);
            //need to check if project on that day already exist
            //if exist append only task else append first project element and then append task element 
            //first check if task is servis or is project task
            if((workingTaskDay.getDay() == 6 || workingTaskDay.getDay() == 0) && activeTasks[i].weekend == false){
              //ne rišem
              //debugger;
            }
            else{
              //riši
              //debugger;
              if(activeTasks[i].subscriber){
                var taskNote = '';
                taskNote = activeTasks[i].subscriber;
                categoryBorder = 'dark';
                noteElement = `<h6 class="card-subtitle mb-1 text-muted">`+taskNote+`</h6>`;
                var taskWorkers = '';
                if(activeTasks[i].workers) taskWorkers = activeTasks[i].workers;
                servisPresent = true;
                collapseName = 'task-'+activeTasks[i].id;
                var taskElement = `<div class="card badge-`+categoryBorder+` mb-1 w-100">
                <div class="card-header p-1"><a class="collapsed d-block calendar-text" id="heading-collapsed" href="#`+collapseName+`" aria-expanded="true" aria-controls="`+collapseName+`">`+taskName+`</a></div>
                <div class="collapse" id="`+collapseName+`" aria-labelledby="heading-collapsed">
                <div class="card-body p-1">
                <p class="card-text">`+taskWorkers+`</p>
                `+noteElement+`
                </div>
                </div>
                </div>`;
                $('#body-'+dateName).append(taskElement);
                $('#body-'+dateName).find('.card').last().on('click', {id:activeTasks[i].id}, onEventDrawTask);
              }
              else{
                var projectBodyName = `project`+activeTasks[i].id_project+'-body-'+dateName; 
                if($('#'+projectBodyName).length > 0){
                  //project element exist, add only taskElement to the project body element
                  $('#'+projectBodyName).append(taskElement);
                }
                else{
                  //project element does not exist, add projectElement and then taskElement
                  var projectElement = `<div class="card badge-`+categoryBorder+` mb-1 w-100">
                  <div class="card-header p-1"><a class="collapsed d-block calendar-text" id="heading-collapsed" data-toggle="collapse" href="#`+collapseName+`" aria-expanded="true" aria-controls="`+collapseName+`">`+projectName+`</a></div>
                  <div class="collapse" id="`+collapseName+`" aria-labelledby="heading-collapsed">
                  <div class="card-body p-1" id="`+projectBodyName+`">
                  `+taskElement+`
                  </div>
                  </div>
                  </div>`;
                  $('#body-'+dateName).append(projectElement);
                  $('#body-'+dateName).find('.card').last().on('click', {id:activeTasks[i].id_project}, onEventChangeProject);
                }
              }
            }
            workingTaskDay = new Date(workingTaskDay.setDate(workingTaskDay.getDate()+1))
          }
        }
      }
    }
    else{
      //console.log('----OPRAVILO NIMA STARTA ALI FINISHA-----')
    }
  }
  drawLegend();
}
function drawCalendar(){
  var element = `<div class="col calendar-day-overview card border-dark p-0" id="day-2019-12-16">
    <div class="card-header w-100">16. 12.</div>
    <div class="card-body p-1 hidden-scroll" id="body-2019-12-16"></div>
  </div>`;
  $('#calendar').empty();
  var weekElements = `<div class="row" id="week1"></div>`;
  for(var i = 2; i <= weekCount; i++){
    weekElements += `<div class="row" id="week`+i+`"></div>`;
  }
  $('#calendar').append(weekElements);
  var workingDay = getMonday(new Date(date));
  //console.log('wd = '+workingDay);
  var count_day = 0;
  var cardHeight = 'calendar-day-overview';
  if(weekCount == 1)
    cardHeight = 'calendar-body';
  if(view == 1)
    cardHeight = 'calendar-day';
  for(var j = 1; j <= weekCount; j++){
    for(var i = 0; i < 7; i++){
      var cardBorder = 'border-dark';
      if(sameDay(workingDay,new Date())) cardBorder = 'calendar-day-selected';
      var cardStyle = '';
      if(i == 5 || i == 6) cardStyle = 'calendar-day-weekend';
      var dateName = workingDay.getFullYear()+'-'+(workingDay.getMonth()+1)+'-'+workingDay.getDate();
      var dateLabel = new Date(workingDay).toLocaleDateString('sl');
      if(i == 0)
        firstDate = dateLabel;
      if(i == 6)
        lastDate = dateLabel;
      var element = `<div class="col `+cardHeight+` card `+cardBorder+` p-0 `+cardStyle+`" id="day-`+dateName+`">
        <div class="card-header p-0 text-center w-100">`+dateLabel+`</div>
        <div class="card-body p-1 hide-scrollbar hidden-scroll" id="body-`+dateName+`"></div>
      </div>`;
      $('#week'+j).append(element);
      workingDay = new Date(workingDay.setDate(workingDay.getDate()+1))
    }
  }
  //update date label
  if(view == 0)
    $('#dateLabel').html(firstDate + ' - ' + lastDate);
  else{
    var d = new Date(date);
    $('#dateLabel').html(monthNames[d.getMonth()] + ' ' + d.getFullYear());
  }
}
function drawLegend(){
  $('#legend').empty();
  var switchButton = `<button id="legendBtn" class="mr-2 badge badge-primary ml-2"><i class="fas fa-exchange-alt fa-lg"></i></button>`;
  $('#legend').append(switchButton);
  $('#legendBtn').on('click', switchLegend);
  if(!showCategoryLegend){
    category = null;
    for(var i = 0; i < projectList.length; i++){
      var linkStart = '';
      var linkEnd = '';
      if(loggedUser.role == 'admin' || loggedUser.role.substring(0,5) == 'vodja' || loggedUser.role == 'tajnik' || loggedUser.role == 'komercialist' || loggedUser.role == 'komerciala' || loggedUser.role == 'računovodstvo'){
        //linkStart = `<a href="/projects/id?id=`+projectList[i].id+`" class="alert-link" style='text-decoration:none;color:#fff'>`;
        //linkEnd = '</a>';
      }
      var badgeColor = 'project'+i;
      if(i > 10)
        badgeColor = 'dark';
      var legendElement = `<div class="mr-2 list-group-item-linkable" id="project`+i+`"><span class="badge badge-`+badgeColor+`">`+projectList[i].number+' - '+projectList[i].name+`</span></div>`;
      $('#legend').append(legendElement);
      $('#legend').find('div').last().on('click', {id:projectList[i].id}, onEventChangeProject);
    }
    if(servisPresent){
      var legendElement = `<div class="mr-2 list-group-item-linkable" id="servisBadge"><span class="badge badge-dark">servis</span></div>`;
        $('#legend').append(legendElement);
    }
  }
  else{
    //not in use, show projects all the time
    for(var i = 0; i < 7; i++){
      var badgeCategory = 'dark';
      var badgeName = 'Brez';
      var categoryNumber = null;
      if(i == 0){
        badgeCategory = 'purchase';
        badgeName = 'Nabava';
        categoryNumber = 2;
      } 
      else if(i == 1){
        badgeCategory = 'construction';
        badgeName = 'Konstrukcija';
        categoryNumber = 3;
      } 
      else if(i == 2){
        badgeCategory = 'mechanic';
        badgeName = 'Strojna izdelava';
        categoryNumber = 4;
      } 
      else if(i == 3){
        badgeCategory = 'electrican';
        badgeName = 'Elektro izdelava';
        categoryNumber = 5;
      } 
      else if(i == 4){
        badgeCategory = 'assembly';
        badgeName = 'Montaža';
        categoryNumber = 6;
      } 
      else if(i == 5){
        badgeCategory = 'programmer';
        badgeName = 'Programiranje';
        categoryNumber = 7;
      }
      else if(i == 6){
        badgeCategory = 'dark';
        badgeName = 'Brez';
        categoryNumber = 1;
      }
      var legendElement = `<div class="mr-2 list-group-item-linkable" id="project`+i+`"><span class="badge badge-`+badgeCategory+`">`+badgeName+`</span></div>`;
      $('#legend').append(legendElement);
      $('#legend').find('div').last().on('click', {categoryNumber}, onEventChangeCategory);
    }
  }
}
function switchLegend(){
  showCategoryLegend = (showCategoryLegend) ? false : true;
  drawLegend();
}
function onEventChangeCategory(event) {
  changeCategory(event.data.categoryNumber);
}
function changeCategory(categoryNumber){
  category = categoryNumber;
  refreshDataOnPage();
}
function countDays(start, finish){
  var oneDay = 24*60*60*1000;
  var firstDay = new Date(start);
  var secondDay = new Date(finish);
  var diffDays = Math.round(Math.abs((firstDay.getTime() - secondDay.getTime()) / (oneDay)))+1;
  return diffDays;
}
function onEventChangeProject(event) {
  changeProject(event.data.id);
}
function changeProject(id){
  projectId = id;
  if(selectedProject == id)
    $('#projectSelect').val(0).trigger('change');
  else
    $('#projectSelect').val(id).trigger('change');
}
function resetInput(){
  //date = '2019-04-17';
  category = null;
  showCategoryLegend = false;
  projectList = [];
  $('#customSwitch1').unbind();
  //$('#weekCount').unbind();
  $('#weekCount').val(2);
  weekCount = 2;
  if(view == 1){
    $('#optionMonth').removeClass('active');
    $('#optionWeek').addClass('active');
  }
  view = 0;
  $('#customSwitch1').prop("checked", true).change();
  seeAll = false;
  $('#dateInput').val(convertDateFormat(new Date()));
  date = $('#dateInput').val();
  $('#worker'+selectedWorker).removeClass('active');
  selectedProject = 0;
  selectedWorker = 0;
  $('#projectSelect').val(0).trigger('change');
  /*
  updateTasks(date);
  drawCalendar();
  setTimeout(()=>{
    drawTasks();
    fillExpired();
  },200);
  */
  $('#customSwitch1').on('change', switcher);
  //$('#weekCount').on('change', counter);
}
function refreshDataOnPage(){
  updateTasks(date);
  drawCalendar();
  setTimeout(()=>{
    if(selectedProject == 0)
      drawProjects();
    else
      drawTasks();
    fillExpired();
  },100);
}
function nextWeek(){
  //console.log('moving one week forward');
  var workingDay = new Date(date);
  var newDate;
  if(view == 0)
    newDate = new Date(workingDay.setDate(workingDay.getDate()+7));
  else
    newDate = addMonth(workingDay);
    //console.log('new active date is:' + newDate);
  date = newDate;
  $('#dateInput').val(convertDateFormat(date));
  //refreshDataOnPage();
  $('#projectSelect').val(selectedProject).trigger('change');
}
function prevWeek(){
  //console.log('moving one week backwards');
  var workingDay = new Date(date);
  var newDate;
  if(view == 0)
    newDate = new Date(workingDay.setDate(workingDay.getDate()-7));
  else
    newDate = subMonth(workingDay);
    //console.log('new active date is:' + newDate);
  date = newDate;
  $('#dateInput').val(convertDateFormat(date));
  //  refreshDataOnPage();
  $('#projectSelect').val(selectedProject).trigger('change');
}
function updateTasks(date){
  //console.log('update task for date:'+date);
  $.get( "/calendar/tasks", {date, selectedProject, seeAll, weekCount, category}, function( data ) {
    //console.log(data)
    activeTasks = data.activeTasks;
    expiredTasks = data.expiredTasks;
  });
}
function convertDateFormat(d){
  var date = new Date(d);
  var d,m,y;
  if( ( date.getDate() ) < 10 ) d = '0' + ( date.getDate() ); else d = date.getDate();
  if( ( date.getMonth() + 1 ) < 10 ) m = '0' + ( date.getMonth() + 1 ); else m = ( date.getMonth() + 1 );
  y = date.getFullYear();
  return date = y + '-' + m + '-' + d;
}
function changeDate(){
  if($('#dateInput').val()){
    date = $('#dateInput').val();
    selectedProject = $('#projectSelect').val();
    refreshDataOnPage();
  }
}
function sameDay(date1, date2) {
  var d1 = new Date(date1);
  var d2 = new Date(date2);
  return d1.getDate() === d2.getDate() &&
    d1.getMonth() === d2.getMonth() &&
    d1.getFullYear() === d2.getFullYear();
}
function animateList(){
  $('#scrollList').animate({ 'scrollTop': '-=-'+hiddenScroll }, {
    duration: 2000*expiredTasks.length,
    easing: 'linear', 
    complete: function() {
        $('#scrollList').animate({ 'scrollTop': '-='+hiddenScroll }, {
            duration: 1000, 
            complete: animateList});
    }});
}
function addMonth(d){
  var now = new Date(d);
  if (now.getMonth() == 11) {
      var current = new Date(now.getFullYear() + 1, 0, 1);
  } else {
      var current = new Date(now.getFullYear(), now.getMonth() + 1, 1);
  }
  return current;
}
function subMonth(d){
  var now = new Date(d);
  if (now.getMonth() == 0) {
      var current = new Date(now.getFullYear() - 1, 11, 1);
  } else {
      var current = new Date(now.getFullYear(), now.getMonth() - 1, 1);
  }
  return current;
}
/////////////////////AUTO REFRESH/////////////////

$(document).ready(function () {
  //Increment the idle time counter every minute.
  var idleInterval = setInterval(timerIncrement, 60000); // 1 minute
    //Zero the idle timer on mouse movement.
    $(this).mousemove(function (e) {
       if(idleTime > 5){
         //console.log('inactive for more than 5 min, lets reload data just in case something new');
         resetInput();
         firstReset = false;
       }
      //aktivnost
      idleTime = 0;
    });
    $(this).keypress(function (e) {
      if(idleTime > 5){
        //console.log('inactive for more than 5 min, lets reload data just in case something new');
        resetInput();
        firstReset = false;
      }
      //aktivnost
      idleTime = 0;
    });
});

function timerIncrement() {
  //console.log(idleTime);
  idleTime = idleTime + 1;
  if(idleTime > 4 && firstReset == false){
    firstReset= true;
    resetInput();
  }
  if (idleTime % 120 == 0) { // every second  hour
    //console.log('----RESETING INPUT---')
    resetInput();
  }
  if(idleTime > 500){
    window.location.reload();
  }
}
////////////////////AUTO REFRESH//////////////////
function switcher(){
  if(seeAll)
    seeAll = false;
  else
    seeAll = true;
  $('#projectSelect').val(selectedProject).trigger('change');
}
function counter(){
  weekCount = $('#weekCount').val();
  $('#projectSelect').val(selectedProject).trigger('change');
}
function changeView(viewInput){
  if(viewInput == 1){
    view = 1;
    //$('#optionWeek').removeClass('active');
    $('#weekCount').prop('disabled', true);
    weekCount = 6;
    var workingDay = new Date(date);
    var n = workingDay.getDate();
    $('#dateInput').val(convertDateFormat(new Date(workingDay.setDate(workingDay.getDate()-n+1))));
  }
  else{
    view = 0;
    //$('#optionMonth').removeClass('active');
    $('#weekCount').prop('disabled', false);
    weekCount = $('#weekCount').val();
  }
  $('#projectSelect').val(selectedProject).trigger('change');
}
function toggleFullScreen() {
  if (!document.fullscreenElement) {
    document.documentElement.requestFullscreen();
  } else {
    if (document.exitFullscreen) {
      document.exitFullscreen(); 
    }
  }
}
//var
var view = 0; // 0 is week, 1 is month
var projectId;
var taskId;
var activeTaskId;
var weekCount = 2;
var loggedUser;
var idleTime = 0;
var firstReset = false;
var seeAll = false;
var activeUser = 1;
var activeProject = 1;
var selectedWorker = 0;
var selectedProject = 0;
var selectedTask = 0;
var projectList = [];
var servisPresent = false;
var date;
var firstDate;
var lastDate;
var activeTasks;
var expiredTasks;
var allUsers;
var allProjects;
var userRoles;
var workRoles;
var priorities;
var categories;
var hiddenScroll;
const monthNames = ["Januar", "Februar", "Marec", "April", "Maj", "Junij",
  "Julij", "August", "September", "Oktober", "November", "December"
];
//$('#dateInput').on('change', changeDate);
//$('#projectSelect').on('change', changeDate);
//$('#customSwitch1').on('change', switcher);
//$('#weekCount').on('change', counter);
var allSubtasks;
var showCategoryLegend = false;
var category;
var tmpTask;