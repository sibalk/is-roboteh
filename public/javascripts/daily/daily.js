$(function(){
  loggedUser = {name:$('#loggedUser').html().split(" (")[0].split(" ")[0], surname:$('#loggedUser').html().split(" (")[0].split(" ")[1], role:$('#loggedUser').html().split(" (")[1].split(")")[0].toLowerCase()};
  if(loggedUser.role == 'admin' || loggedUser.role.substring(0,5) == 'vodja'){
    $('#header').text("Opravila danes, " + new Date().getDate() + "." + (new Date().getMonth()+1) + "." + new Date().getFullYear());
  }
  if(loggedUser.role == 'admin'){
    adminView = true;
  }
  if(loggedUser.role == 'info' || loggedUser.role == 'tajnik' || loggedUser.role == 'komercialist' || loggedUser.role == 'komerciala' || loggedUser.role == 'računovodstvo')
    animationCheck = true;
  hiddenScroll = $('#dailyList').prop('scrollHeight') - $('#dailyList').height() + 15;
  if( hiddenScroll > 0 && animationCheck){
    //big list, animate the list up and down slowly
    animateList();
  }
  hiddenScrollWeek = $('#weeklyList').prop('scrollHeight') - $('#weeklyList').height() + 15;
  if( hiddenScrollWeek > 0 && animationCheck){
    //big list, animate the list up and down slowly
    animateListWeek();
  }
  //get all users so the coloring the names is possible
  $.get('/employees/all', function(resp){
    allUsers = resp.data
    changeTaskTable(0);
    changeWeekTable(0);
  })
  function checkTime(i){
    return (i < 10) ? "0" + i : i;
  }
  function startTime(){
    var today = new Date(),
        h = checkTime(today.getHours()),
        m = checkTime(today.getMinutes()),
        s = checkTime(today.getSeconds()),
        day = today.getDate(),
        month = today.getMonth() + 1,
        year = today.getFullYear();
    $('#time').html(day+"."+month+"."+year+", "+h+":"+m+":"+s);
    t = setTimeout(function () {startTime()}, 500);
  }
  startTime();
  // add on event call functions
  $('#fullScreenBtn').on('click', toggleFullScreen);
  $('#changeDateBtn').on('click', changeDate);
  $('#showImgBtn').on('click', showimage);
  $('#customAdminCheck').on('change', function(event) {
    toggleAdmin(this);
  })
  $('#customAnimationCheck').on('change', function(event) {
    toggleAnimation(this);
  })
  $('#buttonCategoryGroup').on('click', 'input', function(event) {
    //debugger;
    changeTaskTable(this.value);
  })
  $('#modalWorkersSelect').on('click', 'button', function(event) {
    //debugger;
    openWorkerCategory(this.value);
  })
  $('#workerButton').on('click', 'button', function(event) {
    //debugger;
    if (this.value == -1)
      openWorkerTab();
    else
      openWorkerCategory(this.value);
  })
  $('#categoryWorkerList').on('click', 'button', function(event) {
    //debugger;
    openWorkerTasks(this.value);
  })
})

function changeDate(){
  date = $('#dateInput').val();
  changeTaskTable(categoryTask);
  /*
  if(date){
    //debugger;
    //pridobi opravila za ta dan in posodobi dan
    $.get('/daily/date', {date, categoryTask, adminView}, function(resp){
      if(resp.success){
        var tasks = resp.data;
        dailyTasks = tasks;
        $('#dailyList').empty();
        for(var i = 0; i < tasks.length; i++){
          //old with category and subscriber info
          /*
          var element = `<row class="list-group-item flex-column" id=`+tasks[i].id+`>
            <div class="d-flex w-100 justify-content-between">
              <div class="h5 mb-1" id="taskName">`+tasks[i].task_name+`</div>
              <div id="taskCategory">`+tasks[i].category+`</div>
            </div>
            <div class="d-flex justify-content-between">
              <div class="mb-1" id="workers">`+tasks[i].workers+`</div>
              <div id="subscribers">`+tasks[i].subscriber+`</div>
            </div>
          </row>`;
          //
          var workers = ``;
          if(tasks[i].workersId[0]){
            for(var j = 0; j < tasks[i].workersId.length; j++){
              var x = allUsers.find(x => x.id == tasks[i].workersId[j]);
              if(x.role == 'konstrukter' || x.role == 'konstruktor')
                workers += `<div class="mb-1 mr-2 color-builder" id="workers">`+x.name +" "+ x.surname+`</div>`;
              else if(x.role == 'strojnik')
                workers += `<div class="mb-1 mr-2 color-mechanic" id="workers">`+x.name +" "+ x.surname+`</div>`;
              else if(x.role == 'električar')
                workers += `<div class="mb-1 mr-2 color-electrican" id="workers">`+x.name +" "+ x.surname+`</div>`;
              else if(x.role == 'plc')
                workers += `<div class="mb-1 mr-2 color-plc" id="workers">`+x.name +" "+ x.surname+`</div>`;
              else if(x.role == 'robot')
                workers += `<div class="mb-1 mr-2 color-robot" id="workers">`+x.name +" "+ x.surname+`</div>`;
              else if(x.role == 'vodja')
                workers += `<div class="mb-1 mr-2 font-weight-bold" id="workers">`+x.name +" "+ x.surname+`</div>`;
              else if(x.role == 'admin')
                workers += `<div class="mb-1 mr-2 font-weight-bold font-italic" id="workers">`+x.name +" "+ x.surname+`</div>`;
              else
                workers += `<div class="mb-1 mr-2" id="workers">`+x.name +" "+ x.surname+`</div>`;
            }
          }
          else{
            workers = `<div class="mb-1 mr-2" id="workers">Ni dodeljeno</div>`;
          }
          var start = '/';
          var finish = '/';
          if(tasks[i].task_start)
            start = tasks[i].task_start.split("T")[0].split("-")[2] + "." + tasks[i].task_start.split("T")[0].split("-")[1] + "." + tasks[i].task_start.split("T")[0].split("-")[0];
          if(tasks[i].task_finish)
            finish = tasks[i].task_finish.split("T")[0].split("-")[2] + "." + tasks[i].task_finish.split("T")[0].split("-")[1] + "." + tasks[i].task_finish.split("T")[0].split("-")[0];
          var servis = "";
          var head = "";
          var expired = "";
          if(tasks[i].subscriberservis){
            servis = '<span class="badge badge-info ml-2">Servis</span>';
            head = `<div class="d-flex w-100 justify-content-between">
              <div class="h5 mb-1 font-italic" id="projectName">`+tasks[i].subscriberservis+`</div>
            </div>`;
          }
          else if(tasks[i].project_name){
            head = `<div class="d-flex w-100 justify-content-between">
              <div class="h5 mb-1 font-italic" id="projectName">`+tasks[i].project_name+`</div>
            </div>`;
          }
          if(tasks[i].expired)
            expired = '<span class="badge badge-danger ml-2">ZAMUDA</span>';
          var element = `<row class="list-group-item flex-column" id=`+tasks[i].id+`>
            `+head+`
            <div class="d-flex w-100 justify-content-between">
              <div class="h5 mb-1" id="taskName">`+tasks[i].task_name+`</div>
            </div>
            <div class="d-flex w-100 justify-content-start">
              <div class="h6 mb-1 mr-2" id="weekDate">`+ start +" - " + finish +`</div>
              ` + servis + expired + `
            </div>
            <div class="d-flex justify-content-start flex-wrap">
              `+workers+`
            </div>
          </row>`;
          $('#dailyList').append(element);
        }
        //if admin or leader change date info
        if(loggedUser.role == 'vodja' || loggedUser.role == 'admin'){
          $('#header').text("Opravila za dan: "+date.split("-")[2] + "." + date.split("-")[1] + "." + date.split("-")[0])
        }
        //calculate new hiddenScroll
        hiddenScroll = $('#dailyList').prop('scrollHeight') - $('#dailyList').height() + 15;
        $( "#dailyList" ).stop();
        $('#dailyList').animate({ 'scrollTop': '-='+$('#dailyList').scrollTop() }, 'slow');
        if(document.getElementById('customAnimationCheck').checked)
          animateList();
        //debugger;
        changeWeekTable();
      }
    })
  }
  */

}
function changeTaskTable(category){
  //debugger;
  $('#option0').removeClass('active');
  $('#option1').removeClass('active');
  $('#option2').removeClass('active');
  $('#option3').removeClass('active');
  $('#option4').removeClass('active');
  $('#option5').removeClass('active');
  $('#option6').removeClass('active');
  $('#option7').removeClass('active');
  setTimeout(()=>{
    $('#option'+category+'').addClass('active');
  })
  categoryTask = category;
  date = $('#dateInput').val();
  $.get('/daily/date', {date, categoryTask, adminView}, function( resp ) {
    if(resp.success){
      var tasks = resp.data;
      dailyTasks = tasks;
      $('#dailyList').empty();
      //also need to get all absences so i can ignore workers who are absent
      $.get('/absences/date', {date}, function( data ){
        if(data.success){
          var absences = data.data;
          allAbsences = absences;
          // debugger;
          //have daily tasks, workers and absences
          for(var i = 0; i < tasks.length; i++){
            var workers = ``;
            if(tasks[i].workersId[0]){
              for(var j = 0; j < tasks[i].workersId.length; j++){
                //x is worker, y is his absence if x exists
                var x = allUsers.find(x => x.id == tasks[i].workersId[j]);
                var y;
                if(x){
                  y = allAbsences.find(a => a.id_user == x.id && a.id_project == null && a.id_task == null);
                  // debugger;
                  if(!y)
                    y = allAbsences.find(a => a.id_user == x.id && a.id_project == tasks[i].id_project && a.id_task == tasks[i].id);
                }
                // debugger;
                if(!y){
                  if(x.role == 'konstrukter' || x.role == 'konstruktor')
                    workers += `<div class="mb-1 mr-2 color-builder" id="workers">`+x.name +" "+ x.surname+`</div>`;
                  else if(x.role == 'strojnik')
                    workers += `<div class="mb-1 mr-2 color-mechanic" id="workers">`+x.name +" "+ x.surname+`</div>`;
                  else if(x.role == 'električar')
                    workers += `<div class="mb-1 mr-2 color-electrican" id="workers">`+x.name +" "+ x.surname+`</div>`;
                  else if(x.role == 'programer plc-jev')
                    workers += `<div class="mb-1 mr-2 color-plc" id="workers">`+x.name +" "+ x.surname+`</div>`;
                  else if(x.role == 'programer robotov')
                    workers += `<div class="mb-1 mr-2 color-robot" id="workers">`+x.name +" "+ x.surname+`</div>`;
                  else if(x.role == 'vodja' || x.role == 'vodja projektov' || x.role == 'vodja strojnikov' || x.role == 'vodja CNC obdelave' || x.role == 'vodja električarjev' || x.role == 'vodja programerjev')
                    workers += `<div class="mb-1 mr-2 font-weight-bold" id="workers">`+x.name +" "+ x.surname+`</div>`;
                  else if(x.role == 'admin')
                    workers += `<div class="mb-1 mr-2 font-weight-bold font-italic" id="workers">`+x.name +" "+ x.surname+`</div>`;
                  else
                    workers += `<div class="mb-1 mr-2" id="workers">`+x.name +" "+ x.surname+`</div>`;
                }
              }
            }
            else{
              workers = `<div class="mb-1 mr-2" id="workers">Ni dodeljeno</div>`;
            }
            // debugger;
            var start = '/';
            var finish = '/';
            if(tasks[i].task_start)
              start = tasks[i].task_start.split("T")[0].split("-")[2] + "." + tasks[i].task_start.split("T")[0].split("-")[1] + "." + tasks[i].task_start.split("T")[0].split("-")[0];
            if(tasks[i].task_finish)
              finish = tasks[i].task_finish.split("T")[0].split("-")[2] + "." + tasks[i].task_finish.split("T")[0].split("-")[1] + "." + tasks[i].task_finish.split("T")[0].split("-")[0];
            var servis = "";
            var head = "";
            var expired = "";
            var dataLink = "/projects/id?id="+tasks[i].id_project+"&taskId="+tasks[i].id;
            var linkClass = "list-group-item-linkable";
            if(tasks[i].subscriberservis){
              dataLink = "/servis/?taskId="+tasks[i].id;
              linkClass = "list-group-item-linkable";
              servis = '<span class="badge badge-info ml-2 mt-1">Servis</span>';
              head = `<div class="d-flex w-100 justify-content-between">
                <div class="h5 mb-1 font-italic" id="projectName">`+tasks[i].subscriberservis+`</div>
              </div>`;
            }
            else if(tasks[i].project_name){
              head = `<div class="d-flex w-100 justify-content-between">
                <div class="h5 mb-1 font-italic" id="projectName">`+tasks[i].project_name+`</div>
              </div>`;
            }
            if(tasks[i].expired)
              expired = '<span class="badge badge-danger ml-2 mt-1">ZAMUDA</span>';
            var element = `<row class="list-group-item `+linkClass+` flex-column" id=`+tasks[i].id+` data-link="`+dataLink+`">
              `+head+`
              <div class="d-flex w-100 justify-content-between">
                <div class="h5 mb-1" id="taskName">`+tasks[i].task_name+`</div>
              </div>
              <div class="d-flex w-100 justify-content-start">
                <div class="h6 mb-1 mr-2" id="weekDate">`+ start +" - " + finish +`</div>
                ` + servis + expired + `
              </div>
              <div class="d-flex justify-content-start flex-wrap">
                `+workers+`
              </div>
            </row>`;
            // debugger;
            //if(tasks[i].active)
            if(tasks[i].active_project == null || tasks[i].active_project == true)
              $('#dailyList').append(element);
          }
          relinkListItems();
          //if admin or leader change date info
          if(loggedUser.role.substring(0,5) == 'vodja' || loggedUser.role == 'admin'){
            if(!date)
              $('#header').text("Opravila danes");
            else
              $('#header').text("Opravila za dan: "+date.split("-")[2] + "." + date.split("-")[1] + "." + date.split("-")[0])
          }
          //calculate new hiddenScroll
          hiddenScroll = $('#dailyList').prop('scrollHeight') - $('#dailyList').height() + 15;
          $( "#dailyList" ).stop();
          $('#dailyList').animate({ 'scrollTop': '-='+$('#dailyList').scrollTop() }, 'slow');
          if(document.getElementById('customAnimationCheck').checked)
            animateList();
          //debugger;
          changeWeekTable(categoryTask);
        }
        else{
          //have dailyTasks and workers, no absences
          for(var i = 0; i < tasks.length; i++){
            //old with category and subscriber info
            /*
            var element = `<row class="list-group-item flex-column" id=`+tasks[i].id+`>
              <div class="d-flex w-100 justify-content-between">
                <div class="h5 mb-1" id="taskName">`+tasks[i].task_name+`</div>
                <div id="taskCategory">`+tasks[i].category+`</div>
              </div>
              <div class="d-flex justify-content-between">
                <div class="mb-1" id="workers">`+tasks[i].workers+`</div>
                <div id="subscribers">`+tasks[i].subscriber+`</div>
              </div>
            </row>`;
            */
            var workers = ``;
            if(tasks[i].workersId[0]){
              for(var j = 0; j < tasks[i].workersId.length; j++){
                var x = allUsers.find(x => x.id == tasks[i].workersId[j]);
                if(x.role == 'konstrukter' || x.role == 'konstruktor')
                  workers += `<div class="mb-1 mr-2 color-builder" id="workers">`+x.name +" "+ x.surname+`</div>`;
                else if(x.role == 'strojnik')
                  workers += `<div class="mb-1 mr-2 color-mechanic" id="workers">`+x.name +" "+ x.surname+`</div>`;
                else if(x.role == 'električar')
                  workers += `<div class="mb-1 mr-2 color-electrican" id="workers">`+x.name +" "+ x.surname+`</div>`;
                else if(x.role == 'programer plc-jev')
                  workers += `<div class="mb-1 mr-2 color-plc" id="workers">`+x.name +" "+ x.surname+`</div>`;
                else if(x.role == 'programer robotov')
                  workers += `<div class="mb-1 mr-2 color-robot" id="workers">`+x.name +" "+ x.surname+`</div>`;
                else if(x.role == 'vodja' || x.role == 'vodja projektov' || x.role == 'vodja strojnikov' || x.role == 'vodja CNC obdelave' || x.role == 'vodja električarjev' || x.role == 'vodja programerjev')
                  workers += `<div class="mb-1 mr-2 font-weight-bold" id="workers">`+x.name +" "+ x.surname+`</div>`;
                else if(x.role == 'admin')
                  workers += `<div class="mb-1 mr-2 font-weight-bold font-italic" id="workers">`+x.name +" "+ x.surname+`</div>`;
                else
                  workers += `<div class="mb-1 mr-2" id="workers">`+x.name +" "+ x.surname+`</div>`;
              }
            }
            else{
              workers = `<div class="mb-1 mr-2" id="workers">Ni dodeljeno</div>`;
            }
            //debugger;
            var start = '/';
            var finish = '/';
            if(tasks[i].task_start)
              start = tasks[i].task_start.split("T")[0].split("-")[2] + "." + tasks[i].task_start.split("T")[0].split("-")[1] + "." + tasks[i].task_start.split("T")[0].split("-")[0];
            if(tasks[i].task_finish)
              finish = tasks[i].task_finish.split("T")[0].split("-")[2] + "." + tasks[i].task_finish.split("T")[0].split("-")[1] + "." + tasks[i].task_finish.split("T")[0].split("-")[0];
            var servis = "";
            var head = "";
            var expired = "";
            var linkClass = "list-group-item-linkable";
            var dataLink = "/projects/id?id="+tasks[i].id_project+"&taskId="+tasks[i].id;
            if(tasks[i].subscriberservis){
              dataLink = "/servis/?taskId="+tasks[i].id;
              linkClass = "list-group-item-linkable";
              servis = '<span class="badge badge-info ml-2 mt-1">Servis</span>';
              head = `<div class="d-flex w-100 justify-content-between">
                <div class="h5 mb-1 font-italic" id="projectName">`+tasks[i].subscriberservis+`</div>
              </div>`;
            }
            else if(tasks[i].project_name){
              head = `<div class="d-flex w-100 justify-content-between">
                <div class="h5 mb-1 font-italic" id="projectName">`+tasks[i].project_name+`</div>
              </div>`;
            }
            if(tasks[i].expired)
              expired = '<span class="badge badge-danger ml-2 mt-1">ZAMUDA</span>';
            var element = `<row class="list-group-item `+linkClass+` flex-column" id=`+tasks[i].id+` data-link="`+dataLink+`">
              `+head+`
              <div class="d-flex w-100 justify-content-between">
                <div class="h5 mb-1" id="taskName">`+tasks[i].task_name+`</div>
              </div>
              <div class="d-flex w-100 justify-content-start">
                <div class="h6 mb-1 mr-2" id="weekDate">`+ start +" - " + finish +`</div>
                ` + servis + expired + `
              </div>
              <div class="d-flex justify-content-start flex-wrap">
                `+workers+`
              </div>
            </row>`;
            //if(tasks[i].active)
            if(tasks[i].active_project == null || tasks[i].active_project == true)
              $('#dailyList').append(element);
          }
          relinkListItems();
          //if admin or leader change date info
          if(loggedUser.role.substring(0,5) == 'vodja' || loggedUser.role == 'admin'){
            if(!date)
              $('#header').text("Opravila danes");
            else
              $('#header').text("Opravila za dan: "+date.split("-")[2] + "." + date.split("-")[1] + "." + date.split("-")[0])
          }
          //calculate new hiddenScroll
          hiddenScroll = $('#dailyList').prop('scrollHeight') - $('#dailyList').height() + 15;
          $( "#dailyList" ).stop();
          $('#dailyList').animate({ 'scrollTop': '-='+$('#dailyList').scrollTop() }, 'slow');
          if(document.getElementById('customAnimationCheck').checked)
            animateList();
          //debugger;
          changeWeekTable(categoryTask);
        }
      })
    }
  });
}
function changeWeekTable(category){
  //debugger;
  /*
  $('#option0').removeClass('active');
  $('#option1').removeClass('active');
  $('#option2').removeClass('active');
  $('#option3').removeClass('active');
  $('#option4').removeClass('active');
  $('#option5').removeClass('active');
  $('#option6').removeClass('active');
  $('#option7').removeClass('active');
  setTimeout(()=>{
    $('#option'+category+'').addClass('active');
  })
  categoryTask = category;
  */
 date = $('#dateInput').val();
  $.get('/daily/week', {date, categoryTask, adminView}, function( resp ) {
    if(resp.success){
      var tasks = resp.data;
      $('#weeklyList').empty();
      for(var i = 0; i < tasks.length; i++){
        //old with category and subscriber info
        /*
        var element = `<row class="list-group-item flex-column" id=`+tasks[i].id+`>
          <div class="d-flex w-100 justify-content-between">
            <div class="h5 mb-1" id="taskName">`+tasks[i].task_name+`</div>
            <div id="taskCategory">`+tasks[i].category+`</div>
          </div>
          <div class="d-flex justify-content-between">
            <div class="mb-1" id="workers">`+tasks[i].workers+`</div>
            <div id="subscribers">`+tasks[i].subscriber+`</div>
          </div>
        </row>`;
        */
        var workers = ``;
        if(tasks[i].workersId[0]){
          for(var j = 0; j < tasks[i].workersId.length; j++){
            var x = allUsers.find(x => x.id == tasks[i].workersId[j]);
            if(x.role == 'konstrukter' || x.role == 'konstruktor')
              workers += `<div class="mb-1 mr-2 color-builder" id="workers">`+x.name +" "+ x.surname+`</div>`;
            else if(x.role == 'strojnik')
              workers += `<div class="mb-1 mr-2 color-mechanic" id="workers">`+x.name +" "+ x.surname+`</div>`;
            else if(x.role == 'električar')
              workers += `<div class="mb-1 mr-2 color-electrican" id="workers">`+x.name +" "+ x.surname+`</div>`;
            else if(x.role == 'programer plc-jev')
              workers += `<div class="mb-1 mr-2 color-plc" id="workers">`+x.name +" "+ x.surname+`</div>`;
            else if(x.role == 'programer robotov')
              workers += `<div class="mb-1 mr-2 color-robot" id="workers">`+x.name +" "+ x.surname+`</div>`;
            else if(x.role == 'vodja' || x.role == 'vodja projektov' || x.role == 'vodja strojnikov' || x.role == 'vodja CNC obdelave' || x.role == 'vodja električarjev' || x.role == 'vodja programerjev')
              workers += `<div class="mb-1 mr-2 font-weight-bold" id="workers">`+x.name +" "+ x.surname+`</div>`;
            else if(x.role == 'admin')
              workers += `<div class="mb-1 mr-2 font-weight-bold font-italic" id="workers">`+x.name +" "+ x.surname+`</div>`;
            else
              workers += `<div class="mb-1 mr-2" id="workers">`+x.name +" "+ x.surname+`</div>`;
          }
        }
        else{
          workers = `<div class="mb-1 mr-2" id="workers">Ni dodeljeno</div>`;
        }
        //debugger;
        var start = '/';
        var finish = '/';
        if(tasks[i].task_start)
          start = tasks[i].task_start.split("T")[0].split("-")[2] + "." + tasks[i].task_start.split("T")[0].split("-")[1] + "." + tasks[i].task_start.split("T")[0].split("-")[0];
        if(tasks[i].task_finish)
          finish = tasks[i].task_finish.split("T")[0].split("-")[2] + "." + tasks[i].task_finish.split("T")[0].split("-")[1] + "." + tasks[i].task_finish.split("T")[0].split("-")[0];
        var servis = "";
        var head = "";
        var expired = "";
        var linkClass = "list-group-item-linkable";
        var dataLink = "/projects/id?id="+tasks[i].id_project+"&taskId="+tasks[i].id;
        if(tasks[i].subscriberservis){
          dataLink = "/servis/?taskId="+tasks[i].id;
          linkClass = "list-group-item-linkable";
          servis = '<span class="badge badge-info ml-2 mt-1">Servis</span>';
          head = `<div class="d-flex w-100 justify-content-between">
            <div class="h5 mb-1 font-italic" id="projectName">`+tasks[i].subscriberservis+`</div>
          </div>`;
        }
        else if(tasks[i].project_name){
          head = `<div class="d-flex w-100 justify-content-between">
            <div class="h5 mb-1 font-italic" id="projectName">`+tasks[i].project_name+`</div>
          </div>`;
        }
        if(tasks[i].expired)
          expired = '<span class="badge badge-danger ml-2 mt-1">ZAMUDA</span>';
        var element = `<row class="list-group-item `+linkClass+` flex-column" id=`+tasks[i].id+` data-link="`+dataLink+`">
          `+head+`
          <div class="d-flex w-100 justify-content-between">
            <div class="h5 mb-1" id="taskName">`+tasks[i].task_name+`</div>
          </div>
          <div class="d-flex w-100 justify-content-start">
            <div class="h6 mb-1 mr-2" id="weekDate">`+ start +" - " + finish +`</div>
            ` + servis + expired + `
          </div>
          <div class="d-flex justify-content-start flex-wrap">
            `+workers+`
          </div>
        </row>`;
        //debugger
        var x = dailyTasks.find(d => d.id == tasks[i].id);
        //debugger;
        if(!x){
          if(tasks[i].active_project == null || tasks[i].active_project == true)
            $('#weeklyList').append(element);
          //debugger;
        }
        //else
        //debugger;
      }
      relinkListItems();
      //calculate new hiddenScroll
      hiddenScrollWeek = $('#weeklyList').prop('scrollHeight') - $('#weeklyList').height() + 15;
      $( "#weeklyList" ).stop();
      $('#weeklyList').animate({ 'scrollTop': '-='+$('#weeklyList').scrollTop() }, 'slow');
      if(document.getElementById('customAnimationCheck').checked)
        animateListWeek();
      //debugger;
      
    }
  });
}
function animateList(){
  $('#dailyList').animate({ 'scrollTop': '-=-'+hiddenScroll }, {
    duration: 50000,
    easing: 'linear', 
    complete: function() {
        $('#dailyList').animate({ 'scrollTop': '-='+hiddenScroll }, {
            duration: 1000, 
            complete: animateList});
    }});
}
function toggleAnimation(element){
  //debugger;
  if(element.checked){
    $('#dailyList').animate({ 'scrollTop': '-='+$('#dailyList').scrollTop() }, 'slow');
    animateList();
    $('#weeklyList').animate({ 'scrollTop': '-='+$('#weeklyList').scrollTop() }, 'slow');
    animateListWeek();
  }
  else{
    $( "#dailyList" ).stop();
    $( "#weeklyList" ).stop();
  }
}
function animateListWeek(){
  $('#weeklyList').animate({ 'scrollTop': '-=-'+hiddenScrollWeek }, {
    duration: 50000,
    easing: 'linear', 
    complete: function() {
        $('#weeklyList').animate({ 'scrollTop': '-='+hiddenScrollWeek }, {
            duration: 1000, 
            complete: animateListWeek});
    }});
}
function toggleAnimationWeek(element){
  //debugger;
  if(element.checked){
    $('#weeklyList').animate({ 'scrollTop': '-='+$('#weeklyList').scrollTop() }, 'slow');
    animateListWeek();
  }
  else{
    $( "#weeklyList" ).stop();
  }
}
function toggleAdmin(element){
  // debugger;
  if(element.checked){
    adminView = true;
    $('#buttonCategoryGroup').empty();
    var x = `<label class="btn btn-primary">Vsa
      <input id="option0" type="radio" name="options" autocomplete="off" value="0" checked="" />
    </label>
    <label class="btn btn-purchase">Nabava
      <input id="option2" type="radio" name="options" autocomplete="off" value="2" />
    </label>
    <label class="btn btn-construction">Konstrukcija
      <input id="option3" type="radio" name="options" autocomplete="off" value="3" />
    </label>
    <label class="btn btn-mechanic">Strojna izdelava
      <input id="option4" type="radio" name="options" autocomplete="off" value="4" />
    </label>
    <label class="btn btn-electrican">Elektro izdelava
      <input id="option5" type="radio" name="options" autocomplete="off" value="5" />
    </label>
    <label class="btn btn-assembly">Montaža
      <input id="option6" type="radio" name="options" autocomplete="off" value="6" />
    </label>
    <label class="btn btn-programmer">Programiranje
      <input id="option7" type="radio" name="options" autocomplete="off" value="7" />
    </label>
    <label class="btn btn-dark">Brez
      <input id="option1" type="radio" name="options" autocomplete="off" value="1" />
    </label>`;
    $('#buttonCategoryGroup').append(x);
    $('#workerButton').empty();
    var y = '<button class="btn btn-primary" value=-1 >Delavci</button>';
    $('#workerButton').append(y);
    changeTaskTable(0);
    changeWeekTable(0);
    // debugger
  }
  else{
    adminView = false;
    $('#buttonCategoryGroup').empty();
    var x = `</label>
    <label class="btn btn-mechanic">Strojna izdelava
      <input id="option4" type="radio" name="options" autocomplete="off" value="4" />
    </label>
    <label class="btn btn-electrican">Elektro izdelava
      <input id="option5" type="radio" name="options" autocomplete="off" value="5" />
    </label>
    <label class="btn btn-assembly">Montaža
      <input id="option6" type="radio" name="options" autocomplete="off" value="6" />
    </label>`;
    $('#buttonCategoryGroup').append(x);
    $('#workerButton').empty();
    var y = `<button class="btn btn-mechanic mr-2" value=2 >Strojniki</button>
    <button class="btn btn-electrican mr-2" value=3 >Električarji</button>
    <button class="btn btn-secondary mr-2" value=0 >Ostali</button>`;
    $('#workerButton').append(y);
    changeTaskTable(0);
    changeWeekTable(0);
    // debugger
  }
}
function openWorkerTab(){
  $('#categoryWorkerList').empty();
  $('#modalWorkerCategory').modal('toggle');
}
function openWorkerCategory(role){
  //debugger;
  var activeUser = 1;
  $.get('/employees/all', {activeUser}, function(resp){
    $('#categoryWorkerList').empty();
    allUsers = resp.data
    // debugger;
    for(var i = 0; i < allUsers.length; i++){
      var element = `<button class="list-group-item btn btn-outline-secondary" value=` + allUsers[i].id + `>`+allUsers[i].name+" "+allUsers[i].surname+`</button>`;
      if(allUsers[i].role == 'strojnik' && role == '2'){
        //debugger;
        element = `<button class="list-group-item btn btn-outline-mechanic" value=` + allUsers[i].id + `>`+allUsers[i].name+" "+allUsers[i].surname+`</button>`;
        $('#categoryWorkerList').append(element);
      }
      else if(allUsers[i].role == 'električar' && role == '3'){
        //debugger;
        element = `<button class="list-group-item btn btn-outline-electrican" value=` + allUsers[i].id + `>`+allUsers[i].name+" "+allUsers[i].surname+`</button>`;
        $('#categoryWorkerList').append(element);
      }
      else if((allUsers[i].role == 'konstrukter' || allUsers[i].role == 'konstruktor') && role == '1'){
        //debugger;
        element = `<button class="list-group-item btn btn-outline-success" value=` + allUsers[i].id + `>`+allUsers[i].name+" "+allUsers[i].surname+`</button>`;
        $('#categoryWorkerList').append(element);
      }
      else if(allUsers[i].role == 'programer plc-jev' && role == '4'){
        //debugger;
        element = `<button class="list-group-item btn btn-outline-plc" value=` + allUsers[i].id + `>`+allUsers[i].name+" "+allUsers[i].surname+`</button>`;
        $('#categoryWorkerList').append(element);
      }
      else if(allUsers[i].role == 'programer robotov' && role == '5'){
        //debugger;
        element = `<button class="list-group-item btn btn-outline-warning" value=` + allUsers[i].id + `>`+allUsers[i].name+" "+allUsers[i].surname+`</button>`;
        $('#categoryWorkerList').append(element);
      }
      else if((allUsers[i].role != 'admin' && allUsers[i].role != 'vodja' && allUsers[i].role == 'vodja projektov' && allUsers[i].role == 'vodja strojnikov' && allUsers[i].role == 'vodja CNC obdelave' && allUsers[i].role == 'vodja električarjev' && allUsers[i].role == 'vodja programerjev' && allUsers[i].role != 'programer plc-jev'
      && allUsers[i].role != 'programer robotov' && allUsers[i].role != 'konstruktor'
      && allUsers[i].role != 'konstrukter' && allUsers[i].role != 'strojnik'
      && allUsers[i].role != 'električar') && role == '0'){
        //debugger;
        $('#categoryWorkerList').append(element);
      }
    }
    if(adminView)
      $('#modalWorkerCategory').modal('toggle');
    $('#modalWorkerTab').modal('toggle');
  })
}
function openWorkerTasks(workerid){
  // debugger;
  category = 0;
  $('#option0').removeClass('active');
  $('#option1').removeClass('active');
  $('#option2').removeClass('active');
  $('#option3').removeClass('active');
  $('#option4').removeClass('active');
  $('#option5').removeClass('active');
  $('#option6').removeClass('active');
  $('#option7').removeClass('active');
  setTimeout(()=>{
    $('#option'+category+'').addClass('active');
  })
  categoryTask = category;
  date = $('#dateInput').val();
  $.get('/daily/date', {date, categoryTask, adminView}, function( resp ) {
    if(resp.success){
      var tasks = resp.data;
      dailyTasks = tasks;
      $('#dailyList').empty();
      for(var i = 0; i < tasks.length; i++){
        //dailyTasksId.push(tasks[i].id);
        // debugger;
        if(contains.call(tasks[i].workersId, workerid)){
          //old with category and subscriber info
          /*
          var element = `<row class="list-group-item flex-column" id=`+tasks[i].id+`>
            <div class="d-flex w-100 justify-content-between">
              <div class="h5 mb-1" id="taskName">`+tasks[i].task_name+`</div>
              <div id="taskCategory">`+tasks[i].category+`</div>
            </div>
            <div class="d-flex justify-content-between">
              <div class="mb-1" id="workers">`+tasks[i].workers+`</div>
              <div id="subscribers">`+tasks[i].subscriber+`</div>
            </div>
          </row>`;
          */
         var workers = ``;
          if(tasks[i].workersId[0]){
            for(var j = 0; j < tasks[i].workersId.length; j++){
              var x = allUsers.find(x => x.id == tasks[i].workersId[j]);
              if(x.role == 'konstrukter' || x.role == 'konstruktor')
                workers += `<div class="mb-1 mr-2 color-builder" id="workers">`+x.name +" "+ x.surname+`</div>`;
              else if(x.role == 'strojnik')
                workers += `<div class="mb-1 mr-2 color-mechanic" id="workers">`+x.name +" "+ x.surname+`</div>`;
              else if(x.role == 'električar')
                workers += `<div class="mb-1 mr-2 color-electrican" id="workers">`+x.name +" "+ x.surname+`</div>`;
              else if(x.role == 'programer plc-jev')
                workers += `<div class="mb-1 mr-2 color-plc" id="workers">`+x.name +" "+ x.surname+`</div>`;
              else if(x.role == 'programer robotov')
                workers += `<div class="mb-1 mr-2 color-robot" id="workers">`+x.name +" "+ x.surname+`</div>`;
              else if(x.role == 'vodja' || x.role == 'vodja projektov' || x.role == 'vodja strojnikov' || x.role == 'vodja CNC obdelave' || x.role == 'vodja električarjev' || x.role == 'vodja programerjev')
                workers += `<div class="mb-1 mr-2 font-weight-bold" id="workers">`+x.name +" "+ x.surname+`</div>`;
              else if(x.role == 'admin')
                workers += `<div class="mb-1 mr-2 font-weight-bold font-italic" id="workers">`+x.name +" "+ x.surname+`</div>`;
              else
                workers += `<div class="mb-1 mr-2" id="workers">`+x.name +" "+ x.surname+`</div>`;
            }
          }
          else{
            workers = `<div class="mb-1 mr-2" id="workers">Ni dodeljeno</div>`;
          }
          var start = '/';
          var finish = '/';
          if(tasks[i].task_start)
            start = tasks[i].task_start.split("T")[0].split("-")[2] + "." + tasks[i].task_start.split("T")[0].split("-")[1] + "." + tasks[i].task_start.split("T")[0].split("-")[0];
          if(tasks[i].task_finish)
            finish = tasks[i].task_finish.split("T")[0].split("-")[2] + "." + tasks[i].task_finish.split("T")[0].split("-")[1] + "." + tasks[i].task_finish.split("T")[0].split("-")[0];
          var servis = "";
          var head = "";
          var expired = "";
          var linkClass = "list-group-item-linkable";
          var dataLink = "/projects/id?id="+tasks[i].id_project+"&taskId="+tasks[i].id;
          if(tasks[i].subscriberservis){
            dataLink = "/servis/?taskId="+tasks[i].id;
            linkClass = "list-group-item-linkable";
            servis = '<span class="badge badge-info ml-2 mt-1">Servis</span>';
            head = `<div class="d-flex w-100 justify-content-between">
              <div class="h5 mb-1 font-italic" id="projectName">`+tasks[i].subscriberservis+`</div>
            </div>`;
          }
          else if(tasks[i].project_name){
            head = `<div class="d-flex w-100 justify-content-between">
              <div class="h5 mb-1 font-italic" id="projectName">`+tasks[i].project_name+`</div>
            </div>`;
          }
          if(tasks[i].expired)
            expired = '<span class="badge badge-danger ml-2 mt-1">ZAMUDA</span>';
          var element = `<row class="list-group-item `+linkClass+` flex-column" id=`+tasks[i].id+` data-link="`+dataLink+`">
            `+head+`
            <div class="d-flex w-100 justify-content-between">
              <div class="h5 mb-1" id="taskName">`+tasks[i].task_name+`</div>
            </div>
            <div class="d-flex w-100 justify-content-start">
              <div class="h6 mb-1 mr-2" id="weekDate">`+ start +" - " + finish +`</div>
              ` + servis + expired + `
            </div>
            <div class="d-flex justify-content-start flex-wrap">
              `+workers+`
            </div>
          </row>`;
          //if(tasks[i].active)
          if(tasks[i].active_project == null || tasks[i].active_project == true)
            $('#dailyList').append(element);
          // debugger;
        }
      }
      relinkListItems();
      //if admin or leader change date info
      if(loggedUser.role.substring(0,5) == 'vodja' || loggedUser.role == 'admin'){
        if(date)
          $('#header').text("Opravila za dan: "+date.split("-")[2] + "." + date.split("-")[1] + "." + date.split("-")[0]);
        else
        $('#header').text("Opravila danes");
      }
      //calculate new hiddenScroll
      hiddenScroll = $('#dailyList').prop('scrollHeight') - $('#dailyList').height() + 15;
      $( "#dailyList" ).stop();
      $('#dailyList').animate({ 'scrollTop': '-='+$('#dailyList').scrollTop() }, 'slow');
      if(document.getElementById('customAnimationCheck').checked)
        animateList();
      //debugger;
      //same for week tasks
      $.get('/daily/week', {date, categoryTask, adminView}, function( resp ) {
        if(resp.success){
          var tasks = resp.data;
          $('#weeklyList').empty();
          for(var i = 0; i < tasks.length; i++){
            //debugger;
            if(contains.call(tasks[i].workersId, workerid)){
              //old with category and subscriber info
              /*
              var element = `<row class="list-group-item flex-column" id=`+tasks[i].id+`>
                <div class="d-flex w-100 justify-content-between">
                  <div class="h5 mb-1" id="taskName">`+tasks[i].task_name+`</div>
                  <div id="taskCategory">`+tasks[i].category+`</div>
                </div>
                <div class="d-flex justify-content-between">
                  <div class="mb-1" id="workers">`+tasks[i].workers+`</div>
                  <div id="subscribers">`+tasks[i].subscriber+`</div>
                </div>
              </row>`;
              */
             var workers = ``;
              if(tasks[i].workersId[0]){
                for(var j = 0; j < tasks[i].workersId.length; j++){
                  var x = allUsers.find(x => x.id == tasks[i].workersId[j]);
                  if(x.role == 'konstrukter' || x.role == 'konstruktor')
                    workers += `<div class="mb-1 mr-2 color-builder" id="workers">`+x.name +" "+ x.surname+`</div>`;
                  else if(x.role == 'strojnik')
                    workers += `<div class="mb-1 mr-2 color-mechanic" id="workers">`+x.name +" "+ x.surname+`</div>`;
                  else if(x.role == 'električar')
                    workers += `<div class="mb-1 mr-2 color-electrican" id="workers">`+x.name +" "+ x.surname+`</div>`;
                  else if(x.role == 'programer plc-jev')
                    workers += `<div class="mb-1 mr-2 color-plc" id="workers">`+x.name +" "+ x.surname+`</div>`;
                  else if(x.role == 'programer robotov')
                    workers += `<div class="mb-1 mr-2 color-robot" id="workers">`+x.name +" "+ x.surname+`</div>`;
                  else if(x.role == 'vodja' || x.role == 'vodja projektov' || x.role == 'vodja strojnikov' || x.role == 'vodja CNC obdelave' || x.role == 'vodja električarjev' || x.role == 'vodja programerjev')
                    workers += `<div class="mb-1 mr-2 font-weight-bold" id="workers">`+x.name +" "+ x.surname+`</div>`;
                  else if(x.role == 'admin')
                    workers += `<div class="mb-1 mr-2 font-weight-bold font-italic" id="workers">`+x.name +" "+ x.surname+`</div>`;
                  else
                    workers += `<div class="mb-1 mr-2" id="workers">`+x.name +" "+ x.surname+`</div>`;
                }
              }
              else{
                workers = `<div class="mb-1 mr-2" id="workers">Ni dodeljeno</div>`;
              }
              var start = '/';
              var finish = '/';
              if(tasks[i].task_start)
                start = tasks[i].task_start.split("T")[0].split("-")[2] + "." + tasks[i].task_start.split("T")[0].split("-")[1] + "." + tasks[i].task_start.split("T")[0].split("-")[0];
              if(tasks[i].task_finish)
                finish = tasks[i].task_finish.split("T")[0].split("-")[2] + "." + tasks[i].task_finish.split("T")[0].split("-")[1] + "." + tasks[i].task_finish.split("T")[0].split("-")[0];
              var servis = "";
              var head = "";
              var expired = "";
              var linkClass = "list-group-item-linkable";
              var dataLink = "/projects/id?id="+tasks[i].id_project+"&taskId="+tasks[i].id;
              if(tasks[i].subscriberservis){
                dataLink = "/servis/?taskId="+tasks[i].id;
                linkClass = "list-group-item-linkable";
                servis = '<span class="badge badge-info ml-2 mt-1">Servis</span>';
                head = `<div class="d-flex w-100 justify-content-between">
                  <div class="h5 mb-1 font-italic" id="projectName">`+tasks[i].subscriberservis+`</div>
                </div>`;
              }
              else if(tasks[i].project_name){
                head = `<div class="d-flex w-100 justify-content-between">
                  <div class="h5 mb-1 font-italic" id="projectName">`+tasks[i].project_name+`</div>
                </div>`;
              }
              if(tasks[i].expired)
                expired = '<span class="badge badge-danger ml-2 mt-1">ZAMUDA</span>';
              var element = `<row class="list-group-item `+linkClass+` flex-column" id=`+tasks[i].id+` data-link="`+dataLink+`">
                `+head+`
                <div class="d-flex w-100 justify-content-between">
                  <div class="h5 mb-1" id="taskName">`+tasks[i].task_name+`</div>
                </div>
                <div class="d-flex w-100 justify-content-start">
                  <div class="h6 mb-1" id="weekDate">`+ start +" - " + finish +`</div>
                  ` + servis + expired + `
                </div>
                <div class="d-flex justify-content-start flex-wrap">
                  `+workers+`
                </div>
              </row>`;
              //$('#weeklyList').append(element);
              //debugger;
              var x = dailyTasks.find(d => d.id == tasks[i].id);
              if(!x){
                if(tasks[i].active_project == null || tasks[i].active_project == true)
                  $('#weeklyList').append(element);
                //debugger;
              }
            }
          }
          relinkListItems();
          //calculate new hiddenScroll
          hiddenScrollWeek = $('#weeklyList').prop('scrollHeight') - $('#weeklyList').height() + 15;
          $( "#weeklyList" ).stop();
          $('#weeklyList').animate({ 'scrollTop': '-='+$('#weeklyList').scrollTop() }, 'slow');
          if(document.getElementById('customAnimationCheck').checked)
            animateListWeek();
          //debugger;
          //same for week tasks
          
          $('#modalWorkerTab').modal('toggle');
        }
      });
      //$('#modalWorkerTab').modal('toggle');
    }
  });
}
var contains = function(needle) {
  // Per spec, the way to identify NaN is that it is not equal to itself
  var findNaN = needle !== needle;
  var indexOf;

  if(!findNaN && typeof Array.prototype.indexOf === 'function') {
      indexOf = Array.prototype.indexOf;
  } else {
      indexOf = function(needle) {
          var i = -1, index = -1;

          for(i = 0; i < this.length; i++) {
              var item = this[i];

              if((findNaN && item !== item) || item === needle) {
                  index = i;
                  break;
              }
          }
          return index;
      };
  }
  return indexOf.call(this, needle) > -1;
};
$(document).ready(function () {
  //Increment the idle time counter every minute.
  var idleInterval = setInterval(timerIncrement, 60000); // 1 minute
  
    //Zero the idle timer on mouse movement.
    $(this).mousemove(function (e) {
      //aktivnost
      idleTime = 0;
      //hide screensaver
      //$('#screensaver').fadeOut(100);
      if(screenSaver)
        showBackground();
    });
    $(this).keypress(function (e) {
      //aktivnost
      idleTime = 0;
      //hide screensaver
      if(screenSaver)
        showBackground();
      //$('#screensaver').fadeOut(100);
    });
});

function timerIncrement() {
  //console.log(idleTime);
  idleTime = idleTime + 1;
  if (idleTime > 14) { // 15 minutes
    //window.location.reload();
    //SHOW SCREENSAVER; image of roboteh
    showimage();
    screenSaver = true;
    changeTaskTable(0);
    if(idleTime > 500){
      window.location.reload();
    }
    /*
    setTimeout(()=>{
      idleTime = 0;
    })
    */
  }
  /*
  if(idleTime > 24) { // after 25 min reload page otherwise session gets aged out
    window.location.reload();
  }
  */
}
function toggleFullScreen() {
  if (!document.fullscreenElement) {
    document.documentElement.requestFullscreen();
  } else {
    if (document.exitFullscreen) {
      document.exitFullscreen(); 
    }
  }
}
function showimage()
{
  $("body").css("background-image","url('/images/screensaver.png')"); // Onclick of button the background image of body will be test here. Give the image path in url
  $('#clickbutton').hide(); //This will hide the button specified in html
  $('#banner').hide();
  //$('#feedbackButton').hide()
  screenSaver = true;
}
function showBackground(){
  $("body").css("background-image","url('/images/qbkls.png')");
  $("body").css("background-repeat","repeat-xy");
  $('#banner').show();
  //$('#feedbackButton').show();
  changeTaskTable(0);
  screenSaver = false;
}
function relinkListItems(){
  $('.list-group-item-linkable').on('click', function() {
    // same window/tab:
    window.location.href = $(this).data('link');

    // new window/tab:
    //window.open($(this).data('link'));
  });

  $('.list-group-item-linkable a, .list-group-item-linkable button')
  .on('click', function(e) {
      e.stopPropagation();
  });
}
var loggedUser;
var categoryTask = 0;
var date = "";
var hiddenScroll;
var hiddenScrollWeek;
var selectedCategory = 0;
var adminView = false;
var allUsers;
var idleTime = 0;
var animationCheck = false;
var screenSaver = false;
var dailyTasks;
var allAbsences;