$(function(){
  $('.toast').toast();
  $('#notifySessionToast').addClass('d-none');
  // $('#notifySessionToast').toast('hide');
  /*
  $("#notificationBar").hover(
    function() {
      insideBody = 1;
      debugger
      clearInterval(intervalNotifications);
    },
    function() {
      insideBody = 2;
      debugger
      getMyActiveChanges();
      intervalNotifications = setInterval(getMyActiveChanges,5000);
    }
  );
  */
  channelRIS.addEventListener ('message', (event) => {
    // console.log('nove spremembe: ')
    // console.log(event.data);

    localStorage.lastChangesDate = new Date();
    removeSeenNotifications(event.data.deletedNotifications);
    prependChangesAsNotification(event.data.addNotifications);
    changes = event.data.changes;
   });
  activeUserRole = $('#loggedUser').html().split(" (")[1].split(")")[0].toLowerCase();
  //debugger;
  if(localStorage.getItem('bar') == "false"){
    toggleNotificationBar();
    //getMyActiveChanges();
    intervalGetChangesControl(180000);
    intervalNotifications = setInterval(intervalGetChangesControl,180000,180000);
    //testFillNotification();
  }
  else{
    //testFillNotification();
    //getMyActiveChanges();
    intervalGetChangesControl(15000);
    intervalNotifications = setInterval(intervalGetChangesControl,15000, 15000);
  }
  //event handlers
  $('#notificationBar').find('.d-flex.justify-content-between.invisble-button.fake-button').on('click', toggleNotificationBar);

  // start interval
  checkSessionAgeInterval
})
function getChangesThroughChannel(params) {
  debugger;
}
function intervalGetChangesControl(interval) {
  //first check if there is even date, if not get changes and write then in localStorage
  if(localStorage.lastChangesDate){
    let lastChangesDate = new Date(localStorage.lastChangesDate);
    let nowDate = new Date();
    lastChangesDate.setSeconds(lastChangesDate.getSeconds() + (interval/1000));
    //console.log('datum sprememb+interval: '+lastChangesDate);
    //console.log('zdaj: ' + nowDate);
    if(lastChangesDate.getTime() >= nowDate.getTime()){
      //someone else got new changes, only redraw them
      //console.log('narisi ze pridobljene spremembe');
      if(changes.length == 0){
        let tmpChanges = JSON.parse(localStorage.changes);
        changes = tmpChanges;
        //add new notifications
        prependChangesAsNotification(tmpChanges);
      }
    }
    else{
      //interval time has passed and no one got new changes, get them and write them in localStorage
      //console.log('pridobivam nove spremembe')
      getMyActiveChanges();
    }
  }
  else{
    getMyActiveChanges();
  }
}
function toggleNotificationBar(){
  $('#notificationBody').collapse('toggle');
  if(notificationBodyOpen){
    //$('#notificationButton').find('img').attr('src', '/dropup.png');
    $('#notificationButton').find('i').removeClass('fa-caret-down');
    $('#notificationButton').find('i').addClass('fa-caret-up');
    notificationBodyOpen = false;
    localStorage.setItem('bar', false);
    //$('#notificationBar').removeClass('new');
  }
  else{
    //$('#notificationButton').find('img').attr('src', '/dropdown.png');
    $('#notificationButton').find('i').removeClass('fa-caret-up');
    $('#notificationButton').find('i').addClass('fa-caret-down');
    notificationBodyOpen = true;
    localStorage.setItem('bar', true);
    $('#notificationBar').removeClass('new');
  }
}
//middle click with dismiss
function dismiss(id){
  $('#notification'+id).alert('close');
  dismissNotification(id);
}
//dismiss notification, mark as inactive
function dismissNotification(id){
  //console.log(id);
  changes = changes.filter(c => c.id != id);
  localStorage.changes = changes;
  $.post('/changes/dismiss', {id}, function(data){
    if(data.success)
      console.log("Sprememba "+id+" je označena kot videna.")
  })
}
//when user opens modal for messages app can dismiss all notification matched with same ids
function dismissMessageNotifications(){
  var dismissIds = [];
  for(var i = 0; i < changes.length; i++){
    if(messagesModalOpenForSubtask && messagesSubtaskId == changes[i].id_subtask && changes[i].type == 4){
      dismissIds.push(changes[i].id);
    }
    else if(messagesModalOpenForTask && messagesTaskId == changes[i].id_task && !changes[i].id_subtask && changes[i].type == 4){
      dismissIds.push(changes[i].id);
    }
  }
  for(var i = 0; i < dismissIds.length; i++){
    dismiss(dismissIds[i]);
  }
}
//get all unseen changes (active)
function getMyActiveChanges(num){
  localStorage.lastChangesDate = new Date();
  //console.log('getting new changes');
  let type = -1;
  $.get('/changes/get', {type}, function(data){
    if(data.success){
      var deletedNotifications = [];
      var addNotifications = [];
      if(changes.length == 0){
        changes = data.data;
        addNotifications = data.data;
      }
      else{
        //find deleted/seen notifications
        for(var i = 0; i < changes.length; i++){
          var x = data.data.find(c => c.id == changes[i].id)
          if(!x)
            deletedNotifications.push(changes[i]);
        }
        //find new notifications
        for(var i = 0; i < data.data.length; i++){
          var x = changes.find(c => c.id == data.data[i].id)
          if(!x)
            addNotifications.push(data.data[i]);
        }
      }
      //debugger;
      changes = data.data;
      localStorage.changes = JSON.stringify(changes);
      channelRIS.postMessage({deletedNotifications,addNotifications, changes});
      //delete notification that were seen or dismissed
      removeSeenNotifications(deletedNotifications);
      //add new notifications
      prependChangesAsNotification(addNotifications);
    }
  })
}
//remove seen notifications
function removeSeenNotifications(changes){
  for(var i = 0; i < changes.length; i++){
    $('#notification'+changes[i].id).alert('close');
  }
}
//add changes as notification (color style based on status)
function prependChangesAsNotification(changes){
  changes.sort(compareValues('id', 'asc'));
  //changed to append cuz i changed sql query to desc because of limiting the query to 20 because of bad/huge query
  for(var i = 0; i < changes.length; i++){
    var element = ``;
    var link = ``;
    var colorLabel;
    if(changes[i].status == 1 || changes[i].status == 6) colorLabel = 'alert-add';
    else if(changes[i].status == 2) colorLabel = 'alert-edit';
    else if(changes[i].status == 3 || changes[i].status == 5) colorLabel = 'alert-delete';
    else if(changes[i].status == 4) colorLabel = 'alert-complete';
    if(changes[i].type == 1){
      //project
      if(activeUserRole == 'admin' || activeUserRole.substring(0,5) == 'vodja')
        link = `/projects/id?id=`+changes[i].id_project+`&type=`+changes[i].type+`&status=`+changes[i].status;
      else
        link = `/employees/tasks?userId=`+changes[i].for+`&projectId=`+changes[i].id_project+`&taskId=`+changes[i].id_task;
      element = `<div class="alert alert-dismissible `+colorLabel+` notification-alert" id=notification`+changes[i].id+`>
        <button class="close btn-on-stretched" type="button" data-dismiss="alert">×</button>
        <strong>` + changes[i].user_name + ' ' + changes[i].user_surname + `</strong>
        je `+changes[i].status_label+' '+changes[i].type_label+`  
        <a class="info-link stretched-link" href="`+link+`">`+changes[i].project_name+`</a>
        .
        <div class="card-subtitle-message sender notification-date">`+new Date(changes[i].date).toLocaleString('sl')+`</div>
      </div>`;
      $('#notificationBody').prepend(element);
    }
    else if(changes[i].type == 2){
      //task
      if(activeUserRole == 'admin' || activeUserRole.substring(0,5) == 'vodja')
        link = `/projects/id?id=`+changes[i].id_project+`&taskId=`+changes[i].id_task+`&type=`+changes[i].type+`&status=`+changes[i].status;
      else
        link = `/employees/tasks?userId=`+changes[i].for+`&projectId=`+changes[i].id_project+`&taskId=`+changes[i].id_task+`&type=`+changes[i].type+`&status=`+changes[i].status;
      element = `<div class="alert alert-dismissible `+colorLabel+` notification-alert" id=notification`+changes[i].id+`>
        <button class="close btn-on-stretched" type="button" data-dismiss="alert">×</button>
        <strong>` + changes[i].user_name + ' ' + changes[i].user_surname + `</strong>
        je `+changes[i].status_label+' '+changes[i].type_label+`  
        <a class="info-link stretched-link" href="`+link+`">`+changes[i].task_name+`</a>
        na projektu <strong>`+changes[i].project_name+`</strong>.
        <div class="card-subtitle-message sender notification-date">`+new Date(changes[i].date).toLocaleString('sl')+`</div>
      </div>`;
      $('#notificationBody').prepend(element);
    }
    else if(changes[i].type == 3){
      //subtask
      if(activeUserRole == 'admin' || activeUserRole.substring(0,5) == 'vodja')
        link = `/projects/id?id=`+changes[i].id_project+`&taskId=`+changes[i].id_task+`&subtaskId=`+changes[i].id_subtask+`&type=`+changes[i].type+`&status=`+changes[i].status;
      else
        link = `/employees/tasks?userId=`+changes[i].for+`&projectId=`+changes[i].id_project+`&taskId=`+changes[i].id_task+`&subtaskId=`+changes[i].id_subtask+`&type=`+changes[i].type+`&status=`+changes[i].status;
      element = `<div class="alert alert-dismissible `+colorLabel+` notification-alert" id=notification`+changes[i].id+`>
        <button class="close btn-on-stretched" type="button" data-dismiss="alert">×</button>
        <strong>` + changes[i].user_name + ' ' + changes[i].user_surname + `</strong>
        je `+changes[i].status_label+' '+changes[i].type_label+`  
        <a class="info-link stretched-link" href="`+link+`">`+changes[i].subtask_name+`</a>
        pri opravilu <strong>`+changes[i].task_name+`</strong> 
        na projektu <strong>`+changes[i].project_name+`</strong>.
        <div class="card-subtitle-message sender notification-date">`+new Date(changes[i].date).toLocaleString('sl')+`</div>
      </div>`;
      $('#notificationBody').prepend(element);
    }
    else if(changes[i].type == 4){
      //note
      if((messagesModalOpenForTask && messagesTaskId == changes[i].id_task && !changes[i].id_subtask) || (messagesModalOpenForSubtask && messagesSubtaskId == changes[i].id_subtask)){
        //call dismiss
        //console.log("call dismiss; notification and modal ids match");
        dismissNotification(changes[i].id);
      }
      else{
        //show notification
        //console.log("show notification; notification and modal ids dont match");
        colorLabel = 'alert-note';
        if(changes[i].id_subtask){
          //subtask note
          if(activeUserRole == 'admin' || activeUserRole.substring(0,5) == 'vodja')
            link = `/projects/id?id=`+changes[i].id_project+`&taskId=`+changes[i].id_task+`&subtaskId=`+changes[i].id_subtask+`&noteId=`+changes[i].id_note+`&type=`+changes[i].type+`&status=`+changes[i].status;
          else
            link = `/employees/tasks?userId=`+changes[i].for+`&projectId=`+changes[i].id_project+`&taskId=`+changes[i].id_task+`&subtaskId=`+changes[i].id_subtask+`&noteId=`+changes[i].id_note+`&type=`+changes[i].type+`&status=`+changes[i].status;
          element = `<div class="alert alert-dismissible `+colorLabel+` notification-alert" id=notification`+changes[i].id+`>
            <button class="close btn-on-stretched" type="button" data-dismiss="alert">×</button>
            <strong>` + changes[i].user_name + ' ' + changes[i].user_surname + `</strong>
            je `+changes[i].status_label+' '+changes[i].type_label+` pri nalogi
            <a class="info-link stretched-link" href="`+link+`">`+changes[i].subtask_name+`</a>
            opravila <strong>`+changes[i].task_name+`</strong> na projektu <strong>`+changes[i].project_name+`</strong>.
            <div class="card-subtitle-message sender notification-date">`+new Date(changes[i].date).toLocaleString('sl')+`</div>
          </div>`;
          $('#notificationBody').prepend(element);
        }
        else{
          //task note
          if(activeUserRole == 'admin' || activeUserRole.substring(0,5) == 'vodja')
            link = `/projects/id?id=`+changes[i].id_project+`&taskId=`+changes[i].id_task+`&noteId=`+changes[i].id_note+`&type=`+changes[i].type+`&status=`+changes[i].status;
          else
            link = `/employees/tasks?userId=`+changes[i].for+`&projectId=`+changes[i].id_project+`&taskId=`+changes[i].id_task+`&noteId=`+changes[i].id_note+`&type=`+changes[i].type+`&status=`+changes[i].status;
          element = `<div class="alert alert-dismissible `+colorLabel+` notification-alert" id=notification`+changes[i].id+`>
            <button class="close btn-on-stretched" type="button" data-dismiss="alert">×</button>
            <strong>` + changes[i].user_name + ' ' + changes[i].user_surname + `</strong>
            je `+changes[i].status_label+' '+changes[i].type_label+` pri opravilu
            <a class="info-link stretched-link" href="`+link+`">`+changes[i].task_name+`</a>
            na projektu <strong>`+changes[i].project_name+`</strong>.
            <div class="card-subtitle-message sender notification-date">`+new Date(changes[i].date).toLocaleString('sl')+`</div>
          </div>`;
          $('#notificationBody').prepend(element);
        }
      }
    }
    else if(changes[i].type == 5){
      //absence

    }
    else if(changes[i].type == 6){
      //special note
      colorLabel = 'alert-special';
      //admin, leaders or secretaries can make special note so everyone can see it
      //probably will add another case where user can decide who to notify
      element = `<div class="alert alert-dismissible `+colorLabel+` notification-alert" id=notification`+changes[i].id+`>
        <button class="close btn-on-stretched" type="button" data-dismiss="alert">×</button>
        <strong>` + changes[i].user_name + ' ' + changes[i].user_surname + `</strong>
        je `+changes[i].status_label+` <strong>pomembno obvestilo</strong> z sporočilom: `+changes[i].note_label+`.
        <div class="card-subtitle-message sender notification-date">`+new Date(changes[i].date).toLocaleString('sl')+`</div>
      </div>`;
      $('#notificationBody').prepend(element);
    }
    else if(changes[i].type == 7){
      //file
      colorLabel = 'alert-file';
      if(changes[i].id_task){
        //task file
        link = `/projects/id?id=`+changes[i].id_project+`&taskId=`+changes[i].id_task+`&fileId=`+changes[i].id_file+`&type=`+changes[i].type+`&status=`+changes[i].status;
        element = `<div class="alert alert-dismissible `+colorLabel+` notification-alert" id=notification`+changes[i].id+`>
          <button class="close btn-on-stretched" type="button" data-dismiss="alert">×</button>
          <strong>` + changes[i].user_name + ' ' + changes[i].user_surname + `</strong>
          je `+changes[i].status_label+' '+changes[i].type_label+` pri opravilu
          <a class="info-link stretched-link" href="`+link+`">`+changes[i].task_name+`</a>
          na projektu <strong>`+changes[i].project_name+`</strong>.
          <div class="card-subtitle-message sender notification-date">`+new Date(changes[i].date).toLocaleString('sl')+`</div>
        </div>`;
        $('#notificationBody').prepend(element);
      }
      else{
        //project file
        link = `/projects/id?id=`+changes[i].id_project+`&fileId=`+changes[i].id_file+`&type=`+changes[i].type+`&status=`+changes[i].status;
        element = `<div class="alert alert-dismissible `+colorLabel+` notification-alert" id=notification`+changes[i].id+`>
          <button class="close btn-on-stretched" type="button" data-dismiss="alert">×</button>
          <strong>` + changes[i].user_name + ' ' + changes[i].user_surname + `</strong>
          je `+changes[i].status_label+' '+changes[i].type_label+` na projektu
          <a class="info-link stretched-link" href="`+link+`">`+changes[i].project_name+`</a>
          .
          <div class="card-subtitle-message sender notification-date">`+new Date(changes[i].date).toLocaleString('sl')+`</div>
        </div>`;
        $('#notificationBody').prepend(element);
      }
    }
    else if(changes[i].type == 8){
      //subscriber

    }
    else if(changes[i].type == 9){
      //servis
      if(activeUserRole == 'admin' || activeUserRole.substring(0,5) == 'vodja')
        link = `/servis/?taskId=`+changes[i].id_task+`&type=`+changes[i].type+`&status=`+changes[i].status;
      else
        link = `/employees/tasks?userId=`+changes[i].for+`&taskId=`+changes[i].id_task+`&type=`+changes[i].type+`&status=`+changes[i].status;
      element = `<div class="alert alert-dismissible `+colorLabel+` notification-alert" id=notification`+changes[i].id+`>
        <button class="close btn-on-stretched" type="button" data-dismiss="alert">×</button>
        <strong>` + changes[i].user_name + ' ' + changes[i].user_surname + `</strong>
        je `+changes[i].status_label+' '+changes[i].type_label+`  
        <a class="info-link stretched-link" href="`+link+`">`+changes[i].task_name+`</a>
        pri naročniku <strong>`+changes[i].subscriber_label+`</strong>.
        <div class="card-subtitle-message sender notification-date">`+new Date(changes[i].date).toLocaleString('sl')+`</div>
      </div>`;
      $('#notificationBody').prepend(element);
    }
    else if(changes[i].type == 10){
      // servis subtask
      if(activeUserRole == 'admin' || activeUserRole.substring(0,5) == 'vodja')
        link = `/servis/?taskId=`+changes[i].id_task+`&subtaskId=`+changes[i].id_subtask+`&type=`+changes[i].type+`&status=`+changes[i].status;
      else
        link = `/employees/tasks?userId=`+changes[i].for+`&taskId=`+changes[i].id_task+`&subtaskId=`+changes[i].id_subtask+`&type=`+changes[i].type+`&status=`+changes[i].status;
      element = `<div class="alert alert-dismissible `+colorLabel+` notification-alert" id=notification`+changes[i].id+`>
        <button class="close btn-on-stretched" type="button" data-dismiss="alert">×</button>
        <strong>` + changes[i].user_name + ' ' + changes[i].user_surname + `</strong>
        je `+changes[i].status_label+' '+changes[i].type_label+`  
        <a class="info-link stretched-link" href="`+link+`">`+changes[i].subtask_name+`</a>
        za servis <strong>`+changes[i].task_name+`</strong> 
        pri naročniku <strong>`+changes[i].subscriber_label+`</strong>.
        <div class="card-subtitle-message sender notification-date">`+new Date(changes[i].date).toLocaleString('sl')+`</div>
      </div>`;
      $('#notificationBody').prepend(element);
    }
    else if(changes[i].type == 11){
      // servis message
      if((messagesModalOpenForTask && messagesTaskId == changes[i].id_task && !changes[i].id_subtask) || (messagesModalOpenForSubtask && messagesSubtaskId == changes[i].id_subtask)){
        //call dismiss
        //console.log("call dismiss; notification and modal ids match");
        dismissNotification(changes[i].id);
      }
      else{
        //show notification
        //console.log("show notification; notification and modal ids dont match");
        colorLabel = 'alert-note';
        if(changes[i].id_subtask){
          //subtask note
          if(activeUserRole == 'admin' || activeUserRole.substring(0,5) == 'vodja')
            link = `/servis/?taskId=`+changes[i].id_task+`&subtaskId=`+changes[i].id_subtask+`&noteId=`+changes[i].id_note+`&type=`+changes[i].type+`&status=`+changes[i].status;
          else
            link = `/employees/tasks?userId=`+changes[i].for+`&taskId=`+changes[i].id_task+`&subtaskId=`+changes[i].id_subtask+`&noteId=`+changes[i].id_note+`&type=`+changes[i].type+`&status=`+changes[i].status;
          element = `<div class="alert alert-dismissible `+colorLabel+` notification-alert" id=notification`+changes[i].id+`>
            <button class="close btn-on-stretched" type="button" data-dismiss="alert">×</button>
            <strong>` + changes[i].user_name + ' ' + changes[i].user_surname + `</strong>
            je `+changes[i].status_label+' '+changes[i].type_label+` pri nalogi
            <a class="info-link stretched-link" href="`+link+`">`+changes[i].subtask_name+`</a>
            servisa <strong>`+changes[i].task_name+`</strong> pri naročniku <strong>`+changes[i].subscriber_label+`</strong>.
            <div class="card-subtitle-message sender notification-date">`+new Date(changes[i].date).toLocaleString('sl')+`</div>
          </div>`;
          $('#notificationBody').prepend(element);
        }
        else{
          //task note
          if(activeUserRole == 'admin' || activeUserRole.substring(0,5) == 'vodja')
            link = `/servis/?taskId=`+changes[i].id_task+`&noteId=`+changes[i].id_note+`&type=`+changes[i].type+`&status=`+changes[i].status;
          else
            link = `/employees/tasks?userId=`+changes[i].for+`&taskId=`+changes[i].id_task+`&noteId=`+changes[i].id_note+`&type=`+changes[i].type+`&status=`+changes[i].status;
          element = `<div class="alert alert-dismissible `+colorLabel+` notification-alert" id=notification`+changes[i].id+`>
            <button class="close btn-on-stretched" type="button" data-dismiss="alert">×</button>
            <strong>` + changes[i].user_name + ' ' + changes[i].user_surname + `</strong>
            je `+changes[i].status_label+' '+changes[i].type_label+` za servis
            <a class="info-link stretched-link" href="`+link+`">`+changes[i].task_name+`</a>
            pri naročniku <strong>`+changes[i].subscriber_label+`</strong>.
            <div class="card-subtitle-message sender notification-date">`+new Date(changes[i].date).toLocaleString('sl')+`</div>
          </div>`;
          $('#notificationBody').prepend(element);
        }
      }
    }
    else if(changes[i].type == 12){
      // servis absence
    }
    else if(changes[i].type == 13){
      // servis file
      colorLabel = 'alert-file';
      link = `/servis/?taskId=`+changes[i].id_task+`&fileId=`+changes[i].id_file+`&type=`+changes[i].type+`&status=`+changes[i].status;
      element = `<div class="alert alert-dismissible `+colorLabel+` notification-alert" id=notification`+changes[i].id+`>
        <button class="close btn-on-stretched" type="button" data-dismiss="alert">×</button>
        <strong>` + changes[i].user_name + ' ' + changes[i].user_surname + `</strong>
        je `+changes[i].status_label+' '+changes[i].type_label+` za servis
        <a class="info-link stretched-link" href="`+link+`">`+changes[i].task_name+`</a>
        pri naročniku <strong>`+changes[i].subscriber_label+`</strong>.
        <div class="card-subtitle-message sender notification-date">`+new Date(changes[i].date).toLocaleString('sl')+`</div>
      </div>`;
      $('#notificationBody').prepend(element);
    }
    else if(changes[i].type == 14){
      // user
    }
    else if(changes[i].type == 15){
      // system role
    }
    else if(changes[i].type == 16){
      // project role
    }
    else if(changes[i].type == 17){
      // password
    }
    else{
      // unknow change
      console.log("neznana sprememba");
    }
    if(notificationBodyOpen)
      minorFlashAnimation('#notification'+changes[i].id);
    //add event listeners
    if (changes[i].type == 1 || changes[i].type == 2 || changes[i].type == 3 || changes[i].type == 4 || changes[i].type == 7 || changes[i].type == 9 || changes[i].type == 10 || changes[i].type == 11 || changes[i].type == 13){
      $('#notification'+changes[i].id).find('.info-link.stretched-link').on('mouseup', {id:changes[i].id, allowMiddleButton: true}, onClickDismissNotification);
      $('#notification'+changes[i].id).find('.close.btn-on-stretched').on('mouseup', {id:changes[i].id, allowMiddleButton: false}, onClickDismissNotification);
    }
  }
  if(!notificationBodyOpen && changes.length > 0){
    flashAnimation('#notificationBar');
    if(firstNotificationFillOver)
      $('#notificationBar').addClass('new');
  }
  firstNotificationFillOver = true;
  //quickAnimation('#notificationBar');  
  //minorFlashAnimation('#notificationBar');
}
//handler for adding on events
function onClickDismissNotification(event) {
  //dismissNotification(params.data.id);
  console.log('dismiss function');
  if(event.which == 1)
    dismissNotification(event.data.id);
  else if(event.which == 2 && event.data.allowMiddleButton)
    setTimeout(() => {
      dismiss(event.data.id);
    }, 200);
}
//redirect to correct page and dismiss notification
function redirect(id){

}
function stopNotificationInterval(){
  insideBody = 1;
}
function startNotificationInterval(){
  insideBody = 2;
}

//changes for system (type: 8,14-23)
function addChangeSystem(status,type,projectId,taskId,subtaskId,workingProjectId,workingTaskId,noteId,subscriberId,absenceId,fileId,workOrderId,buildPartId,buildPhaseId,projectDateId,reportId,userId,workOrderFileId,roleId,workRoleId,meetingId,activityId,modificationId,modStatusId,compSubId,compSupId,compFileId,compStatusId,stdCompId){
  // debugger;
  $.post('/changes/add', {type,status,projectId,taskId,subtaskId,workingProjectId,workingTaskId,noteId,subscriberId,absenceId,fileId,workOrderId,buildPartId,buildPhaseId,projectDateId,reportId,userId,workOrderFileId,roleId,workRoleId,meetingId,activityId,modificationId,modStatusId,compSubId,compSupId,compFileId,compStatusId,stdCompId}, function(resp){
    if(resp.success)
      console.log("System change successfully added to database.");
    else
      console.log("System change unsuccessfully added to database.");
  })
}
///////////////////////END////////////////////////////

/////////////////////ANIMATIONS//////////////////////
function smoothScroll(selector, diff){
  if(!diff)
    diff = 0;
  $("html, body").animate({ scrollTop: $(selector).offset().top-diff }, 1000);
}
function listSmoothScroll(body, position, diff){
  if(!diff)
    diff = 0;
  $(body).animate({ scrollTop: position-diff }, 1000);
}
function anotherSmoothScroll(parent,child,diff){
  if(!diff)
    diff = 0;
  $(parent).animate({scrollTop: $(parent).scrollTop() + ($(child).offset().top - $(parent).offset().top)},1000);
}
/*
function smoothScroll (selector) {
  document.querySelector(selector).scrollIntoView({
    behavior: 'smooth'
  });
}
*/
//FLASH ANIMATION WITH FADE OUT AND IN
function flashAnimation(element){
  $(element).delay(100).fadeOut().fadeIn().fadeOut().fadeIn().fadeOut().fadeIn();
}
function minorFlashAnimation(element){
  $(element).delay(100).fadeOut().fadeIn();
}
function quickAnimation(element){
  $(element).delay(50).fadeOut(100).fadeIn(100).fadeOut(100).fadeIn(100).fadeOut(100).fadeIn(100);
}
////////////////////////END//////////////////////////
///////////////////////FUNCTIONS////////////////////////
function compareValues(key, order = 'asc') {
  return function innerSort(a, b) {
    if (!a.hasOwnProperty(key) || !b.hasOwnProperty(key)) {
      // property doesn't exist on either object
      return 0;
    }

    const varA = (typeof a[key] === 'string')
      ? a[key].toUpperCase() : a[key];
    const varB = (typeof b[key] === 'string')
      ? b[key].toUpperCase() : b[key];

    let comparison = 0;
    if (varA > varB) {
      comparison = 1;
    } else if (varA < varB) {
      comparison = -1;
    }
    return (
      (order === 'desc') ? (comparison * -1) : comparison
    );
  };
}
// FOR REDIRECTING IN CASE USER HAS NO LONGER SESSION
// the idea: when user logs in there is no localStorage.cookieMaxAge, so save it and set it 8 hours after new Date() (now)
// when time has passed, open modal window for session expired and delete localStorage.cookieMaxAge
// is user click on button inside modal or closes modal then redirect user to login page
function checkSessionAge() {
  // console.log('checking session age');
  if (!localStorage.cookieMaxAge){
    // setting time
    localStorage.cookieMaxAge = new Date().addHours(10).getTime();
  }
  else{
    // check if session is still active
    if ( new Date().getTime() >= localStorage.cookieMaxAge){
      // delete and open modal window to redirect to login page
      // console.log('delete cookieMaxAge and open modal window to redirect');
      localStorage.removeItem('cookieMaxAge');
      // maybe user already has new cookie but script does not know it, make an api call and if response is ok then fix cookie max age and leave user logged in otherwise open modal for session expired
      $.get('/apiv2/cookie', function(data){
        // debugger;
        if (data.success){
          // user has new session or session still lasts but script has wrong cookieMaxAge, fix it and do nothing
          localStorage.cookieMaxAge = data.cookieMaxAge;
        }
        else{
          // session has really expired, open modal and stop interval
          clearInterval(checkSessionAgeInterval);
          $('#modalSessionExpired').modal('toggle');
        }
      })
    }
    else if (localStorage.cookieMaxAge - new Date().getTime() < 1*60*1000){
      // session lasts for less than 5 minutes, open modal with information how much time left user has
      $('#notifyToastMsg').html('Vaša seja bo potekla čez 1 minuto. Da osvežite sejo se <a class="alert-link" href="/logout">znova prijavite</a>.')
      if (!$('#notifySessionToast').hasClass('show')){
        $('#notifySessionToast').removeClass('d-none');
        $('#notifySessionToast').toast('show');
      }
    }
    else if (localStorage.cookieMaxAge - new Date().getTime() < 2*60*1000){
      // session lasts for less than 5 minutes, open modal with information how much time left user has
      $('#notifyToastMsg').html('Vaša seja bo potekla čez 2 minuti. Da osvežite sejo se <a class="alert-link" href="/logout">znova prijavite</a>.')
      if (!$('#notifySessionToast').hasClass('show')){
        $('#notifySessionToast').removeClass('d-none');
        $('#notifySessionToast').toast('show');
      }
    }
    else if (localStorage.cookieMaxAge - new Date().getTime() < 3*60*1000){
      // session lasts for less than 5 minutes, open modal with information how much time left user has
      $('#notifyToastMsg').html('Vaša seja bo potekla čez 3 minute. Da osvežite sejo se <a class="alert-link" href="/logout">znova prijavite</a>.')
      if (!$('#notifySessionToast').hasClass('show')){
        $('#notifySessionToast').removeClass('d-none');
        $('#notifySessionToast').toast('show');
      }
    }
    else if (localStorage.cookieMaxAge - new Date().getTime() < 4*60*1000){
      // session lasts for less than 5 minutes, open modal with information how much time left user has
      $('#notifyToastMsg').html('Vaša seja bo potekla čez 4 minute. Da osvežite sejo se <a class="alert-link" href="/logout">znova prijavite</a>.')
      if (!$('#notifySessionToast').hasClass('show')){
        $('#notifySessionToast').removeClass('d-none');
        $('#notifySessionToast').toast('show');
      }
    }
    else if (localStorage.cookieMaxAge - new Date().getTime() < 5*60*1000){
      // session lasts for less than 5 minutes, open modal with information how much time left user has
      // maybe user client has wrong time
      $.get('/apiv2/cookie', function(data){
        // debugger;
        if (data.success){
          // user has new session or session still lasts but script has wrong cookieMaxAge, fix it and do nothing
          localStorage.cookieMaxAge = data.cookieMaxAge;
          if (localStorage.cookieMaxAge - new Date().getTime() < 5*60*1000){
            $('#notifyToastMsg').html('Vaša seja bo potekla čez 5 minut. Da osvežite sejo se <a class="alert-link" href="/logout">znova prijavite</a>.')
            if (!$('#notifySessionToast').hasClass('show')){
              $('#notifySessionToast').removeClass('d-none');
              $('#notifySessionToast').toast('show');
            }
          }
        }
        else{
          // session has allreally expired, open modal and stop interval
          clearInterval(checkSessionAgeInterval);
          $('#modalSessionExpired').modal('toggle');
        }
      })
    }
    // else{
    //   // do nothing?
    //   console.log('do nothing?');
    // }
  }
}
function unescapeHtml(safe) {
  return safe.replace(/&amp;/g, '&')
      .replace(/&lt;/g, '<')
      .replace(/&gt;/g, '>')
      .replace(/&quot;/g, '"')
      .replace(/&#039;/g, "'");
}
// on button btnRedirectConf click redirect user to login page
$("#btnRedirectConf").on('click', function(){
  window.location.href = "/login";
})
// on modal modalSessionExpired close redirect user to login page anyway
$('#modalSessionExpired').on('hide.bs.modal', function () {
  // console.log('modalno okno se je zaprlo');
  window.location.href = "/login";
})
// catch on logout so localStorage.cookieMaxAge can be deleted
$('#logoutLink').on('click', function(e) {
  e.preventDefault();

  localStorage.removeItem('cookieMaxAge');
  // console.log('redirect to logout')
  window.location.href = "/logout";
})
const checkSessionAgeInterval = setInterval(() => {
  // console.log('check session age');
  checkSessionAge();
}, 10000);
Date.prototype.addHours = function(h){
  this.setHours(this.getHours() + h);
  return this;
}
Date.prototype.addMinutes = function(m){
  this.setMinutes(this.getMinutes() + m);
  return this;
}
///////////////////////END////////////////////////
var notificationBodyOpen = true;
var changes = [];
var intervalNotifications;
var insideBody = 0;
var activeUserRole;
var notificationType;
var notificationStatus;
var firstNotificationFillOver = false;
//////FOR CROSS CHECK IF NOTIFICATION IDS AND MESSAGES ID ARE MATCHING/////
var messagesProjectId;
var messagesTaskId;
var messagesSubtaskId;
var messagesModalOpenForTask = false;
var messagesModalOpenForSubtask = false;
const channelRIS = new BroadcastChannel('app-data');