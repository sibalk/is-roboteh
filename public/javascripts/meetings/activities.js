//IN THIS SCRIPT//
//// get init data that is already shown on page, control collapses and modals ////
$(function(){
  var today = new Date();
  $('#activityDateNew').val(moment().format("YYYY-MM-DD"));
  if(typeof InstallTrigger !== 'undefined'){
    //firefox browser -> expand input date row inside table
    $('#activityDateNew').width('108px');
    $('#activityFirstDeadlineNew').width('108px');
    $('#activitySecondDeadlineNew').width('108px');
    $('#activityThirdDeadlineNew').width('108px');
  }
  //CONF ADD ACTIVITY - in form for req control
  $('#addActivityForm').submit(function (e) {
    e.preventDefault();
    $form = $(this);
    //debugger;
    var date = activityDateNew.value;//dont actually need it cuz now()
    var activityName = activityNameNew.value;
    var activityDeadline1 = activityFirstDeadlineNew.value + ' 15:00';
    var activityDeadline2 = (activitySecondDeadlineNew.value) ? activitySecondDeadlineNew.value + ' 15:00' : activitySecondDeadlineNew.value;
    var activityDeadline3 = (activityThirdDeadlineNew.value) ? activityThirdDeadlineNew.value + ' 15:00' : activityThirdDeadlineNew.value;
    var activityResponsibleUsers = $('#activityResponsibleNew').val();
    var activityResponsibleAll = activityRespAllNew.checked;
    var activityStatus = activityStatusNew.value;
    var activityNote = activityNoteNew.value;
    var category = selectedCategory;
    var project = selectedCategory != '0' ? activityProjectsNew.value : null;
    if(activityResponsibleUsers.length == 0) activityResponsibleAll = true;
    //POST info to route for adding new activity
    debugger;
    let data = {date, activityName, activityDeadline1, activityDeadline2, activityDeadline3, activityResponsibleUsers, activityResponsibleAll, activityStatus, activityNote, category, project};
    $.ajax({
      type: 'POST',
      url: '/meetings/activity',
      contentType: 'application/json',
      data: JSON.stringify(data), // access in body
    }).done(function (resp) {
      if(resp.success){
        debugger;
        resp.activity.responsible_users_id = (activityResponsibleAll) ? null : activityResponsibleUsers.toString();
        var responsibleUsers = '';
        resp.liable.forEach(r => { responsibleUsers += ', ' + allUsers.find(u => r.user_id == u.id).text })
        resp.activity.responsible_users = (activityResponsibleAll) ? null : responsibleUsers.substring(2);
        if(activitiesActivity == '0' || activitiesActivity == '1'){
          //allActivities.push(resp.activity);
          //meeting list element add
          var responsibleLabel = (activityResponsibleAll) ? 'VSI' : resp.activity.responsible_users;
          var noteLabel = (resp.activity.note) ? resp.activity.note : '';
          var deadline2 = (resp.activity.deadline2) ? new Date(resp.activity.deadline2).toLocaleDateString('sl') : '';
          var deadline3 = (resp.activity.deadline3) ? new Date(resp.activity.deadline3).toLocaleDateString('sl') : '';
          var statusColor = 'bg-warning';
          if(resp.activity.status_id == 1) statusColor = 'bg-danger'; else if(resp.activity.status_id == 2) statusColor = 'bg-success';
          var selectOptions = '';
          allStatus.forEach(s => {
            if(resp.activity.status_id == s.id) selectOptions += '<option value="'+s.id+'" selected="">'+s.status+'</option>';
            else selectOptions += '<option value="'+s.id+'">'+s.status+'</option>';
          })
          var classDeadLine1 = '', classDeadLine2 = '', classDeadLine3 = '';
          var today = new Date().toISOString();
          if(resp.activity.status_id != '2'){
            var today = new Date().toISOString();
            if(resp.activity.deadline3){
              if(resp.activity.deadline3 < today)
                classDeadLine3 = 'text-danger';
            }
            else if(resp.activity.deadline2){
              if(resp.activity.deadline2 < today)
                classDeadLine2 = 'text-danger';
            }
            else if(resp.activity.deadline1){
              if(resp.activity.deadline1 < today)
                classDeadLine1 = 'text-danger';
            }
          }
          //if category is not 0 then save project and category (category is already saved)
          let projectLabel = '', projectElement = '';
          if(selectedCategory != '0'){
            let tmpProject = allActiveProjects.find(p => p.id == resp.activity.project_id)
            resp.activity.project_number = tmpProject ? tmpProject.project_number : null;
            resp.activity.project_name = tmpProject ? tmpProject.project_name : null;
            if(resp.activity.project_id) projectLabel = resp.activity.project_number + ' - ' + resp.activity.project_name;
            projectElement = `<td id="activityProject` + resp.activity.id + `">` + projectLabel + `</td>`;
          }
          allActivities.push(resp.activity);
          var element = `<tr id="activity` + resp.activity.id + `">
            <td id="activityDate` + resp.activity.id + `">` + new Date(resp.activity.date).toLocaleDateString('sl') + `</td>
            ` + projectElement + `
            <td id="activityName` + resp.activity.id + `">` + resp.activity.activity + `</td>
            <td id="activityResponsible` + resp.activity.id + `">` + responsibleLabel + `</td>
            <td class="`+classDeadLine1+`" id="activityFirstDeadline` + resp.activity.id + `">` + new Date(resp.activity.deadline1).toLocaleDateString('sl') + `</td>
            <td class="`+classDeadLine2+`" id="activitySecondDeadline` + resp.activity.id + `">` + deadline2 + `</td>
            <td class="`+classDeadLine3+`" id="activityThirdDeadline` + resp.activity.id + `">` + deadline3 + `</td>
            <td class="`+statusColor+`" id="activityStatusCell` + resp.activity.id + `">
              <div class="form-group ml-auto mb-n1 w-130px">
                <select class="custom-select status-select" id="activityStatus` + resp.activity.id + `">
                  `+selectOptions+`
                </select>
              </div>
            </td>
            <td id="activityNotes` + resp.activity.id + `">` + noteLabel + `</td>
            <td id="activityButtons` + resp.activity.id + `">
              <button class="btn btn-danger ml-2 mb-n1 btn-delete" id="activityButtonDelete` + resp.activity.id + `" data-toggle="tooltip" data-original-title="Odstrani">
                <i class="fas fa-trash fa-lg"></i>
              </button>
              <button class="btn btn-primary ml-1 mb-n1 btn-edit" id="activityButtonEdit` + resp.activity.id + `" data-toggle="tooltip" data-original-title="Uredi">
                <i class="fas fa-pen fa-lg"></i>
              </button>
            </td>
          </tr>`;
          $('#activitiesTableBody').append(element);
        }
        //empty values of inputs in insert row
        $('#activityNameNew').val('');
        $('#activityResponsibleNew').val('').trigger('change');
        $('#activityFirstDeadlineNew').val('');
        $('#activitySecondDeadlineNew').val('');
        $('#activityThirdDeadlineNew').val('');
        $('#activityStatusNew').val(1);
        $('#activityNoteNew').val('');
        $('#activityRespAllNew').prop('checked', false);
        $('#activityNameNew').focus()
        //add new change
        if (selectedCategory == '0')
          addChangeSystem(1,31,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,resp.activity.id);
        else
          addChangeSystem(1,37,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,resp.activity.id);
      }
      else{
        $('#modalError').modal('toggle');
      }
    }).fail(function (resp) {
      //console.log('FAIL');
      //debugger;
      $('#modalError').modal('toggle');
    }).always(function (resp) {
      //console.log('ALWAYS');
      //debugger;
    });
  })
  //CONF EDIT ACTIVITY - (EDIT FORM)
  $('#editActivityForm').submit(function (e) {
    e.preventDefault();
    $form = $(this);
    debugger;
  })
  //AUTO RESIZE for activity note
  $("#activityNoteNew").each(function () {
    //this.setAttribute("style", "height:" + (this.scrollHeight) + "px;overflow-y:hidden;");
    this.style.height = this.scrollHeight + "px";
    this.style.overflowY = "hidden";
  }).on("input", function () {
    this.style.height = "auto";
    this.style.height = (this.scrollHeight) + "px";
  });
  //CATCH EVENTS INSIDE ACTIVITY TABLE
  $('#activitiesTableBody').on('click', 'button', function (event) {
    //debugger
    if ($(event.currentTarget).hasClass('btn-edit')){
      //debugger
      editActivity(event.currentTarget.id.substring(18));
    }
    else if ($(event.currentTarget).hasClass('btn-delete')){
      //debugger
      deleteActivity(event.currentTarget.id.substring(20));
    }
    else if ($(event.currentTarget).hasClass('btn-cancel-edit')){
      //debugger
      cancelEditActivity(event.currentTarget.id.substring(20));
    }
    else if ($(event.currentTarget).hasClass('btn-conf-edit')){
      //debugger
      editActivityConf(event.currentTarget.id.substring(22));
    }
  });
  $('#activitiesTableBody').on('change', 'select', function (event) {
    //debugger
    changeStatus(event.currentTarget.id.substring(14));
  });
  $('#activeGroupButtons').on('click', 'button', function (event) {
    // debugger
    // changeStatus(event.currentTarget.id.substring(14));
    changeActiveActivities(event.currentTarget.value);
  });
  $('#statusGroupButtons').on('click', 'button', function (event) {
    // debugger
    // changeStatus(event.currentTarget.id.substring(14));
    changeStatusActivities(event.currentTarget.value);
  });
  $('#btnDeleteActivity').on('click', deleteActivityConf);
})
function changeStatus(id){
  debugger;
  var activityStatus = $('#activityStatus'+id).val();
  var tmp = allActivities.findIndex(a => a.id == id);
  let data = {id, activityStatus};
  $.ajax({
    type: 'PUT',
    url: '/meetings/activity',
    contentType: 'application/json',
    data: JSON.stringify(data), // access in body
  }).done(function (resp) {
    if(resp.success){
      debugger;
      //correct bg color of status
      if(activityStatus == 1){
        $('#activityStatusCell'+id).removeClass('bg-success');
        $('#activityStatusCell'+id).removeClass('bg-warning');
        $('#activityStatusCell'+id).addClass('bg-danger');
      }
      else if(activityStatus == 2){
        $('#activityStatusCell'+id).removeClass('bg-danger');
        $('#activityStatusCell'+id).removeClass('bg-warning');
        $('#activityStatusCell'+id).addClass('bg-success');
      }
      else{
        $('#activityStatusCell'+id).removeClass('bg-success');
        $('#activityStatusCell'+id).removeClass('bg-danger');
        $('#activityStatusCell'+id).addClass('bg-warning');
      }
      //correct overdue color in deadlines
      $('#activityFirstDeadline'+id).removeClass('text-danger');
      $('#activitySecondDeadline'+id).removeClass('text-danger');
      $('#activityThirdDeadline'+id).removeClass('text-danger');
      if(activityStatus != '2'){
        var today = new Date().toISOString();
        if(allActivities[tmp].deadline3){
          if(allActivities[tmp].deadline3 < today)
            $('#activityThirdDeadline'+id).addClass('text-danger');
        }
        else if(allActivities[tmp].deadline2){
          if(allActivities[tmp].deadline2 < today)
            $('#activitySecondDeadline'+id).addClass('text-danger');
        }
        else if(allActivities[tmp].deadline1){
          if(allActivities[tmp].deadline1 < today)
            $('#activityFirstDeadline'+id).addClass('text-danger');
        }
      }
      //update object
      allActivities[tmp].status_id = activityStatus;
      //add system change
      if (selectedCategory == '0'){
        if (activityStatus == '1')
          addChangeSystem(5,31,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,id);
        else if (activityStatus == '2')
          addChangeSystem(4,31,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,id);
        else
          addChangeSystem(11,31,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,id);
      }
      else{
        if (activityStatus == '1')
          addChangeSystem(5,37,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,id);
        else if (activityStatus == '2')
          addChangeSystem(4,37,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,id);
        else
          addChangeSystem(11,37,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,id);
      }
    }
    else{
      //change status back
      $('#activityStatus'+id).val(allActivities[tmp].status_id);
      $('#modalError').modal('toggle');
    }
  }).fail(function (resp) {
    $('#activityStatus'+id).val(allActivities[tmp].status_id);
    //debugger;
    $('#modalError').modal('toggle');
  }).always(function (resp) {
    //debugger;
  });
}
function editActivity(id){
  editActivityId = id;
  var tmpActivity = allActivities.find(a => a.id == id);
  $('#activityDate'+id).empty();//DATE
  var disableProp = (loggedUser.role != 'admin') ? 'disabled=""' : '';
  var dateInput = `<div class="form-group ml-n2 mr-n2 mb-n2">
    <input class="form-control hide-calendar-icon" id="activityDateEdit`+id+`" type="date" name="activityDateEdit" `+ disableProp +` required="">
  </div>`;
  $('#activityDate'+id).append(dateInput);
  $('#activityDateEdit'+id).val(moment(tmpActivity.date).format("YYYY-MM-DD"));
  $('#activityName'+id).empty();//NAME
  var nameInput = `<div class="form-group ml-n2 mr-n2 mb-n2">
    <input class="form-control" id="activityNameEdit`+id+`" type="text" name="activityNameEdit" required="" autocomplete="off">
  </div>`;
  $('#activityName'+id).append(nameInput);
  $('#activityNameEdit'+id).val(tmpActivity.activity);
  if(selectedCategory != '0'){
    $('#activityProject'+id).empty();//PROJECT
    var projectInput = `<div class="form-group ml-n2 mr-n2 mb-n2">
      <select class="custom-select mr-sm-2 form-control w-100 select2-projects-edit" id="activityProjectEdit`+id+`" name="activityProjectEdit"></select>
    </div>`;
    $('#activityProject'+id).append(projectInput);
    $("#activityProjectEdit"+id).prepend('<option selected></option>').select2({
      data: allActiveProjects,
      placeholder: "Projekt",
      allowClear: true,
    });
    if(tmpActivity.project_id) $('#activityProjectEdit'+id).val(tmpActivity.project_id).trigger('change');
  }
  $('#activityResponsible'+id).empty();//RESPONSIBLE USERS
  var responsibleInput = `<div class="form-group ml-n2 mr-n2 mb-n2">
    <select class="custom-select mr-sm-2 form-control select2-users-edit w-100" id="activityResponsibleEdit`+id+`" name="activityResponsibleEdit"></select>
  </div>`;
  $('#activityResponsible'+id).append(responsibleInput);
  $(".select2-users-edit").select2({
    data: allUsers,
    multiple: true,
  });
  if(!tmpActivity.responsible_all) $('#activityResponsibleEdit'+id).val(tmpActivity.responsible_users_id.split(',')).trigger('change');
  $('#activityFirstDeadline'+id).empty();//DEADLINE 1
  var deadline1Input = `<div class="form-group ml-n2 mr-n2 mb-n2">
    <input class="form-control hide-calendar-icon w-110px" id="activityFirstDeadlineEdit`+id+`" type="date" name="activityFirstDeadlineEdit" required="">
  </div>`;
  $('#activityFirstDeadline'+id).append(deadline1Input);
  $('#activityFirstDeadlineEdit'+id).val(moment(tmpActivity.deadline1).format("YYYY-MM-DD"));
  $('#activitySecondDeadline'+id).empty();//DEADLINE 2
  var deadline2Input = `<div class="form-group ml-n2 mr-n2 mb-n2">
    <input class="form-control hide-calendar-icon w-110px" id="activitySecondDeadlineEdit`+id+`" type="date" name="activitySecondDeadlineEdit">
  </div>`;
  $('#activitySecondDeadline'+id).append(deadline2Input);
  if(tmpActivity.deadline2) $('#activitySecondDeadlineEdit'+id).val(moment(tmpActivity.deadline2).format("YYYY-MM-DD"));
  $('#activityThirdDeadline'+id).empty();//DEADLINE 3
  var deadline3Input = `<div class="form-group ml-n2 mr-n2 mb-n2">
    <input class="form-control hide-calendar-icon w-110px" id="activityThirdDeadlineEdit`+id+`" type="date" name="activityThirdDeadlineEdit">
  </div>`;
  $('#activityThirdDeadline'+id).append(deadline3Input);
  if(tmpActivity.deadline3) $('#activityThirdDeadlineEdit'+id).val(moment(tmpActivity.deadline3).format("YYYY-MM-DD"));
  $('#activityNotes'+id).empty();//NOTE
  var noteInput = `<div class="form-group ml-n2 mr-n2 mb-n2">
    <textarea class="form-control" id="activityNoteEdit`+id+`" rows="1" name="activityNoteEdit" autocomplete="off">
  </div>`;
  $('#activityNotes'+id).append(noteInput);
  // add on change resize for editing note
  if(tmpActivity.note) $('#activityNoteEdit'+id).val(tmpActivity.note);
  $("#activityNoteEdit"+id).each(function () {
    //this.setAttribute("style", "height:" + (this.scrollHeight) + "px;overflow-y:hidden;");
    this.style.height = this.scrollHeight + "px";
    this.style.overflowY = "hidden";
  }).on("input", function () {
    this.style.height = "auto";
    this.style.height = (this.scrollHeight) + "px";
  });
  $('#activityButtons'+id).empty();//BUTTONS
  var editButtons = `
  <button class="btn btn-danger ml-2 mb-n1 btn-cancel-edit" id="activityButtonCancel`+id+`" data-toggle="tooltip" data-original-title="Prekliči"><i class="fas fa-times fa-lg"></i></button>
  <button class="btn btn-success ml-2 mb-n1 btn-conf-edit" id="activityButtonEditConf`+id+`" data-toggle="tooltip" data-original-title="Potrdi"><i class="fas fa-check fa-lg"></i></button>`;
  $('#activityButtons'+id).append(editButtons);
}
function cancelEditActivity(id){
  debugger;
  var tmpActivity = allActivities.find(a => a.id == id);
  $('#activityDate'+id).empty();//DATE
  $('#activityDate'+id).html(new Date(tmpActivity.date).toLocaleDateString('sl'));
  $('#activityName'+id).empty();//NAME
  $('#activityName'+id).html(tmpActivity.activity);
  if (selectedCategory != '0'){
    $('#activityProject'+id).empty();//PROJECT
    if (tmpActivity.project_id) $('#activityProject'+id).html(tmpActivity.project_number + ' - ' + tmpActivity.project_name); else $('#activityProject'+id).html('');
  }
  $('#activityResponsible'+id).empty();//RESPONSIBLE USERS
  if (tmpActivity.responsible_all) $('#activityResponsible'+id).html('VSI'); else $('#activityResponsible'+id).html(tmpActivity.responsible_users);
  $('#activityFirstDeadline'+id).empty();//DEADLINE 1
  $('#activityFirstDeadline'+id).html(new Date(tmpActivity.deadline1).toLocaleDateString('sl'));
  $('#activitySecondDeadline'+id).empty();//DEADLINE 2
  if (tmpActivity.deadline2) $('#activitySecondDeadline'+id).html(new Date(tmpActivity.deadline2).toLocaleDateString('sl')); else $('#activitySecondDeadline'+id).html('');
  $('#activityThirdDeadline'+id).empty();//DEADLINE 3
  if (tmpActivity.deadline3) $('#activityThirdDeadline'+id).html(new Date(tmpActivity.deadline3).toLocaleDateString('sl')); else $('#activityThirdDeadline'+id).html('');
  $('#activityNotes'+id).empty();//NOTE
  if (tmpActivity.note) $('#activityNotes'+id).html(tmpActivity.note); else $('#activityNotes'+id).html('');
  $('#activityButtons'+id).empty();//BUTTONS
  var activityButtons = `
  <button class="btn btn-danger ml-2 mb-n1 btn-delete" id="activityButtonDelete` + id + `" data-toggle="tooltip" data-original-title="Odstrani"><i class="fas fa-trash fa-lg"></i></button>
  <button class="btn btn-primary ml-1 mb-n1 btn-edit" id="activityButtonEdit` + id + `" data-toggle="tooltip" data-original-title="Uredi"><i class="fas fa-pen fa-lg"></i></button>`;
  $('#activityButtons'+id).append(activityButtons);
}
function editActivityConf(id){
  var tmpActivity = allActivities.find(a => a.id == id);
  var date = ($('#activityDateEdit'+id).val()) ? $('#activityDateEdit'+id).val() + ' ' + new Date(tmpActivity.date).toLocaleTimeString('sl').substring(0,5) : $('#activityDateEdit'+id).val();//time problem on change 
  var activityName = $('#activityNameEdit'+id).val();
  var activityDeadline1 = ($('#activityFirstDeadlineEdit'+id).val()) ? $('#activityFirstDeadlineEdit'+id).val() + ' 15:00' : $('#activityFirstDeadlineEdit'+id).val();
  var activityDeadline2 = ($('#activitySecondDeadlineEdit'+id).val()) ? $('#activitySecondDeadlineEdit'+id).val() + ' 15:00' : $('#activitySecondDeadlineEdit'+id).val();
  var activityDeadline3 = ($('#activityThirdDeadlineEdit'+id).val()) ? $('#activityThirdDeadlineEdit'+id).val() + ' 15:00' : $('#activityThirdDeadlineEdit'+id).val();
  var activityResponsibleUsers = $('#activityResponsibleEdit'+id).val();
  var activityResponsibleAll = ($('#activityResponsibleEdit'+id).val().length > 0) ? false : true;
  var activityNote = $('#activityNoteEdit'+id).val();
  let activityStatus = $('#activityStatus'+id).val();
  var project = selectedCategory != '0' ? $('#activityProjectEdit'+id).val() : null;
  debugger;
  if(!date || !activityName || !activityDeadline1){
    //missing req inputs
    $('#msgImg').attr('src','/warning_sign.png');
    $('#msg').html('Manjkajoči podatki! Prosimo vnesite vse potrebne podatke (datum, naziv aktivnosti in datum 1. roka) za uspešno posodobitev aktivnosti.');
    $('#modalMsg').modal('toggle');
  }
  else{
    //send ajax put method on server
    //prepare add and delete responsible users -> if responsible all is true then old resp users are delete users
    let addRespUsers = [];
    let deleteRespUsers = [];
    if(activityResponsibleAll){
      if(tmpActivity.responsible_users_id)
        deleteRespUsers = tmpActivity.responsible_users_id.split(',');
    }
    else{
      var oldRespUsers = (tmpActivity.responsible_users_id) ? tmpActivity.responsible_users_id.split(',') : [];
      oldRespUsers = oldRespUsers.map(x => parseInt(x));
      var newRespUsers = activityResponsibleUsers;
      newRespUsers = newRespUsers.map(x => parseInt(x));
      addRespUsers = newRespUsers.filter(x => !oldRespUsers.includes(x));
      deleteRespUsers = oldRespUsers.filter(x => !newRespUsers.includes(x));
    }
    debugger
    let data = {id, date, activityName, activityDeadline1, activityDeadline2, activityDeadline3, activityResponsibleAll, activityNote, addRespUsers, deleteRespUsers, project};
    $.ajax({
      type: 'PUT',
      url: '/meetings/activity',
      contentType: 'application/json',
      data: JSON.stringify(data), // access in body
    }).done(function (resp) {
      if(resp.success){
        debugger;
        tmpActivity = resp.activity;
        if(tmpActivity.responsible_all){
          tmpActivity.responsible_users = null;
          tmpActivity.responsible_users_id = null;
        }
        else{
          tmpActivity.responsible_users_id = activityResponsibleUsers.toString();
          var respUsers = '';
          activityResponsibleUsers.forEach(r => { respUsers += ', ' + allUsers.find(u => r == u.id).text });
          tmpActivity.responsible_users = respUsers.substring(2);
        }
        //if category is not 0 then save project
        let projectLabel = '';
        if(selectedCategory != '0'){
          let tmpProject = allActiveProjects.find(p => p.id == resp.activity.project_id)
          resp.activity.project_number = tmpProject ? tmpProject.project_number : null;
          resp.activity.project_name = tmpProject ? tmpProject.project_name : null;
          if(resp.activity.project_id) projectLabel = resp.activity.project_number + ' - ' + resp.activity.project_name;
          $('#activityProject'+id).empty();//PROJECT
          if (tmpActivity.project_id) $('#activityProject'+id).html(tmpActivity.project_number + ' - ' + tmpActivity.project_name); else $('#activityProject'+id).html('');
        }
        debugger;
        $('#activityDate'+id).empty();//DATE
        $('#activityDate'+id).html(new Date(tmpActivity.date).toLocaleDateString('sl'));
        $('#activityName'+id).empty();//NAME
        $('#activityName'+id).html(tmpActivity.activity);
        $('#activityResponsible'+id).empty();//RESPONSIBLE USERS
        if(tmpActivity.responsible_all) $('#activityResponsible'+id).html('VSI'); else $('#activityResponsible'+id).html(tmpActivity.responsible_users);
        $('#activityFirstDeadline'+id).empty();//DEADLINE 1
        $('#activityFirstDeadline'+id).html(new Date(tmpActivity.deadline1).toLocaleDateString('sl'));
        $('#activitySecondDeadline'+id).empty();//DEADLINE 2
        if(tmpActivity.deadline2) $('#activitySecondDeadline'+id).html(new Date(tmpActivity.deadline2).toLocaleDateString('sl')); else $('#activitySecondDeadline'+id).html('');
        $('#activityThirdDeadline'+id).empty();//DEADLINE 3
        if(tmpActivity.deadline3) $('#activityThirdDeadline'+id).html(new Date(tmpActivity.deadline3).toLocaleDateString('sl')); else $('#activityThirdDeadline'+id).html('');
        $('#activityNotes'+id).empty();//NOTE
        if(tmpActivity.note) $('#activityNotes'+id).html(tmpActivity.note); else $('#activityNotes'+id).html('');
        $('#activityButtons'+id).empty();//BUTTONS
        var activityButtons = `
        <button class="btn btn-danger ml-2 mb-n1 btn-delete" id="activityButtonDelete` + id + `" data-toggle="tooltip" data-original-title="Odstrani"><i class="fas fa-trash fa-lg"></i></button>
        <button class="btn btn-primary ml-1 mb-n1 btn-edit" id="activityButtonEdit` + id + `" data-toggle="tooltip" data-original-title="Uredi"><i class="fas fa-pen fa-lg"></i></button>`;
        $('#activityButtons'+id).append(activityButtons);

        var tmp = allActivities.findIndex(a => a.id == id);
        allActivities[tmp] = tmpActivity;
        //correct overdue color in deadlines
        $('#activityFirstDeadline'+id).removeClass('text-danger');
        $('#activitySecondDeadline'+id).removeClass('text-danger');
        $('#activityThirdDeadline'+id).removeClass('text-danger');
        if(activityStatus != '2'){
          var today = new Date().toISOString();
          if(allActivities[tmp].deadline3){
            if(allActivities[tmp].deadline3 < today)
              $('#activityThirdDeadline'+id).addClass('text-danger');
          }
          else if(allActivities[tmp].deadline2){
            if(allActivities[tmp].deadline2 < today)
              $('#activitySecondDeadline'+id).addClass('text-danger');
          }
          else if(allActivities[tmp].deadline1){
            if(allActivities[tmp].deadline1 < today)
              $('#activityFirstDeadline'+id).addClass('text-danger');
          }
        }
        //add new change
        if (selectedCategory == '0')
          addChangeSystem(2,31,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,id);
        else
          addChangeSystem(2,37,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,id);
      }
      else{
        $('#modalError').modal('toggle');
      }
    }).fail(function (resp) {
      //debugger;
      $('#modalError').modal('toggle');
    }).always(function (resp) {
      //debugger;
    });
  }
}
function deleteActivity(id){
  activityId = id;
  var tmpActivity = allActivities.find(m => m.id == activityId);
  if(tmpActivity.active == true){
    $('#deleteActivityModalMsg').html('Ste prepričani, da želite odstraniti to aktivnost?');
    $('#btnDeleteActivity').html('Odstrani');
  }
  else{
    $('#deleteActivityModalMsg').html('Ste prepričani, da želite ponovno dodati to aktivnost?');
    $('#btnDeleteActivity').html('Ponovno dodaj');
  }
  $('#modalDeleteActivity').modal('toggle');
}
function deleteActivityConf(){
  var tmpActivity = allActivities.find(a => a.id == activityId);
  var active =  (tmpActivity.active == true) ? false : true; // IF current active is true THEN false ELSE true
  
  let data = {activityId, active};
  $.ajax({
    type: 'DELETE',
    url: '/meetings/activity',
    contentType: 'application/json',
    data: JSON.stringify(data), // access in body
  }).done(function (resp) {
    //console.log('SUCCESS');
    debugger;
    if(resp.success){
      if(loggedUser.role == 'admin'){
        //marked new active on activity table and update variables //DEPENDS ON WHICH ACTIVITY TABEL IS SHOWN
        if(activitiesActivity == '0'){
          //true and false, all activities are shown, update variables and  class & icon on table
          tmpActivity.active = active;
          if(active){
            $('#activity'+activityId).removeClass('bg-secondary');
            $('#activityButtonDelete'+activityId).find('i').removeClass('fa-trash-restore');
            $('#activityButtonDelete'+activityId).find('i').addClass('fa-trash');
          }
          else{
            $('#activity'+activityId).addClass('bg-secondary');
            $('#activityButtonDelete'+activityId).find('i').removeClass('fa-trash');
            $('#activityButtonDelete'+activityId).find('i').addClass('fa-trash-restore');
          }
        }
        else if(activitiesActivity == '1' || activitiesActivity == '2'){
          //true, only active activities are shown, same as any other role user // same for false, only non active activities are shown
          allActivities = allActivities.filter(a => a.id != activityId);
          $('#activity'+activityId).remove();
        }
      }
      else{
        //remove deleted activity from activity table and remove activity from variables
        allActivities = allActivities.filter(a => a.id != activityId);
        $('#activity'+activityId).remove();
      }
      //add new change
      if (selectedCategory == '0'){
        if(active) addChangeSystem(6,31,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,activityId);
        else addChangeSystem(3,31,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,activityId);
      }
      else{
        if(active) addChangeSystem(6,37,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,activityId);
        else addChangeSystem(3,37,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,activityId);
      }

      $('#modalDeleteActivity').modal('toggle');
    }
    else{
      $('#modalDeleteActivity').modal('toggle');
      $('#modalError').modal('toggle');
    }
  }).fail(function (resp) {
    //console.log('FAIL');
    //debugger;
    $('#modalError').modal('toggle');
  }).always(function (resp) {
    //console.log('ALWAYS');
    //debugger;
  });
}
function changeStatusActivities(status){
  activitiesStatus = status;
  let data = {meetingsActivity, activitiesActivity, activitiesStatus, selectedCategory};
  $.ajax({
    type: 'GET',
    url: '/meetings/info',
    contentType: 'application/x-www-form-urlencoded',
    data: data, // access in body
  }).done(function (resp) {
    //console.log('SUCCESS');
    debugger;
    if(resp.success){
      allActivities = resp.activities;
      fillActivitiesTable();
      $('#optionStatus0').removeClass('active stay-focus');
      $('#optionStatus1').removeClass('active stay-focus');
      $('#optionStatus2').removeClass('active stay-focus');
      $('#optionStatus3').removeClass('active stay-focus');
      if(status == 0) $('#optionStatus0').addClass('active stay-focus');
      else if(status == 1) $('#optionStatus1').addClass('active stay-focus');
      else if(status == 2) $('#optionStatus2').addClass('active stay-focus');
      else if(status == 3) $('#optionStatus3').addClass('active stay-focus');
      fixPDFLink()
    }
    else{
      
      $('#modalError').modal('toggle');  
    }
  }).fail(function (resp) {
    //console.log('FAIL');
    //debugger;
    $('#modalError').modal('toggle');
  }).always(function (resp) {
    //console.log('ALWAYS');
    //debugger;
  });
}
function changeActiveActivities(activity){
  activitiesActivity = activity;
  let data = {meetingsActivity, activitiesActivity, activitiesStatus, selectedCategory};
  $.ajax({
    type: 'GET',
    url: '/meetings/info',
    contentType: 'application/x-www-form-urlencoded',
    data: data, // access in body
  }).done(function (resp) {
    //console.log('SUCCESS');
    debugger;
    if(resp.success){
      allActivities = resp.activities;
      fillActivitiesTable();
      $('#optionAllActivities').removeClass('active stay-focus');
      $('#optionTrueActivities').removeClass('active stay-focus');
      $('#optionFalseActivities').removeClass('active stay-focus');
      if(activity == 0) $('#optionAllActivities').addClass('active stay-focus');
      else if(activity == 1) $('#optionTrueActivities').addClass('active stay-focus');
      else if(activity == 2) $('#optionFalseActivities').addClass('active stay-focus');
      fixPDFLink();
    }
    else{
      
      $('#modalError').modal('toggle');  
    }
  }).fail(function (resp) {
    //console.log('FAIL');
    //debugger;
    $('#modalError').modal('toggle');
  }).always(function (resp) {
    //console.log('ALWAYS');
    //debugger;
  });
}
function fillActivitiesTable(){
  $('#activitiesTableBody').empty()
  allActivities.forEach(a => {
    var date = new Date(a.date);
    var deadline1 = (a.deadline1) ? new Date(a.deadline1).toLocaleDateString('sl') : ' ';
    var deadline2 = (a.deadline2) ? new Date(a.deadline2).toLocaleDateString('sl') : ' ';
    var deadline3 = (a.deadline3) ? new Date(a.deadline3).toLocaleDateString('sl') : ' ';
    var responsibleLabel = (a.responsible_all) ? 'VSI' : a.responsible_users;
    var noteLabel = (a.note) ? a.note : ' ';
    var activeLabel = (a.active) ? '' : ' bg-secondary';
    var deleteIcon = (a.active) ? 'trash' : 'trash-restore';
    var selectOptions = '';
    var disabledProp = (loggedUser.role == 'admin' || loggedUser.role.substring(0,5) == 'vodja') ? '' : ' disabled';
    allStatus.forEach(s => {
      if(a.status_id == s.id) selectOptions += '<option value="'+s.id+'" selected="">'+s.status+'</option>';
      else selectOptions += '<option value="'+s.id+'">'+s.status+'</option>';
    })
    var statusColor = 'bg-warning';
    if(a.status_id == 1) statusColor = 'bg-danger'; else if(a.status_id == 2) statusColor = 'bg-success';
    var classDeadLine1 = '', classDeadLine2 = '', classDeadLine3 = '';
    var today = new Date().toISOString();
    if(a.status_id != '2'){
      var today = new Date().toISOString();
      if(a.deadline3){
        if(a.deadline3 < today)
          classDeadLine3 = 'text-danger';
      }
      else if(a.deadline2){
        if(a.deadline2 < today)
          classDeadLine2 = 'text-danger';
      }
      else if(a.deadline1){
        if(a.deadline1 < today)
          classDeadLine1 = 'text-danger';
      }
    }
    var buttonsElem = '';
    if(loggedUser.role == 'admin' || loggedUser.role.substring(0,5) == 'vodja'){
      buttonsElem = `<button class="btn btn-danger ml-2 mb-n1 btn-delete" id="activityButtonDelete` + a.id + `" data-toggle="tooltip" data-original-title="Odstrani">
        <i class="fas fa-`+deleteIcon+` fa-lg"></i>
      </button>
      <button class="btn btn-primary ml-1 mb-n1 btn-edit" id="activityButtonEdit` + a.id + `" data-toggle="tooltip" data-original-title="Uredi">
        <i class="fas fa-pen fa-lg"></i>
      </button>`;
    }
    let projectLabel = '', projectElement = '';
    if(selectedCategory != '0'){
      if(a.project_id) projectLabel = a.project_number + ' - ' + a.project_name;
      projectElement = `<td id="activityProject` + a.id + `">` + projectLabel + `</td>`;
    }
    var element = `<tr id="activity` + a.id + `" class="` + activeLabel + `">
      <td id="activityDate` + a.id + `">` + date.toLocaleDateString('sl') + `</td>
      ` + projectElement + `
      <td id="activityName` + a.id + `">` + a.activity + `</td>
      <td id="activityResponsible` + a.id + `">` + responsibleLabel + `</td>
      <td class="`+classDeadLine1+`" id="activityFirstDeadline` + a.id + `">` + deadline1 + `</td>
      <td class="`+classDeadLine2+`" id="activitySecondDeadline` + a.id + `">` + deadline2 + `</td>
      <td class="`+classDeadLine3+`" id="activityThirdDeadline` + a.id + `">` + deadline3 + `</td>
      <td class="`+statusColor+`" id="activityStatusCell` + a.id + `">
        <div class="form-group ml-auto mb-n1 w-130px">
          <select class="custom-select status-select" id="activityStatus` + a.id + `"`+disabledProp+`>
            `+selectOptions+`
          </select>
        </div>
      </td>
      <td id="activityNotes` + a.id + `">` + noteLabel + `</td>
      <td id="activityButtons` + a.id + `">
        `+buttonsElem+`
      </td>
    </tr>`;
    $('#activitiesTableBody').append(element);
  })
}
var activityId;
var editActivityId;
var activitiesActivity = 1;
var activitiesStatus = 0;