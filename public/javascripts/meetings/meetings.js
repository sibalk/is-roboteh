$(function(){
  //CONF ADD MEETING - in form for req control (was addMeetingConf())
  $('#addMeetingForm').submit(function (e) {
    e.preventDefault();
    $form = $(this);
    //debugger;
    var date = $('#meetingDateAdd').val();
    var time = $('#meetingTimeAdd').val();
    var present = [];
    var tmp = document.querySelectorAll('input[name=present-checkbox]:checked');
    tmp.forEach(t => present.push(t.id.substring(4)));
    present = present.toString();
    //POST info to route for adding new meeting
    //debugger;
    let data = {date: date, time: time, present: present, category: selectedCategory};
    $.ajax({
      type: 'POST',
      url: '/meetings/',
      contentType: 'application/json',
      data: JSON.stringify(data), // access in body
    }).done(function (resp) {
      if(resp.success){
        //debugger;
        resp.meeting.present_users_id = present;
        var presentUsers = '';
        resp.presence.forEach(p => { presentUsers += ', ' + allUsers.find(u => p.user_id == u.id).text })
        resp.meeting.present_users = presentUsers.substring(2);
        resp.meeting.counter = allMeetings.length + 1;
        if(meetingsActivity == '0' || meetingsActivity == '1'){

          allMeetings.push(resp.meeting);
          allActiveMeetings.push(resp.meeting);
          //add remove disable status selects
          //meeting list element add
          var element = `<div class="list-group-item" id="meeting`+resp.meeting.id+`">
            <div class="row">
              <div class="col-md-2">
                <div id="meetingNumber`+resp.meeting.id+`">`+resp.meeting.counter+`. sestanek</div>
              </div>
              <div class="col-md-2">
                <div id="meetingDate`+resp.meeting.id+`">Datum: `+new Date(date).toLocaleDateString('sl')+`</div>
              </div>
              <div class="col-md-2">
                <div id="meetingStart`+resp.meeting.id+`">Začetek: `+time+`</div>
              </div>
              <div class="col-md-2">
                <div id="meetingFinish`+resp.meeting.id+`">Konec:</div>
              </div>
              <div class="col-md-4 d-flex">
                <div class="ml-auto mb-1 mt-n1" id="meetingButtons`+resp.meeting.id+`">
                  <button class="btn btn-danger ml-2 mb-n1 btn-delete" id="meetingButtonDelete`+resp.meeting.id+`" data-toggle="tooltip" data-original-title="Odstrani">
                    <i class="fas fa-trash fa-lg"></i>
                  </button>
                  <button class="btn btn-primary ml-2 mb-n1 btn-edit" id="meetingButtonEdit`+resp.meeting.id+`" data-toggle="tooltip" data-original-title="Uredi">
                    <i class="fas fa-pen fa-lg"></i>
                  </button>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-md-12 d-flex">
                <div id="meetingPresence`+resp.meeting.id+`">Prisotni: ` + resp.meeting.present_users + `</div>
              </div>
            </div>
            <div class="collapse multi-collapse task-collapse" id="collapseEditMeeting`+resp.meeting.id+`"></div>
          </div>`;
          $('#meetingsList').append(element);
          //active meeting element add
          var activeMeetingElement = `<option value=` + resp.meeting.id + ` id="activeMeeting` + resp.meeting.id + `">` + resp.meeting.counter + `. sestanek ` + time + `</option>`;
          $('#activeMeetingSelect').append(activeMeetingElement);
          $('#activeMeetingSelect').val(resp.meeting.id);
          //enable adding new activities and finishing meetings since now meeting exist
          $('#addActivityBtn').prop('disabled', false);
          $('#finishMeetingBtn').prop('disabled', false);
          $('.status-select').prop('disabled', false);
          //add new change
          if (selectedCategory == '0')
            addChangeSystem(1,30,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,resp.meeting.id);
          else
            addChangeSystem(1,36,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,resp.meeting.id);
        }

        $('#modalAddMeeting').modal('toggle');
      }
      else{
        $('#modalAddMeeting').modal('toggle');
        $('#modalError').modal('toggle');
      }
    }).fail(function (resp) {
      //console.log('FAIL');
      //debugger;
      $('#modalAddMeeting').modal('toggle');
      $('#modalError').modal('toggle');
    }).always(function (resp) {
      //console.log('ALWAYS');
      //debugger;
    });
  })
  //CONF EDIT MEETING - in form for req control (editMeetingConf())
  $('#editMeetingForm').submit(function (e) {
    e.preventDefault();
    $form = $(this);
    //debugger;
    var date = $('#meetingDateEdit').val() + ' ' + $('#meetingTimeEdit').val();
    let timeLabel = ' ' + $('#meetingTimeEdit').val();
    var finished;
    if($('#meetingFinishDateEdit').val() && $('#meetingFinishTimeEdit').val())
      finished = $('#meetingFinishDateEdit').val() + ' ' + $('#meetingFinishTimeEdit').val();
    var present = [];
    var tmp = document.querySelectorAll('input[name=present-checkbox-edit]:checked');
    tmp.forEach(t => present.push(t.id.substring(8)));
    var tmpMeeting = allMeetings.find(m => m.id == meetingId);
    var currentPresent = tmpMeeting.present_users_id.split(',');
    present = present.map(x => parseInt(x));
    currentPresent = currentPresent.map(x => parseInt(x));
    var addPresence = present.filter(x => !currentPresent.includes(x));
    var deletePresence = currentPresent.filter(x => !present.includes(x));
    //present = present.toString();
    debugger;
    //POST info to route for adding new meeting
    //debugger;
    let data = {meetingId, date, finished, present, addPresence, deletePresence};
    $.ajax({
      type: 'PUT',
      url: '/meetings/',
      contentType: 'application/json',
      data: JSON.stringify(data), // access in body
    }).done(function (resp) {
      if(resp.success){
        debugger;
        tmpMeeting.present_users_id = present.toString();
        var presentUsers = '';
        present.forEach(p => { presentUsers += ', ' + allUsers.find(u => p == u.id).text })
        tmpMeeting.present_users = presentUsers.substring(2);
        tmpMeeting.date = resp.meeting.date;
        tmpMeeting.finished = resp.meeting.finished;
        date = new Date(tmpMeeting.date);
        $('#meetingDate'+meetingId).html('Datum: ' + date.toLocaleDateString('sl'));
        $('#meetingStart'+meetingId).html('Začetek: ' + date.toLocaleTimeString('sl').substring(0,5));
        if(tmpMeeting.finished){
          finished = new Date(tmpMeeting.finished);
          //$('#meetingFinishDate'+meetingId).html('Datum: ' + finished.toLocaleDateString('sl'));
          $('#meetingFinish'+meetingId).html('Konec: ' + finished.toLocaleDateString('sl') + ' ob ' + finished.toLocaleTimeString('sl').substring(0,5));
          allActiveMeetings = allActiveMeetings.filter(m => m.id != meetingId);
          $('#activeMeeting'+meetingId).remove();
          if(allActiveMeetings.length == 0){
            $('#addActivityBtn').prop('disabled', true);
            $('#finishMeetingBtn').prop('disabled', true);
            //add disable all status selects
            $('.status-select').prop('disabled', true);
          }
        }
        else{
          if(!allActiveMeetings.find(m => m.id == meetingId)){
            allActiveMeetings.push(tmpMeeting);
            var activeMeetingElement = `<option value=` + meetingId + ` id="activeMeeting` + meetingId + `">` + tmpMeeting.counter + `. sestanek` + timeLabel + `</option>`;
            $('#activeMeetingSelect').append(activeMeetingElement);
            $('#addActivityBtn').prop('disabled', false);
            $('#finishMeetingBtn').prop('disabled', false);
            $('.status-select').prop('disabled', false);
          }
          else{
            var tmpActiveMeeting = allActiveMeetings.find(m => m.id == meetingId);
            tmpActiveMeeting = tmpMeeting;
            $('#activeMeeting'+meetingId).html(tmpActiveMeeting.counter+'. sestanek' + timeLabel);
          }
          $('#meetingFinish'+meetingId).html('Konec: ');
        }
        $('#meetingPresence'+meetingId).html('Prisotni: ' + tmpMeeting.present_users);
        //add new change
        if (selectedCategory == '0')
          addChangeSystem(2,30,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,tmpMeeting.id);
        else
          addChangeSystem(2,36,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,tmpMeeting.id);
        $('#modalEditMeeting').modal('toggle');
      }
      else{
        debugger;
        $('#modalEditMeeting').modal('toggle');
        $('#modalError').modal('toggle');
      }
    }).fail(function (resp) {
      //console.log('FAIL');
      debugger;
      $('#modalEditMeeting').modal('toggle');
      $('#modalError').modal('toggle');
    }).always(function (resp) {
      //console.log('ALWAYS');
      //debugger;
    });
  })
  //CATCH EVENTS INSIDE ACTIVITY TABLE
  $('#meetingsList').on('click', 'button', function (event) {
    //debugger
    if ($(event.currentTarget).hasClass('btn-edit')){
      //debugger
      editMeeting(event.currentTarget.id.substring(17));
    }
    else if ($(event.currentTarget).hasClass('btn-delete')){
      //debugger
      deleteMeeting(event.currentTarget.id.substring(19));
    }
  });
  $('#activeMeetingGroupButtons').on('click', 'button', function (event) {
    // debugger
    // changeStatus(event.currentTarget.id.substring(14));
    changeActiveMeetings(event.currentTarget.value);
  });
  $('#beginMeetingBtn').on('click', addMeeting);
  $('#finishMeetingBtn').on('click', finishMeeting);
  $('#btnNewMeeting').on('click', addMeeting);
  $('#btnDeleteMeeting').on('click', deleteMeetingConf);
})
function addMeeting(){
  $('#modalAddMeeting').modal('toggle');
  var today = new Date();
  $('#meetingTimeAdd').val(today.toLocaleTimeString('sl').substring(0,5));
  //select users based on their role and type of meeting
  $("input[name='present-checkbox']").prop("checked", false);
  $(".checkbox-director").prop("checked", true);
  $(".checkbox-project-leader").prop("checked", true);
  if (selectedCategory == '0')
    $(".checkbox-leader").prop("checked", true);
  else{
    if (selectedCategory == '1') console.log('Trenutno ni predpostavljene vloge za to kagorijo');
    else if(selectedCategory == '2') console.log('Trenutno ni predpostavljene vloge za to kagorijo');
    else if(selectedCategory == '3') $(".checkbox-builder").prop("checked", true);
    else if(selectedCategory == '4') { $(".checkbox-mechanic").prop("checked", true); $(".checkbox-mech-assembly").prop("checked", true); $(".checkbox-cnc-operator").prop("checked", true); $(".checkbox-welders").prop("checked", true); $(".checkbox-serviser").prop("checked", true);}
    else if(selectedCategory == '5') $(".checkbox-electrican").prop("checked", true);
    else if(selectedCategory == '6') console.log('Trenutno ni predpostavljene vloge za to kagorijo');
    else if(selectedCategory == '7') $(".checkbox-programmer").prop("checked", true);
  }
}
function finishMeeting(){
  var meetingId = $('#activeMeetingSelect').val();

  let data = {meetingId};

  $.ajax({
    type: 'PUT',
    url: '/meetings/',
    contentType: 'application/json',
    data: JSON.stringify(data), // access in body
  }).done(function (resp) {
    //console.log('SUCCESS');
    debugger;
    //update variables and elements on page
    var tmpMeeting = allMeetings.find(m => m.id == meetingId);
    tmpMeeting.finished = resp.meeting.finished;
    var finished = new Date(resp.meeting.finished);
    $('#meetingFinish'+meetingId).html('Konec: ' + finished.toLocaleDateString('sl') + ' ob ' + finished.toLocaleTimeString('sl').substring(0,5));
    allActiveMeetings = allActiveMeetings.filter(m => m.id != meetingId);
    $('#activeMeeting'+meetingId).remove();
    //button correction -> if no active meeting then user can not add new activity
    if(allActiveMeetings.length == 0){
      $('#addActivityBtn').prop('disabled', true);
      $('#finishMeetingBtn').prop('disabled', true);
      //add disable all status selects
      $('.status-select').prop('disabled', true);
    }
    else{
      $('.status-select').prop('disabled', false);
    }
    //add new change
    if (selectedCategory == '0')
      addChangeSystem(4,30,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,meetingId);
    else
      addChangeSystem(4,36,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,meetingId);
  }).fail(function (resp) {
    //console.log('FAIL');
    //debugger;
    $('#modalError').modal('toggle');
  }).always(function (resp) {
    //console.log('ALWAYS');
    //debugger;
  });
}
function editMeeting(id){
  meetingId = id;
  var tmpMeeting = allMeetings.find(m => m.id == id);
  var date = new Date(tmpMeeting.date);
  $('#meetingDateEdit').val(moment(tmpMeeting.date).format("YYYY-MM-DD"));
  $('#meetingTimeEdit').val(date.toLocaleTimeString('sl').substring(0,5));
  if(tmpMeeting.finished){
    var finished = new Date(tmpMeeting.finished);
    $('#meetingFinishDateEdit').val(moment(tmpMeeting.finished).format("YYYY-MM-DD"));
    $('#meetingFinishTimeEdit').val(finished.toLocaleTimeString('sl').substring(0,5));
  }
  else{
    $('#meetingFinishDateEdit').val('');
    $('#meetingFinishTimeEdit').val('');
  }
  //unchecked all present users and check correct present users for meeting id
  $("input[name='present-checkbox-edit']").prop("checked", false);
  var presentEdit = [];
  if(tmpMeeting.present_users_id)
    presentEdit = tmpMeeting.present_users_id.split(',')
  presentEdit.forEach(p => $('#userEdit'+p).prop('checked', true))
  $('#modalEditMeeting').modal('toggle');
}
function deleteMeeting(id){
  meetingId = id;
  var tmpMeeting = allMeetings.find(m => m.id == meetingId);
  if(tmpMeeting.active == true){
    $('#deleteModalMsg').html('Ste prepričani, da želite odstraniti ta sestanek?');
    $('#btnDeleteMeeting').html('Odstrani');
  }
  else{
    $('#deleteModalMsg').html('Ste prepričani, da želite ponovno dodati ta sestanek?');
    $('#btnDeleteMeeting').html('Ponovno dodaj');
  }
  $('#modalDeleteMeeting').modal('toggle');
}
function deleteMeetingConf(){
  var tmpMeeting = allMeetings.find(m => m.id == meetingId);
  var active =  (tmpMeeting.active == true) ? false : true; // IF current active is true THEN false ELSE true
  
  let data = {meetingId, active};
  $.ajax({
    type: 'DELETE',
    url: '/meetings/',
    contentType: 'application/json',
    data: JSON.stringify(data), // access in body
  }).done(function (resp) {
    //console.log('SUCCESS');
    debugger;
    if(resp.success){
      if(loggedUser.role == 'admin'){
        //marked new active on meeting list, update active meeting list and update variables //DEPENDS ON WHICH MEETING TABEL IS SHOWN
        if(meetingsActivity == '0'){
          //true and false, all meetings are shown, update variables and  class & icon on list
          tmpMeeting.active = active;
          allActiveMeetings = allMeetings.filter(m => !m.finished && m.active == true);
          if(active){
            var activeMeetingElement = `<option value=` + meetingId + ` id="activeMeeting` + meetingId + `">` + tmpMeeting.counter + `. sestanek ` + new Date(tmpMeeting.date).toLocaleTimeString('sl').substring(0,5) + `</option>`;
            $('#activeMeetingSelect').append(activeMeetingElement);
            $('#meeting'+meetingId).removeClass('bg-secondary');
            $('#meetingButtonDelete'+meetingId).find('i').removeClass('fa-trash-restore');
            $('#meetingButtonDelete'+meetingId).find('i').addClass('fa-trash');
          }
          else{
            $('#activeMeeting'+meetingId).remove();
            $('#meeting'+meetingId).addClass('bg-secondary');
            $('#meetingButtonDelete'+meetingId).find('i').removeClass('fa-trash');
            $('#meetingButtonDelete'+meetingId).find('i').addClass('fa-trash-restore');
          }
        }
        else if(meetingsActivity == '1' || meetingsActivity == '2'){
          //true, only active meetings are shown, same as any other role user
          //allMeetings = allMeetings.filter(m => m.id != meetingId);
          allActiveMeetings = allMeetings.filter(m => !m.finished && m.active == true);
          $('#meeting'+meetingId).remove();
          $('#activeMeeting'+meetingId).remove();
          if(allActiveMeetings.length == 0){
            $('#addActivityBtn').prop('disabled', true);
            $('#finishMeetingBtn').prop('disabled', true);
            //add disable to all status selects
            $('.status-select').prop('disabled', true);
          }
          else{
            $('.status-select').prop('disabled', false);
          }
        }
      }
      else{
        //remove deleted meeting from meeting and active meeting lists and remove meeting from variables
        //allMeetings = allMeetings.filter(m => m.id != meetingId);
        allActiveMeetings = allActiveMeetings.filter(m => m.id != meetingId);
        $('#meeting'+meetingId).remove();
        $('#activeMeeting'+meetingId).remove();
        if(allActiveMeetings.length == 0){
          $('#addActivityBtn').prop('disabled', true);
          $('#finishMeetingBtn').prop('disabled', true);
          //add disable to all status selects
          $('.status-select').prop('disabled', true);
        }
        else{
          $('.status-select').prop('disabled', false);
        }
      }
      //add new change
      if (selectedCategory == '0'){
        if(active)
          addChangeSystem(6,30,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,meetingId);
        else
          addChangeSystem(3,30,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,meetingId);
      }
      else{
        if(active)
          addChangeSystem(6,36,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,meetingId);
        else
          addChangeSystem(3,36,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,meetingId);
      }

      $('#modalDeleteMeeting').modal('toggle');
    }
    else{
      $('#modalDeleteMeeting').modal('toggle');
      $('#modalError').modal('toggle');  
    }
  }).fail(function (resp) {
    //console.log('FAIL');
    //debugger;
    $('#modalError').modal('toggle');
  }).always(function (resp) {
    //console.log('ALWAYS');
    //debugger;
  });
}
function changeActiveMeetings(activity){
  meetingsActivity = 0;
  let data = {meetingsActivity, activitiesActivity, activitiesStatus, selectedCategory};
  $.ajax({
    type: 'GET',
    url: '/meetings/info',
    contentType: 'application/x-www-form-urlencoded',
    data: data, // access in body
  }).done(function (resp) {
    //console.log('SUCCESS');
    debugger;
    if(resp.success){
      meetingsActivity = activity;
      allMeetings = resp.meetings;
      allActiveMeetings = allMeetings.filter(m => !m.finished && m.active == true);
      fillMeetingsList();
      fillActiveMeetingsList();
      $('#optionAllMeetings').removeClass('active stay-focus');
      $('#optionTrueMeetings').removeClass('active stay-focus');
      $('#optionFalseMeetings').removeClass('active stay-focus');
      if(activity == 0) $('#optionAllMeetings').addClass('active stay-focus');
      else if(activity == 1) $('#optionTrueMeetings').addClass('active stay-focus');
      else if(activity == 2) $('#optionFalseMeetings').addClass('active stay-focus');
    }
    else{
      
      $('#modalError').modal('toggle');  
    }
  }).fail(function (resp) {
    //console.log('FAIL');
    //debugger;
    $('#modalError').modal('toggle');
  }).always(function (resp) {
    //console.log('ALWAYS');
    //debugger;
  });
}
function fillMeetingsList(){
  $('#meetingsList').empty();
  //var meetingsCounter = 0;
  for (const m of allMeetings) {
    //meetingsCounter++;
    //m.counter = meetingsCounter;
    if ((m.active == false && meetingsActivity == '1') || (m.active == true && meetingsActivity =='2'))
      continue;
    else if (m.active == true && meetingsActivity =='2')
      continue;
    var date = new Date(m.date)
    var finishedLabel = '';
    if(m.finished){
      var finished = new Date(m.finished);
      finishedLabel = finished.toLocaleDateString('sl') + ' ob ' + finished.toLocaleTimeString('sl').substring(0,5);
    }
    var deleteLabel = '';
    if(m.active == false) deleteLabel = ' bg-secondary';
    var deleteIcon = 'trash';
    if(m.active == false) deleteIcon = 'trash-restore';
    var presentLabel = (m.present_users_id) ? m.present_users : '';
    var buttonsElem = '';
    if(loggedUser.role == 'admin' || loggedUser.role.substring(0,5) == 'vodja'){
      buttonsElem = `<button class="btn btn-danger ml-2 mb-n1 btn-delete" id="meetingButtonDelete`+m.id+`" data-toggle="tooltip" data-original-title="Odstrani">
        <i class="fas fa-`+deleteIcon+` fa-lg"></i>
      </button>
      <button class="btn btn-primary ml-2 mb-n1 btn-edit" id="meetingButtonEdit`+m.id+`" data-toggle="tooltip" data-original-title="Uredi">
        <i class="fas fa-pen fa-lg"></i>
      </button>`;
    }
    var element = `<div class="list-group-item`+deleteLabel+`" id="meeting`+m.id+`">
      <div class="row">
        <div class="col-md-2">
          <div id="meetingNumber`+m.id+`">`+m.counter+`. sestanek</div>
        </div>
        <div class="col-md-2">
          <div id="meetingDate`+m.id+`">Datum: `+date.toLocaleDateString('sl')+`</div>
        </div>
        <div class="col-md-2">
          <div id="meetingStart`+m.id+`">Začetek: `+date.toLocaleTimeString('sl').substring(0,5)+`</div>
        </div>
        <div class="col-md-2">
          <div id="meetingFinish`+m.id+`">Konec: `+finishedLabel+`</div>
        </div>
        <div class="col-md-4 d-flex">
          <div class="ml-auto mb-1 mt-n1" id="meetingButtons`+m.id+`">
            `+buttonsElem+`
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-md-12 d-flex">
          <div id="meetingPresence`+m.id+`">Prisotni: ` + presentLabel + `</div>
        </div>
      </div>
      <div class="collapse multi-collapse task-collapse" id="collapseEditMeeting`+m.id+`"></div>
    </div>`;
    $('#meetingsList').append(element);
  }
}
function fillActiveMeetingsList(){
  $('#activeMeetingSelect').empty();
  allActiveMeetings.forEach(m => {
    var date = new Date(m.date);
    var activeMeetingElement = `<option value=` + m.id + ` id="activeMeeting` + m.id + `">` + m.counter + `. sestanek ` + date.toLocaleTimeString('sl').substring(0,5) + `</option>`;
    $('#activeMeetingSelect').append(activeMeetingElement);
  })
  if(allActiveMeetings.length == 0){
    $('#addActivityBtn').prop('disabled', true);
    $('#finishMeetingBtn').prop('disabled', true);
    //add disable status selects
    $('.status-select').prop('disabled', true);
  }
  else{
    $('#addActivityBtn').prop('disabled', false);
    $('#finishMeetingBtn').prop('disabled', false);
    //add remove disable status selects
    $('.status-select').prop('disabled', false);
  }
}
var meetingId;
var meetingsActivity = 0;