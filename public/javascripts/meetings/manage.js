//IN THIS SCRIPT//
//// get init data that is already shown on page, control collapses and modals ////
$(function(){
  loggedUser = {name:$('#loggedUser').html().split(" (")[0].split(" ")[0], surname:$('#loggedUser').html().split(" (")[0].split(" ")[1], role:$('#loggedUser').html().split(" (")[1].split(")")[0].toLowerCase()};
  //debugger;
  //get all activities, meetings, users for presence
  //var today = new Date();
  $.get( "/meetings/info", function( data ) {
    allActivities = data.activities;
    allMeetings = data.meetings;
    allUsers = data.users;
    allStatus = data.status;
    allActiveMeetings = allMeetings.filter(m => !m.finished && m.active);
    if(allActiveMeetings.length > 0){
      $('#addActivityBtn').prop('disabled', false);
      $('#finishMeetingBtn').prop('disabled', false);
      //add remove disable status select
      $('.status-select').prop('disabled', false);
    }
    else{
      $('#addActivityBtn').prop('disabled', true);
      $('#finishMeetingBtn').prop('disabled', true);
      //add disable status select
      $('.status-select').prop('disabled', true)
    }
    $(".select2-users").select2({
      data: allUsers,
      multiple: true,
    });
    fillAddMeetingModal();
    fillEditMeetingModal();
  });
  //on event call functions
  $('#btnCollapseActivity').on('click', toggleActivityCollapse);
  $('#btnCollapseMeeting').on('click', toggleMeetingCollapse);
  $('#btnLeaderMeetings').on('click', toggleLeaderMeetings);
  $('#btnCategoryMeetings').on('click', toggleCategoryMeetings);
  $('#btnNavbarCollapseActivity').on('click', toggleActivityCollapse);
  $('#btnNavbarCollapseMeeting').on('click', toggleMeetingCollapse);
  $('#categoryGroupButtons').on('click', 'button', function (event) {
    // debugger
    // changeStatus(event.currentTarget.id.substring(14));
    changeCategory(event.currentTarget.value);
  });
})
function fixPDFLink() {
  let optionStatus = $('#statusGroupButtons').find('.stay-focus').val();
  let optionActive = ($('#activeGroupButtons')[0].children.length > 0) ? $('#activeGroupButtons').find('.stay-focus').val() : '1';
  let optionCategory = ($('#categoryGroupButtons')[0].children.length > 0) ? $('#categoryGroupButtons').find('.stay-focus').val() : '0';

  $('#btnToPDF').prop('href', "/meetings/pdfs/?optionStatus="+optionStatus+"&optionCategory="+optionCategory+"&optionActive="+optionActive);
}
//TOGGLE MEETING COLLAPSE
function toggleMeetingCollapse(){
  $('#collapseMeetings').collapse('toggle');
  if($('#btnCollapseMeeting').hasClass('active')){
    $('#btnCollapseMeeting').removeClass('active');
    $('#btnNavbarCollapseMeeting').removeClass('active');
  }
  else{
    $('#btnCollapseMeeting').addClass('active');
    $('#btnNavbarCollapseMeeting').addClass('active');
  }
}
//TOGGLE ACTIVITY COLLAPSE
function toggleActivityCollapse(){
  $('#collapseActivities').collapse('toggle');
  if($('#btnCollapseActivity').hasClass('active')){
    $('#btnCollapseActivity').removeClass('active');
    $('#btnNavbarCollapseActivity').removeClass('active');
  }
  else{
    $('#btnCollapseActivity').addClass('active');
    $('#btnNavbarCollapseActivity').addClass('active');
  }
}
//CHANGE MODE MEETINGS - switch between meetings for leaders and meetings for chosen category
function toggleCategoryMeetings() {
  if (meetingsMode != 1){
    $('#btnLeaderMeetings').removeClass('active');
    $('#btnCategoryMeetings').addClass('active');
    let categoryButtons = `<button class="btn btn-purchase" id="option2" type="button" name="options" autocomplete="off", value="2">Nabava</button>
      <button class="btn btn-construction active stay-focus" id="option3" type="button" name="options" autocomplete="off", value="3">Konstrukcija</button>
      <button class="btn btn-mechanic" id="option4" type="button" name="options" autocomplete="off", value="4">Strojna izdelava</button>
      <button class="btn btn-electrican" id="option5" type="button" name="options" autocomplete="off", value="5">Elektro izdelava</button>
      <button class="btn btn-assembly" id="option6" type="button" name="options" autocomplete="off", value="6">Montaža</button>
      <button class="btn btn-programmer" id="option7" type="button" name="options" autocomplete="off", value="7">Programiranje</button>
      <button class="btn btn-dark" id="option1" type="button" name="options" autocomplete="off", value="1">Brez</button>`;
    //$('#statusCategoryGroupButtons').empty();
    $('#meetingsList').empty();
    $('#categoryGroupButtons').append(categoryButtons);
    selectedCategory = 3;
    //add new colum for project
    $('#activitiesTableBody').empty();
    $('#activityHeadName').before('<th class="w-190px" scope="col" id="activityHeadProject">Projekt</th>');
    $('#activityNameNewCell').before(`<td id="activityProjectNewCell">
      <div class="form-group ml-n2 mr-n2 mb-n2">
        <select class="custom-select mr-sm-2 form-control select2-projects w-100" id="activityProjectsNew" name="projectsNew"></select>
      </div>
    </td>`);
    //id="activityNameNewCell"
    //get new activities and meetings
    changeActiveActivities(activitiesActivity);
    changeActiveMeetings(1);
    getProjectsForSelect();
    meetingsMode = 1;
    fixPDFLink();
  }
}
function toggleLeaderMeetings() {
  if (meetingsMode != 0){
    $('#btnCategoryMeetings').removeClass('active');
    $('#btnLeaderMeetings').addClass('active');
    // let statusButtons = `<button class="btn btn-primary active stay-focus" id="optionStatus0" type="radio" name="optionsStatus" autocomplete="off" onclick="changeStatusActivities(0)">Vse</button>
    // <button class="btn btn-danger" id="optionStatus1" type="radio" name="optionsStatus" autocomplete="off" onclick="changeStatusActivities(1)">Odprte</button>
    // <button class="btn btn-success" id="optionStatus2" type="radio" name="optionsStatus" autocomplete="off" onclick="changeStatusActivities(2)">Zaključene</button>
    // <button class="btn btn-secondary" id="optionStatus3" type="radio" name="optionsStatus" autocomplete="off" onclick="changeStatusActivities(3)">Ostale</button>`;
    $('#categoryGroupButtons').empty();
    //$('#statusCategoryGroupButtons').append(statusButtons);
    selectedCategory = 0;
    $('#activitiesTableBody').empty();
    $('#meetingsList').empty();
    $('#activityHeadProject').remove();
    $('#activityProjectNewCell').remove();
    changeStatusActivities(activitiesStatus);
    changeActiveMeetings(1);
    meetingsMode = 0;
    fixPDFLink();
  }
}
function changeCategory(category) {
  //color correct category
  $('#option1').removeClass('active stay-focus');
  $('#option2').removeClass('active stay-focus');
  $('#option3').removeClass('active stay-focus');
  $('#option4').removeClass('active stay-focus');
  $('#option5').removeClass('active stay-focus');
  $('#option6').removeClass('active stay-focus');
  $('#option7').removeClass('active stay-focus');
  if(category == 1) $('#option1').addClass('active stay-focus');
  else if(category == 2) $('#option2').addClass('active stay-focus');
  else if(category == 3) $('#option3').addClass('active stay-focus');
  else if(category == 4) $('#option4').addClass('active stay-focus');
  else if(category == 5) $('#option5').addClass('active stay-focus');
  else if(category == 6) $('#option6').addClass('active stay-focus');
  else if(category == 7) $('#option7').addClass('active stay-focus');
  //logic for changing category and showing correct activities and meetings
  selectedCategory = category;
  changeActiveActivities(activitiesActivity);
  let activityForMeeting = 1;
  if ($('#optionAllMeetings').hasClass('active')) activityForMeeting = 0;
  else if ($('#optionFalseMeetings').hasClass('active')) activityForMeeting = 2;
  changeActiveMeetings(activityForMeeting);
  fixPDFLink();
}
function getProjectsForSelect() {
  if(allProjects.length == 0){
    $.get( "/viz/allprojects", function( data ) {
      //sort data to get correct order of RT numbers
      allActiveProjects = data.data;
      allActiveProjectsData = data.altData;
      var tmp1 = allActiveProjects.filter(p => p.project_number.substring(0,2) == 'RT' );
      var tmp2 = allActiveProjects.filter(p => p.project_number.substring(0,3) == 'STR' );
      var tmp3 = allActiveProjects.filter(p => p.project_number.substring(0,3) != 'STR' && p.project_number.substring(0,2) != 'RT');
      tmp1.forEach(p => p.number_order = parseInt(p.project_number.substring(2)));
      tmp2.forEach(p => p.number_order = parseInt(p.project_number.substring(3)));
      var sortedTmp1 = tmp1.sort(function(a,b){
        if (isNaN(a.number_order))
          return 1;
        else if (isNaN(b.number_order))
          return -1;
        return b.number_order - a.number_order;
      });
      var sortedTmp2 = tmp2.sort(function(a,b){
        if (isNaN(a.number_order))
          return 1;
        else if (isNaN(b.number_order))
          return -1;
        return b.number_order - a.number_order;
      });
      allRTProjects = sortedTmp1, allSTRProjects = sortedTmp2;
      allActiveProjects = sortedTmp2.concat(sortedTmp1,tmp3);
      var tmp = allActiveProjects.filter(p => p.completion != 100)
      $(".select2-projects").prepend('<option selected></option>').select2({
        data: tmp,
        placeholder: "Projekt",
        allowClear: true,
      });
    });
  }
}
function fillAddMeetingModal(){
  var today = new Date();
  $('#meetingDateAdd').val(moment().format("YYYY-MM-DD"));
  $('#meetingTimeAdd').val(today.toLocaleTimeString('sl').substring(0,5));
  //make modal - add users to correct role in modal view
  var element = '';
  //DIRECTORS
  var directors = allUsers.filter(u => u.text == 'Srečko Potočnik' || u.text == 'Cveto Jakopič' || u.text == 'Boštjan Skarlovnik');
  for(var i = 0; i < directors.length; i++){
    if(directors.length % 2 == 0){
      element = `<div class="row">
        <div class="col-1"></div>
        <div class="col-5">
          <div class="custom-control custom-checkbox">
            <input class="custom-control-input p-1 checkbox-director" id="user`+projectLeaders[i].id+`" type="checkbox" checked="" name="present-checkbox">
            <label class="custom-control-label" for="user`+projectLeaders[i].id+`">`+projectLeaders[i].text+`</label>
          </div>
        </div>
        <div class="col-5">
          <div class="custom-control custom-checkbox">
            <input class="custom-control-input p-1 checkbox-director" id="user`+projectLeaders[++i].id+`" type="checkbox" checked="" name="present-checkbox">
            <label class="custom-control-label" for="user`+projectLeaders[i].id+`">`+projectLeaders[i].text+`</label>
          </div>
        </div>
        <div class="col-1"></div>
      </div>`;
    }
    else{
      if(i == (directors.length - 1)){
        element = `<div class="row">
          <div class="col-3"></div>
          <div class="col-6">
            <div class="custom-control custom-checkbox">
              <input class="custom-control-input p-1 checkbox-director" id="user`+directors[i].id+`" type="checkbox" checked="" name="present-checkbox">
              <label class="custom-control-label" for="user`+directors[i].id+`">`+directors[i].text+`</label>
            </div>
          </div>
          <div class="col-3"></div>
        </div>`;
      }
      else{
        element = `<div class="row">
          <div class="col-1"></div>
          <div class="col-5">
            <div class="custom-control custom-checkbox">
              <input class="custom-control-input p-1 checkbox-director" id="user`+directors[i].id+`" type="checkbox" checked="" name="present-checkbox">
              <label class="custom-control-label" for="user`+directors[i].id+`">`+directors[i].text+`</label>
            </div>
          </div>
          <div class="col-5">
            <div class="custom-control custom-checkbox">
              <input class="custom-control-input p-1 checkbox-director" id="user`+directors[++i].id+`" type="checkbox" checked="" name="present-checkbox">
              <label class="custom-control-label" for="user`+directors[i].id+`">`+directors[i].text+`</label>
            </div>
          </div>
          <div class="col-1"></div>
        </div>`;
      }
    }
    $('#directorsAdd').append(element);
  }
  //PROJECT LEADERS
  var projectLeaders = allUsers.filter(u => u.role == 'vodja projektov');
  for(var i = 0; i < projectLeaders.length; i++){
    if(projectLeaders.length % 2 == 0){
      element = `<div class="row">
        <div class="col-1"></div>
        <div class="col-5">
          <div class="custom-control custom-checkbox">
            <input class="custom-control-input p-1 checkbox-project-leader" id="user`+projectLeaders[i].id+`" type="checkbox" name="present-checkbox" checked="">
            <label class="custom-control-label" for="user`+projectLeaders[i].id+`">`+projectLeaders[i].text+`</label>
          </div>
        </div>
        <div class="col-5">
          <div class="custom-control custom-checkbox">
            <input class="custom-control-input p-1 checkbox-project-leader" id="user`+projectLeaders[++i].id+`" type="checkbox" name="present-checkbox" checked="">
            <label class="custom-control-label" for="user`+projectLeaders[i].id+`">`+projectLeaders[i].text+`</label>
          </div>
        </div>
        <div class="col-1"></div>
      </div>`;
    }
    else{
      if(i == (projectLeaders.length - 1)){
        element = `<div class="row">
          <div class="col-3"></div>
          <div class="col-6">
            <div class="custom-control custom-checkbox">
              <input class="custom-control-input p-1 checkbox-project-leader" id="user`+projectLeaders[i].id+`" type="checkbox" name="present-checkbox" checked="">
              <label class="custom-control-label" for="user`+projectLeaders[i].id+`">`+projectLeaders[i].text+`</label>
            </div>
          </div>
          <div class="col-3"></div>
        </div>`;
      }
      else{
        element = `<div class="row">
          <div class="col-1"></div>
          <div class="col-5">
            <div class="custom-control custom-checkbox">
              <input class="custom-control-input p-1 checkbox-project-leader" id="user`+projectLeaders[i].id+`" type="checkbox" name="present-checkbox" checked="">
              <label class="custom-control-label" for="user`+projectLeaders[i].id+`">`+projectLeaders[i].text+`</label>
            </div>
          </div>
          <div class="col-5">
            <div class="custom-control custom-checkbox">
              <input class="custom-control-input p-1 checkbox-project-leader" id="user`+projectLeaders[++i].id+`" type="checkbox" name="present-checkbox" checked="">
              <label class="custom-control-label" for="user`+projectLeaders[i].id+`">`+projectLeaders[i].text+`</label>
            </div>
          </div>
          <div class="col-1"></div>
        </div>`;
      }
    }
    $('#projectLeadersAdd').append(element);
  }
  //OTHER LEADERS
  var otherLeaders = allUsers.filter(u => u.role.substring(0,5) == 'vodja' && u.role != 'vodja projektov');
  for(var i = 0; i < otherLeaders.length; i++){
    let classAddOn = '', classAddOnPlusOne = '';
    switch (otherLeaders[i].role) {
      case 'vodja električarjev': classAddOn=' checkbox-electrican'; break;
      case 'vodja strojnikov': classAddOn=' checkbox-mechanic'; break;
      case 'vodja CNC obdelave': classAddOn=' checkbox-mechanic'; break;
      case 'vodja programerjev': classAddOn=' checkbox-programmer'; break;
      default: classAddOn=''; break;
    }
    if (i != (otherLeaders.length - 1)){
      switch (otherLeaders[i+1].role) {
        case 'vodja električarjev': classAddOnPlusOne=' checkbox-electrican'; break;
        case 'vodja strojnikov': classAddOnPlusOne=' checkbox-mechanic'; break;
        case 'vodja CNC obdelave': classAddOnPlusOne=' checkbox-mechanic'; break;
        case 'vodja programerjev': classAddOnPlusOne=' checkbox-programmer'; break;
        default: classAddOnPlusOne=''; break;
      }
    }
    if(otherLeaders.length % 2 == 0){
      element = `<div class="row">
        <div class="col-1"></div>
        <div class="col-5">
          <div class="custom-control custom-checkbox">
            <input class="custom-control-input p-1 checkbox-leader`+classAddOn+`" id="user`+otherLeaders[i].id+`" type="checkbox" name="present-checkbox" checked="">
            <label class="custom-control-label" for="user`+otherLeaders[i].id+`">`+otherLeaders[i].text+`</label>
          </div>
        </div>
        <div class="col-5">
          <div class="custom-control custom-checkbox">
            <input class="custom-control-input p-1 checkbox-leader`+classAddOnPlusOne+`" id="user`+otherLeaders[++i].id+`" type="checkbox" name="present-checkbox" checked="">
            <label class="custom-control-label" for="user`+otherLeaders[i].id+`">`+otherLeaders[i].text+`</label>
          </div>
        </div>
        <div class="col-1"></div>
      </div>`;
    }
    else{
      if(i == (otherLeaders.length - 1)){
        element = `<div class="row">
          <div class="col-3"></div>
          <div class="col-6">
            <div class="custom-control custom-checkbox">
              <input class="custom-control-input p-1 checkbox-leader`+classAddOn+`" id="user`+otherLeaders[i].id+`" type="checkbox" name="present-checkbox" checked="">
              <label class="custom-control-label" for="user`+otherLeaders[i].id+`">`+otherLeaders[i].text+`</label>
            </div>
          </div>
          <div class="col-3"></div>
        </div>`;
      }
      else{
        element = `<div class="row">
          <div class="col-1"></div>
          <div class="col-5">
            <div class="custom-control custom-checkbox">
              <input class="custom-control-input p-1 checkbox-leader`+classAddOnPlusOne+`" id="user`+otherLeaders[i].id+`" type="checkbox" name="present-checkbox" checked="">
              <label class="custom-control-label" for="user`+otherLeaders[i].id+`">`+otherLeaders[i].text+`</label>
            </div>
          </div>
          <div class="col-5">
            <div class="custom-control custom-checkbox">
              <input class="custom-control-input p-1 checkbox-leader`+classAddOnPlusOne+`" id="user`+otherLeaders[++i].id+`" type="checkbox" name="present-checkbox" checked="">
              <label class="custom-control-label" for="user`+otherLeaders[i].id+`">`+otherLeaders[i].text+`</label>
            </div>
          </div>
          <div class="col-1"></div>
        </div>`;
      }
    }
    $('#otherLeadersAdd').append(element);
  }
  //BUILDERS - konstrukterji
  var builders = allUsers.filter(u => u.role == 'konstrukter' || u.role == 'konstruktor');
  for(var i = 0; i < builders.length; i++){
    if(builders.length % 2 == 0){
      element = `<div class="row">
        <div class="col-1"></div>
        <div class="col-5">
          <div class="custom-control custom-checkbox">
            <input class="custom-control-input p-1 checkbox-builder" id="user`+builders[i].id+`" type="checkbox" name="present-checkbox">
            <label class="custom-control-label" for="user`+builders[i].id+`">`+builders[i].text+`</label>
          </div>
        </div>
        <div class="col-5">
          <div class="custom-control custom-checkbox">
            <input class="custom-control-input p-1 checkbox-builder" id="user`+builders[++i].id+`" type="checkbox" name="present-checkbox">
            <label class="custom-control-label" for="user`+builders[i].id+`">`+builders[i].text+`</label>
          </div>
        </div>
        <div class="col-1"></div>
      </div>`;
    }
    else{
      if(i == (builders.length - 1)){
        element = `<div class="row">
          <div class="col-3"></div>
          <div class="col-6">
            <div class="custom-control custom-checkbox">
              <input class="custom-control-input p-1 checkbox-builder" id="user`+builders[i].id+`" type="checkbox" name="present-checkbox">
              <label class="custom-control-label" for="user`+builders[i].id+`">`+builders[i].text+`</label>
            </div>
          </div>
          <div class="col-3"></div>
        </div>`;
      }
      else{
        element = `<div class="row">
          <div class="col-1"></div>
          <div class="col-5">
            <div class="custom-control custom-checkbox">
              <input class="custom-control-input p-1 checkbox-builder" id="user`+builders[i].id+`" type="checkbox" name="present-checkbox">
              <label class="custom-control-label" for="user`+builders[i].id+`">`+builders[i].text+`</label>
            </div>
          </div>
          <div class="col-5">
            <div class="custom-control custom-checkbox">
              <input class="custom-control-input p-1 checkbox-builder" id="user`+builders[++i].id+`" type="checkbox" name="present-checkbox">
              <label class="custom-control-label" for="user`+builders[i].id+`">`+builders[i].text+`</label>
            </div>
          </div>
          <div class="col-1"></div>
        </div>`;
      }
    }
    $('#buildersAdd').append(element);
  }
  //PRORAMMERS - plc in robot programerji
  var programmers = allUsers.filter(u => u.role == 'programer plc-jev' || u.role == 'programer robotov');
  for(var i = 0; i < programmers.length; i++){
    if(programmers.length % 2 == 0){
      element = `<div class="row">
        <div class="col-1"></div>
        <div class="col-5">
          <div class="custom-control custom-checkbox">
            <input class="custom-control-input p-1 checkbox-programmer" id="user`+programmers[i].id+`" type="checkbox" name="present-checkbox">
            <label class="custom-control-label" for="user`+programmers[i].id+`">`+programmers[i].text+`</label>
          </div>
        </div>
        <div class="col-5">
          <div class="custom-control custom-checkbox">
            <input class="custom-control-input p-1 checkbox-programmer" id="user`+programmers[++i].id+`" type="checkbox" name="present-checkbox">
            <label class="custom-control-label" for="user`+programmers[i].id+`">`+programmers[i].text+`</label>
          </div>
        </div>
        <div class="col-1"></div>
      </div>`;
    }
    else{
      if(i == (programmers.length - 1)){
        element = `<div class="row">
          <div class="col-3"></div>
          <div class="col-6">
            <div class="custom-control custom-checkbox">
              <input class="custom-control-input p-1 checkbox-programmer" id="user`+programmers[i].id+`" type="checkbox" name="present-checkbox">
              <label class="custom-control-label" for="user`+programmers[i].id+`">`+programmers[i].text+`</label>
            </div>
          </div>
          <div class="col-3"></div>
        </div>`;
      }
      else{
        element = `<div class="row">
          <div class="col-1"></div>
          <div class="col-5">
            <div class="custom-control custom-checkbox">
              <input class="custom-control-input p-1 checkbox-programmer" id="user`+programmers[i].id+`" type="checkbox" name="present-checkbox">
              <label class="custom-control-label" for="user`+programmers[i].id+`">`+programmers[i].text+`</label>
            </div>
          </div>
          <div class="col-5">
            <div class="custom-control custom-checkbox">
              <input class="custom-control-input p-1 checkbox-programmer" id="user`+programmers[++i].id+`" type="checkbox" name="present-checkbox">
              <label class="custom-control-label" for="user`+programmers[i].id+`">`+programmers[i].text+`</label>
            </div>
          </div>
          <div class="col-1"></div>
        </div>`;
      }
    }
    $('#programmersAdd').append(element);
  }
  //MECHANICS - strojniki
  var mechanics = allUsers.filter(u => u.role == 'strojnik');
  for(var i = 0; i < mechanics.length; i++){
    if(mechanics.length % 2 == 0){
      element = `<div class="row">
        <div class="col-1"></div>
        <div class="col-5">
          <div class="custom-control custom-checkbox">
            <input class="custom-control-input p-1 checkbox-mechanic" id="user`+mechanics[i].id+`" type="checkbox" name="present-checkbox">
            <label class="custom-control-label" for="user`+mechanics[i].id+`">`+mechanics[i].text+`</label>
          </div>
        </div>
        <div class="col-5">
          <div class="custom-control custom-checkbox">
            <input class="custom-control-input p-1 checkbox-mechanic" id="user`+mechanics[++i].id+`" type="checkbox" name="present-checkbox">
            <label class="custom-control-label" for="user`+mechanics[i].id+`">`+mechanics[i].text+`</label>
          </div>
        </div>
        <div class="col-1"></div>
      </div>`;
    }
    else{
      if(i == (mechanics.length - 1)){
        element = `<div class="row">
          <div class="col-3"></div>
          <div class="col-6">
            <div class="custom-control custom-checkbox">
              <input class="custom-control-input p-1 checkbox-mechanic" id="user`+mechanics[i].id+`" type="checkbox" name="present-checkbox">
              <label class="custom-control-label" for="user`+mechanics[i].id+`">`+mechanics[i].text+`</label>
            </div>
          </div>
          <div class="col-3"></div>
        </div>`;
      }
      else{
        element = `<div class="row">
          <div class="col-1"></div>
          <div class="col-5">
            <div class="custom-control custom-checkbox">
              <input class="custom-control-input p-1 checkbox-mechanic" id="user`+mechanics[i].id+`" type="checkbox" name="present-checkbox">
              <label class="custom-control-label" for="user`+mechanics[i].id+`">`+mechanics[i].text+`</label>
            </div>
          </div>
          <div class="col-5">
            <div class="custom-control custom-checkbox">
              <input class="custom-control-input p-1 checkbox-mechanic" id="user`+mechanics[++i].id+`" type="checkbox" name="present-checkbox">
              <label class="custom-control-label" for="user`+mechanics[i].id+`">`+mechanics[i].text+`</label>
            </div>
          </div>
          <div class="col-1"></div>
        </div>`;
      }
    }
    $('#mechanicsAdd').append(element);
  }
  //ELECTRICANS - električarji
  var electricans = allUsers.filter(u => u.role == 'električar');
  for(var i = 0; i < electricans.length; i++){
    if(electricans.length % 2 == 0){
      element = `<div class="row">
        <div class="col-1"></div>
        <div class="col-5">
          <div class="custom-control custom-checkbox">
            <input class="custom-control-input p-1 checkbox-electrican" id="user`+electricans[i].id+`" type="checkbox" name="present-checkbox">
            <label class="custom-control-label" for="user`+electricans[i].id+`">`+electricans[i].text+`</label>
          </div>
        </div>
        <div class="col-5">
          <div class="custom-control custom-checkbox">
            <input class="custom-control-input p-1 checkbox-electrican" id="user`+electricans[++i].id+`" type="checkbox" name="present-checkbox">
            <label class="custom-control-label" for="user`+electricans[i].id+`">`+electricans[i].text+`</label>
          </div>
        </div>
        <div class="col-1"></div>
      </div>`;
    }
    else{
      if(i == (electricans.length - 1)){
        element = `<div class="row">
          <div class="col-3"></div>
          <div class="col-6">
            <div class="custom-control custom-checkbox">
              <input class="custom-control-input p-1 checkbox-electrican" id="user`+electricans[i].id+`" type="checkbox" name="present-checkbox">
              <label class="custom-control-label" for="user`+electricans[i].id+`">`+electricans[i].text+`</label>
            </div>
          </div>
          <div class="col-3"></div>
        </div>`;
      }
      else{
        element = `<div class="row">
          <div class="col-1"></div>
          <div class="col-5">
            <div class="custom-control custom-checkbox">
              <input class="custom-control-input p-1 checkbox-electrican" id="user`+electricans[i].id+`" type="checkbox" name="present-checkbox">
              <label class="custom-control-label" for="user`+electricans[i].id+`">`+electricans[i].text+`</label>
            </div>
          </div>
          <div class="col-5">
            <div class="custom-control custom-checkbox">
              <input class="custom-control-input p-1 checkbox-electrican" id="user`+electricans[++i].id+`" type="checkbox" name="present-checkbox">
              <label class="custom-control-label" for="user`+electricans[i].id+`">`+electricans[i].text+`</label>
            </div>
          </div>
          <div class="col-1"></div>
        </div>`;
      }
    }
    $('#electricansAdd').append(element);
  }
  //MECHASSEMBLERS - strojni monterji
  var mechAssemblers = allUsers.filter(u => u.role == 'strojni monter');
  for(var i = 0; i < mechAssemblers.length; i++){
    if(mechAssemblers.length % 2 == 0){
      element = `<div class="row">
        <div class="col-1"></div>
        <div class="col-5">
          <div class="custom-control custom-checkbox">
            <input class="custom-control-input p-1 checkbox-mech-assembly" id="user`+mechAssemblers[i].id+`" type="checkbox" name="present-checkbox">
            <label class="custom-control-label" for="user`+mechAssemblers[i].id+`">`+mechAssemblers[i].text+`</label>
          </div>
        </div>
        <div class="col-5">
          <div class="custom-control custom-checkbox">
            <input class="custom-control-input p-1 checkbox-mech-assembly" id="user`+mechAssemblers[++i].id+`" type="checkbox" name="present-checkbox">
            <label class="custom-control-label" for="user`+mechAssemblers[i].id+`">`+mechAssemblers[i].text+`</label>
          </div>
        </div>
        <div class="col-1"></div>
      </div>`;
    }
    else{
      if(i == (mechAssemblers.length - 1)){
        element = `<div class="row">
          <div class="col-3"></div>
          <div class="col-6">
            <div class="custom-control custom-checkbox">
              <input class="custom-control-input p-1 checkbox-mech-assembly" id="user`+mechAssemblers[i].id+`" type="checkbox" name="present-checkbox">
              <label class="custom-control-label" for="user`+mechAssemblers[i].id+`">`+mechAssemblers[i].text+`</label>
            </div>
          </div>
          <div class="col-3"></div>
        </div>`;
      }
      else{
        element = `<div class="row">
          <div class="col-1"></div>
          <div class="col-5">
            <div class="custom-control custom-checkbox">
              <input class="custom-control-input p-1 checkbox-mech-assembly" id="user`+mechAssemblers[i].id+`" type="checkbox" name="present-checkbox">
              <label class="custom-control-label" for="user`+mechAssemblers[i].id+`">`+mechAssemblers[i].text+`</label>
            </div>
          </div>
          <div class="col-5">
            <div class="custom-control custom-checkbox">
              <input class="custom-control-input p-1 checkbox-mech-assembly" id="user`+mechAssemblers[++i].id+`" type="checkbox" name="present-checkbox">
              <label class="custom-control-label" for="user`+mechAssemblers[i].id+`">`+mechAssemblers[i].text+`</label>
            </div>
          </div>
          <div class="col-1"></div>
        </div>`;
      }
    }
    $('#mechAssemblersAdd').append(element);
  }
  //CNCOPERATORS - CNC operaterji
  var cncOperators = allUsers.filter(u => u.role == 'cnc' || u.role == 'cnc operater');
  for(var i = 0; i < cncOperators.length; i++){
    if(cncOperators.length % 2 == 0){
      element = `<div class="row">
        <div class="col-1"></div>
        <div class="col-5">
          <div class="custom-control custom-checkbox">
            <input class="custom-control-input p-1 checkbox-cnc-operator" id="user`+cncOperators[i].id+`" type="checkbox" name="present-checkbox">
            <label class="custom-control-label" for="user`+cncOperators[i].id+`">`+cncOperators[i].text+`</label>
          </div>
        </div>
        <div class="col-5">
          <div class="custom-control custom-checkbox">
            <input class="custom-control-input p-1 checkbox-cnc-operator" id="user`+cncOperators[++i].id+`" type="checkbox" name="present-checkbox">
            <label class="custom-control-label" for="user`+cncOperators[i].id+`">`+cncOperators[i].text+`</label>
          </div>
        </div>
        <div class="col-1"></div>
      </div>`;
    }
    else{
      if(i == (cncOperators.length - 1)){
        element = `<div class="row">
          <div class="col-3"></div>
          <div class="col-6">
            <div class="custom-control custom-checkbox">
              <input class="custom-control-input p-1 checkbox-cnc-operator" id="user`+cncOperators[i].id+`" type="checkbox" name="present-checkbox">
              <label class="custom-control-label" for="user`+cncOperators[i].id+`">`+cncOperators[i].text+`</label>
            </div>
          </div>
          <div class="col-3"></div>
        </div>`;
      }
      else{
        element = `<div class="row">
          <div class="col-1"></div>
          <div class="col-5">
            <div class="custom-control custom-checkbox">
              <input class="custom-control-input p-1 checkbox-cnc-operator" id="user`+cncOperators[i].id+`" type="checkbox" name="present-checkbox">
              <label class="custom-control-label" for="user`+cncOperators[i].id+`">`+cncOperators[i].text+`</label>
            </div>
          </div>
          <div class="col-5">
            <div class="custom-control custom-checkbox">
              <input class="custom-control-input p-1 checkbox-cnc-operator" id="user`+cncOperators[++i].id+`" type="checkbox" name="present-checkbox">
              <label class="custom-control-label" for="user`+cncOperators[i].id+`">`+cncOperators[i].text+`</label>
            </div>
          </div>
          <div class="col-1"></div>
        </div>`;
      }
    }
    $('#CNCoperatorsAdd').append(element);
  }
  //WELDERS - varilci
  var welders = allUsers.filter(u => u.role == 'varilec');
  for(var i = 0; i < welders.length; i++){
    if(welders.length % 2 == 0){
      element = `<div class="row">
        <div class="col-1"></div>
        <div class="col-5">
          <div class="custom-control custom-checkbox">
            <input class="custom-control-input p-1 checkbox-welder" id="user`+welders[i].id+`" type="checkbox" name="present-checkbox">
            <label class="custom-control-label" for="user`+welders[i].id+`">`+welders[i].text+`</label>
          </div>
        </div>
        <div class="col-5">
          <div class="custom-control custom-checkbox">
            <input class="custom-control-input p-1 checkbox-welder" id="user`+welders[++i].id+`" type="checkbox" name="present-checkbox">
            <label class="custom-control-label" for="user`+welders[i].id+`">`+welders[i].text+`</label>
          </div>
        </div>
        <div class="col-1"></div>
      </div>`;
    }
    else{
      if(i == (welders.length - 1)){
        element = `<div class="row">
          <div class="col-3"></div>
          <div class="col-6">
            <div class="custom-control custom-checkbox">
              <input class="custom-control-input p-1 checkbox-welder" id="user`+welders[i].id+`" type="checkbox" name="present-checkbox">
              <label class="custom-control-label" for="user`+welders[i].id+`">`+welders[i].text+`</label>
            </div>
          </div>
          <div class="col-3"></div>
        </div>`;
      }
      else{
        element = `<div class="row">
          <div class="col-1"></div>
          <div class="col-5">
            <div class="custom-control custom-checkbox">
              <input class="custom-control-input p-1 checkbox-welder" id="user`+welders[i].id+`" type="checkbox" name="present-checkbox">
              <label class="custom-control-label" for="user`+welders[i].id+`">`+welders[i].text+`</label>
            </div>
          </div>
          <div class="col-5">
            <div class="custom-control custom-checkbox">
              <input class="custom-control-input p-1 checkbox-welder" id="user`+welders[++i].id+`" type="checkbox" name="present-checkbox">
              <label class="custom-control-label" for="user`+welders[i].id+`">`+welders[i].text+`</label>
            </div>
          </div>
          <div class="col-1"></div>
        </div>`;
      }
    }
    $('#weldersAdd').append(element);
  }
  //SERVISER - serviserji
  var serviser = allUsers.filter(u => u.role == 'serviser');
  for(var i = 0; i < serviser.length; i++){
    if(serviser.length % 2 == 0){
      element = `<div class="row">
        <div class="col-1"></div>
        <div class="col-5">
          <div class="custom-control custom-checkbox">
            <input class="custom-control-input p-1 checkbox-serviser" id="user`+serviser[i].id+`" type="checkbox" name="present-checkbox">
            <label class="custom-control-label" for="user`+serviser[i].id+`">`+serviser[i].text+`</label>
          </div>
        </div>
        <div class="col-5">
          <div class="custom-control custom-checkbox">
            <input class="custom-control-input p-1 checkbox-serviser" id="user`+serviser[++i].id+`" type="checkbox" name="present-checkbox">
            <label class="custom-control-label" for="user`+serviser[i].id+`">`+serviser[i].text+`</label>
          </div>
        </div>
        <div class="col-1"></div>
      </div>`;
    }
    else{
      if(i == (serviser.length - 1)){
        element = `<div class="row">
          <div class="col-3"></div>
          <div class="col-6">
            <div class="custom-control custom-checkbox">
              <input class="custom-control-input p-1 checkbox-serviser" id="user`+serviser[i].id+`" type="checkbox" name="present-checkbox">
              <label class="custom-control-label" for="user`+serviser[i].id+`">`+serviser[i].text+`</label>
            </div>
          </div>
          <div class="col-3"></div>
        </div>`;
      }
      else{
        element = `<div class="row">
          <div class="col-1"></div>
          <div class="col-5">
            <div class="custom-control custom-checkbox">
              <input class="custom-control-input p-1 checkbox-serviser" id="user`+serviser[i].id+`" type="checkbox" name="present-checkbox">
              <label class="custom-control-label" for="user`+serviser[i].id+`">`+serviser[i].text+`</label>
            </div>
          </div>
          <div class="col-5">
            <div class="custom-control custom-checkbox">
              <input class="custom-control-input p-1 checkbox-serviser" id="user`+serviser[++i].id+`" type="checkbox" name="present-checkbox">
              <label class="custom-control-label" for="user`+serviser[i].id+`">`+serviser[i].text+`</label>
            </div>
          </div>
          <div class="col-1"></div>
        </div>`;
      }
    }
    $('#serviserAdd').append(element);
  }
  //OTHERS - ostali ki še niso bili dodani na seznam ostalih kategorij
  var others = allUsers.filter(u => u.role != 'serviser' && u.role != 'konstrukter' && u.role != 'konstruktor' && u.role != 'programer plc-jev' && u.role != 'programer robotov' && u.role != 'električar' && u.role != 'strojnik' && u.role != 'strojni monter' && u.role != 'cnc' && u.role != 'cnc operater' && u.role != 'varilec' && u.role.substring(0,5) != 'vodja' && u.role.substring(0,4) != 'info' && u.text != 'Srečko Potočnik' && u.text != 'Cveto Jakopič' && u.text != 'Boštjan Skarlovnik');
  for(var i = 0; i < others.length; i++){
    if(others.length % 2 == 0){
      element = `<div class="row">
        <div class="col-1"></div>
        <div class="col-5">
          <div class="custom-control custom-checkbox">
            <input class="custom-control-input p-1 checkbox-other" id="user`+others[i].id+`" type="checkbox" name="present-checkbox">
            <label class="custom-control-label" for="user`+others[i].id+`">`+others[i].text+`</label>
          </div>
        </div>
        <div class="col-5">
          <div class="custom-control custom-checkbox">
            <input class="custom-control-input p-1 checkbox-other" id="user`+others[++i].id+`" type="checkbox" name="present-checkbox">
            <label class="custom-control-label" for="user`+others[i].id+`">`+others[i].text+`</label>
          </div>
        </div>
        <div class="col-1"></div>
      </div>`;
    }
    else{
      if(i == (others.length - 1)){
        element = `<div class="row">
          <div class="col-3"></div>
          <div class="col-6">
            <div class="custom-control custom-checkbox">
              <input class="custom-control-input p-1 checkbox-other" id="user`+others[i].id+`" type="checkbox" name="present-checkbox">
              <label class="custom-control-label" for="user`+others[i].id+`">`+others[i].text+`</label>
            </div>
          </div>
          <div class="col-3"></div>
        </div>`;
      }
      else{
        element = `<div class="row">
          <div class="col-1"></div>
          <div class="col-5">
            <div class="custom-control custom-checkbox">
              <input class="custom-control-input p-1 checkbox-other" id="user`+others[i].id+`" type="checkbox" name="present-checkbox">
              <label class="custom-control-label" for="user`+others[i].id+`">`+others[i].text+`</label>
            </div>
          </div>
          <div class="col-5">
            <div class="custom-control custom-checkbox">
              <input class="custom-control-input p-1 checkbox-other" id="user`+others[++i].id+`" type="checkbox" name="present-checkbox">
              <label class="custom-control-label" for="user`+others[i].id+`">`+others[i].text+`</label>
            </div>
          </div>
          <div class="col-1"></div>
        </div>`;
      }
    }
    $('#othersAdd').append(element);
  }
}
function fillEditMeetingModal(){
  //make modal - add users to correct role in modal view
  var element = '';
  //DIRECTORS
  var directors = allUsers.filter(u => u.text == 'Srečko Potočnik' || u.text == 'Cveto Jakopič' || u.text == 'Boštjan Skarlovnik');
  for(var i = 0; i < directors.length; i++){
    if(directors.length % 2 == 0){
      element = `<div class="row">
        <div class="col-1"></div>
        <div class="col-5">
          <div class="custom-control custom-checkbox">
            <input class="custom-control-input p-1" id="userEdit`+projectLeaders[i].id+`" type="checkbox" checked="" name="present-checkbox-edit">
            <label class="custom-control-label" for="userEdit`+projectLeaders[i].id+`">`+projectLeaders[i].text+`</label>
          </div>
        </div>
        <div class="col-5">
          <div class="custom-control custom-checkbox">
            <input class="custom-control-input p-1" id="userEdit`+projectLeaders[++i].id+`" type="checkbox" checked="" name="present-checkbox-edit">
            <label class="custom-control-label" for="userEdit`+projectLeaders[i].id+`">`+projectLeaders[i].text+`</label>
          </div>
        </div>
        <div class="col-1"></div>
      </div>`;
    }
    else{
      if(i == (directors.length - 1)){
        element = `<div class="row">
          <div class="col-3"></div>
          <div class="col-6">
            <div class="custom-control custom-checkbox">
              <input class="custom-control-input p-1" id="userEdit`+directors[i].id+`" type="checkbox" checked="" name="present-checkbox-edit">
              <label class="custom-control-label" for="userEdit`+directors[i].id+`">`+directors[i].text+`</label>
            </div>
          </div>
          <div class="col-3"></div>
        </div>`;
      }
      else{
        element = `<div class="row">
          <div class="col-1"></div>
          <div class="col-5">
            <div class="custom-control custom-checkbox">
              <input class="custom-control-input p-1" id="userEdit`+directors[i].id+`" type="checkbox" checked="" name="present-checkbox-edit">
              <label class="custom-control-label" for="userEdit`+directors[i].id+`">`+directors[i].text+`</label>
            </div>
          </div>
          <div class="col-5">
            <div class="custom-control custom-checkbox">
              <input class="custom-control-input p-1" id="userEdit`+directors[++i].id+`" type="checkbox" checked="" name="present-checkbox-edit">
              <label class="custom-control-label" for="userEdit`+directors[i].id+`">`+directors[i].text+`</label>
            </div>
          </div>
          <div class="col-1"></div>
        </div>`;
      }
    }
    $('#directorsEdit').append(element);
  }
  //PROJECT LEADERS
  var projectLeaders = allUsers.filter(u => u.role == 'vodja projektov');
  for(var i = 0; i < projectLeaders.length; i++){
    if(projectLeaders.length % 2 == 0){
      element = `<div class="row">
        <div class="col-1"></div>
        <div class="col-5">
          <div class="custom-control custom-checkbox">
            <input class="custom-control-input p-1" id="userEdit`+projectLeaders[i].id+`" type="checkbox" name="present-checkbox-edit" checked="">
            <label class="custom-control-label" for="userEdit`+projectLeaders[i].id+`">`+projectLeaders[i].text+`</label>
          </div>
        </div>
        <div class="col-5">
          <div class="custom-control custom-checkbox">
            <input class="custom-control-input p-1" id="userEdit`+projectLeaders[++i].id+`" type="checkbox" name="present-checkbox-edit" checked="">
            <label class="custom-control-label" for="userEdit`+projectLeaders[i].id+`">`+projectLeaders[i].text+`</label>
          </div>
        </div>
        <div class="col-1"></div>
      </div>`;
    }
    else{
      if(i == (projectLeaders.length - 1)){
        element = `<div class="row">
          <div class="col-3"></div>
          <div class="col-6">
            <div class="custom-control custom-checkbox">
              <input class="custom-control-input p-1" id="userEdit`+projectLeaders[i].id+`" type="checkbox" name="present-checkbox-edit" checked="">
              <label class="custom-control-label" for="userEdit`+projectLeaders[i].id+`">`+projectLeaders[i].text+`</label>
            </div>
          </div>
          <div class="col-3"></div>
        </div>`;
      }
      else{
        element = `<div class="row">
          <div class="col-1"></div>
          <div class="col-5">
            <div class="custom-control custom-checkbox">
              <input class="custom-control-input p-1" id="userEdit`+projectLeaders[i].id+`" type="checkbox" name="present-checkbox-edit" checked="">
              <label class="custom-control-label" for="userEdit`+projectLeaders[i].id+`">`+projectLeaders[i].text+`</label>
            </div>
          </div>
          <div class="col-5">
            <div class="custom-control custom-checkbox">
              <input class="custom-control-input p-1" id="userEdit`+projectLeaders[++i].id+`" type="checkbox" name="present-checkbox-edit" checked="">
              <label class="custom-control-label" for="userEdit`+projectLeaders[i].id+`">`+projectLeaders[i].text+`</label>
            </div>
          </div>
          <div class="col-1"></div>
        </div>`;
      }
    }
    $('#projectLeadersEdit').append(element);
  }
  //OTHER LEADERS
  var otherLeaders = allUsers.filter(u => u.role.substring(0,5) == 'vodja' && u.role != 'vodja projektov');
  for(var i = 0; i < otherLeaders.length; i++){
    if(otherLeaders.length % 2 == 0){
      element = `<div class="row">
        <div class="col-1"></div>
        <div class="col-5">
          <div class="custom-control custom-checkbox">
            <input class="custom-control-input p-1" id="userEdit`+otherLeaders[i].id+`" type="checkbox" name="present-checkbox-edit" checked="">
            <label class="custom-control-label" for="userEdit`+otherLeaders[i].id+`">`+otherLeaders[i].text+`</label>
          </div>
        </div>
        <div class="col-5">
          <div class="custom-control custom-checkbox">
            <input class="custom-control-input p-1" id="userEdit`+otherLeaders[++i].id+`" type="checkbox" name="present-checkbox-edit" checked="">
            <label class="custom-control-label" for="userEdit`+otherLeaders[i].id+`">`+otherLeaders[i].text+`</label>
          </div>
        </div>
        <div class="col-1"></div>
      </div>`;
    }
    else{
      if(i == (otherLeaders.length - 1)){
        element = `<div class="row">
          <div class="col-3"></div>
          <div class="col-6">
            <div class="custom-control custom-checkbox">
              <input class="custom-control-input p-1" id="userEdit`+otherLeaders[i].id+`" type="checkbox" name="present-checkbox-edit" checked="">
              <label class="custom-control-label" for="userEdit`+otherLeaders[i].id+`">`+otherLeaders[i].text+`</label>
            </div>
          </div>
          <div class="col-3"></div>
        </div>`;
      }
      else{
        element = `<div class="row">
          <div class="col-1"></div>
          <div class="col-5">
            <div class="custom-control custom-checkbox">
              <input class="custom-control-input p-1" id="userEdit`+otherLeaders[i].id+`" type="checkbox" name="present-checkbox-edit" checked="">
              <label class="custom-control-label" for="userEdit`+otherLeaders[i].id+`">`+otherLeaders[i].text+`</label>
            </div>
          </div>
          <div class="col-5">
            <div class="custom-control custom-checkbox">
              <input class="custom-control-input p-1" id="userEdit`+otherLeaders[++i].id+`" type="checkbox" name="present-checkbox-edit" checked="">
              <label class="custom-control-label" for="userEdit`+otherLeaders[i].id+`">`+otherLeaders[i].text+`</label>
            </div>
          </div>
          <div class="col-1"></div>
        </div>`;
      }
    }
    $('#otherLeadersEdit').append(element);
  }
  //BUILDERS - konstrukterji
  var builders = allUsers.filter(u => u.role == 'konstrukter' || u.role == 'konstruktor');
  for(var i = 0; i < builders.length; i++){
    if(builders.length % 2 == 0){
      element = `<div class="row">
        <div class="col-1"></div>
        <div class="col-5">
          <div class="custom-control custom-checkbox">
            <input class="custom-control-input p-1" id="userEdit`+builders[i].id+`" type="checkbox" name="present-checkbox-edit">
            <label class="custom-control-label" for="userEdit`+builders[i].id+`">`+builders[i].text+`</label>
          </div>
        </div>
        <div class="col-5">
          <div class="custom-control custom-checkbox">
            <input class="custom-control-input p-1" id="userEdit`+builders[++i].id+`" type="checkbox" name="present-checkbox-edit">
            <label class="custom-control-label" for="userEdit`+builders[i].id+`">`+builders[i].text+`</label>
          </div>
        </div>
        <div class="col-1"></div>
      </div>`;
    }
    else{
      if(i == (builders.length - 1)){
        element = `<div class="row">
          <div class="col-3"></div>
          <div class="col-6">
            <div class="custom-control custom-checkbox">
              <input class="custom-control-input p-1" id="userEdit`+builders[i].id+`" type="checkbox" name="present-checkbox-edit">
              <label class="custom-control-label" for="userEdit`+builders[i].id+`">`+builders[i].text+`</label>
            </div>
          </div>
          <div class="col-3"></div>
        </div>`;
      }
      else{
        element = `<div class="row">
          <div class="col-1"></div>
          <div class="col-5">
            <div class="custom-control custom-checkbox">
              <input class="custom-control-input p-1" id="userEdit`+builders[i].id+`" type="checkbox" name="present-checkbox-edit">
              <label class="custom-control-label" for="userEdit`+builders[i].id+`">`+builders[i].text+`</label>
            </div>
          </div>
          <div class="col-5">
            <div class="custom-control custom-checkbox">
              <input class="custom-control-input p-1" id="userEdit`+builders[++i].id+`" type="checkbox" name="present-checkbox-edit">
              <label class="custom-control-label" for="userEdit`+builders[i].id+`">`+builders[i].text+`</label>
            </div>
          </div>
          <div class="col-1"></div>
        </div>`;
      }
    }
    $('#buildersEdit').append(element);
  }
  //PRORAMMERS - plc in robot programerji
  var programmers = allUsers.filter(u => u.role == 'programer plc-jev' || u.role == 'programer robotov');
  for(var i = 0; i < programmers.length; i++){
    if(programmers.length % 2 == 0){
      element = `<div class="row">
        <div class="col-1"></div>
        <div class="col-5">
          <div class="custom-control custom-checkbox">
            <input class="custom-control-input p-1" id="userEdit`+programmers[i].id+`" type="checkbox" name="present-checkbox-edit">
            <label class="custom-control-label" for="userEdit`+programmers[i].id+`">`+programmers[i].text+`</label>
          </div>
        </div>
        <div class="col-5">
          <div class="custom-control custom-checkbox">
            <input class="custom-control-input p-1" id="userEdit`+programmers[++i].id+`" type="checkbox" name="present-checkbox-edit">
            <label class="custom-control-label" for="userEdit`+programmers[i].id+`">`+programmers[i].text+`</label>
          </div>
        </div>
        <div class="col-1"></div>
      </div>`;
    }
    else{
      if(i == (programmers.length - 1)){
        element = `<div class="row">
          <div class="col-3"></div>
          <div class="col-6">
            <div class="custom-control custom-checkbox">
              <input class="custom-control-input p-1" id="userEdit`+programmers[i].id+`" type="checkbox" name="present-checkbox-edit">
              <label class="custom-control-label" for="userEdit`+programmers[i].id+`">`+programmers[i].text+`</label>
            </div>
          </div>
          <div class="col-3"></div>
        </div>`;
      }
      else{
        element = `<div class="row">
          <div class="col-1"></div>
          <div class="col-5">
            <div class="custom-control custom-checkbox">
              <input class="custom-control-input p-1" id="userEdit`+programmers[i].id+`" type="checkbox" name="present-checkbox-edit">
              <label class="custom-control-label" for="userEdit`+programmers[i].id+`">`+programmers[i].text+`</label>
            </div>
          </div>
          <div class="col-5">
            <div class="custom-control custom-checkbox">
              <input class="custom-control-input p-1" id="userEdit`+programmers[++i].id+`" type="checkbox" name="present-checkbox-edit">
              <label class="custom-control-label" for="userEdit`+programmers[i].id+`">`+programmers[i].text+`</label>
            </div>
          </div>
          <div class="col-1"></div>
        </div>`;
      }
    }
    $('#programmersEdit').append(element);
  }
  //MECHANICS - strojniki
  var mechanics = allUsers.filter(u => u.role == 'strojnik');
  for(var i = 0; i < mechanics.length; i++){
    if(mechanics.length % 2 == 0){
      element = `<div class="row">
        <div class="col-1"></div>
        <div class="col-5">
          <div class="custom-control custom-checkbox">
            <input class="custom-control-input p-1" id="userEdit`+mechanics[i].id+`" type="checkbox" name="present-checkbox-edit">
            <label class="custom-control-label" for="userEdit`+mechanics[i].id+`">`+mechanics[i].text+`</label>
          </div>
        </div>
        <div class="col-5">
          <div class="custom-control custom-checkbox">
            <input class="custom-control-input p-1" id="userEdit`+mechanics[++i].id+`" type="checkbox" name="present-checkbox-edit">
            <label class="custom-control-label" for="userEdit`+mechanics[i].id+`">`+mechanics[i].text+`</label>
          </div>
        </div>
        <div class="col-1"></div>
      </div>`;
    }
    else{
      if(i == (mechanics.length - 1)){
        element = `<div class="row">
          <div class="col-3"></div>
          <div class="col-6">
            <div class="custom-control custom-checkbox">
              <input class="custom-control-input p-1" id="userEdit`+mechanics[i].id+`" type="checkbox" name="present-checkbox-edit">
              <label class="custom-control-label" for="userEdit`+mechanics[i].id+`">`+mechanics[i].text+`</label>
            </div>
          </div>
          <div class="col-3"></div>
        </div>`;
      }
      else{
        element = `<div class="row">
          <div class="col-1"></div>
          <div class="col-5">
            <div class="custom-control custom-checkbox">
              <input class="custom-control-input p-1" id="userEdit`+mechanics[i].id+`" type="checkbox" name="present-checkbox-edit">
              <label class="custom-control-label" for="userEdit`+mechanics[i].id+`">`+mechanics[i].text+`</label>
            </div>
          </div>
          <div class="col-5">
            <div class="custom-control custom-checkbox">
              <input class="custom-control-input p-1" id="userEdit`+mechanics[++i].id+`" type="checkbox" name="present-checkbox-edit">
              <label class="custom-control-label" for="userEdit`+mechanics[i].id+`">`+mechanics[i].text+`</label>
            </div>
          </div>
          <div class="col-1"></div>
        </div>`;
      }
    }
    $('#mechanicsEdit').append(element);
  }
  //ELECTRICANS - električarji
  var electricans = allUsers.filter(u => u.role == 'električar');
  for(var i = 0; i < electricans.length; i++){
    if(electricans.length % 2 == 0){
      element = `<div class="row">
        <div class="col-1"></div>
        <div class="col-5">
          <div class="custom-control custom-checkbox">
            <input class="custom-control-input p-1" id="userEdit`+electricans[i].id+`" type="checkbox" name="present-checkbox-edit">
            <label class="custom-control-label" for="userEdit`+electricans[i].id+`">`+electricans[i].text+`</label>
          </div>
        </div>
        <div class="col-5">
          <div class="custom-control custom-checkbox">
            <input class="custom-control-input p-1" id="userEdit`+electricans[++i].id+`" type="checkbox" name="present-checkbox-edit">
            <label class="custom-control-label" for="userEdit`+electricans[i].id+`">`+electricans[i].text+`</label>
          </div>
        </div>
        <div class="col-1"></div>
      </div>`;
    }
    else{
      if(i == (electricans.length - 1)){
        element = `<div class="row">
          <div class="col-3"></div>
          <div class="col-6">
            <div class="custom-control custom-checkbox">
              <input class="custom-control-input p-1" id="userEdit`+electricans[i].id+`" type="checkbox" name="present-checkbox-edit">
              <label class="custom-control-label" for="userEdit`+electricans[i].id+`">`+electricans[i].text+`</label>
            </div>
          </div>
          <div class="col-3"></div>
        </div>`;
      }
      else{
        element = `<div class="row">
          <div class="col-1"></div>
          <div class="col-5">
            <div class="custom-control custom-checkbox">
              <input class="custom-control-input p-1" id="userEdit`+electricans[i].id+`" type="checkbox" name="present-checkbox-edit">
              <label class="custom-control-label" for="userEdit`+electricans[i].id+`">`+electricans[i].text+`</label>
            </div>
          </div>
          <div class="col-5">
            <div class="custom-control custom-checkbox">
              <input class="custom-control-input p-1" id="userEdit`+electricans[++i].id+`" type="checkbox" name="present-checkbox-edit">
              <label class="custom-control-label" for="userEdit`+electricans[i].id+`">`+electricans[i].text+`</label>
            </div>
          </div>
          <div class="col-1"></div>
        </div>`;
      }
    }
    $('#electricansEdit').append(element);
  }
  //MECHASSEMBLERS - strojni monterji
  var mechAssemblers = allUsers.filter(u => u.role == 'strojni monter');
  for(var i = 0; i < mechAssemblers.length; i++){
    if(mechAssemblers.length % 2 == 0){
      element = `<div class="row">
        <div class="col-1"></div>
        <div class="col-5">
          <div class="custom-control custom-checkbox">
            <input class="custom-control-input p-1" id="userEdit`+mechAssemblers[i].id+`" type="checkbox" name="present-checkbox-edit">
            <label class="custom-control-label" for="userEdit`+mechAssemblers[i].id+`">`+mechAssemblers[i].text+`</label>
          </div>
        </div>
        <div class="col-5">
          <div class="custom-control custom-checkbox">
            <input class="custom-control-input p-1" id="userEdit`+mechAssemblers[++i].id+`" type="checkbox" name="present-checkbox-edit">
            <label class="custom-control-label" for="userEdit`+mechAssemblers[i].id+`">`+mechAssemblers[i].text+`</label>
          </div>
        </div>
        <div class="col-1"></div>
      </div>`;
    }
    else{
      if(i == (mechAssemblers.length - 1)){
        element = `<div class="row">
          <div class="col-3"></div>
          <div class="col-6">
            <div class="custom-control custom-checkbox">
              <input class="custom-control-input p-1" id="userEdit`+mechAssemblers[i].id+`" type="checkbox" name="present-checkbox-edit">
              <label class="custom-control-label" for="userEdit`+mechAssemblers[i].id+`">`+mechAssemblers[i].text+`</label>
            </div>
          </div>
          <div class="col-3"></div>
        </div>`;
      }
      else{
        element = `<div class="row">
          <div class="col-1"></div>
          <div class="col-5">
            <div class="custom-control custom-checkbox">
              <input class="custom-control-input p-1" id="userEdit`+mechAssemblers[i].id+`" type="checkbox" name="present-checkbox-edit">
              <label class="custom-control-label" for="userEdit`+mechAssemblers[i].id+`">`+mechAssemblers[i].text+`</label>
            </div>
          </div>
          <div class="col-5">
            <div class="custom-control custom-checkbox">
              <input class="custom-control-input p-1" id="userEdit`+mechAssemblers[++i].id+`" type="checkbox" name="present-checkbox-edit">
              <label class="custom-control-label" for="userEdit`+mechAssemblers[i].id+`">`+mechAssemblers[i].text+`</label>
            </div>
          </div>
          <div class="col-1"></div>
        </div>`;
      }
    }
    $('#mechAssemblersEdit').append(element);
  }
  //CNCOPERATORS - CNC operaterji
  var cncOperators = allUsers.filter(u => u.role == 'cnc' || u.role == 'cnc operater');
  for(var i = 0; i < cncOperators.length; i++){
    if(cncOperators.length % 2 == 0){
      element = `<div class="row">
        <div class="col-1"></div>
        <div class="col-5">
          <div class="custom-control custom-checkbox">
            <input class="custom-control-input p-1" id="userEdit`+cncOperators[i].id+`" type="checkbox" name="present-checkbox-edit">
            <label class="custom-control-label" for="userEdit`+cncOperators[i].id+`">`+cncOperators[i].text+`</label>
          </div>
        </div>
        <div class="col-5">
          <div class="custom-control custom-checkbox">
            <input class="custom-control-input p-1" id="userEdit`+cncOperators[++i].id+`" type="checkbox" name="present-checkbox-edit">
            <label class="custom-control-label" for="userEdit`+cncOperators[i].id+`">`+cncOperators[i].text+`</label>
          </div>
        </div>
        <div class="col-1"></div>
      </div>`;
    }
    else{
      if(i == (cncOperators.length - 1)){
        element = `<div class="row">
          <div class="col-3"></div>
          <div class="col-6">
            <div class="custom-control custom-checkbox">
              <input class="custom-control-input p-1" id="userEdit`+cncOperators[i].id+`" type="checkbox" name="present-checkbox-edit">
              <label class="custom-control-label" for="userEdit`+cncOperators[i].id+`">`+cncOperators[i].text+`</label>
            </div>
          </div>
          <div class="col-3"></div>
        </div>`;
      }
      else{
        element = `<div class="row">
          <div class="col-1"></div>
          <div class="col-5">
            <div class="custom-control custom-checkbox">
              <input class="custom-control-input p-1" id="userEdit`+cncOperators[i].id+`" type="checkbox" name="present-checkbox-edit">
              <label class="custom-control-label" for="userEdit`+cncOperators[i].id+`">`+cncOperators[i].text+`</label>
            </div>
          </div>
          <div class="col-5">
            <div class="custom-control custom-checkbox">
              <input class="custom-control-input p-1" id="userEdit`+cncOperators[++i].id+`" type="checkbox" name="present-checkbox-edit">
              <label class="custom-control-label" for="userEdit`+cncOperators[i].id+`">`+cncOperators[i].text+`</label>
            </div>
          </div>
          <div class="col-1"></div>
        </div>`;
      }
    }
    $('#CNCoperatorsEdit').append(element);
  }
  //WELDERS - varilci
  var welders = allUsers.filter(u => u.role == 'varilec');
  for(var i = 0; i < welders.length; i++){
    if(welders.length % 2 == 0){
      element = `<div class="row">
        <div class="col-1"></div>
        <div class="col-5">
          <div class="custom-control custom-checkbox">
            <input class="custom-control-input p-1" id="userEdit`+welders[i].id+`" type="checkbox" name="present-checkbox-edit">
            <label class="custom-control-label" for="userEdit`+welders[i].id+`">`+welders[i].text+`</label>
          </div>
        </div>
        <div class="col-5">
          <div class="custom-control custom-checkbox">
            <input class="custom-control-input p-1" id="userEdit`+welders[++i].id+`" type="checkbox" name="present-checkbox-edit">
            <label class="custom-control-label" for="userEdit`+welders[i].id+`">`+welders[i].text+`</label>
          </div>
        </div>
        <div class="col-1"></div>
      </div>`;
    }
    else{
      if(i == (welders.length - 1)){
        element = `<div class="row">
          <div class="col-3"></div>
          <div class="col-6">
            <div class="custom-control custom-checkbox">
              <input class="custom-control-input p-1" id="userEdit`+welders[i].id+`" type="checkbox" name="present-checkbox-edit">
              <label class="custom-control-label" for="userEdit`+welders[i].id+`">`+welders[i].text+`</label>
            </div>
          </div>
          <div class="col-3"></div>
        </div>`;
      }
      else{
        element = `<div class="row">
          <div class="col-1"></div>
          <div class="col-5">
            <div class="custom-control custom-checkbox">
              <input class="custom-control-input p-1" id="userEdit`+welders[i].id+`" type="checkbox" name="present-checkbox-edit">
              <label class="custom-control-label" for="userEdit`+welders[i].id+`">`+welders[i].text+`</label>
            </div>
          </div>
          <div class="col-5">
            <div class="custom-control custom-checkbox">
              <input class="custom-control-input p-1" id="userEdit`+welders[++i].id+`" type="checkbox" name="present-checkbox-edit">
              <label class="custom-control-label" for="userEdit`+welders[i].id+`">`+welders[i].text+`</label>
            </div>
          </div>
          <div class="col-1"></div>
        </div>`;
      }
    }
    $('#weldersEdit').append(element);
  }
  //SERVISER - serviserji
  var serviser = allUsers.filter(u => u.role == 'serviser');
  for(var i = 0; i < serviser.length; i++){
    if(serviser.length % 2 == 0){
      element = `<div class="row">
        <div class="col-1"></div>
        <div class="col-5">
          <div class="custom-control custom-checkbox">
            <input class="custom-control-input p-1" id="userEdit`+serviser[i].id+`" type="checkbox" name="present-checkbox-edit">
            <label class="custom-control-label" for="userEdit`+serviser[i].id+`">`+serviser[i].text+`</label>
          </div>
        </div>
        <div class="col-5">
          <div class="custom-control custom-checkbox">
            <input class="custom-control-input p-1" id="userEdit`+serviser[++i].id+`" type="checkbox" name="present-checkbox-edit">
            <label class="custom-control-label" for="userEdit`+serviser[i].id+`">`+serviser[i].text+`</label>
          </div>
        </div>
        <div class="col-1"></div>
      </div>`;
    }
    else{
      if(i == (serviser.length - 1)){
        element = `<div class="row">
          <div class="col-3"></div>
          <div class="col-6">
            <div class="custom-control custom-checkbox">
              <input class="custom-control-input p-1" id="userEdit`+serviser[i].id+`" type="checkbox" name="present-checkbox-edit">
              <label class="custom-control-label" for="userEdit`+serviser[i].id+`">`+serviser[i].text+`</label>
            </div>
          </div>
          <div class="col-3"></div>
        </div>`;
      }
      else{
        element = `<div class="row">
          <div class="col-1"></div>
          <div class="col-5">
            <div class="custom-control custom-checkbox">
              <input class="custom-control-input p-1" id="userEdit`+serviser[i].id+`" type="checkbox" name="present-checkbox-edit">
              <label class="custom-control-label" for="userEdit`+serviser[i].id+`">`+serviser[i].text+`</label>
            </div>
          </div>
          <div class="col-5">
            <div class="custom-control custom-checkbox">
              <input class="custom-control-input p-1" id="userEdit`+serviser[++i].id+`" type="checkbox" name="present-checkbox-edit">
              <label class="custom-control-label" for="userEdit`+serviser[i].id+`">`+serviser[i].text+`</label>
            </div>
          </div>
          <div class="col-1"></div>
        </div>`;
      }
    }
    $('#serviserEdit').append(element);
  }
  //OTHERS - ostali ki še niso bili dodani na seznam ostalih kategorij
  var others = allUsers.filter(u => u.role != 'serviser' && u.role != 'konstrukter' && u.role != 'konstruktor' && u.role != 'programer plc-jev' && u.role != 'programer robotov' && u.role != 'električar' && u.role != 'strojnik' && u.role != 'strojni monter' && u.role != 'cnc' && u.role != 'cnc operater' && u.role != 'varilec' && u.role.substring(0,5) != 'vodja' && u.role.substring(0,4) != 'info' && u.text != 'Srečko Potočnik' && u.text != 'Cveto Jakopič' && u.text != 'Boštjan Skarlovnik');
  for(var i = 0; i < others.length; i++){
    if(others.length % 2 == 0){
      element = `<div class="row">
        <div class="col-1"></div>
        <div class="col-5">
          <div class="custom-control custom-checkbox">
            <input class="custom-control-input p-1" id="userEdit`+others[i].id+`" type="checkbox" name="present-checkbox-edit">
            <label class="custom-control-label" for="userEdit`+others[i].id+`">`+others[i].text+`</label>
          </div>
        </div>
        <div class="col-5">
          <div class="custom-control custom-checkbox">
            <input class="custom-control-input p-1" id="userEdit`+others[++i].id+`" type="checkbox" name="present-checkbox-edit">
            <label class="custom-control-label" for="userEdit`+others[i].id+`">`+others[i].text+`</label>
          </div>
        </div>
        <div class="col-1"></div>
      </div>`;
    }
    else{
      if(i == (others.length - 1)){
        element = `<div class="row">
          <div class="col-3"></div>
          <div class="col-6">
            <div class="custom-control custom-checkbox">
              <input class="custom-control-input p-1" id="userEdit`+others[i].id+`" type="checkbox" name="present-checkbox-edit">
              <label class="custom-control-label" for="userEdit`+others[i].id+`">`+others[i].text+`</label>
            </div>
          </div>
          <div class="col-3"></div>
        </div>`;
      }
      else{
        element = `<div class="row">
          <div class="col-1"></div>
          <div class="col-5">
            <div class="custom-control custom-checkbox">
              <input class="custom-control-input p-1" id="userEdit`+others[i].id+`" type="checkbox" name="present-checkbox-edit">
              <label class="custom-control-label" for="userEdit`+others[i].id+`">`+others[i].text+`</label>
            </div>
          </div>
          <div class="col-5">
            <div class="custom-control custom-checkbox">
              <input class="custom-control-input p-1" id="userEdit`+others[++i].id+`" type="checkbox" name="present-checkbox-edit">
              <label class="custom-control-label" for="userEdit`+others[i].id+`">`+others[i].text+`</label>
            </div>
          </div>
          <div class="col-1"></div>
        </div>`;
      }
    }
    $('#othersEdit').append(element);
  }
}
var allActivities, allMeetings, allUsers, allActiveMeetings, allStatus, allProjects = [];
var loggedUser;
var selectedCategory = 0;
let meetingsMode = 0;