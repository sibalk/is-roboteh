//IN THIS SCRIPT//
//// get all messages for specific id, post new message for specific id, refresh for new messages ////
$(function(){
  $("#modalNotes").on("hidden.bs.modal", function () {
    // put your default event here
    clearInterval(intervalMessages);
    messagesModalOpenForTask = false;
    messagesModalOpenForSubtask = false;
  });
  $('#newMessageBtn').on('click', onEventAddMessage);
  $('#newMessage').on('keyup', onEventAddMessage);
})
function onClickOpenMessagesModal(event) {
  openMessagesModal(event.data.id, event.data.type);
}
function openMessagesModal(id, type){
  var projectId;
  var taskId;
  var subtaskId;
  messagesProjectId = null;
  if(projectId)
    messagesProjectId = projectId;
  messagesTaskId = null;
  messagesSubtaskId = null;
  if(type == 0){
    projectId = id;
  }
  else if(type == 1){
    taskId = id;
    messagesTaskId = id;
  }
  else if(type == 2){
    subtaskId = id;
    messagesSubtaskId = id;
    if(activeTaskId)
      messagesTaskId = activeTaskId;
  }
  //debugger;
  if(messagesId){
    if(messagesType == type && messagesId == id){
      //do refresh
      $('#modalNotes').modal('toggle');
      if(messagesType == 1)
        messagesModalOpenForTask = true;
      else if(messagesType == 2)
        messagesModalOpenForSubtask = true;
      if(messagesModalOpenForTask || messagesModalOpenForSubtask) dismissMessageNotifications();
      setTimeout(()=>{$('#newMessage').focus();},500);
      intervalMessages = setInterval(refreshMessages,2000);
    }
    else{
      messagesType = type;
      messagesId = id;
      $.get('/messages/get', {projectId,taskId,subtaskId}, function(data){
        allMessages = data.messages;
        //debugger;
        drawMessages();
      })  
    }
  }
  else{
    messagesType = type;
    messagesId = id;
    $.get('/messages/get', {projectId,taskId,subtaskId}, function(data){
      allMessages = data.messages;
      //debugger;
      drawMessages();
    })
  }
}
//draw messages for first time
function drawMessages(){
  $('#messagesBody').empty();  
  for(var i = 0; i < allMessages.length; i++){
    var element = ``;
    if(allMessages[i].name == loggedUser.name && allMessages[i].surname == loggedUser.surname){
      element = `<div class="card-message bg-color-message-me mb-3" style="max-width: 20rem;" id="message`+allMessages[i].id+`">
      <div class="card-body-message">
      <p class="card-text">`+allMessages[i].note+`</p>
      </div>
      <div class="card-subtitle-message text-muted">`+new Date(allMessages[i].date).toLocaleString('sl')+`</div>
      </div>`;
    }
    else{
      var senderText = `<div class="sender">`+allMessages[i].name + ' ' + allMessages[i].surname +`</div>`;
      if(lastSender && (lastSender == allMessages[i].id_user))
        senderText = ``;
      element = senderText+`
      <div class="d-flex justify-content-end">
      <div class="card-message bg-color-message-sender mb-3" style="max-width: 20rem;" id="message`+allMessages[i].id+`">
      <div class="card-body-message">
      <p class="card-text">`+allMessages[i].note+`</p>
      </div>
      <div class="card-subtitle-message sender text-muted">`+new Date(allMessages[i].date).toLocaleString('sl')+`</div>
      </div>
      </div>`;
    }
    if(i == (allMessages.length-1)){
      lastMessageId = allMessages[i].id;
      lastMessageDate = allMessages[i].date;
    }
    lastSender = allMessages[i].id_user;
    $('#messagesBody').append(element);
  }
  $('#modalNotes').modal('toggle');
  if(messagesType == 1)
    messagesModalOpenForTask = true;
  else if(messagesType == 2)
    messagesModalOpenForSubtask = true;
  if(messagesModalOpenForTask || messagesModalOpenForSubtask) dismissMessageNotifications();
  //$('#newMessage').focus();
  setTimeout(scrollModalMessageBody,500);
  setTimeout(()=>{$('#newMessage').focus();},500);
  //$("#messagesBody").animate({ scrollTop: $("#messagesBody").height() }, "slow");
  intervalMessages = setInterval(refreshMessages,2000);
}
//move modal body to last message
function scrollModalMessageBody(){
  $("#messagesBody").animate({ scrollTop: $("#messagesBody")[0].scrollHeight });
}
function onEventAddMessage(event) {
  if(event.which == 13 || event.which == 1){
    //debugger;
    addNewMessage();
  }
}
//add new message and get back any missed messages
function addNewMessage(){
  //debugger;
  var newMessage = $('#newMessage').val();
  var bodyHeight = $("#messagesBody")[0].scrollHeight;
  //check if message even exist, cant send empty message
  if(newMessage){
    //check if message is over 500 characters
    if(newMessage.length >= 500){
      $('#newMessage').addClass('is-invalid')
      $('#newMessage').attr('data-content', 'Sporočilo je lahko dolgo največ 500 znakov!');
      $('#newMessage').popover('show');
    }
    else{
      //remove invalid class and empty popover text and hide it
      $('#newMessage').removeClass('is-invalid')
      $('#newMessage').attr('data-content', '');
      $('#newMessage').popover('hide');

      $.post('/messages/post',{newMessage,messagesId,messagesType,lastMessageId}, function(data){
        //debugger;
        //first add missing messages
        for(var i = 0; i < data.newMessages.length; i++){
          var element = ``;
          if(data.newMessages[i].name == loggedUser.name && data.newMessages[i].surname == loggedUser.surname){
            element = `<div class="card-message bg-color-message-me mb-3" style="max-width: 20rem;" id="message`+data.newMessages[i].id+`">
            <div class="card-body-message">
            <p class="card-text">`+data.newMessages[i].note+`</p>
            </div>
            <div class="card-subtitle-message text-muted">`+new Date(data.newMessages[i].date).toLocaleString('sl')+`</div>
            </div>`;
          }
          else{
            var senderText = `<div class="sender">`+data.newMessages[i].name + ' ' + data.newMessages[i].surname +`</div>`;
            if(lastSender && (lastSender == data.newMessages[i].id_user))
              senderText = ``;
            element = senderText+`
            <div class="d-flex justify-content-end">
            <div class="card-message bg-color-message-sender mb-3" style="max-width: 20rem;" id="message`+data.newMessages[i].id+`">
            <div class="card-body-message">
            <p class="card-text">`+data.newMessages[i].note+`</p>
            </div>
            <div class="card-subtitle-message sender text-muted">`+new Date(data.newMessages[i].date).toLocaleString('sl')+`</div>
            </div>
            </div>`;
          }
          //lastMessageId = data.newMessages[i].id;
          //lastMessageDate = data.newMessages[i].date;
          lastSender = data.newMessages[i].id_user;
          $('#messagesBody').append(element);
        }
        //append new message to modal body
        var element = `<div class="card-message bg-color-message-me mb-3" style="max-width: 20rem;" id="message`+data.date.id+`">
        <div class="card-body-message">
        <p class="card-text">`+newMessage+`</p>
        </div>
        <div class="card-subtitle-message text-muted">`+new Date(data.date.date).toLocaleString('sl')+`</div>
        </div>`;
        $('#messagesBody').append(element);
        $('#newMessage').val('');
        lastMessageId = data.date.id;
        lastMessageDate = data.date.date;
        lastSender = data.date.id_user;
        if(bodyHeight == ($('#messagesBody').scrollTop() + $('#messagesBody').innerHeight()))
          setTimeout(scrollModalMessageBody);
        //setTimeout(scrollModalMessageBody);
        if(projectId){
          debugger;
          //addNewProjectChange(4,1,projectId,messagesTaskId,messagesSubtaskId,data.date.id);
          addChangeSystem(1,4,projectId,messagesTaskId,messagesSubtaskId,null,null,data.date.id);
        }
        else{
          var subId;
          if($('#taskName'+messagesTaskId).length){
            //servis page
            subId = $('#taskName'+messagesTaskId).html().split('(');
            subId = subId[subId.length-1];
            subId = subId.substring(0,subId.length-1);
            subId = allSubscribers.find(s => s.text == subId).id;
          }
          else{
            //my tasks page
            subId = task.id_subscriber;
          }
          debugger;
          //addNewServisChange(11,1,subId,messagesTaskId,messagesSubtaskId,data.date.id);
          addChangeSystem(1,11,null,messagesTaskId,messagesSubtaskId,null,null,data.date.id,subId);
        }
        //correct the icon
        if(messagesSubtaskId){
          //new message for subtask
          if($('#subtask'+messagesSubtaskId).find('.fa-comments').hasClass('far'))
            $('#subtask'+messagesSubtaskId).find('.fa-comments').removeClass('far').addClass('fas');
        }
        else{
          //new message for task
          if($('#task'+messagesTaskId).find('.fa-comments').hasClass('far'))
            $('#task'+messagesTaskId).find('.fa-comments').removeClass('far').addClass('fas');
        }
      })
    }
  }
}
//do refresh --> get back any missed messages
function refreshMessages(){
  //debugger;
  $.get('/messages/refresh',{messagesId,messagesType,lastMessageId}, function(data){
    //debugger;
    var bodyHeight = $("#messagesBody")[0].scrollHeight;
    //add any missing messages
    for(var i = 0; i < data.newMessages.length; i++){
      var element = ``;
      if(data.newMessages[i].name == loggedUser.name && data.newMessages[i].surname == loggedUser.surname){
        element = `<div class="card-message bg-color-message-me mb-3" style="max-width: 20rem;" id="message`+data.newMessages[i].id+`">
        <div class="card-body-message">
        <p class="card-text">`+data.newMessages[i].note+`</p>
        </div>
        <div class="card-subtitle-message text-muted">`+new Date(data.newMessages[i].date).toLocaleString('sl')+`</div>
        </div>`;
      }
      else{
        var senderText = `<div class="sender">`+data.newMessages[i].name + ' ' + data.newMessages[i].surname +`</div>`;
        if(lastSender && (lastSender == data.newMessages[i].id_user))
          senderText = ``;
        element = senderText+`
        <div class="d-flex justify-content-end">
        <div class="card-message bg-color-message-sender mb-3" style="max-width: 20rem;" id="message`+data.newMessages[i].id+`">
        <div class="card-body-message">
        <p class="card-text">`+data.newMessages[i].note+`</p>
        </div>
        <div class="card-subtitle-message sender text-muted">`+new Date(data.newMessages[i].date).toLocaleString('sl')+`</div>
        </div>
        </div>`;
      }
      if(i == (data.newMessages.length-1)){
        lastMessageId = data.newMessages[i].id;
        lastMessageDate = data.newMessages[i].date;
      }
      //lastMessageId = data.newMessages[i].id;
      //lastMessageDate = data.newMessages[i].date;
      lastSender = data.newMessages[i].id_user;
      $('#messagesBody').append(element);
    }
    if(data.newMessages.length > 0){
      //if(bodyHeight == ($('#messagesBody').scrollTop() + $('#messagesBody').innerHeight()))
        setTimeout(scrollModalMessageBody);
    }
  })
}
function getLocalDateForDB(input){
  var date = new Date(input);
  var y = date.getFullYear();
  var m = date.getMonth()+1;
  var d = date.getDate();
  var h = date.getHours();
  var min = date.getMinutes();
  var s = date.getSeconds();
  var mil = date.getMilliseconds();
  if(m<10) m = '0'+m;
  if(d<10) d = '0'+d;
  if(h<10) h = '0'+h;
  if(min<10) min = '0'+min;
  if(s<10) s = '0'+s;
  return y+'-'+m+'-'+d+' '+h+':'+m+':'+s+'.'+mil;
}
var intervalMessages;
var lastMessageId;
var lastMessageDate; // date of last message, to check if there were some missed messages when sending new message
var lastSender; //id of last message, if the last sender is same no need to show his name again
var allMessages;
var messagesType; // 0 is projectId, 1 is taskId, 2 is subtaskId
var messagesId; // id of project/task/subtask, depends on type
//////for changes//////
//var messagesProjectId;
//var messagesTaskId;
//var messagesSubtaskId;