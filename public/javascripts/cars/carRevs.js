$(function(){
  loggedUser = {name:$('#loggedUser').html().split(" (")[0].split(" ")[0], surname:$('#loggedUser').html().split(" (")[0].split(" ")[1], role:$('#loggedUser').html().split(" (")[1].split(")")[0].toLowerCase()};
  if (loggedUser.role.toLowerCase() == 'admin' || loggedUser.role.toLowerCase() == 'tajnik' || loggedUser.role.toLowerCase() == 'komercialist'  || loggedUser.role == 'komerciala' || loggedUser.role == 'računovodstvo'){
    $('#add-car-button').removeClass('d-none');
  }
  else{
    $('#editCarButton').addClass('d-none');
    $('#deleteCarButton').addClass('d-none');
    $('#addCarNoteButton').addClass('ml-auto');
  }

  // theme for calander
  mobiscroll.setOptions({
    theme: 'material',
    themeVariant: 'light'
  });
  formatDate = mobiscroll.util.datetime.formatDate;

  // popup for adding and editing setup
  popup = mobiscroll.popup('#work-order-popup', {
    display: 'bottom',
    contentPadding: false,
    fullScreen: true,
    scrollLock: false,
    onClose: function () {
      if (deleteEvent) {
        calendar.removeEvent(tempEvent);
      } else if (restoreEvent) {
        calendar.updateEvent(oldEvent);
      }
    },
    responsive: {
      medium: {
        display: 'anchored',
        width: 520,
        fullScreen: false,
        touchUi: false
      }
    }
  });
  
  // tooltip init
  tooltip = mobiscroll.popup('#custom-event-tooltip-popup', {
    display: 'anchored',
    touchUi: false,
    showOverlay: false,
    contentPadding: false,
    closeOnOverlayClick: false,
    width: 350,
    onInit: function () {
      tooltip.addEventListener('mouseenter', function (e) {
        if (timer) {
          clearTimeout(timer);
          timer = null;
        }
      });

      tooltip.addEventListener('mouseleave', function () {
        timer = setTimeout(function () {
          tooltip.close();
        }, 200);
      });
    }
  });
  // custom tooltip, used for seeing all info for reservation and editing additional info
  tooltip = mobiscroll.popup('#custom-event-tooltip-popup', {
    display: 'anchored',
    touchUi: false,
    showOverlay: false,
    contentPadding: false,
    closeOnOverlayClick: false,
    width: 350,
    onInit: function () {
      tooltip.addEventListener('mouseenter', function (e) {
        if (timer) {
          clearTimeout(timer);
          timer = null;
        }
      });

      tooltip.addEventListener('mouseleave', function () {
        timer = setTimeout(function () {
          tooltip.close();
        }, 200);
      });
    }
  });
  // event on keypress enter on location update
  tooltipLocation.addEventListener('keypress', function (e) {
    if (e.key == 'Enter'){
      updateTooltip(tmpCR.finished, tooltipLocation.value, tmpCR.keys);
    }
  })
  // event on delete selected event for deleting a reservation (changing activity to false)
  deleteButton.addEventListener('click', function () {
    // var tmpE = args.event;
    var data = {activity: false};
    $.ajax({
      type: 'DELETE',
      url: '/apiv2/cars/reservations/'+tmpCR.id,
      contentType: 'application/json',
      data: JSON.stringify(data), // access in body
    }).done(function (resp) {
      // console.log('SUCCESS');
      // delete current event on button click and successful delete in database
      calendar.removeEvent(tmpCR);
      tooltip.close();
    
      // save a local reference to the deleted event
      var deletedCR = tmpCR;
    
      mobiscroll.snackbar({
        button: {
            action: function () {
              // undo delete - update active to true and add event back to calendar on success
              var data = {activity: true};
              $.ajax({
                type: 'DELETE',
                url: '/apiv2/cars/reservations/'+tmpCR.id,
                contentType: 'application/json',
                data: JSON.stringify(data), // access in body
              }).done(function (resp) {
                // console.log('SUCCESS');
                // delete current event on button click and successful delete in database
                calendar.addEvent(deletedCR);

              }).fail(function (resp) {
                //console.log('FAIL');
                //debugger;
                tooltip.close();
                $('#errorModalMsg').text('Napaka pri razveljavitvi brisanja rezervacije v podatkovni bazi. Osvežite stran in ob ponovni napaki obvestite admninistratorja RIS-a.')
                $('#modalError').modal('toggle');
              });
            },
            text: 'Razveljavi'
          },
          message: 'Rezervacija je odstranjena'
      });

    }).fail(function (resp) {
      //console.log('FAIL');
      //debugger;
      // tooltip.close();
      $('#errorModalMsg').text('Napaka pri brisanju rezervacije v podatkovni bazi. Osvežite stran in ob ponovni napaki obvestite admninistratorja RIS-a.')
      $('#modalError').modal('toggle');
    });  
  });
  // refresh button
  $('#refreshData').on('click', refreshData);
  // event listener for changing selected user, for changing user reservation
  $('#tooltip-reservation-select').on('change', function() {
    if (tooltip.isVisible() && tooltipOwner.getInnerHTML() == tmpCR.owner_name + ' (' + new Date(tmpCR.created).toLocaleString('sl') + ')')
      updateTooltip(tmpCR.finished, tmpCR.location, tmpCR.keys, $('#tooltip-reservation-select').val());
  })
  // event listener for changing/switching keys switch/checkbox
  tooltipKeys.addEventListener('change', function () {
    updateTooltip(tmpCR.finished, tmpCR.location, this.checked, tmpCR.user);
  })
  // event listener for clicking on save location button
  saveButton.addEventListener('click', function () {
    updateTooltip(tmpCR.finished, tooltipLocation.value, tmpCR.keys, tmpCR.user);
  })
  // event listener for clicking on status button
  statusButton.addEventListener('click', function () {
    // do PUT call and on success update parameters on client side and update html elements else show error
    updateTooltip(!tmpCR.finished, tmpCR.location, tmpCR.keys, tmpCR.user);
  });
  // UPDATE VALUES ON PAGE AFTER EVERYTING LOADED
  // set range value if previously selected custom range
  setTimeout(() => {
    localStorage.carRevSelectRange && localStorage.carRevSelectRange == 'custom' && localStorage.calendarRangeStart && localStorage.calendarRangeEnd ? selectRange.setOptions({data: [ { text: 'Dan', value: 'day', disabled: false }, { text: 'Teden', value: 'week', disabled: false }, { text: 'Teden (razširjeno)', value: 'weekExt', disabled: false }, { text: '2 tedna', value: '2week', disabled: false }, { text: '4 Tedne', value: 'weekMonth', disabled: false }, { text: 'Mesec', value: 'month', disabled: false }, { text: '2 meseca', value: '2month', disabled: false }, { text: 'Pol leta', value: '6month', disabled: false }, { text: 'Leto', value: 'year', disabled: false }, { text: 'Ročno', value: 'custom', disabled: false }]}) : selectRange.setOptions({data: [ { text: 'Dan', value: 'day', disabled: false }, { text: 'Teden', value: 'week', disabled: false }, { text: 'Teden (razširjeno)', value: 'weekExt', disabled: false }, { text: '2 tedna', value: '2week', disabled: false }, { text: '4 Tedne', value: 'weekMonth', disabled: false }, { text: 'Mesec', value: 'month', disabled: false }, { text: '2 meseca', value: '2month', disabled: false }, { text: 'Pol leta', value: '6month', disabled: false }, { text: 'Leto', value: 'year', disabled: false }, { text: 'Ročno', value: 'custom', disabled: true }]});
    if (localStorage.carRevSelectRange && localStorage.carRevSelectRange == 'custom' && localStorage.calendarRangeStart && localStorage.calendarRangeEnd)
      setTimeout(() => { selectRange.setVal(localStorage.carRevSelectRange); }, 200);
    else if (localStorage.carRevSelectRange && localStorage.carRevSelectRange == 'day')
      setTimeout(() => { selectRange.setVal(localStorage.carRevSelectRange); }, 200);
    else if (localStorage.carRevSelectRange && localStorage.carRevSelectRange == '2week')
      setTimeout(() => { selectRange.setVal(localStorage.carRevSelectRange); }, 200);
    else if (localStorage.carRevSelectRange && localStorage.carRevSelectRange == 'week')
      setTimeout(() => { selectRange.setVal(localStorage.carRevSelectRange); }, 200);
    else if (localStorage.carRevSelectRange && localStorage.carRevSelectRange == 'weekExt')
      setTimeout(() => { selectRange.setVal(localStorage.carRevSelectRange); }, 200);
    else if (localStorage.carRevSelectRange && localStorage.carRevSelectRange == 'weekMonth')
      setTimeout(() => { selectRange.setVal(localStorage.carRevSelectRange); }, 200);
    else if (localStorage.carRevSelectRange && localStorage.carRevSelectRange == 'month')
      setTimeout(() => { selectRange.setVal(localStorage.carRevSelectRange); }, 200);
    else if (localStorage.carRevSelectRange && localStorage.carRevSelectRange == '2month')
      setTimeout(() => { selectRange.setVal(localStorage.carRevSelectRange); }, 200);
    else if (localStorage.carRevSelectRange && localStorage.carRevSelectRange == '6month')
      setTimeout(() => { selectRange.setVal(localStorage.carRevSelectRange); }, 200);
    else if (localStorage.carRevSelectRange && localStorage.carRevSelectRange == 'year')
      setTimeout(() => { selectRange.setVal(localStorage.carRevSelectRange); }, 200);
    else
      setTimeout(() => { selectRange.setVal('2week'); }, 200);

    // refresh button
    $('#refreshData').on('click', refreshData);
  }, 700);
  // setting interval for refreshing data when 5 minutes has passed since last data update
  setInterval(() => {
    refreshTimerCount++;
    if (refreshTimerCount >= 300){
      let tmpS = moment(calendar._firstDay).add(-1, 'days').format("YYYY-MM-DDTHH:mm");
      let tmpE = moment(calendar._lastDay).add(1, 'days').format("YYYY-MM-DDTHH:mm");
      getData(tmpS,tmpE);
      console.log('refresh data');
      refreshTimerCount = 0;
    }
  }, 1000);
  // adding row class to head of calendar for better visibility on smaller screens
  setTimeout(() => {
    $('.mbsc-calendar-controls.mbsc-material').addClass('row');
  }, 300);
})
// do update on tooltip change
function updateTooltip(status, location, keys, user) {
  // check if user has changed and with it its department so the color of the reservation changes for better visibility
  let tmpUserRev = users.find(u => u.id == user);
  let tmpColorRev = tmpCR.color;
  if (tmpUserRev.role.toLowerCase() == 'admin' || tmpUserRev.role.toLowerCase() == 'vodja projektov' || tmpUserRev.role.toLowerCase() == 'vodja')
    tmpColorRev = '#212529';
  else if (tmpUserRev.role.toLowerCase() == 'programer plc-jev' || tmpUserRev.role.toLowerCase() == 'programer robotov')
    tmpColorRev = '#325d88';
  else if (tmpUserRev.role.toLowerCase() == 'električar' || tmpUserRev.role.toLowerCase() == 'vodja električarjev')
    tmpColorRev = '#f47c3c';
  else if (tmpUserRev.role.toLowerCase() == 'strojnik' || tmpUserRev.role.toLowerCase() == 'vodja strojnikov' || tmpUserRev.role.toLowerCase() == 'vodja cnc obdelave' || tmpUserRev.role.toLowerCase() == 'varilec' || tmpUserRev.role.toLowerCase() == 'strojni monter' ||  tmpUserRev.role.toLowerCase() =='cnc operater')
    tmpColorRev = '#4baf4f';
  else if (tmpUserRev.role.toLowerCase() == 'serviser')
    tmpColorRev = '#d9534f';
  else if (tmpUserRev.role.toLowerCase() == 'konstrukter')
    tmpColorRev = '#80b139';
  else
    tmpColorRev = '#1a73e8';
  var data = {start: tmpCR.start, end: tmpCR.end, color: tmpColorRev, keys: keys, location: location, userId: user, status: status, car: tmpCR.car};
  $.ajax({
    type: 'PUT',
    url: '/apiv2/cars/reservations/'+tmpCR.id,
    contentType: 'application/json',
    data: JSON.stringify(data), // access in body
  }).done(function (resp) {
    // console.log('SUCCESS');
    tmpCR.finished = status;
    tmpCR.location = tooltipLocation.value;
    tooltipAdditionalTitle.innerHTML = tmpCR.location ? tmpCR.location : '';
    tmpCR.keys = keys ? true : false;
    tooltipKeysText.innerHTML = tmpCR.keys ? 'Ima' : 'Nima';
    // tmpCR.user_name = tooltipUser.options[tooltipUser.selectedIndex].text;
    // tmpCR.user = tooltipUser.value;
    tmpCR.user_name = $('#tooltip-reservation-select option:selected').text();
    tmpCR.user = $("#tooltip-reservation-select").val();
    tmpCR.title = tmpCR.user_name;
    tmpCR.color = tmpColorRev;
    if (tmpCR.finished) {
      $('#status-button').text('V teku');
      $('#status-button').removeClass('btn-success').addClass('btn-warning');
    } else {
      $('#status-button').text('Zaključi');
      $('#status-button').removeClass('btn-warning').addClass('btn-success');
    }
    tooltipStatus.innerHTML = tmpCR.finished ? 'Zaključeno' : 'V teku';
    calendar.updateEvent(tmpCR);

  }).fail(function (resp) {
    //console.log('FAIL');
    //debugger;
    // popup.close();
    $('#errorModalMsg').text('Napaka pri posodobitvi podatkov v podatkovni bazi. Osvežite stran in ob ponovni napaki obvestite admninistratorja RIS-a.')
    $('#modalError').modal('toggle');
  });
}
// setup the calendar - first init of calendar, in a function because need to wait for getting all resources and data/events
function initCalendar() {
  moment.tz.add('Europe/Belgrade|CET CEST|-10 -20|01010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010|-19RC0 3IP0 WM0 1fA0 1cM0 1cM0 1rc0 Qo0 1vmo0 1cM0 1cM0 1fA0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1fA0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1fA0 1o00 11A0 1o00 11A0 1o00 11A0 1qM0 WM0 1qM0 WM0 1qM0 11A0 1o00 11A0 1o00 11A0 1qM0 WM0 1qM0 WM0 1qM0 WM0 1qM0 11A0 1o00 11A0 1o00 11A0 1qM0 WM0 1qM0 WM0 1qM0 11A0 1o00 11A0 1o00 11A0 1o00 11A0 1qM0 WM0 1qM0 WM0 1qM0 11A0 1o00 11A0 1o00 11A0 1qM0 WM0 1qM0 WM0 1qM0 11A0 1o00 11A0 1o00 11A0 1o00 11A0 1qM0 WM0 1qM0 WM0 1qM0 11A0 1o00 11A0 1o00 11A0 1qM0 WM0 1qM0 WM0 1qM0 WM0 1qM0 11A0 1o00 11A0 1o00|12e5')
  mobiscroll.momentTimezone.moment = moment;

  // get saved view option if it exist in localStorage
  let viewType;
  let colorType = localStorage.carRevSelectRange && localStorage.carRevSelectRange == 'day' ?
  [{ slot: 1, allDay: 'true', start: new Date('2025-01-01'), end: new Date('2025-01-01'), cssClass: 'md-dots-bg' },{ slot: 1, allDay: 'true', start: new Date('2025-01-02'), end: new Date('2025-01-02'), cssClass: 'md-dots-bg' },{ slot: 1, allDay: 'true', start: new Date('2025-02-08'), end: new Date('2025-02-08'), cssClass: 'md-dots-bg' },{ slot: 1, allDay: 'true', start: new Date('2025-04-21'), end: new Date('2025-04-21'), cssClass: 'md-dots-bg' },{ slot: 1, allDay: 'true', start: new Date('2025-04-27'), end: new Date('2025-04-27'), cssClass: 'md-dots-bg' },{ slot: 1, allDay: 'true', start: new Date('2025-05-01'), end: new Date('2025-05-02'), cssClass: 'md-dots-bg' },{ slot: 1, allDay: 'true', start: new Date('2025-05-19'), end: new Date('2025-05-19'), cssClass: 'md-dots-bg' },{ slot: 1, allDay: 'true', start: new Date('2025-06-25'), end: new Date('2025-06-25'), cssClass: 'md-dots-bg' },{ slot: 1, allDay: 'true', start: new Date('2025-08-15'), end: new Date('2025-08-15'), cssClass: 'md-dots-bg' },{ slot: 1, allDay: 'true', start: new Date('2025-10-31'), end: new Date('2025-11-1 21:00'), cssClass: 'md-dots-bg' },{ slot: 1, allDay: 'true', start: new Date('2025-12-25'), end: new Date('2025-12-26')},{ slot: 1, allDay: 'true', start: new Date('2026-01-01'), end: new Date('2026-01-02'), cssClass: 'md-dots-bg' },{ slot: 1, allDay: 'true', start: new Date('2023-07-17'), end: new Date('2023-08-16'), cssClass: 'md-stripes-bg' },{ slot: 1, allDay: 'true', start: new Date('2023-04-24'), end: new Date('2023-04-28'), cssClass: 'md-rect-bg' },{ slot: 1, allDay: 'true', start: new Date('2023-12-27'), end: new Date('2023-12-29'), cssClass: 'md-rect-bg' },{ slot: 1, allDay: 'true', start: new Date('2023-10-30'), end: new Date('2023-10-30'), cssClass: 'md-rect-bg' },{background: '#a5ceff4d', slot: 1, recurring: { repeat: 'weekly', weekDays: 'SU,SA' }}]
  :
  [{ slot: 1, allDay: 'true', start: new Date('2025-01-01'), end: new Date('2025-01-01'), cssClass: 'md-dots-bg' },{ slot: 1, allDay: 'true', start: new Date('2025-01-02'), end: new Date('2025-01-02'), cssClass: 'md-dots-bg' },{ slot: 1, allDay: 'true', start: new Date('2025-02-08'), end: new Date('2025-02-08'), cssClass: 'md-dots-bg' },{ slot: 1, allDay: 'true', start: new Date('2025-04-21'), end: new Date('2025-04-21'), cssClass: 'md-dots-bg' },{ slot: 1, allDay: 'true', start: new Date('2025-04-27'), end: new Date('2025-04-27'), cssClass: 'md-dots-bg' },{ slot: 1, allDay: 'true', start: new Date('2025-05-01'), end: new Date('2025-05-02'), cssClass: 'md-dots-bg' },{ slot: 1, allDay: 'true', start: new Date('2025-05-19'), end: new Date('2025-05-19'), cssClass: 'md-dots-bg' },{ slot: 1, allDay: 'true', start: new Date('2025-06-25'), end: new Date('2025-06-25'), cssClass: 'md-dots-bg' },{ slot: 1, allDay: 'true', start: new Date('2025-08-15'), end: new Date('2025-08-15'), cssClass: 'md-dots-bg' },{ slot: 1, allDay: 'true', start: new Date('2025-10-31'), end: new Date('2025-11-1 21:00'), cssClass: 'md-dots-bg' },{ slot: 1, allDay: 'true', start: new Date('2025-12-25'), end: new Date('2025-12-26')},{ slot: 1, allDay: 'true', start: new Date('2026-01-01'), end: new Date('2026-01-02'), cssClass: 'md-dots-bg' },{ slot: 1, allDay: 'true', start: new Date('2023-07-17'), end: new Date('2023-08-16'), cssClass: 'md-stripes-bg' },{ slot: 1, allDay: 'true', start: new Date('2023-04-24'), end: new Date('2023-04-28'), cssClass: 'md-rect-bg' },{ slot: 1, allDay: 'true', start: new Date('2023-12-27'), end: new Date('2023-12-29'), cssClass: 'md-rect-bg' },{ slot: 1, allDay: 'true', start: new Date('2023-10-30'), end: new Date('2023-10-30'), cssClass: 'md-rect-bg' },{ date: moment().format('YYYY-MM-DD'), background: 'cornsilk' },{background: '#a5ceff4d', slot: 1, recurring: { repeat: 'weekly', weekDays: 'SU,SA' }, recurringException: [moment().format('YYYY-MM-DD')]}];
  let refDateSave = localStorage.carRevSelectRange && localStorage.carRevSelectRange == 'custom' && localStorage.carRevSelectStart && localStorage.carRevSelectEnd ? new Date(localStorage.carRevSelectStart) : new Date();
  if (localStorage.carRevSelectRange){
    if(localStorage.carRevSelectRange == 'day'){
      viewType = { timeline: { type: 'day', eventList: true, weekNumbers: true,}};
    }
    else if (localStorage.carRevSelectRange == '2week'){
      viewType = { timeline: { type: 'week', timeCellStep: 1440, timeLabelStep: 1440, startDay: 1, endDay: 0, size: 2, weekNumbers: true, }};
    }
    else if (localStorage.carRevSelectRange == 'week'){
      viewType = { timeline: { type: 'week', eventList: false, startDay: 1, endDay: 0, timeCellStep: 1440, timeLabelStep: 1440, weekNumbers: true, }};
    }
    else if (localStorage.carRevSelectRange == 'weekExt'){
      viewType = { timeline: { type: 'week', eventList: false, startDay: 1, endDay: 0, weekNumbers: true, }};
    }
    else if (localStorage.carRevSelectRange == 'weekMonth'){
      viewType = { timeline: { type: 'week', eventList: false, size: 4, startDay: 1, endDay: 0, timeCellStep: 1440, timeLabelStep: 1440, weekNumbers: true, }};
    }
    else if (localStorage.carRevSelectRange == 'month'){
      viewType = { timeline: { type: 'month', size: 1, weekNumbers: true, }};
    }
    else if (localStorage.carRevSelectRange == '2month'){
      viewType = { timeline: { type: 'month', size: 2, weekNumbers: true, }};
    }
    else if (localStorage.carRevSelectRange == '6month'){
      viewType = { timeline: { type: 'month', size: 6, weekNumbers: true, }};
    }
    else if (localStorage.carRevSelectRange == 'year'){
      viewType = { timeline: { type: 'month', size: 12, weekNumbers: true, }};
    }
    else if (localStorage.carRevSelectRange == 'custom'){
      if (localStorage.carRevSelectStart && localStorage.carRevSelectEnd){
        viewType = { timeline: { 
          type: 'day',
          size: getNrDays(new Date(localStorage.carRevSelectStart), new Date(localStorage.carRevSelectEnd)),
          eventList: true,
          weekNumbers: true,
        }};
      }
      else{
        viewType = { timeline: {
          type: 'week',
          timeCellStep: 1440,
          timeLabelStep: 1440,
          startDay: 1,
          endDay: 0,
          size: 2,
          weekNumbers: true,
        }};
      }
    }
    else {
      viewType = { timeline: {
        type: 'week',
        timeCellStep: 1440,
        timeLabelStep: 1440,
        startDay: 1,
        endDay: 0,
        size: 2,
        weekNumbers: true,
      }};
    }
  }
  else{
    viewType = { timeline: {
      type: 'week',
      timeCellStep: 1440,
      timeLabelStep: 1440,
      startDay: 1,
      endDay: 0,
      size: 2,
      weekNumbers: true,
    }};
  }

  calendar = mobiscroll.eventcalendar('#carReservationCalendar', {
    clickToCreate: clickToCreateVar,
    dragToCreate: dragToCreateVar,
    dragToMove: dragToMoveVar,
    dragToResize: dragToResizeVar,
    dragTimeStep: 60,
    timezonePlugin: mobiscroll.momentTimezone,
    dataTimezone: 'Europe/Belgrade',
    displayTimezone: 'Europe/Belgrade',
    view: viewType,
    refDate: refDateSave,
    selectedDate: refDateSave,
    locale: sloLocal,
    data: [],
    onPageLoading: function (carRev, calendar) {
      let tmpS = moment(calendar._firstDay).add(-1, 'days').format("YYYY-MM-DDTHH:mm");
      let tmpE = moment(calendar._lastDay).add(1, 'days').format("YYYY-MM-DDTHH:mm");
      getData(tmpS,tmpE);
    },
    onPageLoaded: function (args) {
      setTimeout(() => {
        startDate = args.firstDay;
        end = args.lastDay;
        endDate = new Date(end.getFullYear(), end.getMonth(), end.getDate() - 1, 0);
        // set button text
        rangeButton = document.getElementById('custom-date-range-text');
        rangeButton.innerText = getFormattedRange(startDate, endDate);

        // Range picker
        // myRange for selectiong range on displaying calendar timeline
        myRange = mobiscroll.datepicker('#custom-date-range', {
          select: 'range',
          display: 'anchored',
          showOverlay: false,
          touchUi: true,
          buttons: [],
          firstDay: 1,
          locale: sloLocal,
          onClose: function (args, inst) {
            var date = inst.getVal();
            if (date[0] && date[1]) {
              if (date[0].getTime() !== startDate.getTime() || date[1].getTime() !== endDate.getTime()) {
                // navigate the calendar
                calendar.navigate(date[0]);
                // change display option to custom
                selectRange.setOptions({data: [ { text: 'Dan', value: 'day', disabled: false }, { text: 'Teden', value: 'week', disabled: false }, { text: 'Teden (razširjeno)', value: 'weekExt', disabled: false }, { text: 'Teden', value: 'week', disabled: false }, { text: '2 tedna', value: '2week', disabled: false }, { text: '4 Tedne', value: 'weekMonth', disabled: false }, { text: 'Mesec', value: 'month', disabled: false }, { text: '2 meseca', value: '2month', disabled: false }, { text: 'Pol leta', value: '6month', disabled: false }, { text: 'Leto', value: 'year', disabled: false }, { text: 'Ročno', value: 'custom', disabled: false }]});
                setTimeout(() => {
                  selectRange.setVal('custom');
                  localStorage.setItem('carRevSelectRange', 'custom');
                  localStorage.setItem('carRevSelectStart', date[0]);
                  localStorage.setItem('carRevSelectEnd', date[1]);
                }, 200);
              }
              startDate = date[0];
              endDate = date[1];
              // set calendar view
              calendar.setOptions({
                refDate: startDate,
                view: {
                  timeline: {
                    type: 'day',
                    size: getNrDays(startDate, endDate),
                    eventList: true
                  }
                },
                colors: [
                  { slot: 1, allDay: 'true', start: new Date('2023-04-10'), end: new Date('2023-04-10'), cssClass: 'md-dots-bg' },{ slot: 1, allDay: 'true', start: new Date('2023-04-27'), end: new Date('2023-04-27'), cssClass: 'md-dots-bg' },{ slot: 1, allDay: 'true', start: new Date('2023-05-01'), end: new Date('2023-05-02'), cssClass: 'md-dots-bg' },{ slot: 1, allDay: 'true', start: new Date('2023-06-25'), end: new Date('2023-06-25'), cssClass: 'md-dots-bg' },{ slot: 1, allDay: 'true', start: new Date('2023-08-15'), end: new Date('2023-08-15'), cssClass: 'md-dots-bg' },{ slot: 1, allDay: 'true', start: new Date('2023-10-31'), end: new Date('2023-11-2'), cssClass: 'md-dots-bg' },{ slot: 1, allDay: 'true', start: new Date('2023-12-25'), end: new Date('2023-12-26'), cssClass: 'md-dots-bg' }, { slot: 1, allDay: 'true', start: new Date('2023-07-17'), end: new Date('2023-08-16'), cssClass: 'md-stripes-bg' },{ slot: 1, allDay: 'true', start: new Date('2023-04-24'), end: new Date('2023-04-28'), cssClass: 'md-rect-bg' },{ slot: 1, allDay: 'true', start: new Date('2023-12-27'), end: new Date('2023-12-29'), cssClass: 'md-rect-bg' },{ slot: 1, allDay: 'true', start: new Date('2023-10-30'), end: new Date('2023-10-30'), cssClass: 'md-rect-bg' },
                  {background: '#a5ceff4d',slot: 1,recurring: { repeat: 'weekly', weekDays: 'SU,SA' },recurringException: [moment().format('YYYY-MM-DD')]},
                  { date: moment().format('YYYY-MM-DD'), background: 'cornsilk' },
                ],
              });
            } else {
              // myRange.setVal([startDate, endDate])
            }
          }
        });
        myRange.setVal([startDate, endDate]);
      }, 500);
    },
    resources: departmentsWithCarsResource,
    colors: colorType,
    // custom render resource
    renderResource: function (resource) {
      let textStyle = '';
      if (resource.license_plate){
        if (resource.department == 'Vodja projektov'){
          textStyle = 'text-primary';
        }
        switch (resource.department) {
          case 'Vodje projektov': textStyle = 'text-primary'; break;
          case 'Programerji': textStyle = 'text-primary'; break;
          case 'Elektro': textStyle = 'text-warning'; break;
          case 'Strojna': textStyle = 'limegreen-barva'; break;
          case 'Servis': textStyle = 'text-danger'; break;
          case 'Nabava in dostava': textStyle = 'nabava-barva'; break;
          default: textStyle = ''; break;
        }
        let malfunction = resource.malfunction ? ' text-warning' : '';
        let maintenance = resource.maintenance ? ' text-danger' : '';
        let wheels = resource.wheels ? ' text-info' : '';
        let inspection = resource.inspection ? ' text-success' : '';
        return `<div class="cars-shifts-cont" id="` + resource.id + `">
          <div class="`+textStyle+`">` + resource.name + `</div>
          <div class="`+textStyle+`">` + resource.title + `</div>
          <div id="statusResourceRow` + resource.id.substring(3) + `" class="d-none">
            <i class="fas fa-exclamation-triangle fa-lg mr-1` + malfunction + `"></i>
            <i class="fas fa-wrench fa-lg mr-1` + maintenance + `"></i>
            <i class="fas fa-dot-circle fa-lg mr-1` + wheels + `"></i>
            <i class="fas fa-clipboard-list fa-lg mr-1` + inspection + `"></i>
        </div>`;
      }
      else{
        switch (resource.id) {
          case 'Vodje projektov': textStyle = 'text-primary'; break;
          case 'Programerji': textStyle = 'text-primary'; break;
          case 'Elektro': textStyle = 'text-warning'; break;
          case 'Strojna': textStyle = 'limegreen-barva'; break;
          case 'Servis': textStyle = 'text-danger'; break;
          case 'Nabava in dostava': textStyle = 'nabava-barva'; break;
          default: textStyle = ''; break;
        }
        return '<div class="ml-n2">' +
          '<div class="'+textStyle+'">' + resource.name + '</div>' +
          '</div>';
      }
    },
    // custom event content
    renderScheduleEventContent: function (args) {
      var carRev = args.original;
      return '<div class="md-meal-planner-event">' +
          '<div class="md-meal-planner-event-title">' + carRev.title + '</div>' +
          (carRev.location ? '<div class="md-meal-planner-event-desc">Lokacija: ' + carRev.location + '</div>' : '') +
          '</div>';
    },
    // on reservation create
    onEventCreated: function (args) {
      //change to allDay true - the default use case for reservations
      tempCarRev = args.event;
      tempCarRev.allDay = true;
      if(args.action == 'click' && tempCarRev.resource.substring(0,3) == 'car'){
        var tmp = new Date(tempCarRev.start);
        tmp.setHours(7,0);
        tempCarRev.start = tmp.toISOString();
        tmp.setHours(15,0);
        tempCarRev.end = tmp.toISOString();
        tempCarRev.title = loggedUser.name + ' ' + loggedUser.surname;
        // find user and their role from db and correct color of the reservation according to the department user is part of
        let tmpUserRev = users.find(u => u.name + ' ' + u.surname == tempCarRev.title);
        let tmpColorRev = tempCarRev.color;
        if (tmpUserRev.role.toLowerCase() == 'admin' || tmpUserRev.role.toLowerCase() == 'vodja projektov' || tmpUserRev.role.toLowerCase() == 'vodja')
          tmpColorRev = '#212529';
        else if (tmpUserRev.role.toLowerCase() == 'programer plc-jev' || tmpUserRev.role.toLowerCase() == 'programer robotov')
          tmpColorRev = '#325d88';
        else if (tmpUserRev.role.toLowerCase() == 'električar' || tmpUserRev.role.toLowerCase() == 'vodja električarjev')
          tmpColorRev = '#f47c3c';
        else if (tmpUserRev.role.toLowerCase() == 'strojnik' || tmpUserRev.role.toLowerCase() == 'vodja strojnikov' || tmpUserRev.role.toLowerCase() == 'vodja cnc obdelave' || tmpUserRev.role.toLowerCase() == 'varilec' || tmpUserRev.role.toLowerCase() == 'strojni monter' ||  tmpUserRev.role.toLowerCase() =='cnc operater')
          tmpColorRev = '#4baf4f';
        else if (tmpUserRev.role.toLowerCase() == 'serviser')
          tmpColorRev = '#d9534f';
        else if (tmpUserRev.role.toLowerCase() == 'konstrukter')
          tmpColorRev = '#80b139';
        else tmpColorRev = '#1a73e8';
        // post new car reservation - default action
        let data = {start: tempCarRev.start, end: tempCarRev.end, color: tmpColorRev, carId: tempCarRev.resource.substring(3)};
        $.ajax({
          type: 'POST',
          url: '/apiv2/cars/reservations/',
          contentType: 'application/json',
          data: JSON.stringify(data), // access in body
        }).done(function (resp) {
          //console.log('SUCCESS');
          tempCarRev.id = resp.carRev.id;
          tempCarRev.owner = resp.carRev.owner;
          tempCarRev.user = resp.carRev.user;
          tempCarRev.owner_name = loggedUser.name + ' ' + loggedUser.surname;
          tempCarRev.user_name = loggedUser.name + ' ' + loggedUser.surname;
          tempCarRev.my_reservatoin = true;
          tempCarRev.my_creation = true;
          tempCarRev.title = tempCarRev.user_name;
          tempCarRev.created = new Date();
          tempCarRev.finished = false;
          tempCarRev.keys = false;
          tempCarRev.color = resp.carRev.color;
          tempCarRev.car = resp.carRev.car;
          tempCarRev.resource = 'car' + tempCarRev.car;
          // tempEvent.start = tempEvent.all_day ? moment(tempEvent.start).format("YYYY-MM-DDT00:00") : moment(tempEvent.start).format("YYYY-MM-DDTHH:mm");
          // tempEvent.end = tempEvent.all_day ? moment(tempEvent.end).format("YYYY-MM-DDT23:00") : moment(tempEvent.end).format("YYYY-MM-DDTHH:mm");
          calendar.updateEvent(tempCarRev);
          deleteEvent = false;

          // navigate the calendar to the correct view
          // calendar.navigate(tempEvent.start);

          // popup.close();
        }).fail(function (resp) {
          //console.log('FAIL');
          //debugger;
          // popup.close();
          $('#errorModalMsg').text('Napaka pri kreiranju rezervacije v podatkovni bazi. Osvežite stran in ob ponovni napaki obvestite admninistratorja RIS-a.')
          $('#modalError').modal('toggle');
        });
      }
      else{
        calendar.removeEvent(tempCarRev);
      }
    },
    // on reservation update (dragging start/end case)
    onEventUpdated: function (args) {
      // console.log('do update');
      // on drag resize and move action, do PUT call on /cars/reservations, updating start and end only
      if (args.event.owner_name == (loggedUser.name + ' ' + loggedUser.surname) || loggedUser.role == 'admin' || loggedUser.role.includes('vodja')){
        tmpCR = args.event;
        tmpCR.car = tmpCR.resource.substring(3);
        var data = {start: tmpCR.start, end: tmpCR.end, color: tmpCR.color, keys: tmpCR.keys, location: tmpCR.location, userId: tmpCR.user, status: tmpCR.finished, car: tmpCR.car};
        $.ajax({
          type: 'PUT',
          url: '/apiv2/cars/reservations/'+tmpCR.id,
          contentType: 'application/json',
          data: JSON.stringify(data), // access in body
        }).done(function (resp) {
          // console.log('SUCCESS');
  
        }).fail(function (resp) {
          //console.log('FAIL');
          //debugger;
          // popup.close();
          $('#errorModalMsg').text('Napaka pri posodobitvi podatkov v podatkovni bazi. Osvežite stran in ob ponovni napaki obvestite admninistratorja RIS-a.')
          $('#modalError').modal('toggle');
        });
      }
      else{
        // user does not have permission to create events for other users
        // a way around this is to create own event and then drag to other user and this make resource as it was before
        args.event.resource = args.oldEvent.resource;
        // and update event on calendar so user knows it was corrected
        calendar.updateEvent(args.event);
      }
    },
    // custom on hover event (open tooltip)
    onEventHoverIn: function (args, inst) {
      // change tooltip arg/elm to correct value and open tooltip
      var carRev = args.event;
      // show and hide specific elements based on logged user (exception if its his reservation and his creation)
      if (carRev.owner_name == (loggedUser.name + ' ' + loggedUser.surname) || loggedUser.role == 'admin' || loggedUser.role.includes('vodja') || loggedUser.role == 'tajnik' || loggedUser.role == 'komercialist' || loggedUser.role == 'komerciala' || loggedUser.role == 'računovodstvo') {
        // has rights to edit
        deleteButton.style.display = 'block';
        saveButton.style.display = 'block';
        statusButton.style.display = 'block';
        tooltipUser.removeAttribute("disabled");
        tooltipKeys.removeAttribute("disabled");
        tooltipLocation.removeAttribute("disabled");
      }
      else{
        // has no rights to edit or delete
        deleteButton.style.display = 'none';
        saveButton.style.display = 'none';
        statusButton.style.display = 'none';
        tooltipUser.setAttribute("disabled", "disabled");
        tooltipKeys.setAttribute("disabled", "disabled");
        tooltipLocation.setAttribute("disabled", "disabled");
      }


      if (carRev.finished) {
        $('#status-button').text('V teku');
        $('#status-button').removeClass('btn-success').addClass('btn-warning');
      } else {
        $('#status-button').text('Zaključi');
        $('#status-button').removeClass('btn-warning').addClass('btn-success');
      }

      tooltipHeader.style.backgroundColor = carRev.color;
      tooltipTitle.innerHTML = carRev.title;
      tooltipAdditionalTitle.innerHTML = carRev.location ? carRev.location : '';
      tooltipLocation.value = carRev.location ? carRev.location : '';
      tooltipKeys.checked = carRev.keys ? true : false;
      tooltipKeysText.innerHTML = carRev.keys ? 'Ima' : 'Nima';
      tooltipStatus.innerHTML = carRev.finished ? 'Zaključeno' : 'V teku';
      tooltipOwner.innerHTML = carRev.owner_name + ' (' + new Date(carRev.created).toLocaleString('sl') + ')';
      // tooltipUser.value = carRev.user;
      $('#tooltip-reservation-select').val(carRev.user).trigger('change');
      // statusButton.innerHTML = button.text;
      // calendar.getInst(statusButton).setOptions({ color: button.type });

      clearTimeout(timer);
      timer = null;

      tooltip.setOptions({ anchor: args.domEvent.target });
      tooltip.open();
      tmpCR = args.event;
      refreshTimerCount = 0;
    },
    // custom hover out event
    onEventHoverOut: function (args) {
      // close tooltip
      if (!timer) {
        timer = setTimeout(function () {
          tooltip.close();
        }, 200);
      }
    },
    // custom header
    renderHeader: function () {
      let hideClass = !(loggedUser.role.toLowerCase() == 'admin' || loggedUser.role.toLowerCase() == 'tajnik' || loggedUser.role.toLowerCase() == 'komercialist' || loggedUser.role.toLowerCase() == 'komerciala' || loggedUser.role.toLowerCase() == 'računovodstvo') ? ' d-none' : '';
      return `
      <div id="custom-date-range-space" class="col-md-4 col-12">
        <div id="custom-date-range" class="w-210px">
          <button mbsc-button data-variant="flat" class="mbsc-calendar-button">
            <span id="custom-date-range-text" class="mbsc-calendar-title"></span>
          </button>
        </div>
      </div>
      <div class="col-md-8 col-12 row ml-0">
        <div class="md-shift-header-controls">
          <button class="btn btn-primary my-2 mr-2" data-toggle="modal" data-target="#modalPdf" id="pdf-rules-button">PRAVILNIK</button>
          <button class="btn btn-primary my-2 mr-2 btn-on-stretched" id="refreshData" data-toggle="tooltip" data-original-title="Osveži">
            <i class="fas fa-sync fa-lg"></i>
          </button>
          <button class="btn btn-success my-2 ` + hideClass + `" data-toggle="modal" data-target="#modalAddCarForm" id="add-car-button">Dodaj vozilo</button>
        <label class="md-shift-cal-view m-2">
        <input mbsc-input id="shift-management-view" data-dropdown="true" data-input-style="box" />
        </label>
        <select id="shift-management-select">
        <option value="day">Dan</option>
        <option value="week">Teden</option>
        <option value="weekExt">Teden (razširjeno)</option>
        <option value="2week">2 tedna</option>
        <option value="weekMonth">4 tedne</option>
        <option value="month">Mesec</option>
        <option value="2month">2 meseca</option>
        <option value="6month">Pol leta</option>
        <option value="year">Leto</option>
        <option value="custom">Ročno</option>
        </select>
        </div>
        <div mbsc-calendar-prev></div>
        <div mbsc-calendar-today></div>
        <div mbsc-calendar-next></div>
      </div>`;
    },
  });
  addCarButton = document.getElementById('add-car-button');
  // switching views (day,week,2weeks,month)
  selectRange = mobiscroll.select('#shift-management-select', {
    inputElement: document.getElementById('shift-management-view'),
    touchUi: false,
    onChange: function (event, inst) {
      if (event.value == 'day') {
        // myRange.setVal([]);
        setTimeout(() => {
          selectRange.setOptions({data: [ { text: 'Dan', value: 'day' }, { text: 'Teden', value: 'week' }, { text: 'Teden (razširjeno)', value: 'weekExt' }, { text: '2 tedna', value: '2week' }, { text: '4 Tedne', value: 'weekMonth' }, { text: 'Mesec', value: 'month' }, { text: '2 meseca', value: '2month' }, { text: 'Pol leta', value: '6month' }, { text: 'Leto', value: 'year' }, { text: 'Ročno', value: 'custom', disabled: true }]});
          localStorage.setItem('carRevSelectRange', 'day');
        }, 500);
        calendar.setOptions({
          view: {
            timeline: {
              type: 'day',
              eventList: true,
              weekNumbers: true,
            }
          },
          colors: [
            { slot: 1, allDay: 'true', start: new Date('2025-01-01'), end: new Date('2025-01-01'), cssClass: 'md-dots-bg' },{ slot: 1, allDay: 'true', start: new Date('2025-01-02'), end: new Date('2025-01-02'), cssClass: 'md-dots-bg' },{ slot: 1, allDay: 'true', start: new Date('2025-02-08'), end: new Date('2025-02-08'), cssClass: 'md-dots-bg' },{ slot: 1, allDay: 'true', start: new Date('2025-04-21'), end: new Date('2025-04-21'), cssClass: 'md-dots-bg' },{ slot: 1, allDay: 'true', start: new Date('2025-04-27'), end: new Date('2025-04-27'), cssClass: 'md-dots-bg' },{ slot: 1, allDay: 'true', start: new Date('2025-05-01'), end: new Date('2025-05-02'), cssClass: 'md-dots-bg' },{ slot: 1, allDay: 'true', start: new Date('2025-05-19'), end: new Date('2025-05-19'), cssClass: 'md-dots-bg' },{ slot: 1, allDay: 'true', start: new Date('2025-06-25'), end: new Date('2025-06-25'), cssClass: 'md-dots-bg' },{ slot: 1, allDay: 'true', start: new Date('2025-08-15'), end: new Date('2025-08-15'), cssClass: 'md-dots-bg' },{ slot: 1, allDay: 'true', start: new Date('2025-10-31'), end: new Date('2025-11-1 21:00'), cssClass: 'md-dots-bg' },{ slot: 1, allDay: 'true', start: new Date('2025-12-25'), end: new Date('2025-12-26')},{ slot: 1, allDay: 'true', start: new Date('2026-01-01'), end: new Date('2026-01-02'), cssClass: 'md-dots-bg' },{ slot: 1, allDay: 'true', start: new Date('2023-07-17'), end: new Date('2023-08-16'), cssClass: 'md-stripes-bg' },{ slot: 1, allDay: 'true', start: new Date('2023-04-24'), end: new Date('2023-04-28'), cssClass: 'md-rect-bg' },{ slot: 1, allDay: 'true', start: new Date('2023-12-27'), end: new Date('2023-12-29'), cssClass: 'md-rect-bg' },{ slot: 1, allDay: 'true', start: new Date('2023-10-30'), end: new Date('2023-10-30'), cssClass: 'md-rect-bg' },
            {background: '#a5ceff4d',slot: 1,recurring: { repeat: 'weekly', weekDays: 'SU,SA' }},
          ],
        });
      }
      else if (event.value == '2week') {
        // myRange.setVal([]);
        setTimeout(() => {
          selectRange.setOptions({data: [ { text: 'Dan', value: 'day' }, { text: 'Teden', value: 'week' }, { text: 'Teden (razširjeno)', value: 'weekExt' }, { text: '2 tedna', value: '2week' }, { text: '4 Tedne', value: 'weekMonth' }, { text: 'Mesec', value: 'month' }, { text: '2 meseca', value: '2month' }, { text: 'Pol leta', value: '6month' }, { text: 'Leto', value: 'year' }, { text: 'Ročno', value: 'custom', disabled: true }]});
          localStorage.setItem('carRevSelectRange', '2week');
        }, 500);
        calendar.setOptions({
          view: {
            timeline: {
              type: 'week',
              timeCellStep: 1440,
              timeLabelStep: 1440,
              startDay: 1,
              endDay: 0,
              size: 2,
              weekNumbers: true,
            }
          },
          colors: [
            { slot: 1, allDay: 'true', start: new Date('2025-01-01'), end: new Date('2025-01-01'), cssClass: 'md-dots-bg' },{ slot: 1, allDay: 'true', start: new Date('2025-01-02'), end: new Date('2025-01-02'), cssClass: 'md-dots-bg' },{ slot: 1, allDay: 'true', start: new Date('2025-02-08'), end: new Date('2025-02-08'), cssClass: 'md-dots-bg' },{ slot: 1, allDay: 'true', start: new Date('2025-04-21'), end: new Date('2025-04-21'), cssClass: 'md-dots-bg' },{ slot: 1, allDay: 'true', start: new Date('2025-04-27'), end: new Date('2025-04-27'), cssClass: 'md-dots-bg' },{ slot: 1, allDay: 'true', start: new Date('2025-05-01'), end: new Date('2025-05-02'), cssClass: 'md-dots-bg' },{ slot: 1, allDay: 'true', start: new Date('2025-05-19'), end: new Date('2025-05-19'), cssClass: 'md-dots-bg' },{ slot: 1, allDay: 'true', start: new Date('2025-06-25'), end: new Date('2025-06-25'), cssClass: 'md-dots-bg' },{ slot: 1, allDay: 'true', start: new Date('2025-08-15'), end: new Date('2025-08-15'), cssClass: 'md-dots-bg' },{ slot: 1, allDay: 'true', start: new Date('2025-10-31'), end: new Date('2025-11-1 21:00'), cssClass: 'md-dots-bg' },{ slot: 1, allDay: 'true', start: new Date('2025-12-25'), end: new Date('2025-12-26')},{ slot: 1, allDay: 'true', start: new Date('2026-01-01'), end: new Date('2026-01-02'), cssClass: 'md-dots-bg' },{ slot: 1, allDay: 'true', start: new Date('2023-07-17'), end: new Date('2023-08-16'), cssClass: 'md-stripes-bg' },{ slot: 1, allDay: 'true', start: new Date('2023-04-24'), end: new Date('2023-04-28'), cssClass: 'md-rect-bg' },{ slot: 1, allDay: 'true', start: new Date('2023-12-27'), end: new Date('2023-12-29'), cssClass: 'md-rect-bg' },{ slot: 1, allDay: 'true', start: new Date('2023-10-30'), end: new Date('2023-10-30'), cssClass: 'md-rect-bg' },
            {background: '#a5ceff4d',slot: 1,recurring: { repeat: 'weekly', weekDays: 'SU,SA' },recurringException: [moment().format('YYYY-MM-DD')]},
            { date: moment().format('YYYY-MM-DD'), background: 'cornsilk' },
          ],
        });
      }
      else if (event.value == 'week') {
        // myRange.setVal([]);
        setTimeout(() => {
          selectRange.setOptions({data: [ { text: 'Dan', value: 'day' }, { text: 'Teden', value: 'week' }, { text: 'Teden (razširjeno)', value: 'weekExt' }, { text: '2 tedna', value: '2week' }, { text: '4 Tedne', value: 'weekMonth' }, { text: 'Mesec', value: 'month' }, { text: '2 meseca', value: '2month' }, { text: 'Pol leta', value: '6month' }, { text: 'Leto', value: 'year' }, { text: 'Ročno', value: 'custom', disabled: true }]});
          localStorage.setItem('carRevSelectRange', 'week');
        }, 500);
        calendar.setOptions({
          view: {
            timeline: {
              type: 'week',
              eventList: false,
              startDay: 1,
              endDay: 0,
              timeCellStep: 1440,
              timeLabelStep: 1440,
              weekNumbers: true,
            }
          },
          colors: [
            { slot: 1, allDay: 'true', start: new Date('2025-01-01'), end: new Date('2025-01-01'), cssClass: 'md-dots-bg' },{ slot: 1, allDay: 'true', start: new Date('2025-01-02'), end: new Date('2025-01-02'), cssClass: 'md-dots-bg' },{ slot: 1, allDay: 'true', start: new Date('2025-02-08'), end: new Date('2025-02-08'), cssClass: 'md-dots-bg' },{ slot: 1, allDay: 'true', start: new Date('2025-04-21'), end: new Date('2025-04-21'), cssClass: 'md-dots-bg' },{ slot: 1, allDay: 'true', start: new Date('2025-04-27'), end: new Date('2025-04-27'), cssClass: 'md-dots-bg' },{ slot: 1, allDay: 'true', start: new Date('2025-05-01'), end: new Date('2025-05-02'), cssClass: 'md-dots-bg' },{ slot: 1, allDay: 'true', start: new Date('2025-05-19'), end: new Date('2025-05-19'), cssClass: 'md-dots-bg' },{ slot: 1, allDay: 'true', start: new Date('2025-06-25'), end: new Date('2025-06-25'), cssClass: 'md-dots-bg' },{ slot: 1, allDay: 'true', start: new Date('2025-08-15'), end: new Date('2025-08-15'), cssClass: 'md-dots-bg' },{ slot: 1, allDay: 'true', start: new Date('2025-10-31'), end: new Date('2025-11-1 21:00'), cssClass: 'md-dots-bg' },{ slot: 1, allDay: 'true', start: new Date('2025-12-25'), end: new Date('2025-12-26')},{ slot: 1, allDay: 'true', start: new Date('2026-01-01'), end: new Date('2026-01-02'), cssClass: 'md-dots-bg' },{ slot: 1, allDay: 'true', start: new Date('2023-07-17'), end: new Date('2023-08-16'), cssClass: 'md-stripes-bg' },{ slot: 1, allDay: 'true', start: new Date('2023-04-24'), end: new Date('2023-04-28'), cssClass: 'md-rect-bg' },{ slot: 1, allDay: 'true', start: new Date('2023-12-27'), end: new Date('2023-12-29'), cssClass: 'md-rect-bg' },{ slot: 1, allDay: 'true', start: new Date('2023-10-30'), end: new Date('2023-10-30'), cssClass: 'md-rect-bg' },
            {background: '#a5ceff4d',slot: 1,recurring: { repeat: 'weekly', weekDays: 'SU,SA' },recurringException: [moment().format('YYYY-MM-DD')]},
            { date: moment().format('YYYY-MM-DD'), background: 'cornsilk' },
          ],
        });
      }
      else if (event.value == 'weekExt') {
        // myRange.setVal([]);
        setTimeout(() => {
          selectRange.setOptions({data: [ { text: 'Dan', value: 'day' }, { text: 'Teden', value: 'week' }, { text: 'Teden (razširjeno)', value: 'weekExt' }, { text: '2 tedna', value: '2week' }, { text: '4 Tedne', value: 'weekMonth' }, { text: 'Mesec', value: 'month' }, { text: '2 meseca', value: '2month' }, { text: 'Pol leta', value: '6month' }, { text: 'Leto', value: 'year' }, { text: 'Ročno', value: 'custom', disabled: true }]});
          localStorage.setItem('carRevSelectRange', 'weekExt');
        }, 500);
        calendar.setOptions({
          view: {
            timeline: {
              type: 'week',
              eventList: false,
              startDay: 1,
              endDay: 0,
              weekNumbers: true,
            }
          },
          colors: [
            { slot: 1, allDay: 'true', start: new Date('2025-01-01'), end: new Date('2025-01-01'), cssClass: 'md-dots-bg' },{ slot: 1, allDay: 'true', start: new Date('2025-01-02'), end: new Date('2025-01-02'), cssClass: 'md-dots-bg' },{ slot: 1, allDay: 'true', start: new Date('2025-02-08'), end: new Date('2025-02-08'), cssClass: 'md-dots-bg' },{ slot: 1, allDay: 'true', start: new Date('2025-04-21'), end: new Date('2025-04-21'), cssClass: 'md-dots-bg' },{ slot: 1, allDay: 'true', start: new Date('2025-04-27'), end: new Date('2025-04-27'), cssClass: 'md-dots-bg' },{ slot: 1, allDay: 'true', start: new Date('2025-05-01'), end: new Date('2025-05-02'), cssClass: 'md-dots-bg' },{ slot: 1, allDay: 'true', start: new Date('2025-05-19'), end: new Date('2025-05-19'), cssClass: 'md-dots-bg' },{ slot: 1, allDay: 'true', start: new Date('2025-06-25'), end: new Date('2025-06-25'), cssClass: 'md-dots-bg' },{ slot: 1, allDay: 'true', start: new Date('2025-08-15'), end: new Date('2025-08-15'), cssClass: 'md-dots-bg' },{ slot: 1, allDay: 'true', start: new Date('2025-10-31'), end: new Date('2025-11-1 21:00'), cssClass: 'md-dots-bg' },{ slot: 1, allDay: 'true', start: new Date('2025-12-25'), end: new Date('2025-12-26')},{ slot: 1, allDay: 'true', start: new Date('2026-01-01'), end: new Date('2026-01-02'), cssClass: 'md-dots-bg' },{ slot: 1, allDay: 'true', start: new Date('2023-07-17'), end: new Date('2023-08-16'), cssClass: 'md-stripes-bg' },{ slot: 1, allDay: 'true', start: new Date('2023-04-24'), end: new Date('2023-04-28'), cssClass: 'md-rect-bg' },{ slot: 1, allDay: 'true', start: new Date('2023-12-27'), end: new Date('2023-12-29'), cssClass: 'md-rect-bg' },{ slot: 1, allDay: 'true', start: new Date('2023-10-30'), end: new Date('2023-10-30'), cssClass: 'md-rect-bg' },
            {background: '#a5ceff4d',slot: 1,recurring: { repeat: 'weekly', weekDays: 'SU,SA' },recurringException: [moment().format('YYYY-MM-DD')]},
            { date: moment().format('YYYY-MM-DD'), background: 'cornsilk' },
          ],
        });
      }
      else if (event.value == 'weekMonth') {
        // myRange.setVal([]);
        setTimeout(() => {
          selectRange.setOptions({data: [ { text: 'Dan', value: 'day' }, { text: 'Teden', value: 'week' }, { text: 'Teden (razširjeno)', value: 'weekExt' }, { text: '2 tedna', value: '2week' }, { text: '4 Tedne', value: 'weekMonth' }, { text: 'Mesec', value: 'month' }, { text: '2 meseca', value: '2month' }, { text: 'Pol leta', value: '6month' }, { text: 'Leto', value: 'year' }, { text: 'Ročno', value: 'custom', disabled: true }]});
          localStorage.setItem('carRevSelectRange', 'weekMonth');
        }, 500);
        calendar.setOptions({
          view: {
            timeline: {
              type: 'week',
              eventList: false,
              size: 4,
              startDay: 1,
              endDay: 0,
              timeCellStep: 1440,
              timeLabelStep: 1440,
              weekNumbers: true,
            }
          },
          colors: [
            { slot: 1, allDay: 'true', start: new Date('2025-01-01'), end: new Date('2025-01-01'), cssClass: 'md-dots-bg' },{ slot: 1, allDay: 'true', start: new Date('2025-01-02'), end: new Date('2025-01-02'), cssClass: 'md-dots-bg' },{ slot: 1, allDay: 'true', start: new Date('2025-02-08'), end: new Date('2025-02-08'), cssClass: 'md-dots-bg' },{ slot: 1, allDay: 'true', start: new Date('2025-04-21'), end: new Date('2025-04-21'), cssClass: 'md-dots-bg' },{ slot: 1, allDay: 'true', start: new Date('2025-04-27'), end: new Date('2025-04-27'), cssClass: 'md-dots-bg' },{ slot: 1, allDay: 'true', start: new Date('2025-05-01'), end: new Date('2025-05-02'), cssClass: 'md-dots-bg' },{ slot: 1, allDay: 'true', start: new Date('2025-05-19'), end: new Date('2025-05-19'), cssClass: 'md-dots-bg' },{ slot: 1, allDay: 'true', start: new Date('2025-06-25'), end: new Date('2025-06-25'), cssClass: 'md-dots-bg' },{ slot: 1, allDay: 'true', start: new Date('2025-08-15'), end: new Date('2025-08-15'), cssClass: 'md-dots-bg' },{ slot: 1, allDay: 'true', start: new Date('2025-10-31'), end: new Date('2025-11-1 21:00'), cssClass: 'md-dots-bg' },{ slot: 1, allDay: 'true', start: new Date('2025-12-25'), end: new Date('2025-12-26')},{ slot: 1, allDay: 'true', start: new Date('2026-01-01'), end: new Date('2026-01-02'), cssClass: 'md-dots-bg' },{ slot: 1, allDay: 'true', start: new Date('2023-07-17'), end: new Date('2023-08-16'), cssClass: 'md-stripes-bg' },{ slot: 1, allDay: 'true', start: new Date('2023-04-24'), end: new Date('2023-04-28'), cssClass: 'md-rect-bg' },{ slot: 1, allDay: 'true', start: new Date('2023-12-27'), end: new Date('2023-12-29'), cssClass: 'md-rect-bg' },{ slot: 1, allDay: 'true', start: new Date('2023-10-30'), end: new Date('2023-10-30'), cssClass: 'md-rect-bg' },
            {background: '#a5ceff4d',slot: 1,recurring: { repeat: 'weekly', weekDays: 'SU,SA' },recurringException: [moment().format('YYYY-MM-DD')]},
            { date: moment().format('YYYY-MM-DD'), background: 'cornsilk' },
          ],
        });
      }
      else if (event.value == 'month'){
        // myRange.setVal([]);
        setTimeout(() => {
          selectRange.setOptions({data: [ { text: 'Dan', value: 'day' }, { text: 'Teden', value: 'week' }, { text: 'Teden (razširjeno)', value: 'weekExt' }, { text: '2 tedna', value: '2week' }, { text: '4 Tedne', value: 'weekMonth' }, { text: 'Mesec', value: 'month' }, { text: '2 meseca', value: '2month' }, { text: 'Pol leta', value: '6month' }, { text: 'Leto', value: 'year' }, { text: 'Ročno', value: 'custom', disabled: true }]});
          localStorage.setItem('carRevSelectRange', 'month');
        }, 500);
        calendar.setOptions({
          view: {
            timeline: {
              type: 'month',
              size: 1,
              weekNumbers: true,
            }
          },
          colors: [
            { slot: 1, allDay: 'true', start: new Date('2025-01-01'), end: new Date('2025-01-01'), cssClass: 'md-dots-bg' },{ slot: 1, allDay: 'true', start: new Date('2025-01-02'), end: new Date('2025-01-02'), cssClass: 'md-dots-bg' },{ slot: 1, allDay: 'true', start: new Date('2025-02-08'), end: new Date('2025-02-08'), cssClass: 'md-dots-bg' },{ slot: 1, allDay: 'true', start: new Date('2025-04-21'), end: new Date('2025-04-21'), cssClass: 'md-dots-bg' },{ slot: 1, allDay: 'true', start: new Date('2025-04-27'), end: new Date('2025-04-27'), cssClass: 'md-dots-bg' },{ slot: 1, allDay: 'true', start: new Date('2025-05-01'), end: new Date('2025-05-02'), cssClass: 'md-dots-bg' },{ slot: 1, allDay: 'true', start: new Date('2025-05-19'), end: new Date('2025-05-19'), cssClass: 'md-dots-bg' },{ slot: 1, allDay: 'true', start: new Date('2025-06-25'), end: new Date('2025-06-25'), cssClass: 'md-dots-bg' },{ slot: 1, allDay: 'true', start: new Date('2025-08-15'), end: new Date('2025-08-15'), cssClass: 'md-dots-bg' },{ slot: 1, allDay: 'true', start: new Date('2025-10-31'), end: new Date('2025-11-1 21:00'), cssClass: 'md-dots-bg' },{ slot: 1, allDay: 'true', start: new Date('2025-12-25'), end: new Date('2025-12-26')},{ slot: 1, allDay: 'true', start: new Date('2026-01-01'), end: new Date('2026-01-02'), cssClass: 'md-dots-bg' },{ slot: 1, allDay: 'true', start: new Date('2023-07-17'), end: new Date('2023-08-16'), cssClass: 'md-stripes-bg' },{ slot: 1, allDay: 'true', start: new Date('2023-04-24'), end: new Date('2023-04-28'), cssClass: 'md-rect-bg' },{ slot: 1, allDay: 'true', start: new Date('2023-12-27'), end: new Date('2023-12-29'), cssClass: 'md-rect-bg' },{ slot: 1, allDay: 'true', start: new Date('2023-10-30'), end: new Date('2023-10-30'), cssClass: 'md-rect-bg' },
            {background: '#a5ceff4d',slot: 1,recurring: { repeat: 'weekly', weekDays: 'SU,SA' },recurringException: [moment().format('YYYY-MM-DD')]},
            { date: moment().format('YYYY-MM-DD'), background: 'cornsilk' },
          ],
        });
      }
      else if (event.value == '2month'){
        // myRange.setVal([]);
        setTimeout(() => {
          selectRange.setOptions({data: [ { text: 'Dan', value: 'day' }, { text: 'Teden', value: 'week' }, { text: 'Teden (razširjeno)', value: 'weekExt' }, { text: '2 tedna', value: '2week' }, { text: '4 Tedne', value: 'weekMonth' }, { text: 'Mesec', value: 'month' }, { text: '2 meseca', value: '2month' }, { text: 'Pol leta', value: '6month' }, { text: 'Leto', value: 'year' }, { text: 'Ročno', value: 'custom', disabled: true }]});
          localStorage.setItem('carRevSelectRange', '2month');
        }, 500);
        calendar.setOptions({
          view: {
            timeline: {
              type: 'month',
              size: 2,
              weekNumbers: true,
            }
          },
          colors: [
            { slot: 1, allDay: 'true', start: new Date('2025-01-01'), end: new Date('2025-01-01'), cssClass: 'md-dots-bg' },{ slot: 1, allDay: 'true', start: new Date('2025-01-02'), end: new Date('2025-01-02'), cssClass: 'md-dots-bg' },{ slot: 1, allDay: 'true', start: new Date('2025-02-08'), end: new Date('2025-02-08'), cssClass: 'md-dots-bg' },{ slot: 1, allDay: 'true', start: new Date('2025-04-21'), end: new Date('2025-04-21'), cssClass: 'md-dots-bg' },{ slot: 1, allDay: 'true', start: new Date('2025-04-27'), end: new Date('2025-04-27'), cssClass: 'md-dots-bg' },{ slot: 1, allDay: 'true', start: new Date('2025-05-01'), end: new Date('2025-05-02'), cssClass: 'md-dots-bg' },{ slot: 1, allDay: 'true', start: new Date('2025-05-19'), end: new Date('2025-05-19'), cssClass: 'md-dots-bg' },{ slot: 1, allDay: 'true', start: new Date('2025-06-25'), end: new Date('2025-06-25'), cssClass: 'md-dots-bg' },{ slot: 1, allDay: 'true', start: new Date('2025-08-15'), end: new Date('2025-08-15'), cssClass: 'md-dots-bg' },{ slot: 1, allDay: 'true', start: new Date('2025-10-31'), end: new Date('2025-11-1 21:00'), cssClass: 'md-dots-bg' },{ slot: 1, allDay: 'true', start: new Date('2025-12-25'), end: new Date('2025-12-26')},{ slot: 1, allDay: 'true', start: new Date('2026-01-01'), end: new Date('2026-01-02'), cssClass: 'md-dots-bg' },{ slot: 1, allDay: 'true', start: new Date('2023-07-17'), end: new Date('2023-08-16'), cssClass: 'md-stripes-bg' },{ slot: 1, allDay: 'true', start: new Date('2023-04-24'), end: new Date('2023-04-28'), cssClass: 'md-rect-bg' },{ slot: 1, allDay: 'true', start: new Date('2023-12-27'), end: new Date('2023-12-29'), cssClass: 'md-rect-bg' },{ slot: 1, allDay: 'true', start: new Date('2023-10-30'), end: new Date('2023-10-30'), cssClass: 'md-rect-bg' },
            {background: '#a5ceff4d',slot: 1,recurring: { repeat: 'weekly', weekDays: 'SU,SA' },recurringException: [moment().format('YYYY-MM-DD')]},
            { date: moment().format('YYYY-MM-DD'), background: 'cornsilk' },
          ],
        });
      }
      else if (event.value == '6month'){
        // myRange.setVal([]);
        setTimeout(() => {
          selectRange.setOptions({data: [ { text: 'Dan', value: 'day' }, { text: 'Teden', value: 'week' }, { text: 'Teden (razširjeno)', value: 'weekExt' }, { text: '2 tedna', value: '2week' }, { text: '4 Tedne', value: 'weekMonth' }, { text: 'Mesec', value: 'month' }, { text: '2 meseca', value: '2month' }, { text: 'Pol leta', value: '6month' }, { text: 'Leto', value: 'year' }, { text: 'Ročno', value: 'custom', disabled: true }]});
          localStorage.setItem('carRevSelectRange', '6month');
        }, 500);
        calendar.setOptions({
          view: {
            timeline: {
              type: 'month',
              size: 6,
              weekNumbers: true,
            }
          },
          colors: [
            { slot: 1, allDay: 'true', start: new Date('2025-01-01'), end: new Date('2025-01-01'), cssClass: 'md-dots-bg' },{ slot: 1, allDay: 'true', start: new Date('2025-01-02'), end: new Date('2025-01-02'), cssClass: 'md-dots-bg' },{ slot: 1, allDay: 'true', start: new Date('2025-02-08'), end: new Date('2025-02-08'), cssClass: 'md-dots-bg' },{ slot: 1, allDay: 'true', start: new Date('2025-04-21'), end: new Date('2025-04-21'), cssClass: 'md-dots-bg' },{ slot: 1, allDay: 'true', start: new Date('2025-04-27'), end: new Date('2025-04-27'), cssClass: 'md-dots-bg' },{ slot: 1, allDay: 'true', start: new Date('2025-05-01'), end: new Date('2025-05-02'), cssClass: 'md-dots-bg' },{ slot: 1, allDay: 'true', start: new Date('2025-05-19'), end: new Date('2025-05-19'), cssClass: 'md-dots-bg' },{ slot: 1, allDay: 'true', start: new Date('2025-06-25'), end: new Date('2025-06-25'), cssClass: 'md-dots-bg' },{ slot: 1, allDay: 'true', start: new Date('2025-08-15'), end: new Date('2025-08-15'), cssClass: 'md-dots-bg' },{ slot: 1, allDay: 'true', start: new Date('2025-10-31'), end: new Date('2025-11-1 21:00'), cssClass: 'md-dots-bg' },{ slot: 1, allDay: 'true', start: new Date('2025-12-25'), end: new Date('2025-12-26')},{ slot: 1, allDay: 'true', start: new Date('2026-01-01'), end: new Date('2026-01-02'), cssClass: 'md-dots-bg' },{ slot: 1, allDay: 'true', start: new Date('2023-07-17'), end: new Date('2023-08-16'), cssClass: 'md-stripes-bg' },{ slot: 1, allDay: 'true', start: new Date('2023-04-24'), end: new Date('2023-04-28'), cssClass: 'md-rect-bg' },{ slot: 1, allDay: 'true', start: new Date('2023-12-27'), end: new Date('2023-12-29'), cssClass: 'md-rect-bg' },{ slot: 1, allDay: 'true', start: new Date('2023-10-30'), end: new Date('2023-10-30'), cssClass: 'md-rect-bg' },
            {background: '#a5ceff4d',slot: 1,recurring: { repeat: 'weekly', weekDays: 'SU,SA' },recurringException: [moment().format('YYYY-MM-DD')]},
            { date: moment().format('YYYY-MM-DD'), background: 'cornsilk' },
          ],
        });
      }
      else if (event.value == 'custom'){
        var date = myRange.getVal();
        localStorage.setItem('carRevSelectRange', 'custom');
        localStorage.setItem('carRevSelectStart', date[0]);
        localStorage.setItem('carRevSelectEnd', date[1]);
        calendar.setOptions({
          colors: [
            { slot: 1, allDay: 'true', start: new Date('2025-01-01'), end: new Date('2025-01-01'), cssClass: 'md-dots-bg' },{ slot: 1, allDay: 'true', start: new Date('2025-01-02'), end: new Date('2025-01-02'), cssClass: 'md-dots-bg' },{ slot: 1, allDay: 'true', start: new Date('2025-02-08'), end: new Date('2025-02-08'), cssClass: 'md-dots-bg' },{ slot: 1, allDay: 'true', start: new Date('2025-04-21'), end: new Date('2025-04-21'), cssClass: 'md-dots-bg' },{ slot: 1, allDay: 'true', start: new Date('2025-04-27'), end: new Date('2025-04-27'), cssClass: 'md-dots-bg' },{ slot: 1, allDay: 'true', start: new Date('2025-05-01'), end: new Date('2025-05-02'), cssClass: 'md-dots-bg' },{ slot: 1, allDay: 'true', start: new Date('2025-05-19'), end: new Date('2025-05-19'), cssClass: 'md-dots-bg' },{ slot: 1, allDay: 'true', start: new Date('2025-06-25'), end: new Date('2025-06-25'), cssClass: 'md-dots-bg' },{ slot: 1, allDay: 'true', start: new Date('2025-08-15'), end: new Date('2025-08-15'), cssClass: 'md-dots-bg' },{ slot: 1, allDay: 'true', start: new Date('2025-10-31'), end: new Date('2025-11-1 21:00'), cssClass: 'md-dots-bg' },{ slot: 1, allDay: 'true', start: new Date('2025-12-25'), end: new Date('2025-12-26')},{ slot: 1, allDay: 'true', start: new Date('2026-01-01'), end: new Date('2026-01-02'), cssClass: 'md-dots-bg' },{ slot: 1, allDay: 'true', start: new Date('2023-07-17'), end: new Date('2023-08-16'), cssClass: 'md-stripes-bg' },{ slot: 1, allDay: 'true', start: new Date('2023-04-24'), end: new Date('2023-04-28'), cssClass: 'md-rect-bg' },{ slot: 1, allDay: 'true', start: new Date('2023-12-27'), end: new Date('2023-12-29'), cssClass: 'md-rect-bg' },{ slot: 1, allDay: 'true', start: new Date('2023-10-30'), end: new Date('2023-10-30'), cssClass: 'md-rect-bg' },
            {background: '#a5ceff4d',slot: 1,recurring: { repeat: 'weekly', weekDays: 'SU,SA' },recurringException: [moment().format('YYYY-MM-DD')]},
            { date: moment().format('YYYY-MM-DD'), background: 'cornsilk' },
          ],
        });
      }
      else {
        // myRange.setVal([]);
        setTimeout(() => {
          selectRange.setOptions({data: [ { text: 'Dan', value: 'day' }, { text: 'Teden', value: 'week' }, { text: 'Teden (razširjeno)', value: 'weekExt' }, { text: '2 tedna', value: '2week' }, { text: '4 Tedne', value: 'weekMonth' }, { text: 'Mesec', value: 'month' }, { text: '2 meseca', value: '2month' }, { text: 'Pol leta', value: '6month' }, { text: 'Leto', value: 'year' }, { text: 'Ročno', value: 'custom', disabled: true }]});
          localStorage.setItem('carRevSelectRange', 'year');
        }, 500);
        calendar.setOptions({
          view: {
            timeline: {
              type: 'month',
              size: 12,
              weekNumbers: true,
            }
          },
          colors: [
            { slot: 1, allDay: 'true', start: new Date('2025-01-01'), end: new Date('2025-01-01'), cssClass: 'md-dots-bg' },{ slot: 1, allDay: 'true', start: new Date('2025-01-02'), end: new Date('2025-01-02'), cssClass: 'md-dots-bg' },{ slot: 1, allDay: 'true', start: new Date('2025-02-08'), end: new Date('2025-02-08'), cssClass: 'md-dots-bg' },{ slot: 1, allDay: 'true', start: new Date('2025-04-21'), end: new Date('2025-04-21'), cssClass: 'md-dots-bg' },{ slot: 1, allDay: 'true', start: new Date('2025-04-27'), end: new Date('2025-04-27'), cssClass: 'md-dots-bg' },{ slot: 1, allDay: 'true', start: new Date('2025-05-01'), end: new Date('2025-05-02'), cssClass: 'md-dots-bg' },{ slot: 1, allDay: 'true', start: new Date('2025-05-19'), end: new Date('2025-05-19'), cssClass: 'md-dots-bg' },{ slot: 1, allDay: 'true', start: new Date('2025-06-25'), end: new Date('2025-06-25'), cssClass: 'md-dots-bg' },{ slot: 1, allDay: 'true', start: new Date('2025-08-15'), end: new Date('2025-08-15'), cssClass: 'md-dots-bg' },{ slot: 1, allDay: 'true', start: new Date('2025-10-31'), end: new Date('2025-11-1 21:00'), cssClass: 'md-dots-bg' },{ slot: 1, allDay: 'true', start: new Date('2025-12-25'), end: new Date('2025-12-26')},{ slot: 1, allDay: 'true', start: new Date('2026-01-01'), end: new Date('2026-01-02'), cssClass: 'md-dots-bg' },{ slot: 1, allDay: 'true', start: new Date('2023-07-17'), end: new Date('2023-08-16'), cssClass: 'md-stripes-bg' },{ slot: 1, allDay: 'true', start: new Date('2023-04-24'), end: new Date('2023-04-28'), cssClass: 'md-rect-bg' },{ slot: 1, allDay: 'true', start: new Date('2023-12-27'), end: new Date('2023-12-29'), cssClass: 'md-rect-bg' },{ slot: 1, allDay: 'true', start: new Date('2023-10-30'), end: new Date('2023-10-30'), cssClass: 'md-rect-bg' },
            {background: '#a5ceff4d',slot: 1,recurring: { repeat: 'weekly', weekDays: 'SU,SA' },recurringException: [moment().format('YYYY-MM-DD')]},
            { date: moment().format('YYYY-MM-DD'), background: 'cornsilk' },
          ],
        });
      }
    }
  });

  // add event listeners to elements on calendar
  $('.mbsc-timeline-resource').on('click', function (args) {
    onCarClickEvent(args);
  });
}
function getData(tmpS, tmpE){
  $.get( "/apiv2/cars/reservations/", {start: tmpS, end: tmpE}, function( data ) {
    //console.log(data)
    carRevs = data.carRevs;
    carRevs.forEach(cr => {
      cr.resource = 'car' + cr.car;
      cr.title = cr.user_name;
      cr.editable = (cr.owner_name == (loggedUser.name + ' ' + loggedUser.surname) || loggedUser.role == 'admin' || loggedUser.role.includes('vodja')) ? true : false;
    })
    calendar.setEvents(carRevs);
    refreshTimerCount = 0;
    $('#refreshData').blur();
  });
}
// returns the formatted date
function getFormattedRange(start, end) {
  return formatDate('MMM D, YYYY', new Date(start)) + (end && getNrDays(start, end) > 1 ? (' - ' + formatDate('MMM D, YYYY', new Date(end))) : '');
}
// returns the number of days between two dates
function getNrDays(start, end) {
  return Math.round(Math.abs((end.setHours(0) - start.setHours(0)) / (24 * 60 * 60 * 1000))) + 1;
}
// function for refreshing data on button click
function refreshData(){
  let tmpS = moment(calendar._firstDay).add(-1, 'days').format("YYYY-MM-DDTHH:mm");
  let tmpE = moment(calendar._lastDay).add(1, 'days').format("YYYY-MM-DDTHH:mm");
  getData(tmpS, tmpE);
}
// VARIABLES
const rgb2hex = (rgb) => `#${rgb.match(/^rgb\((\d+),\s*(\d+),\s*(\d+)\)$/).slice(1).map(n => parseInt(n, 10).toString(16).padStart(2, '0')).join('')}`
var loggedUser;
var activeUser = 1;
// var users;
var carRevs;
var formatDate;
var tempCarRev;
var clickToCreateVar = true;
var dragToCreateVar = false;
var dragToMoveVar = true;
var dragToResizeVar = true;
var userReservationEvent = false;
var refreshTimerCount = 0;
var calendar;
var timer;
var tmpCR;
var deletedCR;
var selectRange;
var tooltip = document.getElementById('custom-event-tooltip-popup');
var tooltipHeader = document.getElementById('tooltip-event-header');
var tooltipTitle = document.getElementById('tooltip-event-title');
var tooltipAdditionalTitle = document.getElementById('tooltip-event-additional-title');
var tooltipLocation = document.getElementById('tooltip-location');
var tooltipKeys = document.getElementById('has-keys-switch');
var tooltipKeysText = document.getElementById('tooltip-event-keys');
var tooltipStatus = document.getElementById('tooltip-event-status');
var tooltipUser = document.getElementById('tooltip-reservation-select');
var tooltipOwner = document.getElementById('tooltip-event-owner');
var deleteButton = document.getElementById('tooltip-event-delete');
var statusButton = document.getElementById('status-button');
var saveButton = document.getElementById('save-location-button');
var addCarButton;
var tmpCar;
var sloLocal = {
  setText: "Potrdi",
  cancelText: "Prekliči",
  clearText: "Izbriši",
  closeText: "Zapri",
  selectedText: "{count} izbrano",
  dateFormat: "DD.MM.YYYY",
  dateFormatLong: "DDD, D. MMM. YYYY.",
  dateWheelFormat: "|DDD D MMM|",
  dayNames: ["Nedelja", "Ponedeljek", "Torek", "Sreda", "Četrtek", "Petek", "Sobota"],
  dayNamesShort: ["Ned", "Pon", "Tor", "Sre", "Čet", "Pet", "Sob"],
  dayNamesMin: ["Ne", "Po", "To", "Sr", "Če", "Pe", "So"],
  monthNames: ["Januar", "Februar", "Marec", "April", "Maj", "Junij", "Julij", "Avgust", "September", "Oktober", "November", "December"],
  monthNamesShort: ["Jan", "Feb", "Mar", "Apr", "Maj", "Jun", "Jul", "Avg", "Sep", "Okt", "Nov", "Dec"],
  timeFormat: "H:mm",
  nowText: "Zdaj",
  pmText: "pm",
  amText: "am",
  firstDay: 0,
  dateText: "Datum",
  timeText: "Čas",
  todayText: "Danes",
  prevMonthText: "Prejšnji mesec",
  nextMonthText: "Naslednji mesec",
  prevYearText: "Prejšnje leto",
  nextYearText: "Naslednje leto",
  eventText: "Dogodek",
  eventsText: "Dogodki",
  allDayText: "Ves dan",
  noEventsText: "Brez dogodkov",
  moreEventsText: "Še {count}",
  rangeStartLabel: "Začetek",
  rangeEndLabel: "Konec",
  rangeStartHelp: "Izberite",
  rangeEndHelp: "Izberite",
  filterEmptyText: "Brez rezultata",
  filterPlaceholderText: "Išči",
  weekText: '{count}. teden',
};