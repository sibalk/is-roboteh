$(function(){
  // get users/employees
  $.get( "/apiv2/employees", {}, function( data ) {
    //console.log(data)
    users = data.users;
    // for (const user of users) {
    //   if (user.active){
    //     let element = document.createElement('option');
    //     element.textContent = user.name + ' ' + user.surname;
    //     element.value = user.id;
    //     tooltipUser.appendChild(element);
    //   }
    // }
    users.forEach(user => {
      user.text = user.name + ' ' + user.surname;
    });
    $(".users-select").select2({
      data: users,
      tags: false,
      // minimumResultsForSearch: -1,
      dropdownParent: $('#custom-event-tooltip-popup'),
    });
  });
  // departments and cars
  $.get( "/apiv2/departments", {}, function( data ) {
    //console.log(data)
    allDepartments = data.departments;
    allDepartments.forEach(department => {
      department.text = department.department;
    });
    $(".select-department").select2({
      data: allDepartments,
      tags: false,
      minimumResultsForSearch: -1,
      dropdownParent: $('#addCarform'),
    });
    $(".select-department-edit").select2({
      data: allDepartments,
      tags: false,
      minimumResultsForSearch: -1,
      dropdownParent: $('#modalEditCar'),
    });
    // get cars and call initCalendar to make view for current week of cars
    $.get( "/apiv2/cars", {}, function( data ) {
      //console.log(data)
      cars = data.cars.filter(c => c.active);
      cars.forEach(c => {
        c.realId = c.id;
        c.name = c.model + ', ' + c.year;
        c.title = c.license_plate;
        c.id = 'car' + c.id;
      });
      // prepare resource array for calendar with departments and their assignated cars
      let emptyDepartment;
      for (const department of allDepartments) {
        if (department.department == 'Prazno')
          emptyDepartment = {id: department.department,eventCreation: false, realId: department.id, name: department.department, collapsed: false, children: []};
        else
          departmentsWithCarsResource.push({id: department.department, eventCreation: false, realId: department.id, name: department.department, collapsed: false, children: []});
      }
      departmentsWithCarsResource.push(emptyDepartment);
      for (const car of cars) {
        if (!car.active)
          continue;
        let departmentResource = departmentsWithCarsResource.find(d => d.realId == car.department_id);
        if (departmentResource)
          departmentResource.children.push(car);
        // else
        //   departmentsWithCarsResource.find(d => d.id == 'Prazno');
      }
      initCalendar();
    });
  });
  // on event functions
  // on click/toggle malfunction
  $('#malfunctionToggleButton').on('click', function(args) {
    if (tmpCar.malfunction){
      $('#malfunctionToggleButton').children().removeClass('text-warning');
      $('#' + tmpCar.id).find('.fa-exclamation-triangle').removeClass('text-warning');
      tmpCar.malfunction = false;
    }
    else{
      $('#malfunctionToggleButton').children().addClass('text-warning');
      $('#' + tmpCar.id).find('.fa-exclamation-triangle').addClass('text-warning');
      tmpCar.malfunction = true;
    }
  })
  // on click/toggle maintenance
  $('#maintenanceToggleButton').on('click', function(args) {
    if (tmpCar.maintenance){
      $('#maintenanceToggleButton').children().removeClass('text-danger');
      $('#' + tmpCar.id).find('.fa-wrench').removeClass('text-danger');
      tmpCar.maintenance = false;
    }
    else{
      $('#maintenanceToggleButton').children().addClass('text-danger');
      $('#' + tmpCar.id).find('.fa-wrench').addClass('text-danger');
      tmpCar.maintenance = true;
    }
  })
  // on click/toggle wheels
  $('#wheelsToggleButton').on('click', function(args) {
    if (tmpCar.wheels){
      $('#wheelsToggleButton').children().removeClass('text-info');
      $('#' + tmpCar.id).find('.fa-dot-circle').removeClass('text-info');
      tmpCar.wheels = false;
    }
    else{
      $('#wheelsToggleButton').children().addClass('text-info');
      $('#' + tmpCar.id).find('.fa-dot-circle').addClass('text-info');
      tmpCar.wheels = true;
    }
  })
  // on click/toggle inspection
  $('#inspectionToggleButton').on('click', function(args) {
    if (tmpCar.inspection){
      $('#inspectionToggleButton').children().removeClass('text-success');
      $('#' + tmpCar.id).find('.fa-clipboard-list').removeClass('text-success');
      tmpCar.inspection = false;
    }
    else{
      $('#inspectionToggleButton').children().addClass('text-success');
      $('#' + tmpCar.id).find('.fa-clipboard-list').addClass('text-success');
      tmpCar.inspection = true;
    }
  })
  // on edit button event -> enable editing
  $('#editCarButton').on('click', startEditCar);
  // on cancel edit event - undo changes and disable editing
  $('#cancelEditCarButton').on('click', cancelEditCar);
  // on save edit event - save changes and disable editing
  $('#saveEditCarButton').on('click', saveEditCar);
  // on submit adding car form and prevent default action to clear input on success and open error modal on error/fail
  $('#addCarform').submit(function(e){
    e.preventDefault();
    $form = $(this);
    var manufacturer = manufacturerInput.value;
    var model = modelInput.value;
    var year = yearInput.value;
    var licensePlate = plateInput.value.toUpperCase();
    var department = departmentInput.value;
    var registrationExpiration = registrationExpirationInput.value ? moment(registrationExpirationInput.value).format('YYYY-MM-DD 13:00') : null;
    var vignetteExpiration = vignetteExpirationInput.value ? moment(vignetteExpirationInput.value).format('YYYY-MM-DD 13:00') : null;

    var data = {manufacturer, model, year, licensePlate, department, registrationExpiration, vignetteExpiration};
    debugger;
    $.ajax({
      type: 'POST',
      url: '/apiv2/cars/',
      contentType: 'application/json',
      data: JSON.stringify(data), // access in body
    }).done(function (resp) {
      // console.log('SUCCESS');
      let tmpCar = resp.car;
      tmpCar.realId = resp.car.id;
      tmpCar.name = tmpCar.model + ', ' + tmpCar.year;
      tmpCar.title = tmpCar.license_plate;
      tmpCar.id = 'car' + tmpCar.id;
      tmpCar.department = allDepartments.find(d => d.id == tmpCar.department_id).department;
      calendar.setOptions({resources: []});
      let departmentResource = departmentsWithCarsResource.find(d => d.realId == tmpCar.department_id);
      if (departmentResource)
        departmentResource.children.push(tmpCar);
      cars.push(tmpCar);
      setTimeout(() => {
        calendar.setOptions({resources: departmentsWithCarsResource});
      }, 200);

      // close modal and empty value
      $('#modalAddCarForm').modal('toggle');
      manufacturerInput.value = '';
      modelInput.value = '';
      yearInput.value = '';
      plateInput.value = '';
      setTimeout(() => {
        // add event listeners to elements on calendar
        $('.mbsc-timeline-resource').on('click', function (args) {
          onCarClickEvent(args);
        });
      }, 300);

    }).fail(function (resp) {
      //console.log('FAIL');
      //debugger;
      // popup.close();
      $('#errorModalMsg').text('Napaka pri dodajanju vozila v podatkovno bazo. Osvežite stran in ob ponovni napaki obvestite admninistratorja RIS-a.')
      $('#modalError').modal('toggle');
    });
  })
  // on delete car event -> remove car from object cars and update resources on calendar
  $('#deleteCarButton').on('click', modalConfCarDelete);
  // on event call funkctions
  $("#modalEditCar").on("hidden.bs.modal", cancelEditCar);
  $('#btnDeleteCarConf').on('click', deleteCar);
  $("#modalDeleteCar").on("hidden.bs.modal", () =>{
    // debugger
    let car = cars.find(c => c.id == tmpCar.id);
    if (car)
      $('#modalEditCar').modal('toggle');
  });
})
// on resource click function
function onCarClickEvent(args) {
  tmpCar = cars.find(c => c.id == args.currentTarget.children[0].children[0].id)
  if (tmpCar){
    $('#editCarHeader').text(tmpCar.manufacturer + ' ' + tmpCar.name + ', ' + tmpCar.license_plate);
    $('#carManufacturerInput').val(tmpCar.manufacturer);
    $('#carModelInput').val(tmpCar.model);
    $('#carYearInput').val(tmpCar.year);
    $('#carPlateInput').val(tmpCar.license_plate);
    $('#carRegistrationExpirationInput').val(moment(tmpCar.registration_expiration).format('YYYY-MM-DD'));
    $('#carVignetteExpirationInput').val(moment(tmpCar.vignette_expiration).format('YYYY-MM-DD'));
    $('#carDepartmentInput').val(tmpCar.department_id).trigger('change');
    tmpCar.malfunction ? $('#malfunctionToggleButton').children().addClass('text-warning') : $('#malfunctionToggleButton').children().removeClass('text-warning');
    tmpCar.maintenance ? $('#maintenanceToggleButton').children().addClass('text-danger') : $('#maintenanceToggleButton').children().removeClass('text-danger');
    tmpCar.wheels ? $('#wheelsToggleButton').children().addClass('text-info') : $('#wheelsToggleButton').children().removeClass('text-info');
    tmpCar.inspection ? $('#inspectionToggleButton').children().addClass('text-success') : $('#inspectionToggleButton').children().removeClass('text-success');

    // get and fill car notes in notes body
    $('#carNotes').empty();
    // call function to get all notes and fill
    getCarNotesAndAddToBody(tmpCar.realId);
    // let tmpCarNotes = carNotes.find(cn => cn.car_id == tmpCar.id);

    $('#modalEditCar').modal('toggle');
  }
}
// function for saving editing selected car
function saveEditCar(args){
  let manufacturer = $('#carManufacturerInput').val();
  let model = $('#carModelInput').val();
  let year = $('#carYearInput').val();
  let licensePlate = $('#carPlateInput').val();
  let registrationExpiration = $('#carRegistrationExpirationInput').val() ? moment(moment($('#carRegistrationExpirationInput').val()).format('YYYY-MM-DD 13:00')).toISOString() : null;
  let vignetteExpiration = $('#carVignetteExpirationInput').val() ? moment(moment($('#carVignetteExpirationInput').val()).format('YYYY-MM-DD 13:00')).toISOString() : null;
  let department = $('#carDepartmentInput').val();
  var data = {manufacturer, model, year, licensePlate, registrationExpiration, vignetteExpiration, department};
  debugger;
  $.ajax({
    type: 'PUT',
    url: '/apiv2/cars/' + tmpCar.realId,
    contentType: 'application/json',
    data: JSON.stringify(data), // access in body
  }).done(function (resp) {
    // console.log('SUCCESS');
    // check if model or year or licensePlate or department has changed -> also update resources so the new names/variables are visible on resources as well
    // let updateResource = (tmpCar.manufacturer != manufacturer || tmpCar.model != model || tmpCar.year != year || tmpCar.license_plate != licensePlate) ? true : false;
    let departmentChange = tmpCar.department_id != department ? true : false;
    let oldDepartmentName = tmpCar.department;
    tmpCar = resp.car;
    tmpCar.realId = resp.car.id;
    tmpCar.name = tmpCar.model + ', ' + tmpCar.year;
    tmpCar.title = tmpCar.license_plate;
    tmpCar.id = 'car' + tmpCar.id;
    tmpCar.department = allDepartments.find(d => d.id == tmpCar.department_id).department;
    $('#' + tmpCar.id).children('div')[0].innerHTML = tmpCar.model + ', ' + tmpCar.year;
    $('#' + tmpCar.id).children('div')[1].innerHTML = tmpCar.license_plate;
    if (departmentChange){
      calendar.setOptions({resources: []});
      departmentsWithCarsResource.find(d => d.id == oldDepartmentName).children = departmentsWithCarsResource.find(d => d.id == oldDepartmentName).children.filter(car => car.realId != tmpCar.realId);
      let departmentResource = departmentsWithCarsResource.find(d => d.realId == tmpCar.department_id);
      if (departmentResource)
        departmentResource.children.push(tmpCar);
      setTimeout(() => {
        calendar.setOptions({resources: departmentsWithCarsResource});
      }, 200);
    }
    $('#cancelEditCarButton').addClass('d-none');
    $('#saveEditCarButton').addClass('d-none');
    $('#deleteCarButton').removeClass('d-none');
    $('#editCarButton').removeClass('d-none');
    // $('#addCarNoteButton').removeClass('d-none');
    $('#carManufacturerInput').prop('disabled', true);
    $('#carModelInput').prop('disabled', true);
    $('#carYearInput').prop('disabled', true);
    $('#carPlateInput').prop('disabled', true);
    $('#carRegistrationExpirationInput').prop('disabled', true);
    $('#carVignetteExpirationInput').prop('disabled', true);
    $('#carDepartmentRow').addClass('d-none');
  }).fail(function (resp) {
    //console.log('FAIL');
    //debugger;
    // popup.close();
    $('#errorModalMsg').text('Napaka pri urejanja vozila v podatkovno bazo. Osvežite stran in ob ponovni napaki obvestite admninistratorja RIS-a.')
    $('#modalError').modal('toggle');
  });
}
// cancel editing selected car ---> undo changes, disable inputs and show/hide buttons
function cancelEditCar(args){
  if (loggedUser.role.toLowerCase() == 'admin' || loggedUser.role.toLowerCase() == 'tajnik' || loggedUser.role.toLowerCase() == 'komercialist' || loggedUser.role.toLowerCase() == 'komerciala' || loggedUser.role.toLowerCase() == 'računovodstvo'){
    $('#cancelEditCarButton').addClass('d-none');
    $('#saveEditCarButton').addClass('d-none');
    $('#deleteCarButton').removeClass('d-none');
    $('#editCarButton').removeClass('d-none');
  }
  $('#addCarNoteButton').removeClass('d-none');
  $('#carManufacturerInput').prop('disabled', true);
  $('#carModelInput').prop('disabled', true);
  $('#carYearInput').prop('disabled', true);
  $('#carPlateInput').prop('disabled', true);
  $('#carRegistrationExpirationInput').prop('disabled', true);
  $('#carVignetteExpirationInput').prop('disabled', true);
  $('#carManufacturerInput').val(tmpCar.manufacturer);
  $('#carModelInput').val(tmpCar.model);
  $('#carYearInput').val(tmpCar.year);
  $('#carPlateInput').val(tmpCar.license_plate);
  $('#carRegistrationExpirationInput').val(moment(tmpCar.registration_expiration).format('YYYY-MM-DD'));
  $('#carVignetteExpirationInput').val(moment(tmpCar.vignette_expiration).format('YYYY-MM-DD'));
  $('#carDepartmentRow').addClass('d-none');
}
// start editing selected car ---> enable inputs and show correct buttons for cancel/save edit
function startEditCar(args){
  $('#deleteCarButton').addClass('d-none');
  $('#editCarButton').addClass('d-none');
  // $('#addCarNoteButton').addClass('d-none');
  $('#cancelEditCarButton').removeClass('d-none');
  $('#saveEditCarButton').removeClass('d-none');
  $('#carManufacturerInput').prop('disabled', false);
  $('#carModelInput').prop('disabled', false);
  $('#carYearInput').prop('disabled', false);
  $('#carPlateInput').prop('disabled', false);
  $('#carRegistrationExpirationInput').prop('disabled', false);
  $('#carVignetteExpirationInput').prop('disabled', false);
  $('#carDepartmentRow').removeClass('d-none');
}
// open modal for confirming delete car
function modalConfCarDelete(args){
  $('#modalEditCar').modal('toggle');
  $('#modalDeleteCar').modal('toggle');
}
// delete car and remove it from resource list
function deleteCar(args){
  var data = {carId: tmpCar.realId, activity: false};
  $.ajax({
    type: 'DELETE',
    url: '/apiv2/cars/'+tmpCar.realId,
    contentType: 'application/json',
    data: JSON.stringify(data), // access in body
  }).done(function (resp) {
    // console.log('SUCCESS');
    calendar.setOptions({resources: []});
    departmentsWithCarsResource.find(d => d.id == tmpCar.department).children = departmentsWithCarsResource.find(d => d.id == tmpCar.department).children.filter(car => car.realId != tmpCar.realId);
    cars = cars.filter(c => c.id != tmpCar.id);
    setTimeout(() => {
      calendar.setOptions({resources: departmentsWithCarsResource});
      $('#modalDeleteCar').modal('toggle');
    }, 200);
    setTimeout(() => {
      // add event listeners to new resource elements on calendar
      $('.mbsc-timeline-resource').on('click', function (args) {
        onCarClickEvent(args);
      });
    }, 200);
  }).fail(function (resp) {
    //console.log('FAIL');
    //debugger;
    $('#errorModalMsg').text('Napaka pri brisanju vozila v podatkovni bazi. Osvežite stran in ob ponovni napaki obvestite admninistratorja RIS-a.')
    $('#modalError').modal('toggle');
  })
}
let allDepartments;
let departmentsWithCarsResource = [];
let cars;
let Users;