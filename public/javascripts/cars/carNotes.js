$(function(){
  // get car note statuses and add text with status label for select2
  $.get( "/apiv2/cars/notes/status", {}, function( data ) {
    //console.log(data)
    carNotesStatus = data.carStatus;
    carNotesStatus.forEach(status => { status.text = status.status; });
  });
  // on event functions
  // add new note to selected car
  $('#addCarNoteButton').on('click', function(args){
    let noteHeader = loggedUser.name + ' ' + loggedUser.surname + ', ' + new Date().toLocaleString('sl');
    // get car note status and prepare there select element
    let statusCarNodeElements = ``;
    for (const status of carNotesStatus) {
      statusCarNodeElements += '<option value=' + status.id + '>' + status.status + '</option>'
    }
    let element = `<div class="card border-dark mt-2" id="carNoteNew">
      <div class="card-header d-flex">
        <div id="carNoteAuthorNew">` + noteHeader + `</div>
        <div class="form-group my-n2 ml-auto">
          <select class="form-control" id="carNoteStatusNew">
            ` + statusCarNodeElements + `
          </select>
        </div>
      </div>
      <div class="card-body">
        <div class="row">
          <div class="col-6">
            <div class="form-group d-flex justify-content-center">
              <label class="mr-2 mt-2" for="carNoteStartNew">Začetek</label>
              <input class="form-control w-150px" id="carNoteStartNew" type="date" aria-describedby="carNoteStart" placeholder="Začetek">
            </div>
          </div>
          <div class="col-6">
            <div class="form-group d-flex justify-content-center">
              <label class="mr-2 mt-2" for="carNoteEndNew">Konec</label>
              <input class="form-control w-150px" id="carNoteEndNew" type="date" aria-describedby="carNoteFinish" placeholder="Konec">
            </div>
          </div>
        </div>
        <div class="form-group">
          <textarea class="form-control" id="carNoteNoteNew" rows="3"></textarea>
        </div>
        <div class="row">
          <div class="col-12 d-flex">
            <button class="btn btn-danger ml-auto" id="cancelNewCarNoteButton"><i class="fas fa-times fa-lg"></i></button>
            <button class="btn btn-success ml-2" id="saveNewCarNoteButton"><i class="fas fa-check fa-lg"></i></button>
          </div>
        </div>
      </div>
    </div>`;
    $('#carNotes').prepend(element);
    $('#carNoteStartNew').val(moment().format("YYYY-MM-DD"));
    $('#carNoteEndNew').val(moment().format("YYYY-MM-DD"));
    $('#addCarNoteButton').addClass('d-none');
    $('#cancelNewCarNoteButton').on('click', {}, onEventCancelAddNewCarNote);
    $('#saveNewCarNoteButton').on('click', {}, onEventAddNewCarNote);
  })
  // call functions
  $("#modalDeleteCarNote").on("hidden.bs.modal", closeDeleteCarNoteModal);
})
// on event add new car note
function onEventAddNewCarNote(event){
  let start = $('#carNoteStartNew').val() + ' 07:00';
  let end = $('#carNoteStartNew').val() + ' 15:00';
  let carNote = $('#carNoteNoteNew').val();
  let statusId = $('#carNoteStatusNew').val();
  let carId = tmpCar.realId;
  // let status = $('#carNoteStatusNew>option:selected').text();
  var data = {start, end, note: carNote, statusId, carId};
  debugger;
  $.ajax({
    type: 'POST',
    url: '/apiv2/cars/notes/',
    contentType: 'application/json',
    data: JSON.stringify(data), // access in body
  }).done(function (resp) {
    // console.log('SUCCESS');
    resp.carNote.author = loggedUser.name + ' ' + loggedUser.surname;
    resp.carNote.status = $('#carNoteStatusNew>option:selected').text();
    // get new id from database
    let tmpId = resp.carNote.id;
    // get car note status and prepare there select element
    let statusCarNodeElements = ``;
    for (const status of carNotesStatus) {
      statusCarNodeElements += '<option value=' + status.id + '>' + status.status + '</option>'
    }
    let noteHeader = resp.carNote.author + ', ' + new Date(resp.carNote.created).toLocaleString('sl');
    let note = resp.carNote;
    tmpCarNotes.unshift(note);
    // let tmpCarNotes = carNotes.find(cn => cn.car_id == tmpCar.id)
    // tmpCarNotes ? tmpCarNotes.notes.unshift(note) : carNotes.push({car_id: tmpCar.id, notes: [note]});
    let element = `<div class="card border-dark mt-2" id="carNote` + tmpId + `">
      <div class="card-header d-flex">
        <div id="carNoteAuthor` + resp.carNote.id + `">` + noteHeader + `</div>
        <div class="form-group my-n2 ml-auto">
          <select class="form-control" id="carNoteStatus` + tmpId + `" disabled="">
            ` + statusCarNodeElements + `
          </select>
        </div>
      </div>
      <div class="card-body">
        <div class="row">
          <div class="col-6">
            <div class="form-group d-flex justify-content-center">
              <label class="mr-2 mt-2" for="carNoteStart` + tmpId + `">Začetek</label>
              <input class="form-control w-150px" id="carNoteStart` + tmpId + `" type="date" aria-describedby="carNoteStart" placeholder="Začetek" disabled="">
            </div>
          </div>
          <div class="col-6">
            <div class="form-group d-flex justify-content-center">
              <label class="mr-2 mt-2" for="carNoteEnd` + tmpId + `">Konec</label>
              <input class="form-control w-150px" id="carNoteEnd` + tmpId + `" type="date" aria-describedby="carNoteFinish" placeholder="Konec" disabled="">
            </div>
          </div>
        </div>
        <div class="form-group">
          <textarea class="form-control" id="carNoteNote` + tmpId + `" rows="3" disabled="">` + note.note + `</textarea>
        </div>
        <div class="row">
          <div class="col-12 d-flex">
            <button class="btn btn-danger ml-auto" id="deleteCarNoteButton` + tmpId + `"><i class="fas fa-trash fa-lg"></i></button>
            <button class="btn btn-danger ml-auto d-none" id="cancelEditCarNoteButton` + tmpId + `"><i class="fas fa-times fa-lg"></i></button>
            <button class="btn btn-success ml-2 d-none" id="saveEditCarNoteButton` + tmpId + `"><i class="fas fa-save fa-lg"></i></button>
            <button class="btn btn-primary ml-2" id="editCarNoteButton` + tmpId + `"><i class="fas fa-pen fa-lg"></i></button>
          </div>
        </div>
      </div>
    </div>`;
    $('#carNoteNew').remove();
    $('#carNotes').prepend(element);
    $('#carNoteStart' + tmpId).val(moment(note.start).format("YYYY-MM-DD"));
    $('#carNoteEnd' + tmpId).val(moment(note.end).format("YYYY-MM-DD"));
    $('#carNoteStatus' + tmpId).val(note.status_id);
    $('#deleteCarNoteButton'+tmpId).on('click', {id: tmpId}, onEventAskDeleteCarNote);
    $('#cancelEditCarNoteButton'+tmpId).on('click', {id: tmpId}, onEventCancelEditCarNote);
    $('#saveEditCarNoteButton'+tmpId).on('click', {id: tmpId}, onEventSaveEditCarNote);
    $('#editCarNoteButton'+tmpId).on('click', {id: tmpId}, onEventEditCarNote);
    $('#addCarNoteButton').removeClass('d-none');

  }).fail(function (resp) {
    //console.log('FAIL');
    //debugger;
    // popup.close();
    $('#errorModalMsg').text('Napaka pri dodajanju opombe na vozilo v podatkovno bazo. Osvežite stran in ob ponovni napaki obvestite admninistratorja RIS-a.')
    $('#modalError').modal('toggle');
  });

}
// on event start edit car note
function onEventEditCarNote(event) {
  debugger;
  $('#editCarNoteButton' + event.data.id).addClass('d-none');
  $('#deleteCarNoteButton' + event.data.id).addClass('d-none');
  $('#cancelEditCarNoteButton' + event.data.id).removeClass('d-none');
  $('#saveEditCarNoteButton' + event.data.id).removeClass('d-none');
  $('#carNoteStatus' + event.data.id).prop('disabled', false);
  $('#carNoteStart' + event.data.id).prop('disabled', false);
  $('#carNoteEnd' + event.data.id).prop('disabled', false);
  $('#carNoteNote' + event.data.id).prop('disabled', false);
}
// on event cancel adding new car note
function onEventCancelAddNewCarNote(event) {
  $('#carNoteNew').remove();
  $('#addCarNoteButton').removeClass('d-none');
}
// open modal for comfirming deletion of car note
function onEventAskDeleteCarNote(event){
  // console.log(event.data);
  $('#modalEditCar').modal('toggle');
  $('#btnDeleteCarNoteConf').off();
  $('#btnDeleteCarNoteConf').on('click', {id: event.data.id}, onEventDeleteCarNote);
  $('#modalDeleteCarNote').modal('toggle');
}
// closing delete modal, open edit car modal again
function closeDeleteCarNoteModal(event){
  // $('#modalDeleteCarNote').modal('toggle');
  $('#modalEditCar').modal('toggle');
}
// on event delete car note
function onEventDeleteCarNote(event) {
  debugger;
  // console.log(event.data);
  // first send call to delete and on success delete note on client side
  var data = {activity: false};
  $.ajax({
    type: 'DELETE',
    url: '/apiv2/cars/notes/'+event.data.id,
    contentType: 'application/json',
    data: JSON.stringify(data), // access in body
  }).done(function (resp) {
    // console.log('SUCCESS');
    // delete current event on button click and successful delete in database
    tmpCarNotes = tmpCarNotes.filter(cn => cn.id != event.data.id);
    $('#carNote' + event.data.id).remove();
    $('#modalDeleteCarNote').modal('toggle');
  }).fail(function (resp) {
    //console.log('FAIL');
    //debugger;
    tooltip.close();
    $('#errorModalMsg').text('Napaka pri brisanju opombe vozila v podatkovni bazi. Osvežite stran in ob ponovni napaki obvestite admninistratorja RIS-a.')
    $('#modalError').modal('toggle');
  });
}
// on event cancel edit car note
function onEventCancelEditCarNote(event) {
  debugger;
  $('#cancelEditCarNoteButton' + event.data.id).addClass('d-none');
  $('#saveEditCarNoteButton' + event.data.id).addClass('d-none');
  $('#editCarNoteButton' + event.data.id).removeClass('d-none');
  $('#deleteCarNoteButton' + event.data.id).removeClass('d-none');
  $('#carNoteStatus' + event.data.id).prop('disabled', true);
  $('#carNoteStart' + event.data.id).prop('disabled', true);
  $('#carNoteEnd' + event.data.id).prop('disabled', true);
  $('#carNoteNote' + event.data.id).prop('disabled', true);
  let tmpCarNote = tmpCarNotes.find(n => n.id == event.data.id);
  $('#carNoteStatus' + event.data.id).val(tmpCarNote.status_id);
  $('#carNoteStart' + event.data.id).val(moment(tmpCarNote.start).format("YYYY-MM-DD"));
  $('#carNoteEnd' + event.data.id).val(moment(tmpCarNote.end).format("YYYY-MM-DD"));
  $('#carNoteNote' + event.data.id).val(tmpCarNote.note);
}
// on event save car note
function onEventSaveEditCarNote(event) {
  debugger;
  // make call and on success save change to correct note and disable editing note
  let statusId = $('#carNoteStatus' + event.data.id).val();
  let start = $('#carNoteStart' + event.data.id).val() ? $('#carNoteStart' + event.data.id).val() + ' 07:00' : null;
  let end = $('#carNoteEnd' + event.data.id).val() ? $('#carNoteEnd' + event.data.id).val() + ' 15:00' : null;
  let carNote = $('#carNoteNote' + event.data.id).val();
  var data = {statusId, start, end, note: carNote};
  $.ajax({
    type: 'PUT',
    url: '/apiv2/cars/notes/'+event.data.id,
    contentType: 'application/json',
    data: JSON.stringify(data), // access in body
  }).done(function (resp) {
    // console.log('SUCCESS');
    let tmpCarNote = tmpCarNotes.find(n => n.id == event.data.id);
    tmpCarNote.status = $('#carNoteStatus' + event.data.id + '>option:selected').text();
    tmpCarNote.status_id = $('#carNoteStatus' + event.data.id).val();
    tmpCarNote.start = $('#carNoteStart' + event.data.id).val() ? new Date($('#carNoteStart' + event.data.id).val() + ' 07:00').toISOString() : null;
    tmpCarNote.end = $('#carNoteEnd' + event.data.id).val() ? new Date($('#carNoteEnd' + event.data.id).val() + ' 15:00').toISOString() : null;
    tmpCarNote.note = $('#carNoteNote' + event.data.id).val();
    $('#cancelEditCarNoteButton' + event.data.id).addClass('d-none');
    $('#saveEditCarNoteButton' + event.data.id).addClass('d-none');
    $('#editCarNoteButton' + event.data.id).removeClass('d-none');
    $('#deleteCarNoteButton' + event.data.id).removeClass('d-none');
    $('#carNoteStatus' + event.data.id).prop('disabled', true);
    $('#carNoteStart' + event.data.id).prop('disabled', true);
    $('#carNoteEnd' + event.data.id).prop('disabled', true);
    $('#carNoteNote' + event.data.id).prop('disabled', true);
  }).fail(function (resp) {
    //console.log('FAIL');
    //debugger;
    $('#errorModalMsg').text('Napaka pri urejanjus opombe vozila v podatkovni bazi. Osvežite stran in ob ponovni napaki obvestite admninistratorja RIS-a.')
    $('#modalError').modal('toggle');
  });
}
// get all car notes from database and fill notes body
function getCarNotesAndAddToBody(carId){
  let data = {carId};
  $.ajax({
    type: 'GET',
    url: '/apiv2/cars/'+carId+'/notes',
    contentType: 'application/x-www-form-urlencoded',
    data: data, // access in body
  }).done(function (resp) {
    debugger;
    tmpCarNotes = resp.carNotes;
    // get car note status and prepare there select element
    let statusCarNodeElements = ``;
    for (const status of carNotesStatus) {
      statusCarNodeElements += '<option value=' + status.id + '>' + status.status + '</option>'
    }
    // create div for note, append it to notes body and fill inputs with correct data from note
    for (const note of tmpCarNotes) {
      if (!note.active) continue;
      let noteHeader = note.author + ', ' + new Date(note.created).toLocaleString('sl');
      let hideControlClass = (loggedUser.role.toLowerCase() == 'admin' || note.author == loggedUser.name + ' ' + loggedUser.surname) ? '' : ' d-none';
      let element = `<div class="card border-dark mt-2" id="carNote` + note.id + `">
        <div class="card-header d-flex">
          <div id="carNoteAuthor` + note.id + `">` + noteHeader + `</div>
          <div class="form-group my-n2 ml-auto">
            <select class="form-control" id="carNoteStatus` + note.id + `" disabled="">
              ` + statusCarNodeElements + `
            </select>
          </div>
        </div>
        <div class="card-body">
          <div class="row">
            <div class="col-6">
              <div class="form-group d-flex justify-content-center">
                <label class="mr-2 mt-2" for="carNoteStart` + note.id + `">Začetek</label>
                <input class="form-control w-150px" id="carNoteStart` + note.id + `" type="date" aria-describedby="carNoteStart" placeholder="Začetek" disabled="">
              </div>
            </div>
            <div class="col-6">
              <div class="form-group d-flex justify-content-center">
                <label class="mr-2 mt-2" for="carNoteEnd` + note.id + `">Konec</label>
                <input class="form-control w-150px" id="carNoteEnd` + note.id + `" type="date" aria-describedby="carNoteFinish" placeholder="Konec" disabled="">
              </div>
            </div>
          </div>
          <div class="form-group">
            <textarea class="form-control" id="carNoteNote` + note.id + `" rows="3" disabled="">` + note.note + `</textarea>
          </div>
          <div class="row">
            <div class="col-12 d-flex">
              <button class="btn btn-danger ml-auto` + hideControlClass + `" id="deleteCarNoteButton` + note.id + `"><i class="fas fa-trash fa-lg"></i></button>
              <button class="btn btn-danger ml-auto d-none" id="cancelEditCarNoteButton` + note.id + `"><i class="fas fa-times fa-lg"></i></button>
              <button class="btn btn-success ml-2 d-none" id="saveEditCarNoteButton` + note.id + `"><i class="fas fa-save fa-lg"></i></button>
              <button class="btn btn-primary ml-2` + hideControlClass + `" id="editCarNoteButton` + note.id + `"><i class="fas fa-pen fa-lg"></i></button>
            </div>
          </div>
        </div>
      </div>`;
      $('#carNotes').prepend(element);
      $('#carNoteStart' + note.id).val(moment(note.start).format("YYYY-MM-DD"));
      $('#carNoteEnd' + note.id).val(moment(note.end).format("YYYY-MM-DD"));
      $('#carNoteStatus' + note.id).val(note.status_id);
      // $('#deleteCarNoteButton'+note.id).on('click', {id: note.id}, onEventDeleteCarNote);
      $('#deleteCarNoteButton'+note.id).on('click', {id: note.id}, onEventAskDeleteCarNote);
      $('#cancelEditCarNoteButton'+note.id).on('click', {id: note.id}, onEventCancelEditCarNote);
      $('#saveEditCarNoteButton'+note.id).on('click', {id: note.id}, onEventSaveEditCarNote);
      $('#editCarNoteButton'+note.id).on('click', {id: note.id}, onEventEditCarNote);
    }
  }).fail(function (resp) {
    //console.log('FAIL');
    //debugger;
    
    $('#modalError').modal('toggle');
  })
}
var carNotesStatus;
var tmpCarNotes;
var tmpCarNoteId;