//IN THIS SCRIPT//
//// create new work orders, show work ordes, show selected work order, edit or delete work order ////
$(function(){
	// on page load
	$('.mypopover').popover({ html: true });

	// on event call functions
	// $('#workOrderList .card.card-report').on('click', function (event) {
	// 	debugger;
	// 	selectWorkOrder(this.id.substring(9));
	// });
	$(document).on("click", '#workOrderList .card.card-report', function() {
		// alert("You have just clicked on ");
		selectWorkOrder(this.id.substring(9));
	});
	$('#addNewWorkOrderForm button.btn-secondary').on('click', closeWorkOrderNew);
	$('#addNewBtnCollapse').on('click', addNewWorkOrder);
	// $('#addNewBtnCollapse').on('click', function (params) {
	// 	if (true){
	// 		addNewWorkOrder();
	// 	}
	// 	else{
	// 		$('#addNewWorkOrderForm').submit();
	// 	}
	// })
	$('#woMailButton').on('click', sendMail);
	$('#woDeleteButton').on('click', openDeleteModal);
	$('#woEditButton').on('click', openWorkOrderEdit);
	$('#woCloseButton').on('click', closeWorkOrderInfo);
	$('#unsaveService').on('click', function (event) {
		saveService(true);
	})
	$('#saveService').on('click', function (event) {
		saveService(false);
	})
	$('#editWorkOrderForm button.btn-secondary').on('click', closeWorkOrderEdit);
	$('#btnDeleteWO').on('click', deleteWorkOrder);

	//$('[data-toggle="popover"]').popover()
	setTimeout(()=>{
		$('[data-toggle="tooltip"]').tooltip();
		//get user info from page
		loggedUser = {name:$('#loggedUser').html().split(" (")[0].split(" ")[0], surname:$('#loggedUser').html().split(" (")[0].split(" ")[1], role:$('#loggedUser').html().split(" (")[1].split(")")[0].toLowerCase()};
		activeUserRole = loggedUser.role;
		//UPORABNIKI
		$.get( "/employees/all", function( data ) {
			if(data.success){
				allUsers = data.data;
				for (const u of allUsers) { u.text = u.name + ' ' + u.surname }
				let tmpUsersForSelect = allUsers.filter(u => u.text.substring(0,4).toLowerCase() != 'info');
				tmpUsersForSelect.unshift({id:0, text: "Vsi"});
				// filling data with all active and unactive users for editing work orders and searching (if old data also needs unactive user)
				$(".select2-users").select2({
					data: tmpUsersForSelect,
				});
				$(".select2-edit-users").select2({
					data: tmpUsersForSelect,
					multiple: true,
				});
				$(".select2-edit-user").select2({
					data: tmpUsersForSelect,
				});
				// filter out unactive users and fill users data to select for new work orders
				tmpUsersForSelect = tmpUsersForSelect.filter(u => u.active == true);
				tmpUsersForSelect.shift();
				// allUsers = data.data;
				$(".select2-new-users").select2({
					data: tmpUsersForSelect,
					multiple: true,
				});
				$(".select2-new-user").select2({
					data: tmpUsersForSelect,
				});
				// $(".select2-edit-report-users").select2({
				// 	data: allUsers,
				// });
				userId = allUsers.find(u => u.text == (loggedUser.name+" "+loggedUser.surname)).id;
				$('#woWorkerNew').val(userId).trigger('change');
				if (!(loggedUser.role.toLowerCase() == 'admin' || loggedUser.role.substring(0,5).toLocaleLowerCase() == 'vodja' || loggedUser.role.toLowerCase() == 'tajnik' || loggedUser.role == 'komercialist' || loggedUser.role == 'komerciala' || loggedUser.role == 'računovodstvo')){
					$('#woWorkersNew').val(userId).trigger('change');
				}
				// filter all users who have work mails and add them to the list for mail select
				let mailUsers = allUsers.filter(u => u.work_mail != null).map(obj => ({ ...obj }));
				mailUsers.forEach(user => user.text = user.work_mail);
				$(".select2-email").select2({
					data: mailUsers,
					tags: true,
					multiple: true,
					placeholder: "Vpišite elektronske naslove kamor želite poslati delovni nalog.",
				});
			}
			else{
				//open error modal
			}
			/*
			if(loggedUser.role == 'admin' || loggedUser.role == 'tajnik' || loggedUser.role.substring(0,5) == 'vodja'){
				userId = allUsers.find(u => u.text == (loggedUser.name+" "+loggedUser.surname)).id;
				$('#woWorkerNew').val(userId).trigger('change');
				//$('#woUserSelect').val(userId).trigger('change');
				//$('#newReportUser').val(userId).trigger('change');
			}
			*/
		});
		//SERVISI
		getAllServices();
		//TIPI
		$.get( "/workorders/types", function( data ) {
			if(data.success){
				allTypes = data.data;
				allTypes.unshift({id:0, text: "Vsi"});
				$(".select2-type").select2({
					data: allTypes,
					minimumResultsForSearch: Infinity,
					placeholder: "Izberi tip",
				});
				allTypes.shift();
				$(".select2-new-type").select2({
					data: allTypes,
					minimumResultsForSearch: Infinity,
					placeholder: "Tip"
				});
			}
			else{
				//open error modal
			}
			//allTypes.unshift({id:0, text: "Vsi"});
		});
		//STATUSI
		$.get( "/workorders/status", function( data ) {
			if(data.success){
				allStatus = data.data;
				// fill select on info section of selected work order
				$(".select2-status").select2({
					data: allStatus,
					minimumResultsForSearch: Infinity,
					placeholder: "Izberi status",
				});
				$('#workOrderStatus').change(changeWorkOrderStatus);
				// fill select filter in search/filter form
				allStatus.unshift({id:0, text: "Vsi"});
				$(".select2-status-search").select2({
					data: allStatus,
					minimumResultsForSearch: Infinity,
					placeholder: "Izberi status",
				});
				//allTypes.unshift({id:0, text: "Vsi"});
			}
			else{
				//open error modal
			}
		});
		//NAROČNIKI
		$.get( "/subscribers/all", function( data ) {
			if(data.success){
				allSubscribers = data.data;
				allSubscribers.unshift({id:0, text: "Vsi"});
				$(".select2-subscribers").select2({
					data: allSubscribers,
					search: false,
					placeholder: "Izberi naročnika",
				});
				$(".select2-new-subscribers").select2({
					data: allSubscribers,
					placeholder: "Izberi naročnika",
					tags: true,
				});
			}
			else{
				//open error modal
			}
		});
		//PROJEKTI
		$.get( "/projects/all", function( data ) {
			if(data.success){
				allProjects = data.data;
				for (let project of allProjects) {
					project.text = project.project_number + ' - ' + project.project_name
				}
				let tmpActiveProjects = allProjects.filter(p => p.active == true);
				allProjects.unshift({id:0, text: "Vsi"});
				tmpActiveProjects.unshift({id:0, text: "Vsi"});
				$(".select2-new-projects").select2({
					data: tmpActiveProjects,
					search: false,
				});
				if (loggedUser.role.toLowerCase() != 'admin'){
					$(".select2-projects").select2({
						data: tmpActiveProjects,
						search: false,
						placeholder: "Izberi projekt",
					});
				}
				else{
					$(".select2-projects").select2({
						data: allProjects,
						search: false,
						placeholder: "Izberi projekt",
					});
				}
				/*
				$(".select2-subscribers").select2({
					data: allProjects,
					search: false,
					placeholder: "Izberi naročnika",
				});
				*/
			}
			else{
				//open error modal
			}
		});
		// KONTAKTI
		$.get( "/workorders/contacts", function( data ) {
			if(data.success){
				allContacts = data.contacts;
				allContacts.forEach(c => c.text = c.full_name);
				$(".select2-new-contacts").prepend('<option selected></option>').select2({
					data: allContacts,
					search: true,
					allowClear: true,
					placeholder: "Kontaktna oseba",
					tags: true,
					dropdownParent: $('#modalNewContact')
				});
				$(".select2-edit-contacts").prepend('<option selected></option>').select2({
					data: allContacts,
					search: true,
					allowClear: true,
					placeholder: "Kontaktna oseba",
					tags: true,
					dropdownParent: $('#modalEditContact')
				});
			}
			else{
				//open error modal
			}
		});
		setTimeout(()=>{
			
			$('#report_start').val(moment().format("YYYY-MM-DD"));
			$('#report_finish').val(moment().format("YYYY-MM-DD"));
			$('#woDateNew').val(moment().format("YYYY-MM-DD"));
			getWorkOrders();
			$('#woArrivalNew').on('change', checkNewTime);
			$('#woDepartureNew').on('change', checkNewTime);
			$('#woArrivalEdit').on('change', checkEditTime);
			$('#woDepartureEdit').on('change', checkEditTime);
			$("#openContactModalBtn").on('click', function () {
				$('#modalNewContact').modal('toggle');
			});
			$("#contactEditBtn").on('click', function () {
				let tmp = allContacts.find(c => c.id == tmpWorkOrder.contact_id);
				if (tmp){
					$('#woContactEdit').val(tmp.id).trigger('change');
					$('#woContactPhoneEdit').val(tmp.phone_number);
				}
				else{
					$('#woContactEdit').val(0).trigger('change');
					$('#woContactPhoneEdit').val('');
				}
				$('#modalEditContact').modal('toggle');

			});
		},100);
		//SEND MAIL
		//CONF ADD ACTIVITY - in form for req control
		$('#sendMailForm').submit(function (e) {
			e.preventDefault();
			$form = $(this);
			//debugger;
			// var email = woEmail.value;
			let email;
			var officeCheck = officeCheckbox.checked;
			var workOrderId = savedWorkOrderIdForMail ? savedWorkOrderIdForMail : tmpWorkOrder.id;
			var officeMail = $('#btnSendConf').is( ":visible" );
			let emailsFromSelect = filterEmailSelectInput(); // filter out mails for sending through select input
			// check if email input is empty and notify user if its empty or incorrect
			if (emailsFromSelect.length < 1){
				$('#woEmailSelect').addClass('is-invalid');
			}
			else{
				email = emailsFromSelect;
				// if officeMail is true then user wants to send work order to some other mail but not to office mail
				// send it to office mail anyway because user is lazy and wont send it manually
				if (officeMail){
					// timeout for about 10 seconds and send mail to tanja.muzerlin@roboteh.si mail of new work order
					// later check to check who wants to get email notify of new work order
					// var email = 'tanja.muzerlin@roboteh.si';
					var officeCheck = false;
					var useTimeout = true;
					var timeAmount = 10000;
					//POST info to route for adding new activity
					debugger;
					let data = {workOrderId,email:'tanja.muzerlin@roboteh.si',officeCheck,useTimeout,timeAmount,officeMail:true};
					$.ajax({
						type: 'POST',
						url: '/workorders/mail',
						contentType: 'application/json',
						data: JSON.stringify(data), // access in body
					}).done(function (resp) {
						if(resp.success){
							debugger;
							// $('#woEmail').val('');
							// $('#modalSendMail').modal('toggle');
							console.log('Uspešno poslano elektronsko sporočilo upravi o novem delovnem nalogu.');
						}
						else{
							$('#modalError').modal('toggle');
							console.log('Neuspešno poslano elektronsko sporočilo upravi o novem delovnem nalogu.');
						}
					}).fail(function (resp) {
						//console.log('FAIL');
						//debugger;
						console.log('Neuspešno poslano elektronsko sporočilo upravi o novem delovnem nalogu.');
						$('#modalError').modal('toggle');
					}).always(function (resp) {
						//console.log('ALWAYS');
						//debugger;
					});
					// setTimeout(function () {
					// }, 5000);
				}
				//POST info to route for adding new activity
				debugger;
				let data = {workOrderId,email,officeCheck,notificationMail};
				$.ajax({
					type: 'POST',
					url: '/workorders/mail',
					contentType: 'application/json',
					data: JSON.stringify(data), // access in body
				}).done(function (resp) {
					if(resp.success){
						debugger;
						// $('#woEmail').val('');
						$('#woEmailSelect').val([]).trigger('change');
						$('#woEmailSelect').removeClass('is-invalid');
						$('#modalSendMail').modal('toggle');
					}
					else{
						$('#modalError').modal('toggle');
					}
				}).fail(function (resp) {
					//console.log('FAIL');
					//debugger;
					$('#modalError').modal('toggle');
				}).always(function (resp) {
					//console.log('ALWAYS');
					//debugger;
				});
			}
		})
		//ADD NEW WORK ORDER
		$('#btnAddWorkOrder').click(function () {
			$('#addNewWorkOrderForm').submit();
		});
		$('#addNewWorkOrderForm').submit(function(e){
			e.preventDefault();
			$form = $(this);
			
			//debugger;
			var userId = null;
			if(loggedUser.role == 'admin' || loggedUser.role == 'tajnik' || loggedUser.role == 'komercialist' || loggedUser.role == 'komerciala' || loggedUser.role == 'računovodstvo' || loggedUser.role.substring(0,5) == 'vodja')
				userId = woWorkerNew.value;
			var number = woNumberNew.value;
			var type = woTypeNew.value;
			var date = woDateNew.value;
			var location = woLocationNew.value;
			var locationCordLat = null;
			var locationCordLon = null;
			if (crd){
				locationCordLat = crd.latitude;
				locationCordLon = crd.longitude;
			}
			var subscriberId = woSubscriberNew.value;
			var projectId = woProjectNew.value;
			var description = woDescriptionNew.value;
			var workersId = $('#woWorkersNew').val();
			workersId = workersId.toString();
			var vehicle = woVehicleNew.value;
			var arrival = woArrivalNew.value;
			var departure = woDepartureNew.value;
			var representative = woRepresentativeNew.value;
			//EXPENSES
			var timeQuantity = woTimeQuantityNew.value;
			var timePrice = woTimePriceNew.value;
			var timeSum = woTimeSumNew.value;
			var distanceQuantity = woDistanceQuantityNew.value;
			var distancePrice = woDistancePriceNew.value;
			var distanceSum = woDistanceSumNew.value;
			var materialQuantity = woMaterialQuantityNew.value;
			var materialPrice = woMaterialPriceNew.value;
			var materialSum = woMaterialSumNew.value;
			var otherQuantity = woOtherQuantityNew.value;
			var otherPrice = woOtherPriceNew.value;
			var otherSum = woOtherSumNew.value;
			//MATERIALS
			var name1 = woMaterialName1.value;
			var quantity1 = woMaterialQuantity1.value;
			var price1 = woMaterialPrice1.value;
			var sum1 = woMaterialSum1.value;
			var name2 = woMaterialName2.value;
			var quantity2 = woMaterialQuantity2.value;
			var price2 = woMaterialPrice2.value;
			var sum2 = woMaterialSum2.value;
			var name3 = woMaterialName3.value;
			var quantity3 = woMaterialQuantity3.value;
			var price3 = woMaterialPrice3.value;
			var sum3 = woMaterialSum3.value;
			var name4 = woMaterialName4.value;
			var quantity4 = woMaterialQuantity4.value;
			var price4 = woMaterialPrice4.value;
			var sum4 = woMaterialSum4.value;
			var name5 = woMaterialName5.value;
			var quantity5 = woMaterialQuantity5.value;
			var price5 = woMaterialPrice5.value;
			var sum5 = woMaterialSum5.value;
			var name6 = woMaterialName6.value;
			var quantity6 = woMaterialQuantity6.value;
			var price6 = woMaterialPrice6.value;
			var sum6 = woMaterialSum6.value;
			var name7 = woMaterialName7.value;
			var quantity7 = woMaterialQuantity7.value;
			var price7 = woMaterialPrice7.value;
			var sum7 = woMaterialSum7.value;
			//SIGN
			var signId = tmpSignId;
			//CONTACT
			var contactId = $('#woContactNew').val();
			if(isNaN(subscriberId)){
				//new subscriber, create subscriber then create new work order
				var subscriber = subscriberId;
				$.post('/subscribers/create', {subscriber}, function(respSub){
					if(respSub.success){
						subscriberId = respSub.id.id;
						allSubscribers.push({id:subscriberId,text:subscriber});
						$(".select2-subscribers").select2({
							data: allSubscribers,
							search: false,
							placeholder: "Izberi naročnika",
						});
						$(".select2-new-subscribers").select2({
							data: allSubscribers,
							placeholder: "Izberi naročnika",
							tags: true,
						});
						$.post('/workorders/add', {userId,number,type,date,location,subscriberId,projectId,description,workersId,vehicle,arrival,departure,representative,locationCordLat,locationCordLon,contactId}, function(resp){
							if(resp.success){
								var workOrderId = resp.woId;
								console.log("Uspešno dodajanje novega delovnega naloga.");
								tmpSubscriber = {id:null,text:null};
								tmpProject = {id:null,text:null};
								if(subscriberId != 0){
									tmpSubscriber = allSubscribers.find(s => s.id == subscriberId);
								}
								if(projectId != 0)
									tmpProject = allProjects.find(p => p.id == projectId);
								tmpType = allTypes.find(t => t.id == type);
								//formatted date
								var newDate = date.split('-');
								var formatted_date = {date:newDate[2]+'.'+newDate[1]+'.'+newDate[0],time:'00:00'}
								//formatted time
								var formatted_arrival = {hours:arrival.split(':')[0], minutes:arrival.split(':')[1]};
								var formatted_departure = {hours:departure.split(':')[0], minutes:departure.split(':')[1]};
								//make wo number
								var author = allUsers.find(u=> u.id == woWorkerNew.value).text.split(' ');
								var newWoNumber = author[0][0]+author[1][0]+'-'+workOrderId+'/'+newDate[0];
								//make author boolean
								var isAuthor = loggedUser.name + ' ' + loggedUser.surname == allUsers.find(u => u.id == $('#woWorkerNew').val()).text;
								var tmpStatus = allStatus.find(s => s.id == 1);
								//everything together
								var newWorkOrder = ({
									id: workOrderId,
									wo_number: newWoNumber,
									author: isAuthor,
									subscriber_id: tmpSubscriber.id,
									subscriber_name: tmpSubscriber.text,
									project_id: tmpProject.id,
									project_name: tmpProject.text,
									number: number,
									date: date,
									location: location,
									description: description,
									vehicle: vehicle,
									arrival: arrival,
									departure: departure,
									work_order_type_id: tmpType.id,
									work_order_type: tmpType.text,
									representative: representative,
									user_id: woWorkerNew.value,
									formatted_date: formatted_date,
									formatted_arrival: formatted_arrival,
									formatted_departure: formatted_departure,
									status: tmpStatus.text,
									status_id: tmpStatus.id,
								})
								allWorkOrders.unshift(newWorkOrder);
								var info = '';
								if(tmpSubscriber.id)
									info = tmpSubscriber.text;
								else if(tmpProject.id)
									info = tmpProject.text;
								var woElement = `<div class="card bg-light card-report mb-3" id="workOrder`+workOrderId+`">
									<div class="card-header">
										<div class="alert-dissmisable">
											<div class="close text-dark" id="workOrderDate`+workOrderId+`">`+newWorkOrder.formatted_date.date+`</div>
										</div>
										<div class="row">
											<div class="col-4">
												<div id="workOrderNumber`+workOrderId+`">`+newWoNumber+`</div>
											</div>
											<div class="col-4">
												<div id="workOrderInfo`+workOrderId+`">`+info+`</div>
											</div>
											<div class="col-4">
												<div id="workOrderStatus`+workOrderId+`">`+tmpStatus.text+`</div>
											</div>
										</div>
										<div class="row">
											<span class="d-inline-block text-truncate text-muted mx-2 mb-n2" id="workOrderDescription`+workOrderId+`">`+newWorkOrder.description+`</span>
										</div>
									</div>
								</div>`;
								$('#workOrderBody').prepend(woElement);
								$('#workOrderNewCollapse').collapse('toggle');
								savedWorkOrderIdForMail = workOrderId;
								if (($('#woDateNew').val() == '' || $('#woDescriptionNew').val() == '' || $('#woArrivalNew').val() == '' || $('#woDepartureNew').val() == '') && (loggedUser.role == 'admin' || loggedUser.role.substring(0,5) == 'vodja')) {
									// some information is missing on work order and is unfinished, make sending notification mail possible
									notificationMail = true;
									$('#autoMailColaps').collapse('hide');
									$('#modalMailHeader').text('Vnesite elek. naslov, kamor želite poslati obvestilo o napotitvi storitve');
								}
								else{
									// normal work order -> send automatic mail to office
									notificationMail = false;
									$('#btnSendConf').hide();
									$('#autoMailNotify').text('Nov delovni nalog se je avtomatsko poslal na elektronski naslov:');
									$('#autoMailColaps').collapse('show');
									$('#modalMailHeader').text('Vnesite elek. naslov, kamor želite poslati kopijo delovnega naloga');
								}
								$('#modalSendMail').modal('toggle');
								if(workersId){
									var addWorkers = workersId;
									$.post('/workorders/workers', {workOrderId,addWorkers}, function(respWorkers){
										if(respWorkers.success)
											console.log('Uspešno dodajanje delavcev na delovni nalog.');
										else{
											console.log('Neuspešno dodajanje delavcev na delovni nalog!');
											//open error modal
										}
										if(timeQuantity||timePrice||timeSum||distanceQuantity||distancePrice||distanceSum||materialQuantity||materialPrice||materialSum||otherQuantity||otherPrice||otherSum){
											$.post('/workorders/expenses/add', {workOrderId,timeQuantity,timePrice,timeSum,distanceQuantity,distancePrice,distanceSum,materialQuantity,materialPrice,materialSum,otherQuantity,otherPrice,otherSum}, function(respExpenses){
												if(respExpenses.success)
													console.log('Uspešno dodajanje stroškov na delovni nalog.');
												else{
													console.log('Neuspešno dodajanje stroškov na delovni nalog!');
													//open error modal
												}
											})
										}
										if(name1||quantity1||price1||sum1||name2||quantity2||price2||sum2||name3||quantity3||price3||sum3||name4||quantity4||price4||sum4||name5||quantity5||price5||sum5||name6||quantity6||price6||sum6||name7||quantity7||price7||sum7){
											$.post('/workorders/materials/add', {workOrderId,name1,quantity1,price1,sum1,name2,quantity2,price2,sum2,name3,quantity3,price3,sum3,name4,quantity4,price4,sum4,name5,quantity5,price5,sum5,name6,quantity6,price6,sum6,name7,quantity7,price7,sum7}, function(respMaterials){
												if(respMaterials.success)
													console.log('Uspešno dodajanje materialov na delovni nalog.');
												else{
													console.log('Neuspešno dodajanje materialov na delovni nalog!');
													//open error modal
												}
											})
										}
										if(signId){
											$.post('/workorders/signs/attach', {workOrderId,signId}, function(respSigns){
												if (respSigns.success){
													console.log('Uspešno dodajanje podpisa na delovni nalog.');
													signaturePad.clear();
													tmpSignId = undefined;
												}
												else{
													console.log('Neuspešno dodajanje podpisa na delovni nalog!');
													//open error modal
												}
											})
											$('#openSignModalBtn').removeClass('btn-primary btn-danger btn-success').addClass('btn-primary');
      								$('#openSignModalBtn').find('i').removeClass('invisible fa-check fa-times').addClass('invisible');
										}
									})
								}
								//no else, workers are required
								//ADD SYSTEM CHANGE FOR EDITING WORK ORDER
								//addSystemChange(18,1,workOrderId);
								addChangeSystem(1,18,null,null,null,null,null,null,null,null,null,workOrderId);
								addChangeSystem(1,8,null,null,null,null,null,null,tmpSubscriber.id);
								
								// empty input values
								$('#addNewWorkOrderForm').find('input').val('');
								$('#woDateNew').val(moment().format("YYYY-MM-DD"));
								$('#woTypeNew').val(1).trigger('change');
								$('#woSubscriberNew').val(0).trigger('change');
								$('#woProjectNew').val(0).trigger('change');
								$('#woWorkersNew').val('').trigger('change');
								$('#woDescriptionNew').val('');

								if (!notificationMail){
									// timeout for about 10 seconds and send mail to tanja.muzerlin@roboteh.si mail of new work order
									// later check to check who wants to get email notify of new work order
									var email = 'tanja.muzerlin@roboteh.si';
									var officeCheck = false;
									var useTimeout = true;
									var timeAmount = 10000;
									//POST info to route for adding new activity
									debugger;
									let data = {workOrderId,email,officeCheck,useTimeout,timeAmount,signId,officeMail:true};
									$.ajax({
										type: 'POST',
										url: '/workorders/mail',
										contentType: 'application/json',
										data: JSON.stringify(data), // access in body
									}).done(function (resp) {
										if(resp.success){
											debugger;
											// $('#woEmail').val('');
											// $('#modalSendMail').modal('toggle');
											console.log('Uspešno poslano elektronsko sporočilo upravi o novem delovnem nalogu.');
										}
										else{
											$('#modalError').modal('toggle');
											console.log('Neuspešno poslano elektronsko sporočilo upravi o novem delovnem nalogu.');
										}
									}).fail(function (resp) {
										//console.log('FAIL');
										//debugger;
										console.log('Neuspešno poslano elektronsko sporočilo upravi o novem delovnem nalogu.');
										$('#modalError').modal('toggle');
									}).always(function (resp) {
										//console.log('ALWAYS');
										//debugger;
									});
									// setTimeout(function () {
									// }, 5000);
								}
							}
							else{
								console.log("Pri dodajanju novega delovnega naloga je prišlo do napake.");
								//open error modal
							}
						})
					}
					else{
						//open error modal
					}
				})
			}
			else{
				//no new subscriber, create new work order
				$.post('/workorders/add', {userId,number,type,date,location,subscriberId,projectId,description,workersId,vehicle,arrival,departure,representative,locationCordLat,locationCordLon,contactId}, function(resp){
					if(resp.success){
						var workOrderId = resp.woId;
						console.log("Uspešno dodajanje novega delovnega naloga.");
						tmpSubscriber = {id:null,text:null};
						tmpProject = {id:null,text:null};
						if(subscriberId != 0){
							tmpSubscriber = allSubscribers.find(s => s.id == subscriberId);
						}
						if(projectId != 0)
							tmpProject = allProjects.find(p => p.id == projectId);
						tmpType = allTypes.find(t => t.id == type);
						//formatted date
						var newDate = date.split('-');
						var formatted_date = {date:newDate[2]+'.'+newDate[1]+'.'+newDate[0],time:'00:00'}
						//formatted time
						var formatted_arrival = {hours:arrival.split(':')[0], minutes:arrival.split(':')[1]};
						var formatted_departure = {hours:departure.split(':')[0], minutes:departure.split(':')[1]};
						//make wo number
						var author = allUsers.find(u=> u.id == woWorkerNew.value).text.split(' ');
						var newWoNumber = author[0][0]+author[1][0]+'-'+workOrderId+'/'+newDate[0];
						//make author boolean
						var isAuthor = loggedUser.name + ' ' + loggedUser.surname == allUsers.find(u => u.id == $('#woWorkerNew').val()).text;
						var tmpStatus = allStatus.find(s => s.id == 1);
						//everything together
						var newWorkOrder = ({
							id: workOrderId,
							wo_number: newWoNumber,
							author: isAuthor,
							subscriber_id: tmpSubscriber.id,
							subscriber_name: tmpSubscriber.text,
							project_id: tmpProject.id,
							project_name: tmpProject.text,
							number: number,
							date: date,
							location: location,
							description: description,
							vehicle: vehicle,
							arrival: arrival,
							departure: departure,
							work_order_type_id: tmpType.id,
							work_order_type: tmpType.text,
							representative: representative,
							user_id: woWorkerNew.value,
							formatted_date: formatted_date,
							formatted_arrival: formatted_arrival,
							formatted_departure: formatted_departure,
							status: tmpStatus.text,
							status_id: tmpStatus.id,
						})
						allWorkOrders.unshift(newWorkOrder);
						var info = '';
						if(tmpSubscriber.id)
							info = tmpSubscriber.text;
						else if(tmpProject.id)
							info = tmpProject.text;
						var woElement = `<div class="card bg-light card-report mb-3" id="workOrder`+workOrderId+`">
							<div class="card-header">
								<div class="alert-dissmisable">
									<div class="close text-dark" id="workOrderDate`+workOrderId+`">`+newWorkOrder.formatted_date.date+`</div>
								</div>
								<div class="row">
									<div class="col-4">
										<div id="workOrderNumber`+workOrderId+`">`+newWoNumber+`</div>
									</div>
									<div class="col-4">
										<div id="workOrderInfo`+workOrderId+`">`+info+`</div>
									</div>
									<div class="col-4">
										<div id="workOrderStatus`+workOrderId+`">`+tmpStatus.text+`</div>
									</div>
								</div>
								<div class="row">
									<span class="d-inline-block text-truncate text-muted mx-2 mb-n2" id="workOrderDescription`+workOrderId+`">`+newWorkOrder.description+`</span>
								</div>
							</div>
						</div>`;
						$('#workOrderBody').prepend(woElement);
						$('#workOrderNewCollapse').collapse('toggle');
						savedWorkOrderIdForMail = workOrderId;
						if (($('#woDateNew').val() == '' || $('#woDescriptionNew').val() == '' || $('#woArrivalNew').val() == '' || $('#woDepartureNew').val() == '') && (loggedUser.role == 'admin' || loggedUser.role.substring(0,5) == 'vodja')) {
							// some information is missing on work order and is unfinished, make sending notification mail possible
							notificationMail = true;
							$('#autoMailColaps').collapse('hide');
							$('#modalMailHeader').text('Vnesite elek. naslov, kamor želite poslati obvestilo o napotitvi storitve');
						}
						else{
							// normal work order -> send automatic mail to office
							notificationMail = false;
							$('#btnSendConf').hide();
							$('#autoMailNotify').text('Nov delovni nalog se je avtomatsko poslal na elektronski naslov:');
							$('#autoMailColaps').collapse('show');
							$('#modalMailHeader').text('Vnesite elek. naslov, kamor želite poslati kopijo delovnega naloga');
						}
						$('#modalSendMail').modal('toggle');
						if(workersId){
							var addWorkers = workersId;
							$.post('/workorders/workers', {workOrderId,addWorkers}, function(respWorkers){
								if(respWorkers.success)
									console.log('Uspešno dodajanje delavcev na delovni nalog.');
								else{
									console.log('Neuspešno dodajanje delavcev na delovni nalog!');
									//open error modal
								}
								if(timeQuantity||timePrice||timeSum||distanceQuantity||distancePrice||distanceSum||materialQuantity||materialPrice||materialSum||otherQuantity||otherPrice||otherSum){
									$.post('/workorders/expenses/add', {workOrderId,timeQuantity,timePrice,timeSum,distanceQuantity,distancePrice,distanceSum,materialQuantity,materialPrice,materialSum,otherQuantity,otherPrice,otherSum}, function(respExpenses){
										if(respExpenses.success)
											console.log('Uspešno dodajanje stroškov na delovni nalog.');
										else{
											console.log('Neuspešno dodajanje stroškov na delovni nalog!');
											//open error modal
										}
									})
								}
								if(name1||quantity1||price1||sum1||name2||quantity2||price2||sum2||name3||quantity3||price3||sum3||name4||quantity4||price4||sum4||name5||quantity5||price5||sum5||name6||quantity6||price6||sum6||name7||quantity7||price7||sum7){
									$.post('/workorders/materials/add', {workOrderId,name1,quantity1,price1,sum1,name2,quantity2,price2,sum2,name3,quantity3,price3,sum3,name4,quantity4,price4,sum4,name5,quantity5,price5,sum5,name6,quantity6,price6,sum6,name7,quantity7,price7,sum7}, function(respMaterials){
										if(respMaterials.success)
											console.log('Uspešno dodajanje materialov na delovni nalog.');
										else{
											console.log('Neuspešno dodajanje materialov na delovni nalog!');
											//open error modal
										}
									})
								}
								if(signId){
									$.post('/workorders/signs/attach', {workOrderId,signId}, function(respSigns){
										if (respSigns.success){
											console.log('Uspešno dodajanje podpisa na delovni nalog.');
											signaturePad.clear();
											tmpSignId = undefined;
										}
										else{
											console.log('Neuspešno dodajanje podpisa na delovni nalog!');
											//open error modal
										}
									})
									$('#openSignModalBtn').removeClass('btn-primary btn-danger btn-success').addClass('btn-primary');
									$('#openSignModalBtn').find('i').removeClass('invisible fa-check fa-times').addClass('invisible');
								}
							})
						}
						//ADD SYSTEM CHANGE FOR EDITING WORK ORDER
						//addSystemChange(18,1,workOrderId);
						addChangeSystem(1,18,null,null,null,null,null,null,null,null,null,workOrderId);
						// empty input values
						$('#addNewWorkOrderForm').find('input').val('');
						$('#woDateNew').val(moment().format("YYYY-MM-DD"));
						$('#woTypeNew').val(1).trigger('change');
						$('#woSubscriberNew').val(0).trigger('change');
						$('#woProjectNew').val(0).trigger('change');
						$('#woWorkersNew').val('').trigger('change');
						$('#woDescriptionNew').val('');

						if (!notificationMail){
							// timeout for about 10 seconds and send mail to tanja.muzerlin@roboteh.si mail of new work order
							// later check to check who wants to get email notify of new work order
							var email = 'tanja.muzerlin@roboteh.si';
							var officeCheck = false;
							var useTimeout = true;
							var timeAmount = 10000;
							//POST info to route for adding new activity
							debugger;
							let data = {workOrderId,email,officeCheck,useTimeout,timeAmount,signId,officeMail:true};
							$.ajax({
								type: 'POST',
								url: '/workorders/mail',
								contentType: 'application/json',
								data: JSON.stringify(data), // access in body
							}).done(function (resp) {
								if(resp.success){
									debugger;
									// $('#woEmail').val('');
									// $('#modalSendMail').modal('toggle');
									console.log('Uspešno poslano elektronsko sporočilo upravi o novem delovnem nalogu.');
								}
								else{
									$('#modalError').modal('toggle');
									console.log('Neuspešno poslano elektronsko sporočilo upravi o novem delovnem nalogu.');
								}
							}).fail(function (resp) {
								//console.log('FAIL');
								//debugger;
								console.log('Neuspešno poslano elektronsko sporočilo upravi o novem delovnem nalogu.');
								$('#modalError').modal('toggle');
							}).always(function (resp) {
								//console.log('ALWAYS');
								//debugger;
							});
							// setTimeout(function () {
							// }, 5000);
						}
					}
					else{
						console.log("Pri dodajanju novega delovnega naloga je prišlo do napake.");
						//open error modal
					}
				})
			}
		})
		//EDIT WORK ORDER
		$('#editWorkOrderForm').submit(function(e){
			e.preventDefault();
			$form = $(this);
			
			//debugger;
			var userId = null;
			if(loggedUser.role == 'admin' || loggedUser.role == 'tajnik' || loggedUser.role == 'komercialist' || loggedUser.role == 'komerciala' || loggedUser.role == 'računovodstvo' || loggedUser.role.substring(0,5) == 'vodja')
				userId = woWorkerEdit.value;
			var number = woNumberEdit.value;
			var type = woTypeEdit.value;
			var date = woDateEdit.value;
			var location = woLocationEdit.value;
			var subscriberId = woSubscriberEdit.value;
			var projectId = woProjectEdit.value;
			var description = woDescriptionEdit.value;
			var workersId = $('#woWorkersEdit').val();
			workersId = workersId.toString();
			var vehicle = woVehicleEdit.value;
			var arrival = woArrivalEdit.value;
			var departure = woDepartureEdit.value;
			var representative = woRepresentativeEdit.value;
			//EXPENSES
			var timeQuantity = woTimeQuantityEdit.value;
			var timePrice = woTimePriceEdit.value;
			var timeSum = woTimeSumEdit.value;
			var distanceQuantity = woDistanceQuantityEdit.value;
			var distancePrice = woDistancePriceEdit.value;
			var distanceSum = woDistanceSumEdit.value;
			var materialQuantity = woMaterialQuantityEdit.value;
			var materialPrice = woMaterialPriceEdit.value;
			var materialSum = woMaterialSumEdit.value;
			var otherQuantity = woOtherQuantityEdit.value;
			var otherPrice = woOtherPriceEdit.value;
			var otherSum = woOtherSumEdit.value;
			//MATERIALS
			var name1 = woMaterialNameEdit1.value;
			var quantity1 = woMaterialQuantityEdit1.value;
			var price1 = woMaterialPriceEdit1.value;
			var sum1 = woMaterialSumEdit1.value;
			var name2 = woMaterialNameEdit2.value;
			var quantity2 = woMaterialQuantityEdit2.value;
			var price2 = woMaterialPriceEdit2.value;
			var sum2 = woMaterialSumEdit2.value;
			var name3 = woMaterialNameEdit3.value;
			var quantity3 = woMaterialQuantityEdit3.value;
			var price3 = woMaterialPriceEdit3.value;
			var sum3 = woMaterialSumEdit3.value;
			var name4 = woMaterialNameEdit4.value;
			var quantity4 = woMaterialQuantityEdit4.value;
			var price4 = woMaterialPriceEdit4.value;
			var sum4 = woMaterialSumEdit4.value;
			var name5 = woMaterialNameEdit5.value;
			var quantity5 = woMaterialQuantityEdit5.value;
			var price5 = woMaterialPriceEdit5.value;
			var sum5 = woMaterialSumEdit5.value;
			var name6 = woMaterialNameEdit6.value;
			var quantity6 = woMaterialQuantityEdit6.value;
			var price6 = woMaterialPriceEdit6.value
			var sum6 = woMaterialSumEdit6.value;
			var name7 = woMaterialNameEdit7.value;
			var quantity7 = woMaterialQuantityEdit7.value;
			var price7 = woMaterialPriceEdit7.value;
			var sum7 = woMaterialSumEdit7.value;
			//SIGN
			var signId = tmpEditSignId;
			//CONTACT
			var contactId = (loggedUser.role == 'admin' || loggedUser.role == 'tajnik' || loggedUser.role == 'komercialist' || loggedUser.role == 'komerciala' || loggedUser.role == 'računovodstvo' || loggedUser.role.substring(0,5) == 'vodja') ? $('#woContactEdit').val() : tmpWorkOrder.contact_id;

			if(isNaN(subscriberId)){
				//new subscriber, create subscriber first then update work order
				var subscriber = subscriberId;
				$.post('/subscribers/create', {subscriber}, function(respSub){
					if(respSub.success){
						subscriberId = respSub.id.id;
						allSubscribers.push({id:subscriberId,text:subscriber});
						$(".select2-subscribers").select2({
							data: allSubscribers,
							search: false,
							placeholder: "Izberi naročnika",
						});
						$(".select2-new-subscribers").select2({
							data: allSubscribers,
							placeholder: "Izberi naročnika",
							tags: true,
						});
						$.post('/workorders/edit', {workOrderId,userId,number,type,date,location,subscriberId,projectId,description,workersId,vehicle,arrival,departure,representative, contactId}, function(resp){
							if(resp.success){
								console.log("Uspešno posodobitev delovnega naloga.");
								tmpSubscriber = {id:null,text:null};
								tmpProject = {id:null,text:null};
								if(subscriberId != 0){
									tmpSubscriber = allSubscribers.find(s => s.id == subscriberId);
								}
								if(projectId != 0)
									tmpProject = allProjects.find(p => p.id == projectId);
								tmpType = allTypes.find(t => t.id == type);
								//formatted date
								var newDate = date.split('-');
								var formatted_date = {date:newDate[2]+'.'+newDate[1]+'.'+newDate[0],time:'00:00'}
								//formatted time
								var formatted_arrival = {hours:arrival.split(':')[0], minutes:arrival.split(':')[1]};
								var formatted_departure = {hours:departure.split(':')[0], minutes:departure.split(':')[1]};
								//make wo number
								var author = allUsers.find(u=> u.id == woWorkerEdit.value).text.split(' ');
								var newWoNumber = author[0][0]+author[1][0]+'-'+workOrderId+'/'+newDate[0];
								//everything together
								tmpWorkOrder = allWorkOrders.find(wo => wo.id == workOrderId);
								tmpWorkOrder.subscriber_id = tmpSubscriber.id;
								tmpWorkOrder.wo_number = newWoNumber;
								tmpWorkOrder.user_id = woWorkerEdit.value;
								tmpWorkOrder.subscriber_name = tmpSubscriber.text;
								tmpWorkOrder.project_id = tmpProject.id;
								tmpWorkOrder.project_name = tmpProject.text;
								tmpWorkOrder.number = number;
								tmpWorkOrder.date = date;
								tmpWorkOrder.location = location;
								tmpWorkOrder.description = description;
								tmpWorkOrder.vehicle = vehicle;
								tmpWorkOrder.arrival = arrival;
								tmpWorkOrder.departure = departure;
								tmpWorkOrder.work_order_type_id = tmpType.id;
								tmpWorkOrder.work_order_type = tmpType.text;
								tmpWorkOrder.formatted_date = formatted_date;
								tmpWorkOrder.formatted_arrival = formatted_arrival;
								tmpWorkOrder.formatted_departure = formatted_departure;
								tmpWorkOrder.contact_id = contactId;
								tmpWorkOrder.representative = representative;
								
								if(workersId){
									editWorkers = $('#woWorkersEdit').val();
									var addWorkers = [];
									var deleteWorkers = [];
									for(var i = 0; i < workOrderWorkers.length; i++){
										tmpWorker = editWorkers.find(w => w == workOrderWorkers[i].worker_id);
										if(!tmpWorker)
											deleteWorkers.push(workOrderWorkers[i].id)
									}
									for(var i = 0; i < editWorkers.length; i++){
										tmpWorker = workOrderWorkers.find(wow => wow.worker_id == editWorkers[i])
										if(!tmpWorker)
											addWorkers.push(parseInt(editWorkers[i]));
									}
									//console.log("Novi delavci: ");
									//console.log(addWorkers);
									//console.log("Izbrisani zapisi delavcev: ");
									//console.log(deleteWorkers);
									if(addWorkers.length > 0 || deleteWorkers.length > 0){
										addWorkers = addWorkers.toString();
										deleteWorkers = deleteWorkers.toString();
										$.post('/workorders/workers', {workOrderId,addWorkers,deleteWorkers}, function(respWorkers){
											if(respWorkers.success){
												console.log('Uspešno posodabljanje delavcev na delovni nalog.');
												//update or create expenses
												if(workOrderExpenses){
													if(timeQuantity||timePrice||timeSum||distanceQuantity||distancePrice||distanceSum||materialQuantity||materialPrice||materialSum||otherQuantity||otherPrice||otherSum){
														$.post('/workorders/expenses/update', {workOrderId,timeQuantity,timePrice,timeSum,distanceQuantity,distancePrice,distanceSum,materialQuantity,materialPrice,materialSum,otherQuantity,otherPrice,otherSum}, function(respExpenses){
															if(respExpenses.success){
																console.log('Uspešno dodajanje stroškov na delovni nalog.');
																//update or create materials
																if(workOrderMaterials){//update
																	if(name1||quantity1||price1||sum1||name2||quantity2||price2||sum2||name3||quantity3||price3||sum3||name4||quantity4||price4||sum4||name5||quantity5||price5||sum5||name6||quantity6||price6||sum6||name7||quantity7||price7||sum7){
																		$.post('/workorders/materials/update', {workOrderId,name1,quantity1,price1,sum1,name2,quantity2,price2,sum2,name3,quantity3,price3,sum3,name4,quantity4,price4,sum4,name5,quantity5,price5,sum5,name6,quantity6,price6,sum6,name7,quantity7,price7,sum7}, function(respMaterials){
																			if(respMaterials.success)
																				console.log('Uspesno dodajanje materialov na delovni nalog.1');
																			else{
																				console.log('Neuspesno dodajanje materialov na delovni nalog.1');
																				//open error modal
																			}
																			closeEditOnSuccess();
																		})
																	}
																	else
																		closeEditOnSuccess();
																}
																else{//create
																	if(name1||quantity1||price1||sum1||name2||quantity2||price2||sum2||name3||quantity3||price3||sum3||name4||quantity4||price4||sum4||name5||quantity5||price5||sum5||name6||quantity6||price6||sum6||name7||quantity7||price7||sum7){
																		$.post('/workorders/materials/add', {workOrderId,name1,quantity1,price1,sum1,name2,quantity2,price2,sum2,name3,quantity3,price3,sum3,name4,quantity4,price4,sum4,name5,quantity5,price5,sum5,name6,quantity6,price6,sum6,name7,quantity7,price7,sum7}, function(respMaterials){
																			if(respMaterials.success)
																				console.log('Uspesno dodajanje materialov na delovni nalog.2');
																			else{
																				console.log('Neuspesno dodajanje materialov na delovni nalog.2');
																				//open error modal
																			}
																			closeEditOnSuccess();
																		})
																	}
																	else
																		closeEditOnSuccess();
																}
															}
															else{
																console.log('Neuspešno dodajanje stroškov na delovni nalog!');
																closeEditOnSuccess();
																//open error modal
															}
														})
													}
													else{
														//no expenses, update/create materials if any
														//update or create materials
														if(workOrderMaterials){//update
															if(name1||quantity1||price1||sum1||name2||quantity2||price2||sum2||name3||quantity3||price3||sum3||name4||quantity4||price4||sum4||name5||quantity5||price5||sum5||name6||quantity6||price6||sum6||name7||quantity7||price7||sum7){
																$.post('/workorders/materials/update', {workOrderId,name1,quantity1,price1,sum1,name2,quantity2,price2,sum2,name3,quantity3,price3,sum3,name4,quantity4,price4,sum4,name5,quantity5,price5,sum5,name6,quantity6,price6,sum6,name7,quantity7,price7,sum7}, function(respMaterials){
																	if(respMaterials.success)
																		console.log('Uspesno dodajanje materialov na delovni nalog.1');
																	else{
																		console.log('Neuspesno dodajanje materialov na delovni nalog.1');
																		//open error modal
																	}
																	closeEditOnSuccess();
																})
															}
															else
																closeEditOnSuccess();
														}
														else{//create
															if(name1||quantity1||price1||sum1||name2||quantity2||price2||sum2||name3||quantity3||price3||sum3||name4||quantity4||price4||sum4||name5||quantity5||price5||sum5||name6||quantity6||price6||sum6||name7||quantity7||price7||sum7){
																$.post('/workorders/materials/add', {workOrderId,name1,quantity1,price1,sum1,name2,quantity2,price2,sum2,name3,quantity3,price3,sum3,name4,quantity4,price4,sum4,name5,quantity5,price5,sum5,name6,quantity6,price6,sum6,name7,quantity7,price7,sum7}, function(respMaterials){
																	if(respMaterials.success)
																		console.log('Uspesno dodajanje materialov na delovni nalog.2');
																	else{
																		console.log('Neuspesno dodajanje materialov na delovni nalog.2');
																		//open error modal
																	}
																	closeEditOnSuccess();
																})
															}
															else
																closeEditOnSuccess();
														}
														//closeEditOnSuccess();
													}
												}
												else{
													if(timeQuantity||timePrice||timeSum||distanceQuantity||distancePrice||distanceSum||materialQuantity||materialPrice||materialSum||otherQuantity||otherPrice||otherSum){
														$.post('/workorders/expenses/add', {workOrderId,timeQuantity,timePrice,timeSum,distanceQuantity,distancePrice,distanceSum,materialQuantity,materialPrice,materialSum,otherQuantity,otherPrice,otherSum}, function(respExpenses){
															if(respExpenses.success){
																console.log('Uspešno dodajanje stroškov na delovni nalog.');
																//update or create materials
																if(workOrderMaterials){
																	if(name1||quantity1||price1||sum1||name2||quantity2||price2||sum2||name3||quantity3||price3||sum3||name4||quantity4||price4||sum4||name5||quantity5||price5||sum5||name6||quantity6||price6||sum6||name7||quantity7||price7||sum7){
																		$.post('/workorders/materials/update', {workOrderId,name1,quantity1,price1,sum1,name2,quantity2,price2,sum2,name3,quantity3,price3,sum3,name4,quantity4,price4,sum4,name5,quantity5,price5,sum5,name6,quantity6,price6,sum6,name7,quantity7,price7,sum7}, function(respMaterials){
																			if(respMaterials.success)
																				console.log('Uspesno dodajanje materialov na delovni nalog.3');
																			else{
																				console.log('Neuspesno dodajanje materialov na delovni nalog.3');
																				//open error modal
																			}
																			closeEditOnSuccess();
																		})
																	}
																}
																else{
																	if(name1||quantity1||price1||sum1||name2||quantity2||price2||sum2||name3||quantity3||price3||sum3||name4||quantity4||price4||sum4||name5||quantity5||price5||sum5||name6||quantity6||price6||sum6||name7||quantity7||price7||sum7){
																		$.post('/workorders/materials/add', {workOrderId,name1,quantity1,price1,sum1,name2,quantity2,price2,sum2,name3,quantity3,price3,sum3,name4,quantity4,price4,sum4,name5,quantity5,price5,sum5,name6,quantity6,price6,sum6,name7,quantity7,price7,sum7}, function(respMaterials){
																			if(respMaterials.success)
																				console.log('Uspesno dodajanje materialov na delovni nalog.4');
																			else{
																				console.log('Neuspesno dodajanje materialov na delovni nalog.4');
																				//open error modal
																			}
																			closeEditOnSuccess();
																		})
																	}
																}
															}
															else{
																console.log('Neuspešno dodajanje stroškov na delovni nalog!');
																closeEditOnSuccess();
																//open error modal
															}
														})
													}
													else{
														//update or create materials
														if(workOrderMaterials){//update
															if(name1||quantity1||price1||sum1||name2||quantity2||price2||sum2||name3||quantity3||price3||sum3||name4||quantity4||price4||sum4||name5||quantity5||price5||sum5||name6||quantity6||price6||sum6||name7||quantity7||price7||sum7){
																$.post('/workorders/materials/update', {workOrderId,name1,quantity1,price1,sum1,name2,quantity2,price2,sum2,name3,quantity3,price3,sum3,name4,quantity4,price4,sum4,name5,quantity5,price5,sum5,name6,quantity6,price6,sum6,name7,quantity7,price7,sum7}, function(respMaterials){
																	if(respMaterials.success)
																		console.log('Uspesno dodajanje materialov na delovni nalog.1');
																	else{
																		console.log('Neuspesno dodajanje materialov na delovni nalog.1');
																		//open error modal
																	}
																	closeEditOnSuccess();
																})
															}
															else
																closeEditOnSuccess();
														}
														else{//create
															if(name1||quantity1||price1||sum1||name2||quantity2||price2||sum2||name3||quantity3||price3||sum3||name4||quantity4||price4||sum4||name5||quantity5||price5||sum5||name6||quantity6||price6||sum6||name7||quantity7||price7||sum7){
																$.post('/workorders/materials/add', {workOrderId,name1,quantity1,price1,sum1,name2,quantity2,price2,sum2,name3,quantity3,price3,sum3,name4,quantity4,price4,sum4,name5,quantity5,price5,sum5,name6,quantity6,price6,sum6,name7,quantity7,price7,sum7}, function(respMaterials){
																	if(respMaterials.success)
																		console.log('Uspesno dodajanje materialov na delovni nalog.2');
																	else{
																		console.log('Neuspesno dodajanje materialov na delovni nalog.2');
																		//open error modal
																	}
																	closeEditOnSuccess();
																})
															}
															else
																closeEditOnSuccess();
														}
														//closeEditOnSuccess();
													}
												}
												//closeEditOnSuccess();
											}
											else{
												console.log('Neuspešno posodabljane delavcev na delovni nalog!');
												//open error modal
											}
										})
									}
									else{
										//update or create expenses
										if(workOrderExpenses){
											if(timeQuantity||timePrice||timeSum||distanceQuantity||distancePrice||distanceSum||materialQuantity||materialPrice||materialSum||otherQuantity||otherPrice||otherSum){
												$.post('/workorders/expenses/update', {workOrderId,timeQuantity,timePrice,timeSum,distanceQuantity,distancePrice,distanceSum,materialQuantity,materialPrice,materialSum,otherQuantity,otherPrice,otherSum}, function(respExpenses){
													if(respExpenses.success){
														console.log('Uspešno dodajanje stroškov na delovni nalog.');
														//update or create materials
														if(workOrderMaterials){
															if(name1||quantity1||price1||sum1||name2||quantity2||price2||sum2||name3||quantity3||price3||sum3||name4||quantity4||price4||sum4||name5||quantity5||price5||sum5||name6||quantity6||price6||sum6||name7||quantity7||price7||sum7){
																$.post('/workorders/materials/update', {workOrderId,name1,quantity1,price1,sum1,name2,quantity2,price2,sum2,name3,quantity3,price3,sum3,name4,quantity4,price4,sum4,name5,quantity5,price5,sum5,name6,quantity6,price6,sum6,name7,quantity7,price7,sum7}, function(respMaterials){
																	if(respMaterials.success)
																		console.log('Uspesno dodajanje materialov na delovni nalog.5');
																	else{
																		console.log('Neuspesno dodajanje materialov na delovni nalog.5');
																		//open error modal
																	}
																	closeEditOnSuccess();
																})
															}
														}
														else{
															if(name1||quantity1||price1||sum1||name2||quantity2||price2||sum2||name3||quantity3||price3||sum3||name4||quantity4||price4||sum4||name5||quantity5||price5||sum5||name6||quantity6||price6||sum6||name7||quantity7||price7||sum7){
																$.post('/workorders/materials/add', {workOrderId,name1,quantity1,price1,sum1,name2,quantity2,price2,sum2,name3,quantity3,price3,sum3,name4,quantity4,price4,sum4,name5,quantity5,price5,sum5,name6,quantity6,price6,sum6,name7,quantity7,price7,sum7}, function(respMaterials){
																	if(respMaterials.success)
																		console.log('Uspesno dodajanje materialov na delovni nalog.6');
																	else{
																		console.log('Neuspesno dodajanje materialov na delovni nalog.6');
																		//open error modal
																	}
																	closeEditOnSuccess();
																})
															}
														}
													}
													else{
														console.log('Neuspešno dodajanje stroškov na delovni nalog!');
														closeEditOnSuccess();
														//open error modal
													}
												})
											}
											else{
												//update or create materials
												if(workOrderMaterials){//update
													if(name1||quantity1||price1||sum1||name2||quantity2||price2||sum2||name3||quantity3||price3||sum3||name4||quantity4||price4||sum4||name5||quantity5||price5||sum5||name6||quantity6||price6||sum6||name7||quantity7||price7||sum7){
														$.post('/workorders/materials/update', {workOrderId,name1,quantity1,price1,sum1,name2,quantity2,price2,sum2,name3,quantity3,price3,sum3,name4,quantity4,price4,sum4,name5,quantity5,price5,sum5,name6,quantity6,price6,sum6,name7,quantity7,price7,sum7}, function(respMaterials){
															if(respMaterials.success)
																console.log('Uspesno dodajanje materialov na delovni nalog.1');
															else{
																console.log('Neuspesno dodajanje materialov na delovni nalog.1');
																//open error modal
															}
															closeEditOnSuccess();
														})
													}
													else
														closeEditOnSuccess();
												}
												else{//create
													if(name1||quantity1||price1||sum1||name2||quantity2||price2||sum2||name3||quantity3||price3||sum3||name4||quantity4||price4||sum4||name5||quantity5||price5||sum5||name6||quantity6||price6||sum6||name7||quantity7||price7||sum7){
														$.post('/workorders/materials/add', {workOrderId,name1,quantity1,price1,sum1,name2,quantity2,price2,sum2,name3,quantity3,price3,sum3,name4,quantity4,price4,sum4,name5,quantity5,price5,sum5,name6,quantity6,price6,sum6,name7,quantity7,price7,sum7}, function(respMaterials){
															if(respMaterials.success)
																console.log('Uspesno dodajanje materialov na delovni nalog.2');
															else{
																console.log('Neuspesno dodajanje materialov na delovni nalog.2');
																//open error modal
															}
															closeEditOnSuccess();
														})
													}
													else
														closeEditOnSuccess();
												}
												//closeEditOnSuccess();
											}
										}
										else{
											if(timeQuantity||timePrice||timeSum||distanceQuantity||distancePrice||distanceSum||materialQuantity||materialPrice||materialSum||otherQuantity||otherPrice||otherSum){
												$.post('/workorders/expenses/add', {workOrderId,timeQuantity,timePrice,timeSum,distanceQuantity,distancePrice,distanceSum,materialQuantity,materialPrice,materialSum,otherQuantity,otherPrice,otherSum}, function(respExpenses){
													if(respExpenses.success){
														console.log('Uspešno dodajanje stroškov na delovni nalog.');
														//update or create materials
														if(workOrderMaterials){
															if(name1||quantity1||price1||sum1||name2||quantity2||price2||sum2||name3||quantity3||price3||sum3||name4||quantity4||price4||sum4||name5||quantity5||price5||sum5||name6||quantity6||price6||sum6||name7||quantity7||price7||sum7){
																$.post('/workorders/materials/update', {workOrderId,name1,quantity1,price1,sum1,name2,quantity2,price2,sum2,name3,quantity3,price3,sum3,name4,quantity4,price4,sum4,name5,quantity5,price5,sum5,name6,quantity6,price6,sum6,name7,quantity7,price7,sum7}, function(respMaterials){
																	if(respMaterials.success)
																		console.log('Uspesno dodajanje materialov na delovni nalog.7');
																	else{
																		console.log('Neuspesno dodajanje materialov na delovni nalog.7');
																		//open error modal
																	}
																	closeEditOnSuccess();
																})
															}
														}
														else{
															if(name1||quantity1||price1||sum1||name2||quantity2||price2||sum2||name3||quantity3||price3||sum3||name4||quantity4||price4||sum4||name5||quantity5||price5||sum5||name6||quantity6||price6||sum6||name7||quantity7||price7||sum7){
																$.post('/workorders/materials/add', {workOrderId,name1,quantity1,price1,sum1,name2,quantity2,price2,sum2,name3,quantity3,price3,sum3,name4,quantity4,price4,sum4,name5,quantity5,price5,sum5,name6,quantity6,price6,sum6,name7,quantity7,price7,sum7}, function(respMaterials){
																	if(respMaterials.success)
																		console.log('Uspesno dodajanje materialov na delovni nalog.8');
																	else{
																		console.log('Neuspesno dodajanje materialov na delovni nalog.8');
																		//open error modal
																	}
																	closeEditOnSuccess();
																})
															}
														}
													}
													else{
														console.log('Neuspešno dodajanje stroškov na delovni nalog!');
														closeEditOnSuccess();
														//open error modal
													}
												})
											}
											else{
												//update or create materials
												if(workOrderMaterials){//update
													if(name1||quantity1||price1||sum1||name2||quantity2||price2||sum2||name3||quantity3||price3||sum3||name4||quantity4||price4||sum4||name5||quantity5||price5||sum5||name6||quantity6||price6||sum6||name7||quantity7||price7||sum7){
														$.post('/workorders/materials/update', {workOrderId,name1,quantity1,price1,sum1,name2,quantity2,price2,sum2,name3,quantity3,price3,sum3,name4,quantity4,price4,sum4,name5,quantity5,price5,sum5,name6,quantity6,price6,sum6,name7,quantity7,price7,sum7}, function(respMaterials){
															if(respMaterials.success)
																console.log('Uspesno dodajanje materialov na delovni nalog.1');
															else{
																console.log('Neuspesno dodajanje materialov na delovni nalog.1');
																//open error modal
															}
															closeEditOnSuccess();
														})
													}
													else
														closeEditOnSuccess();
												}
												else{//create
													if(name1||quantity1||price1||sum1||name2||quantity2||price2||sum2||name3||quantity3||price3||sum3||name4||quantity4||price4||sum4||name5||quantity5||price5||sum5||name6||quantity6||price6||sum6||name7||quantity7||price7||sum7){
														$.post('/workorders/materials/add', {workOrderId,name1,quantity1,price1,sum1,name2,quantity2,price2,sum2,name3,quantity3,price3,sum3,name4,quantity4,price4,sum4,name5,quantity5,price5,sum5,name6,quantity6,price6,sum6,name7,quantity7,price7,sum7}, function(respMaterials){
															if(respMaterials.success)
																console.log('Uspesno dodajanje materialov na delovni nalog.2');
															else{
																console.log('Neuspesno dodajanje materialov na delovni nalog.2');
																//open error modal
															}
															closeEditOnSuccess();
														})
													}
													else
														closeEditOnSuccess();
												}
												//closeEditOnSuccess();
											}
										}
										//closeEditOnSuccess();
									}
								}
								//ADD SYSTEM CHANGE FOR EDITING WORK ORDER
								//addSystemChange(18,2,workOrderId);
								addChangeSystem(2,18,null,null,null,null,null,null,null,null,null,workOrderId);
								addChangeSystem(1,8,null,null,null,null,null,null,tmpSubscriber.id);
							}
							else
								console.log("Pri posodabljanju delovnega naloga je prišlo do napake.");
						})
					}
					else{
						//open error modal

					}
				})
			}
			else{
				//no new subscriber, edit work order
				$.post('/workorders/edit', {workOrderId,userId,number,type,date,location,subscriberId,projectId,description,workersId,vehicle,arrival,departure,representative,contactId}, function(resp){
					if(resp.success){
						console.log("Uspešno posodobitev delovnega naloga.");
						tmpSubscriber = {id:null,text:null};
						tmpProject = {id:null,text:null};
						if(subscriberId != 0){
							tmpSubscriber = allSubscribers.find(s => s.id == subscriberId);
						}
						if(projectId != 0)
							tmpProject = allProjects.find(p => p.id == projectId);
						tmpType = allTypes.find(t => t.id == type);
						//formatted date
						var newDate = date.split('-');
						var formatted_date = {date:newDate[2]+'.'+newDate[1]+'.'+newDate[0],time:'00:00'}
						//formatted time
						var formatted_arrival = {hours:arrival.split(':')[0], minutes:arrival.split(':')[1]};
						var formatted_departure = {hours:departure.split(':')[0], minutes:departure.split(':')[1]};
						//make wo number
						var author = allUsers.find(u=> u.id == woWorkerEdit.value).text.split(' ');
						var newWoNumber = author[0][0]+author[1][0]+'-'+workOrderId+'/'+newDate[0];
						//everything together
						tmpWorkOrder = allWorkOrders.find(wo => wo.id == workOrderId);
						tmpWorkOrder.subscriber_id = tmpSubscriber.id;
						tmpWorkOrder.wo_number = newWoNumber;
						tmpWorkOrder.user_id = woWorkerEdit.value;
						tmpWorkOrder.subscriber_name = tmpSubscriber.text;
						tmpWorkOrder.project_id = tmpProject.id;
						tmpWorkOrder.project_name = tmpProject.text;
						tmpWorkOrder.number = number;
						tmpWorkOrder.date = date;
						tmpWorkOrder.location = location;
						tmpWorkOrder.description = description;
						tmpWorkOrder.vehicle = vehicle;
						tmpWorkOrder.arrival = arrival;
						tmpWorkOrder.departure = departure;
						tmpWorkOrder.work_order_type_id = tmpType.id;
						tmpWorkOrder.work_order_type = tmpType.text;
						tmpWorkOrder.formatted_date = formatted_date;
						tmpWorkOrder.formatted_arrival = formatted_arrival;
						tmpWorkOrder.formatted_departure = formatted_departure;
						tmpWorkOrder.contact_id = contactId;
						tmpWorkOrder.representative = representative;
						
						if(workersId){
							editWorkers = $('#woWorkersEdit').val();
							var addWorkers = [];
							var deleteWorkers = [];
							for(var i = 0; i < workOrderWorkers.length; i++){
								tmpWorker = editWorkers.find(w => w == workOrderWorkers[i].worker_id);
								if(!tmpWorker)
									deleteWorkers.push(workOrderWorkers[i].id)
							}
							for(var i = 0; i < editWorkers.length; i++){
								tmpWorker = workOrderWorkers.find(wow => wow.worker_id == editWorkers[i])
								if(!tmpWorker)
									addWorkers.push(parseInt(editWorkers[i]));
							}
							//console.log("Novi delavci: ");
							//console.log(addWorkers);
							//console.log("Izbrisani zapisi delavcev: ");
							//console.log(deleteWorkers);
							if(addWorkers.length > 0 || deleteWorkers.length > 0){
								addWorkers = addWorkers.toString();
								deleteWorkers = deleteWorkers.toString();
								$.post('/workorders/workers', {workOrderId,addWorkers,deleteWorkers}, function(respWorkers){
									if(respWorkers.success){
										console.log('Uspešno posodabljanje delavcev na delovni nalog.');
										//update or create expenses
										if(workOrderExpenses){
											if(timeQuantity||timePrice||timeSum||distanceQuantity||distancePrice||distanceSum||materialQuantity||materialPrice||materialSum||otherQuantity||otherPrice||otherSum){
												$.post('/workorders/expenses/update', {workOrderId,timeQuantity,timePrice,timeSum,distanceQuantity,distancePrice,distanceSum,materialQuantity,materialPrice,materialSum,otherQuantity,otherPrice,otherSum}, function(respExpenses){
													if(respExpenses.success){
														console.log('Uspešno dodajanje stroškov na delovni nalog.');
														//update or create materials
														if(workOrderMaterials){
															if(name1||quantity1||price1||sum1||name2||quantity2||price2||sum2||name3||quantity3||price3||sum3||name4||quantity4||price4||sum4||name5||quantity5||price5||sum5||name6||quantity6||price6||sum6||name7||quantity7||price7||sum7){
																$.post('/workorders/materials/update', {workOrderId,name1,quantity1,price1,sum1,name2,quantity2,price2,sum2,name3,quantity3,price3,sum3,name4,quantity4,price4,sum4,name5,quantity5,price5,sum5,name6,quantity6,price6,sum6,name7,quantity7,price7,sum7}, function(respMaterials){
																	if(respMaterials.success)
																		console.log('Uspesno dodajanje materialov na delovni nalog.1');
																	else{
																		console.log('Neuspesno dodajanje materialov na delovni nalog.1');
																		//open error modal
																	}
																	closeEditOnSuccess();
																})
															}
															else
																closeEditOnSuccess();
														}
														else{
															if(name1||quantity1||price1||sum1||name2||quantity2||price2||sum2||name3||quantity3||price3||sum3||name4||quantity4||price4||sum4||name5||quantity5||price5||sum5||name6||quantity6||price6||sum6||name7||quantity7||price7||sum7){
																$.post('/workorders/materials/add', {workOrderId,name1,quantity1,price1,sum1,name2,quantity2,price2,sum2,name3,quantity3,price3,sum3,name4,quantity4,price4,sum4,name5,quantity5,price5,sum5,name6,quantity6,price6,sum6,name7,quantity7,price7,sum7}, function(respMaterials){
																	if(respMaterials.success)
																		console.log('Uspesno dodajanje materialov na delovni nalog.2');
																	else{
																		console.log('Neuspesno dodajanje materialov na delovni nalog.2');
																		//open error modal
																	}
																	closeEditOnSuccess();
																})
															}
															else
																closeEditOnSuccess();
														}
													}
													else{
														console.log('Neuspešno dodajanje stroškov na delovni nalog!');
														closeEditOnSuccess();
														//open error modal
													}
												})
											}
											else{
												//no expenses, update/create materials if any
												//update or create materials
												if(workOrderMaterials){//update
													if(name1||quantity1||price1||sum1||name2||quantity2||price2||sum2||name3||quantity3||price3||sum3||name4||quantity4||price4||sum4||name5||quantity5||price5||sum5||name6||quantity6||price6||sum6||name7||quantity7||price7||sum7){
														$.post('/workorders/materials/update', {workOrderId,name1,quantity1,price1,sum1,name2,quantity2,price2,sum2,name3,quantity3,price3,sum3,name4,quantity4,price4,sum4,name5,quantity5,price5,sum5,name6,quantity6,price6,sum6,name7,quantity7,price7,sum7}, function(respMaterials){
															if(respMaterials.success)
																console.log('Uspesno dodajanje materialov na delovni nalog.1');
															else{
																console.log('Neuspesno dodajanje materialov na delovni nalog.1');
																//open error modal
															}
															closeEditOnSuccess();
														})
													}
													else
														closeEditOnSuccess();
												}
												else{//create
													if(name1||quantity1||price1||sum1||name2||quantity2||price2||sum2||name3||quantity3||price3||sum3||name4||quantity4||price4||sum4||name5||quantity5||price5||sum5||name6||quantity6||price6||sum6||name7||quantity7||price7||sum7){
														$.post('/workorders/materials/add', {workOrderId,name1,quantity1,price1,sum1,name2,quantity2,price2,sum2,name3,quantity3,price3,sum3,name4,quantity4,price4,sum4,name5,quantity5,price5,sum5,name6,quantity6,price6,sum6,name7,quantity7,price7,sum7}, function(respMaterials){
															if(respMaterials.success)
																console.log('Uspesno dodajanje materialov na delovni nalog.2');
															else{
																console.log('Neuspesno dodajanje materialov na delovni nalog.2');
																//open error modal
															}
															closeEditOnSuccess();
														})
													}
													else
														closeEditOnSuccess();
												}
												//closeEditOnSuccess();
											}
										}
										else{
											if(timeQuantity||timePrice||timeSum||distanceQuantity||distancePrice||distanceSum||materialQuantity||materialPrice||materialSum||otherQuantity||otherPrice||otherSum){
												$.post('/workorders/expenses/add', {workOrderId,timeQuantity,timePrice,timeSum,distanceQuantity,distancePrice,distanceSum,materialQuantity,materialPrice,materialSum,otherQuantity,otherPrice,otherSum}, function(respExpenses){
													if(respExpenses.success){
														console.log('Uspešno dodajanje stroškov na delovni nalog.');
														//update or create materials
														if(workOrderMaterials){
															if(name1||quantity1||price1||sum1||name2||quantity2||price2||sum2||name3||quantity3||price3||sum3||name4||quantity4||price4||sum4||name5||quantity5||price5||sum5||name6||quantity6||price6||sum6||name7||quantity7||price7||sum7){
																$.post('/workorders/materials/update', {workOrderId,name1,quantity1,price1,sum1,name2,quantity2,price2,sum2,name3,quantity3,price3,sum3,name4,quantity4,price4,sum4,name5,quantity5,price5,sum5,name6,quantity6,price6,sum6,name7,quantity7,price7,sum7}, function(respMaterials){
																	if(respMaterials.success)
																		console.log('Uspesno dodajanje materialov na delovni nalog.3');
																	else{
																		console.log('Neuspesno dodajanje materialov na delovni nalog.3');
																		//open error modal
																	}
																	closeEditOnSuccess();
																})
															}
															else
																closeEditOnSuccess();
														}
														else{
															if(name1||quantity1||price1||sum1||name2||quantity2||price2||sum2||name3||quantity3||price3||sum3||name4||quantity4||price4||sum4||name5||quantity5||price5||sum5||name6||quantity6||price6||sum6||name7||quantity7||price7||sum7){
																$.post('/workorders/materials/add', {workOrderId,name1,quantity1,price1,sum1,name2,quantity2,price2,sum2,name3,quantity3,price3,sum3,name4,quantity4,price4,sum4,name5,quantity5,price5,sum5,name6,quantity6,price6,sum6,name7,quantity7,price7,sum7}, function(respMaterials){
																	if(respMaterials.success)
																		console.log('Uspesno dodajanje materialov na delovni nalog.4');
																	else{
																		console.log('Neuspesno dodajanje materialov na delovni nalog.4');
																		//open error modal
																	}
																	closeEditOnSuccess();
																})
															}
															else
																closeEditOnSuccess();
														}
													}
													else{
														console.log('Neuspešno dodajanje stroškov na delovni nalog!');
														closeEditOnSuccess();
														//open error modal
													}
												})
											}
											else{
												//no expenses, update/create materials if any
												//update or create materials
												if(workOrderMaterials){//update
													if(name1||quantity1||price1||sum1||name2||quantity2||price2||sum2||name3||quantity3||price3||sum3||name4||quantity4||price4||sum4||name5||quantity5||price5||sum5||name6||quantity6||price6||sum6||name7||quantity7||price7||sum7){
														$.post('/workorders/materials/update', {workOrderId,name1,quantity1,price1,sum1,name2,quantity2,price2,sum2,name3,quantity3,price3,sum3,name4,quantity4,price4,sum4,name5,quantity5,price5,sum5,name6,quantity6,price6,sum6,name7,quantity7,price7,sum7}, function(respMaterials){
															if(respMaterials.success)
																console.log('Uspesno dodajanje materialov na delovni nalog.1');
															else{
																console.log('Neuspesno dodajanje materialov na delovni nalog.1');
																//open error modal
															}
															closeEditOnSuccess();
														})
													}
													else
														closeEditOnSuccess();
												}
												else{//create
													if(name1||quantity1||price1||sum1||name2||quantity2||price2||sum2||name3||quantity3||price3||sum3||name4||quantity4||price4||sum4||name5||quantity5||price5||sum5||name6||quantity6||price6||sum6||name7||quantity7||price7||sum7){
														$.post('/workorders/materials/add', {workOrderId,name1,quantity1,price1,sum1,name2,quantity2,price2,sum2,name3,quantity3,price3,sum3,name4,quantity4,price4,sum4,name5,quantity5,price5,sum5,name6,quantity6,price6,sum6,name7,quantity7,price7,sum7}, function(respMaterials){
															if(respMaterials.success)
																console.log('Uspesno dodajanje materialov na delovni nalog.2');
															else{
																console.log('Neuspesno dodajanje materialov na delovni nalog.2');
																//open error modal
															}
															closeEditOnSuccess();
														})
													}
													else
														closeEditOnSuccess();
												}
												//closeEditOnSuccess();
											}
										}
										//closeEditOnSuccess();
									}
									else{
										console.log('Neuspešno posodabljane delavcev na delovni nalog!');
										//open error modal
									}
								})
							}
							else{
								//update or create expenses
								if(workOrderExpenses){
									if(timeQuantity||timePrice||timeSum||distanceQuantity||distancePrice||distanceSum||materialQuantity||materialPrice||materialSum||otherQuantity||otherPrice||otherSum){
										$.post('/workorders/expenses/update', {workOrderId,timeQuantity,timePrice,timeSum,distanceQuantity,distancePrice,distanceSum,materialQuantity,materialPrice,materialSum,otherQuantity,otherPrice,otherSum}, function(respExpenses){
											if(respExpenses.success){
												console.log('Uspešno dodajanje stroškov na delovni nalog.');
												//update or create materials
												if(workOrderMaterials){
													if(name1||quantity1||price1||sum1||name2||quantity2||price2||sum2||name3||quantity3||price3||sum3||name4||quantity4||price4||sum4||name5||quantity5||price5||sum5||name6||quantity6||price6||sum6||name7||quantity7||price7||sum7){
														$.post('/workorders/materials/update', {workOrderId,name1,quantity1,price1,sum1,name2,quantity2,price2,sum2,name3,quantity3,price3,sum3,name4,quantity4,price4,sum4,name5,quantity5,price5,sum5,name6,quantity6,price6,sum6,name7,quantity7,price7,sum7}, function(respMaterials){
															if(respMaterials.success)
																console.log('Uspesno dodajanje materialov na delovni nalog.5');
															else{
																console.log('Neuspesno dodajanje materialov na delovni nalog.5');
																//open error modal
															}
															closeEditOnSuccess();
														})
													}
													else
														closeEditOnSuccess();
												}
												else{
													if(name1||quantity1||price1||sum1||name2||quantity2||price2||sum2||name3||quantity3||price3||sum3||name4||quantity4||price4||sum4||name5||quantity5||price5||sum5||name6||quantity6||price6||sum6||name7||quantity7||price7||sum7){
														$.post('/workorders/materials/add', {workOrderId,name1,quantity1,price1,sum1,name2,quantity2,price2,sum2,name3,quantity3,price3,sum3,name4,quantity4,price4,sum4,name5,quantity5,price5,sum5,name6,quantity6,price6,sum6,name7,quantity7,price7,sum7}, function(respMaterials){
															if(respMaterials.success)
																console.log('Uspesno dodajanje materialov na delovni nalog.6');
															else{
																console.log('Neuspesno dodajanje materialov na delovni nalog.6');
																//open error modal
															}
															closeEditOnSuccess();
														})
													}
													else
														closeEditOnSuccess();
												}
											}
											else{
												console.log('Neuspešno dodajanje stroškov na delovni nalog!');
												closeEditOnSuccess();
												//open error modal
											}
										})
									}
									else{
										//no expenses, update/create materials if any
										//update or create materials
										if(workOrderMaterials){//update
											if(name1||quantity1||price1||sum1||name2||quantity2||price2||sum2||name3||quantity3||price3||sum3||name4||quantity4||price4||sum4||name5||quantity5||price5||sum5||name6||quantity6||price6||sum6||name7||quantity7||price7||sum7){
												$.post('/workorders/materials/update', {workOrderId,name1,quantity1,price1,sum1,name2,quantity2,price2,sum2,name3,quantity3,price3,sum3,name4,quantity4,price4,sum4,name5,quantity5,price5,sum5,name6,quantity6,price6,sum6,name7,quantity7,price7,sum7}, function(respMaterials){
													if(respMaterials.success)
														console.log('Uspesno dodajanje materialov na delovni nalog.1');
													else{
														console.log('Neuspesno dodajanje materialov na delovni nalog.1');
														//open error modal
													}
													closeEditOnSuccess();
												})
											}
											else
												closeEditOnSuccess();
										}
										else{//create
											if(name1||quantity1||price1||sum1||name2||quantity2||price2||sum2||name3||quantity3||price3||sum3||name4||quantity4||price4||sum4||name5||quantity5||price5||sum5||name6||quantity6||price6||sum6||name7||quantity7||price7||sum7){
												$.post('/workorders/materials/add', {workOrderId,name1,quantity1,price1,sum1,name2,quantity2,price2,sum2,name3,quantity3,price3,sum3,name4,quantity4,price4,sum4,name5,quantity5,price5,sum5,name6,quantity6,price6,sum6,name7,quantity7,price7,sum7}, function(respMaterials){
													if(respMaterials.success)
														console.log('Uspesno dodajanje materialov na delovni nalog.2');
													else{
														console.log('Neuspesno dodajanje materialov na delovni nalog.2');
														//open error modal
													}
													closeEditOnSuccess();
												})
											}
											else
												closeEditOnSuccess();
										}
										//closeEditOnSuccess();
									}
								}
								else{
									if(timeQuantity||timePrice||timeSum||distanceQuantity||distancePrice||distanceSum||materialQuantity||materialPrice||materialSum||otherQuantity||otherPrice||otherSum){
										$.post('/workorders/expenses/add', {workOrderId,timeQuantity,timePrice,timeSum,distanceQuantity,distancePrice,distanceSum,materialQuantity,materialPrice,materialSum,otherQuantity,otherPrice,otherSum}, function(respExpenses){
											if(respExpenses.success){
												console.log('Uspešno dodajanje stroškov na delovni nalog.');
												//update or create materials
												if(workOrderMaterials){
													if(name1||quantity1||price1||sum1||name2||quantity2||price2||sum2||name3||quantity3||price3||sum3||name4||quantity4||price4||sum4||name5||quantity5||price5||sum5||name6||quantity6||price6||sum6||name7||quantity7||price7||sum7){
														$.post('/workorders/materials/update', {workOrderId,name1,quantity1,price1,sum1,name2,quantity2,price2,sum2,name3,quantity3,price3,sum3,name4,quantity4,price4,sum4,name5,quantity5,price5,sum5,name6,quantity6,price6,sum6,name7,quantity7,price7,sum7}, function(respMaterials){
															if(respMaterials.success)
																console.log('Uspesno dodajanje materialov na delovni nalog.7');
															else{
																console.log('Neuspesno dodajanje materialov na delovni nalog.7');
																//open error modal
															}
																closeEditOnSuccess();
														})
													}
													else
														closeEditOnSuccess();
												}
												else{
													if(name1||quantity1||price1||sum1||name2||quantity2||price2||sum2||name3||quantity3||price3||sum3||name4||quantity4||price4||sum4||name5||quantity5||price5||sum5||name6||quantity6||price6||sum6||name7||quantity7||price7||sum7){
														$.post('/workorders/materials/add', {workOrderId,name1,quantity1,price1,sum1,name2,quantity2,price2,sum2,name3,quantity3,price3,sum3,name4,quantity4,price4,sum4,name5,quantity5,price5,sum5,name6,quantity6,price6,sum6,name7,quantity7,price7,sum7}, function(respMaterials){
															if(respMaterials.success)
																console.log('Uspesno dodajanje materialov na delovni nalog.8');
															else{
																console.log('Neuspesno dodajanje materialov na delovni nalog.8');
																//open error modal
															}
															closeEditOnSuccess();
														})
													}
													else
														closeEditOnSuccess();
												}
											}
											else{
												console.log('Neuspešno dodajanje stroškov na delovni nalog!');
												closeEditOnSuccess();
												//open error modal
											}
										})
									}
									else{
										//no expenses, update/create materials if any
										//update or create materials
										if(workOrderMaterials){//update
											if(name1||quantity1||price1||sum1||name2||quantity2||price2||sum2||name3||quantity3||price3||sum3||name4||quantity4||price4||sum4||name5||quantity5||price5||sum5||name6||quantity6||price6||sum6||name7||quantity7||price7||sum7){
												$.post('/workorders/materials/update', {workOrderId,name1,quantity1,price1,sum1,name2,quantity2,price2,sum2,name3,quantity3,price3,sum3,name4,quantity4,price4,sum4,name5,quantity5,price5,sum5,name6,quantity6,price6,sum6,name7,quantity7,price7,sum7}, function(respMaterials){
													if(respMaterials.success)
														console.log('Uspesno dodajanje materialov na delovni nalog.1');
													else{
														console.log('Neuspesno dodajanje materialov na delovni nalog.1');
														//open error modal
													}
													closeEditOnSuccess();
												})
											}
											else
												closeEditOnSuccess();
										}
										else{//create
											if(name1||quantity1||price1||sum1||name2||quantity2||price2||sum2||name3||quantity3||price3||sum3||name4||quantity4||price4||sum4||name5||quantity5||price5||sum5||name6||quantity6||price6||sum6||name7||quantity7||price7||sum7){
												$.post('/workorders/materials/add', {workOrderId,name1,quantity1,price1,sum1,name2,quantity2,price2,sum2,name3,quantity3,price3,sum3,name4,quantity4,price4,sum4,name5,quantity5,price5,sum5,name6,quantity6,price6,sum6,name7,quantity7,price7,sum7}, function(respMaterials){
													if(respMaterials.success)
														console.log('Uspesno dodajanje materialov na delovni nalog.2');
													else{
														console.log('Neuspesno dodajanje materialov na delovni nalog.2');
														//open error modal
													}
													closeEditOnSuccess();
												})
											}
											else
												closeEditOnSuccess();
										}
										//closeEditOnSuccess();
									}
								}
								//closeEditOnSuccess();
							}
						}
						// ADD SIGN IF EXISTS TO EXISTING WORK ORDER
						if(signId){
							$.post('/workorders/signs/attach', {workOrderId,signId}, function(respSigns){
								if (respSigns.success){
									console.log('Uspešno dodajanje podpisa na obstoječi delovni nalog.');
									editSignaturePad.clear();
									tmpEditSignId = undefined;
									workOrderSigns = [tmpEditSign];
								}
								else{
									console.log('Neuspešno dodajanje podpisa na delovni nalog!');
									//open error modal
								}
							})
							savedWorkOrderIdForMail = workOrderId;
							$('#openSignModalEditBtn').removeClass('btn-primary btn-danger btn-success').addClass('btn-primary');
							$('#openSignModalEditBtn').find('i').removeClass('invisible fa-check fa-times').addClass('invisible');
							$('#signatureIcon').show();
							$('#openSignModalEditBtn').hide();

						}
						//ADD SYSTEM CHANGE FOR EDITING WORK ORDER
						//addSystemChange(18,2,workOrderId);
						addChangeSystem(2,18,null,null,null,null,null,null,null,null,null,workOrderId);
					}
					else{
						console.log("Pri posodabljanju delovnega naloga je prišlo do napake.");
						//open error modal
					}
				})
			}
		})
		//get work orders based on search input
		$('#getWorkOrdersForm').submit(function(e){
			e.preventDefault();
			$form = $(this);
	
			var userId = null;
			if(loggedUser.role == 'admin' || loggedUser.role == 'tajnik' || loggedUser.role == 'komercialist' || loggedUser.role == 'komerciala' || loggedUser.role == 'računovodstvo' || loggedUser.role.substring(0,5) == 'vodja')
				userId = woUserSelect.value;
			var dateStart = woStartSelect.value;
			var dateFinish = woFinishSelect.value;
			var projectId = woProjectSelect.value;
			var subscriberId = woSubscriberSelect.value;
			var woType = woTypeSelect.value;
			var woStatus = woStatusSelect.value;
			$.get( "/workorders/get",{userId,dateStart,dateFinish,projectId,subscriberId,woType,woStatus}, function( resp ) {
				if(resp.success){
					//debugger;
					console.log('Uspešno pridobivanje podatkov');
					allWorkOrders = resp.data;
					$('#workOrderName').html('Seznam iskanih delovnih nalog:');
					drawWorkOrders();
				}
				else{
					//open error modal
				}
			});
		})
	},200)
	// change table siye if screen size is smaller (mobile users)
	if (screen.width < 769) {
		$('#woTableExpensesNew').addClass('table-sm');
		$('#woTableMaterialNew').addClass('table-sm');
		$('#woTableExpensesEdit').addClass('table-sm');
		$('#woTableMaterialEdit').addClass('table-sm');
		$('#getWorkOrdersForm').collapse('toggle');
	}
	// call functions
	$('#searchMenuCollapse').on('click', function () {
		$('#getWorkOrdersForm').collapse('toggle');
	});
	$('#woContactNew').on('change', function () {
		var tmpContactId = $('#woContactNew').val();
		if (tmpContactId){
			var tmpContact = allContacts.find(c => c.id == tmpContactId);
			if (tmpContact){
				tmpContact.phone_number ? $('#woContactPhoneNew').val(tmpContact.phone_number) : $('#woContactPhoneNew').val('');
			}
			else
				$('#woContactPhoneNew').val('');
		}
		else{
			$('#woContactPhoneNew').val('');
		}
	});
	$('#woContactEdit').on('change', function () {
		var tmpContactId = $('#woContactEdit').val();
		if (tmpContactId){
			var tmpContact = allContacts.find(c => c.id == tmpContactId);
			if (tmpContact){
				tmpContact.phone_number ? $('#woContactPhoneEdit').val(tmpContact.phone_number) : $('#woContactPhoneEdit').val('');
			}
			else
				$('#woContactPhoneEdit').val('');
		}
		else{
			$('#woContactPhoneEdit').val('');
		}
	});
	// editing work order contact
	$('#btnConfContactEdit').on('click', function () {
		var tmp = allContacts.find(c => c.id == $('#woContactEdit').val());
		if(!tmp){
			console.log('create new contact and close modal')
			let data = {contactName: $('#woContactEdit').val(), contactPhone: $('#woContactPhoneEdit').val()};
			$.ajax({
				type: 'POST',
				url: '/workorders/contacts',
				contentType: 'application/x-www-form-urlencoded',
				data: data, // access in body
			}).done(function (resp) {
				if (resp.success){
					// allCompSub = resp.allCompSub;
					let tmpContact = resp.contact;
					tmpContact.text = tmpContact.full_name;
					allContacts.push(tmpContact);
					$(".select2-new-contacts").prepend('<option selected></option>').select2({
						data: allContacts,
						search: true,
						allowClear: true,
						placeholder: "Kontaktna oseba",
						tags: true,
						dropdownParent: $('#modalNewContact')
					});
					$(".select2-edit-contacts").prepend('<option selected></option>').select2({
						data: allContacts,
						search: true,
						allowClear: true,
						placeholder: "Kontaktna oseba",
						tags: true,
						dropdownParent: $('#modalEditContact')
					});
					$('#woContactEdit').val(tmpContact.id).trigger('change');
					$('#modalEditContact').modal('toggle');
				}
				else{
					$('#modalEditContact').modal('toggle');
					$('#modalError').modal('toggle');
				}
			}).fail(function (resp) {//console.log('FAIL');//debugger;
				$('#modalEditContact').modal('toggle');
				$('#modalError').modal('toggle');
			}).always(function (resp) {//console.log('ALWAYS');//debugger;
			});
		}
		else{
			console.log('contact exists, check if new phone number and save if so');
			if (tmp.phone_number != $('#woContactPhoneEdit').val()){
				let data = {contactId: tmp.id, contactPhone: $('#woContactPhoneEdit').val()};
				$.ajax({
					type: 'PUT',
					url: '/workorders/contacts',
					contentType: 'application/x-www-form-urlencoded',
					data: data, // access in body
				}).done(function (resp) {
					if (resp.success){
						// allCompSub = resp.allCompSub;
						let tmpContact = resp.contact;
						tmpContact.text = tmpContact.full_name;
						allContacts.push(tmpContact);
						$(".select2-new-contacts").prepend('<option selected></option>').select2({
							data: allContacts,
							search: true,
							allowClear: true,
							placeholder: "Kontaktna oseba",
							tags: true,
							dropdownParent: $('#modalNewContact')
						});
						$(".select2-edit-contacts").prepend('<option selected></option>').select2({
							data: allContacts,
							search: true,
							allowClear: true,
							placeholder: "Kontaktna oseba",
							tags: true,
							dropdownParent: $('#modalEditContact')
						});
						$('#woContactEdit').val(tmpContact.id).trigger('change');
						$('#modalEditContact').modal('toggle');
					}
					else{
						$('#modalEditContact').modal('toggle');
						$('#modalError').modal('toggle');
					}
				}).fail(function (resp) {//console.log('FAIL');//debugger;
					$('#modalEditContact').modal('toggle');
					$('#modalError').modal('toggle');
				}).always(function (resp) {//console.log('ALWAYS');//debugger;
				});

			}
			else{
				$('#modalEditContact').modal('toggle');
			}
		}
	})
	//adding 
	$('#btnConfContactNew').on('click', function () {
		var tmp = allContacts.find(c => c.id == $('#woContactNew').val());
		if(!tmp){
			console.log('create new contact and close modal')
			let data = {contactName: $('#woContactNew').val(), contactPhone: $('#woContactPhoneNew').val()};
			$.ajax({
				type: 'POST',
				url: '/workorders/contacts',
				contentType: 'application/x-www-form-urlencoded',
				data: data, // access in body
			}).done(function (resp) {
				if (resp.success){
					// allCompSub = resp.allCompSub;
					let tmpContact = resp.contact;
					tmpContact.text = tmpContact.full_name;
					allContacts.push(tmpContact);
					$(".select2-new-contacts").prepend('<option selected></option>').select2({
						data: allContacts,
						search: true,
						allowClear: true,
						placeholder: "Kontaktna oseba",
						tags: true,
						dropdownParent: $('#modalNewContact')
					});
					$(".select2-edit-contacts").prepend('<option selected></option>').select2({
						data: allContacts,
						search: true,
						allowClear: true,
						placeholder: "Kontaktna oseba",
						tags: true,
						dropdownParent: $('#modalEditContact')
					});
					$('#woContactNew').val(tmpContact.id).trigger('change');
					$('#modalNewContact').modal('toggle');
				}
				else{
					$('#modalNewContact').modal('toggle');
					$('#modalError').modal('toggle');
				}
			}).fail(function (resp) {//console.log('FAIL');//debugger;
				$('#modalNewContact').modal('toggle');
				$('#modalError').modal('toggle');
			}).always(function (resp) {//console.log('ALWAYS');//debugger;
			});
		}
		else{
			console.log('contact exists, check if new phone number and save if so');
			if (tmp.phone_number != $('#woContactPhoneNew').val()){
				let data = {contactId: tmp.id, contactPhone: $('#woContactPhoneNew').val()};
				$.ajax({
					type: 'PUT',
					url: '/workorders/contacts',
					contentType: 'application/x-www-form-urlencoded',
					data: data, // access in body
				}).done(function (resp) {
					if (resp.success){
						// allCompSub = resp.allCompSub;
						let tmpContact = resp.contact;
						tmpContact.text = tmpContact.full_name;
						allContacts.push(tmpContact);
						$(".select2-new-contacts").prepend('<option selected></option>').select2({
							data: allContacts,
							search: true,
							allowClear: true,
							placeholder: "Kontaktna oseba",
							tags: true,
							dropdownParent: $('#modalNewContact')
						});
						$(".select2-edit-contacts").prepend('<option selected></option>').select2({
							data: allContacts,
							search: true,
							allowClear: true,
							placeholder: "Kontaktna oseba",
							tags: true,
							dropdownParent: $('#modalEditContact')
						});
						$('#woContactNew').val(tmpContact.id).trigger('change');
						$('#modalNewContact').modal('toggle');
					}
					else{
						$('#modalNewContact').modal('toggle');
						$('#modalError').modal('toggle');
					}
				}).fail(function (resp) {//console.log('FAIL');//debugger;
					$('#modalNewContact').modal('toggle');
					$('#modalError').modal('toggle');
				}).always(function (resp) {//console.log('ALWAYS');//debugger;
				});

			}
			else{
				$('#modalNewContact').modal('toggle');
			}
		}
	})
	$('#btnSendConf').on('click', function () {
		var email = 'tanja.muzerlin@roboteh.si';
		var officeCheck = false;
		var useTimeout = true;
		var timeAmount = 10000;
		let tmp = tmpWorkOrder;
		tmp.office_mail = email;
		//POST info to route for adding new activity
		debugger;
		let data = {workOrderId,email,officeCheck,useTimeout,timeAmount,officeMail:true};
		$.ajax({
			type: 'POST',
			url: '/workorders/mail',
			contentType: 'application/json',
			data: JSON.stringify(data), // access in body
		}).done(function (resp) {
			if(resp.success){
				debugger;
				// $('#woEmail').val('');
				// $('#modalSendMail').modal('toggle');
				console.log('Uspešno poslano elektronsko sporočilo upravi o novem delovnem nalogu.');
			}
			else{
				$('#modalError').modal('toggle');
				console.log('Neuspešno poslano elektronsko sporočilo upravi o novem delovnem nalogu.');
			}
		}).fail(function (resp) {
			//console.log('FAIL');
			//debugger;
			tmp.office_mail = null;
			console.log('Neuspešno poslano elektronsko sporočilo upravi o novem delovnem nalogu.');
			$('#modalError').modal('toggle');
		}).always(function (resp) {
			//console.log('ALWAYS');
			//debugger;
		});
		$('#modalSendMail').modal('toggle');
	});
	// on change subscriber filter project list
	$("#woSubscriberNew").on('change', function(){
		// console.log('sprememba narocnika, projekti ki ustrezajo: ');
		let tmpSub = allSubscribers.find(s => s.id == $('#woSubscriberNew').val())
		// newSelectedSubscriber = 
		let tmpSelectedProject = $('#woProjectNew').val();
		if (tmpSub){
			$("#woProjectNew").empty();
			if (tmpSub.id == 0){
				// add all active projects to list
				let tmpProjects = allProjects.filter(p => p.active == true);
				$("#woProjectNew").prepend('<option value="0">Vsi</option>');
				for (const project of tmpProjects) {
					// let selected = '';
					// if (project.id == tmpSelectedProject)
					// 	selected = ' selected =""';
					// if selected is added then subscriber cannot be fixed to all because it gets fixed before it gets empty value
					$("#woProjectNew").append('<option value="' + project.id + '">' + project.text + '</option>');
				}
				$("#woProjectNew").trigger('change');
			}
			else{
				let tmpProjects = allProjects.filter(p => p.name == tmpSub.text && p.active == true);
				// console.log(tmpProjects);
				// tmpProjects.unshift({id:0, text: "Vsi"});
				// $(".woProjectNew").select2({
				// 	data: tmpProjects,
				// 	search: false,
				// }).trigger('change');
				$("#woProjectNew").prepend('<option value="0">Vsi</option>');
				for (const project of tmpProjects) {
					let selected = '';
					if (project.id == tmpSelectedProject)
						selected = ' selected =""';
					$("#woProjectNew").append('<option value="' + project.id + '" ' + selected + '>' + project.text + '</option>');
				}
				$("#woProjectNew").trigger('change');
			}
		}
	})
	// on change project find correct subscriber
	$('#woProjectNew').on('change', function(){
		// debugger
		if($('#woProjectNew').val() != 0){
			// console.log('sprememba projekta');
			// debugger;
			let tmpProject = allProjects.find(p => p.id == $('#woProjectNew').val());
			if (tmpProject.id_subscriber != $('#woSubscriberNew').val()){
				// subscriber does not match to projects subscriber, fix subscriber to correct one
				$('#woSubscriberNew').val(tmpProject.id_subscriber).trigger('change');
				// console.log('fix subscriber')
			}
		}
	})
})
function disableFunction(){
  $('#btnAddDoc').attr('disabled',true);
}
function closeEditOnSuccess(){
	tmpWorkOrder = allWorkOrders.find(wo => wo.id == workOrderId);
	tmpSerialNumber = tmpWorkOrder.id + '/' + tmpWorkOrder.formatted_date.date.split('.')[2];
	$('#workOrderNumber').html(tmpWorkOrder.wo_number);
	$('#workOrderNumber'+tmpWorkOrder.id).html(tmpWorkOrder.wo_number)
	$('#workOrderDate'+workOrderId).html(tmpWorkOrder.formatted_date.date);
	$('#workOrderDate').html(tmpWorkOrder.formatted_date.date)
	$('#workOrderType').html(tmpWorkOrder.work_order_type)
	$('#workOrderLocation').html(tmpWorkOrder.location)
	filteredServices = allServices.filter(s => s.id_subscriber == tmpWorkOrder.subscriber_id);
	$("#workOrderService").empty().select2({ data: filteredServices, });
	$('#workOrderService').val(tmpWorkOrder.task_id).trigger('change');
	//var tmpService;
	if(tmpWorkOrder.task_id && $("#workOrderService").val() == null)
		saveService(true);
	var info = '';
	if(tmpWorkOrder.project_id){
		$('#workOrderProject').html(tmpWorkOrder.project_name)
		info = tmpWorkOrder.project_name;
	}
	else
		$('#workOrderProject').html('')
	if(tmpWorkOrder.subscriber_id){
		$('#workOrderSubscriber').html(tmpWorkOrder.subscriber_name)
		info = tmpWorkOrder.subscriber_name;
	}
	else
		$('#workOrderSubscriber').html('')
	$('#workOrderInfo'+tmpWorkOrder.id).html(info)
	$('#workOrderDescription').html(tmpWorkOrder.description)
	$('#workOrderVehicle').html(tmpWorkOrder.vehicle)
	tmpWorkOrder.arrival ? $('#workOrderArrival').html(tmpWorkOrder.formatted_arrival.hours+":"+tmpWorkOrder.formatted_arrival.minutes) : $('#workOrderArrival').html('/');
	tmpWorkOrder.departure ? $('#workOrderDeparture').html(tmpWorkOrder.formatted_departure.hours+":"+tmpWorkOrder.formatted_departure.minutes) : $('#workOrderDeparture').html('/');
	$('#workOrderRepresentative').html(tmpWorkOrder.representative)
	tmpUser = allUsers.find(u => u.id == tmpWorkOrder.user_id)
	$('#workOrderUser').html(tmpUser.text)
	$.get( "/workorders/workers",{workOrderId}, function( data ) {
		if(data.success){
			workOrderWorkers = data.data;
			var workers = '';
			for(var i = 0; i < workOrderWorkers.length; i++){
				workers += workOrderWorkers[i].name + ' ' + workOrderWorkers[i].surname;
				if(i+1 != workOrderWorkers.length)
					workers += ', ';
			}
			$('#workOrderWorkers').html(workers);
		}
		else{

		}
	});
	$.get( "/workorders/expenses",{workOrderId}, function( data ) {
		if(data.success){
			workOrderExpenses = data.data[0];
			if(workOrderExpenses){
				if(workOrderExpenses.time_quantity){
					var time = workOrderExpenses.time_quantity.split(':');
					$('#timeQuantity').html(time[0]+'h'+time[1]+'m');
				}
				else
					$('#timeQuantity').html('');
				if(workOrderExpenses.time_price)
					$('#timePrice').html(workOrderExpenses.time_price/100);
				else
					$('#timePrice').html('');
				if(workOrderExpenses.time_sum)
					$('#timeSum').html(workOrderExpenses.time_sum/100);
				else
					$('#timeSum').html('');
				if(workOrderExpenses.distance_quantity)
					$('#distanceQuantity').html(workOrderExpenses.distance_quantity);
				else
					$('#distanceQuantity').html('');
				if(workOrderExpenses.distance_price)
					$('#distancePrice').html(workOrderExpenses.distance_price/100);
				else
					$('#distancePrice').html('');
				if(workOrderExpenses.distance_sum)
					$('#distanceSum').html(workOrderExpenses.distance_sum/100);
				else
					$('#distanceSum').html('');
				if(workOrderExpenses.material_quantity)
					$('#materialQuantity').html(workOrderExpenses.material_quantity);
				else
					$('#materialQuantity').html('');
				if(workOrderExpenses.material_price)
					$('#materialPrice').html(workOrderExpenses.material_price/100);
				else
					$('#materialPrice').html('');
				if(workOrderExpenses.material_sum)
					$('#materialSum').html(workOrderExpenses.material_sum/100);
				else
					$('#materialSum').html('');
				if(workOrderExpenses.other_quantity)
					$('#otherQuantity').html(workOrderExpenses.other_quantity);
				else
					$('#otherQuantity').html('');
				if(workOrderExpenses.other_price)
					$('#otherPrice').html(workOrderExpenses.other_price/100);
				else
					$('#otherPrice').html('');
				if(workOrderExpenses.other_sum)
					$('#otherSum').html(workOrderExpenses.other_sum/100);
				else
					$('#otherSum').html('');
			}
			else{
				$('#timeQuantity').html('');
				$('#timePrice').html('');
				$('#timeSum').html('');
				$('#distanceQuantity').html('');
				$('#distancePrice').html('');
				$('#distanceSum').html('');
				$('#materialQuantity').html('');
				$('#materialPrice').html('');
				$('#materialSum').html('');
				$('#otherQuantity').html('');
				$('#otherPrice').html('');
				$('#otherSum').html('');
			}
		}
		else{
			//open error modal
		}
	});
	//MATERIALS
	$.get( "/workorders/materials",{workOrderId}, function( data ) {
		if(data.success){
			workOrderMaterials = data.data[0];
			//1
			$('#materialName1').html('');
			$('#materialQuantity1').html('');
			$('#materialPrice1').html('');
			$('#materialSum1').html('');
			//2
			$('#materialName2').html('');
			$('#materialQuantity2').html('');
			$('#materialPrice2').html('');
			$('#materialSum2').html('');
			//3
			$('#materialName3').html('');
			$('#materialQuantity3').html('');
			$('#materialPrice3').html('');
			$('#materialSum3').html('');
			//4
			$('#materialName4').html('');
			$('#materialQuantity4').html('');
			$('#materialPrice4').html('');
			$('#materialSum4').html('');
			//5
			$('#materialName5').html('');
			$('#materialQuantity5').html('');
			$('#materialPrice5').html('');
			$('#materialSum5').html('');
			//6
			$('#materialName6').html('');
			$('#materialQuantity6').html('');
			$('#materialPrice6').html('');
			$('#materialSum6').html('');
			//7
			$('#materialName7').html('');
			$('#materialQuantity7').html('');
			$('#materialPrice7').html('');
			$('#materialSum7').html('');
			if(workOrderMaterials){
				//1
				if(workOrderMaterials.name1) $('#materialName1').html(workOrderMaterials.name1);
				if(workOrderMaterials.quantity1) $('#materialQuantity1').html(workOrderMaterials.quantity1);
				if(workOrderMaterials.price1) $('#materialPrice1').html(workOrderMaterials.price1);
				if(workOrderMaterials.sum1) $('#materialSum1').html(workOrderMaterials.sum1);
				//2
				if(workOrderMaterials.name2) $('#materialName2').html(workOrderMaterials.name2);
				if(workOrderMaterials.quantity2) $('#materialQuantity2').html(workOrderMaterials.quantity2);
				if(workOrderMaterials.price2) $('#materialPrice2').html(workOrderMaterials.price2);
				if(workOrderMaterials.sum2) $('#materialSum2').html(workOrderMaterials.sum2);
				//3
				if(workOrderMaterials.name3) $('#materialName3').html(workOrderMaterials.name3);
				if(workOrderMaterials.quantity3) $('#materialQuantity3').html(workOrderMaterials.quantity3);
				if(workOrderMaterials.price3) $('#materialPrice3').html(workOrderMaterials.price3);
				if(workOrderMaterials.sum3) $('#materialSum3').html(workOrderMaterials.sum3);
				//4
				if(workOrderMaterials.name4) $('#materialName4').html(workOrderMaterials.name4);
				if(workOrderMaterials.quantity4) $('#materialQuantity4').html(workOrderMaterials.quantity4);
				if(workOrderMaterials.price4) $('#materialPrice4').html(workOrderMaterials.price4);
				if(workOrderMaterials.sum4) $('#materialSum4').html(workOrderMaterials.sum4);
				//5
				if(workOrderMaterials.name5) $('#materialName5').html(workOrderMaterials.name5);
				if(workOrderMaterials.quantity5) $('#materialQuantity5').html(workOrderMaterials.quantity5);
				if(workOrderMaterials.price5) $('#materialPrice5').html(workOrderMaterials.price5);
				if(workOrderMaterials.sum5) $('#materialSum5').html(workOrderMaterials.sum5);
				//6
				if(workOrderMaterials.name6) $('#materialName6').html(workOrderMaterials.name6);
				if(workOrderMaterials.quantity6) $('#materialQuantity6').html(workOrderMaterials.quantity6);
				if(workOrderMaterials.price6) $('#materialPrice6').html(workOrderMaterials.price6);
				if(workOrderMaterials.sum6) $('#materialSum6').html(workOrderMaterials.sum6);
				//7
				if(workOrderMaterials.name7) $('#materialName7').html(workOrderMaterials.name7);
				if(workOrderMaterials.quantity7) $('#materialQuantity7').html(workOrderMaterials.quantity7);
				if(workOrderMaterials.price7) $('#materialPrice7').html(workOrderMaterials.price7);
				if(workOrderMaterials.sum7) $('#materialSum7').html(workOrderMaterials.sum7);
			}
			/*
			for(i = 1; i <= 7; i++){
				//debugger;	
				var element = `<tr>
					<td scope="row">`+(i+1)+`.</td>
					<td>`+workOrderMaterials[i].material_name+`</td>
					<td>`+workOrderMaterials[i].quantity+`</td>
					<td>`+workOrderMaterials[i].price+`</td>
					<td>`+workOrderMaterials[i].sum+`</td>
				</tr>`;
				$('#materialBody').append(element);
			}
			*/
		}
		else{
			console.log('Unsuccessfully getting work order materials');
			//open error modal
		}
	});
	//CONTACTS
	let tmpContact = allContacts.find(c => c.id == tmpWorkOrder.contact_id);
	tmpContact ? $('#workOrderContact').html(tmpContact.full_name + ', ' + tmpContact.phone_number) : $('#workOrderContact').html('Ni kontakta');

	// OFFICE MAIL
	if (!tmpWorkOrder.office_mail){
		$('#btnSendConf').show();
		$('#autoMailNotify').text('Predlagan elektronski naslov za obvestilo o končanem delovnem nalogu:');
		$('#autoMailColaps').collapse('show');
		$('#modalMailHeader').text('Vnesite elek. naslov, kamor želite poslati kopijo delovnega naloga');
		$('#modalSendMail').modal('toggle');
	}
	// // SIGNS
	// $.get( "/workorders/signs",{workOrderId}, function( data ) {
	// 	if(data.success){
	// 		workOrderSigns = data.data;
	// 		debugger
	// 		// if work order has signs then show icon and hide add sign edit button else hide icon and show add sign edit button
	// 		if(workOrderSigns.length > 0){
	// 			$('#signatureIcon').show();
	// 			$('#openSignModalEditBtn').hide();
	// 		}
	// 		else{
	// 			$('#signatureIcon').hide();
	// 			$('#openSignModalEditBtn').show();
	// 		}
	// 		console.log('Successfully getting all the signs of selected work order.');
			
	// 	}
	// 	else{
	// 		console.log('Unsuccessfully getting all the signs of selected work order.');
	// 		//open error modal
	// 	}
	// });
	//get changes to get updated list
	setTimeout(getWorkOrderChanges(),500);
	//getWorkOrderChanges();
	$('#workOrderEdit').collapse('toggle');
	$('#workOrderInfo').collapse('toggle');
}
function drawWorkOrders(){
	$('#workOrderBody').empty();
	for(var i = 0; i < allWorkOrders.length; i++){
		var info = '';
		if(allWorkOrders[i].subscriber_id)
			info = allWorkOrders[i].subscriber_name;
		else if(allWorkOrders[i].project_id)
			info = allWorkOrders[i].project_name;
		var activeClass = 'bg-light';
		if(allWorkOrders[i].active == false)
			activeClass = 'bg-secondary';
		var element = `<div class="card `+activeClass+` card-report mb-3" id="workOrder`+allWorkOrders[i].id+`">
			<div class="card-header">
				<div class="alert-dissmisable">
					<div class="close text-dark" id="workOrderDate`+allWorkOrders[i].id+`">`+allWorkOrders[i].formatted_date.date+`</div>
				</div>
				<div class="row">
					<div class="col-4">
						<div id="workOrderNumber`+allWorkOrders[i].id+`">`+allWorkOrders[i].wo_number+`</div>
					</div>
					<div class="col-4">
						<div id="workOrderInfo`+allWorkOrders[i].id+`">`+info+`</div>
					</div>
					<div class="col-4">
						<div id="workOrderStatus`+allWorkOrders[i].id+`">`+allWorkOrders[i].status+`</div>
					</div>
				</div>
				<div class="row">
					<span class="d-inline-block text-truncate text-muted mx-2 mb-n2" id="workOrderDescription`+allWorkOrders[i].id+`">`+allWorkOrders[i].description+`</span>
				</div>
			</div>
		</div>`;
		$('#workOrderBody').append(element);
		//$('#workOrder' + allWorkOrders[i].id).on('click', {id: allWorkOrders[i].id}, onEventSelectWorkOrder);
	}
	// $('#workOrderList .card.card-report').on('click', function (event) {
	// 	debugger;
	// 	selectWorkOrder(this.id.substring(9));
	// });
}
function getWorkOrders(){
	$.get( "/workorders/get", function( data ) {
		if(data.success)
			allWorkOrders = data.data;
		else
			//open error modal
			console.log('TODO');
	});
}
function addNewWorkOrder(){
	if (!$('#workOrderNewCollapse').hasClass('show')){
		// $('#workOrderNewCollapse').collapse('toggle');
		$('#workOrderNewCollapse').collapse('toggle');
		navigator.geolocation.getCurrentPosition(success, error, options);
	}
	else{
		// $('#workOrderNewCollapse').collapse('toggle');
		// call submit function because collapse is open
		$('#btnAddWorkOrder').click();
	}
}
function closeWorkOrderNew(){
	$('#workOrderNewCollapse').collapse('toggle');
}
function changeRadioOtherNew(){
	//console.log('test menjava na drugo');
	$('#newReportProjectForm').hide();
	$('#newReportOtherForm').show();
	$('#newReportOther').prop('required', true);
}
function changeRadioProjectNew(){
	//console.log('test menjava na projekt');
	$('#newReportProjectForm').show();
	$('#newReportOtherForm').hide();
	$('#newReportOther').prop('required', false);
}
function changeRadioOtherInput(){
	//console.log('test menjava na drugo');
	$('#inputReportProjectForm').hide();
	$('#inputReportOtherForm').show();
}
function changeRadioProjectInput(){
	//console.log('test menjava na drugo');
	$('#inputReportOtherForm').hide();
	$('#inputReportProjectForm').show();
}
function changeRadioOtherEdit(){
	//console.log('test menjava na drugo');
	$('#editReportProjectForm').hide();
	$('#editReportOtherForm').show();
	$('#editReportOther').prop('required', true);
}
function changeRadioProjectEdit(){
	//console.log('test menjava na drugo');
	$('#editReportOtherForm').hide();
	$('#editReportProjectForm').show();
	$('#editReportOther').prop('required', false);
}
function changeNewReportUser(){
	$('#newReportUser').val($('#userSelect').val()).trigger('change');
}
function onEventSelectWorkOrder(event) {
	selectWorkOrder(event.data.id);
}
function selectWorkOrder(id){
	//console.log(id);
	workOrderId = id;
	updatingInfo = true;
	workOrderExpenses = null;
	workOrderMaterials = null;
	tmpWorkOrder = allWorkOrders.find(wo => wo.id == id);
	$('#woDownloadButton').prop('href', "/workorders/pdf/?workOrderId="+id);
	//color header based on active status
	$('#workOrderHeader').removeClass('bg-secondary');
	if(tmpWorkOrder.active == false){
		$('#workOrderHeader').addClass('bg-secondary');
		//$('#woDeleteButton').find('img').attr('src', '/undelete1.png');
		$('#woDeleteButton').find('i').removeClass('fa-trash');
		$('#woDeleteButton').find('i').addClass('fa-trash-restore');
	}
	else{
		//$('#woDeleteButton').find('img').attr('src', '/delete.png');
		$('#woDeleteButton').find('i').removeClass('fa-trash-restore');
		$('#woDeleteButton').find('i').addClass('fa-trash');
	}
	//disable or enabled control buttons
	if(loggedUser.role == 'admin' || loggedUser.role == 'tajnik' || loggedUser.role == 'komercialist' || loggedUser.role == 'komerciala' || loggedUser.role == 'računovodstvo' || loggedUser.role.substring(0,5) == 'vodja'){
		$('#woEditButton').attr('disabled',false);
		$('#woDeleteButton').attr('disabled',false);
	}
	else{
		if(tmpWorkOrder.author){
			$('#woEditButton').attr('disabled',false);
			$('#woDeleteButton').attr('disabled',false);
		}
		else{
			$('#woEditButton').attr('disabled',true);
			$('#woDeleteButton').attr('disabled',true);
		}
	}
	tmpSerialNumber = tmpWorkOrder.id + '/' + tmpWorkOrder.formatted_date.date.split('.')[2];
	$('#workOrderNumber').html(tmpWorkOrder.wo_number);
	$('#workOrderDate').html(tmpWorkOrder.formatted_date.date)
	$('#workOrderType').html(tmpWorkOrder.work_order_type)
	$('#workOrderLocation').html(tmpWorkOrder.location)
	if(tmpWorkOrder.project_id)
		$('#workOrderProject').html(tmpWorkOrder.project_name)
	else
		$('#workOrderProject').html('')
	if(tmpWorkOrder.subscriber_id)
		$('#workOrderSubscriber').html(tmpWorkOrder.subscriber_name)
	else
		$('#workOrderSubscriber').html('')
	$('#workOrderDescription').html(tmpWorkOrder.description)
	$('#workOrderVehicle').html(tmpWorkOrder.vehicle)
	tmpWorkOrder.arrival ? $('#workOrderArrival').html(tmpWorkOrder.formatted_arrival.hours+":"+tmpWorkOrder.formatted_arrival.minutes) : $('#workOrderArrival').html('/');
	tmpWorkOrder.departure ? $('#workOrderDeparture').html(tmpWorkOrder.formatted_departure.hours+":"+tmpWorkOrder.formatted_departure.minutes) : $('#workOrderDeparture').html('/');
	$('#workOrderRepresentative').html(tmpWorkOrder.representative)
	$('#workOrderStatus').val(tmpWorkOrder.status_id).trigger('change');
	if(tmpWorkOrder.location_cord){
		$('#locationLink').show();
		$('#locationLink').attr("href", "http://www.google.si/maps/search/"+tmpWorkOrder.location_cord.x+","+tmpWorkOrder.location_cord.y+"");
		$('#locationIcon').attr('data-original-title', 'X:'+tmpWorkOrder.location_cord.x+',Y:'+tmpWorkOrder.location_cord.y);
		$('[data-toggle="tooltip"]').tooltip();
	}
	else{
		$('#locationLink').hide();
		$('#locationLink').attr("href", "#");
	}
	filteredServices = allServices.filter(s => s.id_subscriber == tmpWorkOrder.subscriber_id);
	$("#workOrderService").empty().select2({ data: filteredServices, });
	$('#workOrderService').val(tmpWorkOrder.task_id).trigger('change');
	if(tmpWorkOrder.task_id){
		if(loggedUser.role == 'admin' || loggedUser.role == 'tajnik' || loggedUser.role == 'komercialist' || loggedUser.role == 'komerciala' || loggedUser.role == 'računovodstvo' || loggedUser.role.substring(0,5) == 'vodja' || loggedUser.role.substring(0,9) == 'programer' || loggedUser.role == 'konstrukter' || loggedUser.role == 'konstruktor')
			$('#serviceLink').attr("href", "/servis/?taskId="+tmpWorkOrder.task_id);
		else
			$('#serviceLink').attr("href", "/employees/tasks?userId="+userId+"&taskId="+tmpWorkOrder.task_id);
	}
	else
		$('#serviceLink').attr("href", "#");
	//$('#workOrderStatus').change(changeWorkOrderStatus);
	tmpUser = allUsers.find(u => u.id == tmpWorkOrder.user_id)
	$('#workOrderUser').html(tmpUser.text)
	$.get( "/workorders/workers",{workOrderId}, function( data ) {
		if(data.success){
			workOrderWorkers = data.data;
			var workers = '';
			for(var i = 0; i < workOrderWorkers.length; i++){
				workers += workOrderWorkers[i].name + ' ' + workOrderWorkers[i].surname;
				if(i+1 != workOrderWorkers.length)
					workers += ', ';
			}
			$('#workOrderWorkers').html(workers);
		}
		else{
			//open error modal
		}
	});
	$.get( "/workorders/expenses",{workOrderId}, function( data ) {
		if(data.success){
			workOrderExpenses = data.data[0];
			if(workOrderExpenses){
				if(workOrderExpenses.time_quantity){
					var time = workOrderExpenses.time_quantity.split(':');
					$('#timeQuantity').html(time[0]+'h'+time[1]+'m');
				}
				else
					$('#timeQuantity').html('');
				if(workOrderExpenses.time_price)
					$('#timePrice').html(workOrderExpenses.time_price/100);
				else
					$('#timePrice').html('');
				if(workOrderExpenses.time_sum)
					$('#timeSum').html(workOrderExpenses.time_sum/100);
				else
					$('#timeSum').html('');
				if(workOrderExpenses.distance_quantity)
					$('#distanceQuantity').html(workOrderExpenses.distance_quantity);
				else
					$('#distanceQuantity').html('');
				if(workOrderExpenses.distance_price)
					$('#distancePrice').html(workOrderExpenses.distance_price/100);
				else
					$('#distancePrice').html('');
				if(workOrderExpenses.distance_sum)
					$('#distanceSum').html(workOrderExpenses.distance_sum/100);
				else
					$('#distanceSum').html('');
				if(workOrderExpenses.material_quantity)
					$('#materialQuantity').html(workOrderExpenses.material_quantity);
				else
					$('#materialQuantity').html('');
				if(workOrderExpenses.material_price)
					$('#materialPrice').html(workOrderExpenses.material_price/100);
				else
					$('#materialPrice').html('');
				if(workOrderExpenses.material_sum)
					$('#materialSum').html(workOrderExpenses.material_sum/100);
				else
					$('#materialSum').html('');
				if(workOrderExpenses.other_quantity)
					$('#otherQuantity').html(workOrderExpenses.other_quantity);
				else
					$('#otherQuantity').html('');
				if(workOrderExpenses.other_price)
					$('#otherPrice').html(workOrderExpenses.other_price/100);
				else
					$('#otherPrice').html('');
				if(workOrderExpenses.other_sum)
					$('#otherSum').html(workOrderExpenses.other_sum/100);
				else
					$('#otherSum').html('');
			}
			else{
				$('#timeQuantity').html('');
				$('#timePrice').html('');
				$('#timeSum').html('');
				$('#distanceQuantity').html('');
				$('#distancePrice').html('');
				$('#distanceSum').html('');
				$('#materialQuantity').html('');
				$('#materialPrice').html('');
				$('#materialSum').html('');
				$('#otherQuantity').html('');
				$('#otherPrice').html('');
				$('#otherSum').html('');
			}
		}
		else{
			console.log('Napaka pri pridobivanju stroškov!');
			//console.log(data.error)
			//open error modal
		}
	});
	$.get( "/workorders/materials",{workOrderId}, function( data ) {
		if(data.success){
			workOrderMaterials = data.data[0];
			//1
			$('#materialName1').html('');
			$('#materialQuantity1').html('');
			$('#materialPrice1').html('');
			$('#materialSum1').html('');
			//2
			$('#materialName2').html('');
			$('#materialQuantity2').html('');
			$('#materialPrice2').html('');
			$('#materialSum2').html('');
			//3
			$('#materialName3').html('');
			$('#materialQuantity3').html('');
			$('#materialPrice3').html('');
			$('#materialSum3').html('');
			//4
			$('#materialName4').html('');
			$('#materialQuantity4').html('');
			$('#materialPrice4').html('');
			$('#materialSum4').html('');
			//5
			$('#materialName5').html('');
			$('#materialQuantity5').html('');
			$('#materialPrice5').html('');
			$('#materialSum5').html('');
			//6
			$('#materialName6').html('');
			$('#materialQuantity6').html('');
			$('#materialPrice6').html('');
			$('#materialSum6').html('');
			//7
			$('#materialName7').html('');
			$('#materialQuantity7').html('');
			$('#materialPrice7').html('');
			$('#materialSum7').html('');
			if(workOrderMaterials){
				//1
				if(workOrderMaterials.name1) $('#materialName1').html(workOrderMaterials.name1);
				if(workOrderMaterials.quantity1) $('#materialQuantity1').html(workOrderMaterials.quantity1);
				if(workOrderMaterials.price1) $('#materialPrice1').html(workOrderMaterials.price1);
				if(workOrderMaterials.sum1) $('#materialSum1').html(workOrderMaterials.sum1);
				//2
				if(workOrderMaterials.name2) $('#materialName2').html(workOrderMaterials.name2);
				if(workOrderMaterials.quantity2) $('#materialQuantity2').html(workOrderMaterials.quantity2);
				if(workOrderMaterials.price2) $('#materialPrice2').html(workOrderMaterials.price2);
				if(workOrderMaterials.sum2) $('#materialSum2').html(workOrderMaterials.sum2);
				//3
				if(workOrderMaterials.name3) $('#materialName3').html(workOrderMaterials.name3);
				if(workOrderMaterials.quantity3) $('#materialQuantity3').html(workOrderMaterials.quantity3);
				if(workOrderMaterials.price3) $('#materialPrice3').html(workOrderMaterials.price3);
				if(workOrderMaterials.sum3) $('#materialSum3').html(workOrderMaterials.sum3);
				//4
				if(workOrderMaterials.name4) $('#materialName4').html(workOrderMaterials.name4);
				if(workOrderMaterials.quantity4) $('#materialQuantity4').html(workOrderMaterials.quantity4);
				if(workOrderMaterials.price4) $('#materialPrice4').html(workOrderMaterials.price4);
				if(workOrderMaterials.sum4) $('#materialSum4').html(workOrderMaterials.sum4);
				//5
				if(workOrderMaterials.name5) $('#materialName5').html(workOrderMaterials.name5);
				if(workOrderMaterials.quantity5) $('#materialQuantity5').html(workOrderMaterials.quantity5);
				if(workOrderMaterials.price5) $('#materialPrice5').html(workOrderMaterials.price5);
				if(workOrderMaterials.sum5) $('#materialSum5').html(workOrderMaterials.sum5);
				//6
				if(workOrderMaterials.name6) $('#materialName6').html(workOrderMaterials.name6);
				if(workOrderMaterials.quantity6) $('#materialQuantity6').html(workOrderMaterials.quantity6);
				if(workOrderMaterials.price6) $('#materialPrice6').html(workOrderMaterials.price6);
				if(workOrderMaterials.sum6) $('#materialSum6').html(workOrderMaterials.sum6);
				//7
				if(workOrderMaterials.name7) $('#materialName7').html(workOrderMaterials.name7);
				if(workOrderMaterials.quantity7) $('#materialQuantity7').html(workOrderMaterials.quantity7);
				if(workOrderMaterials.price7) $('#materialPrice7').html(workOrderMaterials.price7);
				if(workOrderMaterials.sum7) $('#materialSum7').html(workOrderMaterials.sum7);
			}
		}
		else{
			console.log('Unsuccessfully getting work order materials');
			//open error modal
		}
	});
	//SIGNS
	//getSigns();
	$.get( "/workorders/signs",{workOrderId}, function( data ) {
		if(data.success){
			workOrderSigns = data.data;
			debugger
			// if work order has signs then show icon and hide add sign edit button else hide icon and show add sign edit button
			if(workOrderSigns.length > 0){
				$('#signatureIcon').show();
				$('#openSignModalEditBtn').hide();
			}
			else{
				$('#signatureIcon').hide();
				$('#openSignModalEditBtn').show();
			}
			console.log('Successfully getting all the signs of selected work order.');
			
		}
		else{
			console.log('Unsuccessfully getting all the signs of selected work order.');
			//open error modal
		}
	});
	getWorkOrderChanges();
	getWorkOrderFiles();
	$('#workOrderInfo').collapse('toggle');
	$('#workOrderList').collapse('toggle');
	updatingInfo = false;
	//$('#workOrderInfo').show();

	//CONTACTS
	let tmpContact = allContacts.find(c => c.id == tmpWorkOrder.contact_id);
	tmpContact ? $('#workOrderContact').html(tmpContact.full_name + ', ' + tmpContact.phone_number) : $('#workOrderContact').html('Ni kontakta');
}
function closeWorkOrderInfo(){
	$('#workOrderInfo').collapse('toggle');
	$('#workOrderList').collapse('toggle');
	//$('#workOrderInfo').show();
}
function openWorkOrderEdit(){
	tmpWorkOrder = allWorkOrders.find(wo => wo.id == workOrderId);
	$('#woNumberEdit').val(tmpWorkOrder.wo_number)
	var date = tmpWorkOrder.formatted_date.date.split('.');
	$('#woDateEdit').val(date[2]+'-'+date[1]+'-'+date[0]);
	$('#woTypeEdit').val(tmpWorkOrder.work_order_type_id).trigger('change');
	$('#woLocationEdit').val(tmpWorkOrder.location)
	if(tmpWorkOrder.project_id)
		$('#woProjectEdit').val(tmpWorkOrder.project_id).trigger('change');
	else
		$('#woProjectEdit').val(0).trigger('change');
	if(tmpWorkOrder.subscriber_id)
		$('#woSubscriberEdit').val(tmpWorkOrder.subscriber_id).trigger('change');
	else
		$('#woSubscriberEdit').val(0).trigger('change');
	$('#woDescriptionEdit').val(tmpWorkOrder.description)
	$('#woVehicleEdit').val(tmpWorkOrder.vehicle)
	tmpWorkOrder.arrival ? $('#woArrivalEdit').val(tmpWorkOrder.formatted_arrival.hours+":"+tmpWorkOrder.formatted_arrival.minutes) : $('#woArrivalEdit').val('');
	tmpWorkOrder.departure ? $('#woDepartureEdit').val(tmpWorkOrder.formatted_departure.hours+":"+tmpWorkOrder.formatted_departure.minutes) : $('#woDepartureEdit').val('');
	checkEditTime();
	$('#woRepresentativeEdit').val(tmpWorkOrder.representative)
	$('#woWorkerEdit').val(tmpWorkOrder.user_id).trigger('change');
	var workers = [];
	for(var i = 0; i < workOrderWorkers.length; i++){
		workers.push(workOrderWorkers[i].worker_id);
	}
	$('#woWorkersEdit').val(workers).trigger('change')
	//WORK ORDER EXPENSES
	if(workOrderExpenses){
		if(workOrderExpenses.time_quantity)
			$('#woTimeQuantityEdit').val(workOrderExpenses.time_quantity);
		else
			$('#woTimeQuantityEdit').val('');
		if(workOrderExpenses.time_price)
			$('#woTimePriceEdit').val(workOrderExpenses.time_price/100);
		else
			$('#woTimePriceEdit').val('');
		if(workOrderExpenses.time_sum)
			$('#woTimeSumEdit').val(workOrderExpenses.time_sum/100);
		else
			$('#woTimeSumEdit').val('');
		if(workOrderExpenses.distance_quantity)
			$('#woDistanceQuantityEdit').val(workOrderExpenses.distance_quantity);
		else
			$('#woDistanceQuantityEdit').val('');
		if(workOrderExpenses.distance_price)
			$('#woDistancePriceEdit').val(workOrderExpenses.distance_price/100);
		else
			$('#woDistancePriceEdit').val('');
		if(workOrderExpenses.distance_sum)
			$('#woDistanceSumEdit').val(workOrderExpenses.distance_sum/100);
		else
			$('#woDistanceSumEdit').val('');
		if(workOrderExpenses.material_quantity)
			$('#woMaterialQuantityEdit').val(workOrderExpenses.material_quantity);
		else
			$('#woMaterialQuantityEdit').val('');
		if(workOrderExpenses.material_price)
		$('#woMaterialPriceEdit').val(workOrderExpenses.material_price/100);
		else
		$('#woMaterialPriceEdit').val('');
		if(workOrderExpenses.material_sum)
			$('#woMaterialSumEdit').val(workOrderExpenses.material_sum/100);
		else
			$('#woMaterialSumEdit').val('');
		if(workOrderExpenses.other_quantity)
			$('#woOtherQuantityEdit').val(workOrderExpenses.other_quantity);
		else
			$('#woOtherQuantityEdit').val('');
		if(workOrderExpenses.other_price)
			$('#woOtherPriceEdit').val(workOrderExpenses.other_price/100);
		else
			$('#woOtherPriceEdit').val('');
		if(workOrderExpenses.other_sum)
			$('#woOtherSumEdit').val(workOrderExpenses.other_sum/100);
		else
			$('#woOtherSumEdit').val('');
	}
	else{
		$('#woTimeQuantityEdit').val('');
		$('#woTimePriceEdit').val('');
		$('#woTimeSumEdit').val('');
		$('#woDistanceQuantityEdit').val('');
		$('#woDistancePriceEdit').val('');
		$('#woDistanceSumEdit').val('');
		$('#woMaterialQuantityEdit').val('');
		$('#woMaterialPriceEdit').val('');
		$('#woMaterialSumEdit').val('');
		$('#woOtherQuantityEdit').val('');
		$('#woOtherPriceEdit').val('');
		$('#woOtherSumEdit').val('');
	}
	//WORK ORDER MATERIALS EDIT
	//1
	$('#woMaterialNameEdit1').val('');
	$('#woMaterialQuantityEdit1').val('');
	$('#woMaterialPriceEdit1').val('');
	$('#woMaterialSumEdit1').val('');
	//2
	$('#woMaterialNameEdit2').val('');
	$('#woMaterialQuantityEdit2').val('');
	$('#woMaterialPriceEdit2').val('');
	$('#woMaterialSumEdit2').val('');
	//3
	$('#woMaterialNameEdit3').val('');
	$('#woMaterialQuantityEdit3').val('');
	$('#woMaterialPriceEdit3').val('');
	$('#woMaterialSumEdit3').val('');
	//4
	$('#woMaterialNameEdit4').val('');
	$('#woMaterialQuantityEdit4').val('');
	$('#woMaterialPriceEdit4').val('');
	$('#woMaterialSumEdit4').val('');
	//5
	$('#woMaterialNameEdit5').val('');
	$('#woMaterialQuantityEdit5').val('');
	$('#woMaterialPriceEdit5').val('');
	$('#woMaterialSumEdit5').val('');
	//6
	$('#woMaterialNameEdit6').val('');
	$('#woMaterialQuantityEdit6').val('');
	$('#woMaterialPriceEdit6').val('');
	$('#woMaterialSumEdit6').val('');
	//7
	$('#woMaterialNameEdit7').val('');
	$('#woMaterialQuantityEdit7').val('');
	$('#woMaterialPriceEdit7').val('');
	$('#woMaterialSumEdit7').val('');
	if(workOrderMaterials){
		//1
		if(workOrderMaterials.name1) $('#woMaterialNameEdit1').val(workOrderMaterials.name1);
		if(workOrderMaterials.quantity1) $('#woMaterialQuantityEdit1').val(workOrderMaterials.quantity1);
		if(workOrderMaterials.price1) $('#woMaterialPriceEdit1').val(workOrderMaterials.price1);
		if(workOrderMaterials.sum1) $('#woMaterialSumEdit1').val(workOrderMaterials.sum1);
		//2
		if(workOrderMaterials.name2) $('#woMaterialNameEdit2').val(workOrderMaterials.name2);
		if(workOrderMaterials.quantity2) $('#woMaterialQuantityEdit2').val(workOrderMaterials.quantity2);
		if(workOrderMaterials.price2) $('#woMaterialPriceEdit2').val(workOrderMaterials.price2);
		if(workOrderMaterials.sum2) $('#woMaterialSumEdit2').val(workOrderMaterials.sum2);
		//3
		if(workOrderMaterials.name3) $('#woMaterialNameEdit3').val(workOrderMaterials.name3);
		if(workOrderMaterials.quantity3) $('#woMaterialQuantityEdit3').val(workOrderMaterials.quantity3);
		if(workOrderMaterials.price3) $('#woMaterialPriceEdit3').val(workOrderMaterials.price3);
		if(workOrderMaterials.sum3) $('#woMaterialSumEdit3').val(workOrderMaterials.sum3);
		//4
		if(workOrderMaterials.name4) $('#woMaterialNameEdit4').val(workOrderMaterials.name4);
		if(workOrderMaterials.quantity4) $('#woMaterialQuantityEdit4').val(workOrderMaterials.quantity4);
		if(workOrderMaterials.price4) $('#woMaterialPriceEdit4').val(workOrderMaterials.price4);
		if(workOrderMaterials.sum4) $('#woMaterialSumEdit4').val(workOrderMaterials.sum4);
		//5
		if(workOrderMaterials.name5) $('#woMaterialNameEdit5').val(workOrderMaterials.name5);
		if(workOrderMaterials.quantity5) $('#woMaterialQuantityEdit5').val(workOrderMaterials.quantity5);
		if(workOrderMaterials.price5) $('#woMaterialPriceEdit5').val(workOrderMaterials.price5);
		if(workOrderMaterials.sum5) $('#woMaterialSumEdit5').val(workOrderMaterials.sum5);
		//6
		if(workOrderMaterials.name6) $('#woMaterialNameEdit6').val(workOrderMaterials.name6);
		if(workOrderMaterials.quantity6) $('#woMaterialQuantityEdit6').val(workOrderMaterials.quantity6);
		if(workOrderMaterials.price6) $('#woMaterialPriceEdit6').val(workOrderMaterials.price6);
		if(workOrderMaterials.sum6) $('#woMaterialSumEdit6').val(workOrderMaterials.sum6);
		//7
		if(workOrderMaterials.name7) $('#woMaterialNameEdit7').val(workOrderMaterials.name7);
		if(workOrderMaterials.quantity7) $('#woMaterialQuantityEdit7').val(workOrderMaterials.quantity7);
		if(workOrderMaterials.price7) $('#woMaterialPriceEdit7').val(workOrderMaterials.price7);
		if(workOrderMaterials.sum7) $('#woMaterialSumEdit7').val(workOrderMaterials.sum7);
	}
	//open&close collapses
	$('#workOrderInfo').collapse('toggle');
	$('#workOrderEdit').collapse('toggle');
}
function closeWorkOrderEdit(){
	$('#workOrderEdit').collapse('toggle');
	$('#workOrderInfo').collapse('toggle');
}
function openDeleteModal(){
	var tmpWorkOrder = allWorkOrders.find(wo => wo.id == workOrderId);
	if(tmpWorkOrder.active){
		$('#btnDeleteWO').html('Odstrani');
		$('#deleteModalMsg').html('Ste prepričani, da želite odstraniti ta delovni nalog?');
	}
	else{
		$('#btnDeleteWO').html('Ponovno dodaj');
		$('#deleteModalMsg').html('Ste prepričani, da želite ponovno dodati izbrisan delovni nalog?');
	}
	$('#modalDeleteWorkOrder').modal('toggle');
}
function deleteWorkOrder(){
	//console.log("delete work order "+workOrderId);
	var tmpWorkOrder = allWorkOrders.find(wo => wo.id == workOrderId);
	var active = tmpWorkOrder.active;
	if(active == true)
		active = false;
	else
		active = true;
	$.post('/workorders/delete', {workOrderId,active}, function(resp){
		if(resp.success){
			if(active == false){
				console.log('Uspešno odstranitev delovnega naloga.');
				$('#modalDeleteWorkOrder').modal('toggle');
				if(activeUserRole == 'admin'){
					tmpWorkOrder.active = active;
					$('#workOrderHeader').addClass('bg-secondary');
					$('#workOrder'+workOrderId).removeClass('bg-light');
					$('#workOrder'+workOrderId).addClass('bg-secondary');
					$('#woDeleteButton').find('img').attr('src', '/undelete1.png');
				}
				else{
					$('#workOrderInfo').collapse('toggle');
					$('#workOrder'+workOrderId).remove();
					$('#workOrderList').collapse('toggle');
					deleteIndex = allWorkOrders.findIndex(wo => wo.id == workOrderId);
					allWorkOrders.splice(deleteIndex,1);
				}
				//ADD SYSTEM CHANGE FOR EDITING WORK ORDER
				//addSystemChange(18,3,workOrderId);
				addChangeSystem(3,18,null,null,null,null,null,null,null,null,null,workOrderId);
			}
			else{
				console.log('Uspešno ponovno dodajanje izbrisanega delovnega naloga.');
				$('#modalDeleteWorkOrder').modal('toggle');
				//$('#workOrderInfo').collapse('toggle');
				//$('#workOrder'+workOrderId).remove();
				tmpWorkOrder.active = active;
				$('#workOrderHeader').removeClass('bg-secondary');
				$('#workOrder'+workOrderId).removeClass('bg-secondary');
				$('#workOrder'+workOrderId).addClass('bg-light');
				$('#woDeleteButton').find('img').attr('src', '/delete.png');
				//$('#workOrderList').collapse('toggle');
				//deleteIndex = allWorkOrders.findIndex(wo => wo.id == workOrderId);
				//allWorkOrders.splice(deleteIndex,1);
				//ADD SYSTEM CHANGE FOR EDITING WORK ORDER
				//addSystemChange(18,6,workOrderId);
				addChangeSystem(6,18,null,null,null,null,null,null,null,null,null,workOrderId);
			}
			setTimeout(getWorkOrderChanges(),500);
		}
		else
			console.log('Neuspešna odstranitev delovnega naloga!');
			//open error modal
	})
}
function getWorkOrderExpenses(){
	$.get( "/workorders/expenses",{workOrderId}, function( data ) {
		//console.log('Test');
		if(data.success){
			workOrderExpenses = data.data[0];
		}
		else{
			console.log('Napaka pri pridobivanju stroškov!');
			//open error modal
		}
	});
}
function changeWorkOrderStatus(){
	//debugger;
	if(updatingInfo == false){
		//console.log('need to update work order ' + workOrderId + ' with status ' + $('#workOrderStatus').val());
		var tmpStatus = allStatus.find(s => s.id == $('#workOrderStatus').val());
		var statusId = tmpStatus.id;
		$.post('/workorders/status', {workOrderId,statusId}, function(data){
			if(data.success){
				//ADD NEW WORK ORDER CHANGE
				if(statusId == 1)
					//addSystemChange(18,5,workOrderId);
					addChangeSystem(5,18,null,null,null,null,null,null,null,null,null,workOrderId);
				else
					//addSystemChange(18,4,workOrderId);
					addChangeSystem(4,18,null,null,null,null,null,null,null,null,null,workOrderId);
				console.log('Uspešno posodobitev statusa za trenutni delovni nalog.');
				$('#workOrderStatus'+workOrderId).html(tmpStatus.text);
				var tmpWorkOrder = allWorkOrders.find(wo => wo.id == workOrderId);
				tmpWorkOrder.status = tmpStatus.text;
				tmpWorkOrder.status_id = tmpStatus.id;
				setTimeout(getWorkOrderChanges(),500);
			}
			else
				console.log('Neuspešno posodobitev statusa za trenutni delovni nalog!');
				//open error modal
		})
	}
}
function getWorkOrderChanges(){
	$.get( "/workorders/changes",{workOrderId}, function( data ) {
		if(data.success){
			workOrderChanges = data.data;
			console.log('Successfully getting all the changes of selected work order.');
			var element = "";
			var fileLabel = "";
        for(var i=0; i<workOrderChanges.length; i++){
					if(workOrderChanges[i].id_type == 23 || workOrderChanges[i].id_type == 27)
						fileLabel = " datoteko";
					else if(workOrderChanges[i].id_type == 29)
						fileLabel = " podpis";
					else
						fileLabel = "";
          element = new Date(workOrderChanges[i].date).toLocaleDateString('sl') + ' ' + new Date(workOrderChanges[i].date).toLocaleTimeString('sl').substring(0,5) + ", " + workOrderChanges[i].name + " " + workOrderChanges[i].surname + ', ' + workOrderChanges[i].status + fileLabel + "<br />" + element;
        }
				$('.mypopover').attr('data-content', element);
				$('[data-toggle="popover"]').popover({trigger: "hover"});
		}
		else{
			console.log('Unsuccessfully getting all the changes of selected work order.');
			//open error modal
		}
	});
}
function getAllServices(){
	$.get( "/workorders/services", function( data ) {
		if(data.success){
			allServices = data.data;
			//debugger
			//console.log('Successfully getting all services.');
			$(".select2-service").select2({
				data: allServices,
			});
		}
		else{
			console.log('Unsuccessfully getting all services.');
			//open error modal
		}
	});
}
function saveService(clearService){
	if(clearService){
		//taskId = null;
		$('#workOrderService').val(null).trigger('change');
	}
	var taskId = $('#workOrderService').val();
	if(clearService || taskId){
		//
		$.post('/workorders/services', {workOrderId,taskId}, function(data){
			if(data.success){
				console.log('Uspešno posodobitev servisa za trenutni delovni nalog!');
				var tmpWorkOrder = allWorkOrders.find(wo => wo.id == workOrderId);
				tmpWorkOrder.task_id = taskId;
				if(taskId){
					if(loggedUser.role == 'admin' || loggedUser.role == 'tajnik' || loggedUser.role == 'komercialist' || loggedUser.role == 'komerciala' || loggedUser.role == 'računovodstvo' || loggedUser.role.substring(0,5) == 'vodja' || loggedUser.role.substring(0,9) == 'programer' || loggedUser.role == 'konstrukter' || loggedUser.role == 'konstruktor')
						$('#serviceLink').attr("href", "/servis/?taskId="+taskId);
					else
						$('#serviceLink').attr("href", "/employees/tasks?userId="+userId+"&taskId="+taskId);
				}
				else
					$('#serviceLink').attr("href", "#");
			}
			else
				console.log('Neuspešno posodobitev servisa za trenutni delovni nalog!');
				//open error modal
		})
	}
	debugger;
}
function sendMail(){
	savedWorkOrderIdForMail = workOrderId;
	$('#modalSendMail').modal('toggle');
	$('#autoMailColaps').collapse('hide');
}
//check if start and end time of new work order makes sense, otherwise disable adding new work order
function checkNewTime(){
	var start = $('#woArrivalNew').val();
	var finish = $('#woDepartureNew').val();
	if(start && finish){
		if(start > finish){
			$('#woArrivalNew').addClass('is-invalid');
			$('#woDepartureNew').addClass('is-invalid');
			$('#btnAddWorkOrder').prop('disabled', true);
		}
		else{
			$('#woArrivalNew').removeClass('is-invalid');
			$('#woDepartureNew').removeClass('is-invalid');
			$('#btnAddWorkOrder').prop('disabled', false);
		}
	}
	else{
		$('#woArrivalNew').removeClass('is-invalid');
		$('#woDepartureNew').removeClass('is-invalid');
		$('#btnAddWorkOrder').prop('disabled', false);
	}
}
//check if start and end time of edit work order makes sense, otherwise disable editing new work order
function checkEditTime(){
	var start = $('#woArrivalEdit').val();
	var finish = $('#woDepartureEdit').val();
	if(start && finish){
		if(start > finish){
			$('#woArrivalEdit').addClass('is-invalid');
			$('#woDepartureEdit').addClass('is-invalid');
			$('#btnEditWorkOrder').prop('disabled', true);
		}
		else{
			$('#woArrivalEdit').removeClass('is-invalid');
			$('#woDepartureEdit').removeClass('is-invalid');
			$('#btnEditWorkOrder').prop('disabled', false);
		}
	}
	else{
		$('#woArrivalEdit').removeClass('is-invalid');
		$('#woDepartureEdit').removeClass('is-invalid');
		$('#btnEditWorkOrder').prop('disabled', false);
	}
}
// for validating emails
function simpleValidateEmail(email){
	let reg = /\S+@\S+\.\S+/;
	return reg.test(email);
}
// on change/input mail check if all mail are ok
// problem ---> known emails will be numbers, others will be as strings
function filterEmailSelectInput(){
	let selectInputs = [...new Set($('#woEmailSelect').val())];
	let emails = '';
	for (const input of selectInputs) {
		if ( isNaN(input) ){
			if (simpleValidateEmail(input)) emails += input + ',';
			// debugger;
		}
		else{
			let user = allUsers.find(u => u.id == input);
			if ( user && user.work_mail) emails += user.work_mail + ',';
			// debugger;
		}
	}
	// debugger;
	if ( emails.length > 1 )
		return emails.substring(0, emails.length - 1);
	else
		return '';
}
var workOrderId;
var workOrderWorkers;
var workOrderExpenses;
var workOrderMaterials;
var workOrderSigns;
var workOrderChanges;
var allServices, filteredServices;
//var reportId;
//var reportsUserId = 0;
var ctrlChanges;
var userId;
var loggedUser;
var allWorkOrders;
var allUsers;
var allTypes;
var allStatus;
var allProjects;
var allSubscribers;
var allContacts;
//var allReports;
var activeUserRole;
var reverseChanges = false;
var updatingInfo = false;
//parameters for sending correct mail
var savedWorkOrderIdForMail;
var notificationMail = false;
// for filtering select2 project and subscriber and abbility to stop when its already chosen corectly
let newSelectedProject = 0;
let newSelectedSubscriber = 0;