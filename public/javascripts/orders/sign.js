$(function(){
  // canvas and signature pad for new work order
  canvas = document.getElementById('signature-pad');
  signaturePad = new SignaturePad(canvas, {
    //backgroundColor: 'rgb(255, 255, 255)' // necessary for saving image as JPEG; can be removed is only saving as PNG or SVG
  });
  // canvas and signature pad for existing work order and adding new signature while editing selected work order
  editCanvas = document.getElementById('signature-pad-edit');
  editSignaturePad = new SignaturePad(editCanvas, {
    //backgroundColor: 'rgb(255, 255, 255)' // necessary for saving image as JPEG; can be removed is only saving as PNG or SVG
  });
  canvas.width = screen.width-80 < 400  ? screen.width-80: 400;
  editCanvas.width = screen.width-80 < 400  ? screen.width-80: 400;
  ctx = canvas.getContext('2d');
  ctxEdit = editCanvas.getContext('2d');
  // console.log('w:'+ctx.canvas.height+', h:'+ctx.canvas.width);
  // W = canvas.width, H = canvas.height;
  $('#clearSign').on('click', function () {
    if(tmpSignId){
      let oldSignId = parseInt(tmpSignId);
      let data = {signId:tmpSignId};
      $.ajax({
        url: '/workorders/signs/',
        type: 'DELETE',
        data: JSON.stringify(data),
        contentType: 'application/json',
        success: function(res) {
          //console.log(res);
          if (res.success){
            if (tmpSignId == oldSignId){
              //current sign id is still the same, user hasnt yet made new sign, delete sign id
              tmpSignId = undefined;
            }
            signaturePad.clear();
            $('#openSignModalBtn').removeClass('btn-primary btn-danger btn-success').addClass('btn-primary');
            $('#openSignModalBtn').find('i').removeClass('invisible fa-check fa-times').addClass('invisible');
            debugger;
          }
          else{
            debugger;
            $('#msgImg').attr('src','/warning_sign.png');
            $('#msg').html(res.msg);
            $('#modalMsg').modal('toggle');  
          }
        },
        error: function(res) {
          $('#msgImg').attr('src','/warning_sign.png');
          $('#msg').html(res.msg);
          $('#modalMsg').modal('toggle');
        }
      })
    }
    else
      signaturePad.clear();
  });
  // undo last change on signature area (for new work order)
  $('#undoSign').on('click', function () {
    var data = signaturePad.toData();
    if (data) {
      data.pop(); // remove the last dot or line
      signaturePad.fromData(data);
    }
  });
  // save sign on server and save id of sign so when user creates work order, if there is already saved sign on server then delete unused saved sign
  $('#saveSign').on('click', function () {
    if (signaturePad.isEmpty()) {
      return alert("Please provide a signature first.");
    }
    else {
      if (tmpSignId){
        let data = {signId:tmpSignId};
        $.ajax({
          url: '/workorders/signs/',
          type: 'DELETE',
          data: JSON.stringify(data),
          contentType: 'application/json',
          success: function(res) {
            if (res.success){
              //console.log(res);
              tmpSignId = undefined;
              debugger;
            }
            else{
              debugger;
              $('#msgImg').attr('src','/warning_sign.png');
              $('#msg').html(res.msg);
              $('#modalMsg').modal('toggle');  
            }
          },
          error: function(res) {
            $('#msgImg').attr('src','/warning_sign.png');
            $('#msg').html(res.msg);
            $('#modalMsg').modal('toggle');
          }
        })
        var dataURL = signaturePad.toDataURL();
        convertAndSave(dataURL);
        $('#modalSign').modal('toggle');
      }
      else {
        var dataURL = signaturePad.toDataURL();
        convertAndSave(dataURL);
        $('#modalSign').modal('toggle');
      }
    }
  });
  // removes signature from canvas and from server if it was already cerated
  $('#clearSignEdit').on('click', function () {
    if(tmpEditSignId){
      let oldSignId = parseInt(tmpEditSignId);
      let data = {signId:tmpEditSignId};
      $.ajax({
        url: '/workorders/signs/',
        type: 'DELETE',
        data: JSON.stringify(data),
        contentType: 'application/json',
        success: function(res) {
          //console.log(res);
          if (res.success){
            if (tmpEditSignId == oldSignId){
              //current sign id is still the same, user hasnt yet made new sign, delete sign id
              tmpEditSignId = undefined;
            }
            editSignaturePad.clear();
            $('#openSignModalBtn').removeClass('btn-primary btn-danger btn-success').addClass('btn-primary');
            $('#openSignModalBtn').find('i').removeClass('invisible fa-check fa-times').addClass('invisible');
            debugger;
          }
          else{
            debugger;
            $('#msgImg').attr('src','/warning_sign.png');
            $('#msg').html(res.msg);
            $('#modalMsg').modal('toggle');  
          }
        },
        error: function(res) {
          $('#msgImg').attr('src','/warning_sign.png');
          $('#msg').html(res.msg);
          $('#modalMsg').modal('toggle');
        }
      })
    }
    else
      editSignaturePad.clear();
  });
  // undo last change on signature area (for editing work order)
  $('#undoSignEdit').on('click', function () {
    var data = editSignaturePad.toData();
    if (data) {
      data.pop(); // remove the last dot or line
      editSignaturePad.fromData(data);
    }
  });
  // save sign on server and save id of sign so when user updates work order, if there is already saved sign on server then delete unused saved sign
  $('#saveSignEdit').on('click', function () {
    if (editSignaturePad.isEmpty()) {
      return alert("Please provide a signature first.");
    }
    else {
      if (tmpEditSignId){
        let data = {signId:tmpEditSignId};
        $.ajax({
          url: '/workorders/signs/',
          type: 'DELETE',
          data: JSON.stringify(data),
          contentType: 'application/json',
          success: function(res) {
            if (res.success){
              //console.log(res);
              tmpEditSignId = undefined;
              debugger;
            }
            else{
              debugger;
              $('#msgImg').attr('src','/warning_sign.png');
              $('#msg').html(res.msg);
              $('#modalMsg').modal('toggle');  
            }
          },
          error: function(res) {
            $('#msgImg').attr('src','/warning_sign.png');
            $('#msg').html(res.msg);
            $('#modalMsg').modal('toggle');
          }
        })
        var dataURL = editSignaturePad.toDataURL();
        convertAndSaveEdit(dataURL);
        $('#modalSignEdit').modal('toggle');
      }
      else {
        var dataURL = editSignaturePad.toDataURL();
        convertAndSaveEdit(dataURL);
        $('#modalSignEdit').modal('toggle');
      }
    }
  });
  // open modal for adding sign for new work order
  $('#openSignModalBtn').on('click', function () {
    $('#modalSign').modal('toggle');
  })
  // open modal for adding sign for editing work order
  $('#openSignModalEditBtn').on('click', function () {
    $('#modalSignEdit').modal('toggle');
  })
  // listener for chaning orientation on mobile devices so canvas gets correct width
  window.addEventListener("orientationchange", function() {
    // alert(window.orientation);
    // let oldW = canvas.width;
    // let oldH = canvas.height;
    // let newW = screen.width-80 < 400  ? screen.width-80: 400;
    // let newH = canvas.height;
    // let ratioW = oldW/newW;
    // let ratioH = oldH/newH;
    // ctx.scale(ratioW,ratioH);
    let temp = ctx.getImageData(0,0,canvas.width,canvas.height);
    canvas.width = screen.width-80 < 400  ? screen.width-80: 400;
    ctx.putImageData(temp,0,0);
    // // ctx.canvas.width = window.innerWidth - 99;
    // // ctx.canvas.height = window.innerHeight - 99;
    // ctx.canvas.width = screen.width - 80;
    // //ctx.canvas.height = screen.height - 80;
    // //W = canvas.width, H = canvas.height

    let tempEdit = ctxEdit.getImageData(0,0,editCanvas.width,editCanvas.height);
    editCanvas.width = screen.width-80 < 400  ? screen.width-80: 400;
    ctxEdit.putImageData(tempEdit,0,0);
  }, false);
})
// save image and save id of new image for new work order
function convertAndSave(dataURL) {
  var blob = dataURLToBlob(dataURL);
  //var url = window.URL.createObjectURL(blob);

  //var file = blob;
  var formData = new FormData();
  formData.append("file", blob,"sign.png");
  //formData.append("workOrderId", workOrderId);
  //console.log(formData);
  debugger;
  
  $.ajax({
    url: '/workorders/signs/',
    type: 'POST',
    data: formData,
    contentType: false,
    processData: false,
    success: function(res) {
      //console.log(res);
      tmpSignId = res.image.id;
      $('#openSignModalBtn').removeClass('btn-primary btn-danger btn-success').addClass('btn-success');
      $('#openSignModalBtn').find('i').removeClass('invisible fa-check fa-times').addClass('fa-check');
      debugger;
    },
    error: function(res) {
      //console.log('error');
      //$('#btnAddDoc').attr('disabled',false);
      //$('#docNew').val('');
      //$('#modalDocumentAddForm').modal('toggle')
      //prepare modalMsg for info about success
      $('#msgImg').attr('src','/warning_sign.png');
      $('#msg').html(res.responseJSON.message);
      $('#modalMsg').modal('toggle');
      $('#openSignModalBtn').removeClass('btn-primary btn-danger btn-success').addClass('btn-danger');
      $('#openSignModalBtn').find('i').removeClass('invisible fa-check fa-times').addClass('fa-times');
    }
  })
  // if (navigator.userAgent.indexOf("Safari") > -1 && navigator.userAgent.indexOf("Chrome") === -1) {
  //   window.open(dataURL);
  // } else {
  // }
}
// upload image to server and save sign id for editing signature instead of a new work order
function convertAndSaveEdit(dataURL) {
  var blob = dataURLToBlob(dataURL);
  //var url = window.URL.createObjectURL(blob);

  //var file = blob;
  var formData = new FormData();
  formData.append("file", blob,"sign.png");
  //formData.append("workOrderId", workOrderId);
  //console.log(formData);
  debugger;
  
  $.ajax({
    url: '/workorders/signs/',
    type: 'POST',
    data: formData,
    contentType: false,
    processData: false,
    success: function(res) {
      //console.log(res);
      tmpEditSignId = res.image.id;
      tmpEditSign = res.image;
      $('#openSignModalEditBtn').removeClass('btn-primary btn-danger btn-success').addClass('btn-success');
      $('#openSignModalEditBtn').find('i').removeClass('invisible fa-check fa-times').addClass('fa-check');
      debugger;
    },
    error: function(res) {
      //console.log('error');
      //$('#btnAddDoc').attr('disabled',false);
      //$('#docNew').val('');
      //$('#modalDocumentAddForm').modal('toggle')
      //prepare modalMsg for info about success
      $('#msgImg').attr('src','/warning_sign.png');
      $('#msg').html(res.responseJSON.message);
      $('#modalMsg').modal('toggle');
      $('#openSignModalEditBtn').removeClass('btn-primary btn-danger btn-success').addClass('btn-danger');
      $('#openSignModalEditBtn').find('i').removeClass('invisible fa-check fa-times').addClass('fa-times');
    }
  })
}
function dataURLToBlob(dataURL) {
  // Code taken from https://github.com/ebidel/filer.js
  var parts = dataURL.split(';base64,');
  var contentType = parts[0].split(":")[1];
  var raw = window.atob(parts[1]);
  var rawLength = raw.length;
  var uInt8Array = new Uint8Array(rawLength);

  for (var i = 0; i < rawLength; ++i) {
    uInt8Array[i] = raw.charCodeAt(i);
  }

  return new Blob([uInt8Array], { type: contentType });
}
let canvas;
let editCanvas;
let signaturePad;
let editSignaturePad;
let tmpSignId;
let tmpEditSignId;
let tmpEditSign;
let ctx;
let ctxEdit;