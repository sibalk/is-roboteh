//IN THIS SCRIPT//
//// pdf create ////
$(function(){
  // $('#woDownloadButton').on('click', toPDF);
})
//Make PDF if work order can be find in array of work orders
function toPDF(){
  var tmpWorkOrder = allWorkOrders.find(wo => wo.id == workOrderId)
  var sign = '';
  var signElement = [];
  if(workOrderSigns.length > 0){
    var filename = workOrderSigns[0].path_name;
    $.get( "/workorders/sendMeSign",{filename}, function( data ) {
      if(data.success){
        //workOrderSigns = data.data;
        //console.log(data.sign);
        sign = 'data:image/png;base64,' + data.sign;
        signElement = [{
          image: 'podpis',
          absolutePosition: {x: 470, y: 755},
          fit: [100, 60],
        }]
        makePDF(tmpWorkOrder,sign, signElement);
        debugger
        //console.log('Successfully getting base64 for the first sign of selected work order.');
        
      }
      else{
        console.log('Unsuccessfully getting base64 for the first sign of selected work order.');
      }
    }); 
  }
  else
    makePDF(tmpWorkOrder,sign, signElement);
}
function makePDF(tmpWorkOrder, sign, signElement){
  if(tmpWorkOrder){
    //INFO
    var woName = 'DELOVNI NALOG št.: ' + $('#workOrderNumber').html();
    var woSubscriber = '';
    if(tmpWorkOrder.subscriber_id) woSubscriber = tmpWorkOrder.subscriber_name;
    var woProject = ' ';
    if(tmpWorkOrder.project_id) woProject = tmpWorkOrder.project_name;
    var userName = ' ';
    if(tmpWorkOrder.user_id) userName = allUsers.find(u => u.id == tmpWorkOrder.user_id).text;
    //TIP - OZNAČBA Z BARVO
    var typeOne = 'white', typeTwo = 'white', typeThree = 'white', typeFour = 'white', typeFive = 'white', typeSix = 'white', typeSeven = 'white';
    //if(tmpWorkOrder.work_order_type_id)
    switch(tmpWorkOrder.work_order_type_id) {
      case 1: typeOne = 'silver';
        break;
      case 2: typeTwo = 'silver';
        break;
      case 3: typeThree = 'silver';
        break;
      case 4: typeFour = 'silver';
        break;
      case 5: typeFive = 'silver';
        break;
      case 6: typeSix = 'silver';
        break;
      case 7: typeSeven = 'silver';
        break
      default:
        // code block
    }
    //DELAVCI
    var workers = '';
    if(workOrderWorkers && workOrderWorkers.length > 0){
      for(var i = 0; i < workOrderWorkers.length; i++){
        if(i > 0 && i < workOrderWorkers.length)
          workers += ', ';
        workers += workOrderWorkers[i].name + ' ' + workOrderWorkers[i].surname;
      }
    }
    else
      workers = ' ';
    //EXPENSES
    var timeQunatity = '', timePrice = '', timeSum = '', distanceQunatity = '', distancePrice = '', distanceSum = '', materialQuantity = '', materialPrice = '', materialSum = '', otherQuantity = '', otherPrice = '', otherSum = '';
    if(workOrderExpenses){
      if(workOrderExpenses.time_quantity){
        var time = workOrderExpenses.time_quantity.split(':');
        timeQunatity = time[0]+'h '+time[1]+'m';
      }
      if(workOrderExpenses.time_price) timePrice = workOrderExpenses.time_price/100;
      if(workOrderExpenses.time_sum) timeSum = workOrderExpenses.time_sum/100;
      if(workOrderExpenses.distance_quantity) distanceQunatity = workOrderExpenses.distance_quantity;
      if(workOrderExpenses.distance_price) distancePrice = workOrderExpenses.distance_price/100;
      if(workOrderExpenses.distance_sum) distanceSum = workOrderExpenses.distance_sum/100;
      if(workOrderExpenses.material_quantity) materialQuantity = workOrderExpenses.material_quantity;
      if(workOrderExpenses.material_price) materialPrice = workOrderExpenses.material_price/100;
      if(workOrderExpenses.material_sum) materialSum = workOrderExpenses.material_sum/100;
      if(workOrderExpenses.other_quantity) otherQuantity = workOrderExpenses.other_quantity;
      if(workOrderExpenses.other_price) otherPrice = workOrderExpenses.other_price/100;
      if(workOrderExpenses.other_sum) otherSum = workOrderExpenses.other_sum/100;
      debugger
    }
    //MATERIALS
    var name1 = '',name2 = '',name3 = '',name4 = '',name5 = '',name6 = '',name7 = '',quantity1 = '',quantity2 = '',quantity3 = '',quantity4 = '',quantity5 = '',quantity6 = '',quantity7 = '',price1 = '',price2 = '',price3 = '',price4 = '',price5 = '',price6 = '',price7 = '',sum1 = '',sum2 = '',sum3 = '',sum4 = '',sum5 = '',sum6 = '',sum7 = '';
    if(workOrderMaterials){
      if(workOrderMaterials.name1) name1 = workOrderMaterials.name1;
      if(workOrderMaterials.name2) name2 = workOrderMaterials.name2;
      if(workOrderMaterials.name3) name3 = workOrderMaterials.name3;
      if(workOrderMaterials.name4) name4 = workOrderMaterials.name4;
      if(workOrderMaterials.name5) name5 = workOrderMaterials.name5;
      if(workOrderMaterials.name6) name6 = workOrderMaterials.name6;
      if(workOrderMaterials.name7) name7 = workOrderMaterials.name7;
      if(workOrderMaterials.quantity1) quantity1 = workOrderMaterials.quantity1;
      if(workOrderMaterials.quantity2) quantity2 = workOrderMaterials.quantity2;
      if(workOrderMaterials.quantity3) quantity3 = workOrderMaterials.quantity3;
      if(workOrderMaterials.quantity4) quantity4 = workOrderMaterials.quantity4;
      if(workOrderMaterials.quantity5) quantity5 = workOrderMaterials.quantity5;
      if(workOrderMaterials.quantity6) quantity6 = workOrderMaterials.quantity6;
      if(workOrderMaterials.quantity7) quantity7 = workOrderMaterials.quantity7;
      if(workOrderMaterials.price1) price1 = workOrderMaterials.price1;
      if(workOrderMaterials.price2) price2 = workOrderMaterials.price2;
      if(workOrderMaterials.price3) price3 = workOrderMaterials.price3;
      if(workOrderMaterials.price4) price4 = workOrderMaterials.price4;
      if(workOrderMaterials.price5) price5 = workOrderMaterials.price5;
      if(workOrderMaterials.price6) price6 = workOrderMaterials.price6;
      if(workOrderMaterials.price7) price7 = workOrderMaterials.price7;
      if(workOrderMaterials.sum1) sum1 = workOrderMaterials.sum1;
      if(workOrderMaterials.sum2) sum2 = workOrderMaterials.sum2;
      if(workOrderMaterials.sum3) sum3 = workOrderMaterials.sum3;
      if(workOrderMaterials.sum4) sum4 = workOrderMaterials.sum4;
      if(workOrderMaterials.sum5) sum5 = workOrderMaterials.sum5;
      if(workOrderMaterials.sum6) sum6 = workOrderMaterials.sum6;
      if(workOrderMaterials.sum7) sum7 = workOrderMaterials.sum7;
    }
    debugger;
    var docDefinition = {
      info: {
        title: woName,
        author: 'Roboteh-IS',
        subject: 'Delovni nalog',
        keywords: woName + ", " + woSubscriber,
      },
      pageSize: 'A4',
      pageOrientation: 'portrait',
      header: {
        columns: [
          {
            style: 'noga',
            table: {
              widths: [280, 130, 105, 100],
              body: [
                [
                  {
                    //border: [true,false,false,false],
                    text: '',
                    color: 'white',
                    alignment: 'center',
                    fillColor: 'black'
                  },
                  {
                    //border: [false,false,false,true],
                    text: '',
                    color: 'white',
                    alignment: 'center',
                    fillColor: 'black'
                  },
                  {
                    //rowSpan: 2,
                    //border: [false,false,false,false],
                    text: '',
                    preserveLeadingSpaces: true,
                    color: 'white',
                    alignment: 'center',
                    fillColor: 'black'
                  },
                  {
                    //rowSpan: 2,
                    //border: [false,false,false,false],
                    text: ' ',
                    preserveLeadingSpaces: true,
                    color: 'white',
                    alignment: 'center',
                    fillColor: 'black'
                  },
                ],
              ]
            },
          },
        ]
      },
      footer: {
        columns: [
          {
			      style: 'noga',
			      table: {
			        widths: [350,130,45,75],
				      body: [
					      [
					        {
                    text: '*Podpisan izvod velja kot dobavnica',
                    color: 'white',
                    alignment: 'center',
                    fillColor: 'black'
					        },
					        {
                    text: 'Hvala za zaupanje!',
                    color: 'white',
                    alignment: 'center',
                    fillColor: 'black'
                  },
                  {
                    text: '',
                    preserveLeadingSpaces: true,
                    color: 'white',
                    alignment: 'center',
                    fillColor: 'black'
                  },
                  {
                    text: 'Obr.: št. 01/1',
                    preserveLeadingSpaces: true,
                    color: 'white',
                    alignment: 'center',
                    fillColor: 'black'
					        },
				        ],
				      ]
            },
		      },
        ]
      },
      content: [
        signElement,
        {
          style: 'tableRoboteh',
          table: {
            widths: [200],
            body: [
              [
                {
                  image: 'roboteh',
                  width: 200
                },
              ]
            ]
          }, layout: 'noBorders'
        },
        {
          style: 'tableKontaktHead',
          table: {
            widths: [200],
            body: [
              [
                { text: 'ROBOTEH', fontSize: 16, preserveLeadingSpaces: true },
              ],
            ]
          }, layout: 'noBorders'
        },
        {
          style: 'tableKontakt',
          table: {
            widths: [200, 50, 240, 100],
            body: [
              [
                [
                  { text: '                                     D.O.O', style: 'kontakt', preserveLeadingSpaces: true },
                  { text: 'GORIČICA 2B, SI-3230 ŠENTJUR, SLOVENIA-EU', style: 'kontakt', preserveLeadingSpaces: true },
                  { text: 'ID za DDV: SI24773832', style: 'kontakt', preserveLeadingSpaces: true },
                  { text: 'Tel: +386 (0)3 746 42 44', style: 'kontakt' },
                  { text: 'Mobile: +386 51 645 205 ', style: 'kontakt' },
                  { text: 'E-mail: office@roboteh.si', style: 'kontakt' },
                  { text: 'WWW.ROBOTEH.SI', style: 'kontakt' },

                ]
              ],
            ]
          }, layout: 'noBorders'
        },
        {
          style: 'tableKuka',
          table: {
            widths: [200],
            body: [
              [
                {
                  image: 'kuka',
                  width: 50
                },
              ]
            ]
          }, layout: 'noBorders'
        },
        {
          style: 'crta1',
          table: {
            widths: [600],
            body: [[" "], [" "]]
          },
          layout: {
            hLineWidth: function (i, node) {
              return (i === 0 || i === node.table.body.length) ? 0 : 2;
            },
            vLineWidth: function (i, node) {
              return 0;
            },
          }
        },
        {
          style: 'crta2',
          table: {
            widths: [600],
            body: [[" "], [" "]]
          },
          layout: {
            hLineWidth: function (i, node) {
              return (i === 0 || i === node.table.body.length) ? 0 : 2;
            },
            vLineWidth: function (i, node) {
              return 0;
            },
          }
        },
        {
          style: 'crta3',
          table: {
            widths: [600],
            body: [[" "], [" "]]
          },
          layout: {
            hLineWidth: function (i, node) {
              return (i === 0 || i === node.table.body.length) ? 0 : 2;
            },
            vLineWidth: function (i, node) {
              return 0;
            },
          }
        },
        {
          style: 'header',
          table: {
            body: [
              [{ text: 'DELOVNI NALOG', preserveLeadingSpaces: true },]
            ]
          },
          layout: 'noBorders'
        },
        {
          style: 'infotable',
          table: {
            widths: [16, 90, 100, 37, 92],
            body: [
              [
                {
                  rowSpan: 2,
                  border: [false, false, false, false],
                  text: 'št.: ',
                },
                {
                  border: [false, false, false, true],
                  text: $('#workOrderNumber').html(),
                  preserveLeadingSpaces: true

                },
                {
                  rowSpan: 2,
                  border: [false, false, false, false],
                  text: ' ',
                  preserveLeadingSpaces: true

                },
                {
                  border: [false, false, false, false],
                  text: 'Datum: '

                },
                {
                  border: [false, false, false, true],
                  text: tmpWorkOrder.formatted_date.date

                }
              ],
              [
                {
                  border: [false, false, false, false],
                  text: '    1 = servis,   2 = programiranje,    3 = montaža,    4 = zagon,    5 = storitev,   6 = svetovanje,   7 = pomoč',
                  preserveLeadingSpaces: true
                },
                {
                  border: [false, false, false, false],
                  text: '',
                  preserveLeadingSpaces: true
                },
                {
                  rowSpan: 2,
                  border: [false, false, false, false],
                  text: ' št.:',
                  preserveLeadingSpaces: true

                },
                {
                  border: [false, false, false, false],
                  text: 'Kraj:'

                },
                {
                  border: [false, false, false, true],
                  text: tmpWorkOrder.location

                }
              ]
            ]
          },
          layout: {
            defaultBorder: true,
          }
        },
        {
          style: 'typestable',
          table: {
            widths: [50, 90, 66, 60, 65, 70, 65],
            body: [
              [
                {
                  border: [true, true, false, true],
                  text: '1 = servis,',
                  alignment: 'center',
                  preserveLeadingSpaces: true,
                  fillColor: typeOne
                },
                {
                  border: [false, true, false, true],
                  text: '2 = programiranje,',
                  alignment: 'center',
                  preserveLeadingSpaces: true,
                  fillColor: typeTwo
                },
                {
                  border: [false, true, false, true],
                  text: '3 = montaža,',
                  alignment: 'center',
                  preserveLeadingSpaces: true,
                  fillColor: typeThree
                },
                {
                  border: [false, true, false, true],
                  text: '4 = zagon,',
                  alignment: 'center',
                  preserveLeadingSpaces: true,
                  fillColor: typeFour
                },
                {
                  border: [false, true, false, true],
                  text: '5 = storitev,',
                  alignment: 'center',
                  preserveLeadingSpaces: true,
                  fillColor: typeFive
                },
                {
                  border: [false, true, false, true],
                  text: '6 = svetovanje,',
                  alignment: 'center',
                  preserveLeadingSpaces: true,
                  fillColor: typeSix
                },
                {
                  border: [false, true, true, true],
                  text: '7 = pomoč',
                  alignment: 'center',
                  preserveLeadingSpaces: true,
                  fillColor: typeSeven
                },
              ]
            ]
          },
          layout: {
            defaultBorder: false,
          }
        },
        {
          style: 'zacetek',
          table: {
            widths: [520],
            body: [
              [
                {
                  border: [true, true, true, false],
                  text: ' ',
                },
              ],
            ]
          },
          layout: {
            defaultBorder: true,
          }
        },
        {
          style: 'subproject',
          table: {
            widths: [58, 370, 74],
            body: [
              [
                {
                  border: [true, false, false, false],
                  text: 'NAROČNIK: ',
                },
                {
                  border: [false, false, false, true],
                  text: woSubscriber,
                  preserveLeadingSpaces: true

                },
                {
                  rowSpan: 2,
                  border: [false, false, true, false],
                  text: ' ',
                  preserveLeadingSpaces: true

                },
              ],
              [
                {
                  border: [true, false, false, false],
                  text: 'PROJEKT:',
                  preserveLeadingSpaces: true
                },
                {
                  border: [false, false, false, true],
                  text: woProject,
                  preserveLeadingSpaces: true
                },
                {
                  rowSpan: 2,
                  border: [false, false, false, false],
                  text: ' ',
                  preserveLeadingSpaces: true

                },
              ]
            ]
          },
          layout: {
            defaultBorder: true,
          }
        },
        {
          style: 'description',
          table: {
            widths: [141, 370, 2],
            body: [
              [
                {
                  border: [true, false, false, false],
                  text: 'OPIS OPRAVLJENEGA DELA: ',
                },
                {
                  border: [false, false, true, false],
                  text: '',
                  preserveLeadingSpaces: true

                },
                {
                  border: [false, false, false, false],
                  text: ' ',
                  preserveLeadingSpaces: true

                },
              ],
              [
                {
                  colSpan: 2,
                  rowSpan: 6,
                  border: [true, false, true, false],
                  text: tmpWorkOrder.description,
                  preserveLeadingSpaces: true
                },
                {
                  text: ' ',
                  preserveLeadingSpaces: true
                },
                {
                  border: [false, false, false, false],
                  text: ' ',
                  preserveLeadingSpaces: true

                },
              ],
              [
                {
                  colSpan: 2,
                  text: ' ',
                  preserveLeadingSpaces: true
                },
                {
                  text: ' ',
                  preserveLeadingSpaces: true
                },
                {
                  border: [false, false, false, false],
                  text: ' ',
                  preserveLeadingSpaces: true

                },
              ],
              [
                {
                  colSpan: 2,
                  text: ' ',
                  preserveLeadingSpaces: true
                },
                {
                  text: ' ',
                  preserveLeadingSpaces: true
                },
                {
                  border: [false, false, false, false],
                  text: ' ',
                  preserveLeadingSpaces: true

                },
              ],
              [
                {
                  colSpan: 2,
                  text: ' ',
                  preserveLeadingSpaces: true
                },
                {
                  text: ' ',
                  preserveLeadingSpaces: true
                },
                {
                  border: [false, false, false, false],
                  text: ' ',
                  preserveLeadingSpaces: true

                },
              ],
              [
                {
                  colSpan: 2,
                  text: ' ',
                  preserveLeadingSpaces: true
                },
                {
                  text: ' ',
                  preserveLeadingSpaces: true
                },
                {
                  border: [false, false, false, false],
                  text: ' ',
                  preserveLeadingSpaces: true

                },
              ],
              [
                {
                  colSpan: 2,
                  text: ' ',
                  preserveLeadingSpaces: true
                },
                {
                  text: ' ',
                  preserveLeadingSpaces: true
                },
                {
                  border: [false, false, false, false],
                  text: ' ',
                  preserveLeadingSpaces: true

                },
              ]
            ]
          },
        },
        {
          style: 'workers',
          table: {
            widths: [74, 306, 122],
            body: [
              [
                {
                  border: [true, false, false, false],
                  text: 'Izvajalec naloge: ',
                },
                {
                  border: [false, false, false, true],
                  text: workers,
                  preserveLeadingSpaces: true

                },
                {
                  rowSpan: 2,
                  border: [false, false, true, false],
                  text: ' ',
                  preserveLeadingSpaces: true

                },
              ],
              [
                {
                  border: [true, false, false, false],
                  text: 'Podatki o vozilu:',
                  preserveLeadingSpaces: true
                },
                {
                  border: [false, false, false, true],
                  text: tmpWorkOrder.vehicle,
                  preserveLeadingSpaces: true
                },
                {
                  rowSpan: 2,
                  border: [false, false, false, false],
                  text: ' ',
                  preserveLeadingSpaces: true

                },
              ]
            ]
          },
          layout: {
            defaultBorder: true,
          }
        },
        {
          style: 'time',
          table: {
            widths: [56, 80, 20, 56, 80, 183],
            body: [
              [
                {
                  border: [true, false, false, false],
                  text: 'Čas prihoda: ',
                },
                {
                  border: [false, false, false, true],
                  text: tmpWorkOrder.formatted_arrival.hours+':'+tmpWorkOrder.formatted_arrival.minutes,
                  preserveLeadingSpaces: true

                },
                {
                  border: [false, false, false, false],
                  text: ' ',
                  preserveLeadingSpaces: true

                },
                {
                  border: [false, false, false, false],
                  text: 'Čas odhoda: ',
                },
                {
                  border: [false, false, false, true],
                  text: tmpWorkOrder.formatted_departure.hours+':'+tmpWorkOrder.formatted_departure.minutes,
                  preserveLeadingSpaces: true

                },
                {
                  border: [false, false, true, false],
                  text: ' ',
                  preserveLeadingSpaces: true

                },
              ],
            ]
          },
          layout: {
            defaultBorder: true,
          }
        },
        {
          style: 'konec',
          table: {
            widths: [520],
            body: [
              [
                {
                  border: [true, false, true, true],
                  text: ' ',
                },
              ],
            ]
          },
          layout: {
            defaultBorder: true,
          }
        },
        {
          style: 'stroskiheader',
          table: {
            widths: [333],
            body: [
              [
                {
                  text: 'STROŠKI DELA, PREVOZ, OSTALO: ',
                  fillColor: 'silver'
                },
              ],
            ]
          },
        },
        {
          style: 'stroskiheaderPlus',
          table: {
            widths: [50, 50, 60],
            body: [
              [
                {
                  text: 'Količina',
                  alignment: 'center',
                  fillColor: 'silver'
                },
                {
                  text: 'Cena',
                  alignment: 'center',
                  fillColor: 'silver'
                },
                {
                  text: 'Znesek',
                  alignment: 'center',
                  fillColor: 'silver'
                },
              ],
            ]
          },
        },
        {
          style: 'stroski',
          table: {
            widths: [333, 50, 50, 60],
            body: [
              [
                {
                  text: 'Porabljen čas dela (h):',
                },
                {
                  text: timeQunatity,
                  alignment: 'center',
                  preserveLeadingSpaces: true

                },
                {
                  text: timePrice,
                  alignment: 'center',
                  preserveLeadingSpaces: true

                },
                {
                  text: timeSum,
                  alignment: 'center',
                  preserveLeadingSpaces: true

                },
              ],
              [
                {
                  text: 'Prevoženi kilometri - relacija (km):',
                  preserveLeadingSpaces: true
                },
                {
                  text: distanceQunatity,
                  alignment: 'center',
                  preserveLeadingSpaces: true
                },
                {
                  text: distancePrice,
                  alignment: 'center',
                  preserveLeadingSpaces: true

                },
                {
                  text: distanceSum,
                  alignment: 'center',
                  preserveLeadingSpaces: true

                },
              ],
              [
                {
                  text: 'Materialni stroški:',
                  preserveLeadingSpaces: true
                },
                {
                  text: materialQuantity,
                  alignment: 'center',
                  preserveLeadingSpaces: true
                },
                {
                  text: materialPrice,
                  alignment: 'center',
                  preserveLeadingSpaces: true

                },
                {
                  text: materialSum,
                  alignment: 'center',
                  preserveLeadingSpaces: true

                },
              ],
              [
                {
                  text: 'Ostalo:',
                  preserveLeadingSpaces: true
                },
                {
                  text: otherQuantity,
                  alignment: 'center',
                  preserveLeadingSpaces: true
                },
                {
                  text: otherPrice,
                  alignment: 'center',
                  preserveLeadingSpaces: true

                },
                {
                  text: otherSum,
                  alignment: 'center',
                  preserveLeadingSpaces: true

                },
              ]
            ]
          },
          layout: {
            defaultBorder: true,
          }
        },
        {
          style: 'materialheader',
          table: {
            widths: [520],
            body: [
              [
                {
                  text: 'MATERIAL: ',
                  fillColor: 'silver'
                },
              ],
            ]
          },
        },
        {
          style: 'material',
          table: {
            widths: [34, 290, 50, 50, 60],
            body: [
              [
                {
                  text: 'Zap. št.',
                },
                {
                  text: 'Naziv materiala',
                  preserveLeadingSpaces: true

                },
                {
                  text: 'Količina',
                  alignment: 'center',
                  preserveLeadingSpaces: true

                },
                {
                  text: 'Cena',
                  alignment: 'center',
                  preserveLeadingSpaces: true

                },
                {
                  text: 'Znesek',
                  alignment: 'center',
                  preserveLeadingSpaces: true

                },
              ],
              [
                {
                  text: '1.',
                  preserveLeadingSpaces: true
                },
                {
                  text: name1,
                  preserveLeadingSpaces: true
                },
                {
                  text: quantity1,
                  preserveLeadingSpaces: true

                },
                {
                  text: price1,
                  preserveLeadingSpaces: true

                },
                {
                  text: sum1,
                  preserveLeadingSpaces: true

                },
              ],
              [
                {
                  text: '2.',
                  preserveLeadingSpaces: true
                },
                {
                  text: name2,
                  preserveLeadingSpaces: true
                },
                {
                  text: quantity2,
                  preserveLeadingSpaces: true

                },
                {
                  text: price2,
                  preserveLeadingSpaces: true

                },
                {
                  text: sum2,
                  preserveLeadingSpaces: true

                },
              ],
              [
                {
                  text: '3.',
                  preserveLeadingSpaces: true
                },
                {
                  text: name3,
                  preserveLeadingSpaces: true
                },
                {
                  text: quantity3,
                  preserveLeadingSpaces: true

                },
                {
                  text: price3,
                  preserveLeadingSpaces: true

                },
                {
                  text: sum3,
                  preserveLeadingSpaces: true

                },
              ],
              [
                {
                  text: '4.',
                  preserveLeadingSpaces: true
                },
                {
                  text: name4,
                  preserveLeadingSpaces: true
                },
                {
                  text: quantity4,
                  preserveLeadingSpaces: true

                },
                {
                  text: price4,
                  preserveLeadingSpaces: true

                },
                {
                  text: sum4,
                  preserveLeadingSpaces: true

                },
              ],
              [
                {
                  text: '5.',
                  preserveLeadingSpaces: true
                },
                {
                  text: name5,
                  preserveLeadingSpaces: true
                },
                {
                  text: quantity5,
                  preserveLeadingSpaces: true

                },
                {
                  text: price5,
                  preserveLeadingSpaces: true

                },
                {
                  text: sum5,
                  preserveLeadingSpaces: true

                },
              ],
              [
                {
                  text: '6.',
                  preserveLeadingSpaces: true
                },
                {
                  text: name6,
                  preserveLeadingSpaces: true
                },
                {
                  text: quantity6,
                  preserveLeadingSpaces: true

                },
                {
                  text: price6,
                  preserveLeadingSpaces: true

                },
                {
                  text: sum6,
                  preserveLeadingSpaces: true

                },
              ],
              [
                {
                  text: '7.',
                  preserveLeadingSpaces: true
                },
                {
                  text: name7,
                  preserveLeadingSpaces: true
                },
                {
                  text: quantity7,
                  preserveLeadingSpaces: true

                },
                {
                  text: price7,
                  preserveLeadingSpaces: true

                },
                {
                  text: sum7,
                  preserveLeadingSpaces: true

                },
              ]
            ]
          },
          layout: {
            defaultBorder: true,
          }
        },
        {
          style: 'podpis',
          table: {
            widths: [81, 110, 35, 137, 110, 20],
            heights: ['auto', 40],
            body: [
              [
                {
                  border: [false, false, false, false],
                  text: 'Izvajalec naloge:'
                },
                {
                  border: [false, false, false, true],
                  text: userName,
                  preserveLeadingSpaces: true

                },
                {
                  border: [false, false, false, false],
                  text: ' ',
                  preserveLeadingSpaces: true

                },
                {
                  border: [false, false, false, false],
                  text: 'Pooblaščenec za naročnika:',
                },
                {
                  border: [false, false, false, true],
                  text: tmpWorkOrder.representative,
                  preserveLeadingSpaces: true

                },
                {
                  border: [false, false, false, false],
                  text: ' ',
                  preserveLeadingSpaces: true

                },
              ],
              [
                {
                  border: [false, false, false, false],
                  text: '',
                },
                {
                  border: [false, false, false, true],
                  text: '',
                  preserveLeadingSpaces: true

                },
                {
                  border: [false, false, false, false],
                  text: ' ',
                  preserveLeadingSpaces: true

                },
                {
                  border: [false, false, false, false],
                  text: '',
                },
                {
                  border: [false, false, false, true],
                  text: '',
                  preserveLeadingSpaces: true

                },
                {
                  border: [false, false, false, false],
                  text: ' ',
                  preserveLeadingSpaces: true

                },
              ],
            ]
          },
          layout: {
            defaultBorder: true,
          }
        },
      ],
      images: {
        podpis: sign,
        roboteh: 'data:image/jpeg;base64,/9j/4AAQSkZJRgABAQEASABIAAD/4QDARXhpZgAATU0AKgAAAAgABgEaAAUAAAABAAAAVgEbAAUAAAABAAAAXgEoAAMAAAABAAIAAAExAAIAAAARAAAAZgEyAAIAAAAUAAAAeIdpAAQAAAABAAAAjAAAAAAAAABIAAAAAQAAAEgAAAABcGFpbnQubmV0IDQuMi4xMAAAMjAxMzowMzoyMCAwOToyNDowNwAAA6ABAAMAAAABAAEAAKACAAQAAAABAAAF4aADAAQAAAABAAABwgAAAAAAAP/iDFhJQ0NfUFJPRklMRQABAQAADEhMaW5vAhAAAG1udHJSR0IgWFlaIAfOAAIACQAGADEAAGFjc3BNU0ZUAAAAAElFQyBzUkdCAAAAAAAAAAAAAAABAAD21gABAAAAANMtSFAgIAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAEWNwcnQAAAFQAAAAM2Rlc2MAAAGEAAAAbHd0cHQAAAHwAAAAFGJrcHQAAAIEAAAAFHJYWVoAAAIYAAAAFGdYWVoAAAIsAAAAFGJYWVoAAAJAAAAAFGRtbmQAAAJUAAAAcGRtZGQAAALEAAAAiHZ1ZWQAAANMAAAAhnZpZXcAAAPUAAAAJGx1bWkAAAP4AAAAFG1lYXMAAAQMAAAAJHRlY2gAAAQwAAAADHJUUkMAAAQ8AAAIDGdUUkMAAAQ8AAAIDGJUUkMAAAQ8AAAIDHRleHQAAAAAQ29weXJpZ2h0IChjKSAxOTk4IEhld2xldHQtUGFja2FyZCBDb21wYW55AABkZXNjAAAAAAAAABJzUkdCIElFQzYxOTY2LTIuMQAAAAAAAAAAAAAAEnNSR0IgSUVDNjE5NjYtMi4xAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAABYWVogAAAAAAAA81EAAQAAAAEWzFhZWiAAAAAAAAAAAAAAAAAAAAAAWFlaIAAAAAAAAG+iAAA49QAAA5BYWVogAAAAAAAAYpkAALeFAAAY2lhZWiAAAAAAAAAkoAAAD4QAALbPZGVzYwAAAAAAAAAWSUVDIGh0dHA6Ly93d3cuaWVjLmNoAAAAAAAAAAAAAAAWSUVDIGh0dHA6Ly93d3cuaWVjLmNoAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAGRlc2MAAAAAAAAALklFQyA2MTk2Ni0yLjEgRGVmYXVsdCBSR0IgY29sb3VyIHNwYWNlIC0gc1JHQgAAAAAAAAAAAAAALklFQyA2MTk2Ni0yLjEgRGVmYXVsdCBSR0IgY29sb3VyIHNwYWNlIC0gc1JHQgAAAAAAAAAAAAAAAAAAAAAAAAAAAABkZXNjAAAAAAAAACxSZWZlcmVuY2UgVmlld2luZyBDb25kaXRpb24gaW4gSUVDNjE5NjYtMi4xAAAAAAAAAAAAAAAsUmVmZXJlbmNlIFZpZXdpbmcgQ29uZGl0aW9uIGluIElFQzYxOTY2LTIuMQAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAdmlldwAAAAAAE6T+ABRfLgAQzxQAA+3MAAQTCwADXJ4AAAABWFlaIAAAAAAATAlWAFAAAABXH+dtZWFzAAAAAAAAAAEAAAAAAAAAAAAAAAAAAAAAAAACjwAAAAJzaWcgAAAAAENSVCBjdXJ2AAAAAAAABAAAAAAFAAoADwAUABkAHgAjACgALQAyADcAOwBAAEUASgBPAFQAWQBeAGMAaABtAHIAdwB8AIEAhgCLAJAAlQCaAJ8ApACpAK4AsgC3ALwAwQDGAMsA0ADVANsA4ADlAOsA8AD2APsBAQEHAQ0BEwEZAR8BJQErATIBOAE+AUUBTAFSAVkBYAFnAW4BdQF8AYMBiwGSAZoBoQGpAbEBuQHBAckB0QHZAeEB6QHyAfoCAwIMAhQCHQImAi8COAJBAksCVAJdAmcCcQJ6AoQCjgKYAqICrAK2AsECywLVAuAC6wL1AwADCwMWAyEDLQM4A0MDTwNaA2YDcgN+A4oDlgOiA64DugPHA9MD4APsA/kEBgQTBCAELQQ7BEgEVQRjBHEEfgSMBJoEqAS2BMQE0wThBPAE/gUNBRwFKwU6BUkFWAVnBXcFhgWWBaYFtQXFBdUF5QX2BgYGFgYnBjcGSAZZBmoGewaMBp0GrwbABtEG4wb1BwcHGQcrBz0HTwdhB3QHhgeZB6wHvwfSB+UH+AgLCB8IMghGCFoIbgiCCJYIqgi+CNII5wj7CRAJJQk6CU8JZAl5CY8JpAm6Cc8J5Qn7ChEKJwo9ClQKagqBCpgKrgrFCtwK8wsLCyILOQtRC2kLgAuYC7ALyAvhC/kMEgwqDEMMXAx1DI4MpwzADNkM8w0NDSYNQA1aDXQNjg2pDcMN3g34DhMOLg5JDmQOfw6bDrYO0g7uDwkPJQ9BD14Peg+WD7MPzw/sEAkQJhBDEGEQfhCbELkQ1xD1ERMRMRFPEW0RjBGqEckR6BIHEiYSRRJkEoQSoxLDEuMTAxMjE0MTYxODE6QTxRPlFAYUJxRJFGoUixStFM4U8BUSFTQVVhV4FZsVvRXgFgMWJhZJFmwWjxayFtYW+hcdF0EXZReJF64X0hf3GBsYQBhlGIoYrxjVGPoZIBlFGWsZkRm3Gd0aBBoqGlEadxqeGsUa7BsUGzsbYxuKG7Ib2hwCHCocUhx7HKMczBz1HR4dRx1wHZkdwx3sHhYeQB5qHpQevh7pHxMfPh9pH5Qfvx/qIBUgQSBsIJggxCDwIRwhSCF1IaEhziH7IiciVSKCIq8i3SMKIzgjZiOUI8Ij8CQfJE0kfCSrJNolCSU4JWgllyXHJfcmJyZXJocmtyboJxgnSSd6J6sn3CgNKD8ocSiiKNQpBik4KWspnSnQKgIqNSpoKpsqzysCKzYraSudK9EsBSw5LG4soizXLQwtQS12Last4S4WLkwugi63Lu4vJC9aL5Evxy/+MDUwbDCkMNsxEjFKMYIxujHyMioyYzKbMtQzDTNGM38zuDPxNCs0ZTSeNNg1EzVNNYc1wjX9Njc2cjauNuk3JDdgN5w31zgUOFA4jDjIOQU5Qjl/Obw5+To2OnQ6sjrvOy07azuqO+g8JzxlPKQ84z0iPWE9oT3gPiA+YD6gPuA/IT9hP6I/4kAjQGRApkDnQSlBakGsQe5CMEJyQrVC90M6Q31DwEQDREdEikTORRJFVUWaRd5GIkZnRqtG8Ec1R3tHwEgFSEtIkUjXSR1JY0mpSfBKN0p9SsRLDEtTS5pL4kwqTHJMuk0CTUpNk03cTiVObk63TwBPSU+TT91QJ1BxULtRBlFQUZtR5lIxUnxSx1MTU19TqlP2VEJUj1TbVShVdVXCVg9WXFapVvdXRFeSV+BYL1h9WMtZGllpWbhaB1pWWqZa9VtFW5Vb5Vw1XIZc1l0nXXhdyV4aXmxevV8PX2Ffs2AFYFdgqmD8YU9homH1YklinGLwY0Njl2PrZEBklGTpZT1lkmXnZj1mkmboZz1nk2fpaD9olmjsaUNpmmnxakhqn2r3a09rp2v/bFdsr20IbWBtuW4SbmtuxG8eb3hv0XArcIZw4HE6cZVx8HJLcqZzAXNdc7h0FHRwdMx1KHWFdeF2Pnabdvh3VnezeBF4bnjMeSp5iXnnekZ6pXsEe2N7wnwhfIF84X1BfaF+AX5ifsJ/I3+Ef+WAR4CogQqBa4HNgjCCkoL0g1eDuoQdhICE44VHhauGDoZyhteHO4efiASIaYjOiTOJmYn+imSKyoswi5aL/IxjjMqNMY2Yjf+OZo7OjzaPnpAGkG6Q1pE/kaiSEZJ6kuOTTZO2lCCUipT0lV+VyZY0lp+XCpd1l+CYTJi4mSSZkJn8mmia1ZtCm6+cHJyJnPedZJ3SnkCerp8dn4uf+qBpoNihR6G2oiailqMGo3aj5qRWpMelOKWpphqmi6b9p26n4KhSqMSpN6mpqhyqj6sCq3Wr6axcrNCtRK24ri2uoa8Wr4uwALB1sOqxYLHWskuywrM4s660JbSctRO1irYBtnm28Ldot+C4WbjRuUq5wro7urW7LrunvCG8m70VvY++Cr6Evv+/er/1wHDA7MFnwePCX8Lbw1jD1MRRxM7FS8XIxkbGw8dBx7/IPci8yTrJuco4yrfLNsu2zDXMtc01zbXONs62zzfPuNA50LrRPNG+0j/SwdNE08bUSdTL1U7V0dZV1tjXXNfg2GTY6Nls2fHadtr724DcBdyK3RDdlt4c3qLfKd+v4DbgveFE4cziU+Lb42Pj6+Rz5PzlhOYN5pbnH+ep6DLovOlG6dDqW+rl63Dr++yG7RHtnO4o7rTvQO/M8Fjw5fFy8f/yjPMZ86f0NPTC9VD13vZt9vv3ivgZ+Kj5OPnH+lf65/t3/Af8mP0p/br+S/7c/23////bAEMAAgEBAQEBAgEBAQICAgICBAMCAgICBQQEAwQGBQYGBgUGBgYHCQgGBwkHBgYICwgJCgoKCgoGCAsMCwoMCQoKCv/bAEMBAgICAgICBQMDBQoHBgcKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCv/AABEIAcIF4QMBIQACEQEDEQH/xAAfAAABBQEBAQEBAQAAAAAAAAAAAQIDBAUGBwgJCgv/xAC1EAACAQMDAgQDBQUEBAAAAX0BAgMABBEFEiExQQYTUWEHInEUMoGRoQgjQrHBFVLR8CQzYnKCCQoWFxgZGiUmJygpKjQ1Njc4OTpDREVGR0hJSlNUVVZXWFlaY2RlZmdoaWpzdHV2d3h5eoOEhYaHiImKkpOUlZaXmJmaoqOkpaanqKmqsrO0tba3uLm6wsPExcbHyMnK0tPU1dbX2Nna4eLj5OXm5+jp6vHy8/T19vf4+fr/xAAfAQADAQEBAQEBAQEBAAAAAAAAAQIDBAUGBwgJCgv/xAC1EQACAQIEBAMEBwUEBAABAncAAQIDEQQFITEGEkFRB2FxEyIygQgUQpGhscEJIzNS8BVictEKFiQ04SXxFxgZGiYnKCkqNTY3ODk6Q0RFRkdISUpTVFVWV1hZWmNkZWZnaGlqc3R1dnd4eXqCg4SFhoeIiYqSk5SVlpeYmZqio6Slpqeoqaqys7S1tre4ubrCw8TFxsfIycrS09TV1tfY2dri4+Tl5ufo6ery8/T19vf4+fr/2gAMAwEAAhEDEQA/AP38ooA8G/b3/wCClf7HX/BNX4Yj4nftZfFm10VbpJP7D8P2i/aNV1mRAMx2tqp3SclQZDtiQsu90BBr+fv/AIKC/wDB4L+3B+0BqF54P/Yn8OWfwb8KsWjj1Z44tS1+7jPG5pZUMFrkc7Yoy6E8TNgGgD8rvjD8fPjj+0L4ok8bfHn4xeKPGmsSMzNqXirXri/m5OTh5nYgewwK5GgAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAK7r4G/tNftFfsx+J08Zfs6/HPxZ4H1RZFc3nhbX7ixaTHZ/KdRIvYqwKkcEEUAfrD/wTy/4PG/2tPg1fWPgf9v7wNa/Fbw1vWObxTotvBpuv2kfA3FECWt5tA4VlhdiSWmJr9//ANir9vf9k/8A4KE/CeP4yfsm/GDTvFGlrsTUrWJjFe6VMwz5F3bPiSCTg43DDAbkLLhiAexUUAFFABXwP/wXN/4LmfCf/gkb8JofD/h+0svFHxi8UWTP4P8AB8k37qzh+Zf7SvtpDJbK4IVAQ87qUUqqySRgH8nf7Tv7U3x//bK+MeqfH39pf4n6n4s8VavJm51LUpsiKPJKwQxjCQQpkhIowqKOgFef0AFFABRQAUUAFFABRQAUUAFFABRQAUUAFFABRQAUUAFFABRQAUUAFFABRQAUUAFFABRQAUUAFFABRQAUUAFFABRQAUUAFFABRQAUUAFFABRQAUUAFFABRQAUUAFFABRQAUUAFFABRQAUUAFFABRQAUUAFFABRQAUUAFFABRQAUUAFFABRQAUUAFFABRQAUUAFFABRQAUUAFFABRQAUUAFFABRQAUUAFFABRQAUUAFFABRQAUUAFFABRQAUUAFFABRQAUUAFFABRQAUUAFFABRQAUUAFFABRQAUUAFFABRQAUUAFerfsa/tsftK/sDfG7Tv2gP2XPiZeeHNesWVbhI2LWupW+QWtbuEnbcQNjlG6HDKVdVYAH9a3/AARg/wCCzXwQ/wCCu/wJk8SaBbQeG/iP4bijj8eeBJLre1q7DAvLYn5pbSRs7WPzI2Ufnaz/AGhQAUUAeC/8FLP29/hh/wAE1P2OvFn7WHxPEdyNGtfI8P6J5/lyazq0oK2tkh5I3uNzMASkSSPghDX8X37Uv7Tvxk/bK+P/AIo/aW+P3iqTWPFXizU3vNRuWyI4geI4IVJPlwxIFjjjBwqIoHSgDz6igAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooA9c/YZ/bU+Nf/BPr9p7wx+1R8BNa+za34dvMz2czt9n1WzbieyuFB+eGVMqe6na6lXRWH9pX7Ef7X/wo/b0/ZZ8G/tX/AAYvN+h+L9JW5+yySBptPuVJS4s5ccebDMskbY4JTIypBIB6tRQB/L//AMHgH/BQq+/aF/bh0/8AYp8GawzeE/gzaj+1o4ZMx3fiC7iSSZzjhvIgaGEZ5Rzcj+IivyDoAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAr9vP+DM3/goTefDz9oDxX/wTo8cawf7F8fW0viHwTHNLxBrNrCPtUKD/AKb2kfmH0NiMD5jQB/SBRQB/Bf8AHf4v+Kf2g/jb4w+PHjiYyaz408T3+uao28t/pF1cPO4BPYM5A9q5OgAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACvVv2Gf2jtV/ZD/AGxvhj+01pF1JG3gnxtp2q3Qjzma1jnX7RCcclZIPMjI7hzQB/cZ/wALF8A/9Dnpf/gcn+NFAH8DtFABRQAUUAFFABRQAUUAFFABRQAUUAFFABRQAUUAFFABRQAUUAFFABRQAUUAFFABRQAUUAFFABRQAUUAFFABRQAUUAFFABRQAUUAFFABRQAUUAFFABRQAUUAFFABRQAUUAFFABRQAUUAFFABRQAUUAFFABRQAUUAFFABRQAUUAFFABRQAUUAFFABRQAUUAFFABRQAUUAFFABRQAUUAFFABRQAUUAFFABRQAUUAFFABRQAUUAFFABRQAUUAFFABRQAUUAFFABRQAUUAFFABRQAUUAFFABRQAUUAFFABRQAUUAFFABRQAUUAFFAH6Gf8PnPi//ANDVdf8AgSf8aKAPzzooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigDT0Pwb4u8TxST+G/CupahHEwWR7GxkmCH0JUHBq7/AMKm+Kn/AETTxB/4Jp//AIigA/4VN8VP+iaeIP8AwTT/APxFH/Cpvip/0TTxB/4Jp/8A4igA/wCFTfFT/omniD/wTT//ABFH/Cpvip/0TTxB/wCCaf8A+IoAP+FTfFT/AKJp4g/8E0//AMRR/wAKm+Kn/RNPEH/gmn/+IoAP+FTfFT/omniD/wAE0/8A8RR/wqb4qf8ARNPEH/gmn/8AiKAD/hU3xU/6Jp4g/wDBNP8A/EUf8Km+Kn/RNPEH/gmn/wDiKAD/AIVN8VP+iaeIP/BNP/8AEUf8Km+Kn/RNPEH/AIJp/wD4igClrng3xd4Yijn8SeFdS0+OVisb31jJCHPoCwGTWZQAUUAFFABRQAUUAFFABRQAVNp+nahq15Hp2lWM1zcTNthgt4y7ufQKOSfpQBtf8Km+Kn/RNPEH/gmn/wDiKP8AhU3xU/6Jp4g/8E0//wARQAf8Km+Kn/RNPEH/AIJp/wD4ij/hU3xU/wCiaeIP/BNP/wDEUAH/AAqb4qf9E08Qf+Caf/4ij/hU3xU/6Jp4g/8ABNP/APEUAH/Cpvip/wBE08Qf+Caf/wCIo/4VN8VP+iaeIP8AwTT/APxFAB/wqb4qf9E08Qf+Caf/AOIo/wCFTfFT/omniD/wTT//ABFAB/wqb4qf9E08Qf8Agmn/APiKP+FTfFT/AKJp4g/8E0//AMRQBz9FABRQAUUAFFABRQAUUAb0Pws+JtxCs9v8OdekjdQyOujzEMD0IwvIp3/Cpvip/wBE08Qf+Caf/wCIoAP+FTfFT/omniD/AME0/wD8RR/wqb4qf9E08Qf+Caf/AOIoAP8AhU3xU/6Jp4g/8E0//wARR/wqb4qf9E08Qf8Agmn/APiKAD/hU3xU/wCiaeIP/BNP/wDEUf8ACpvip/0TTxB/4Jp//iKAD/hU3xU/6Jp4g/8ABNP/APEUf8Km+Kn/AETTxB/4Jp//AIigA/4VN8VP+iaeIP8AwTT/APxFH/Cpvip/0TTxB/4Jp/8A4igA/wCFTfFT/omniD/wTT//ABFH/Cpvip/0TTxB/wCCaf8A+IoAx9S0zUtGv5NM1jT57W4hOJbe6haORDjPKkAjgjrVegAooAKKACigAooAKKACigAra0/4bfETVrKPUtK8Ba1dW8y7obi30uZ0ceoYLgigCb/hU3xU/wCiaeIP/BNP/wDEUf8ACpvip/0TTxB/4Jp//iKAD/hU3xU/6Jp4g/8ABNP/APEUf8Km+Kn/AETTxB/4Jp//AIigA/4VN8VP+iaeIP8AwTT/APxFH/Cpvip/0TTxB/4Jp/8A4igA/wCFTfFT/omniD/wTT//ABFH/Cpvip/0TTxB/wCCaf8A+IoAP+FTfFT/AKJp4g/8E0//AMRR/wAKm+Kn/RNPEH/gmn/+IoAP+FTfFT/omniD/wAE0/8A8RWZrfh7X/DV2tj4j0O80+d4xIsN7avEzKSRuAYAkZBGfY0AUqKACigAooAKtaTo2r69fLpmhaVc3ty4JS3tIGkkYAZOFUEnAoA1v+FTfFT/AKJp4g/8E0//AMRR/wAKm+Kn/RNPEH/gmn/+IoAP+FTfFT/omniD/wAE0/8A8RR/wqb4qf8ARNPEH/gmn/8AiKAD/hU3xU/6Jp4g/wDBNP8A/EUf8Km+Kn/RNPEH/gmn/wDiKAD/AIVN8VP+iaeIP/BNP/8AEUf8Km+Kn/RNPEH/AIJp/wD4igA/4VN8VP8AomniD/wTT/8AxFH/AAqb4qf9E08Qf+Caf/4igA/4VN8VP+iaeIP/AATT/wDxFH/Cpvip/wBE08Qf+Caf/wCIoAP+FTfFT/omniD/AME0/wD8RR/wqb4qf9E08Qf+Caf/AOIoAP8AhU3xU/6Jp4g/8E0//wARR/wqb4qf9E08Qf8Agmn/APiKAD/hU3xU/wCiaeIP/BNP/wDEUf8ACpvip/0TTxB/4Jp//iKAK+rfD/x5oFi2p674J1eytkID3F3psscaknABZlAGTWPQAUUAFaeh+DfF3ieKSfw34V1LUI4mCyPY2MkwQ+hKg4NAF3/hU3xU/wCiaeIP/BNP/wDEUf8ACpvip/0TTxB/4Jp//iKAD/hU3xU/6Jp4g/8ABNP/APEUf8Km+Kn/AETTxB/4Jp//AIigA/4VN8VP+iaeIP8AwTT/APxFH/Cpvip/0TTxB/4Jp/8A4igA/wCFTfFT/omniD/wTT//ABFH/Cpvip/0TTxB/wCCaf8A+IoAP+FTfFT/AKJp4g/8E0//AMRR/wAKm+Kn/RNPEH/gmn/+IoAP+FTfFT/omniD/wAE0/8A8RR/wqb4qf8ARNPEH/gmn/8AiKAD/hU3xU/6Jp4g/wDBNP8A/EUf8Km+Kn/RNPEH/gmn/wDiKAD/AIVN8VP+iaeIP/BNP/8AEUf8Km+Kn/RNPEH/AIJp/wD4igDK1jQ9a8PXp07X9HurG4Chjb3lu0ThT0O1gDg1UoAKKACigArT0Pwb4u8TxST+G/CupahHEwWR7GxkmCH0JUHBoAu/8Km+Kn/RNPEH/gmn/wDiKP8AhU3xU/6Jp4g/8E0//wARQAf8Km+Kn/RNPEH/AIJp/wD4isrWND1rw9enTtf0e6sbgKGNveW7ROFPQ7WAODQBUooAKKACigAooAKKACtnSvh74+1ywj1XRPA2sXlrLny7i10yWSN8Eg4ZVIOCCPqKAJ/+FTfFT/omniD/AME0/wD8RR/wqb4qf9E08Qf+Caf/AOIoAP8AhU3xU/6Jp4g/8E0//wARWHd2l1YXUljfW0kM8MjRzQzIVaNgcFSDyCDwQelAEdFABRQAUUAFFABRQAUUAFFABRQAUUAf0ff8GQ3/ACax8cP+ygaf/wCkJr9vqACigAooAKKACigAooAKKACuF+K37MH7Nfx3tZrH43/s9eB/GENwpWePxR4Us9QWQEAHPnxtngD8hQB+cP7fv/BpH/wTq/ab0G+8RfssWV18FfGnks1nJockl3ol1L1AnspnJiU8Lm2eIL94pIRg/wA4P7dv7Bn7SX/BOX9oHUv2cf2nvBR0vWrOMT2F9bMZbHV7NiRHd2k2AJYmwRnAZWVkdVdWUAHjNFABRQAUUAFFAHq37Gf7Ff7R37fnx40v9nL9l34ez+IPEmpBpZBvEdtp9qpHmXd1M3ywQJkZY8klUUM7qjf0U/sCf8Ge/wCwn8B9D0/xR+2vr2o/GLxdsWS802O8m03QLaXk7Y4oWW4uApIBaWQLJtBMKAlaAP0j+Dv7BX7EX7Plgmm/BD9kT4a+FY0UAyaH4JsbeWTAIy8ixB5GwSNzEk5OTXrCIsahEUKqjCqB0oAdRQAUUAFFABRQAUUAFFAFfVNJ0rXLCTS9a0y3vLWXHmW91Cskb4IIyrAg4IB+ozXz/wDHr/gkr/wTO/aasZ7P40/sNfDXVJbhdsmpWvheCxv8YA4u7QRXC8AdJB0oA/I//gp//wAGb+gWXhbUvi3/AMEvPHWpPqFqrTyfCvxhfrKtygBJjsL5grI4wAsVzv3EnM64Cn8C/FPhbxN4G8Tah4L8aeH7zSdY0m8ls9U0vUrV4bi0uI3KSRSRuAyOrAqVIBBBBFAGfRQAUUAFFABRQB/d5+yf/wAms/DX/sn+jf8ApDDXoFABRQAUUAFFABRQAUUAFFAEd1a2t/ayWN9bRzQTRtHNDMgZZFIwVIPBBHBB614D+0R/wSm/4Ju/tW6fcWPx6/Yo+HetzXW7ztVi8OxWWo/NyxW9tRHcoSeTtkHPPWgD8X/+CuP/AAaD3/ww8Iav+0B/wTD8Qav4gs9NgkvNT+FGvS/aNQEKjc39m3AUNcsBnFvKPNYDCySuVQ/hTcW89pPJaXcDxTROUkjkUqyMDggg9CD2oAjooAKKACigAp0cbyuqRoWZjhVUck0Afth/wSQ/4NE/iJ+0N4S0r9oD/go74q1nwB4e1GKO60n4c6KsceuXcLbXR7yWRXWxVl/5Y7Gnw3zGBlwf2i/Zv/4Ij/8ABKX9lSwtbf4TfsNeApLyzCmPW/E2jrrWoeYP+Wgub/zpEYnn5CoHQADAAB9MeHPCnhfwdp40nwj4a0/S7UYxbabZpBGMAAfKgA4AA+gxWhQAUUAFFABRQAUUAFFABRQB558Vv2R/2VfjraTWXxt/Zo8A+L47gsZ18TeD7K+3s27LHzom5yzHPXLE9TX5/wD7b3/Bpl/wTA/af0q81b4E+HdS+CviqRWa31DwhM1xpjynOPO0+4cpsGfu27254HOMggH86X/BSv8A4Jd/tU/8EsPjePg1+0p4ZhNvqEclx4X8V6SzSaZrtsrYMkEjKCHXK74XCyRllyNrozfOdABRQAV+j/8Awaf/APKbX4c/9i/4i/8ATTc0Af1u0UAFFABRQAUUAFFABRQAUUAFFABRQB+cP/B1/wD8oSviP/2MHh3/ANO9tX8kNABRQAV/R9/wZDf8msfHD/soGn/+kJoA/b6igAooAKKACigAooAKKACigAooA/lC/wCDvL/lMlrX/ZP9B/8ARL1+YFABRQAUUAFf0ff8GQ3/ACax8cP+ygaf/wCkJoA/b6igAr+UL/g7y/5TJa1/2T/Qf/RL0AfmBRQAUUAFFABRQAUUAFf1+/8ABrj/AMoKPgZ/3M3/AKk+rUAff9FABX8Qf/BWL/lKb+0t/wBnAeMv/T5eUAfP9FABRQAUUAFFABRQAUUAFFABRQAUUAf0ff8ABkN/yax8cP8AsoGn/wDpCa/b6gAooAKKACigAooAKKACigAooAK+D/8Ag4b/AOCY/h7/AIKSf8E/PEVvoHhmK4+JXw6srjxB8Ob6OL9+00aB7nTwRyUuoY/L2ZC+asDn/VigD+POigAooAKKACigD+tD/g11/wCCbvhz9ib/AIJ0aD8bPEvhyBfiF8ZrKDxJrmoSQjzrfS5V36dZK2AVQQMs7L1824cHIVcfpZQAUUAFFABRQAUUAFFABRQAUUAFFABX87//AAeX/wDBNzw/4H8V+Ef+Clnwu8PR2qeKr5fDXxIS1hCrJfrCz2N820fekhhmgdzgfuLccs5yAfhHRQAUUAFFABRQB/d5+yf/AMms/DX/ALJ/o3/pDDXoFABRQAUUAFFABRQAUUAFFABRQAV/Mv8A8Hf3/BMnw9+zd+0poP7ePwf8MpY+G/i5czWvjC2s7fbBbeI4l8wz8HCm7h3SFQOZba4kJJkNAH410UAFFABRQAV+qX/Bpz/wTe8Ofto/t3337QPxY8OW+peC/graW+qNYXkIeG91y4aRdPjdWGHSPyZ7g88PBCCCrEUAf1VUUAFFABRQAUUAFFABRQAUUAFFABRQB8s/8Fj/APgnX4T/AOCm/wCwd4x/Z6v9Jtm8UW9k+rfDvU5lXfY65BGzW+HP3El+a3kP/POZz1Ax/FffWN7pl7NpupWctvcW8rRXFvPGUeJ1OGVlPKsCCCDyDQBDRQAV+j//AAaf/wDKbX4c/wDYv+Iv/TTc0Af1u0UAFFABRQAUUAFFABRQAUUAFFABRQB+cP8Awdf/APKEr4j/APYweHf/AE721fyQ0AFFABX9H3/BkN/yax8cP+ygaf8A+kJoA/b6igAooAKKACigAooAKKACigAooA/lC/4O8v8AlMlrX/ZP9B/9EvX5gUAFFABRQAV/R9/wZDf8msfHD/soGn/+kJoA/b6igAr+UL/g7y/5TJa1/wBk/wBB/wDRL0AfmBRQAUUAFFABRQAUUAFf1+/8GuP/ACgo+Bn/AHM3/qT6tQB9/wBFABX8Qf8AwVi/5Sm/tLf9nAeMv/T5eUAfP9FABRQAUUAFFABRQAUUAFFABRQAUUAf0ff8GQ3/ACax8cP+ygaf/wCkJr9vqACigAooAKKACigAooAKKACigAooA/iF/wCCrf7P2n/ss/8ABSb43fAfQ9M+xaXoPxI1QaHZ7Nvk6dNO1xaJ9BbyxDPQ9Rwa+faACigAooAK9A/ZP+Cl5+0l+1F8OP2edPSRpfHPjrSdBXyjhlF3eRQFs9gA5JY8ADJ4FAH92WjaPpfh7SbXQNDsIrWysbaO3s7WBNqQxIoVEUdgAAAPQVaoAKKACigAooAKKACigAooAKKACigAr4//AOC9/wCz5ZftK/8ABIL47+BJrDz7rS/A9x4k03ZHukW40orqKiPvuYWzR8ckSFe+KAP4w6KACigAooAKKAP7vP2T/wDk1n4a/wDZP9G/9IYa9AoAKKACigAooAKKACigAooAKKACvgX/AIOa/wBnzT/2gf8AgjP8WFez8zUfBUNn4s0eTbu8iSyuUM7YHrZvdp7b8ngYoA/j9ooAKKACigAr+rT/AINDP2fLf4Q/8Ei9P+KE+miK++J/jfVtckndR5j28Eo06JfUIDZSuoP/AD1Zhw1AH6kUUAFFABRQAUUAFFABRQAUUAFFABRQAV/GH/wXy/Z8sv2Zv+Cv/wAdvhtpNh9n0+68ZNr+nxrHtjWLVIItS2R9tiNdNGMcDYV7YoA+P6KACv0f/wCDT/8A5Ta/Dn/sX/EX/ppuaAP63aKACigAooAKKACigAooAKKACigAooA/OH/g6/8A+UJXxH/7GDw7/wCne2r+SGgAooAK/o+/4Mhv+TWPjh/2UDT/AP0hNAH7fUUAFFABRQAUUAFFABRQAUUAFFAH8oX/AAd5f8pkta/7J/oP/ol6/MCgAooAKKACv6Pv+DIb/k1j44f9lA0//wBITQB+31FABX8oX/B3l/ymS1r/ALJ/oP8A6JegD8wKKACigAooAKKACigAr+v3/g1x/wCUFHwM/wC5m/8AUn1agD7/AKKACv4g/wDgrF/ylN/aW/7OA8Zf+ny8oA+f6KACigAooAKKACigAooAKKACigAooA/o+/4Mhv8Ak1j44f8AZQNP/wDSE1+31ABRQB+b/wC3r/wdAfsC/wDBO79rHxX+x38a/hF8YNU8TeD/ALD/AGlfeFvD+lTWEv2uxt72Pynn1KGQ4iuUVt0a4cMBkAMfIf8AiNW/4JZf9ED/AGgP/CV0P/5cUAH/ABGrf8Esv+iB/tAf+Erof/y4o/4jVv8Agll/0QP9oD/wldD/APlxQAf8Rq3/AASy/wCiB/tAf+Erof8A8uKP+I1b/gll/wBED/aA/wDCV0P/AOXFAB/xGrf8Esv+iB/tAf8AhK6H/wDLij/iNW/4JZf9ED/aA/8ACV0P/wCXFAE2nf8AB6X/AMEq7y8jtbj4KfHm0SRsNcXHhTRikfudmrM2PoCa9W+Bn/B1x/wRs+NPiGHwzq3xn8ReA7i4lEdvN468JzW9u7H+9PbGeKIf7UrIoxyRxQB+inhzxJ4e8Y+H7Hxb4R16y1XStTtY7rTdT026Se3u4JFDJLFIhKyIykMGUkEEEHFXqACigD+Q/wD4OnPDFr4b/wCC4PxeurQxqmrWfh69aGOAII2OhWMbdPvFmjLluMlznJ5P550AFFABRQAV9s/8G5vgO3+I3/Ba34A+H7m1WZbfxNeaoFO3hrHTLy9VvmBHym3DevHBBwQAf2R0UAFeL/ts/wDBQn9kD/gnR4B0n4oftk/F3/hDtC1zWP7K0u+/sHUNQ8+78p5fL2WMEzr8kbtuZQvGM5IBAPmj/iKN/wCCFH/R8v8A5jPxP/8AKyj/AIijf+CFH/R8v/mM/E//AMrKAD/iKN/4IUf9Hy/+Yz8T/wDyso/4ijf+CFH/AEfL/wCYz8T/APysoAP+Io3/AIIUf9Hy/wDmM/E//wArKP8AiKN/4IUf9Hy/+Yz8T/8AysoAP+Io3/ghR/0fL/5jPxP/APKyj/iKN/4IUf8AR8v/AJjPxP8A/KygA/4ijf8AghR/0fL/AOYz8T//ACso/wCIo3/ghR/0fL/5jPxP/wDKygA/4ijf+CFH/R8v/mM/E/8A8rKP+Io3/ghR/wBHy/8AmM/E/wD8rKAPs/4A/Hr4T/tQ/Bvw9+0D8C/Ff9ueEfFWnrfaDq32Ge1+1W5JAfyrhI5U5U8OinjpXYUAFYHxV8D2vxO+F/iT4bXwTyfEOg3mmTCT7u2eB4jng8Yf0NAH8EVFABRQAUUAFFAH93n7J/8Ayaz8Nf8Asn+jf+kMNegUAFeX/tu/GvxV+zX+xf8AF79ovwLp+n3Wt+APhfr/AIk0e11aF5LWa6sdOnuokmWN0doi8ShgroxUkBlOCAD+dH/iNW/4Km/9ED/Z/wD/AAldc/8AlxR/xGrf8FTf+iB/s/8A/hK65/8ALigA/wCI1b/gqb/0QP8AZ/8A/CV1z/5cUf8AEat/wVN/6IH+z/8A+Errn/y4oAP+I1b/AIKm/wDRA/2f/wDwldc/+XFH/Eat/wAFTf8Aogf7P/8A4Suuf/LigA/4jVv+Cpv/AEQP9n//AMJXXP8A5cUf8Rq3/BU3/ogf7P8A/wCErrn/AMuKAL2h/wDB67/wUot5ZD4k/Zt+Bt1GV/drY6NrNuVPcktqcmR7YH1r6Q/Y3/4PW/Bnizxrpvgz9uH9lNfCum3k0cV3428D6xJeQ2RYhTJJYTJ5nlA5ZjHNI4XhY3PUA/c7wd4w8LfELwlpfj3wL4hs9X0XWtPhvtI1XT7hZYLy2lQSRTRupw6MjBgw4IINaVABXhv/AAU48NWvjH/gm3+0D4VvWVY9Q+CfimDzGjD+WW0m5AcA9Spww9wORQB/DrRQAUUAFFABX9q3/BEPwHB8Of8AgkR+zp4ftrUQrcfCbSNUKrt5a+gW9ZvlAHzG4LevPJJySAfU1FAFfVtUsND0q61rVJ/KtbO3ee4k2ltkaKWY4AJOAD0BPpXwR/xFG/8ABCj/AKPl/wDMZ+J//lZQAf8AEUb/AMEKP+j5f/MZ+J//AJWUf8RRv/BCj/o+X/zGfif/AOVlAB/xFG/8EKP+j5f/ADGfif8A+VlH/EUb/wAEKP8Ao+X/AMxn4n/+VlAB/wARRv8AwQo/6Pl/8xn4n/8AlZR/xFG/8EKP+j5f/MZ+J/8A5WUAH/EUb/wQo/6Pl/8AMZ+J/wD5WUf8RRv/AAQo/wCj5f8AzGfif/5WUAH/ABFG/wDBCj/o+X/zGfif/wCVlH/EUb/wQo/6Pl/8xn4n/wDlZQAf8RRv/BCj/o+X/wAxn4n/APlZXt/7EX/BW/8A4J7/APBR3xTrngr9jH9oH/hMtU8N6fHfa1a/8Ipq2n/Z4JH8tH3X1rCr5bjCliOpGKAPpCigAr+WP/g8l8D2vhP/AIK3aXr9uE3+J/g/o2pT7c53LeahZ88ddtovrxj6AA/J+igAr7Y/4N8P2t/2ev2Hv+Co/gr9ov8Aaj+IH/CL+DdJ0fWoNQ1j+ybu98qSfTp4Yh5VpFLK26R1X5UIGcnAyaAP6Jf+Io3/AIIUf9Hy/wDmM/E//wArKP8AiKN/4IUf9Hy/+Yz8T/8AysoAP+Io3/ghR/0fL/5jPxP/APKyj/iKN/4IUf8AR8v/AJjPxP8A/KygA/4ijf8AghR/0fL/AOYz8T//ACso/wCIo3/ghR/0fL/5jPxP/wDKygA/4ijf+CFH/R8v/mM/E/8A8rK+t/2T/wBrf9nv9uH4Jab+0Z+y78Qf+Eo8G6vcXEGn6x/ZN3ZebJBM0Mo8q7iilXbIjL8yAHGRkYNAHpFFABXw/wDFj/g48/4IyfA74p+Jfgp8Uv2yf7L8TeD/ABBeaJ4i03/hXniKf7Jf2k7wTw+ZDp7RybJY3XcjMrYypIINAGB/xFG/8EKP+j5f/MZ+J/8A5WUf8RRv/BCj/o+X/wAxn4n/APlZQAf8RRv/AAQo/wCj5f8AzGfif/5WUf8AEUb/AMEKP+j5f/MZ+J//AJWUAH/EUb/wQo/6Pl/8xn4n/wDlZR/xFG/8EKP+j5f/ADGfif8A+VlAHxR/wcHf8F1f+CVv7cH/AAS38bfs5/su/tS/8JR4y1fWNGn0/R/+EI1yy82ODUYJpT5t3ZRRLtjRmwzgnGBk4Ffzs0AFFABX9H3/AAZDf8msfHD/ALKBp/8A6QmgD9vqKACvz3/4KNf8HJX7Df8AwTE/aWuv2V/j38Kvixq/iG00e01KS88I6HplxZmK4Usih7jUYJNwAORswOxNAHhP/Eat/wAEsv8Aogf7QH/hK6H/APLij/iNW/4JZf8ARA/2gP8AwldD/wDlxQAf8Rq3/BLL/ogf7QH/AISuh/8Ay4o/4jVv+CWX/RA/2gP/AAldD/8AlxQAf8Rq3/BLL/ogf7QH/hK6H/8ALij/AIjVv+CWX/RA/wBoD/wldD/+XFAB/wARq3/BLL/ogf7QH/hK6H/8uKP+I1b/AIJZf9ED/aA/8JXQ/wD5cUAH/Eat/wAEsv8Aogf7QH/hK6H/APLij/iNW/4JZf8ARA/2gP8AwldD/wDlxQAf8Rq3/BLL/ogf7QH/AISuh/8Ay4o/4jVv+CWX/RA/2gP/AAldD/8AlxQB+JP/AAXW/wCChfwX/wCCnf7feoftUfATwx4o0jw9d+F9N02Oz8X2Vtb3glt42V2KW9xPHtJIwd+T3Ar43oAKKACigAr+j7/gyG/5NY+OH/ZQNP8A/SE0Aft9RQAV/KF/wd5f8pkta/7J/oP/AKJegD8wKKACigAooAKKACigAr+v3/g1x/5QUfAz/uZv/Un1agD7/ooAK/iD/wCCsX/KU39pb/s4Dxl/6fLygD5/ooAKKACigAooAKKACigAooAKKACigD+j7/gyG/5NY+OH/ZQNP/8ASE1+31ABRQB/IF/wdHf8p1/jn/3LP/qMaTXwBQAUUAFFABRQAUUAf0lf8GWP7X/jb4n/ALM3xQ/ZC8a63cXtr8M9asNT8Jm6kLfZ7HUhc+bax8fLHHPatKAT967bHA4/bKgAooA/ki/4OwP+U2vxG/7F/wAO/wDpptq/OCgAooAKKACv0Y/4NS9Nsr//AILd/C+6u7cPJZ6N4jmtWyf3ch0W8jLf98SOPx9aAP65KKACvxh/4PZ/+TBPhR/2WBf/AE1X1AH80NFABRQAUUAFFABRQAUUAf2e/wDBAj/lDb+z3/2T+H/0dLX1/QAUUAfwR/FZUj+KPiSONQqrr94FVegHnvXP0AFFABRQAUUAf3efsn/8ms/DX/sn+jf+kMNegUAFfP8A/wAFYv8AlFl+0t/2b/4y/wDTHeUAfxB0UAFFABRQAUUAFFAH9av/AAaefGPXPix/wRo8H6Pr+pyXcvgnxRrHh6GSbJZYEuPtUUeT1CJdqi9QFVV7YH6T0AFeP/8ABQn/AJMF+OH/AGR/xN/6armgD+GOigAooAKKACv7lf8AgnHptjo3/BPT4D6RpluIbe1+DPheG3hXJCRrpNsFXnngACgD2eigDn/iv/yS3xL/ANi/ef8Aoh6/gjoAKKACigAooAKKACigAr9vv+DIb/k6b44f9k/07/0uNAH9H1FABX80P/B7Mij9vv4UyBRuPwfUFsckf2rff4mgD8YaKACigAooAKKACigAr+t7/g1A/wCUJXw4/wCxg8Rf+ne5oA/R6igAr+IP/grF/wApTf2lv+zgPGX/AKfLygD5/ooAKKACigAooAKKACv6Pv8AgyG/5NY+OH/ZQNP/APSE0Aft9RQAV/KF/wAHeX/KZLWv+yf6D/6JegD8wKKACigAooAKKACigAooAKKACigAooAK/o+/4Mhv+TWPjh/2UDT/AP0hNAH7fUUAFfyhf8HeX/KZLWv+yf6D/wCiXoA/MCigAooAKKACigAooAK/r9/4Ncf+UFHwM/7mb/1J9WoA+/6KACv4g/8AgrF/ylN/aW/7OA8Zf+ny8oA+f6KACigAooAKKACigAooAKKACigAooA/o+/4Mhv+TWPjh/2UDT//AEhNft9QAUUAfyBf8HR3/Kdf45/9yz/6jGk18AUAFFABRQAUUAFFAH7g/wDBkNNKv7UHxygWVvLbwDprMm7gkXrYOPUZP5mv6O6ACigD+SL/AIOwP+U2vxG/7F/w7/6abavzgoAKKACigAr9H/8Ag0//AOU2vw5/7F/xF/6abmgD+t2igAr8Yf8Ag9n/AOTBPhR/2WBf/TVfUAfzQ0UAFFABRQAUUAFFABRQB/Z7/wAECP8AlDb+z3/2T+H/ANHS19f0AFFAH8EfxZ/5Kp4m/wCxgvP/AEe9c/QAUUAFFABRQB/d5+yf/wAms/DX/sn+jf8ApDDXoFABXz//AMFYv+UWX7S3/Zv/AIy/9Md5QB/EHRQAUUAFFABRQAUUAf1Pf8Gbf/KI/Uv+ywaz/wCkmn1+r1ABXj//AAUJ/wCTBfjh/wBkf8Tf+mq5oA/hjooAKKACigAr+5z/AIJ7f8mC/A//ALI/4Z/9NVtQB7BRQBz/AMV/+SW+Jf8AsX7z/wBEPX8EdABRQAUUAFFABRQAUUAFft9/wZDf8nTfHD/sn+nf+lxoA/o+ooAK/mh/4PaP+T+vhR/2R9f/AE63tAH4w0UAFFABRQAUUAFFABX9b3/BqB/yhK+HH/YweIv/AE73NAH6PUUAFfxB/wDBWL/lKb+0t/2cB4y/9Pl5QB8/0UAFFABRQAUUAFFABX9H3/BkN/yax8cP+ygaf/6QmgD9vqKACv5Qv+DvL/lMlrX/AGT/AEH/ANEvQB+YFFABRQAUUAFFABRQAUUAFFABRQAUUAFf0ff8GQ3/ACax8cP+ygaf/wCkJoA/b6igAr+UL/g7y/5TJa1/2T/Qf/RL0AfmBRQAUUAFFABRQAUUAFf1+/8ABrj/AMoKPgZ/3M3/AKk+rUAff9FABX8Qf/BWL/lKb+0t/wBnAeMv/T5eUAfP9FABRQAUUAFFABRQAUUAFFABRQAUUAf0ff8ABkN/yax8cP8AsoGn/wDpCa/b6gAooA/kC/4Ojv8AlOv8c/8AuWf/AFGNJr4AoAKKACigAooAKKAP2+/4Mhv+Tpvjh/2T/Tv/AEuNf0fUAFFAH8kX/B2B/wAptfiN/wBi/wCHf/TTbV+cFABRQAUUAFfo/wD8Gn//ACm1+HP/AGL/AIi/9NNzQB/W7RQAV+MP/B7P/wAmCfCj/ssC/wDpqvqAP5oaKACigAooAKKACigAooA/s9/4IEf8obf2e/8Asn8P/o6Wvr+gAooA/gj+LP8AyVTxN/2MF5/6PeufoAKKACigAooA/u8/ZP8A+TWfhr/2T/Rv/SGGvQKACvn/AP4Kxf8AKLL9pb/s3/xl/wCmO8oA/iDooAKKACigAooAKKAP6nv+DNv/AJRH6l/2WDWf/STT6/V6gArx/wD4KE/8mC/HD/sj/ib/ANNVzQB/DHRQAUUAFFABX9zn/BPb/kwX4H/9kf8ADP8A6aragD2CigDn/iv/AMkt8S/9i/ef+iHr+COgAooAKKACigAooAKKACv2+/4Mhv8Ak6b44f8AZP8ATv8A0uNAH9H1FABX80P/AAe0f8n9fCj/ALI+v/p1vaAPxhooAKKACigAooAKKACv63v+DUD/AJQlfDj/ALGDxF/6d7mgD9HqKACv4g/+CsX/AClN/aW/7OA8Zf8Ap8vKAPn+igAooAKKACigAooAK/o+/wCDIb/k1j44f9lA0/8A9ITQB+31FABX8oX/AAd5f8pkta/7J/oP/ol6APzAooAKKACigAooAKKACigAooAKKACigAr+j7/gyG/5NY+OH/ZQNP8A/SE0Aft9RQAV/KF/wd5f8pkta/7J/oP/AKJegD8wKKACigAooAKKACigAr+v3/g1x/5QUfAz/uZv/Un1agD7/ooAK/iD/wCCsX/KU39pb/s4Dxl/6fLygD5/ooAKKACigAooAKKACigAooAKKACigD+j7/gyG/5NY+OH/ZQNP/8ASE1+31ABRQB/IF/wdHf8p1/jn/3LP/qMaTXwBQAUUAFFABRQAUUAft9/wZDf8nTfHD/sn+nf+lxr+j6gAooA/ki/4OwP+U2vxG/7F/w7/wCmm2r84KACigAooAK/R/8A4NP/APlNr8Of+xf8Rf8AppuaAP63aKACvxh/4PZ/+TBPhR/2WBf/AE1X1AH80NFABRQAUUAFFABRQAUUAf2e/wDBAj/lDb+z3/2T+H/0dLX1/QAUUAfwR/Fn/kqnib/sYLz/ANHvXP0AFFABRQAUUAf3efsn/wDJrPw1/wCyf6N/6Qw16BQAV8//APBWL/lFl+0t/wBm/wDjL/0x3lAH8QdFABRQAUUAFFABRQB/Yt/wbk/sk+Nv2N/+CSPw08A/E3RJtM8SeIlu/E+tabcQmOW0a/mMsEUin5lkW1FuHVgGV9ykArX3NQAV8l/8F2vjLpnwK/4JAftCeNNUnSNb/wCG194ft9zYJm1UDTI8e+67BH0z0FAH8W9FABRQAUUAFf3Of8E9v+TBfgf/ANkf8M/+mq2oA9gooA5/4r/8kt8S/wDYv3n/AKIev4I6ACigAooAKKACigAooAK/b7/gyG/5Om+OH/ZP9O/9LjQB/R9RQAV/ND/we0f8n9fCj/sj6/8Ap1vaAPxhooAKKACigAooAKKACv63v+DUD/lCV8OP+xg8Rf8Ap3uaAP0eooAK/iD/AOCsX/KU39pb/s4Dxl/6fLygD5/ooAKKACigAooAKKACv6Pv+DIb/k1j44f9lA0//wBITQB+31FABX8oX/B3l/ymS1r/ALJ/oP8A6JegD8wKKACigAooAKKACigAooAKKACigAooAK/o+/4Mhv8Ak1j44f8AZQNP/wDSE0Aft9RQAV/KF/wd5f8AKZLWv+yf6D/6JegD8wKKACigAooAKKACigAr+v3/AINcf+UFHwM/7mb/ANSfVqAPv+igAr+IP/grF/ylN/aW/wCzgPGX/p8vKAPn+igAooAKKACigAooAKKACigAooAKKAP6Pv8AgyG/5NY+OH/ZQNP/APSE1+31ABRQB/IF/wAHR3/Kdf45/wDcs/8AqMaTXwBQAUUAFFABRQAUUAf0Sf8ABkb+zl4p8O/Cb43ftU65p80Ol+KNY0rw74fkkiKic2KXE926kj513XdugYcBo5ByQQP3YoAKKAP5Iv8Ag7A/5Ta/Eb/sX/Dv/pptq/OCgAooAKKACv0f/wCDT/8A5Ta/Dn/sX/EX/ppuaAP63aKACvxh/wCD2f8A5ME+FH/ZYF/9NV9QB/NDRQAUUAFFABRQAUUAFFAH9nv/AAQI/wCUNv7Pf/ZP4f8A0dLX1/QAUUAfwR/Fn/kqnib/ALGC8/8AR71z9ABRQAUUAFFAH93n7J//ACaz8Nf+yf6N/wCkMNegUAFcf+0L8FPC37SnwD8cfs6eOtQ1C00Px/4P1Pw3rF1pMqR3UNrfWslrK8LSI6LKElYqWRlDAEqwyCAflj/xBU/8Esv+i+ftAf8AhVaH/wDKej/iCp/4JZf9F8/aA/8ACq0P/wCU9AB/xBU/8Esv+i+ftAf+FVof/wAp6P8AiCp/4JZf9F8/aA/8KrQ//lPQAf8AEFT/AMEsv+i+ftAf+FVof/yno/4gqf8Agll/0Xz9oD/wqtD/APlPQAf8QVP/AASy/wCi+ftAf+FVof8A8p6P+IKn/gll/wBF8/aA/wDCq0P/AOU9AF7Q/wDgzA/4JR6TLJJf/FX45aoHXCx33i7S1Cc9R5OlxnP1JFfRH7IX/BuN/wAElP2M/GGn/EnwJ+zf/wAJL4m0qSOXTte8fatLqr28ycrMkEhFssoYBhIIQysAVK4oA+6KKACv5/f+Dy//AIKU+HdVg8K/8Exvhf4gS4u7G/h8T/E57WYFbZxGw0/T3wfvkSPdOjAYH2VhnccAH4C0UAFFABRQAV/c5/wT2/5MF+B//ZH/AAz/AOmq2oA9gooA5/4r/wDJLfEv/Yv3n/oh6/gjoAKKACigAooAKKACigAr9vv+DIb/AJOm+OH/AGT/AE7/ANLjQB/R9RQAV/ND/wAHtH/J/Xwo/wCyPr/6db2gD8YaKACigAooAKKACigAr+t7/g1A/wCUJXw4/wCxg8Rf+ne5oA/R6igAr+IP/grF/wApTf2lv+zgPGX/AKfLygD5/ooAKKACigAooAKKACv6Pv8AgyG/5NY+OH/ZQNP/APSE0Aft9RQAV8n/ALXn/BD7/gl5+3l8ZJv2gf2rv2Yf+Eq8XXGn29jNq/8Awmmt2O63hBEaeVZ3sUQwCeQmT3JoA8w/4hcf+CFH/RjX/mTPE/8A8s6P+IXH/ghR/wBGNf8AmTPE/wD8s6AD/iFx/wCCFH/RjX/mTPE//wAs6P8AiFx/4IUf9GNf+ZM8T/8AyzoAP+IXH/ghR/0Y1/5kzxP/APLOj/iFx/4IUf8ARjX/AJkzxP8A/LOgDzj9sf8A4Nr/APgip8Kv2RPip8UPAX7F/wBg13w38N9c1TRb7/hY3iOX7Pd2+nzSwybJNRZH2uittZWU4wQRkV/KTQAUUAFFABRQAUUAFFABX9H3/BkN/wAmsfHD/soGn/8ApCaAP2+ooAK+T/2vP+CH3/BLz9vL4yTftA/tXfsw/wDCVeLrjT7exm1f/hNNbsd1vCCI08qzvYohgE8hMnuTQB5h/wAQuP8AwQo/6Ma/8yZ4n/8AlnR/xC4/8EKP+jGv/MmeJ/8A5Z0AH/ELj/wQo/6Ma/8AMmeJ/wD5Z0f8QuP/AAQo/wCjGv8AzJnif/5Z0AH/ABC4/wDBCj/oxr/zJnif/wCWdH/ELj/wQo/6Ma/8yZ4n/wDlnQAf8QuP/BCj/oxr/wAyZ4n/APlnR/xC4/8ABCj/AKMa/wDMmeJ//lnQAf8AELj/AMEKP+jGv/MmeJ//AJZ0f8QuP/BCj/oxr/zJnif/AOWdAB/xC4/8EKP+jGv/ADJnif8A+WdfX/7Ln7LnwJ/Yt+BOh/s0fs0eBv8AhGvBPhr7V/Yui/2ndXn2b7RdS3U3766llmfdNPK/zO2N2BhQAAD0CigAr+IP/grF/wApTf2lv+zgPGX/AKfLygD5/ooAKKACigAooAKKACigAooAKKACigD+j7/gyG/5NY+OH/ZQNP8A/SE1+31ABRQB+QP/AAVH/wCDUv8A4eUft1+Ov21f+G8/+EL/AOE0/sz/AIpn/hVv9o/Y/sml2lh/x8f2pB5m/wCy+Z/q1279vO3cfn//AIgY/wDrKL/5hP8A+/VAB/xAx/8AWUX/AMwn/wDfqj/iBj/6yi/+YT/+/VAB/wAQMf8A1lF/8wn/APfqj/iBj/6yi/8AmE//AL9UAH/EDH/1lF/8wn/9+qP+IGP/AKyi/wDmE/8A79UATWX/AAYz2SXcb6l/wU8llgDgzRw/BkRuy9wGOsMFPuVP0NerfAz/AIMpf2IvBniGLWPj3+1D8QfHFrBKHXSdLs7XRYJwP4JWHnylTzny3jb0YdwD9d/gh8DvhH+zZ8KND+BvwI8Aad4X8J+G7JbTRdD0uHZDbxgknqSzuzFmZ2Jd3ZmYszEnq6ACigD+RT/g6o1611n/AILg/FiytkYNpem+HLWZmxhmOhWMuR7YlA+oNfndQAUUAFFABX6P/wDBp/8A8ptfhz/2L/iL/wBNNzQB/W7RQAV+MP8Awez/APJgnwo/7LAv/pqvqAP5oaKACigAooAKKACigAooA/s9/wCCBH/KG39nv/sn8P8A6Olr6/oAKKAP4I/iz/yVTxN/2MF5/wCj3rn6ACigAooAKKAP7vP2T/8Ak1n4a/8AZP8ARv8A0hhr0CgAooAKKACigAooAKKACigAqj4i8R+HfB+hXfijxbr9lpemafA019qOpXSQQW0ajLPJI5CooHUkgCgD8hf+Cvn/AAdg/s2fs1eGNV+C/wDwTw1/TfiZ8SJo5LZvGFuvneH/AA+3QyrJ93UZh1RYt0GcF5G2mJv5pPiD4/8AG/xX8dax8TPiR4nvNa8QeINSm1DWtX1CYyTXl1K5eSV2PVmYkmgDGooAKKACigAr+5z/AIJ7f8mC/A//ALI/4Z/9NVtQB7BRQBz/AMV/+SW+Jf8AsX7z/wBEPX8EdABRQAUUAFFABRQAUUAFft9/wZDf8nTfHD/sn+nf+lxoA/o+ooAK/mh/4PaP+T+vhR/2R9f/AE63tAH4w0UAFFABRQAUUAFFABX9b3/BqB/yhK+HH/YweIv/AE73NAH6PUUAFfxB/wDBWL/lKb+0t/2cB4y/9Pl5QB8/0UAFFABRQAUUAFFABX9H3/BkN/yax8cP+ygaf/6QmgD9vqKACigAooAKKACigDx//goT/wAmC/HD/sj/AIm/9NVzX8MdABRQAUUAFFABRQAUUAFf0ff8GQ3/ACax8cP+ygaf/wCkJoA/b6igAooAKKACigAooAKKACigAooAKKACv4g/+CsX/KU39pb/ALOA8Zf+ny8oA+f6KACigAooAKKACigAooAKKACigAooA/o+/wCDIb/k1j44f9lA0/8A9ITX7fUAFFABRQAUUAFFABRQAUUAFFABRQB/Ev8A8FifjtY/tKf8FR/jt8YdIuluNP1D4kaja6XcJjE1naSfY7eQezRW8bfjXzXQAUUAFFABX6P/APBp/wD8ptfhz/2L/iL/ANNNzQB/W7RQAV+MP/B7P/yYJ8KP+ywL/wCmq+oA/mhooAKKACigAooAKKACigD+z3/ggR/yht/Z7/7J/D/6Olr6/oAKKAP4I/iz/wAlU8Tf9jBef+j3rn6ACigAooAKKAP7vP2T/wDk1n4a/wDZP9G/9IYa9AoAKKACigAooAKKACigAooAK+T/APgon/wRl/Yr/wCCnejta/tHWvjSLUkXGn61oHjm+haxO0gGK0lkkssgnPzW7c/U5AP54f8AgsF/wbRftVf8EzdHvvjn8LtZk+KXwltd0l94h0/TTDqOgx5639spYCIDA+0xsUzkusOVB/NGgAooAKKACigAr+5z/gnt/wAmC/A//sj/AIZ/9NVtQB7BRQBz/wAV/wDklviX/sX7z/0Q9fwR0AFFABRQAUUAFFABRQAV+33/AAZDf8nTfHD/ALJ/p3/pcaAP6PqKACv5of8Ag9o/5P6+FH/ZH1/9Ot7QB+MNFABRQAUUAFFABRQAV/W9/wAGoH/KEr4cf9jB4i/9O9zQB+j1FABX8Qf/AAVi/wCUpv7S3/ZwHjL/ANPl5QB8/wBFABRQAUUAFFABRQAV/R9/wZDf8msfHD/soGn/APpCaAP2+ooAKKACigAooAKKAPH/APgoT/yYL8cP+yP+Jv8A01XNfwx0AFFABRQAUUAFFABRQAV/R9/wZDf8msfHD/soGn/+kJoA/b6igAooAKKACigAooAKKACigAooAKKACv4g/wDgrF/ylN/aW/7OA8Zf+ny8oA+f6KACigAooAKKACigAooAKKACigAooA/o+/4Mhv8Ak1j44f8AZQNP/wDSE1+31ABRQAUUAFFABRQAUUAFFABRQAV8g/8ABcT/AIKNaB/wTP8A+Ce/jD4zQa1FD401y0k0H4b2PmYln1i5jZUmUDnZbpvuXOQMQhchnUEA/jDkd5HaSRyzMcszHkmm0AFFABRQAV+j/wDwaf8A/KbX4c/9i/4i/wDTTc0Af1u0UAFfjD/wez/8mCfCj/ssC/8ApqvqAP5oaKACigAooAKKACigAooA/s9/4IEf8obf2e/+yfw/+jpa+v6ACigD+CP4s/8AJVPE3/YwXn/o965+gAooAKKACigD+7z9k/8A5NZ+Gv8A2T/Rv/SGGvQKACqfiDxDoHhHw/feK/FeuWemaXplnLd6lqWoXSQ29pbxoXkmlkchY0VVLMzEBQCSQBQB5X/w8J/YG/6Pg+D/AP4cvSv/AJIo/wCHhP7A3/R8Hwf/APDl6V/8kUAH/Dwn9gb/AKPg+D//AIcvSv8A5Io/4eE/sDf9HwfB/wD8OXpX/wAkUAH/AA8J/YG/6Pg+D/8A4cvSv/kij/h4T+wN/wBHwfB//wAOXpX/AMkUAH/Dwn9gb/o+D4P/APhy9K/+SKP+HhP7A3/R8Hwf/wDDl6V/8kUAH/Dwn9gb/o+D4P8A/hy9K/8AkivQ/APxQ+GnxX0VfEnwt+Ieh+JdOYKVv/D+rw3kJBGR88LMvI5HPIoA3aKAINQ0/T9X0+fSdWsYbq1uoWiuba4jDxyxsCGRlIwykEggjBHBr+TT/g5Z/wCCPWlf8Ey/2q7P4mfA3Qzb/CL4oNcXfhuziUlNB1CMg3WmZyf3YDrLCTg+W7RgHyGZgD81aKACigAooAK/uc/4J7f8mC/A/wD7I/4Z/wDTVbUAewUUAc/8V/8AklviX/sX7z/0Q9fwR0AFFABRQAUUAFFABRQAV+33/BkN/wAnTfHD/sn+nf8ApcaAP6PqKACv5of+D2j/AJP6+FH/AGR9f/Tre0AfjDRQAUUAFFABRQAUUAFf1vf8GoH/AChK+HH/AGMHiL/073NAH6PUUAFfxB/8FYv+Upv7S3/ZwHjL/wBPl5QB8/0UAFFABRQAUUAFFABX9H3/AAZDf8msfHD/ALKBp/8A6QmgD9vqKACvPfiP+1t+yp8HfEzeCvi5+018PfCusRwpM+k+JPGljY3SxtyrmKaVWCkdDjB7UAYP/Dwn9gb/AKPg+D//AIcvSv8A5Io/4eE/sDf9HwfB/wD8OXpX/wAkUAH/AA8J/YG/6Pg+D/8A4cvSv/kij/h4T+wN/wBHwfB//wAOXpX/AMkUAH/Dwn9gb/o+D4P/APhy9K/+SKP+HhP7A3/R8Hwf/wDDl6V/8kUAeUft4ft4fsO+Iv2HfjN4f8P/ALZnwpvr+++FPiK3sbGz+ImmSzXEz6ZcKkaIs5Z3ZiFCgEkkAc1/F1QAUUAFFABRQAUUAFFABX9H3/BkN/yax8cP+ygaf/6QmgD9vqKACigAooAKKACigAooAKKACigAooAK/iD/AOCsX/KU39pb/s4Dxl/6fLygD5/ooAKKACigAooAKKACigAooAKKACigD+j7/gyG/wCTWPjh/wBlA0//ANITX7fUAFFABRQAUUAFFABRQAUUAFNllit4mnmkVEjUs7u2AoHUk9qAPh//AIKCf8HC/wDwTO/4J+6FfWfiH446d498aW4ZLfwH8Pb6LUbwzAfcuJY2MNkASM+c6vtOVR8Yr+YD/gqd/wAFVf2kf+CsX7QP/C6PjpdRafpWlxSWng3wXpszNY6DZs5bYucebO/y+bcMA0hRRhUSONAD5kooAKKACigAr9H/APg0/wD+U2vw5/7F/wARf+mm5oA/rdooAK/GH/g9n/5ME+FH/ZYF/wDTVfUAfzQ0UAFFABRQAUUAFFABRQB/Z7/wQI/5Q2/s9/8AZP4f/R0tfX9ABRQB/BH8Wf8Akqnib/sYLz/0e9c/QAUUAFFABRQB/d5+yf8A8ms/DX/sn+jf+kMNegUAFfP/APwVi/5RZftLf9m/+Mv/AEx3lAH8QdFABRQAUUAFFABXafAT9oz47/stfEex+Lv7Ovxa17wb4k0+RWt9W8P6i9vIQDnY+07ZYz0aNwyMMhlIJFAH9jX/AARX/wCChd1/wU3/AOCeng39pvxJZ2tr4qZp9H8bWtjEUhj1a1YJK8aknakqGKcLk7RMFydtfV1ABXw7/wAHFv7Immftg/8ABI/4raD/AGYs+teB9Hbxr4bl2kvDc6YjzyhAASzSWn2uEL3MwxzigD+OWigAooAKKACv7nP+Ce3/ACYL8D/+yP8Ahn/01W1AHsFFAHP/ABX/AOSW+Jf+xfvP/RD1/BHQAUUAFFABRQAUUAFFABX7ff8ABkN/ydN8cP8Asn+nf+lxoA/o+ooAK/mh/wCD2j/k/r4Uf9kfX/063tAH4w0UAFFABRQAUUAFFABX9b3/AAagf8oSvhx/2MHiL/073NAH6PUUAFfxB/8ABWL/AJSm/tLf9nAeMv8A0+XlAHz/AEUAFFABRQAUUAFFABX9H3/BkN/yax8cP+ygaf8A+kJoA/b6igAr+UL/AIO8v+UyWtf9k/0H/wBEvQB+YFFABRQAUUAFFABRQAUUAFFABRQAUUAFf0ff8GQ3/JrHxw/7KBp//pCaAP2+ooAK/lY/4O2fHvjnw9/wWF1nTtB8Z6tY248A6Gwt7PUpYkBMT5O1WAzQB+ZX/C2fip/0UvxB/wCDmf8A+Lo/4Wz8VP8AopfiD/wcz/8AxdAB/wALZ+Kn/RS/EH/g5n/+Lo/4Wz8VP+il+IP/AAcz/wDxdAB/wtn4qf8ARS/EH/g5n/8Ai6P+Fs/FT/opfiD/AMHM/wD8XQAf8LZ+Kn/RS/EH/g5n/wDi6P8AhbPxU/6KX4g/8HM//wAXQAf8LZ+Kn/RS/EH/AIOZ/wD4uj/hbPxU/wCil+IP/BzP/wDF0AH/AAtn4qf9FL8Qf+Dmf/4uv65P+DYfVtV1z/ghv8ENV1rU7i8upf8AhJfMuLqZpJHx4m1UDLMSTgAD6CgD74ooAK/iD/4Kxf8AKU39pb/s4Dxl/wCny8oA+f6KACigAooAKKACigAooAKKACigAooA/o+/4Mhv+TWPjh/2UDT/AP0hNft9QAUUAfy9f8HE/wDwVC/4KJ/s7f8ABY34w/Bz4E/tqfEjwl4V0f8A4R/+yvD+g+Kri2tLTzfD2mzy+XGjBV3SyyOcDlnY96+KP+H1v/BXD/pI18YP/C4u/wD4ugA/4fW/8FcP+kjXxg/8Li7/APi6ueHv+C43/BXbw3r9j4itv+ChvxUuZLG8iuY7fUPFlxcW8rI4YJLFIxWRCRhkYFWGQQQaAP6jP+CL/wDwV0+E/wDwVt/Zdt/iRoxs9I+IPh6OK0+JHg2GU5068IO24hDfM1rPtZ42y235o2YtGxP2JQAUUAfL/wDwVf8A2RP2m/2sv2ZrzTv2Mf2p/GHwr+KGgrJd+FdQ8O+KLjT7LVJMDdY3yxHDRyBQElxuifDA7DIj/wAs/wAX/wDgqb/wW9+AHxP1z4L/ABo/bj+OHhvxV4b1B7LXNE1TxldxzWky9VI34IIIZWBKspVlJUgkA43W/wDgsZ/wVh8QRRw3/wDwUi+N0axtuU2PxL1K1J+phmUsPY5FeZ/Ff9r/APaz+PNu9r8cv2oviL40ikOZI/Fnja/1FW4A5FxK+eABQB5zRQAUUAFFABRQAV+j/wDwaf8A/KbX4c/9i/4i/wDTTc0Af1u0UAFfjD/wez/8mCfCj/ssC/8ApqvqAP5oaKACigAooAKKACigAooA/s9/4IEf8obf2e/+yfw/+jpa+v6ACigD+CP4s/8AJVPE3/YwXn/o965+gAooAKKACigD+7z9k/8A5NZ+Gv8A2T/Rv/SGGvQKACvn/wD4Kxf8osv2lv8As3/xl/6Y7ygD+IOigAooAKKACigAooA/pU/4MlPFl1d/sOfF7wM+/wAnTfiul/Hlvl3XOmW0bYGODi1XJzzx0xz+1FABWX408K6X478H6t4H1xC1jrOmz2N4q94po2jcf98saAP4IdZ0m+0DV7rQtTjCXNlcyQXCKwYLIjFWGR15BqrQAUUAFFABX9zn/BPb/kwX4H/9kf8ADP8A6aragD2CigDn/iv/AMkt8S/9i/ef+iHr+COgAooAKKACigAooAKKACv2+/4Mhv8Ak6b44f8AZP8ATv8A0uNAH9H1FABX80P/AAe0f8n9fCj/ALI+v/p1vaAPxhooAKKACigAooAKKACv63v+DUD/AJQlfDj/ALGDxF/6d7mgD9HqKACv4g/+CsX/AClN/aW/7OA8Zf8Ap8vKAPn+igAooAKKACigAooAK/o+/wCDIb/k1j44f9lA0/8A9ITQB+31FABX8oX/AAd5f8pkta/7J/oP/ol6APzAooAKKACigAooAKKACigAooAKKACigAr+j7/gyG/5NY+OH/ZQNP8A/SE0Aft9RQAV/KF/wd5f8pkta/7J/oP/AKJegD8wKKACigAooAKKACigAr+v3/g1x/5QUfAz/uZv/Un1agD7/ooAK/iD/wCCsX/KU39pb/s4Dxl/6fLygD5/ooAKKACigAooAKKACigAooAKKACigD+j7/gyG/5NY+OH/ZQNP/8ASE1+31ABRQB/IF/wdHf8p1/jn/3LP/qMaTXwBQAUUAe5f8E7v+CgHx5/4Jp/tQ6H+1D8AtW23unMbfWtDuJmW013TnI86xuVH3kcAENgmOREkX5kU1/ZJ+wP+3T8CP8Agox+zH4f/aj/AGfdfW50nWIfL1DTZZB9q0a/QDz7G5QfcljY+mHQpIhZHRiAezUUAFfnD/wX/wD+CEPgj/gqn8JW+K3wb03TNF+OnheyxoGszYhj8Q2qAn+y7x+nJJMMzZ8p/lJCO5AB/J98S/hp8QPg38QNY+FXxV8H6h4f8R+H9Qksda0XVLdobizuI22vG6NyCD+BHIyCDWFQAUUAFFABRQAUUAFfo/8A8Gn/APym1+HP/Yv+Iv8A003NAH9btFABX4w/8Hs//Jgnwo/7LAv/AKar6gD+aGigAooAKKACigAooAKKAP7Pf+CBH/KG39nv/sn8P/o6Wvr+gAooA/gj+LP/ACVTxN/2MF5/6PeufoAKKACigAooA/u8/ZP/AOTWfhr/ANk/0b/0hhr0CgAr5/8A+CsX/KLL9pb/ALN/8Zf+mO8oA/iDooAKKACigAooAKKAP6Pv+DIb/k1j44f9lA0//wBITX7fUAFFAH8EfxZ/5Kp4m/7GC8/9HvXP0AFFABRQAV/c5/wT2/5MF+B//ZH/AAz/AOmq2oA9gooA5/4r/wDJLfEv/Yv3n/oh6/gjoAKKACigAooAKKACigAr9vv+DIb/AJOm+OH/AGT/AE7/ANLjQB/R9RQAV/ND/wAHtH/J/Xwo/wCyPr/6db2gD8YaKACigAooAKKACigAr+t7/g1A/wCUJXw4/wCxg8Rf+ne5oA/R6igAr+IP/grF/wApTf2lv+zgPGX/AKfLygD5/ooAKKACigAooAKKACv6Pv8AgyG/5NY+OH/ZQNP/APSE0Aft9RQAV/KF/wAHeX/KZLWv+yf6D/6JegD8wKKACigAooAKKACigAooAKKACigAooAK/o+/4Mhv+TWPjh/2UDT/AP0hNAH7fUUAFfyhf8HeX/KZLWv+yf6D/wCiXoA/MCigAooAKKACigAooAK/r9/4Ncf+UFHwM/7mb/1J9WoA+/6KACv4g/8AgrF/ylN/aW/7OA8Zf+ny8oA+f6KACigAooAKKACigAooAKKACigAooA/o+/4Mhv+TWPjh/2UDT//AEhNft9QAUUAfyBf8HR3/Kdf45/9yz/6jGk18AUAFFABX2n/AMER/wDgsJ8Uf+CSX7TcXi6JrvWPhn4olhtPiN4RjlOLi3Bwl7bgnat3BuZkJ4dS8ZIDh1AP7Afgx8Zfhh+0P8KtA+N/wW8aWXiLwr4o0yO/0PWdPk3RXMDjIPqrA5VkYBkZWVgGUgdRQAUUAfmP/wAHCv8AwQM8Mf8ABT74dSfH/wDZ40rTdH+Onhqxxazvthi8XWaLxp9zJkKsygfuJ24X/VuRGVeL+VHxz4G8ZfDHxlqnw7+Inhe/0PXtDv5bLWNH1S1aC5srmNikkUkbgMjqwIIIyDQBk0UAFFABRQAUUAFfo/8A8Gn/APym1+HP/Yv+Iv8A003NAH9btFABX4w/8Hs//Jgnwo/7LAv/AKar6gD+aGigAooAKKACigAooAKKAP7Pf+CBH/KG39nv/sn8P/o6Wvr+gAooA/gj+LP/ACVTxN/2MF5/6PeufoAKKACigAooA/u8/ZP/AOTWfhr/ANk/0b/0hhr0CgAr5/8A+CsX/KLL9pb/ALN/8Zf+mO8oA/iDooAKKACigAooAKKAP6Tf+DJDw9e237GPxk8VyH/R734n29pF8p+/Dp0LtzjHS4T39e2f2voAKKAP4I/iz/yVTxN/2MF5/wCj3rn6ACigAooAK/uc/wCCe3/JgvwP/wCyP+Gf/TVbUAewUUAc/wDFf/klviX/ALF+8/8ARD1/BHQAUUAFFABRQAUUAFFABX7ff8GQ3/J03xw/7J/p3/pcaAP6PqKACv5of+D2j/k/r4Uf9kfX/wBOt7QB+MNFABRQAUUAFFABRQAV/W9/wagf8oSvhx/2MHiL/wBO9zQB+j1FABX8Qf8AwVi/5Sm/tLf9nAeMv/T5eUAfP9FABRQAUUAFFABRQAV/R9/wZDf8msfHD/soGn/+kJoA/b6igAr+UL/g7y/5TJa1/wBk/wBB/wDRL0AfmBRQAUUAFFABRQAUUAFFABRQAUUAFFABX9H3/BkN/wAmsfHD/soGn/8ApCaAP2+ooAK/lC/4O8v+UyWtf9k/0H/0S9AH5gUUAFFABRQAUUAFFABX9fv/AAa4/wDKCj4Gf9zN/wCpPq1AH3/RQAV/EH/wVi/5Sm/tLf8AZwHjL/0+XlAHz/RQAUUAFFABRQAUUAFFABRQAUUAFFAH9H3/AAZDf8msfHD/ALKBp/8A6Qmv2+oAKKAP5Av+Do7/AJTr/HP/ALln/wBRjSa+AKACigAooA/VL/g21/4Ls33/AATo+K0X7K/7SviaWT4I+MtTBW+upmYeDtRkIH21Bg4tZDgTxjAXiZeVkWX+qPT9Q0/V9Pg1bSb6G6tbqFZba5t5A8csbAFXVgcMpBBBBwRyKAJ6KACvyp/4OJ/+DfnR/wDgpF4Sm/ao/Zb0qy0344eH9O23FiFjhh8aWsa/LbTPwFvEUbYZmO0jEUhC7HiAP5afFPhbxN4G8Tah4L8aeH7zSdY0m8ls9U0vUrV4bi0uI3KSRSRuAyOrAqVIBBBBFZ9ABRQAUUAFFABX6Lf8Gp2q2Onf8FvfhZZ3chWTUNI8RwWoCk7pBol7KR7fJG5/D3oA/rmooAK/GH/g9n/5ME+FH/ZYF/8ATVfUAfzQ0UAFFABRQAUUAFFABRQB/Z7/AMECP+UNv7Pf/ZP4f/R0tfX9ABRQB/BD8ULi3vPiX4iu7SdJYpNdvHjkjYMrqZnIII6gjvWDQAUUAFFABRQB/d5+yf8A8ms/DX/sn+jf+kMNegUAFfP/APwVi/5RZftLf9m/+Mv/AEx3lAH8QdFABRQAUUAFFABVzw94e1/xdr1n4W8K6HeapqmpXUdtp+m6favNcXUzsFSKONAWd2YgBVBJJwKAP7Fv+DfT9gDxb/wTl/4Jk+EPg58UNEXTfG/iK+uvFHjfT1YMbW/u9ipA5GR5kVrDawvgkb4nwSMGvtqgAooA/gj+LP8AyVTxN/2MF5/6PeufoAKKACigAr+5P/gm7q1lr3/BO/4Ca7pkhktr34L+Fp7d2UqWjfSLVlOD04IoA9pooA5/4r/8kt8S/wDYv3n/AKIev4I6ACigAooAKKACigAooAK/b7/gyG/5Om+OH/ZP9O/9LjQB/R9RQAV/M9/wey3EDf8ABQH4VWazoZY/g7G7xhhuVW1W/AJHUAlWx64PoaAPxkooAKKACigAooAKKACv63v+DUD/AJQlfDj/ALGDxF/6d7mgD9HqKACv4g/+CsX/AClN/aW/7OA8Zf8Ap8vKAPn+igAooAKKACigAooAK/o+/wCDIb/k1j44f9lA0/8A9ITQB+31FABX8oX/AAd5f8pkta/7J/oP/ol6APzAooAKKACigAooAKKACigAooAKKACigAr+j7/gyG/5NY+OH/ZQNP8A/SE0Aft9RQAV/KF/wd5f8pkta/7J/oP/AKJegD8wKKACigAooAKKACigAr+v3/g1x/5QUfAz/uZv/Un1agD7/ooAK/iD/wCCsX/KU39pb/s4Dxl/6fLygD5/ooAKKACigAooAKKACigAooAKKACigD+j7/gyG/5NY+OH/ZQNP/8ASE1+31ABRQB/IF/wdHf8p1/jn/3LP/qMaTXwBQAUUAFFABX73f8ABrP/AMF7BoNxov8AwTB/bL8cN9imdbX4PeL9Wuj/AKO5OF0OeRv4GPFszH5T+4BwYVUA/oOooAKKAPyL/wCDjP8A4N5LD9vrRr/9s79j/Q4LP40aVYA65oMe2OHxpaxJhVycKl+iALHIcCVVWNyMI6/zA65oeteGdavPDXiXR7rT9R0+6ktr+wvrdoZraaNirxSIwDI6sCpUgEEEEZoAq0UAFFABRQAV9rf8G6fj2L4cf8FqfgD4hmuvJW48VXWl7/l5N9p13ZBfm4+Y3AX1545xQB/ZNRQAUUAFFABRQAUUAFFABRQAUUAFFABXPfFvx1a/C74V+JviZfSxRweHfD97qczzj5FW3geUluRxhOeRx3FAH8ElFABRQAUUAFFAH93n7J//ACaz8Nf+yf6N/wCkMNegUAFc/wDFj4W+Bfjj8LPE3wU+KWhf2p4Z8YeH7zRPEWm/apYPtdhdwPBcQ+ZEyyR7opHXcjKy5ypBANAHxB/xC4/8EKP+jGv/ADJnif8A+WdH/ELj/wAEKP8Aoxr/AMyZ4n/+WdAB/wAQuP8AwQo/6Ma/8yZ4n/8AlnR/xC4/8EKP+jGv/MmeJ/8A5Z0AH/ELj/wQo/6Ma/8AMmeJ/wD5Z0f8QuP/AAQo/wCjGv8AzJnif/5Z0AH/ABC4/wDBCj/oxr/zJnif/wCWdH/ELj/wQo/6Ma/8yZ4n/wDlnQBe0T/g2S/4Ib+HpZJrD9hKxkaRdrC+8c+ILoD6Ca/YKfcYNe/fsyf8Ezv+Cf8A+xrfx65+zJ+yJ4E8I6rFF5UevafoMb6kEK7Sv2yUPcYI6jfg9TkmgD3OigAqn4h17SvCugX3ifXrxbex02zlury4fpFFGhd2PsFBNAH8DniHW7vxLr994jvo40n1C8luZlhUhVZ3LEDJJxk8ZJ+tUqACigAooAK/tS/4Id+PYviR/wAEhv2dfEMN35y2/wAKdK0vd8vBsYRZFfl4+U25X1455zQB9VUUAFFABRQAUUAFFABRQAUUAFFABRQAV/LD/wAHkPjq18Xf8FcNP8PwSxM3hf4Q6Npk6xjlWe7v73Dcnnbdqe3BXjuQD8oaKACigAooAKKACigAr+t7/g1A/wCUJXw4/wCxg8Rf+ne5oA/R6igAr+IP/grF/wApTf2lv+zgPGX/AKfLygD5/ooAKKACigAooAKKACv6Pv8AgyG/5NY+OH/ZQNP/APSE0Aft9RQAUUAFFABRQAUUAFFABRQAUUAfzw/8Hxf/ACVL9nX/ALF/xJ/6P0+vwhoAKKACigAr+j7/AIMhv+TWPjh/2UDT/wD0hNAH7fUUAFfyhf8AB3l/ymS1r/sn+g/+iXoA/MCigAooAKKACigAooAK/r9/4Ncf+UFHwM/7mb/1J9WoA+/6KACv4g/+CsX/AClN/aW/7OA8Zf8Ap8vKAPn+igAooAKKACigAooAKKACigAooAKKAP6Pv+DIb/k1j44f9lA0/wD9ITX7fUAFFAH8gX/B0d/ynX+Of/cs/wDqMaTXwBQAUUAFFABUlvcT2k8d3aTvFNE4eOSNirIwOQQR0IPegD+oj/g2f/4LyQft4fDu1/Yt/at8Yx/8Ll8J6bjQ9Y1C4Ak8Z6bEv+syfv30KD96vLSoPO+YiYr+uFABRQAV+Nv/AAccf8G6Vr+2RY6v+3R+xB4Vjg+LNpbm48YeD7NAsfjGJF5nhHRdQVR06XAGP9aAZAD+ZzUNP1DSL+fSdWsZrW6tZmhura4jKSRSKSGRlIBVgQQQeQRg1BQAUUAFFABXoH7KXxpuv2b/ANqH4cftC2ZfzPAvjvSdfURrkt9jvIrgrjvkR4weDnB4oA/uy0TWtK8SaLZ+I9Bv47qx1C1jubO6hbKTROoZHB7gqQR9atUAFFABRQAUUAFFABRQAUUAFFABRQAV8e/8F9P2gbb9mz/gkD8dvHTan9lutU8EzeG9NZX2yNcaoy6ePL77lW5aTI5URs38OaAP4xaKACigAooAKKAP7vP2T/8Ak1n4a/8AZP8ARv8A0hhr0CgAooAKKACigAooAKKACigAooAK+Nf+C/n7XWlfsZ/8Envi54/k1lLTWvEnh6Xwl4VXzNskuoakjWwMXIy8ULT3HstuxwcYoA/jNooAKKACigAr+rL/AINCf2g7P4t/8EjbD4VSagJL74X+NtW0WS3b7629xL/aUT+6k3sqg/8ATIjoooA/UqigAooAKKACigAooAKKACigAooAKKACv4wf+C9n7QNv+0v/AMFfPjt8SNN1P7XY2vjR9B0+VX3RmHS4Y9ODR9tjG1ZwRw28t1YmgD5BooAKKACigAooAKKACv63v+DUD/lCV8OP+xg8Rf8Ap3uaAP0eooAK/iD/AOCsX/KU39pb/s4Dxl/6fLygD5/ooAKKACigAooAKKACv6Pv+DIb/k1j44f9lA0//wBITQB+31FABRQAUUAFFABRQAUUAFFABRQB/PD/AMHxf/JUv2df+xf8Sf8Ao/T6/CGgAooAKKACv6Pv+DIb/k1j44f9lA0//wBITQB+31FABX8oX/B3l/ymS1r/ALJ/oP8A6JegD8wKKACigAooAKKACigAr+v3/g1x/wCUFHwM/wC5m/8AUn1agD7/AKKACv4g/wDgrF/ylN/aW/7OA8Zf+ny8oA+f6KACigAooAKKACigAooAKKACigAooA/o+/4Mhv8Ak1j44f8AZQNP/wDSE1+31ABRQB/IF/wdHf8AKdf45/8Acs/+oxpNfAFABRQAUUAFFAG/8Lvih8Qvgp8RdF+Lfwn8X32geJfDupRX+h61psxjns7mNgySKfUEdDkEZBBBIr+vH/ghP/wWc+Hf/BWn9nFZNensdH+LvhG1ig+IHhaOUL53RV1O1Xgm2mPUAHyZCY2JHlvIAfdlFABRQB+Jv/Bx9/wbjWv7Rtrrn7fn7BfguOH4hQxyX3xA8B6bCFXxQgBaS+tEHAvwAWeMf8fPLL++yJ/5t5I3idkkQqynDKw5BoAbRQAUUAFFAH9aH/Brp/wUi8P/ALbP/BOrQvgp4n8Q27fEL4MWcHhvWtPecefc6VEgTTr4KTkoYVEDNyTLbOTjeuf0soAKKACigAooAKKACigAooAKKACigAr+d/8A4PL/APgpH4f8c+K/CP8AwTT+F3iGO6TwrfL4l+JD2swZY79oWSxsW2n70cM007ocj9/bnhkOAD8I6KACigAooAKKAP7vP2T/APk1n4a/9k/0b/0hhr0CgAooAKKACigAooAKKACigAooAp69r2h+FtEvPEvifWrTTdN0+2e4vtQv7hYYLaFFLPJI7kKiKASWJAAGSa/k7/4OT/8Agsvp3/BT39pax+FnwK1eWT4O/DOe4h8O3O0oPEGpOdk+qFevlFVWOAMNwj3v8pmZFAPzTooAKKACigAr9Uf+DTv/AIKReH/2Lf28L39n34reIbfTfBXxrtbfS5L+9mEcNjrdu0jafIzE4VZPOntj0y9xESQqGgD+quigAooAKKACigAooAKKACigAooAKKAPln/gsf8A8FFPCf8AwTI/YO8Y/tCX+rWy+KLiyfSfh3pkzLvvtcnjYW+EP30i+a4kH/POFx1Iz/FffX17qd7NqWpXktxcXErS3FxPIXeV2OWZmPLMSSSTyTQBDRQAUUAFFABRQAUUAFf1vf8ABqB/yhK+HH/YweIv/Tvc0Afo9RQAV/EH/wAFYv8AlKb+0t/2cB4y/wDT5eUAfP8ARQAUUAFFABRQAUUAFf0ff8GQ3/JrHxw/7KBp/wD6QmgD9vqKACigAooAKKACigAooAKKACigD+eH/g+L/wCSpfs6/wDYv+JP/R+n1+ENABRQAUUAFf0ff8GQ3/JrHxw/7KBp/wD6QmgD9vqKACv5Qv8Ag7y/5TJa1/2T/Qf/AES9AH5gUUAFFABRQAUUAFFABX9fv/Brj/ygo+Bn/czf+pPq1AH3/RQAV/EH/wAFYv8AlKb+0t/2cB4y/wDT5eUAfP8ARQAUUAFFABRQAUUAFFABRQAUUAFFAH9H3/BkN/yax8cP+ygaf/6Qmv2+oAKKAP5Av+Do7/lOv8c/+5Z/9RjSa+AKACigAooAKKACvWP2Jv2zfjr+wF+0l4d/ah/Z48Tvpuv+H7nMkLMTb6laNgTWVygP7yCVRtZeoO11KuisoB/ZB/wTL/4KO/Av/gqH+ytov7S3wVvFt5ZlFr4q8MTXKyXXh/VFUGazlwBuAyGjkwokjZHwpJVfoSgAooAK/Cv/AIOTv+Dcuy+JVj4i/wCCin7BPgtYfE1vHLqXxM+Hml2/y6ygBeXU7GNRxdAZaaFR+/GXQebuWYA/nXooAKKACigD1b9jP9tT9o79gT48aX+0b+y78Qp/D/iTTQ0Uh2CS21C1YjzLS6hb5Z4HwMqeQQrqVdFZf6Kf2BP+Dwj9hP48aHp/hf8AbX0LUfg74u2LHealHZzaloFzLyN0csKtcW4YgErLGVj3AGZwC1AH6R/B39vX9iL9oKwTUvgh+138NfFUbqCY9D8a2NxLHkE4eNZS8bYBO1gCMHIr1iORZFDowZWGVZT1oAdRQAUUAFFABRQAUUAFFAFXVdY0jQbJtT1zVbaztkID3F3Osca5OBlmIHWvAfj1/wAFav8Agmd+zLYz3nxp/bl+Guly267pNNtfFEF9f4wDxaWhluG4I6RnrQB+R/8AwU//AODyDQL7wtqXwj/4JeeBdSTULpWgk+KnjCwSJbZCCDJYWLFmdzkFZbnZtIOYGyGH4F+KfFPibxz4m1Dxp408QXmraxq15LeapqmpXTzXF5cSOXklkkclndmJYsSSSSSaAM+igAooAKKACigD+7z9k/8A5NZ+Gv8A2T/Rv/SGGvQKACigAooAKKACigAooAKKAPMPih+2x+xv8EbCTU/jH+1h8N/C0EKbnk8QeN7C047YEsoJJyMAZJJAGSa+Hf2t/wDg6/8A+CTP7N+m3ll8MviJrHxc8QQqVh0vwNpMi2pk5xvvroRw7MjlojMQDwrdKAPwq/4Kvf8ABwz+29/wVOguPhvrd1b/AA9+FzTK8fw98L3Tst7tO5Wv7ohXvSG5C7Y4QQrCLeu+vgmgAooAKKACigAp0bvG6yRuVZTlWU8g0Afth/wSQ/4O7PiJ+zz4S0r9n/8A4KO+FdZ8f+HtOijtdJ+I2itHJrlpCu1ES8ikKLfKq/8ALbes+F+YTs2R+0X7N/8AwW4/4JS/tV2FrcfCb9uXwFHeXgUR6J4m1hdF1DzD/wAsxbX/AJMjsDx8gYHqCRgkA+mPDnivwv4x08at4R8S6fqlqcYudNvEnjOQCPmQkcgg/Q5rQoAKKACigAooAKKACigAooA87+Kn7XX7KXwMs5tQ+NP7TPgDwjDDnzZPEnjGysdpG7I/fSrz8rcdcqR2r4A/be/4Ozf+CYH7MGlXmk/AnxFqXxq8VRqy2+n+EIWt9MSUZx52oXCBNhx963S4PI4xkgA/nS/4KV/8FRP2qf8Agqf8bx8Zf2lPEsIt9Pjkt/C/hTSVaPTNCtmbJjgjZiS7YXfM5aSQquTtRFX5zoAKKACigAooAKKACigAr+t7/g1A/wCUJXw4/wCxg8Rf+ne5oA/R6igAr+IP/grF/wApTf2lv+zgPGX/AKfLygD5/ooAKKACigAooAKKACv6Pv8AgyG/5NY+OH/ZQNP/APSE0Aft9RQAUUAFFABRQAUUAFFABRQAUUAfzw/8Hxf/ACVL9nX/ALF/xJ/6P0+vwhoAKKACigAr+j7/AIMhv+TWPjh/2UDT/wD0hNAH7fUUAFfyhf8AB3l/ymS1r/sn+g/+iXoA/MCigAooAKKACigAooAK/r9/4Ncf+UFHwM/7mb/1J9WoA+/6KACv4g/+CsX/AClN/aW/7OA8Zf8Ap8vKAPn+igAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooA/rmP8Awanf8EQf+jWNW/8ADja3/wDJdH/EKd/wRA/6NY1b/wAONrf/AMl0AH/EKd/wRA/6NY1b/wAONrf/AMl0f8Qp3/BED/o1jVv/AA42t/8AyXQAf8Qp3/BED/o1jVv/AA42t/8AyXR/xCnf8EQP+jWNW/8ADja3/wDJdAB/xCnf8EQP+jWNW/8ADja3/wDJdH/EKd/wRA/6NY1b/wAONrf/AMl0AH/EKd/wRA/6NY1b/wAONrf/AMl0f8Qp3/BED/o1jVv/AA42t/8AyXQAf8Qp3/BED/o1jVv/AA42t/8AyXR/xCnf8EQP+jWNW/8ADja3/wDJdAB/xCnf8EQP+jWNW/8ADja3/wDJdH/EKd/wRA/6NY1b/wAONrf/AMl0AH/EKd/wRA/6NY1b/wAONrf/AMl0f8Qp3/BED/o1jVv/AA42t/8AyXQAf8Qp3/BED/o1jVv/AA42t/8AyXR/xCnf8EQP+jWNW/8ADja3/wDJdAB/xCnf8EQP+jWNW/8ADja3/wDJdH/EKd/wRA/6NY1b/wAONrf/AMl0AH/EKd/wRA/6NY1b/wAONrf/AMl0f8Qp3/BED/o1jVv/AA42t/8AyXQAf8Qp3/BED/o1jVv/AA42t/8AyXR/xCnf8EQP+jWNW/8ADja3/wDJdAB/xCnf8EQP+jWNW/8ADja3/wDJdH/EKd/wRA/6NY1b/wAONrf/AMl0AH/EKd/wRA/6NY1b/wAONrf/AMl0f8Qp3/BED/o1jVv/AA42t/8AyXQAf8Qp3/BED/o1jVv/AA42t/8AyXR/xCnf8EQP+jWNW/8ADja3/wDJdAB/xCnf8EQP+jWNW/8ADja3/wDJdH/EKd/wRA/6NY1b/wAONrf/AMl0AH/EKd/wRA/6NY1b/wAONrf/AMl0f8Qp3/BED/o1jVv/AA42t/8AyXQAf8Qp3/BED/o1jVv/AA42t/8AyXR/xCnf8EQP+jWNW/8ADja3/wDJdAB/xCnf8EQP+jWNW/8ADja3/wDJdH/EKd/wRA/6NY1b/wAONrf/AMl0AH/EKd/wRA/6NY1b/wAONrf/AMl0f8Qp3/BED/o1jVv/AA42t/8AyXQAf8Qp3/BED/o1jVv/AA42t/8AyXR/xCnf8EQP+jWNW/8ADja3/wDJdAB/xCnf8EQP+jWNW/8ADja3/wDJdH/EKd/wRA/6NY1b/wAONrf/AMl0AH/EKd/wRA/6NY1b/wAONrf/AMl0f8Qp3/BED/o1jVv/AA42t/8AyXQAf8Qp3/BED/o1jVv/AA42t/8AyXR/xCnf8EQP+jWNW/8ADja3/wDJdAB/xCnf8EQP+jWNW/8ADja3/wDJdH/EKd/wRA/6NY1b/wAONrf/AMl0AH/EKd/wRA/6NY1b/wAONrf/AMl0f8Qp3/BED/o1jVv/AA42t/8AyXQAf8Qp3/BED/o1jVv/AA42t/8AyXR/xCnf8EQP+jWNW/8ADja3/wDJdAB/xCnf8EQP+jWNW/8ADja3/wDJdH/EKd/wRA/6NY1b/wAONrf/AMl0AH/EKd/wRA/6NY1b/wAONrf/AMl0f8Qp3/BED/o1jVv/AA42t/8AyXQAf8Qp3/BED/o1jVv/AA42t/8AyXR/xCnf8EQP+jWNW/8ADja3/wDJdAB/xCnf8EQP+jWNW/8ADja3/wDJdH/EKd/wRA/6NY1b/wAONrf/AMl0AH/EKd/wRA/6NY1b/wAONrf/AMl0f8Qp3/BED/o1jVv/AA42t/8AyXQAf8Qp3/BED/o1jVv/AA42t/8AyXR/xCnf8EQP+jWNW/8ADja3/wDJdAB/xCnf8EQP+jWNW/8ADja3/wDJdH/EKd/wRA/6NY1b/wAONrf/AMl0AH/EKd/wRA/6NY1b/wAONrf/AMl0f8Qp3/BED/o1jVv/AA42t/8AyXQAf8Qp3/BED/o1jVv/AA42t/8AyXR/xCnf8EQP+jWNW/8ADja3/wDJdAB/xCnf8EQP+jWNW/8ADja3/wDJdH/EKd/wRA/6NY1b/wAONrf/AMl0AH/EKd/wRA/6NY1b/wAONrf/AMl0f8Qp3/BED/o1jVv/AA42t/8AyXQAf8Qp3/BED/o1jVv/AA42t/8AyXR/xCnf8EQP+jWNW/8ADja3/wDJdAB/xCnf8EQP+jWNW/8ADja3/wDJdH/EKd/wRA/6NY1b/wAONrf/AMl0AH/EKd/wRA/6NY1b/wAONrf/AMl0f8Qp3/BED/o1jVv/AA42t/8AyXQAf8Qp3/BED/o1jVv/AA42t/8AyXR/xCnf8EQP+jWNW/8ADja3/wDJdAB/xCnf8EQP+jWNW/8ADja3/wDJdH/EKd/wRA/6NY1b/wAONrf/AMl0AH/EKd/wRA/6NY1b/wAONrf/AMl0f8Qp3/BED/o1jVv/AA42t/8AyXQAf8Qp3/BED/o1jVv/AA42t/8AyXR/xCnf8EQP+jWNW/8ADja3/wDJdAB/xCnf8EQP+jWNW/8ADja3/wDJdH/EKd/wRA/6NY1b/wAONrf/AMl0AH/EKd/wRA/6NY1b/wAONrf/AMl0f8Qp3/BED/o1jVv/AA42t/8AyXQAf8Qp3/BED/o1jVv/AA42t/8AyXR/xCnf8EQP+jWNW/8ADja3/wDJdAB/xCnf8EQP+jWNW/8ADja3/wDJdH/EKd/wRA/6NY1b/wAONrf/AMl0AH/EKd/wRA/6NY1b/wAONrf/AMl0f8Qp3/BED/o1jVv/AA42t/8AyXQAf8Qp3/BED/o1jVv/AA42t/8AyXR/xCnf8EQP+jWNW/8ADja3/wDJdAB/xCnf8EQP+jWNW/8ADja3/wDJdH/EKd/wRA/6NY1b/wAONrf/AMl0AH/EKd/wRA/6NY1b/wAONrf/AMl0f8Qp3/BED/o1jVv/AA42t/8AyXQAf8Qp3/BED/o1jVv/AA42t/8AyXR/xCnf8EQP+jWNW/8ADja3/wDJdAB/xCnf8EQP+jWNW/8ADja3/wDJdH/EKd/wRA/6NY1b/wAONrf/AMl0AH/EKd/wRA/6NY1b/wAONrf/AMl0f8Qp3/BED/o1jVv/AA42t/8AyXQAf8Qp3/BED/o1jVv/AA42t/8AyXR/xCnf8EQP+jWNW/8ADja3/wDJdAB/xCnf8EQP+jWNW/8ADja3/wDJdH/EKd/wRA/6NY1b/wAONrf/AMl0AH/EKd/wRA/6NY1b/wAONrf/AMl0f8Qp3/BED/o1jVv/AA42t/8AyXQAf8Qp3/BED/o1jVv/AA42t/8AyXR/xCnf8EQP+jWNW/8ADja3/wDJdAB/xCnf8EQP+jWNW/8ADja3/wDJdH/EKd/wRA/6NY1b/wAONrf/AMl0AH/EKd/wRA/6NY1b/wAONrf/AMl0f8Qp3/BED/o1jVv/AA42t/8AyXQAf8Qp3/BED/o1jVv/AA42t/8AyXR/xCnf8EQP+jWNW/8ADja3/wDJdAB/xCnf8EQP+jWNW/8ADja3/wDJdH/EKd/wRA/6NY1b/wAONrf/AMl0AH/EKd/wRA/6NY1b/wAONrf/AMl0f8Qp3/BED/o1jVv/AA42t/8AyXQAf8Qp3/BED/o1jVv/AA42t/8AyXR/xCnf8EQP+jWNW/8ADja3/wDJdAB/xCnf8EQP+jWNW/8ADja3/wDJdH/EKd/wRA/6NY1b/wAONrf/AMl0AH/EKd/wRA/6NY1b/wAONrf/AMl0f8Qp3/BED/o1jVv/AA42t/8AyXQAf8Qp3/BED/o1jVv/AA42t/8AyXR/xCnf8EQP+jWNW/8ADja3/wDJdAB/xCnf8EQP+jWNW/8ADja3/wDJdH/EKd/wRA/6NY1b/wAONrf/AMl0AH/EKd/wRA/6NY1b/wAONrf/AMl0f8Qp3/BED/o1jVv/AA42t/8AyXQAf8Qp3/BED/o1jVv/AA42t/8AyXR/xCnf8EQP+jWNW/8ADja3/wDJdAB/xCnf8EQP+jWNW/8ADja3/wDJdH/EKd/wRA/6NY1b/wAONrf/AMl0AH/EKd/wRA/6NY1b/wAONrf/AMl0f8Qp3/BED/o1jVv/AA42t/8AyXQAf8Qp3/BED/o1jVv/AA42t/8AyXR/xCnf8EQP+jWNW/8ADja3/wDJdAB/xCnf8EQP+jWNW/8ADja3/wDJdH/EKd/wRA/6NY1b/wAONrf/AMl0AH/EKd/wRA/6NY1b/wAONrf/AMl0f8Qp3/BED/o1jVv/AA42t/8AyXQAf8Qp3/BED/o1jVv/AA42t/8AyXR/xCnf8EQP+jWNW/8ADja3/wDJdAB/xCnf8EQP+jWNW/8ADja3/wDJdH/EKd/wRA/6NY1b/wAONrf/AMl0AH/EKd/wRA/6NY1b/wAONrf/AMl0f8Qp3/BED/o1jVv/AA42t/8AyXQAf8Qp3/BED/o1jVv/AA42t/8AyXR/xCnf8EQP+jWNW/8ADja3/wDJdAB/xCnf8EQP+jWNW/8ADja3/wDJdH/EKd/wRA/6NY1b/wAONrf/AMl0AH/EKd/wRA/6NY1b/wAONrf/AMl0f8Qp3/BED/o1jVv/AA42t/8AyXQAf8Qp3/BED/o1jVv/AA42t/8AyXR/xCnf8EQP+jWNW/8ADja3/wDJdAB/xCnf8EQP+jWNW/8ADja3/wDJdH/EKd/wRA/6NY1b/wAONrf/AMl0AH/EKd/wRA/6NY1b/wAONrf/AMl0f8Qp3/BED/o1jVv/AA42t/8AyXQAf8Qp3/BED/o1jVv/AA42t/8AyXR/xCnf8EQP+jWNW/8ADja3/wDJdAB/xCnf8EQP+jWNW/8ADja3/wDJdH/EKd/wRA/6NY1b/wAONrf/AMl0AH/EKd/wRA/6NY1b/wAONrf/AMl0f8Qp3/BED/o1jVv/AA42t/8AyXQAf8Qp3/BED/o1jVv/AA42t/8AyXR/xCnf8EQP+jWNW/8ADja3/wDJdAB/xCnf8EQP+jWNW/8ADja3/wDJdH/EKd/wRA/6NY1b/wAONrf/AMl0AH/EKd/wRA/6NY1b/wAONrf/AMl0f8Qp3/BED/o1jVv/AA42t/8AyXQAf8Qp3/BED/o1jVv/AA42t/8AyXR/xCnf8EQP+jWNW/8ADja3/wDJdAB/xCnf8EQP+jWNW/8ADja3/wDJdH/EKd/wRA/6NY1b/wAONrf/AMl0AH/EKd/wRA/6NY1b/wAONrf/AMl0UAfotRQAUUAFFABRQAUUAFFABRQAUUAFFABRQAUUAFFABRQAUUAFFABRQAUUAFFABRQAUUAFFABRQAUUAFFABRQAUUAFFABRQAUUAFFABRQAUUAFFABRQAUUAFFABRQAUUAFFABRQAUUAFFABRQAUUAFFABRQAUUAFFABRQAUUAFFABRQAUUAFFABRQAUUAFFABRQAUUAFFABRQAUUAFFABRQAUUAFFABRQAUUAFFABRQAUUAFFABRQAUUAFFABRQAUUAFFABRQAUUAFFABRQAUUAFFABRQAUUAFFABRQAUUAFFABRQAUUAFFABRQAUUAfzK/wDEab/wUA/6N6+G/wD3xdf/AByj/iNP/b//AOjefht/3xdf/HKAD/iNP/b/AP8Ao3n4bf8AfF1/8co/4jT/ANv/AP6N5+G3/fF1/wDHKAD/AIjT/wBv/wD6N5+G3/fF1/8AHKP+I0/9v/8A6N5+G3/fF1/8coAP+I0/9v8A/wCjefht/wB8XX/xyj/iNP8A2/8A/o3n4bf98XX/AMcoAP8AiNP/AG//APo3n4bf98XX/wAco/4jT/2//wDo3n4bf98XX/xygA/4jT/2/wD/AKN5+G3/AHxdf/HKP+I0/wDb/wD+jefht/3xdf8AxygA/wCI0/8Ab/8A+jefht/3xdf/AByj/iNP/b//AOjefht/3xdf/HKAD/iNP/b/AP8Ao3n4bf8AfF1/8co/4jT/ANv/AP6N5+G3/fF1/wDHKAD/AIjT/wBv/wD6N5+G3/fF1/8AHKP+I0/9v/8A6N5+G3/fF1/8coAP+I0/9v8A/wCjefht/wB8XX/xyj/iNP8A2/8A/o3n4bf98XX/AMcoAP8AiNP/AG//APo3n4bf98XX/wAco/4jT/2//wDo3n4bf98XX/xygA/4jT/2/wD/AKN5+G3/AHxdf/HKP+I0/wDb/wD+jefht/3xdf8AxygA/wCI0/8Ab/8A+jefht/3xdf/AByj/iNP/b//AOjefht/3xdf/HKAD/iNP/b/AP8Ao3n4bf8AfF1/8co/4jT/ANv/AP6N5+G3/fF1/wDHKAD/AIjT/wBv/wD6N5+G3/fF1/8AHKP+I0/9v/8A6N5+G3/fF1/8coAP+I0/9v8A/wCjefht/wB8XX/xyj/iNP8A2/8A/o3n4bf98XX/AMcoAP8AiNP/AG//APo3n4bf98XX/wAco/4jT/2//wDo3n4bf98XX/xygA/4jT/2/wD/AKN5+G3/AHxdf/HKP+I0/wDb/wD+jefht/3xdf8AxygA/wCI0/8Ab/8A+jefht/3xdf/AByj/iNP/b//AOjefht/3xdf/HKAD/iNP/b/AP8Ao3n4bf8AfF1/8co/4jT/ANv/AP6N5+G3/fF1/wDHKAD/AIjT/wBv/wD6N5+G3/fF1/8AHKP+I0/9v/8A6N5+G3/fF1/8coAP+I0/9v8A/wCjefht/wB8XX/xyj/iNP8A2/8A/o3n4bf98XX/AMcoAP8AiNP/AG//APo3n4bf98XX/wAco/4jT/2//wDo3n4bf98XX/xygA/4jT/2/wD/AKN5+G3/AHxdf/HKP+I0/wDb/wD+jefht/3xdf8AxygA/wCI0/8Ab/8A+jefht/3xdf/AByj/iNP/b//AOjefht/3xdf/HKAD/iNP/b/AP8Ao3n4bf8AfF1/8co/4jT/ANv/AP6N5+G3/fF1/wDHKAD/AIjT/wBv/wD6N5+G3/fF1/8AHKP+I0/9v/8A6N5+G3/fF1/8coAP+I0/9v8A/wCjefht/wB8XX/xyj/iNP8A2/8A/o3n4bf98XX/AMcoAP8AiNP/AG//APo3n4bf98XX/wAco/4jT/2//wDo3n4bf98XX/xygA/4jT/2/wD/AKN5+G3/AHxdf/HKP+I0/wDb/wD+jefht/3xdf8AxygA/wCI0/8Ab/8A+jefht/3xdf/AByj/iNP/b//AOjefht/3xdf/HKAD/iNP/b/AP8Ao3n4bf8AfF1/8co/4jT/ANv/AP6N5+G3/fF1/wDHKAD/AIjT/wBv/wD6N5+G3/fF1/8AHKP+I0/9v/8A6N5+G3/fF1/8coAP+I0/9v8A/wCjefht/wB8XX/xyj/iNP8A2/8A/o3n4bf98XX/AMcoAP8AiNP/AG//APo3n4bf98XX/wAco/4jT/2//wDo3n4bf98XX/xygA/4jT/2/wD/AKN5+G3/AHxdf/HKP+I0/wDb/wD+jefht/3xdf8AxygA/wCI0/8Ab/8A+jefht/3xdf/AByj/iNP/b//AOjefht/3xdf/HKAD/iNP/b/AP8Ao3n4bf8AfF1/8co/4jT/ANv/AP6N5+G3/fF1/wDHKAD/AIjT/wBv/wD6N5+G3/fF1/8AHKP+I0/9v/8A6N5+G3/fF1/8coAP+I0/9v8A/wCjefht/wB8XX/xyj/iNP8A2/8A/o3n4bf98XX/AMcoAP8AiNP/AG//APo3n4bf98XX/wAco/4jT/2//wDo3n4bf98XX/xygA/4jT/2/wD/AKN5+G3/AHxdf/HKP+I0/wDb/wD+jefht/3xdf8AxygA/wCI0/8Ab/8A+jefht/3xdf/AByj/iNP/b//AOjefht/3xdf/HKAD/iNP/b/AP8Ao3n4bf8AfF1/8co/4jT/ANv/AP6N5+G3/fF1/wDHKAD/AIjT/wBv/wD6N5+G3/fF1/8AHKP+I0/9v/8A6N5+G3/fF1/8coAP+I0/9v8A/wCjefht/wB8XX/xyj/iNP8A2/8A/o3n4bf98XX/AMcoAP8AiNP/AG//APo3n4bf98XX/wAco/4jT/2//wDo3n4bf98XX/xygA/4jT/2/wD/AKN5+G3/AHxdf/HKP+I0/wDb/wD+jefht/3xdf8AxygA/wCI0/8Ab/8A+jefht/3xdf/AByj/iNP/b//AOjefht/3xdf/HKAD/iNP/b/AP8Ao3n4bf8AfF1/8co/4jT/ANv/AP6N5+G3/fF1/wDHKAD/AIjT/wBv/wD6N5+G3/fF1/8AHKP+I0/9v/8A6N5+G3/fF1/8coAP+I0/9v8A/wCjefht/wB8XX/xyj/iNP8A2/8A/o3n4bf98XX/AMcoAP8AiNP/AG//APo3n4bf98XX/wAco/4jT/2//wDo3n4bf98XX/xygA/4jT/2/wD/AKN5+G3/AHxdf/HKP+I0/wDb/wD+jefht/3xdf8AxygA/wCI0/8Ab/8A+jefht/3xdf/AByj/iNP/b//AOjefht/3xdf/HKAD/iNP/b/AP8Ao3n4bf8AfF1/8co/4jT/ANv/AP6N5+G3/fF1/wDHKAD/AIjT/wBv/wD6N5+G3/fF1/8AHKP+I0/9v/8A6N5+G3/fF1/8coAP+I0/9v8A/wCjefht/wB8XX/xyj/iNP8A2/8A/o3n4bf98XX/AMcoAP8AiNP/AG//APo3n4bf98XX/wAco/4jT/2//wDo3n4bf98XX/xygA/4jT/2/wD/AKN5+G3/AHxdf/HKP+I0/wDb/wD+jefht/3xdf8AxygA/wCI0/8Ab/8A+jefht/3xdf/AByj/iNP/b//AOjefht/3xdf/HKAD/iNP/b/AP8Ao3n4bf8AfF1/8co/4jT/ANv/AP6N5+G3/fF1/wDHKAD/AIjT/wBv/wD6N5+G3/fF1/8AHKP+I0/9v/8A6N5+G3/fF1/8coAP+I0/9v8A/wCjefht/wB8XX/xyj/iNP8A2/8A/o3n4bf98XX/AMcoAP8AiNP/AG//APo3n4bf98XX/wAco/4jT/2//wDo3n4bf98XX/xygA/4jT/2/wD/AKN5+G3/AHxdf/HKP+I0/wDb/wD+jefht/3xdf8AxygA/wCI0/8Ab/8A+jefht/3xdf/AByj/iNP/b//AOjefht/3xdf/HKAD/iNP/b/AP8Ao3n4bf8AfF1/8co/4jT/ANv/AP6N5+G3/fF1/wDHKAD/AIjT/wBv/wD6N5+G3/fF1/8AHKP+I0/9v/8A6N5+G3/fF1/8coAP+I0/9v8A/wCjefht/wB8XX/xyj/iNP8A2/8A/o3n4bf98XX/AMcoAP8AiNP/AG//APo3n4bf98XX/wAco/4jT/2//wDo3n4bf98XX/xygA/4jT/2/wD/AKN5+G3/AHxdf/HKP+I0/wDb/wD+jefht/3xdf8AxygA/wCI0/8Ab/8A+jefht/3xdf/AByj/iNP/b//AOjefht/3xdf/HKAD/iNP/b/AP8Ao3n4bf8AfF1/8co/4jT/ANv/AP6N5+G3/fF1/wDHKAD/AIjT/wBv/wD6N5+G3/fF1/8AHKP+I0/9v/8A6N5+G3/fF1/8coAP+I0/9v8A/wCjefht/wB8XX/xyj/iNP8A2/8A/o3n4bf98XX/AMcoAP8AiNP/AG//APo3n4bf98XX/wAco/4jT/2//wDo3n4bf98XX/xygA/4jT/2/wD/AKN5+G3/AHxdf/HKP+I0/wDb/wD+jefht/3xdf8AxygA/wCI0/8Ab/8A+jefht/3xdf/AByj/iNP/b//AOjefht/3xdf/HKAD/iNP/b/AP8Ao3n4bf8AfF1/8co/4jT/ANv/AP6N5+G3/fF1/wDHKAD/AIjT/wBv/wD6N5+G3/fF1/8AHKP+I0/9v/8A6N5+G3/fF1/8coAP+I0/9v8A/wCjefht/wB8XX/xyj/iNP8A2/8A/o3n4bf98XX/AMcoAP8AiNP/AG//APo3n4bf98XX/wAco/4jT/2//wDo3n4bf98XX/xygA/4jT/2/wD/AKN5+G3/AHxdf/HKP+I0/wDb/wD+jefht/3xdf8AxygA/wCI0/8Ab/8A+jefht/3xdf/AByj/iNP/b//AOjefht/3xdf/HKAD/iNP/b/AP8Ao3n4bf8AfF1/8co/4jT/ANv/AP6N5+G3/fF1/wDHKAD/AIjT/wBv/wD6N5+G3/fF1/8AHKP+I0/9v/8A6N5+G3/fF1/8coAP+I0/9v8A/wCjefht/wB8XX/xyj/iNP8A2/8A/o3n4bf98XX/AMcoAP8AiNP/AG//APo3n4bf98XX/wAco/4jT/2//wDo3n4bf98XX/xygA/4jT/2/wD/AKN5+G3/AHxdf/HKP+I0/wDb/wD+jefht/3xdf8AxygA/wCI0/8Ab/8A+jefht/3xdf/AByj/iNP/b//AOjefht/3xdf/HKAD/iNP/b/AP8Ao3n4bf8AfF1/8co/4jT/ANv/AP6N5+G3/fF1/wDHKAD/AIjT/wBv/wD6N5+G3/fF1/8AHKP+I0/9v/8A6N5+G3/fF1/8coAP+I0/9v8A/wCjefht/wB8XX/xyj/iNP8A2/8A/o3n4bf98XX/AMcoAP8AiNP/AG//APo3n4bf98XX/wAcooA/G2igAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKfDDLcyrb28TSSSMFjjRcszHoAO5zQB+4X7M/wDwZTfFn4tfAPwr8Tfjn+24vw68V69o8d9rHgf/AIVedSfRWkyyW0lwdTg3yqhTzF8tQkhdAXCh2+GP+C1P/BEf4v8A/BHH4meGdG1v4g/8J94J8X6c0mh+Orfw+dOQ3kR/0ixmg8+fypUBR1PmESI+V5SRUAPiGtz4aaR4C8QfELRdD+KXjS98N+G7zU4Ydc8Qadoo1GfTrVnAkuEtfNh+0FFJby/MQtjAINAH7oeDv+DJXwr8QvCOl+PfA3/BWWz1fRda0+G+0jVdP+DSywXltKgeKaN11vDIysGDDgg1+MH7Y37J3xf/AGG/2lvF37LHxz0X7H4j8Ias9pcNGD5N5CQGgu4SQC0M0TJKhIB2uMgHIAB5lRQB+rX/AASN/wCDWb4t/wDBTT9laH9rD4hftK/8Kn0fWNTlh8I2E/gNtWn1ezi+V705vbbyYjKHjT7xfy2b5VKF+R8a/wDBAn4P6T/wWH8B/wDBID4ff8FCf+Em17xFpmoXHjLxdb/DFYoPDFxBpl1qMVn5A1VzdTPFbDePMiEQnjOZG3IoB9pf8QMf/WUX/wAwn/8Afqj/AIgY/wDrKL/5hP8A+/VAB/xAx/8AWUX/AMwn/wDfqj/iBj/6yi/+YT/+/VAHkv7dv/BoL/wxR+x78RP2sP8Ah4X/AMJN/wAID4Zn1f8A4R//AIVN9j+3+Xj915/9rS+VnP3vLbHoa/GGwsL7Vb6HTNMs5ri5uJlit7e3jLvLIxwqqo5ZiSAAOSaAP2u/Zj/4MpP2mviX8O7Hxd+0z+2B4e+G2rX9jFcDw3o/hGTXp7NnyTDcSG6tY1kUbc+WZV3bgGIAZvSv+IGP/rKL/wCYT/8Av1QAf8QMf/WUX/zCf/36o/4gY/8ArKL/AOYT/wDv1QBk+N/+DHDx1YeHZrn4cf8ABSHSdW1ZSPs9lrfwtl0+3cd900Wo3DLx6RNn2r8af2zf2Pfjh+wX+0h4k/ZY/aH0GGx8UeGLhEujZzGW1u4ZI1khuYJCF8yKSN1ZSQCM4ZVZWUAHltff/wDwQx/4IY/8Po/+Fo/8ZRf8K1/4Vr/Yn/Mk/wBsf2j/AGh9v/6fbbyfL+w/7e7zf4dvzAG1/wAFm/8Ag3M/aF/4JJ+DtI+NOi/Ev/hanw4vGW21rxbY+GW02TQr1n2xx3Vv9ouNsUuVWOcSFTJmNgjGPzPzmoAKKAP2c/YS/wCDOD4/ftPfs26H8cv2jP2ov+FPa14ij+12Xgi4+Hh1W7trJlUwyXTG/tvImcEsYNrNGu0OQ5ZE/Nv/AIKZfsT/APDun9uPx5+xl/wsv/hMf+EJuLKL/hJP7F/s/wC2/aLC2u8/Z/Om8vb9o2f6xs7M8ZwADweigDa+HXw78bfF3x/onwr+Gfhm61rxF4k1a30zQ9IsY9017dzyLHFCg7szsqj61+6fg/8A4McfE+o+E9L1Dxv/AMFJbXStauNOgk1fS7H4Rm8gs7powZYY5zq0RnRHLKJDHGXADbFztAB+WH/BWf8A4Jd/F7/gk3+1dd/s4/EnWv8AhINKurCPUfB/jSDTTa2+u2LjBkWIvJ5MkcgeKSIuxVlBBZHRm+YaAPv/AP4IY/8ABDH/AIfR/wDC0f8AjKL/AIVr/wAK1/sT/mSf7Y/tH+0Pt/8A0+23k+X9h/293m/w7fm+/wD/AIgY/wDrKL/5hP8A+/VAB/xAx/8AWUX/AMwn/wDfqj/iBj/6yi/+YT/+/VAB/wAQMf8A1lF/8wn/APfqvif/AILc/wDBvb/w5w+Dfgz4tf8ADXX/AAsb/hLvE0ukf2f/AMID/Y/2TZbtN5u/7fc+Znbt27V9c9qAPln/AIJcfsMf8PKP26/A37FX/C0f+EL/AOE0/tP/AIqX+xP7R+x/ZNLu7/8A49/Ph8zf9l8v/WLt37vm27T+iH/BR/8A4NK/+Hff7E3j79sX/hv7/hLv+EH06C6/4R3/AIVV9g+2+Zdw2+37R/ak3l487dny2ztxgZyAD8b69a/YS/Zd/wCG1/2wvh3+yf8A8Jx/wjP/AAn3iaDSP+Eg/sz7Z9h8zP73yPNi83GPu+YufUUAftJ/xAx/9ZRf/MJ//fqj/iBj/wCsov8A5hP/AO/VAB/xAx/9ZRf/ADCf/wB+qP8AiBj/AOsov/mE/wD79UAfjv8A8FMP2M9G/wCCe/7cHjz9jjQvjPa/EGDwPdWdtJ4stNLWyW6mlsLe5mjMCzziJ4ZZnt2XzWIaFs7TlV8JoAK/Uj/gjr/wbEfF/wD4Kn/s23n7Unjj9oX/AIVP4ZutWaz8GGbwSdXl16OIslzdAfbLYQwpKPKVsuZHSYYQRqXANT/grb/waw/F7/gmh+ytP+1f8Of2k/8AhbWj6LqMcfjDT4PAh0mfSLOT5Vvhi9ufOiWUokmNpQSK/KB2T8o6ACigD74/4JA/8G+f7U//AAV08Lat8W/B3jvQfAvw/wBF1j+y5vE2vQS3Et5dhY5JYrW2jx5vlxyIzM7xruZVBJ3bP0G/4gY/+sov/mE//v1QAf8AEDH/ANZRf/MJ/wD36o/4gY/+sov/AJhP/wC/VAHnX7SX/Bk7+0v8PPAN74o/Zp/bI8OfETVrKxlnXw7rfhGXQZbx1wRDBILq7jLsu7HmGNdwUFgGLL+Jl/YX2lX02manZzW9zbzNFcW9xGUeKRThlZTyrAggg8g0AQ1+v3/BLj/g1L/4eUfsKeBf21f+G8/+EL/4TT+0/wDimf8AhVv9pfY/smqXdh/x8f2pB5m/7L5n+rXbv287dxAPhL/grD/wT5/4df8A7bPiL9jr/hbn/Ccf2Dp2nXX/AAkX9g/2Z5/2q0juNv2fz59u3zNufMO7GcDOB83UAFFABX7j/szf8GY//DRf7N/w+/aD/wCHkP8AY/8AwnfgfSfEP9kf8Kf+0fYfttnFc+R5v9rp5uzzdu/Yu7Gdq5wAD8eP2sfgX/wy/wDtTfEv9mj/AISn+3P+Fd/EDWfDH9tfYfsv9ofYL6a1+0eTvk8rzPK37N77d2NzYyfP6ACv0I/4Il/8G/fxf/4LE6Z4u+Ic/wAXf+FZ+A/C8iWNv4qn8LHVTqmqna7WcMP2m3GI4WEkkhk+UywqFbexQA+lP29/+DO34v8A7Jf7KXi79o74Kftef8LU1XwfY/2jeeC4vhudLuLuxjObmSCUajcb5Io90nlbAXVGCkvtVvxjoAKKACigAooAKKAPoD/glx+wx/w8o/br8DfsVf8AC0f+EL/4TT+0/wDipf7E/tH7H9k0u7v/APj38+HzN/2Xy/8AWLt37vm27T+v3/EDH/1lF/8AMJ//AH6oAP8AiBj/AOsov/mE/wD79Uf8QMf/AFlF/wDMJ/8A36oAhv8A/gxnvY7GaTTP+CnkM1ysLG3hn+DJjjeTHyqzjWGKgnGWCsQOcHpX5Y/8FWv+CRH7Tv8AwSM+Luk/Dj4+XGk6xpPiazluvCfi7w9JI1nqccTKsseJFV4po98e+NgQBIhVmBBoA+Va+hv+CaH/AATQ/aM/4Ko/tGR/s7fs7QadbTW2nPqXiDxBrkzx2OkWKOiNNKyKzMxd0RI1Us7HsoZlAP1u0L/gxq1yfRrWbxL/AMFM7W01BoVN5a2Pwfa4hikx8ypK2rRtIoPRiiE/3R0q1/xAx/8AWUX/AMwn/wDfqgA/4gY/+sov/mE//v1R/wAQMf8A1lF/8wn/APfqgD57/wCChX/BoT+1V+x98DfEn7QXwP8A2idD+K+j+EtJk1PW9JHhybR9UNrEjPcSQQedcxzeWi79vmqzKG2qWAVvyFoAK/cf9mb/AIMx/wDhov8AZv8Ah9+0H/w8h/sf/hO/A+k+If7I/wCFP/aPsP22ziufI83+1083Z5u3fsXdjO1c4AB2/wDxAx/9ZRf/ADCf/wB+qP8AiBj/AOsov/mE/wD79UAH/EDH/wBZRf8AzCf/AN+q+VP+Cp3/AAap/tP/APBOz4B61+1F8OfjzofxU8F+GIUm8TCHQ5dI1Sxt2KKbkWpluI5YUZjvKzb1XD7Cu8xgH5WV0vwd+EfxD+PvxW8OfBP4S+GptY8TeK9at9K0HS7fAa4up5BHGmSQFG5hlmIVRkkgAmgD9sfhF/wY/fGXXvCEOo/HL/goD4d8Ma5IqmXSvDPw/m1m3jygLD7RNe2ZJDErxHggZzzgdR/xAx/9ZRf/ADCf/wB+qAD/AIgY/wDrKL/5hP8A+/VH/EDH/wBZRf8AzCf/AN+qAOV+Lv8AwY//ABn0HwnNqHwM/b/8OeKNajR2j0vxN4Bn0WCQhSVUTw3l4QS2F5jAAOcnpX4ofGL4R/EP4BfFbxH8E/i14am0fxN4U1q40rXtLuMFre6gkMciZBIYblOGUlWGCCQQaAP0a/4Iqf8ABt9/w+C/ZY1/9pf/AIbK/wCFd/2H8QLrwx/Yv/Cu/wC1vP8AJsbG6+0ed/aNtt3fbdmzYceXncd2F+v/APiBj/6yi/8AmE//AL9UAH/EDH/1lF/8wn/9+qP+IGP/AKyi/wDmE/8A79UAH/EDH/1lF/8AMJ//AH6r85/+C3//AARfP/BGn4h+AvAf/DSX/CyP+E40W8v/ALV/wh39j/YvImjj2bftlz5m7fnOVxjGDnNAHjP/AAS4/YY/4eUft1+Bv2Kv+Fo/8IX/AMJp/af/ABUv9if2j9j+yaXd3/8Ax7+fD5m/7L5f+sXbv3fNt2n9EP8Ago//AMGlf/Dvv9ibx9+2L/w39/wl3/CD6dBdf8I7/wAKq+wfbfMu4bfb9o/tSby8eduz5bZ24wM5AB+N9fTP/BJD/gnV/wAPTP20NJ/ZD/4XF/wgv9qaLqF//wAJD/wj/wDanlfZoTLs8j7RBu3YxnzBjrg9KAP1l/4gY/8ArKL/AOYT/wDv1R/xAx/9ZRf/ADCf/wB+qAD/AIgY/wDrKL/5hP8A+/VH/EDH/wBZRf8AzCf/AN+qAPw4/aW+E2jfAT9o34gfAvw54/tfFmn+C/G2q6DY+KbKFY4dYhs7yW3S9jRZJFVJljEgUSOAHADMPmPEUAFfqR/wR1/4NiPi/wD8FT/2bbz9qTxx+0L/AMKn8M3WrNZ+DDN4JOry69HEWS5ugPtlsIYUlHlK2XMjpMMII1LgGp/wVt/4NYfi9/wTQ/ZWn/av+HP7Sf8AwtrR9F1GOPxhp8HgQ6TPpFnJ8q3wxe3PnRLKUSTG0oJFflA7J+UdABRQB+nP/BKD/g1//ao/4KafA/TP2n/EHxj0H4Y+ANcupo9DvNQ0uXUtS1GGKR4pLiO0V4kEXmoyKXmQttZgNu1m+xf+IGP/AKyi/wDmE/8A79UAH/EDH/1lF/8AMJ//AH6o/wCIGP8A6yi/+YT/APv1QB8//wDBQT/g0C/aj/ZD+Bfib9oL4HftJ6D8VNL8IaLJqut6PJ4am0bU2tYUeS4e3h865jmMaLv2earONwUFgqv+QNABX7ffsnf8Ga3/AA1B+yx8NP2l/wDh45/Yf/CxPh/o3if+xf8AhUH2r+z/ALfYw3X2fzv7Xj83y/N2b9ibtudq5wAD8kv22P2bf+GO/wBrn4jfsr/8Jn/wkX/Cv/F97of9vf2d9j+3/Z5TH53k+ZJ5W7Gdu98dNxry6gAooAs6PYDVdWtdL83y/tNwkXmbc7dzAZxnnrX76/8AEDH/ANZRf/MJ/wD36oA/AGigAr9SP+COv/BsR8X/APgqf+zbeftSeOP2hf8AhU/hm61ZrPwYZvBJ1eXXo4iyXN0B9sthDCko8pWy5kdJhhBGpcA1P+Ctv/BrD8Xv+CaH7K0/7V/w5/aT/wCFtaPouoxx+MNPg8CHSZ9Is5PlW+GL2586JZSiSY2lBIr8oHZPyjoAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACv2Q/4NN/8Agj7/AMNR/Hb/AIeF/Hrwv5vw/wDhnqyr4Lsb6H93rniJAHWbB+9DZ5SQngNO0IBYRSpQB+iH/BeP/g4ng/4JhftJ/DP9m74J6TZeItbt9Ytdc+MFowSRrbQXyF06MniK7nQmcMSDGkcJIZZ8j68/a+/Zq/Zh/wCC2f8AwTkm8BW/iS31Lwr8QvD9vrngPxbaIGfTL7yzJZ3yDqrozFJI/lYo00TbSzYAP43f2mP2cviz+yL8fPFf7NXxz8NtpPivwbq8mn6xZ5JQsuCksbEDfFJGySxvjDxyIw4YVwtAH9An/Bor/wAFjGvIl/4JV/tEeK8yRLNd/BzVNQuCSyjdLcaNuP8AdG+eAE9PNjB4iWvo7/g6o/4I/wD/AA2t+zWP20/gb4ZE3xQ+FGkyPqlrawlptf8ADqF5ZbcAffmtmaS4jHVladAGZ4wAD+W2vrT/AIIuf8EyvF3/AAVQ/bh8P/Ae2t7q38G6WV1f4ja5CCosdIidd8avj5Zp2Kwx9SGcvgrG5AB/UD/wVd/b/wDg3/wRZ/4J2SeMvBXh3S7XULDS4fC/wf8ABUQ2wyXiweXbJ5YOfs1tEnmyYIykYQMHkTP8gdh+01+0Ro3x3u/2ovDnxv8AFWj/ABGvtUvNSuPHGh65PY6p9quxItzKtzbskiNIs0qttIBWRl+6SKAPR/8Ah7F/wVN/6SWftAf+Hk1z/wCSq+wv+CBX/BRX/goL8Yv+CvnwT+Gvxe/bq+Mnirw3q2vXseqeH/EnxO1a+sbxF0y7dVlgmuGjkAdVYBgQCoPUCgD+gf8A4Lp/Ef4h/CL/AIJJ/HH4kfCjx5rXhjxFpPhNZtK17w7qk1le2Un2uBd8U8LLJG2CRlWBwSO9fybf8PYv+Cpv/SSz9oD/AMPJrn/yVQBk+O/+Ckn/AAUT+KXg7Ufh58Tf29/jR4i8P6xatbatoevfFLV7yzvYW+9HNDLcMkiHurAg1nfsFWNpqf7c3wX02/hEkFx8WPDkU0bZwyNqduCPyNAH9s/7Uusat4d/Zk+I2v6DqlxY31j4E1e4sr2zmaKa3mSylZJEdSGV1YAhgQQQCK/i/wD+HsX/AAVN/wCkln7QH/h5Nc/+SqAD/h7F/wAFTf8ApJZ+0B/4eTXP/kqj/h7F/wAFTf8ApJZ+0B/4eTXP/kqgD+mH/g1n+OHxq/aE/wCCUmm/EX49/F/xR448QSePNZt5Nd8YeILnU7xoUePZGZrh3cquThc4HYV+O3/B4vY2lp/wV3t57eEK918J9FluGH8b+fepn/vlFH4UAflPX7/f8GMf/N0X/ck/+5+gD9mr741fsv8A7S/xZ+J3/BPfx5b6brGtaL4ctZfF3gnXIEkj1TRdQg4nWNs+bDuZoX7o+3djzIy38wP/AAX5/wCCEfjz/glP8WW+Knwms77Wvgb4r1Jk8N6zITLLoNy+5/7LvG65AVjFMf8AWovJ3o9AH5y1++H/AAbEf8G+Ca4fD3/BTH9t/wAGutrFNHqPwj8DapbYFwQA0WtXUbD7gOGtoz94gTH5fK3AH7lfs7/tN/Cn9qTSfE3ib4NeIYdY0fw14xvfDcmsWcyyW95d2ixC4aJlOGRJXeLd0YxMQSpBP8mf/BzN/wApxfjx/wBhHRf/AEw6dQB8I0UAf0H/APBob/wR8/sXTD/wVW/aC8LYu72Oew+Dum30PzQwHMV1rOD0Ljfbwnj5PPfBEkTj3H/gob/wc7eBf2QP+Cufgv8AZD8N/YtQ+F3ha6fS/jpr0USzSQahc7VUW7Lkgaf8rzhcs7PNDtDwgkA+pf8Agt3/AMEu/A//AAV7/YYn8G+ELnT5PHOgwtr3wl8SLcJ5LXbRA/Z2mGQbW6jCozA7Q3ky/N5QB/js8X+EfE/w/wDFmqeBPG+gXWk61omoz2Gr6XfQmOezuoZGjlhkQ8o6OrKVPIIINAHXfAv9rH9qb9l/+1P+GaP2lviB8O/7c8j+2v8AhBfGV9pH9oeT5nk+f9llj83y/Nl27s7fMfGNxz6B/wAPYv8Agqb/ANJLP2gP/Dya5/8AJVAH7Qf8GdH7Xv7WX7TvxO+O1h+0n+1B8RPiFb6PoOgyaTD448bX+rJZPJNeiRoRdSyCMsEUMVwSFXOcCvSv+Dw/9p79pb9mT4F/BTV/2bf2h/HXw9u9V8WarDql14H8W3mkyXkaWsJRJWtZUMiqSSA2QCSRQB+Cv/D2L/gqb/0ks/aA/wDDya5/8lVxPxu/bL/a/wD2mdGsvDn7SH7VnxK+IGn6ddG50+x8b+OtQ1aG1mKlTJGl1M6o5UldygHBxnFAH1r/AMGuP/Kdf4Gf9zN/6jGrV/RB/wAHH3/KE34+f9i7Yf8Ap1sqAP44K2PAnj7x18LfGGm/EP4ZeNNW8N6/o90tzpOuaDqUtneWUy/dkhmiZXjcdmUgj1oA9o/4exf8FTf+kln7QH/h5Nc/+Sq9F/ZU/ay/4LlftsfHHRf2df2af26f2i/EvirXJtttZ2vxk1tY4Ih9+4nka6CwwoDlpHIVR3yQCAf1I/8ABLn9g/4s/sG/AqS2/ae/bV+I3xm8f6tClx4q8SeOPHmpajpthsBP2fT4LyZ1ghTLZmKiWU/M5VQkUf5Vf8F6f+DpxIhrH7HX/BLnx1uY77Pxb8ZNNk+UAjD2+juO/JVr3tg+Rn5Z6APwDvLy71G7lv7+6knuJ5GkmmmkLPI5OSzE8kknJJ5JqKgD6t/4I3/8ExfHf/BVf9tTQ/2f9H+1WPhPT8ar8RPEdun/ACDNIjcbwrEECeZiIYhg/O+8qUjcj+mP/gsT/wAFC/hJ/wAEQf8AgnFbWvwW0HStL8RzabH4U+CvhCGNTDbyxQBBcGMklra0iCyNkEM/kxsQZg1AHUf8Eif+ChXws/4LGf8ABPay+IHivS9NvNal0+Xwz8XPCM8atEl8YdlwrRHOba5ifzUByNkrISWRwP5l/wDguh/wSk8Uf8EoP20dQ+G2mWt1cfDfxZ52rfDDWrhjIZrDeA9nJJj5ri2ZljfuytDKQolAAB8X0UAf1rf8GmtjaWn/AART8Bz28Kq114n8Qy3DD+NxqUyZP/AUUfhX5d/8HRv7en7c37Pv/BWPXPhx8Bf2z/ix4I8PQ+C9Fni0Hwj8RdT02ySV4CXcQW86RhmPJbGSetAH52/8PYv+Cpv/AEks/aA/8PJrn/yVR/w9i/4Km/8ASSz9oD/w8muf/JVAH9Qnw3/4L6/8Eofgz+zX4FHx1/4KE+EbzxDZ+A9EbxH/AGde3OvXjXj2kAl8z7DHcPLN5jN5g+Z1O4uFwxH8n/7XXjPwX8R/2rvid8Q/hxfJdeHde+IetajoNzHavCstlPfTSwOI3VWjBjZSFZVZehAIxQB53X9fv/Brj/ygo+Bn/czf+pPq1AH4R/8AB2H/AMpsviJ/2Lvh3/01W9fm9QAUUAFf3Hf8EzP+Ub37Pv8A2RHwn/6Z7WgD+PH/AIKxf8pTf2lv+zgPGX/p8vK+f6APXv2Ev2Lvi9/wUF/ap8I/sn/BKw36x4o1ER3GoSRFoNKs1+a4vp8dIoYgzkdWIVFy7qp/rZ+MnxG/ZP8A+De//glItz4e0dY/DXw30BNN8K6PJMsd14n1ybcY1dgPmmubgyTzOqnYnnSBdse0AHnH/BvD/wAFk5f+Cr37MmpWHxi1DToPjB4FvfJ8YWNjCtvHf2czs1rqEEQPyxkAwuBnbJFk4EiZ/E//AIOcv+CPLf8ABO/9qf8A4aJ+CvhgwfB/4qalNcabHbx/utA1o7pbnTeBtjif5prccfu/MjUYgLEA/L+igAooAKKACigD7/8A+DXH/lOv8DP+5m/9RjVq/o7/AOC+vxN+JHwc/wCCQnxs+Jfwh+IOueFfEmk6DZSaX4g8N6tNY31m7anaIzRTwsskZKMykqwJDEdCaAP5Rf8Ah7F/wVN/6SWftAf+Hk1z/wCSqP8Ah7F/wVN/6SWftAf+Hk1z/wCSqAP3D/4M6v25P2uv2qrb47+Av2m/2ifF3xCs/DP/AAjt5oFx4012fU7qykuTqKzqtxcM8pRxBD8hYqpTKhdzbsn/AIPgrS2f4C/AG/eBTNH4u1uOOTHzKrWtsWH0JRfyFAH86Vfut/wY82NrJ8Zv2hNSaEGeHwxoEUcn91HubwsPxKL+VAH05/weF/tOftJ/syfs7/BrW/2bv2hPHHw9vdU8aajBqd54H8WXmky3cS2iMscrWsqGRQckKxIB5r8Df+HsX/BU3/pJZ+0B/wCHk1z/AOSqAD/h7F/wVN/6SWftAf8Ah5Nc/wDkqu0/Zv8A+Cpv/BTnXf2iPAWi63/wUa+PF5Z3njTS4Lyzuvi9rUkU8T3cStG6tckMrAkFSCCDg0Af2QfG2xtNT+DPi7Tb+ASQXHhjUIpo26MjW0gI/EGv4K6ACv7jv+CZn/KN79n3/siPhP8A9M9rQB/LD/wU1/4Ka/8ABSTwF/wUk/aD8C+Bf+Cg3xv0XRNF+N/iyw0fR9J+K+sW1rYWsOsXUcUEMUdyEiiRFVVRQFVQAAAAK8P/AOHsX/BU3/pJZ+0B/wCHk1z/AOSqAPTP2M/+Cpf/AAVw179rH4c6L4S/by+O3ia+u/GWnxR6DffELVdXhvkadRJHJaTzSRzoU35VkYAZPGMj+qb/AIK8X2kad/wSo/aTuNaureGFvgT4siRrhgFMz6RcpEoz/EZGRV7lioHOKAP4ja+2P+Dc+xtNQ/4LWfAGC8hEiL4ou5VVv76abdup/BlB/CgD+nj/AILg/ET4gfCb/gk58cviP8K/HWseGfEOk+C2n0nXvD+py2d5ZS+fEN8U8LLJG2CRuVgcE1/Jf/w9i/4Km/8ASSz9oD/w8muf/JVAB/w9i/4Km/8ASSz9oD/w8muf/JVH/D2L/gqb/wBJLP2gP/Dya5/8lUAf1of8EPviJ8QPiz/wSc+BvxH+KnjrWPE3iHVvBaz6tr3iDU5by8vZfPlG+WeZmkkbAA3MxOAK/mH/AODjCxtNP/4LWfH6CzhEaN4otJWVf776baOx/FmJ/GgD53+Cn7bv7aH7NfhW48C/s6ftd/FDwBol1qD391o/gnx9qOlWs10yRxtO8VrMiNKUiiUuRuKxoCcKAOw/4exf8FTf+kln7QH/AIeTXP8A5KoA/oQ/4NBP2kP2iP2l/wBir4m+Kv2jvj140+IGqWHxS+yWOpeNvFN3q1xbW/8AZto/lRyXUjsibmZtoIGWJxkmvmr/AIPA/wBsv9sD9mb9q74S+HP2b/2q/iV8PtP1L4eXFzqFj4I8dahpMN1ML6RRJIlrMiu4UBdzAnHFAH5Cf8PYv+Cpv/SSz9oD/wAPJrn/AMlV5z8cP2n/ANpb9pvUdP1f9pL9ofx18QrvSoXh0u68ceLbzVpLONyC6RNdSuY1YgEhcAkDNAH2L/wa4/8AKdf4Gf8Aczf+oxq1f0Qf8HH3/KE34+f9i7Yf+nWyoA/jgrp/hH8avjJ+z/4zh+JHwH+LXibwT4it4ZIYNe8I69cabexxyLtdFnt3SQKw4IBwRwaAPWf+HsX/AAVN/wCkln7QH/h5Nc/+Sq95/wCCd/xG/wCC9H/BTf8AaAs/gB+zT+3/APtAXE3yTeIPEF98Y9dTTtAsy2GurqUXJ2r12ooMkhG1FY0Af1D/ALI/7P2jf8E5/wBlB9I+OX7XfjXx9Jo1lJqvjj4ofF/xxdXjM6R5mmDXs8iWFqir8sKMFVRl2kctI34Lf8F6v+Dnnxj+1k+tfsh/8E+fEeoeHfhczSWXiTx1Cr22o+LEyVaODo9rYsM5B2yzKcOI0LxMAfjRRQB9W/8ABG//AIJi+O/+Cq/7amh/s/6P9qsfCen41X4ieI7dP+QZpEbjeFYggTzMRDEMH533lSkbkf0x/wDBYn/goX8JP+CIP/BOK2tfgtoOlaX4jm02Pwp8FfCEMamG3ligCC4MZJLW1pEFkbIIZ/JjYgzBqAOo/wCCRP8AwUK+Fn/BYz/gntZfEDxXpem3mtS6fL4Z+LnhGeNWiS+MOy4VojnNtcxP5qA5GyVkJLI4H8y//BdD/glJ4o/4JQfto6h8NtMtbq4+G/izztW+GGtXDGQzWG8B7OSTHzXFszLG/dlaGUhRKAAD4vooA/tU/wCCINja6f8A8EiP2dYLKHy0b4T6RKyjPLvAHY/izE/jX85//Ba//gpD/wAFEPhV/wAFXPjr8Ovhf+3r8aPDfh/SPHdxb6ToWgfFLV7OzsoQiYjihiuFSNBk/KoA9qAPlv8A4exf8FTf+kln7QH/AIeTXP8A5Krpvgr/AMFYv+Cln/C5PCX/AAl3/BSz45f2T/wk1h/an9pfGTWfs/2f7Qnmebvutvl7N27dxjOeKAP6a/2n/wDgv3/wRk8FfDvxP4P8Q/t4+C9WmvNPu9LFt4bt7zXI55ZLeQAZsIJ0aI8gyZMfIBbkZ/juoAK/t8/4JO/8osv2af8As3/wb/6Y7OgD+SX/AILZf8pcv2jP+yva1/6VPXy7QAUUAaPhD/kbNL/7CMH/AKMFf310AfwB0UAfVv8AwRv/AOCYvjv/AIKr/tqaH+z/AKP9qsfCen41X4ieI7dP+QZpEbjeFYggTzMRDEMH533lSkbkf0x/8Fif+Chfwk/4Ig/8E4ra1+C2g6VpfiObTY/CnwV8IQxqYbeWKAILgxkktbWkQWRsghn8mNiDMGoA6j/gkT/wUK+Fn/BYz/gntZfEDxXpem3mtS6fL4Z+LnhGeNWiS+MOy4VojnNtcxP5qA5GyVkJLI4H8y//AAXQ/wCCUnij/glB+2jqHw20y1urj4b+LPO1b4Ya1cMZDNYbwHs5JMfNcWzMsb92VoZSFEoAAPi+igAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigD3L/AIJy/sH/ABY/4KSftd+FP2UPhHGYbjXLrzda1iSEvDo2mREG5vZQOyIcKpI3yNHGCC4r+yLRfhBD+wV+wynwa/Yb+AjeJJvh94Oe0+H/AIFj1S1s21W8VSUE9zcPFErSzsZZ5mYFi8jgM5CkA/mX+PH/AAbxf8HEH7Snxl8TfH74xfseyat4o8Xa1Pqmt38nxM8MfvJ5XLEKP7T+RF4VUHCqqqMACv12/wCDZf8AZz/4K3/sJfD3xJ+x3+3t+zPdaH8Pbdn1f4f+Ij400TUF0y5kkH2nTjHZ3sswjlLeehCbUcTbiDItAHG/8HYH/BHkftU/Alv+CgvwE8LeZ8Q/hrpJXxhY2NuWl17w/GS7SYX701pl5AcZaEyAk+XEtfzH0Aangfxt4t+GvjPSfiJ4C8QXWk65oWpQaho+qWUhSa0uoZBJFKjdmV1VgfUV/ZB/wQ//AOCqnhL/AIKufsX6b8Vrie1tfiD4a8rSfiZoUDKv2fUAny3UaA5W3uVUyR8YBEkYLGJjQB+BX/Byx/wRu1L9gb9siD4s/s+eBZm+Fvxh1V5PDOm6VZs40fW3IafSURF4V2Yy28agZjZo1B8gk/uX/wAEHv8AgmL4T/4JF/sBxr8VBp+n+PvFFmviP4sa5czKqWLLEXSyaUnasNpEWVjnb5hncHDDAB/Ol/wXp/4Ks69/wVS/ba1Lxp4d1O4X4Z+DWm0f4Z6XJlVNoH/e37oek1y6iQ5AZY1hjP8Aq8n4ioAK+3f+DcH/AJTZfAP/ALGK/wD/AE1XtAH9Kf8AwcLf8oX/ANoH/sTE/wDSy3r+M+gAr17/AIJ+f8n6fBH/ALK94a/9OltQB/a1+17/AMmm/FD/ALJ3rf8A6QTV/CPQAUUAf1c/8Gg//KHHS/8Asomu/wDocVflF/weQf8AKXKw/wCyQ6L/AOlV/QB+Udfv9/wYx/8AN0X/AHJP/ufoA8B/4OOv2rvjp+xL/wAHCX/DSn7OfjWbQvFXhvwfoMtpcJloriM27CS2njziWCRco8Z4ZT2OCP2l/wCCfH7d/wCx3/wX5/YH1bS/F3g3TL7+0NMTRvi98MdSkLtpl1Iu75TkOYHZDLb3K4YGPgpLE4QA+C/2N/8Ag0A8IfB//go/rXxN/aA8YWHjD4E+F7qLUvh/4dusPda9M5ZktdTTAURWpUb8ZW5/d5CqZYgf8HOX/BwBB8FtK1r/AIJpfsPeM1j8V3FubD4n+MtHmGNBt2XD6Taup+W6Zflmdf8AUKTGCJS3kgH0R/waD/8AKHHS/wDsomu/+hxV+Gn/AAczf8pxfjx/2EdF/wDTDp1AHwjX2V/wQ5/4JYeJv+CrP7bOk/Ci+trq3+Hvhoxav8TtYgLJ5OmrJgWkcgHy3FywMSd1HmSAMIiKAP6tf23n/aZ+Bf7DOteD/wDgmn+z3a+IvHljoEOhfDrwzZ6jp+m2ejJsEEdzm8mhhEVrENyRAksyRptClmX+YnW/+DYz/gvd4k1m78ReIP2Lri+1DULqS5vry6+KXhmSW4mdizyOx1PLMzEkk8knNAH78f8ABvT4F/4Ka/AL9jdf2Uf+CkvwEuvDV18P5I7TwD4im8V6TqY1HR2DbLN/sN3O6Paldis4VTE8SqSY2r87/wDg7u/4I8rpV6f+Cqn7PfhbFvdSQWXxi0uxtziOU7YrbWMDgBvkgmPHzeS/JeVqAPwOooA/dz/gx2/5Kz+0R/2Lvh3/ANH39eq/8HwH/JvXwF/7HPWP/SSCgD+c6igD7/8A+DXH/lOv8DP+5m/9RjVq/og/4OPv+UJvx8/7F2w/9OtlQB/HBRQB79/wTp/4Js/tQ/8ABT3492/wI/Zo8IfaGj8ubxH4kvwyaboFozEfaLqUA7c4bZGoMkhUhFbBx/Vx+wN/wTp/YQ/4IT/sl6vq1jr+maatjpi33xN+Lniny7e51Mxj70jknyLdWO2G1QkKWA/eSuzuAfhb/wAF4P8Ag5X+JP8AwUFn1X9lv9j281Pwh8FVkkttU1HLW+peNFBKlp+Q1vZMPu23DODmbqIo/wAm6ACr/hnw14h8aeJNP8HeEdFutS1bVr6Gz0vTrGEyTXVxK4SOKNFGWdnZVCjkkgUAf2L/APBCX/glR4d/4JTfsT6b8O9asbWb4k+LvK1j4naxbkN5l8VPlWSPjJgtUYxr2ZzNIAvmkD8lf+C2n/BLL/gv9/wVV/bg1z42R/sP3Ft4H0Pdo3w30eb4meGV+zaVHIcTuh1P5Z7hszSd13LHkiNTQB03/BAr/gmh/wAF5v8AglT+2vZeMfGn7F11/wAKu8aLHpHxKsYPiR4al8q23fudRSNdSLNJbOxfCgs0TTIoLOK/XD/gsV/wTG8Af8FVv2L9c/Z/10Wtj4qsN2qfDzxJcRnOl6tGjBNxHPkSgmGVeRtfcFLxoQAfxl/FH4Y+Pfgv8R9c+EfxS8L3Wi+JPDWrT6brmk30eyW0uoXKSRsPUMD7HqMg1g0Af1t/8Gnn/KE34d/9jF4i/wDTrcV+L3/B3X/ymX8Qf9iHoP8A6TtQB+YtFAH1L4o/4Ilf8FZvCvhfTfGp/YD+I+s6XrFjaXmm3nhHQzriz29zAJ4ZB/ZxnODGwJyBsY7H2t8tfMWq6VqehancaJrenXFne2dw8F5Z3ULRywSoxVkdWAKspBBUgEEYNAFev6/f+DXH/lBR8DP+5m/9SfVqAPwj/wCDsP8A5TZfET/sXfDv/pqt6/N6gD+gb9mX/gyo+E/i7wR4e+Ifxt/b08RX0GuaVp2oyaT4V8Dwae9sksSyzQC4nubkSHDhUk8pQpUsY2ztH4VftCfD/RvhN8ffHHwr8OXN1Np/hnxhqelWM166tNJDb3UkKNIVVVLlUBJCqCc4AHFAHHV/cd/wTM/5Rvfs+/8AZEfCf/pntaAP48f+CsX/AClN/aW/7OA8Zf8Ap8vK+f6AP6tv+DX7/gkF/wAO+/2Uz+0n8afC4t/i58WNPhuLyO6hIm0HQztlttOww3Ryuds844+byo2GYAT8y/8ABx7+wJ/wXE/4KmftQ2PhD4Efsd3Fx8Gfh7G0PhCST4ieHrY61eSqv2nU5IZtQSRM4EMSyKGWOMsQrSuoAPmn/glr/wAEe/8Ag4c/4Jj/ALaHhT9qfwH+w1cXVnYXH2PxdocPxQ8LqNZ0eZlFzanOp43YAkjJ4WWONj93Ff0Lftxfsa/CP/goV+yh4p/ZY+Oeisuk+KtM2xXBjVrnSL1fnt7uIhsCWGUKww21gGQlkdgQD+LP9s39kj4vfsLftM+Lv2WPjjo5tfEHhLVGtpJljZYb6A/NDdwlgC0M0RSRD12sM4IIHl9ABRQAUUAFFAH3/wD8GuP/ACnX+Bn/AHM3/qMatX9VP7a/w+/ZS+Kn7Lni74f/ALcV/odr8KdSs4U8YXHiTxM+jWKQC4ieMy3iTQtAPOWLDCRcnC5OcEA/OvwJ/wAElv8Ag0W+KfjDTvh38MtS+C/iPxBrF0ttpOh6D+0/qF5eXszfdjhhi1tnkc9lUEn0r2h/+DXX/ghLGhkk/YdCqoyWPxO8T4A/8GdAGh+yD4//AODeL/gmB4a8TeEP2TP2pP2ffAcOp3Sz+KIIfjXZ6hf3UtsJAkcjXV9NcOY98oWEE7Wdwq5Y5/Gf/g6o/wCCwH7Mf/BRbx18OfgZ+yL4rk8TeGvh1JqV5rXixbOWG11C/uRBGsVsJVV3SJIZMybQrmUbCyruYA/Iuv3c/wCDHb/krP7RH/Yu+Hf/AEff0Aevf8Hvf/JsnwL/AOx81P8A9Ikr+cWgArvP2Wv+Tm/hz/2Pmkf+lsVAH9z3xe/5JN4o/wCxdvf/AEQ9fwS0AFf3Hf8ABMz/AJRvfs+/9kR8J/8ApntaAPh79oX/AIO6v+Cbn7Nfx98cfs6eOvgl8b7vW/AHjDU/DmsXWk+G9HktZrqxupLWV4Wk1VHaMvExUsisVIJVTkDkP+I1b/gll/0QP9oD/wAJXQ//AJcUAbXw1/4PI/8AglR8RPH2keBb34c/Gjw3Hq1/HanXNf8ACumfYrIucCSb7Nqc0oQEjJSNyOuMZI6L/g53/YE/at/ap/Yc8X/E/wCCX7Wviqz8P+B9HfX/ABJ8GmtbRNJ1y0s1E00izQwx3TzRpG04inkuIneJRGkT4YgH8otfbv8Awbg/8psvgH/2MV//AOmq9oA/pb/4OAv+UNH7Qf8A2Ib/APpRDX8Y9ABRQB/Zx/wb+/8AKGj9nz/sQ0/9KJq/mk/4OPv+U2Xx8/7GKw/9NVlQB8RUUAf0uf8ABkt/yYX8Wf8Asrx/9NdlXyr/AMHuX/J5HwZ/7Jnc/wDpxloA/E2igD7/AP8Ag1x/5Tr/AAM/7mb/ANRjVq/og/4OPv8AlCb8fP8AsXbD/wBOtlQB/HBRQB9g/wDBIf8A4I0ftM/8Fb/jF/wjfw3s28P+AdFvI18bfEa/tGa00xDhjBCuR9pu2Q5WBSMZVpGjQ7q/qN+HPwz/AOCcX/BAr9ha4Nteab4B8AeHo1l1zxDqZE2peIdQZcCSVkXzL28l2kJGi8KNsaJGgVQD+bn/AILZ/wDBf/4//wDBWLxbL8OfCcd/4H+Cum3QfR/BCXX7/VXRgUvNTZCVmlyAyQgmKH5du9wZW/PegAq/4Z8NeIfGniTT/B3hHRbrUtW1a+hs9L06xhMk11cSuEjijRRlnZ2VQo5JIFAH9i//AAQl/wCCVHh3/glN+xPpvw71qxtZviT4u8rWPidrFuQ3mXxU+VZI+MmC1RjGvZnM0gC+aQPyV/4Laf8ABLL/AIL/AH/BVX9uDXPjZH+w/cW3gfQ92jfDfR5viZ4ZX7NpUchxO6HU/lnuGzNJ3XcseSI1NAHTf8ECv+CaH/Beb/glT+2vZeMfGn7F11/wq7xosekfEqxg+JHhqXyrbd+51FI11Is0ls7F8KCzRNMigs4r9cP+CxX/AATG8Af8FVv2L9c/Z/10Wtj4qsN2qfDzxJcRnOl6tGjBNxHPkSgmGVeRtfcFLxoQAfxl/FH4Y+Pfgv8AEfXPhH8UvC91oviTw1q0+m65pN9HsltLqFykkbD1DA+x6jINYNAH9rX/AARN/wCURv7Of/ZIdF/9JUr+WD/gvd/ymO/aG/7KJc/+gR0AfIlXvDnh7WPFviGw8J+H7T7RqGp3sVpY2/mKnmzSOERdzEKuWIGSQB3IoA+kvjP/AMEVv+CsXwCW7m+JP/BP74nJbWH2g32oaH4Zl1e0t0gGZZXuLDzoliAywlLbGUEqxAJHy/QAV/b5/wAEnf8AlFl+zT/2b/4N/wDTHZ0AfyS/8Fsv+UuX7Rn/AGV7Wv8A0qevAfg/8O7j4u/Fzwt8J7PU0sZvFHiOx0iK8liLrbtc3CQiQqCCwUvkjIzigD971/4MrPgN8K/hv4h8dfF/9uvxf4ok0XR9Q1CO38N+D7XRllWK0Z4oyZ5r3BEq5Zv4kO0BT89fz2UAaPhD/kbNL/7CMH/owV/fXQB/AHV/wz4a8Q+NPEmn+DvCOi3Wpatq19DZ6Xp1jCZJrq4lcJHFGijLOzsqhRySQKAP7F/+CEv/AASo8O/8Epv2J9N+HetWNrN8SfF3lax8TtYtyG8y+KnyrJHxkwWqMY17M5mkAXzSB+Sv/BbT/gll/wAF/v8Agqr+3Brnxsj/AGH7i28D6Hu0b4b6PN8TPDK/ZtKjkOJ3Q6n8s9w2ZpO67ljyRGpoA6b/AIIFf8E0P+C83/BKn9tey8Y+NP2Lrr/hV3jRY9I+JVjB8SPDUvlW279zqKRrqRZpLZ2L4UFmiaZFBZxX64f8Fiv+CY3gD/gqt+xfrn7P+ui1sfFVhu1T4eeJLiM50vVo0YJuI58iUEwyryNr7gpeNCAD+Mv4o/DHx78F/iPrnwj+KXhe60XxJ4a1afTdc0m+j2S2l1C5SSNh6hgfY9RkGsGgAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACnwwy3Mq29vE0kkjBY40XLMx6ADuc0Af1r/8ABt1/wSJtf+CZH7H3/Cz/AIyaElr8XPiXZw6j4wa6279DsQC9tpYP8BRW8yb1lYqSyxIa/Pv9uD/g8u/af8EftUeMvBH7Enwn+E2ufDPRdWax8N694t0fU7q81VYgEku99tqEEflSSB2iAQHyyhYkk4APJ/8AiNW/4Km/9ED/AGf/APwldc/+XFH/ABGrf8FTf+iB/s//APhK65/8uKAP27/4Io/8FTPDH/BW/wDYps/jTqel6XpvjbR7ptH+JHhnTw32e0vwu4SwpI7yC2niZZE3sxB8yMs5iZj/AD3f8HKf/BHyf/gmr+1m3xZ+EHhryfg78ULye88LraxnytC1D79zpTf3FBJkgBxmIlRkwuaAPzXr6w/4I1/8FQPH3/BKX9s/Rvj1owur/wAI6oF0r4jeG4Wz/aWku6l2RSQv2iEgTRNkfMpQsEkkBAP7E7CP4A/ta/DDwl8SbWz0Pxt4VvptN8VeDtTkt1uIDKm24sr6HcPldSVdW4ZTxxyK/Hb/AIO7/wDgrx/wqf4dR/8ABL34D+JdviTxfYR3vxUvrO4+bTtHf5oNNypysl1jzJFOCLcICGW5yAD+cWigAr7d/wCDcH/lNl8A/wDsYr//ANNV7QB/Sn/wcLf8oX/2gf8AsTE/9LLev4z6ACvQv2SPHmifCz9qz4Y/E7xLcrDpvhz4haLqmoTP0jgt76GV2PsFQmgD+5L4oeEbH41fBnxF4C0zxFFDbeLPDF3p9vq1uguI447q2eNZ1AYCQASBgAwDDuM5r8Kf+IGP/rKL/wCYT/8Av1QAf8QMf/WUX/zCf/36o/4gY/8ArKL/AOYT/wDv1QB+rn/BIP8A4JtH/glP+xza/sl/8Ln/AOE8+zeIr7Vf+Eg/4R3+y932llPl+R9ouMbdv3vM5z0Ffzw/8HdHxK8L/ED/AILE6zo/hrVre7k8KeA9F0bVDbyBhFdBZrpoyR/Eq3SZHY8HkGgD8w6/f7/gxj/5ui/7kn/3P0AfHf8Awd1/8pl/EH/Yh6D/AOk7V8c/8E7P+Cg/x+/4Jn/tOaL+038ANYC3lifs+t6HdSN9j13T3I82yuFHVGwCrfejdUdcMooA/bL/AIKgf8HdPwT8QfsTaX4e/wCCceratb/Fbx3pYj8QXWpaVNA3gBGjHnKskkax3d3likUkJeNcGUsGVEb+eDUtS1HWdRuNX1fUJrq7upmmurq5lMkk0jEszszElmJJJJ5JOTQB/Vl/waD/APKHHS/+yia7/wChxV+Gn/BzN/ynF+PH/YR0X/0w6dQB8V/Dz4f+NPix490X4X/Dfw3dax4g8RapBpuiaTZJumvLqaQRxRIPVnYAduea/sP/AOCU37AvwV/4Ijf8E5ZNH+IPiDTrPUrHSJvFXxk8ZSyfumuo7fzJyHwD9mto0McYwMhC+N0jZAPx2+LX/B6v+30/xQ8Q/wDCjv2e/g7D4N/ti4HhePxPoGrT6j9gEjeQbl4dUijaYptLbEVQxIAwM1zv/Eat/wAFTf8Aogf7P/8A4Suuf/LigA/4jVv+Cpv/AEQP9n//AMJXXP8A5cV++37En7VXwA/4K4/sBaL8Z7bw3Zal4Z+IXh2fTPGfhG/YTLaXJQwX+mzDuAxcAkKXjaOQABxQB/KT/wAFrf8Aglr41/4JSfto6t8G54bi78D695mrfDXXpAWF5pbSECB36G4t2/dSDqcJJgLKmfkGgD93P+DHb/krP7RH/Yu+Hf8A0ff16r/wfAf8m9fAX/sc9Y/9JIKAP5zqKAPv/wD4Ncf+U6/wM/7mb/1GNWr+iD/g4+/5Qm/Hz/sXbD/062VAH8cFfXX/AAR5/wCCQvxv/wCCvP7Qtx8Mvh7rFv4f8J+GVt7r4geMboLJ/ZNrMziNYodwaeeXypRGgwuUYuygZIB/W9+w1+wd+zN/wTq+BGn/ALPH7LngJNH0W1Pm319cMsl9q90VAa7u5goM0zYHOAqgBUVUVVHx7/wV9/4IaftVf8FcvGkNn40/4Kf/APCF/DXSZhL4f+GekfCVp7aKbbj7Tdzf2vGb245IV2RFjUkIiFnZwD4n/wCIGP8A6yi/+YT/APv1R/xAx/8AWUX/AMwn/wDfqgD8gf8AgqP+wx/w7X/br8c/sVf8LR/4TT/hC/7M/wCKl/sT+zvtn2vS7S//AOPfz5vL2favL/1jbtm75d20fq//AMGiP/BH4+LvEf8Aw9Q/aA8MH+zNHuJrP4QafdqNt1eLuiudWKkcrEd0MJ/56ea2AYo2IB9J/wDBfb/g5e+I3/BNT9pPSf2Uf2NvBfgPxT4n0vTBe/EW68aWd5dQabJOqPaWcS2t1bss3lHzpCzMAs0IABLY+C/+I1b/AIKm/wDRA/2f/wDwldc/+XFAB/xGrf8ABU3/AKIH+z//AOErrn/y4r9Hv+De3/g4o8Zf8FVPiN4s/Zy/at8IeDfC/wAQrGz/ALW8Hr4Ptbm1s9X09Aq3MOy6uZ3+0xMVk+V8PE7EKvkszAHg/wDwd0f8Edm8d+FW/wCCpn7PXhVW1fQbWK1+L2nWUR8y9sV2xwattH3mgG2KY9fJ8tzhYXNfzt0Af1d/8GiHxH8M+Mv+CO2i+D9H1OGW+8I+Otb0/V7ZHG+CSW4+2JuHYNHcoQeh59DXP/8ABXX/AINgf+HqX7Zeoftcf8Nwf8IJ9u0Gw03/AIR//hWn9qbPs0ZTzPP/ALSt87s52+WMepoA+Yv+IGP/AKyi/wDmE/8A79Uf8QMf/WUX/wAwn/8AfqgD90vhD4FtPgd8EPC/w0v/ABFHdW/g/wAK2WmTatNELdJktLZIjOylmEYIjLEFiFB6nGa/h1/bC8ceH/ib+1v8UviT4SvVudK8Q/EbXNS0y4Q5EtvPfzSxsPYo4NAHnNf1+/8ABrj/AMoKPgZ/3M3/AKk+rUAfhH/wdh/8psviJ/2Lvh3/ANNVvX5vUAf3SfsI/Evw18Yv2KfhL8UPB+tQ6hp2t/DnRrm3urc/KxNlFuUj+FlcMrL1VlIPINfj/wDGr/gyp/4XB8ZPFvxa/wCHln9nf8JT4mv9X/s//hTfnfZftNw83lb/AO2F37d+3dtXOM4HSgDmf+IGP/rKL/5hP/79V+4n7Nnwhh/Zs/Zp8A/AS48ULqsfgHwNpWgSa09r9mW8FjZRW5uDHvfyg4i37d7bc43NjJAP4n/+ChnxH8NfGL9v345fF3wZeR3Oj+KvjB4m1jSbiOQOsttc6rczROGHBBR1ORwa+/v+DWT/AII+n9uT9pn/AIbB+OPhgzfCv4U6pFLZ290o8rX/ABAm2WC1II+eGAFJ5R0JMKEMruAAfrF/wcM/8F9Nd/4JI2ng34S/s5+HPCfiX4qeKGOp32m+LLe4uLPS9FXfGJpI7aeCQyzTKVj+cLiCYkH5c/l3/wARq3/BU3/ogf7P/wD4Suuf/LigA/4jVv8Agqb/ANED/Z//APCV1z/5cV9nf8ENv+DpL4t/t9ftiRfsl/tueAfh74Xm8V2DJ8P9Y8F2N7aRy6nGGdrO5+2XlwGM0YPlMpQ+ZGI8OZV2gHrH/B0d/wAEd2/by/Zo/wCGsvgT4VW4+LHws0yWWW3tYSZ/EOgpulmswBzJNCS80K8lsyxqC0q4/lboA/qW/wCDbj/gnV/wT8+NH/BGz4S/Fj4x/sS/CXxl4p1y48QSa14h8WfD3TdUvbhote1C3iDTXMLuAsMMShQQoC5AyST8r/8AB4b+xh+yD+zR8A/gv4n/AGb/ANlr4efD3UNQ8Yana6jdeBvBtlpDXkP2WJwk32WKPzQrKCu/O3LbcbmyAfgjRQAUUAff/wDwa4/8p1/gZ/3M3/qMatX9EH/Bx9/yhN+Pn/Yu2H/p1sqAP45La5uLO4jvLOd4ponDxSxsVZGByGBHIIPev3M/4Ig/8HXfiH4YppH7LH/BUTxBfa14fUx2nh74utGZr7TF4VY9UA+a6hAx/pKhpl/5aCUMXQA/Rf8A4Klf8ECP2Df+CwPg3/hfnw01jSfCXxE1WwW60X4p+DFhubLXVZcxtfRxN5d9GwxidWWUDbiRkXyz/M9/wUL/AOCXn7ZH/BMX4on4bftT/DCWxtrqZ10DxZpm640fW0X+O2udoBIGCYnCSoCN6LkZAPnmv27/AODI74leFtD/AGpvjV8KdS1a3h1bxF4H06/0u1lkCvcJZ3cizBAT8xUXaMQOcZPQGgD9X/8Agt9/wRo/4fJ/DHwL8Of+Gj/+Fcf8IXr11qX2z/hD/wC2PtnnQrF5ez7ZbeXjGd2Wz0wOtfnH/wAQMf8A1lF/8wn/APfqgA/4gY/+sov/AJhP/wC/Vb3ws/4Mmv8AhWnxN8OfEb/h5l9t/wCEf16z1L7H/wAKZ8vz/ImSXy9/9stt3bcbsHGc4PSgD9nP2t/iX4W+Df7LPxH+K3jbVrex0nw94H1S/vrq5kCoiR2kjYycckjaB1JIA5Ir+EegAr+47/gmZ/yje/Z9/wCyI+E//TPa0Afh/wDtu/8ABor/AMFI/wBpT9tD4vftF+BfjZ8ELTRPH/xQ1/xHo9rq3iTWI7qG1vtRnuokmWPSnRZQkqhgrsoYEBmGCfL/APiCp/4Km/8ARfP2f/8Awqtc/wDlPQB0Hwq/4Msf+CiFt8SdDu/ij+0j8F7Hw7DqcMmsXWg6xq95exwKwZvJhm02GN3OMANIg5yTxg/vR/wVB8f+Dfhf/wAE4vjr408feIrXS9Mt/hPr0Ml3dyhVMs1hNBDEM9XklkjjRRyzuqgEkCgD+Hmvrz/ggl8SvC3wk/4LDfAHxp4y1W3sdP8A+E4WwkurqQJHHJeW81nGWYnCjzJ0GTwM5PFAH9b3/BQH9k4/t0/safEL9kf/AIT7/hFv+E80FtN/4SD+y/t32HMiP5nkebF5v3MbfMXr1r8Y/wDiBj/6yi/+YT/+/VAB/wAQMf8A1lF/8wn/APfqj/iBj/6yi/8AmE//AL9UAfs5/wAE/v2Tj+wt+xp8Pf2R/wDhPv8AhKf+ED0FdN/4SD+y/sP27Ejv5nkebL5X38bfMbp1r+SH/gvb8SvC3xb/AOCw3x+8aeDdVt77T/8AhOGsI7q1kDxySWdvDZyFWBww8yBxkcHGRxQB8h0UAf0uf8GS3/JhfxZ/7K8f/TXZV8q/8HuX/J5HwZ/7Jnc/+nGWgD8TaKAPv/8A4Ncf+U6/wM/7mb/1GNWr+iD/AIOPv+UJvx8/7F2w/wDTrZUAfxwV+hn/AAQb/wCCEHxC/wCCuXxDufiF4316Tw18GPCWsLaeLNes5YzfajdBI5TptmhzskMciM07qUjVwQJGwhAP6uf2df2cvgp+yb8HdF+AX7PPw80/wv4T8P2vk6bpOnR7VXu0jsctLK7ZZ5HJd2JZiSc1+aP/AAU8/wCDcD9qv/gqx8bG+Kf7QP8AwVp+y6Lp8kieEPA2l/Bhhpmg27H7saHWv3kzADzLh/nkIA+VFSNAD5m/4gY/+sov/mE//v1R/wAQMf8A1lF/8wn/APfqgD8Qf2sfgX/wy/8AtTfEv9mj/hKf7c/4V38QNZ8Mf219h+y/2h9gvprX7R5O+TyvM8rfs3vt3Y3NjJ/Z/wD4NEf+CPx8XeI/+HqH7QHhg/2Zo9xNZ/CDT7tRturxd0VzqxUjlYjuhhP/AD081sAxRsQD6T/4L7f8HL3xG/4JqftJ6T+yj+xt4L8B+KfE+l6YL34i3XjSzvLqDTZJ1R7SziW1urdlm8o+dIWZgFmhAAJbHwX/AMRq3/BU3/ogf7P/AP4Suuf/AC4oAP8AiNW/4Km/9ED/AGf/APwldc/+XFfo9/wb2/8ABxR4y/4KqfEbxZ+zl+1b4Q8G+F/iFY2f9reD18H2tza2er6egVbmHZdXM7/aYmKyfK+HidiFXyWZgDwf/g7o/wCCOzeO/Crf8FTP2evCqtq+g2sVr8XtOsoj5l7Yrtjg1baPvNANsUx6+T5bnCwua/nboA/s6/4IDfEfwz8T/wDgjp8ANa8L6nDdR6f4Bt9HvPJcHyrqyZ7SaNh2YPCeD2IPQiviH9vb/g0M/wCG4P2x/iF+1p/w8J/4Rf8A4TzxFJqn/CP/APCp/tv2Heqjy/P/ALVi83G373lr9KAPIf8AiBj/AOsov/mE/wD79VtfDj/gyS/4V/8AEPQfHn/DzT7Z/YetWt/9l/4Ux5fneTMsmzd/bJ2524zg464PSgD9pv2qfiH4U+En7MvxC+J/jrV4bHR9A8Fapf6ld3EgRI4orWRmOT34wPUkAda/hBoAK/t8/wCCTv8Ayiy/Zp/7N/8ABv8A6Y7OgD+SX/gtl/yly/aM/wCyva1/6VPXh/7PHjTTfhv8f/AvxD1e58mz0Hxjpmo3U3llvLjgu45WbA64CnjvQB/dR4v0jS/jB8JtU0HQvEMP2HxT4dmt7PVrdRPH5VzAypOuGAkXa4YYYBh3Gc1+Ef8AxAx/9ZRf/MJ//fqgCxpH/Bjl/ZOrWuqf8PP/ADPs1xHL5f8AwpXG7awOM/21x0r91Pi38TfCvwW+FniT4weOtRhs9F8LaHd6tqtzNMsaR29vC0rks3A+VT1oA/glr93/APg0R/4I/Hxd4j/4eoftAeGD/Zmj3E1n8INPu1G26vF3RXOrFSOViO6GE/8APTzWwDFGxAPpP/gvt/wcvfEb/gmp+0npP7KP7G3gvwH4p8T6XpgvfiLdeNLO8uoNNknVHtLOJbW6t2Wbyj50hZmAWaEAAlsfBf8AxGrf8FTf+iB/s/8A/hK65/8ALigA/wCI1b/gqb/0QP8AZ/8A/CV1z/5cV+j3/Bvb/wAHFHjL/gqp8RvFn7OX7VvhDwb4X+IVjZ/2t4PXwfa3NrZ6vp6BVuYdl1czv9piYrJ8r4eJ2IVfJZmAPB/+Duj/AII7N478Kt/wVM/Z68Kq2r6DaxWvxe06yiPmXtiu2ODVto+80A2xTHr5PlucLC5r+dugAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACuk+EPxX8b/Ar4oaD8ZPhpf2dr4i8M6pDqWiXd9pFrfxW91EweOUwXUckMhVgGAdGAYA4yAaAPrz4qf8AByB/wWl+NHw1174RfET9ti6utB8TaTcaZrVrZ+B9AspZ7WaMxyxrPbWEc0W5GK7o3VgDwRXw/QAUUAe3/sRf8FHf20/+CcfizW/G/wCxh8b7jwXqHiTTo7HXGXR7HUIbuFJPMQNDewTRblbO2QKHUO4DAOwPpH7V3/BdH/gqP+3D8Gb79n79qr9pKy8YeEdQuIbifS7z4d+HoGSaJt0csU1vYRzQyDkb43VtrMpO1mBAPkiigD64/ZS/4Ls/8FW/2I/gtp/7PH7M37Wd14e8G6TcTzaZotx4S0bUltWmcySCOS9s5pFQuWbYG2gsxABJz83/ABp+M/xQ/aK+K/iD45fGrxjdeIPFnirVJdR17WLxVWS6uJDlm2oFRB2VEVUVQFUKoAABy9FABXcfs4ftG/Gb9kf42aB+0X+z34x/4R/xl4XuZJ9D1j+zre7+zSPE8LN5VzHJE+Y5HXDow5yOQCAD6U/aF/4OCv8Agrx+1X8F/EH7PXx7/a3/ALe8H+KbL7Jr2j/8IDoFr9qhDq+3zbewjlT5kU5R1PHXGa+M6ACigD61/Zl/4Lrf8Faf2P8AwJa/DD4Cftr+JNP8P6fapa6bo+tWFjrUFjAn3YoF1K3n8hF6BY9oAwAMACvSv+Io3/guv/0fL/5jPwx/8rKAD/iKN/4Lr/8AR8v/AJjPwx/8rKP+Io3/AILr/wDR8v8A5jPwx/8AKygDH8df8HKn/Bbz4i+HJvC/iD9vPWLe1mIMkug+FNF0u5HX7txZWUUydf4XFfE3iPxH4h8YeIL7xZ4u1291TVNSupLrUtS1K6ee4up3Ys8skjktI7MSxZiSSSSc0AUa+gP2GP8AgqP+3X/wTX/4Sj/hir45f8IX/wAJp9h/4SX/AIpnS9R+2fZPtH2f/j/tpvL2fap/ubd2/wCbO1cAHH/tdftk/tIft3/Ga4/aC/as+I//AAlXi+6sLeyuNX/sezsd0EK7Yk8qzhiiG0HqEye5NeX0AFFAH1d+yD/wW+/4KhfsF/ByL4AfsoftO/8ACKeEYdRnv49J/wCEL0S+23ExBkfzbyyll5IHG/A7AV4b+0z+0x8bv2xfjjr37SP7R3jb/hIvGniaSCTXNa/s22s/tLQ28dvGfJtY44UxFDGvyouduTkkkgFj9lr9qn45/sXfGjS/2h/2b/FdnoPjLRUmXSdZu/D1jqRtDLGY3eOK+gmiV9jMocJuUM2CMmvff2nv+C9//BWr9sf4K6x+zr+0V+13da54N8QCJdZ0e18IaLp32tY5VlVGmsrKGXZvRSV3hWxhgRxQB8fUUAFfR/7EH/BW/wD4KG/8E4vDGu+C/wBjT9oy48H6T4kv4r3WNObw7pmpQy3EaFFlVb+2nETlCFYx7d4VN27YuACz+2t/wWA/4KGf8FFPA+l/Dr9sr462XjTS9E1I32jrL4E0Oyns5ymxjHcWdlDMqsuNyB9jbVLAlVI+Z6APeP2Hf+CmP7bf/BN3VvEWufsX/Gv/AIQy68V29vB4gl/4RvTdR+1RwNI0S4vraYJtMr8oFJ3c5wMan7cH/BWf/goH/wAFH/D2g+Ff2z/j9/wmWn+GLya70O3/AOEV0rTvs00qKkjbrG1hZ8qqjDlgMcAGgD5zooA9A/Zc/aj+O37Fvx20P9pf9mjxz/wjXjbw19q/sXWv7Mtbz7N9otZbWb9zdRSwvuhnlT5kbG7IwwBH0R+0f/wX/wD+CuP7XHwS1/8AZ0/aE/a0/wCEg8G+KLeODXNH/wCED0C0+0xpKkyr5ttYRyph40bKOp4weCQQD43r3z9hr/gp3+3H/wAE2rvxLffsWfG//hC5vGEdoniJv+Ea0zUfta2xmMA/062m8vaZ5fubc7uc4GAD6D/4ijf+C6//AEfL/wCYz8Mf/Kyj/iKN/wCC6/8A0fL/AOYz8Mf/ACsoAP8AiKN/4Lr/APR8v/mM/DH/AMrKP+Io3/guv/0fL/5jPwx/8rKAPkD9qP8Aaj+O37aXx21z9pf9pfxz/wAJL428S/Zf7a1r+zLWz+0/Z7WK1h/c2sUUKbYYIk+VFztycsST9R/DH/g5B/4LMfBj4daH8JfhZ+11Z6H4b8N6XBp2h6PYfC/wwsNnawoEjiUf2b0CgcnJPUkkk0AfH3xZ+K3xE+OvxO174zfFzxZda74n8UatPqevaxebfMu7qZy8khCgKuWJwqgKowFAAAHO0AFdh8BPjz8Xf2X/AIx+Hf2gPgL42uPDnjDwrqK32g61axRyNbTAEZKSq0cispZWjdWR1ZlZWUkEA+xPEH/Bzb/wW58WaBfeFvFH7ZtrqOmanZyWmo6fffCvwtLDdQSIUkikRtMKujKSpUgggkGvg+5ne6uJLmVEDSOWZY41RQSc8KoAUegAAHYUAepfsp/tw/tcfsO+Lbnxv+yZ+0D4k8C6hfLGuo/2LfbYL5Y23ItxA4aKcKScCRGA3MOjHP1T/wARRv8AwXX/AOj5f/MZ+GP/AJWUAH/EUb/wXX/6Pl/8xn4Y/wDlZR/xFG/8F1/+j5f/ADGfhj/5WUAecftJf8F3/wDgrf8AtbeB7v4afHP9tvxLe6DqFo9pqWlaHYWOiw3sDkb4pxptvb+cjAYKvuBUlTwSD8j0AFfYH7Ln/Ber/grB+xd8CdD/AGaP2aP2rP8AhGvBPhv7V/Yui/8ACC6FefZvtF1LdTfvrqxlmfdPPK/zO2N2BhQAADwf9rD9rn9oX9uL426j+0X+1H8Qf+Eo8Zatb28Goax/ZNpZebHBEsMS+VaRRRLtRFXIQE4ycnJrzegD6R/Y+/4K8/8ABSb9grw4vgv9lL9rbxJ4Y0GOSWSHw7NHbalpsLyEmR47S+imhjZiSxZUBLHJOea9u/4ijf8Aguv/ANHy/wDmM/DH/wArKAD/AIijf+C6/wD0fL/5jPwx/wDKyvPv2jf+C9f/AAV4/av8CXXwy+Nf7b/iS60G+tZLXUNN0HTdP0RLyCTbvimOm29u0yMF2lXLAqWXG12BAPkGvsP9mb/gvf8A8FXf2Ofgto37PH7NP7T1n4U8HaAki6Xo9n8OPDsojMkjSO7yTae8srs7MzPI7MxPJNAHz9+1D+1R8fv20fjXq37RP7TnxIuvFfjLXBCupaxc20MG9YolijRIYEjiiRURQFjRV6nGSSfPaACtDwt4o8ReCPE+m+NPCGtXGm6to9/De6XqFnKUmtbiJxJHKjDlWV1VgR0IFAH3f/xFG/8ABdf/AKPl/wDMZ+GP/lZXwx4/8c+Ifib441f4i+Lnsm1TXNRmvtRbTtKt7GBppXLuUt7aOOGFSxJCRoqjoABxQB+t3/BqL8BP2z/2gvjxJ45k/ar+J3g39nX4QX39seJPD+i/EHUNN0fWNWYebFZPBFMkLRnaJ7nKkNEixvxMCPJ/+DlL/gstp3/BTj9pi1+EvwO1COb4Q/DG6uIPDeoLCN2v6g3yXGpBsbhAwVY4V7opkODLtQA/NGigAooA9A/Zc/aj+O37Fvx20P8AaX/Zo8c/8I1428Nfav7F1r+zLW8+zfaLWW1m/c3UUsL7oZ5U+ZGxuyMMAR9EftH/APBf/wD4K4/tcfBLX/2dP2hP2tP+Eg8G+KLeODXNH/4QPQLT7TGkqTKvm21hHKmHjRso6njB4JBAPjeigD6i/Y2/4LRf8FOP+Cf/AMNrj4Pfsm/tW6n4Z8L3F412NDutD03Vbe3mbJdoF1C2n+zhiSzLFsDt8zAnmuw+OP8AwcJf8Fbv2mPhpqPwb/aA/aV0Xxh4X1aMLfaHr3wj8K3EEhH3XAbTPldTysi4ZGwVIIBoA+La6L4VfFr4ofAv4gaZ8V/gz8QdY8K+JtFn87Ste0HUJLW6tXwQSkkZDDKkqRnDKSCCCRQB9paB/wAHOP8AwXK8NaLbaDp37dl5JBaxCOKTUPAfh67mZR3eafT3kkP+07MT3NW/+Io3/guv/wBHy/8AmM/DH/ysoAP+Io3/AILr/wDR8v8A5jPwx/8AKyj/AIijf+C6/wD0fL/5jPwx/wDKygDxT9rz/gsD/wAFLf27vDUngj9qf9rvxL4k0GeSJ7nw7bx22m6dcNGcoZLSxihhkKsAwLIfmAbqAa+a6ACvuT4Zf8HI3/BaP4O/Dfw/8I/hx+2b/Zvh3wrodpo+g6f/AMK78OTfZbK2hSGCLzJdPaR9saKu52ZjjJJJJoA2/wDiKN/4Lr/9Hy/+Yz8Mf/Kyj/iKN/4Lr/8AR8v/AJjPwx/8rKAD/iKN/wCC6/8A0fL/AOYz8Mf/ACsr5w/az/4KT/t4/t0MsX7WH7U/i7xlZx3j3UOjX+oeTpsMzMzGSOygCW8ZG4gbYxtXCrhQAADw+nI7xuJI2KspyrL1BoA+0vg7/wAHEn/BaD4E+D4fAngH9vHxHLp1vGiW6+JtF0zXJo1VQqqJ9StZ5QAAON+K6v8A4ijf+C6//R8v/mM/DH/ysoAP+Io3/guv/wBHy/8AmM/DH/yso/4ijf8Aguv/ANHy/wDmM/DH/wArKAOT+Mf/AAcRf8Fn/jv4Sm8EeP8A9vLxJFp9xHJHMvhnR9M0OaRHXaymbTbWCUgjtuwO1fFzu8jmSRizMcszdSaAG0UAfTH7Ev8AwWC/4KL/APBOjwFq3wx/Y1/aI/4Q7Q9c1f8AtTVLH/hEdI1Dz7vyki8zffWkzr8kaDarBeM4ySTyn7bn/BRX9sf/AIKN+MtF+IH7Zfxi/wCEy1fw/pjafo93/wAI/p2n/Z7ZpDIU22NvCrfOSdzAntnHFAHiVFAHoH7Ln7Ufx2/Yt+O2h/tL/s0eOf8AhGvG3hr7V/Yutf2Za3n2b7Ray2s37m6ilhfdDPKnzI2N2RhgCPoj9o//AIL/AP8AwVx/a4+CWv8A7On7Qn7Wn/CQeDfFFvHBrmj/APCB6BafaY0lSZV822sI5Uw8aNlHU8YPBIIB8b19OfsT/wDBY7/go9/wTr+G+pfCL9jj9ov/AIQ/w7rGuPrGpaf/AMIjo+oedetDFC0vmX1pNIv7uGJdqsF+XOMkkgHsf/EUb/wXX/6Pl/8AMZ+GP/lZR/xFG/8ABdf/AKPl/wDMZ+GP/lZQAf8AEUb/AMF1/wDo+X/zGfhj/wCVlH/EUb/wXX/6Pl/8xn4Y/wDlZQB8QfFj4peO/jj8U/E3xr+KWu/2p4m8YeILzW/EWpfZYoPtd/dzvPcTeXEqxx7pZHbaiqgzhQAAK+w/hj/wcg/8FmPgx8OtD+Evws/a6s9D8N+G9Lg07Q9HsPhf4YWGztYUCRxKP7N6BQOTknqSSSaAPj74s/Fb4ifHX4na98Zvi54sutd8T+KNWn1PXtYvNvmXd1M5eSQhQFXLE4VQFUYCgAADnaACuw+Anx5+Lv7L/wAY/Dv7QHwF8bXHhzxh4V1Fb7QdatYo5GtpgCMlJVaORWUsrRurI6sysrKSCAfYniD/AIObf+C3PizQL7wt4o/bNtdR0zU7OS01HT774V+FpYbqCRCkkUiNphV0ZSVKkEEEg18H3M73VxJcyogaRyzLHGqKCTnhVACj0AAA7CgD3L9jv/gpn+3p+wHJcD9kP9p3xJ4MtLy7F1eaPayRXWnXE+3b5slncpJbu+0BdzRkkBRngY+if+Io3/guv/0fL/5jPwx/8rKAD/iKN/4Lr/8AR8v/AJjPwx/8rKP+Io3/AILr/wDR8v8A5jPwx/8AKygDxj9rj/gsb/wU0/bq8LTeA/2ov2v/ABL4i8P3BiN34etYbXS9PuTGcp5trYRQwy4b5vnVvmCnqoI+Z6ACvuD4T/8ABx5/wWb+B3ws8NfBT4W/tk/2X4Z8H+H7PRPDum/8K88Oz/ZLC0gSCCHzJtPaSTZFGi7nZmbGWJJJoA+SPjZ8Z/iX+0X8XPEfx3+MviT+2PFXi3WJ9U8Qar9jht/tV3MxeSTyoESOPLEnaiqo7AVy1AH1j+y9/wAFzP8AgrD+xt4Etfhf+z5+2n4k0zw7p9qtrpmi6xY2Os21hAv3YrdNRgnFug6BY9oA4GBxXp3/ABFG/wDBdf8A6Pl/8xn4Y/8AlZQAf8RRv/Bdf/o+X/zGfhj/AOVlePftb/8ABZr/AIKfft0eEJfh7+09+2F4k1/w9cLGt54fsbe00mwuxGzMnnW+nwwRT4Zt37xWyVQnlFwAfMNfcfwx/wCDkH/gsx8GPh1ofwl+Fn7XVnofhvw3pcGnaHo9h8L/AAwsNnawoEjiUf2b0CgcnJPUkkk0AfH3xZ+K3xE+OvxO174zfFzxZda74n8UatPqevaxebfMu7qZy8khCgKuWJwqgKowFAAAHO0AFdh8BPjz8Xf2X/jH4d/aA+Avja48OeMPCuorfaDrVrFHI1tMARkpKrRyKyllaN1ZHVmVlZSQQD7E8Qf8HNv/AAW58WaBfeFvFH7ZtrqOmanZyWmo6fffCvwtLDdQSIUkikRtMKujKSpUgggkGvg+5ne6uJLmVEDSOWZY41RQSc8KoAUegAAHYUAR0UAFFABRQAUUAFFABRQAUUAfbn/Dov4y/wDQq3H/AIB//WooA+Rvi/8ADDxP8Eviz4o+DPja1MOs+EvEV7o2rQspXZc2s7wSDB6YdGrnKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooA+iPAv/BVr9vr4Yfsc6p+wJ8OPj2dB+E+uRXEeseGtI8MaXby3qzuGnEt8lqLx/M2hHJmy0f7o5j+SvnegAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACvSP2PfgBrH7Vn7Vnw5/Zr0KKRrjxz4103RS8KkmGO4uUjklPoqRl5CeyqT2oA/uG/wCFB/Bj/ommk/8AgKKKAP5k/wDg7p/YC1D9mj/goUn7VvhXR/L8I/Gyy+3vJDHiO21y2SOK9iOOhkXyLnJOWaabA+Q1+TtABRQAUUAFFABRQAUUAFFABRQAUUAFFABRQAUUAFFABRQAUUAFFABRQAUUAFFABRQAUUAFFABRQAUUAFFABRQAUUAFFABRQAUUAFFABRQAUUAFFABRQAUUAFFABRQAUUAFFABRQAUUAFFABRQAUUAFFABRQAUUAFFABRQAUUAFFABRQAUUAFFABRQAUUAFFABRQAUUAFFABRQAUUAFFABRQAUUAFFABRQAUUAFFABRQAUUAFFABRQAUUAFFABRQAUUAFFABRQAUUAFFABRQAUUAFFABRQAUUAFFABRQAUUAFFABRQAUUAFfs//AMGbf/BP69+L37WfiL9vzxlo+fDvwssZNK8Lyyx/Lca9ewlHZT0PkWbybgeQ13Cw6UAf0u0UAfNf/BWP/gnP4C/4KifsUeKP2XvFslvZatOg1HwTr88Zb+x9ahVvs9xwCfLO54pAOTFNIBhsEfxi/Hj4G/FL9mf4x+JPgF8bPCVxoXirwnq0un63pd0vzQzIeoPR0YYdHXKujKykqwJAORooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKACigAooAKKAPRv2Tf2WfjJ+2r+0N4W/Zi+AXhptU8UeLNSW0sYjkRQJgtLczMAdkMUavI74O1EY4JwD/aR/wTr/AGGfhf8A8E5P2QPB/wCyZ8KVWa18O2O7VtXaEJLq+pSHfdXsg55kkJwpJ2IEjBIQUAe3UUAFfmx/wX6/4IF+Cf8Agqr4DX4z/BX+zvDvxw8N6eY9L1S4UR23iW1UErp944HysDnyZznYSVYFGBQA/lS+M3wX+K/7PHxO1j4MfHD4f6p4X8VeH7xrbWND1i1MM9vIPUHqpBDK65V1KspKkE8vQAUUAFFABRQAUUAFFABRQAUUAFFABRQAUUAFFABRQAUUAFFABRQAUUAFFABRQAUUAFFABRQAUUAFFABRQAUUAFFABRQAUUAFFABRQAUUAFFABRQAUUAFFABRQAUUAFFABRQAUUAFFABRQAUUAFFABRQAUUAFFABRQAUUAFFABRQAUUAFFABRQAUUAFFABRQAUUAFFABRQAUUAFFABRQAUUAFFABRQAUUAFFABRQAUUAFFABRQAUUAFFABRQAUUAFFABRQAUUAFFABRQAUUAFFABRQAUUAFFABRQAUUAFFABRQAV3X7OH7Nfxy/a5+MWjfAL9nP4cal4q8Wa9cCLT9K02HccfxSyMcLFEg+Z5XKoigliAM0Af1k/8ELf+CG/wt/4JHfCGbXvEl1Y+J/jF4qs0Xxh4uhhPlWcWQ39m2JYblt1YAs5CtO6hmChY44/vmgAooAKKAPln/gpf/wAEev2Kf+CqXgdND/aN8Atb+JNPtmi8PePvD5S31jS85IUSlSs8OSSYZldOSVCsdw/nl/4KC/8ABqX/AMFJP2QNQu/EvwH8OL8b/BcbM0Oo+C7Urq8KDoJ9MZmlLf8AXu049SpOKAPzT8UeFPFPgbxBdeE/GvhrUNH1WxmMV9pmqWclvcW8g6o8cgDI3sQDWfQAUUAFFABRQAUUAFFABRQAUUAFFABRQAUUAFFABRQAUUAFFABRQAUUAFFABRQAUUAFFABRQAUUAFFABRQAUUAFFABRQAUUAFFABRQAUUAFFABRQAUUAFFABRQAUUAFFABRQAUUAFFABRQAUUAFFABRQAUUAFFABRQAUUAFFABRQAUUAFFABRQAUUAFFABRQAUUAFFABRQAUUAFFABRQAUUAFFABRQAUUAFFABRQAUUAFFABRQAUUAFFABRQAUUAFFABRQAUUAFFABRQAUUAFFABRQAUUAFFABRQAUUAFbHgT4fePfil4ptPA3wy8Eax4i1u/k2WOj6Dpst5dXLf3Y4olZ3PsAaAP1C/wCCef8AwaTf8FBv2qL2z8YftTJD8EfBrurzLr0Iudeuo+pEVgjDyD1Um5eJlOCI3HFf0Mf8E7v+CWP7Gf8AwS++GbfD39ln4aLaXl7Gi+IPF+rMtxrGtMvQ3FxtX5QeRDGqRKSSqAliQD6KooAKKACigAooA8x/ad/Zm/Zv/aD8A6lH8e/2ffBHjhbPS52tV8X+E7PUhCyozKU+0RvtIbkYxg81/Kv/AMFd/gv8Hfhr8VPFmnfDn4T+GfD9vb2KtbwaJoNvapEfNIyoiRQpx6UAfnzRQAUUAFFABRQAUUAFFABRQAUUAFFABRQAUUAFFABRQAUUAFFABRQAUUAFFABRQAUUAFFABRQAUUAFFABRQAUUAFFABRQAUUAFFABRQAUUAFFABRQAUUAFFABRQAUUAFFABRQAUUAFFABRQAUUAFFABRQAUUAFFABRQAUUAFFABRQAUUAFFABRQAUUAFFABRQAUUAFFABRQAUUAFFABRQAUUAFFABRQAUUAFFABRQAUUAFFABRQAUUAFFABRQAUUAFFABRQAUUAFFABRQAUUAFFABRQAUUAFFABRQB+mf/AARq+AvwN+KHxf8AC+mfEz4MeE/EVtcafYtcW+u+Hba7jlZgNxZZUYEnvnrX9R/wI/Z4+AH7PvhSHRPgJ8DPB/geznt4zNaeEPDNppsUhxn5lt40B5J6igDuqKACigAooA//2Q==',
        kuka: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAN0AAAEjCAYAAAC2KwTHAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAALM8SURBVHhe7f0HgG1ZVeePrxsq18u5+3XO3XSkaVKTQSSIgoiogCKKAYG/4a8/neTM/GZGHR0dFRUBA8ZBVIYcREmCZOicc79+OVW+dcPv+1n7rHtP3XerboVX9Trcb/d++5wd1l5r7bV2OueeKjR+fkvDpqfNZmtmhaJZQ3G9YVYyXSsOFLK4nl1EVqSvFgq5BvL8BDokLQnz8R90O7X5aEKDjlouQrZ6FgPZQB7zyd/IFFc4yfpZa30Xyie2mb/P298JkN686EI8qz70SmqnVLLCu8cLhcbbNzRmp6asUK1bSU5XCKcrKlA4a7SWES42UqfkdR36Xys0ckpZaZ/Px/vJtqXlIi9rJxS8P5bbAQ3VzDscaHO6+bAaTtdF1k5o537pQI4kczddt6PYqKv2wnXQsNOV0xVxuj+fkdO9bX2j0sHpCnI65yHTrVIdxUzZp8Iol6qUk4nlmvVK0VXkFY54xXmNpgvdVXC6U9K/mRztbcftghOd0E37kHFacrgSTvfeSprpqpNT8qrkdFavJj3mZjoYilVlp5luLXCiUtaGgUKm9W7KXS3UWXUshNV2uvmsbhWcLllnC/Rx6H+1oMlqDpZqV924KxSlYWhqpivgdH82nWa62ZzTzTfT4XTcx0yHT641TnQ8hez6ZKFdie19vuYzfJf2YjBcLubvxy6EHyUz3Uqb79TkMtiYF4XiictLn7byo0leiPkGmTU3vAztox63qx0e7TgVg99qYbVntU7o1M/5tLhebsDh8Dl3vMybNdNpeak9XaNa85muWM92b+5ZqpDumhCdhGyZuebIGF91LLaZ1WZnIXmbnbEayPp3vvZP1kyHZYK2dmLW6+qIK2x+Dk7gocXechG7g0ZRe7qylph+kPJWLS/ldDwyKGr9GU6nHBcI3eZZCR4KofRTgOUsQ5aKpi11a2r1WVkYq9YNifC8uj5JThdO1d5O3HYz+pX6fB6rYVZBMu90zeVlPjiI+V9By9J0q3ACoLpWgWGD/aaiCCcbJ9BF5nxoR3v+yQ6nGO22cbJD9K3u0n+Z/jFMQtzPF+bYxwpDJ/rdwnIwZ40456aHHnpYFXT0swUHWF9vZiXwdC7XKETT+XCy0ZUueavQbg/LRPTHSQh5u1psWA7c6ZZ1ArbMqbWHHp7o6K0o50HXEY20R0Po4TGHntP10MMaY9FO93gdWFkl58N8mHfGO9VoF2Cp4TGO/GrkVITloO3Vu8W9s+1tLbPBHnp4osOdjucNxZzDNfIPILjMhaav+Rspa7M65cHpaj0QR558aEf+mUxeLasK2onQDe0CLDV0TCR0wclWCP27jD7O981ygpt9FjrldwvLwYleU1jcbNdDDz0sD02nW+nb6j300MPiMGem6zleDz2sPtZmU9ZDDz000XO6HnpYY/Scroce1hg9p+uhhzXG4p2OZxLLfC6xEqzFD1Z7OIVY5vO5xzJ6M92jDb0x5nGPntP10MMao+d0PfSwxug5XQ89rDF6TtdDD2uMntP10MMaY47TLedbKc0qXEQA/PQn/0HaOBqO4+HIjzr8ySe/zxKyaM5PKJp1FDJ4XpbWaBtDom6z/kLI6ju5lJLRVsj9/IMy9UK0Nbe9lSC1PZcefxPGQ/6d2OCpLVAmleMfBf+VZZafQ6tchtC3gIi1QsFDtJ3/pWaowRGE5vk1Z5RtyuAhkLUZttCGIJnI8k8rzG1ubl5HdGyCv7WTwqlAs5fndbhMP53QUk5WyJ2KSxmQOo6QHKKNgN/iZAr0hJfpV+gjI+XX9A/B86UmyqhOw/oUSn6fmiVGgeQRJ8dpV2fctzpsbplUv8+qxaJCM7EVqgriBZmqBZV12aDQVOGKUC0WbNY/QwrdzFgxfvSYirR4ISSLTkGAFXcm1ym6Df5aqIu8ZwXLWV3gpFw2yV8oK/SrGf52mwqLTp4frq2mdEK9VSZQV8ckfkTP9ZkNVF53HmRyUa+mQB8Q+0Dk9NOgFIOCO0yW1gpKC4SeQMRC2EhNMSGXtSzAb070eeHf+MwgaU4y8sIKzlC0l2cuyuDthbp3CkbhHOXr5ICyUgHC3O7LTCG7FoJ+W1zwRhRzn6XllVbn94Qd2g5gCM2ZwonMa0aLh+h550V3iCx/qCU1Q5quuJkvCM6JrhNvquOGynVC5CVDFnJ5gIETZ0moW6mRQvp9JU6U8vGz1KfwRWiVaQWQzLukLEIaJIshYb5Y6xoeFUEfXpv9kpVJ7LV4aSzzt5/+wgX6OUVotpw6a2lACSlkGgotKUoKSvARKMtqxTUPyF4tNWy2VPXACMdojG80NEIyIboNuTMw3Sh4nMHT07hVbBDkfG5wDOnMSMSaQRUKdQWPB9TAkPhi1lQWZHzcm5GhqUlkctrwohG0JBqEIszATmpPuX6/EsAqskKv1JBcbnUKikuy8LIyC5KjroKM8BXxM1Mq21Rfv1XKqqj/CWGkSU8KRWKkSMNR3blWgE6jrHutLKiYhM8Cc1vVBupVOUpFRNP0XpdCahocayhGFZw9qYJJztRn3pYPnroXuCxJPR7qcl7+IpRCUQG5PAAnRN8MiG+tMmIFkUOaGxUYANS/nqt/kt5ok7Qk4eLnLbSh0GJ5TUGXNbEsx8tiB6MfoTlE6RJVMItJmc3Ryz0SoyWAgiuAQD6jcU0dUPeQ5wtqOFVavuRDE5AmSDTaxYxO2INlZdKSNS1ToevG4ulZGQd8Z3KpbEltl92xU+5K4LwTKzAbQLfVsGL+bJmcIPQSS6OQp6Y4lmEg8SQaDGg+mhOaUuta/LsTa8ZBdjkf+VQJHTZlh5UMoZc+kS3zWXsVqMvR6sVs8Is6BNCsqwR48AxkyQpkg4HrFDZ06VU0WNC3tFdWAqFVP9V1PSnAD3IlmVv5HZHJNhcQJ6w9Co23rG80pqetVp9t/gER5EzfmdfFCbJkErjBgmA8014O3q+KWZJwFYZdzNP1Oox2MgApvV6allFpaVUdVLoqFqZVLxvhBJTsBpiFoAPdpsGk7uAizZhcKLOAs6TLlOj10+zlRpHR9HwXS8bN7CZBsGNP9Ati7gWXbXnAcWJk9z/cEqp0hjO0RPHBJ2Wlb/6Dph6UQxqX6KI5ngHRCD2wKvF82qIi05Xyqn1V9RNLSpxfeWmSy9rmHwW/VqYI1KQW2iv6vk7B8wQvin4gQsjQvEyDhg9koKEZVaCa9yE3zhsXbYi0aAvkr/Pw9E6ZGrA70V4mku7V0jw0m8ntf59uRcg3BgM5knMZSaN0E3P0IS378iHr5aZDAwwz3Qe9fFVvUoEsnwF8iUIiTpSWgbEUdBsjiJz7GuUAlbOQX5pSITmqLNCXtKLjPGbIs7ksiIFGv2yUQ6QgpjT46hAwSpyC2baoGYfgGeIVvpsORQC5GOqQSQNeFJIsJCo36qMfnzm9sP7x2TAbFJ1HpfF/0HaQrkA/eV8R5gd2UGcFomsfkEWvucIgMeJQNbzQX0HWy5BI6ABPTnmQmsPqowALa2cJcINuDqEgEzfXO3RqMupIUPB1AvuCGYXZbO9QE61k6H6wMQ8gXZQfEPSvllralbHnKcvNss70MllgJT+r2XxGHTgtG6oouAF6J0ZIy+FY+rjz0kDaoKTOV6j0FUWDEVv3K0HTwQmip5m1Jhm8bQUGCD+8SaUTcvzGcrFU65cTwpwQPBETMhpNOyVQlFDWTFNiL1uzfsRz2tKfeKj7PpaZUAR0X5O8VQK8CUR1VaiX1W/lNFMmXpXjBqFA30AzgvQdhzTUT4NIPj/t8awxRK7zWhML00qeUfB9dWHIqiWFIquh+QHZxCXL8RawiQinApn6loncMoINvOsspGFGYJbwkJY+GL4Xz5ZU9EvLEFKmL6EUfNvf0JJXtNM+jgqtQJT+ORHeDhfkZ53phql7PxUkn0AevKR+ScEbUlWfNYlJ86Q5YAkMZ6nS8uA85GZiyKWZhpkgzTqJG/jWP3GT50fOwmFF0jf3KTn9fTPxRsgBuqF/HDqFrKJmTpyYBnzpi2x+SJL2b/QxjuU0gKqgHw5ayPNDnCYSHUfw5OAmHRpx6IWTe7WMhVQP72JE0AoA+rpCQzg0uvFxW9dz28vQqck82jKy7l5TIOEJ8D7xZUi67woUI+6TEjIVuTTQaFhZIxWLidmKZi+yCwNW0Ug1qes2m/BBlTIl0Spw8qVAvzsvOX6Uow5QxzBbUUcd2C8+BlW+b0YOSx1vK4KWXor6Z+s2OKtyCtrGaMTss2kyhpKRYwUFGVilXtGg2+ed7W3QtuokHrQ+VxtD4mFlnSaHK0yJJkEtSYZaTcZdlUFKtqKsrKTZZlrZVpYhssIVj0yKbpfwy7NMTj3hUQLTbdhro1yyGemiob2EP9sS74hZ1MxVq6bBDfU4ydAh/xT7VF3GzvLV96+aCbXvKmrVwclm6g+GGy3j1ZavQEVIzSlWH1dVVjdViKtt71D49MYVABXrklFyehMA3Q6Kt/pssqPBAWmHU1PpWvU4PBmUTkpSeKE+Y33FKfHC6ihV946CF+SXwVUqKhfGpAALDvLXGPCUHlOkxv0gpT41JR1LCWKy+eePye/IYMY+ww3KgZCSZqV0OrJf1d0QMyVgALPqWWzEDUcb95lCv03292uElCPMjMnetbSR9irqzAkUJQPcWKlQNQE6eV5UBodj1MWZG4zQWvbMTnAtHkaHVUhGp4b97QrVr9XSn3cedB40QOgefqYVajZt9clx26gVDavaWWXNDq63SRlCWaM4He3Lr6K41FqnOiNDlBH2D8rwZifFHh27TCCu2pyWgxWGRmRZQzKYqgYEGb3knKxOW1ltrUMB1VnpTBEyYk/oRVbPyf50rU9d0mfFgYbNqmhNGi+X+606NW4j8qH+ujwXD0MZeBkrONHDgWdluwPQQm5d941o2dbfZ5NjY9bfX7K6nKmmfq6z1Cz1yylgmmuRrMn4yw0bKA/Y1MSkrR9aJ6euWK0i2mKsTyMhKx2MwidUQIzKiN3ZREg2oIWNWChaeXCDHZvSjWyERxKzMCUa/Wpbi2nZaMUGGloSM1hQn+U4/GuAqs9oddSnfuHxRA5ke4NtXeUrmRUg6s83+JLsvibeGEAL760UTorTcVmV3pCn6XQhnBSjvvXZzkqDNjVet4nyejs2utHGqgV1dsn6UKhG7Ko8pqLRbr3Kbjj6sI3UJr2+zwLA+VH74jOWRhV5iLZwzkrfgDqpOKQOq9uYrKo6OKpZTMandDquVpmxIZzah3m1PTBg0zLqob6KbSoet+HCtDWOaZaTco4O7LCxggxI/M3OqiMxIHV5qVq2fulqU9+0Vcf32fphGZX3/NLB8m28UreBjZvtwPSg2tMQIiOnvZL8elBGN1Oo2LYhddie+20L2xx5W51nXtIFA8oBjeiljdvseGXQqgVxKBVMz8zI8cqSq2zrNEuN1MZtePJIciw8BR0UtA9TG8VhtSnL4XDGlzhV6VpeXceih0btYEX3A+usIrmnaUvOhZHPqE80T2mL1WcDuq+MHbX1aqCsQWiDfLZQn5Yjqn8qcvacRcJ3Ubqnv5wd38umUVls28DIiE2qyqHysM0O6FozX1l6GSxIPs2MNZUd0f5/y+wxG5DDQ4STdmaSIisTOTtiMNtqtBDtrO250UnDKXM6kmZxDqEcTscCXP9r7nEyak83JZtqrLehZ7zI7MnPU+F1SlQn0qSWValHFGTM9sn32dS931LHsZRR92p2asK1qqbVfgHCMhBeXToy1bDjjUE77zrRvv7F6nGV65OlelX9IyN0B56VtTHT1dTLZTnMgXvs2Af+3GzsmG0YlgGV1lv/S99otvtJKs9oi6FRXjxqoPBj7kN3WOUT77PCsYc1aIjOMjCrAWG6b52t+87XmO28XG1ophuSYH0KasKn7boGnvEDNv4377SRqYPSp/hVH7EXGq/32ehVTzV79svljNtcvz4C+YiHMavsxEGzr33Wpr/xGS294VPyoEs/QJEDywGqui+q/70L0VXfoE2qXx6ebtiOq59l6698mtlpF6ZZhWmUPqejNRt6U6x/C5kujz9i9qG/seN77rEhxkD6XYAsY13aU+tCN7F/xAAafXJiFaprTn9kqmxnf/cPmp11gdmIVi0arG1GZeFbg4VGO7Pbvm4PfOz9tk39pc2KHHZG7MioZRs1rRTwuQL/0M80DhSHk5wsrL3TeSytq0UNhI7WxlgJFPWHPklw+uZoY6Pt/N4fM3uRDG1AhlIelTKlHNqFc95wmN5n0+/6L3b0G5+y9f0aXeUc6RAEX4MojVBelyp+VM41tXGHHR7YbGc98ztt9OXfL7py6CJOLaebUfkBdR7S02FFDE6dN3PI7Nav2Mzf/anVD99rQ3KsmaPHbXrz6bbhF383OYLJEfrkdNpDeLu+HhONh79qt/2v/2C7Zw7YaHVCeUvHlJxsb2mbnfMz/9XsnGerHXhWO+hMe0YN7ZrudH3wbrvvP7/FNo49LGOqasbSvlV8HLZha1z3ctvy+rdKzNPFm5wAnaBHYmaCygGzD/65PfLRv7KtDc3kci2Wff1aUmuV7+NSSYNRfUYzOXW0JD040bDx9afZlmueY+t+8Kekgp2it1H9Irp9qsTAhZPx1gpmMHNcaRocjjxgM+/+bRu/59ualaWrCekJp3OeCCzElSDb8cM1nA478pmuYZOazfZV1tvmK663DT/+c2p3s/JUkYGTI1NmWXRTlVz777EH3vNbZrd/wU4b0P5b9OoaTAckRE1bE+yNVeccKC2cRJeO7HbZWI7TtbO1dPjyID0fIiQe+JfTL21otVRhhQL6hrTv6JcCS3KAojqxtMlqfVutMrBJmQrlDUqX4ckRJ7WEYs9Xw2FlZA3tXXhQ3XyoGlCHlDafZnsLW+yy736zjb7yJ2WoZyh9h+iojX7RXbdN7Y5YDQcnjU6cPGb1L37a7vjTP7Bje+6zGgaidfCANkCHJ6ZTPWaPwdMUb9FWdJPGEQXxq/Wg2ajSRkZ9ibdcsHStMIv2S2bakh5m+mVoZbUxuEtGp1De4ul1LbcGpLshLYnZm7KRm5RiG0Pic2i7ZKIOsUJJdaCHTjXozGhGrGsl0Kf9Ymmw3wZGB60wQL+JvHyjpAGsT7PCjJanByv9dnTdLtv5zJfauje8PTlz/y6r9G20yjrJryVfQysBjpFskEFCvDAz3/Jtu/MPf8/2PXCXJkE5BXsKVONWh7SZwwl+NsvSjxGTwZM9tkbtsapm/m1n2IaXv1Z9dqbqSc9D0oFkawxts5oG1SPVdTaLfDsuszNf9gM2Jb1MasAocmCkNqrapjCIsABijJyDOV2FHbXZ0hphZa3i5j5SMXJlMrmCAadmmjl9n6VkKYCDkinKFzV7yKhr/RukpHr2ug+sRChYX1X7L42kgwoyGXWXRkZGSp5j0dFqp6Kye1T30Oh2u/onfsXsmZo9i2ep6V1aNqmN4qBGmbqvCCdVnmpW0Ih88B6N/u+0u/7yf9rW6YdsQ2nMRterlzTjs/Rcv1mdqv0LhoBQHBCyiClofhBnacSf0nAwNqOBBqLLBaORHNyY3dSkWChoU8Zez5dxMM4yXfu+kvgqTI9pRtLgwFGklhQjg8rjnuXyjGgEtMqQGaqMiGoJWJJTaKeochM2rb1dldme2SrTSf2oViqzfVqFbLAjmy+y81/9Mzb4A9nsOSAnrg/4Y1ROe72fdVHi8KOhGX56r9nH/9pu/LPftvp937LtStvAidS0KqAaFeP53oz2fjwjVXWBtnE2Bd9YavtQVn55SM7+HLOLtawf2mi1UQ0cLIcp6k6qcUj2VFQ569PAfcGldtHTnqFlqehrb89MwoGZTy3+PE8VUoOPKsyxGGYqx2IZbc6pHCbkAdm02ef4u6QZS4O6gpY2zPlae/OsQH2o5ZIck8qxP5Rt4MwF7Z/8zQvoQFFltBjWFX3QbxOFYTtU2myN859iZ7/lP5hdcr3qaaYs9bspF1mKsGEXE2UZNpvvQk3LyQe+YQ//2f+0uz/8l3bhUNVGG+NWLqrRaRm//InK4+MYpdoblNNmgtUw/uYgI/615PST0CbmqLKFUNEJSOUbvlRzRfl9VfryKizjJIsP2cof0rJqUI7WJ0X44CH4qR4zCsrRDBgsQslN1PdA2qNKl3WOOEVjQMu4Em/XBF/Y/KZNtnd22CY2XmAX/NDPmj33e1VWM6eW6DVGHNFEVFc/+2CWkrOHtZy8z8b/8vfstn94jwavfXbhZumEU9LDWm6KbYfqwlcT0c+kEVSOfeV4Y8BmNu22TS/T1kAz2rgq0Y8ukByoxP4TmyBNmJiUDJr97LvfYBObzlJ5zeByRrYfdQ6DcD6ArnLt+9ZHYH9JCDWsNpq+JYhHcRGcAIzKuVHsFpcCxxmEFrI6JLl3KSaiqhdjrK3IcTRKUzR0wCGERlwKy6QcHIi48WFgjGDKacjoalI0R9+z1C9qo60Ox9CmtHcZ6zvDhp70Ujv9rb+uDfd1qqIO0FKDdjjzSOyJIfYd0zKQ6QfMbviw3fGuf2/TN/2Tnbt5yKqTYz57YpgOZBAL/Wwg+0RAe6AQj4HDhaWMRm4/anNkD5FdaOIIup0TsnTKeB30KWNiioPX+oxp+2rDHItn70P6EoAHjnUNCBgzOuK0gUxkLA1a2Y+FVUbNM17NkhVjgTtsyQ+ZWHIxQ3K2kOqmWEOR7Z1SpQufZuf+9K+aXfFitasldUlLR+WVyhX5rpwIfqiDw1TGtJf6N9v7u//Bxv/tw3aGHbVNCvUpDWq0qSWsV8hUUueEVgNtiQGF/TXpQwXnVYzZWHm93Ta7wc770V+W3tW2rbdRrVL6GT2KGqiZ2dUuq9i6ZjSqjQyJvz4txXdcY2e85udsoqil+fislsmD3l+MFfyAgUdZTadHbqW73+ufbj/jWgwYiLK5oCPmkM/Kwb9fuycuUHle5B0WJI/LOOGtEkV0VAbeQvCxWDEl03G7rmUUXgxFz9Q0+klhs7M+U/mGWJleZv02OzywzbY/9RW28Yf/H/XPBZrH2PPwbEnlMhv0oCWqH3rUjlj9k39t33rXr9ngvtvtjCEtHccPue8wk7oEOJ47hcAMEXzCn2TBsZgzWzoKdXptiR0ZWePcNssCpWW6Ssnp32we9ytv05PlkK4cpXuMrlLdBCVqsIh3Uh3iPXeXrjNaSBEf7eVAkwMU7Hh6cKPtH9pppYufYbvf9h/NzrlSs8xG7X81w/PswamUNLPKetFlhaWsHOuLH7f7pMvSvd+0LbVjNlKfsEERLkHcLVv8+SBDLAqQqVSkPWnQHVLSsI+TQzTUp/u0lL/kxZpdT79EjqTVii85hEyggvbgs8zoQl+Z008SFYpazbCXvfq5tu3yZ9o0e80ZleNZhYDGPOT33UkNCbnk1UR7M5lYbVgjZpLJ+fZXuhAr9A767u/XcmrQBjVCFyqaMTN7O6rrOypDds73vsWKr/5pTQtni8R6GygNyzXqGmsrNi4SM/Sr95s64MgDNvme37S73v8eO33mkG3VOqmhWcqPshW87+iaXGc0Dd7V47tJ7zw/NPE8Yj8OUKycLPibFJ6fVY0QaY5W+VRHBaItv06sKDfpJAvpOgMFnF/tV5GDvFrRn5EyW/ZrRvRDY4gU+rQ0HNIAIwOVOgrS6dhxLd3W77I7pKgtz/9B2/a2/2G28VyJKj1qdp2SHmflQFqMakmnvXFVeygGhJlHzD7ybtv7V79lowfusqHZ4+oyuTSs0Zbar+ufup8sKzEbxPrUFw0t+ZLjD8nhJaQWIAx6h7WPHj3rNBt5/neYbdBqBT2pbfoz3p+lP2b7h21KvPtoLF4qs7IZ6EFycJ2Nftdrbe/oFqsOiF8tPUuDZd/qsmiBZOqzRM9vle4TwilA0kogY2jN4J2iTb/6iLnEAQ9aj1e1Eed1o7KWJZwnHNCa//Cm8+2qn/wVs2d/txyO0zkZg/8wlWoFfw2MI4QBDibGHjLbe4s9/If/rx37+mdst43bNvV0ceKwDFK90SdH0qqN5lxsn9mIcTDxBW+ZM2T2lDj0woykcgTPb4Pn55DvWOW5QzcRUuP9rQzSCIkj0sVDWz2f+b2geMnzITl8NxSOrTKzM1WrTCDzBs0u62xqaLvdqeXcFa99qw1/95uly9PFwgYtxXj1Ctk02Gk6pHpRS0PjAff+O+zIe3/b7v37P7LNUw/ZlpKcRUvEohdSlYx/f9eWAxJXHc8ASfQssabZmaNs8cy+lLnrkPbep73o1Wbbz1d5LRn9ZWf1acikvS6vfQ3Xjlv/zFEaUJqWmv3wqGsaYEA56xI790Wvsn0ahRrab7OFHdB6lCKcDTBo1WnUec3YU57rcI2RSXbq4J2svUmBoY+TPPYsUhIvxA5wmFAbsv2z6+2R3dfbhb/yTrNrXqKhnEcBdI6UqVGUfuVtkWGFwbqWQBWNyDd/3O7+7Z+12h1ftI3VQ+o0Gc7smA0O8LKt2phQN3D6p0sf8cKoPUlq8U5PU+FcR8mQM3Sy6Ty3B11TPgLlmve5cp6l2B+BZCHK+SYfOgreRd5Wq70sI9HgGSdK0MaZ5VrAH7Xw6wjpcVgONFDQfneqYZOazY6tO80ue90vmL34TXK4M1RGs4Po93Fwo7heZ2aFGRam+8xu/agdfccv2/S/vt+29o1bvTShJWrDKizlYEL81ORgrATc15XEC9J1TbexUmfvXNRMxetZJfpVMh2eHbDRqzSAPut14n+LiEgOpjc1648yWK7W5GgHb7ev/+F/s9KxB0VYe0weHem/hsuIfmQLWir3fcd32/D5V9vhxpBNaTVc1KrGnxu7nkVKwY8rFKIfTgVyPXlqEIdMsOInZbxRUeb1p7IdnC3b/bUNNnzVS+2Kt/6a2eZLtZzaYpXyqHxTPdMn01Bxdxqes2m5w4P1qY/8ud36nv9mw/tvtDOGZ23Iz7rVgYyKtKcJ0kb6rOqHEhmalyLoaiHOJXeCZsPmT5WygumegDOkpWm6zsoGvHx6vilTyEInJF7SQJBS8qjyPqQbHg6ChUGH8iFHWqZNyKEOFEfs0JZz7YI3/7LZ018q/Wk/VOL4XcZeLtuMnKigcW9UPPVP8DhADvfVD9u9f/b/WvX2z9uuwWkbKVf8rKSqtZsmmybbvBLGs8Bk1eJAe4KanB72eM0sHeFzUs2S2ux4pWgTI7s0y71WUxKHYNqTwy4h06WfzGrAPPDpv7V9//S3Zl/4sA+cRfVlsS5e6U/3IAZuDRzrdtnWV77R9pc2i+YGq08xkAui55/xyJh9wjtdf0kbcHWZn1JqVnOopw5UCnZw5HQrPfs1tvEt/03rfW2yy9tVlu24uthPE+VBrCNmxqR3DW37b7ep9/x3O/CBP7bdtf22a53W/mPH1CFSvg+diTYvkBybUIeOsOTwFhMgqdA63OAAOoE7v/aOUptZJ/pzOvoy52h808SXfbmQ0rJAowrUTYYA0eQmGEJqOQ+WuEphhPGQUpkl+U2fy6V9XIGZQQRw0AIriLoMuap9bl/NHu7Tku7KZ9iZb/91s8teIH1otQC01GbGwGX7B/psoKyrqQMy7vvMPvB7du/v/bLtOnynlpLTVmlM2MQMLzPX5aPMSqqU7FjtMvvAdVp6uJrEZ5HHGc4wKaogh5nWfuzYwHY759nfY3bR1aqimUr9x6/Xp1R9RrcVijNN3nuT7fv0++wZWxp2z0f+QvuMu8TfYY0tNEJ7Ep5ngN6YHO/ip9vu579aA/M6q2lf4kVQHVsVzEXFgmVYOhWOB0srQLDfFGNp8FFZxusGLL3LThzMQNvOtPOe93Lb/fq3KUMjoRTKx3l04/9Rw02F9yHZuN/5dbv3vb9rB77yCdtVO2LreJY0MWPsqxvT2tzzsNmNU+Y42GfD60pWYb8yB+LC2wgon2p0ju74pUFKo6cwscR7sw7JBIHyc6EyuXxir+vXODEh3fteQ/EJNGiHgOMJ6cgb+RUc8MHsWnKeYdUGBu2Qxpxdz3iBbXmTdHnaZZodNyU6FOAgRHQo6u+3zmhEOvqgHfvT/2n3ffTP7KyBCRssTNgAr+eJ8sjwoC/7a/wEA306K+lEUUNLuleAOgdg/IzIX9FS3Tr9pUFisthvs9svsNLLf1j365XT7z+lYpcH2wDf55W9wx/6a9s89ohtnD1q/UfutSMf/HMR1ZKTV8G8Uc6/6QsZT4ET7G227iXfb2NDW+W82RtDQVQxfDXhvK89NCRKYVknOtygTjLy9PPwjqfnMOjs3T/semDYznnmC7XJ/0GV0fqf42vl+QN2sdeocjzuB9BSssLBh+yL7/oNq9z8edvKER4jPi/JCjMclgwNiMSgOraoEZBXhjUuztS0B1Qx0cXWfN+E5WkZVGQpxHuPrgqZi5YxzMYFjJxDAujLM3h/sMbZO+mI4mJW5TvU5VQWp8xmIAwu3hf1sjJ0z6MuDSnwdobvo5SmKGlG11rP6V9/9pSeUitHpHgGOsjDaoLo8kUtf4qVkfM9cr3Pzr3iejv7jb9ktul8lRsQORVmgGPfJZ5QOSz5NHB8n938V++0+7/8CdsxMG0znDZhz9o79s9qr8c+TnyzbPRG3aiVpz7x56O6dWKwrUCfstjFrWv9JRtXmcMaQM/9kZ9V3+1Q5jrl91l/rd9ZGpLgA9VJqUSz7bf/2Y7c8DXbxQhSHbOtA1Ub+/qnzG74nOpNW00OV/F+yfiHAvvTTWfZma/7cW1NZDfaiiCCFxAZtrn8TpFDHpaZhLVG4nXZiOpdyCzkyP6xUgxJ2vA31Ums2t6H7rPG1/9NeTLEqfSGA5MV7wHz2zm5hooms7RtW+0Zz3ue9a/bYGOVmvyNB7tF3xIMrBu2Sc14k1oWlQeHfLnDWx0+q6ozfPJyoH3JgeE5MPMUeJtKJfW/7slGHjaj0OFNe8TzYzrSVAS2/PMOCixrqY9juZqUxtN+3fqLxlz4038hnJ02cWRvX+UFX+hqkIrXobwdVR2AD347JqcgB1YSor7ZvocescNf+IrWbLqXDLOTY4rVjvaCdX+2UrJZX8+J1qYNdsmTn2wbd59te8cq1rd+kx9KzPKep+QuDQ5IZ4knBitpW1foTZEPIARlcE8Q+H1ieXjAJqSjg33r7JznvlxT78VyOl5ozvTHwKryDTykekSVHrG7PvAntqmirQPtSfyyeF4/cdD2f/BvVGZCK9VjaplBQ7MpLMEPOuYd23OeZOc872W2Z6xqxWHxSBv0g7yM5448V/V+OgVwMzjl8JlQc4k0N6NZiAewEzd/1m59/zvNvvJxcamlhGYPzBHFTfl7ibrmeY/PNIpf+lo75yd/xaa2naf9YNHGjk1q26LlxfikDQ/329CoDEsjaH1aFXl30n/2o6rqaG9ePYBR+bOfJnTtRq7A7JU5gM+KGup597OK5dN52C6dqnsOhFhWUZfTupqcit/AeWOUkX1Rh6eT/DrdnRia8MG180B9teeXmjGdyYwdGQ6LBPaE/YzavCPKtzxV1peVFPXDFWhraX3gIbv53b9r9om/l+yHtXfTjEU5MV1saEUgSx3ieQsDX3HQis96qZ35E//JRp78XXbPfg1uQ9usrsGl3id+xzQLaQ/FRwBoI/2glSDGfFBi9oQ2aVr6iSQHzVaZtXEJPr7zQiu//HWa0raqrDJUzJE5bJ2TnLK2Bp/4Cxt88Nu2Gd3zSEA88iuYdf1FO37vHWb//DFN1NNS54xU6cKIRuoKH7xkB8PP/16z7efY0Yr4RR/w63tqtQO7WbW1Roh8aiChfdBUzFKRL3l5f8kANmtPNnT0Xrvlfe8w+9IH1Wn7bLgx4/Y6xJpFSnPrYrjt15KCN+ovfaad/db/YHbu1Ta2bpuNjVesNDyi1VdFI/Ws9WnKKsl4k7Il+hzFe8uO9GoYVzRCkLHRS1wyPDIq943wzrOWX+kdQTc05FDMCxz8eMCdWv7ASTxpTlNlKEsd3oiqZbS8ICbD8tF5UqEmH+RohFaI3xXyKQLo8Nt5ryfD972ngq9Wm2jYZjnLztn9tucf3mXj7/sjP5Xk2RczJAbAaaTvulh29mvRzSOZs6627a/7eTvzed9n98+M2LH6kFjUUnBw0KaPztrAEK+rqS3Xif7xgFQZ/ABJTi2avBA43RiwI/URu/RlPyCHOFdT9HqVT0X5IX6DlxZUjscKdvCwfewjn7CJ8og9rD3ffu1B721st3sKO2xP/xabGNlkH1K+7dmniizjxUci5TSRhb2infEkO/M7vlf119uEjUhfQ8qR46kv0dOpcrrSrz514Fd5KRnjdWNrcpJpJIsCc2+5U/BI9Twzpfkl1pNu/ZLj7QmNyBsvudbs/KdKMSOuLAyVAZvvbqB606a59o2PWfnwfTY6O273f/NLtkXLzMI5Z6rMgBWYpXi0gHXxkw7t1epFLR3psA1bbPQpT7XKQw/Z2JFDVh8/ppluWCs9zXSa5fqGZTx9JW2f+M5nMn5iHsZi0AUtGY/3r7f113+X2cbTlYFRSx4fKfu1r2I/KVSO2f2f+5Rt1Bp1UINBTR3pK0OxhRGDWU3C/pxY96iWUd9fBGB20Gwx1bfB9hbW2xkv+j6z0dNtViM6y2ZfLrKmdUMW0bFHbOJz/9dGK0fdoGCFmYGZo3r2FTZyzXPUCEasRqQS/wcHcMeasuId37KZ2z5rpw1M2MO3fN0aDz9gQ1c+Xc4+qpL8aiPtjbLzUzmB9knoc91mK196uY1K7n133G/9TG/1WRtcJ960ZIeROIZPHujV9T90UIJ0VS7J4fpsX6Xftj7lRTby6h/XjmKLSrCjFqeqQ4B3/0wFXxeYmrXTLrjaTn/6C239U59nI9c93zY949W2+ZoX24brnmtbr3uebb1G6edcrBlYM7MGnnh009CmTXO7TIPBS3zs3m7jD9xvk4/ssyHprCibKZbU9/QJrMPqGsDtS/r6zzfU/jPinlqIgxjQ+VVxbNyqvpGYsU02aRcMzdit//ePbPr//LbWlverpPZ4nF5JCP9ytKYRP+qQ4dVLvKmyy7b8+K/YGc//fjs8dJodrw1oVlHH8CuHakV2M506WraDAbvyMVa/0czF9MvMlqGh5WJDzseI7Z+fY7nTv842n36uHdd0N1PvlyOvV7JmLPFSk4MzECAUstU0JVWpqLziwKgVVLeikf/YVMMGN+8yk3Fz8CMfTYrAKYGvgRRrgKjxoRkRY4kVAyPq8hnWDV48e11y8H6gstpHVSRvX0lONXHIzt0wa1M3ftpu/Y1fMDtwh3zzuPZKM8YcgBZ5a6RPPFb7hmyqIccYPc36X/56u+ANb7Wjuj6o5Sgvt9iQlqX+dol4kYPzRTH2U5wWcrIszpQuelrKVzVLTq7fbVu+64ek841akjOzq4jClPaZiISLDmZvGA2t22UbrnmB2WXPNXvyd5hdpeurn2923Yt1/TwrX/N823btM60xpP5G925EIiCn4i2VMh4FRV6AH95lZ73w1VbbdKYdm2U1paJetqnGNYe4PfVgxcTSyJnRDMDrXUMy4KHygO+HitNjdsHAlO359HvtyLv+H7Mj35ZCZ21SUwk+ihJ5UMqcVOTDraUdcorzzb7nrXbeD/+yPbzubNsjOn2btDziTXe2DbIZZiL6x3Xv+xFdKfa32uWcCRw2pCAT89DAYwc22EVPeZaWZOtsWp05rjFgRvHMrGawSkmGzlDKwc2wX5NG3rTisQmzyemyDHXErn6GjElOiFPjQO4u/lOAHPjmiX+cB3OGx2TsybxxNvFbmhEvkoFfxLMCYIZmTasNVUUrjJKWjSXt5arTx2yDHbZt9/2z7f/tt5nd9DktNzWITUxaQQNdOVviT8lZ+jjo4S0RfrR7/QvtzJ/+ObOzn2RHNcBUyv3WYB/oa1kGJTkd/Ks+f6Un3oYpaLCp1kftspd8v2adS1V2SPvdNCj4zkA8eb9rYLCKZOSwpzTsz9hskNfWeD1NTsrLpHQwv6UrDGrLgIP1y7XEHxT4eRafoMDzNcD6oOrTmXi/9Hq74JnfYVPahmhST0pWM+7ppwBaXg5peSmGWVqh7uUsL7NL/0Q406jfIhUXQY+uKduYlLTxkqeYnZeWl4w8NFnUaM3Pe/wn/NUJm/3Sh7VEvMNGBvo0kLJZrtn6oaLtefBeG3vwftu4+xzr27xVRqIFqZoqa03HJ/743ZXvu6o4nzpr127bdsaZNnPkoO25/y7bMFCWwzGTyrfTUwXvAJeCtrUXOawl7KanaXTdKueV4bIE41frHPDhGN4elbZtsi2nnWbD2j9OlGUc63dYaes5Vtt4mlXW77Lq+tNsVkvUygYtU7ecbQ3tZabX7bSGrrdd8XTb8vxXmD3jO0VsvQaPYbGtBbboYxiVWe1HMTSe5B+42w5+/kO22XA8GTPWqv/5vAHvHI5e/QzJOiohGPXJYgWQGXN10mZv/4ZN3PBF2yD75DdpvITPmyUT+/fYnvsetC0bNdNu0wpBAx6fbWjIUXmbH5R5NoDxQmzrdlt/3nlW2X/I7nvgQRsdHbFyjZ8/+fsyAn2P0zL3MBioTxpDNrvjQhv6kf+fGtWsroHGX74W/5gLsvo+leUGg53P5DyS0N5L7ZJV9tfT5CmsQOCHgVjLStbqviXSkjcRSv2HEqpalvtLAlpRFEg7favt+/JnbVNtXOMSz/j0P2TV7Fogv7ws/ep1g79aZ+ki6VI3pQ5tOouYa8Yw2kwQ8Ba3QrkYow+nZZKC51k+IgOKOz30ov2SZqINT5KRnCvHk1NIXcnWUaryiyhuZtwKX/+41fbeJePmk2osIRv+6YfRoWEbP7jPJu+60dZvkqFtOks2oVjVapwwejvIAV9SNjPftjNs9MJLbfrIMTu6Z49tHBjQjDirvqrbjIppQE9TGKehGgiOVks2fPppVr5QsyWGKHq8MeOHMFAWeST1J++nX2R2xbNs+Fkvt5Fnv9IGrn+FDT77VTb8nFfZ4HO+1wZ0PaR48NkKyqPMyLPkbFom2VlXaKjfIoJDVpLRlNUpzKlVydHQ1F9iBqsfs6mvfcYOfvsLtp3DmUk5nthEXbx1UTntPFt3zbMkK0st7W9EA3554ca//jVz3Grf/pLZvTfbcEO6ZABhltSSkM/jlceP2EM3ftW28Pm/s88WKxo8NNBQlYkATfoVBz58UXndFhu+7BpbJ73ec+fdNlKs2ADLFM08bA+K0kxR/cQD60nNhg9raXn6T/+iBr/LpTQ5tvrEfSzrIl9cqB/8B0jMVpxe8qhFA607JdOn/zgOJ6aCvATHhAh1ePUPZplBxSdZMF3FbnRf8lFdvPfVbOvWEXv43/7FhjVQlIbVgzx/8rrLB+RdjnTr8mRJzQCnJzhdOkiR0hAq7ywd4jlO5xqjFdbzGCfy8ngWJWaAnL+sJ6dR6lGNPJsv1Sx31lWyCpZVUWxWDsZsKw+ojlv1ix/SCH+vDBFvEG/ZYNsn5vtrFasc2WPH7rpLjqfRkxlJswLPvfjzWi4H1aCNRzGard9g66+6yoYmJuyWm2+x4XKfDQ4rT0ZDPzob6emzlYZGbc/4pG257mninRlIe4PCgIuCqnjzKh2siCny+AhSM6zPwoa2EOlRTgMFdTXqFxi9JVeV3w/6CauMhQGEt+qrmlU+8Be2buIRG5Ve0sN0MSInOKYZ2c682NZd9Wzdb5TB+VCQllZizT+VWxON275iM3d9Q07Hklk9KAdv8D6q+nxEjt2nZd1dt91i2/nO5FlnaPUgcxVP/qZ/ppuaZGW/56vJoXXWd/FVtk2zzX333mWNqSkbamiG8xVG+khQadNWe0iT9Hkv+n4rPEX7spHdcubBtI2AhmzGf9I1PSWZxctdN5jtuVPb9T1mh+5VeDDdH3zIbL/uD9yjcJ/utac/QFA+n904orz9D2gEEt0RXm2r2/j0hPXzKEaKashp3RF5S+L0HTaw72E79MiDNsAAFL+GWAky+82iZpwH4vqEtqDT5Wu2UZvjdACm6Rs2zVoi9rGOpxWCR/LwwSF1RNVmtKY+1rfOtlz+dHe6mvYy6ibcTArgK8K64K352TGb/sIHrHz0YfmMCGlZQRYHchwoDA3yqQTt9ybH7fCNX7f12s/YOWelJzaaPXXHykYBAxU/faIBca0ny5deoRFvuz18x+1WP35YTqxBQnR5aYTVmpdTteMTVdssg7adGv3lIJVav380d53o+qpPPJZZC2KY3hjXSwv+n5yoxpKuT0spDU6cH/E8nZnYR/Fvf87fPdxZGtcEMOV+zsFcbWDQ9mkA6z/jUlt3JTMdex05oeqiLByvzNeja4dt5rYv29jdX9espPtMJw0sH7VoMByQzNt0f/c3viTnPmKlC88RDTmFNr7sNb2PtZTz5a6W71Upoqg9beHMC2zr7nPtnrsf1GA2lT7xN6j9nfp7z8Fp69/9ZFv3Q7+glYZWA9r7+m5RJNzEfE+hpWlJg8L+W+3Gd/+WHfjo39jkp95nU5/9vzb+jY/aoS990I598R9s4ov/aFP/+o82+a8fsLF/Vdq/fsyOfFFBW5ADn/lbO3bjl+3grbfalidfo5WDdnmDI75a4DSbj1v5L9QHRv2+vHu33f2tr1r/zGEtsVlJKG8lkEi5qBnngcg+gHVyOkZAV0YebdTmOB2XBNHjD39Ql5HMW8lQZaRhvyXjYvN9SBvrHU+6zuyMy7Q8V8dBwP/H6lXRlxOTNvOlj1ph7BHRkoGgQO0xCryJ4g+dZQAqNqC1/1DlqO299w5ve/iiS9WxfZqJ+jWxNnzmFQcKmkel3JK/UT9sxdPPtM1nnGF777wr8cxSi1mPB2+zNSv3q3NkcA/dfa9tvuxK1S8pbUTLKJVREXRV4hABxjXCs5eFdWbtpcRp36vBTg43LWZxFL7343+b4Phejfb32PH3/bGNjGvwmTnizygZpZCLL4kd1VKt/6zLNNM9U0Q1Yvib+uoH/z2LLmtjkueITd72VZu850YblZwlViT+0F1y029ST1HMFGYnbLOqP3jPbVbY94gNXqaViAY2/xCTZj323NXqjGINouX0hbHyoGbrbbtsh/Z5swf22gMP3m19I0M2rjYOFrbaed/3E2aXXS+db1EPSD6XOanN92F8JGrmYdvz9++26rf/zc5rjNvWyhFbVziu5euYwoRtsEnbUJ+00fqUjWjQGdX1+saYbWhM2jp+IzmoQVVL5OrxMRvRjF26/ArxxxKX/uYkQEtdzgqkr6JsrzCy3jZLRw99+4u2Sd3ptgVPy4UL1IyacR4SewGnc+OhSA5t1Do5HUbkhPUfA6jfOBkcJb0Bzif0ONk6VqnL6bSfO0drfC2t6jJaflxY0DLHKbPUqk3Y2Nc+aWN777H+fpaLMiLNdlosuNH5MrAiZcliyjKwAY2w+2++2Qb3H7DyxVJ6TU4ig2Fv19CSraSNeMWXXqzxFfPq2ZbTbNMlV9lD99xn+/fst+2DGgDYqHP6p7VKZXbSpiePW2PPXhuSg/pHTzUj+azGCatGTQ6GfK0e/8Xstcg4rWu1LmLTLx2UtGwu88pb/YjZ3lut8r4/sqPs5YbU0oyWYVRBRRK/rD6bUvvTZ1xsm5+imU570bQGF00VSGetmnlknIU7b7bjCiPSW7k4pOZkgOgQQ5RhYnjQbagPRsol2//QwzZ2zwO24ayzzTaM+irGP3mgvmEJXJb8vvyk47HYTRts4NKLbFpGv++BR+xItd+Gr3+pbfme18vh1vs7rzgAgwpPToiLLC+YyW/5vD3yob+1zZph1/OWM18YY0TSCFOVminPgWScjyUFKFJ9PzvQAMLbPkPi6d6HH7JtT5ZtaStRMI0gmIv2if3ivawBil/NM0iWdu20wj1324GH7pW8ReWp3eUCflpRM85DbKhtF/rkOR0BPRVZ16vPk8MJMoKC9gk+1WMpos9LuRvOvcTstPOkUcY/ra1r2qtoxjJ/706GMn3IBm76rFX33atxdtaGtGTghJUX0Vn9uePhQ/5Bf80U01XbNFSy4488pGX+PbbhMi1n6JjKtHgSPRkydEp8pp3TQPY2bMTXD2lJcoXtqEzZkVtusUEZfWEUp6xqT1Cz0aE+O7LnIZu84w4b5dfTOznhEwMcb1flBBg4X2D2oHaWHIumRncOO0oNbe5Jn9Ts/pkP2CMfeJdN3fU1294nGThYGuDED+EFlK0uq0qn686/xAYuuoxEWadoIp+Wvv5RXWa6yX1Wvv2bNnnXTTak2YVPlEPFT4pRn3SJMTe0dHTVcoIoOpPHjtnhe++2zbu2aa80qD7ic4OaKfkGAh90Yj1ek9MQ6DutFNZfcrFt14pm+shxO+9Hf1z9w3pdddRxBemM17a0gNa4AH8Kxx6wY3/5Ozaw5zbbxh6T/uGwSH3DR4V4Y0926uICl17/4P/ZpY+/A+tHrTQ5YfzxkeNHD4kP2RdTuNpz/WoA9T/SQB/6l8zGbHj3Fqt9+2vWzzbBV0PLRI6XfJwH7OedrtD4mQ2NGt97lOP50oGlh0NFvXR2F8TjwhWhf+KWP27IEMaoEesI3frrkZpBOHxgWTRbGLaR87X2PkdLoqKWJ9DAWOhEDj20T+FtD9Oaflqd4Z9NGZFVBF+yc/ZWTDaMFbIDGxhRPfXQhJamxwY3W2nbObbj6d8pZ4O26kAER+OjOLwSgsNpT2SHtITjj7Lx8aKvfEHGpBmmelyDhCYO7cl5PYmnf9M2amP1UaXtttN4m+a8S+WAu+S060RTdLAK9LLUGNl5+XhM4dBhm9XSbg9vjBy6z7Y0jmmPNKb9Sb98smJ9o6M2oxEdZxnkQTcvEDDw7L7QBi7hkQGnoJqNYV7iemAWrckh7v6Wjd/6NS0v1Z5mfJqus7xTGfaQw8Ma+ioNH8MKvGI327BxrQjEgdU2nmlnokued3HUi+Ix0n6cTnqjDQZKlrYsixmIHrjPT0Jdz/7+J8yoj3ioyseGNaj5s8VjD9vxf36frWfWUxfj7/w4hK0Af/yEX7BjNPztA9eZ/4SJuhogVAYb4wByYFC0p6paSRVtT3HEznzBy81Gd6hd2qYcBVVhENsSrzU543oNw5/8gHT/sGY6ybBMeDcKsJeP84DVIiObVhGFv+Cz6ityOuC9q1ucTpcoHuVQTAk1BR+t0BKkNRuOVwdszLZo7unXwDaj5T0v0YqSOm5C+QNakG+tHZAPZnRUD1/huSiHdxV1Tv9QWbH2YtpnVadFSXqnLOWqGmEPVljOjFifhkzebhkdGbAp/3xfw4b6+q04Nq4V47D2UnzFij8IosqMiCwvs3Z4bcudW31SNm3OeYukUrBJ7Q2nNbpW+wf8q8vLBfriWzDMyPWJSRvSrLy5b1amPqGxQIYhmXzlIx5meEitvSoP7gf76laTkaGPceXP1AdtZnbQSuVRjV+SR6uLWX72IuPq02y6UQ4yNDPtfoHN+ls86jFskZ8x+aDo+1Vl0p76Al3ydsm0lDE+pb0jTwmn6xq3BjRwzthsic82aEWgTfyM9D/Qv94faNdFa5A/clKZVFfX3M94QVo7QK3ORVw8zmi25PsqNnlYy3opWX7AatBtCnMST7Nqqw8BsVicNpZQOLkbUgZ1PM8WqWYDciTp5bhsiPc2ec4+oK0Gk0BBeVXZZl2dSVf3K940or2f9o38RGq5WJ7TvXVjY3Z8nP4Vb4wgLDskAu//uSQJnZ2OAjgcMWt80mlCyqWYEvyPfFDY87KgsrOmThIjpcK4aqeRpqalzwxH6FLqsJTBoOl0BK+qa2ItKLM03n7gxVut4DlgYcRiBJV8/KVVRj5mTv5CKSeCAX6M6kfyzovKiEcfjNVuHMmzj+DUiyLUlS+kRBjSzDkpw+JNDz6UmlVeNPK/ZGCg4/CpLIMoi6+ylpm+hwWZ7DDBJw9YvieeMTpdKD9NBjJ2Ti81svNHOdBTRYbb4GRWuu3XUraEQwF04uvKHIIdlg1xk4mK4zVwGQ2Uyfir/pdyp/s0WKp/+6tltZm+5o3iSzX6D31qIFFZfoXBuc4AmzNhRksezsoY6DB2yvrhGyI3+aAdxQRnRMHzVIj+aTogaYALLJhykl/RBCetUsSgt8sv7JEltYue+1nyUnWFCHcIZ4s4D7g9YaabnZhYntPpEodTLaXnnQ7FcAONjItgxstwwXMvNVDU3ijSBT/wkFJRjDefq54uk1EB3rfka88wMqDFfYFNH05HRcnoB6EUlLFijOk7GdkombMvpuKaPAv/Cef0Db9i0vjuLF98cO3RNvSlMPjgMcZSkPM3B7NNR5BOaAme2g/hSYwygHScEsgB+PQ81XgWhpZ8qeegQlbO4VpOyfQdKxXvv5Tm3z3SZQlFkMWd0vzXRLrzD8K6IoiVwU+MhDqvpUk5Gnbd6QqaBaOu+5QuUZ3/eS8laAudCLr9iCh2o3JMVF4t2te1/wO5jKTH2JPk9s84yH5m/UAsGyxFf7YvvTeLo5cV/PEvUN2mSpeBqLumTsdmngs/J5FwPlOQRAiH6yBUTR3kryqpiDuDK5RRSrMHUxynmaRDkwqQmtM0FbhOTphm1FaTUd7JaqRmdpTaZeSSjyyWa9qH0lZNy1n+wCQHBQyQKC6RQR4NKD6dKKiNmqyD0zwc0Qt5wZyiuiHvdfnrIAFzuWQH96QHok3qe9Ct52eFlMWknLqKIRGj5TqD1wfkaBhSP/C+JAMT3xFJStM/krOqPS+vkYKSHJcf5nubqWqiFYGGOSFUHJ+S8F9FKIUTUy/D2yaqx2VVIw6rG/rY/66h02aVpAuRweFmICfglAyIvq0DEAUhFzQxbIG22dbAXjgz3cevXMjzVYUPFgqwRcFlIuouxemC9RVCLdFa0lq6blq/kF3CIAMWf5Cf53o4iv8N6xAaJ9NyiL+VBqP+96WZClyyrLKTR4uUT9e8AUM2M5vToj0nIDakbVeEeqvO8JZIqF1mA0hTMDkcVV0MBR8M1DkukjucWtGycqakuSPf4RBbKESDne5BxDmkQR2pZHG8yhYWRjpBty6n6yZleQzjrNVCCA/Kyuq5gCCulZ8GOB4ZYOxJjx5oMuQUREn6ot90wRFyk2gGChCUFA5PcBJKl1+lm6xcs6/wIgWvg89QxtMVMkRTqLuZ1x68Hv2r2TWT25Hl0Z9lthQC9E4lYHVFoI9dXpQJQglN6Mb3BdpjKLAkZHbyZz9yAvYHNYZXHMLnfBk/ivMZKKy6Fc0BaVkoalgs+ezlOeoh9SD3iv0Pyouu8+k9p/2alh+10pTSZjRr1fiaX3OWcxqU8+ZJVGfxtFVxWaO9L0EpAzyOSvOE0InrJX+vf1hV5Mc+ZbvDY6UKOB+DD3srVmhhzEkQ6LtUzQjj9EmFMhmQiZKZ6AlOQ9UUsPV+fqVR45uSM5rdZqwyUPGvc8HBgFY9LN+Z6SoadKY0DlR8JhLVjE7IwnLRf2nACat4LIghVgv0pfenew/2oCDGuGIZOqOpB7rVclnOJ45UlElxQMtDAjMTe/N0IokNJcdlNvSzkiYfKaIAZXxVC0SP5rEFrWdUKAunALneXgbQuRTIS6k+uCCohPSQbCZpwG/QjIISKOf7JIU5QEuEDEHLQZwFpxu0CVkdOpnyLFv9FXJ/jZyCaalBcKh8fAcRXrjHhv0aREGvRAYjKA/v5Zya/XzJAsh3XnThRtchBlnUZLR5z/IqORXc5MGglORpKSl0PIfPZvuqo2sPZKkMRsZ4hp79r542OySVccrQCnpAFTkgYebBP3wAcBpyOl3jKM0+cf6lRWT1NFYMfFKRrQFEcSn2liwh05U4EpH0rBB6DHQ+2KmuD7TKYKDxovCtGP7TM2DqIgP50E7FCPyDzKjL95DZ/ZxCpCli8GQ5zR6wJcvaAQmWDxTAbOIhDTfIhuLSej0pHSCbT/HqwLJGTkYxjNdtKiwlgtKTkjKaoRg3hrQcbdqQ9mus5asizH7LRz7l17QU5L27unqAzsyqOyNU65dV9TXXiULGR120OFXlDREvyDhBrHrekfCmcuwTeck7dZwqh4MsFKMAEcGoIyaPkOMkkw9ZmfFREid96UDCFwTEIqEoqU7BnSQjQhRlWTX6erhW1r0GIZ8t4D/NonWWryhSbVIRXUObA5L0/RUlwpDS4IG/DT+Q5fFrfHTlTgcBX+pX5EScmGqgEzG+TVKTfD4pwk9iyFcM9D/nHbwCmWjqWnxT1gFJBddHpkMHtBS50yqp7PxJLp6oZ1NfWfJyqlrk25/8yiCDs4pIxYqKsedce6j5FrCF5SOTxmNCO6SdoE+M/hQzAjc/xJqr1jxAyYDyo7rruC0EQgbfTGO0iqPeHPmy9puZeJd4wBh95lHvRDtzGSGkwQQTi7bni2O2a6ZnGoc2DEEpDVVCsxKImSHN0tllK2SgSuxhQj6Pm+UoQKPR1dk1+iVdTsfekdWB8yQQ+U9iIiFx4MAh/bP0mf64b9HOQJrynT76x1lI87L6BwbjPgs4EKsPtJoOcSibBwVplDzK4NRZMiGTKYKrJEYhAD0F+HWedZ0GQ89dVSQdtTDnOZ0fK3OgoA5Ibxd4GUfYw5zTS49VKBjPGXigKRPlmkhK8ysvkE4x88pOBp8pL4jm6Ss/KT3RjdnM4YXkRIIvc1SvSSMQ98FgJoeTDNrz1ms5W8cy86FDubDr9o5p2ruQdJOum8jyfVnagYFmHWeUMqS2cEK7GQ2fhQX/dEbkBTwvq+h5aWACRfTsuvZkGFORlM/g4VW9zjxQOcpn5BItQHqW6LYXNE6QOSvk7eSWwN6vrT7iNl/zBDJLRFSP5pr08hlc8/aH9qqFv+T0MoRbLjB2CeVBoI18aCLKKEglrhRCKpdGQ1dILj32XR3pN4VKedw3A8kcyHiYxymCUMDpp/bm7Pfa4fWy/PnKLAEYfbvDgUhvOUVbyIBB58ueUIcxiYSmclLwelhDpGXwH6LSASDfnidl6cDT8nR0H3vZrBwzOLNik3xkdQoeUTAL+XRunUZ20SSYB2kpL9h3ZHYTdpUn0ZHMGoAuORF5pnvoYRloGnkPJ2CO0/WUtIo4RaNqD48+dJ7peuihh1VDz+nWCr1VRA8Zek7XQw9rjJ7T9dDDGqOYjnjXDvnfkj0hgLgRngCgf09lHz8WzKv4mOCyhx4eR+gtL3voYY2xpk73hFta9rCmeKyYV2+m66GHNcaaOh3f1eyhh9XCY8W8ejNdDz2sMeY43al667qHHh6rYHKdd4LNMvI/0wKdn9OtkvP1DlLWAmEGEebByfoZAF2aC/mfzazlII5pnSrz6ihvJ9Vmab3ndD30sAJ0G1hiXMuPb533dCdhAOzhVAEr6A2kj2bMcbre7+l66GH10Tu97KGHNUbP6XroYY3Rc7oeelhj9Jyuhx7WGKv+e7r4fdUT6hldHCA+kQ4R6d9T0MenqNkVofecroce1hi95WUPPawxek7XQw9rjJ7T9dDDGqPndD30sMboOV0PPawx0h9cyU4weWM6zjL9PcyFniZ0y+9hiQhlnow4rtcC0d6pCqcY/qemuoW5fBYaP72uYTNTVqvwFzLlhfFHDBfxKCFIdft5w+MaJ+stcej4qLfE2P8SlGLvjYjXDo32X2g+ztDdD+RUXcBnSgr+l32LVvjL6UJh/9svaVSqs1ab5Q+zQ4K/upb+pli3/naCKZoXj/eH4ifP6KCDrpYW+9+SW0xnrUYMB4/3/j1JTlfE4RTvfvedhULjW59o1Oo10z9NfYbT8Uf+cML59B5YqNnHe6d0w6Lln0/JXeKia1/3XZxzteJGo6bL9IcXn5Bg+bgI+GxHeMp3FQoThw6gQTeOMBDiKNTDyrDagw6D4qmDbMb46/2PX6fr7gMLO127Lw1v2VkoHDywrxEJeccDpK+20awUp5a/MLaV8LAyp+luFKuLBn8n/nGMleo3nA4Qb912mvZ0+/c3LaZe18IyKxQFFzJqVjhPbGBwKzW6xS1PHr14rPN/MoANoIf540Kh5PHO7bsKhQMHD7tX4Vy1GksFFeOkRVjsLLLQWcJKR4pHP57ITifeeyOvjFx+Ms+em7hAF2f3O7duwumOumcxy+F0OEk4HWndnKbb4d1iHbeoYtBaalzS8mwx5R79MXKk08jHTOz9VtT9QnKtbrza6G6/adDFY06c32Sf8p/8/Wk7NuN0x0Q37eUW6yCrAfXjQoPFEyAuKtYg95iLZTeLkm914lMPXEn8KIileeOinI94x/ataaY7lU6H8h7beFT0/KlDIW1JThVOqfn4oJNdd0GsGLdv3y6nO3RwRU732HealSItxZcNzRiPWcjoEiJ+oqK7/C2n24nT7e853YpwspwmVv2PpbiHpIvuaJvpVuZ0K0XPaQOLMfIT44YfjS2+/MmNhUY5xT0siHC6Hds5SFnh8vJkAedbzMa5F8+NTzl6TrcozHG6gweWf5ASnX4yjm4Xe0T86Ivriyy3OnEPjw2E0+3athWn45EBb6LIifyVHjJ1s4i4wXCrmOc2HYfhbvHJwmLbW434CQ3sJR/3MB9aTseezh8Z1ORwvGfJ8e9i1vInKz5ZWGx7qxH30EN3tJaXW9mFh+GcijiuV4KF6K9F3EMPSwPDdQ899LCG6DldDz2sMXpO10MPa4ye0/XQwxrjUeB0HEisJPTQw2MLvZmuhx7WGF2dLj7hwA9bI+bHrvl7MDo6akNDQ543OztrpVLJBgcHbWBgwNPK5XKTVuRzzfML6lKuv7/fRkZGrK+vz+8J1COf8nleKENZygwPD3vblCUmPa6r1WqzLdIItMk9gCb50JyZmXEeaI9yeVlJhx7XUYeYQDr50K1UKs5TyEj5APchW9TnnjpRP35ITDo8Ujb4zbdN2mIRdJEJ2WiLe2jSRwTygjb6DN6JQ1eANELolbqhZ9qB9vT0dLNvoIteoyzpQYf2QhdRnhC6jvphW+gk5KY9ysErZdavX+/Xod+wvcinDdoDtB20KU8eQCeA+5CZMvQP7W7YsMHrUX8p+m+Hf66BBiK0g7RgAkNCUIDQwTTp//qv/2rf/OY3XcEwD3Nnn322PetZz7ItW7a4wNSdmpryOGjdd9999tnPftbpUQ+Bor0Qbnx83K699lp75jOf6R169OhR+8xnPuMxZcLACZRHSVzv3LnTvvu7v9smJiac3qc+9SnbunWrPeMZz3A+4R9MTk42O5m2brrpJrvhhhtsz549ng/OOussu+yyy+yiiy5yA6EOxgDPH/rQh7xzX/SiFzkvpIW8oTdok/a+973PY8pu27bNrwnIEfID+L/55pvttttusxe/+MXe4eRDC6D3MPSFQD51kPuOO+6wf/mXf7E777zT6cMf6RdeeKE96UlPss2bN7v8H/7wh113L3jBC7xuOA204CFkQX9///d/77x8z/d8j42NjTlN9PKlL33Jvv3tb7sOqUf9Xbt22QUXXGCXXHKJOwm6wW7QN/WgF85EG/Qb9UKn6OAlL3mJ8wk+97nPuUzQh8dohxj+iLEF2nv2s5/tdbiHT+wHWbFP+payBNqnD6I+9/Qt7X/0ox+1Bx980G3xqU99qvNFucUAWiB7OL4wwgiCOJVRFkA5Bw8etN/5nd9xw+MehgibNm2yG2+80X7t137NvvjFLzrjOFwIBA066+DBw+qgL4taUca8zuNisSwhGQQwGEZ+FIkjFjSCDUtxVTnq50Vvxke0UHTwGvzlO4AO/epXv+pOTrukhxHDN3WPHDlif/zHf2x/8id/4p2IHNdff71deumlduDAAc/7x3/8R6ePk9Jp0EbOBx54wNsgj4DRQJ8Aj7SHAX7lK1+xL3zhC3bLLbc0OxN+Qr9Bg/oPPfSQff3rX3c5KIfBwCf5EXcD9eiLT3/60/abv/mbzufTn/50e+ELX2jnnXeeG/b/+T//x/sPHilL/P73v98eeeQRvyaEzuAP3SDTrbfe6sa7ceNG5wd5SP/zP/9ze+973+vGffXVV7sezzjjDDdY0r/1rW+5TOgQWsgavFL//vvvd5tBvnCm6FtiBjzq33333fa1r32tqQ94jHKRFjyDPP84+7/92795HDonP2yTa0A75NN3lKWv77nnnmb+ctD1FfE8IzSOIDgPoxlKfc973uPxW9/6Vu9EylMGQ967d6/91V/9lY+GjGzXXHONdzKzFR0VhsYI9spXvtJHQhRKJ+SVRjkUhZGHAZPPSHzdddd6Hvd0GnkErqFDHToD5QVf0OM6RjXy4ekP/uAPXLZ/9+/+ne3evdvlxSmZ2WiDDiYNQAOjIR9jo0zoiTaCb9rlGtBhO3bs8DaZwZ7//Oc3ZYkyXFMPGoD6XMMrebSPbATodAM8YsQf/OAHpavr7A1veIPTCF1ChwEA/rlGD695zWt8NqTOT/zET7iMtB18hmzk0+esQNAPYGBjloMGjg1Nyoa+v/GNb7huwfHjx+07vuM7mjqg3MaNG+zv/u79vmp605ve5OnIAF/U55rBkWv6l5nzR37kR7wPAenojHaDT/Kwq6hPPjaHPdIn0MMeg9foY5Dnmzrbt2/3MqGP5aBrr9EAiA6OhmCMUfjhhx+2H/iBH7Arr7zcHYrOZNmHw2GMP/VTP+WG9oEPfMA7BoERHOPOA/oIDSjHqA4tAgoLo6RdAjQwBvKgRadEDB3y4AN+Q3F0Eu0jE2WCFveMYozsGCUdiSzwQTlmOeg9+clP9iUmvNEWaXQobURHQB+QB8/cwyP5LKOuvPJKe+5zn+sjJzMZhg+9QNCBX5wN3smHzxiMAPSjTxYC5XE6yrPEQh50SjoxtFlessQC8ItMz3nOc7x/mU1ol/rUJTCAMgBB9zu/8zudN2QlxoExTGbTY8eO+YCMLg8fPuxtYugsaXE4ZI1+hj73k5NTXo80dEAa1+iba+jRFnwA+higY/LCOaEXthD2hFzolDTu2Spgo5///Oe9HwA6DVuDX+7hh6Uyg1bM6sgafbFUdO21aJgQSsfgEZJlEksvFLlv3wFXCkLRSWHMCPeKV7zC9u/f76MKQiI86XQwgTZCYQiMwgB50COfwD3thxOi3OgA8mkTUJ972iGNfHiJzoA/0gC0wMc//nF3KpZDKJkylKccNOCXuqShi1A+IQYQ2suXIXCNzMiPU7O/wMiRl6Uu8gWQD3mIaQ+euYZGGAL0wxnJ7wbkwMExKmYY2qUuvIZeQr4wbNrA6TBKlpm0DR3ymBFxItLZG19xxRVN3RDjTOzhQ3+Atgk4D3Uphw2xWgLwE/1JQL6YeUP+4JeYPOjTJnSoC2/E0CI96pKO09EHpGNjpGMfDDRPe9rTfG+IXsJeqEM+oB0GEnjH6eA/8ii3HHR1uhAIwCzCoJR9+/Z5Z+J0lIERFMDIg2AolE5ECXQ2oyNrYWhh6NyHcNBFuLwBkkdbKAz6odC8wYUDRz50QCglykUblAlwT2eyZLjrrrvcENgcx8hJHvWpE45LpwGukS1mQvLhP/KCH0LwyXKJttjbELOUZomJrvIy5HmNa/LhB9oAvkK2boAG/YLRMLPG4Q/OgfHSDnnQ4h5ZkJN6z3ve83w2Y8YjHV6IMVKMj6UhOgg+yYMOtkF+0EOn0ERuZOGagB2gd/qUeoSgQXrE8Bg6gfdoM+SDFnQJeb1E/xFTP/QGqAOP9DllmLmJCQHqoJsvf/nLvkLhcAnalKF+0FoqFuV0dDiNhXIASy7uGS0YJQAK4mQJZkjDEFESDsaSg86AWRQbRkQcncY1Rkg+gfToBAL3tIFyucfwoREhaFEHHihLep4W6SgS3kmD1r333uu8hlFAhzhowhv3ABrohDzkAuTRFrxDh/swDNqhc5nlmeFwANLoxNtvv90NEp4oS4BOnlfo0H60w3Verm6gLLMrcrDfggY8MPNAGxqk0VbM2NQJHqnLaSZ5pMEvJ6DMhBhh9Cd1oHnxxRf74Rp7IO6hi16RCUeHb5yRdELYSNBBPu6JgzeucVCuwxYIIT/XeTslPXgKXVKf9qCB3PBGm6eddprzzAEX7RKgRaAeqxEmC7YE9G+ActBaDha1vIQ4TMBkMITyYQrmuY8yMBNKAJHGqRjGjiIAdFFmsU/KqaXHDxjDpk0bNDMM2kB/2UZHhqxe03Kqj2Uos0ZaZkG/USzY8LpRjdzD3nHr1q3zJR+GQXsgOpa2CKQT004YHEAW5MCJ4C94hz/kCp6hRccSUzc5Fvx4rvgQv7rmdLW/f7CZvn//QS0t99nll1/p95zEXnDBReKnYXfeebf44dkVfPKcq1WPuFTC+VniMwMhF13GyJ3SuwHezzzzTHeSj33sY/aud73Ll7nIhh5os1plGYvjpTaGh1mlsMIo2cte9l0aYA/J0T6rAXWjnxofPcoByHd6WfSATmiHfuFQ5dxzz/VTSg5a6HP0ieOhY8rRB5QF3pe6R8/E0IK3yAt7Ij/yoBf9Cs/oj7hc5gALZ+XxAysinDjpiXtkjb4JXUIHnnEuTnbRCe2QTuB0lmU2MoVdwQMhZFgqFjU/0hiMICyNwUwwEAZKPgFG6AjKhRJZXqFwZj/SyEf50KE8eR/5yEf8OJ7nWH/zN39jf/u3f+sjLKAOnRcOTqAT/umf/sn+5E/+zPdj1KGjOSllRMo730KgLPRw3OjkGH0Xo1RkyOsAhDOjA3hmRmNZx9ISR2XU5RkmgwTP4phFom1oIW/oCXC9XKBn9M++mmdpHOb8p//0n/zUmdkIfnheGDqjLdKQizRmZw5FOML/4he/4kvLV7/61b6iYRCmX9AXTsWMTlscRvGo5a//+q/tv//3/+79wkEEtGkrBryQbyVgr/xbv/Vb9o53vMP+8A//0E+guebxCHlgIf0hA4PS6aef7g5GWeSBNw4JecyB/OiR/sXuQj+LsY9OWNTyMoyKOIwDZrmmcRgkzjMSZQlcwyz1QgF5RZDOKMODTp79YKRM6ewZo43gg3YpDz2uKcfxNvVYJjKKs4whD+V1Q9CMNpAF+iiYtIU6DFAnyoTs1IMGMbQ5GWW0xOCgi3GzxOIkFJnzy92QE1rd2l4McAaMB5o43dvf/nbfi7E9+L3f+z37H//jf9gnP/lJ5wdeGSQY7XE4dMGA8F3f9V1udH/xF3/hKxaMkGN2BhWcjEGE8tTH8Viy/dAP/ZD9x//4H11Gnsv97u/+rv3v//2/3elZlUAP410p0BvtoVuWuwSucSL6v12H6CEfk8/BDwcq8Hbo0KFmHi9IQP+qq65y2UinPIG+jf5eKro6HYRRLiHuaRSjh4m8wcbshsFRLtJQLp0Sa/kYMcIwKfPGN77Rfu7nfs7+/b//9/Yrv/Ir9rM/+7M+YkIP0LnQCYVAj00wZX/6p3/afvmXf9l+8Rd/0d785jf7Iwr4omO7AcOifcrTFtfUI30xQE7kCFmJuSdwz/4GQ6TjMALkDh1w+sdJKaNp3kDIB5RZKXAOls20Cx/Mtjzi+YVf+AUPGOif/dmf+T4tBjICQDZ0jlHyTJFjf97ggFcck/wYnDBO+ggalMNZmc1/9Ed/1P7Lf/kv9vrXv9554UUKnDAewq8UzLg4+Gtf+1r7/u//fg9ckxZvriwE+Ecv7F/pcx6YIwsDN9fM2MiErKRjd8TRV8tBV6cLgwI0jCER6EgYjs03HUU+iPJhPMQoHKdDgDDuYBzDo8NoC3oYPTSohyJoh7Lk0+GkMbryvCff8eEs3NMOhtEN1McAUDz1wlmoz3U74KvdWEIO6gHoBA2Wj+wZedWNZQ9LrXe+851u6Dy7hF+MMHQbsnLd3s5ygF6hjbEQoyMGAsDjkbe85S0e/8M//IM7C21SDt4BMx91OTBjOcwMQj6GF3JyTf/TJ8jDUjpkoS1shZPQX/qlX/IBkccNcdC2UtAG7RLHgME1dkScRyd9YjfUwZZ49MVJJWmsmliBMKuHjZOO7MRguf3T1emiAQyQEA3TATCLw4SBYCzko3iuKQcQPp76k0cZaFGHwHIjlhrQAlE32ocGdUnnmg6j46AT6dAixmCgx2jVDfCJwlEw9GifNjFOjCmPvJKpF/IS8u0D6kKD5SOzCY4NTzFbE9MWb3TwyAI9YuChN8qELlYCaEETHQU92uEah0CP3/d93+d6ZHmFk8I79aKfQp84H3TgDd2SDm3SAfUohy7pI8rhcNzT/+ggZnf2W6GrlQB+4JF2w+G4DruIfsoDvgLogXLU5eUBeOWNGgLPbbFNysArtJGZsmGXy8GinC7PZCiZEQtj5cEhQoaBhoAhLJ3KqIFBs6+BVpThmrrE0CQdAblHyAjBAwGBUSh0yaPdoINSSIMG6exnugFa8RyRzT5twQfpcb0Q+IJauYzTMapiRPBLZxZs7949cqg77EUveoH95E++2d72tp/R8vfHPLzpTW/Ucvjn7JnPfLo6+rjdd989Xoe6hQK6Y//LTJtozh8WRhglukEW9IMOQZoFmd3Z//ZpRJ9QKrMiLx/w/Iu+4TCMFQTH7RznU5fDL/Se+pg2MMboI67DIVlBoMdYdcTqBCek/ZUCXjntRnfwB58E0mZmmKHm6gue8/fohcBsiR2wB+UwjrOBeL0t7wPhgHG/HHR1OhpAeTEiAJhk88oozYNTlk/BRBg+oA7KjofAPA+BFkZAHteMuhgBMfWYHYgRNIyewDW04QGHQxlcU5580igDXdKgGSPwQqAM+xyWT4xuQReDoCO6IXiFh0B0DAdB8MGzLoyM2YFyjKbogJe9ebufsgxMlEWPoec8zeUC+VjWh35xBujTFgF5ATpjJUJ+gGt0DojhB/4I8A89rpEXOtCjv7mnXfLCAWPpySwHLQbtxfRPN8TsCm/QDT5Ji4lgIQT/UZY9K4d6nGiec845Tgs+w7bygzF1l4NFzXQ0BMLAUCJLk5e//OWex3E9wtJpGCtliJmaeRODN9g5MUPRofxgmvUyZcPgoE1eGC5p3AMcCwXQJp1Lm7RF4BpEWeoToh1o0QbKpWzwAKjPy7mMyrwhHzRpA95oD8OlDu8ikgc9AjRCL0GPDmdfw/4Ah2Z0hwayYdzEAD6gy1v4PJzlHp6jkwG0kRtdMhvDP7SRibRYklKetHwd+EAG3kThxQTqQx85kIs86rPfpPz5558/Z9kYcoWuaBv+84gyAFoMsKxqOMSgrZCVVQeDDcfyHLDESS75IW9cR3+RRuCasrTPdeTBE3oNWdAlOiGmLGkE6OZtDkAPGqRjg5Sn/5nteF+YgyNkhR/oUx5Ag8A9dZeDrk6XRzQIsyiYTuR0iuN6TqXoPE7iUDpH+fzCgGNiHszidDCKwgACwTSdzUyJYlAQhsI1+dFeKBmnQQEA46BTg14glAqCVzqGtgjQJ6YeZWmT9llW8GY8z644RuftDQyV43aMhWP/3/iN3/BnTxhxdCx6gCayQS86kpmLV6igizwgeIWf4I2YFQDyoEdoUp8y5CMjAxftc5oGX9zzhgtOjZFTBx7QGfqEDwwJ3rjmd4TIxLM5tgM8LqCf2Ev+/u//vv/shxeXMTh0TPsg+A09o6c4JIEuvHONsWLs8MBz1v/6X/+rD7TogMMZlmqsiOABGjy6gGdkxKjRJbS4D565BrQLP7QTNoGM8InOuUcfvMZFQCcMYKQRc0jFQEkf0g/QgU/6HRrUD7sjYF88PmAVB314Y6KApwD8EBazEuqERblqNBiKIYZ5jOLyyy/3n3/wpjYzXhgYAjDa8VCWtTGdhcKjHuUoA+JQJgSnQ2iTMrSF4olROm8YBO1wYvLySslfUwalxohFOwTawDDjGsNhacFm/5//+Z/9lJHORbnUJeY9U156ZemBwmmbNxxGR9erLL+coNOSUd58862iXfO3UHgTIgJvTCSeucbpCpoNORncLGfeY1df/WQZ3rDa5OWBsoz2qH34wx/1OsgV+uEe/nft2uEjMzNJ6C/KAmJmceTioASnpS8Ag+P27TvtrW99u5/S4RzoCB0fPz7enFF5YwVeNm3a4nyNj8crVRh/MlSWjeiIgQsH42E6Ly3AK/rDKdmScGjDkprnYaFfbCpWPPQl4BodEwN0Sl/RLoE6vFrIbzH/7u/+3vOwl2QjaZVDGvpghfXzP//zrmvkQr/Iw5sp0bfoDIeHD+hXM4fmnraYYEL3pHONfoiXikX9chzFE7hOBtNKg+EQECUxM1CGTiawjMCg6WCEoiyMUo/6NSv4jHLm6ek3VmzaQU1CA2ihhIJ3cnpVCcU+sn+fK1MLyCZPlAMhB/SjM0CcEKJAaMBHopnecUSJ0QHswVjbU47nVOQRcGA6gU5xvuREzByMiNFpGBMyk4/RwEerfBrF0QN8wh/8YPDk0wbtx0ABveCTuvADj5SlLocGGB/6JY08kG8TQwm985MrDJ50HHXnztM8n/rwAW/MCuEAoT+u2VLwyADAV9JzeqEb2tCMWYMBFh2icwZV5II+zsesHnwiD+0H4BNeoMGDbmJ4CGNHLvRDfWThFTDKkE7dsBeALNShbfII9BF2ygxM+shImvGoR78RE6iL3LSLHglcx0/WsG34DNvqBvgD2Z8/XtjpYCAqhDAgX5Z0BEG5gDooEyExnGAMOpRFAOqjuOnZqnfE1PiECz80lAQvqRzlKYcCiqIFmE1QTt9g2t+VxRrlQfCX5422w3BRFLS4hzfoUId2CbSHQjEKrnE20kHwTFvRASktLVEpH8ZKGepRhvZoGz5CHgLtUi7axwDQGTTRC3kEeCSfGFrQCDrEDFLogfwANKAb+gCkAfhDdkD+5GQa6eGRmMECvsmLckEfg8VhSacsMnH6CX/cUydkoh3aRDfUCV1QN+RA3rxtQIcQS/c44YQ37ikDKEO9VDcNmFxDFz5A6DFsgbrIFrzDY+gv+oT6tBP1oEugb4LnaAfeuYfOYhC8L8rpIEojocz2RmjcnSYb+Qih4GiImBCMhlDQtFKa8fjTT6lONppl9WmPeoPqPJSNkfuMUklG0qgmpwhQJy8H19EmHRH8okzaD54xjljO0CZlaY+6wQflqE/HISMys8SCDmnUoVMoQz73+XTKEaAHgnbkR3rwFfkB+EGOuCaPo35AfYwbOWg76SrpGN5D51zDN0gGmMqSDj3SCKShI3QNf0ED2aJs0kf6GRflKYf+oE8IY4UedJhdAPzBL/Whid65pjzgnjqhw5AhbCrapgwvMwff8Bv6gc9wfHhBL1EHeiDFadaEH5wSWqRzT3vwAJAv+MgH6C8G0AWLcjoYCsGjIqBsCMs1ZbiOe4RG0TAWSy0ERzGURRgXWk6HUtYNx2Y6GQBOB/waYUU3dWDaDB8bH3Olah06h692kAcvKBFZwsCISSMfuqFkeCOmTCgcnrlntIcXQtRheekzrmgTIh8eQVxTHoTeIp17AD/c0w4gnWvqkQ5f3BPy9dBX8AtdeEBe0pgxwlhCDzEjhi7Yr9EvebnzfUeZ4Jc2qUc6QC84PXnUjXagxzUhaBGjT0Isy6JO2AL30Ked4CXaJB8eQ0byCQx6pAHuuQ4ZkBVd5Z0ROsgUfYvTcU8d6sJXtEOIPPggj3uuo0+IF4PgcVEfJkIQGglhaCSuCSAUSnqMoggMgzF7QIN7GIdG1AV0CvlhWFznOxdatME9/ECH2QoFBkIo6uYDNGPE455y1CcNWtFmOBb5GA1tRj738A0P1A0jgB5lKEu9aIN79BD8Rh4I2SlHzD200B/30A+DIJ20CKSTD69hPNCBN0B+tAvP6B5QhnaiHPXgC1CHQSNPB2elDcpBC/6J6adIB8F7lPNBUEBPBNqAfshOOXiibMhGm5SjfdoJvRAA5QJhE/m6lKc+tOEn6JBPOrKBKEeATrTFPXSiX4JveCamHHnQJy3KUZfr5aDrTPe4R6HVqctCo+u41UMP7rxgUTNdDz30cHLRczpmqpWEHnpYInpW00MPa4ye0/XQwxqj53Q99LDG6DldDz2sMXpO10MPa4yuThcPPuPBIyEeLvKQkteIeNM+vY7Dg/P0alF6pgn5FHh9izRC/nuEXkr0AbTjgWQ8EG9/dhgPK8nnYWU8KI1yXEODAJ/dQL1on4ekIVeSLdUnjUDZyOOaNPRDOe4JXMfD1bxcwWOEuEdf/BohdCfSHqOv0Bv6IkZf+W82EqINdEKbwSsBPoJXArxRPv/gvhugSz8QR514oM099EHkRXt5RPuUAdCCvzxv8wVo8oCda8rTNuAaenl5CZEGoj73+fIhA/mkoTfyySONGB65phx5eVA+3rpZDhb17iXEaYhOg5l4I4KGMQTKwBzplCNwj7IoRz60iePJfutNhdYrU1GO9BCctOh0HC1+R0caLzDTJmUAafAYDkke991AfcrTGbTf/voUIKZctEU58uGFtx6Qh/rQiTdw4IfyXFOf8vATNCmbBp/WGxkErgmUTWWSkXIdg1HQTZ9aSF8SazewoAGoE/LQbzhe6GkhRLt54wv+AG+pID956IB2KR/58A6iDoF8yoasC4H8KEeAPvfICkLOAHkgysMXPFC+ZXMJ5FMePVCOPgekhawRIz+gPXQXskZ+NwRfi3r3MgDTNIwAGDOMcp++DpwUi0AYLDEM0dHQDMGDWdL4yUd66z+9ygR9ykGTe+rSoaSHgjFmOhhawQvthCIpRxvkkR71FgJGT7u0iWKgiWxcE0iPQLkoC8iPdx/RCcYM6DxoEOc7Jjo3/9qbutHbJB0H4Tr0lq+LTNAjHwRP6Rsmrc8URIi6xNSlTegCeA1aYQyLAfXD2KAHv/yygnvygjf0EdfwEDKgnxhIwlljUJgP0IZfZIBO9BH8UzdeliaAvDyk8fMl+Ameom+4J8Ze4B1a1IUn2uQ+BqagTTn4Dn3DU+R1Q/C1KKcjDQaIYZQYAjDPr3JZ9tAR3Ecnw1AIER3Ej05xMn5TRR6KTPSSY4RCQmBoUJ8ydBbK5T7aojy/eEYR1IE2v/jlGmUFnejw+cDSLeihRF7O5tfj1CMtZCKEfijHAIBcO3du9/dA4ZHfkMEv6cFX6AA6BH79TVqA9smnXsS0E4ZBWhgF9YMe+iHm93Toku+wYGDkkwfvABp8+yWMh37gR6RcUy/PSycED0GPGMPk04L89IbfS/JNT+iRTkwZ6kCb+vBOOtf8opvVSjgDMi2EaI9+pzz30CWN31PysStoR8jLwz0/qEV/1Ae0Bz/YSPAZ/YduoEk70KePqRt0uabvqRf9u1gEX4tyOgrDaDRAGTqQT6jxU/9Dh440lYpglI0QDUU6P4Dkh4n8eSU+FMuPP3E68hCQ8hgCQDBoYkAoBMfgOpTHpwD4QjHlCPws5Cd/8iebf9AEmqCbYtg/RWeiUHjiExN0Cul5uaFJDJ+tdof9AzZ8ro1flgMGiHA8aHBNHX7a8r/+1//y2YG6pPNLbIAOKROdGYZBGdolkE8A6AJ++RoW8c/8zM/439UDGAxp6Ipv9P/6r/+66xDaDHpve9vbfJCKQW4hwAf1aDd4hD6fL+fL2vzi/HWve53nMWBhGyAGMsrTBnqjb/n0OR9s4p4y8LkQqI/s0AhbgE4MNMgd/UIIxDWf2ODHt7RDeyD4gXbQJ59VGvZDX6JL9EebYR/IyH2Ae/IWA9oBi3r3EuYgTEzDkYbwMMsPV7kmRuEwggPQufFLYRQEDX5VzucC/vIv/9I/0U05RrxkfGlfQBvEwST0yOMexUAHmjgdyqNNlIMh8+tr8kmPDu+GTnJRDxoMBATaIMBvzNbUgS9mNz7d90d/9Ef2p3/6pz6KUwaZ4R16IRPgPmZJ2kFvXCMbbdLx0Eem+CRFjMTBD+UpRxptkUZdjAqeaJs4nIVyBK4ZkPh7BMhDWjdQh4Bu4AXatM81NGgTOYkpF06EvPAE4IN86kGH8oBZJXibL4TeuUbOSAs9QC8QvMY1wL7CFvhRMrqlHjTRIXqGJnbDloe+5I+sYJ8MwLRDOfKhgUxc59tdKhY10xHCaFA218SMdvxFGkYF/qA+AqBg8lAszAEcAgH4aAw/tKQ+nfCUpzzFvud7XuGGh/EiUHQknQcd4jCePH+0zQhGm5THyPh+Cd/goFzw20mmuUijHoH2maWgjfPw1arv/d7vdT4IlIEP+GFUZya66aYbfKlFRyIn3wFh5GfEjCVxGCA64YNNlOU60UzGgRzQx0D46BB/XwC5+DsCLAcx9kCUgy4rBXhhtoUGNGkToDs+QMSAQH0MiD6BLz5Fj3zRR/MBWekT+KfdkIUPUfGZDb4O/cM//MPOA7RoM2giY15+0pnpsQE+McEfIgknnQ/RL8RxHTLiQMgU5UJuEOWDT75rwweR2ttDPgJlmLlZGWCL8M5gx6f9mVHpW+iRnvSe7Crf5kKIcsx0pV/8xV/6Vb+bB6FIOo2KjBB0OIyyP6lUZv0PHPK1L4wJRVAGpfL9ic2bt/g1f2qID/+EIUADAdetW6/OP8uFoa0wRpSLcKRhTFwDFMHXuPiLPeThuPCGIeEEfBob2nnjWwjFYjJe+KId6PBVKToBx+Grv8iDXIyaBEZMZObzepdeeokvK/mSFx2B8fEdDZYogeCDdqgLjRhp+RvbGzakTxdyz/KbtvmSFbrA4fjOPuUJyE99ZkE+qMo1Mwbyog/6Bfm5RofMbOzpuIYWOiKNUZ09MPUWAvXQDXQpiwNCmxULAyl7OrYLUTb6jnIMhOgEudEB9eJvfDO7vPjFL/YBF93OF5AvZM6vqtAjtCME2vucPR3yUuelL32p0yKg61i1oHO+/sXelD5nwKUPoIWNIh/togPSIiwFUf43/+dv/Oeuy8swYBTPNYpEiSFstaH9hujxs32+DFWZmTKNbTY+dsxqfL9jasJmtO/g78xNTY5L0S+yV73qe2ymquXGyLAvdZgt2VvFSSIoFDWqadVHXKunH3bCB88A+dIWMwS/Gn7mM5+lUYwRu+FfzuLvvfF1rvh7ZMxkC4X0N+9YQnMK2fqVdCw5aJMZB8dG6cjJl4PZSxGgcfHFl9pP/dRbZBibXIb77nvAvvjFf5PhpeVbUybRhhZ0IiZAj291oL/JmUnptGqlfs300q0WMv73+6YqU9IZp44pTEyMNWnQJ4D+iAEqDIPT5Xi2d9ppu+1FL3qxp91++51aeXxVRrfR7+M5ITrjHoeHXqKBjtjz8ogBWTgk4RBCM1m5z8NsXXtzqRuHA/CFzXCfeEn9O1Ot+ceo+GIA16Hf0EV7CL0TIyd9QvlYReQD/EK7zlfWGEwVV+WPkTalCSLagw40uY5BG/rnn3+hZu43auVwnvOMbX7iE5/yvkR+6uEHtAet5SBpaJFAqMUjGVp85hqh6AAE5FuQjLooc2xs3O6++x7PQyDicHQQozaBmZRRi+8Yzs5WNULt8lGKj6RiaNRlZKJsGPpqA+XTWYy8fKA02uWUjg4No+sET+dHtMv5Ie0i68VyHV0TszJgVOea75QeOHBQem19lTlmfGZErucHMnWWaz7MsZ9G1F2SCZ6ApdnkiUBGbA35oYXcfByYlRt9STrnByyJGYi4R3fYGHpaDlYmcQeggrwaYM4VIwPjOyesjVkaMWUjQKUyo1H3NjfAMFi/1ragYBpRFPjdGorB6Tj54iOupVJRznaep+F0LBcwcNblLA9iZl5t0GaMuuwpWbLQNjzSUfAwn9OtBUKv8EG8bt2QZrsXOY98YpA/kcXSlsEwZndClH+8AxlxImyUAQo7oz/Zq/LxXfoXPbFliL6kLPXIWw5OutN1AqMso0KMSsxWPD7AURCUxw8IixDR0WGoGEAIRxpOhYGwv8HZ2EOx3mfPSF0OVzB4nDHaW03QBvwxQmK0dFSMnJzWkjdf5yzHGZcqU+g+jOX48Ukf8DAq+GLviFHRD5SBd3TOPXUf70Be9IPjAa7jUI8DQmyKMuxfAffMeOSjp+Vg1Z2OETU6HMEwTIRgM4tjkI8TMQNyjSCUjZGWa+ojKEaA0yEwG2MOMqDJPUvWmPb51Dh1lquUpQAe6Sh4Ry4OjXBA+OawAt7mAzyuNsJI0B36gFeu+Yw6Axd6jy8xk47euUaWhXh/PCFOdVleI3/cY2PE2CmrFpDvM/p4OVh1reIIGGQ4EDGCReeGozClYxyAMnQ4cTgsNDhRYnkJOJpHKdDBaTn+RlnUY1+HMQW91QT85Q0UHugk5An+QXTQSh1tqfVjUENP6AM+cC6O0FkOwyP7T/7ULysG6JNGGWa7xzvoN2RFbnREX4bdoDv0RTorKspGGXTE9XJw0pyO0yFCQIsuD7NVOZtiGITp2bpmPAWYRiBO6BpFGZL2aJzUcR1pBE7ECOTfdOstNj416V93vuRJl6VyDY7JOcjYpCXm2aLLUmCflkz3yWg4CGC5ulBYGeCRkztOGLmenq1YeaDfeS72lT1GLkLIkg+rDXTOoIb+CczCDHYMCvx11DPOOstmNPB94p8+ZUfHjjflQd9xMt0xiPbKtXfqgSz0WaEs/WQnxtxHf6EP+habC/skpjzxcrDqMx2jPrNZjPR0OA5HYDpnVCEwqpKGkVCWENcxuvNXTblnL8g7d9THoEDs8RiF2DMy260FaB+ZgkcOcTDsWDqHLO3olLYaQPcs6ZntuIYveEZ3LIX5i0rwyYkwz964pi9ihnwigP7D1pCXmBADFEBfbIewzxjAYpW2HCzJ6eZb2sAExo4hhRNwDfMwFssUnAvBYJj3ENPehxeaB+RIPNNCqPSbMZ4HxXM2/srKnj17be9eDlwq2uCeJyfjA7Jpo09bLBH4k1M4JIbFX6iBp+ANXgKxhArlUp80AvzHNXmUy+cTuI80jBMZkY2YtT9ptMlDa9KiHWgFveBjtUHb8AJPwTvgnj0nLxPw+Abwt+M41ALwma9DQAbkJC902w2UDR6CVtBA10EbRJwHdUkPu6IuadCKvloIUZ968JCvE3QjIE/wCX+8VhjLbLYyUQ+a2C76WQ6W5HSdlAKTMAhjOBPCwRxlo2MYVeNYnUA59makIRQnj6EcaBEoBy3qk8ffUouRiBkt8rmPNng7gjdFqI/x0waKoRy0o31ATDmUnQf0SIf34J80EPwB0qAL/2GcDCTMxlzDR7w1ASKO9kGknQpE2/DOMpO9KCd0/Ikr0pCbMmGEIHQCyM/LMh8oE7QAOgtb4HkYbWEP+TgfADH7Tfo/DdRpQCe9mw6DZ0LM4AHq0iZ9GnaEHkhntcSBHO1Sh/6MmT/qhS6WihUvL2m8MpuEQiHxxgFKhTkCwmzaus0mptMsh4P8y2c+6+kY/UXnX2AF7fPK2hP2l7TcVMwTOqtps6q4Lvq33XyLzar+htF1dvEFF/o1+cyGvDnADMmMyNshXI+NTUhpd0iZ+V+pI24K1EkzaTKMPK/EkQ7COSM/+GZzPTI4ZLWKlnD9A/aRD37I5SCcd/Y5tmv7DueRe/5ACrJE4J5wqjElGa664nK79uprXO9f/PwX7L6777G+orgU3/BKnxC4jz5Bi422AWs+oCt0SYjBCvDOKrMJgyNxp8DMy2MgXh1kBcNSGWBvDHjdEM4ZAyl16MfgYWZyyvsO2bnG1jauW2+f+OjH3MYmx8bt/HPO9T/lFv1Wnak0r5eDFTkdG03UjxEiGA4VwoRxEgAjB6MG4GVef8KvOheed6E/20IRMeKAuAeUpQO4Z1ZkhGTZFmWIaR+n5z1I+KBdHi/g4FxHuQjhUGEIpIVxAOSgHmnRcYB2SWPU5EE45Xl/74Mf/KDvi6DDaMl7hezvor2ThZNJC0CPt4Je9rKX+R6P0fwjH/mIp0ffoTsCspJGTD56WAwwdvRJHfSHfugr3uR/5zvf6YGXsiPk7/n5Fi+g/9Zv/ZY7H3TghT6JbctCoBwheKcf4ZsAP9AgjmUkezfe1OGdTcrAK+/fhl0DyiNT2MpSsSKnAzSOMAjGfgomwwG5jgMFDjp4qv8nf/In9olPfMLrsKfgdRuMF9Ah0cEAmtDhzXwMGCFxKuJQJnRQAmkojofu7KXIw1Gjo7hvRyelRRrlMUBkoDPgg1EWObinLQYDHJvf3yET/MMPBsxSF8Ab4dEKZGK5Rx/EC+kskXmMgOzogz6OQS6vM2RdDMI+MGJoQQfdxh+NXCjgnPR9/IoBOtQPp+kG+okA39TFRulL+pB9GvIjG3bKSwL8rOfv/u7vvF0GbJ5n8isDdATPtBmOulws+nMNAZgn8Bzsd37vd23s+KTt2LXTrnvKtc36LmAjjUYoB6U98PAeX0bEQ3BWKq961Svt6apHOTqDkF5UTryQjlLe+973+pvyGMZb3vJTPtOFc7J0RGm0SefiFCiNP2FMGj8fYaQKhYG8nPHLddoi8EcnGVWZmXEcZlb4Sm0xUqbNNm8tYBQHDqSHpnQsbfNToGc84xlePw0eaQCBl44oZAc1WrAQD42O+K83/vqv/9azoccJ49TUhOezpIGWzM7zrZFGcfI8XzIQA4zr4Ycf8R+OonfesmdAwIAD/Alm0FB/IR8/TOZdw+3bt9pb3/rW5qndzEzrQIg+/f3ff4f/2uOKq6+0N77xjc4f+b6czmQlRlfpxDTNDn/wzj9y4964YZM97/nP9fLoKHjuBNrH0DkowwZiAMCOoBntuQ5YFmf31GO2xO4YQDgwKvB4R32FHNGn9CMDaDp5bn3/h58C8WNr6MaAUSol56VM6H0xCJ6W9I2UAJUJ4XTTU5qdeH6huowMxbAtnltlaXQcb3ijuBnFF154of8d8qfI4eqVGaXxy4R4eJvo19lPSCEo4h3veIeMe7+ftL3udT+YRjnfOTKCpuUGoB4dzOyDsukc3lT5kR/5Ec8PRc29Ts5L29xPTEz5b7AwTGhxOAIf5NEuv0Tgng5PS470c39GQ5yDh/aMioyU1A+n7gT0w68oPD5FTlfVHpz6g4PpN3McHrDsO3r0sI/yL3nJS1xOfqUQdNHXO97xB+48V15zVVeno9+pTz5Oh1Pv2nma/f9/8RdsoJyepQZtEPIE0Du6jNUOeZFPHO15epvTMYiwL4QGAy92Sd/BC/YBf9DGKSkzMrLOf5vIy+v0ZTglTgh9ngNDl3QQfHRD8LSoX44vBN9Uygj7+uQsUvbI8FDzyJ6YUYlfAWCUvNmO0/zID7/B3vSmH7WnP/VpVpESEDQECEUgCNcohw00SsOAeBeO/ROdiCHQDsJwT3sokLq0xUvVpPMeJg6AoggBylHXDSVDUmqaoX021jUzXSxZ2XsyYHB6ymwL3/DLu4yvf/3rvQxOGjxhTHlAPx9S4oq6YMXo72fwSC+iwxPPPwnoF+dnRndjFYJv9IOMeX3Oh9AR9ajDNWn0O4PT5OS4DwJct8dxTfvwQR1C9FFThwuAMpRnKckJJP2HM9F/vMXEdoV06MIbrxb+2I/9mL+xQ7s4OvbLdV5e9EOd5aDrj1jzQGkRM0XzS/DJiXE786wz7c0/9uN29VVX2DVXX2VPu+46f8WIl2r5oSTLrWc+/en2ZN2fecZuK6mTZ6amrTqbHgGguzD+mpwXB0MgOudzn/us9nR3uxNdfvnlErzmhn3s6HFXyORkWvNz3I3hwBcjE47Ka2MoC6XisHQizgTtaI9lVYD2pqdn/CExNKjDd1dYYiAP8bXXPtm/C8LMlj4yVFQ7+3103LBhvbeXliC50dp/gpNm8FZgRCZV0OzuN8JAn2YnLdtuuOkGMvxHsvzIl1O1QpCjPisCr40DIAvX0G1dl0plX+byo1z0gjwYHcYFDYAh8dMrbmdneTm6z9ujb6l7+Nhxe/K1T/FfiFSl+0JJSyvJ9dWvfdWOHDtqp+/cZddcdbXVmMnUNvoM2iErevCBWXlf/fJX7fjRY7ZehvxU6RSHD0S9PNppRQgnZqWQFJPVVaRkv6AMfURfbt262d7+9rfZU6691vvxaU/HRp/qfXnllVdrD8tHtsyXzAyu2Bv1CS1bYfXFygpH5gSURxbdBx4Ar2BRP2LNg0bbgRFzjL9r1w4fOZgZ+PUtjDNDsC9idkpLlPRZBY7Y+eEodZMgrdPFGEFwPDqdX2SjAIzm/e9/v/32b/+2n2ZxwvXHf/zHzdOt97znPfbud7/bl4ak33777T4TojCe8VE/lBfxYkBZFAZf8MQ9fDKL83xrYmLSZ1hO/JABWZER+Mi44G/elqT+k4roS/qBlQB9gIwMGpwys7zinjd7+G4IsiNPLJvzjrtYdLKflaIbTeQK2eK6eV9mtk5fI6AvkQk5P/rRj7qsBMoSoxfkpj0Ctkv55WDFvc7sQkdg1ASuMTquMUYfjRSC2XwIYMh5IREGoVgasryADkgzW3qgTjpLDtrjmphAGTbFAD5QLqdxzI5hOIHFGA1lGASC57hn+ci+lKUs7d566y0+Q8I/b9RQbinOfaoQOiAmoGv4ZnXC4InePv/5z/spMPpj70OZ0MVjBXl7A3Ef9sDnNVhWIxsDDQdxOCP39CkORz8D7vOrpaVixVbBBhMDhBGQN04MPhwQ4UiD0eisSAvBuc4r55ZbbnGhCZxA/uAP/qC99rWv9Y8gcc0+ig8RveY1r/EDB675kAwfBuKDPsy80MMRUWSMVHnHWyxQMnwAaNABzGocTJDHyeWHPvQhd3j0QdnVcLq8fk4GoEcfwi9yMNgxsDHboXN0hu4YUHA68ulPZDvZvKwG6GsCvEbI3wP6Eh3w+Ar5GFj4cS9LTfqVWQ7ZibHRsNmw46VixVbBrESHhSGHUyFQTNc4XzBKx5LHfTgqaVyTFh3P0pKHzaQx4nLy9tznPteuv/76ZmBtzrMl0omZeVijYywveMELfIkED9DmVI52uY72g+eFQFn4oV4oGnkwPDqBvR2fQIBfZlw+mBSGmQhIxR0PS2h76c5/soG+6bPQBbKRRp9y8IV8XPNjV56zch19hW4e7YDPbgEgPwdk9CXyYze88EA+ctK3OGaUDXtYDlbsdNEBAa7D8WKk5zoCQAhCME0cdQg4KktLljYYNidK5HMfsyZGztKWmZQyjFbkoRzKcZ++SDbiNHnAziyEQ0Q7izEalE/byElMh1CXa/ioVtNn8hgdoc27i+xDee8SAz3ZoO2TCeSKmYsQ8hLjgDw2wNj4FTxvaiBjlH2soVN/8wgAmehXbAd5OUPgnoGa/uSkMxA2Sz76WQ5OitNFByAUzARjGAidh/GRF8YaBk854kijLvQAP6rknnU1R/JRjrSYRfwotzrTDAVtjEv9GgQUV2oV27R1k114yYWed/jYYbvznjs9r17QjKv/Gpx8dQF8JedqfcuTEApHvu3bt/lMjKPDI8tMBgPqrj7Q9fJDyIfeo08ijYGNfQ4ntyyZOQXlGVu+z3gVcCVhtQGv84WwP3GifuSr0VO2Y8c2rZCeK32wEivYpz/9Kc3we7TM5Dkeb0xxTkF9Bt/lzfQrdjocACOkA+gIAgihMM5I4z4EpXx0NGWIMWzKEsfSkn0Tz1Uw4hiRKEMdjJw0DIL64eCAa/J4i4E02mWNTjkC99DvBmhQNkZ+2oYHQDr3zLgsxVgGA/ZA/EyG/cCjHaEHAnIhL9fIRcxxOwMKoz1y8mkH0tHhYwXwu1AM0AODOI+a2KLwCAi74VU13hWmbCwvuWavi00uB4tyOhSMwcFYGB4N4jQsBcOQ88ZMGUK+c0iPAKJOzG5BmzcdWAqSx54C44Uuhk8+5VEI5QHLS4wF5NtmuYDycFzqsOyDb5yUwYKypHMPbe5pM3gkjXZJQ07uKR9yRTl4YUnC0gTDZan5z//8GX8bhDdm+PUDZWLAgFfah/+gBx2uKRPtUYd0QBkQ+g1agPIg+AreiKMMtGiXe+pyTxuUC3rRPoh2oIH+OWSgPIdbPPsKA6QO+qdc8E492kJf3IO4B9FvQM2oDDLCZ+eQ/shKevtnbtnU37Sbt0PSaAMZkYNr2uc69M099cgjpk7YEfevetWr/Jp0Zni2J6QjN3QJ5C8HifMugFGA0jBaGGWvBEJQ0kLBCAeDxN0A4zhHdDIGy08+aId73gwIcJ83LNroBpyBDTJleQcPx4t2oEU7zJgAXpCDNJQaMlIeRLshJ8B50A8j4rXXXutfY4YeZRghaYfTQEZG2qNj0SPpyM01bYW+KBMyYkikcU0ZYvIiLRDX+bTgOfQYcoVhQZv7boAveEQ2ZnJkja87Q4d8DJGBCxlpC16pQ5shC/eU5x6d5WVdKIQcgOt8Hugrp18aoEtAm9CGB67hi2vkpj785vUAb6SjD2TjnoDd8Pod5aD3j//4j95m6Bga4RdLRVenC6XTMCMeDWNUKC4MgTxADPMIA6jbDdCjfAjAszeeq6EwHqzzHCxPJwRHUcTFhjpggdBX7LOLzr/IyoWyDfUP2Z4H99hg36CY0+g/k/4KDAG+wxjCWLjPd3onUDY6DMfjUIXjdnhmRuDNDpYtGAUGQHmv11e0/oE0ApMWBsk1cuGo8BJpAF5oB9pcJ72nUT9mgPxMwFsvQ0P88QsMibo4G49NmN0Y8VszznygPZaV8PSKV7zC+cHh2O+5vma1nK/VbWp8wipTcvB6w3+Lp521x+TxW7VGVTMpb4lk8ejQsF9Dr1tAVgLX8EOINGwxnD30hF6SbpJzwDt1yCdmwAuHI4SuqEKI32by9XC+ik0aXy34/Of/VfWx7aJWTxvlF93tuxO6Oh2GAvB4GAzhYDqcJWIExRFxUsqhjG5AadRDOZRn34XjAd5oYcShbRRO+6HMUHA3wA9vyUAbY+UzDhgMtKBBu4TgPdJDnm6gfMhLHXhmhMRQed+P5z1cxxIWHjBWnh3SDvVplzT44BogL84cPHAPaCdvYN1Ae9FHtEU78ME1ed1APQAfHKrwmIZ9HvqMQTj6P+8UoU/S8rwHL+yd/uqv/srfIloo8KZRhPw917x5hL1AM+w02iQNGWOgo21kj5kr9ICzAXQRdalHOd46Ylkd/fapT33KXy2ENvnEy0FXp4MBOh9mYBSGw4Di7Q8Y4B7hKMd1GE83QBt6BDqDPQPLQITkpxi0D73oONonLe67AZ556Zq9HQ7O0Te/swtjIoYeMSMg9DF+ZiZkII8wP1hKJ6eDBm9u8NzwvPPOl3Ee9188sDRhtgPIyTVyA2jTeRgxMy5yhcFi3OSjX4BOCNHWQsjzjdMzC0M3loKgGw2AzlhmUxc6/DiXxyH0O4G+QV/QjEEk+giQD8/ISBy2wUzOCTV/UGShQJl84JW0fGCAhibt5fWDHolpB75Dh6FjEAddUTbqEWIA5H1ftgzUpz/43ST6ID9kXCoW9Vd7ghGUikIJNJpeJN3qM0nsvULxCBAKWAjRQQQM/YEHHvQ67MM4qoYedKJtmVOTfooXdj46m7ooDfoYDL8G4Bleqp94INAOL1DTkcxSPB+Mr/xGm7Qf4J5AG/ARsxedsnXrNjfKHTu2e5mzzjpbOmzphBe3GRAaWo5BGwQf0BmfnHAe6HR0XFQdykVZ+gOjCPkTb60YcI3c8AEtDqUYgHAkDDWV8WheVLU8xDnDyFg6Y6zUZ/l/vpb/6Ik8+KnV0l6KtuGRuuRxTZ0HH3zIDZ8TaV5ERzb6Yr5AX8Fz/r6Vv80dgr98VNeSEN3xNQLa48V5wEzIIHfGGbt9qyL38vTKbDaQVVsHeSB4DbtnMEFObIIDOVZJ2AR6oL28PSyE6BdeeO76e7oYocjjOnV0+ulKjDAElMuoEqMncdRbCNAKGoz+7DfiPoTnOsopyXkgL6UvPMVTP0a34Nc7R4aB8fGjVPJIQz7+MgsGBpCBtpJyY4k39xAD+pTjx490LnzSMfwJMNpLekinXf39qU1egiadsloIOf1qJmcsyacq6e+6Qwsn5pEibQHnVbxwX8jty0JH5HONPNwjK7zjgORxHf3Dc6eFUOxLp7ZxIETdaAf67OPCCQk8uwqb8foqTzppyDE5mR79oI+g0Q1J763Zm/YBvDNwDQ0PuPNw3T+UXsZmX4bc9D1ysxpBn3wHhbhaS896qxp7gh/KU5d72gHIgV1CGzkphx64ZhDnFxqLQfC86B+xRoU8aDyYQymhZMpGXii7GygTZTP7dsWEE0MfxaEUnIRr2iE9HRwsDHiEJ0YnZhGUGIaZHnamQ4nUuWlW55565IVeSE8PRlu6Ig6+qQOoE4FiirwuH8WlbTocWeCJ37hyTSFokUY9PnwKPcp752pGS+2nckjNdT2TP9qjDoHrJE/iMYyHdK5D390e8PpHhEUPGtRnQCKmX+gHfoQKf6Sl9lon16RhrLGfpjwHO7RL3+LIyLsQoJOXAyBDQlqG82uBUjHxMTCc/kKw/ne90j688qCbdD4qxH2dP0emOq5LDdyJt+SgeZtDBmJkQGchE/JgozjzYhA8L+uX4z0sFckQe3hiI+90aQjpYRXRc7ge5qLndD30sMboOV0PPawxek7XQw9rjJ7T9dDDGqPndD30sMboOV0PPawxek7XQw9rjJ7T9dDDGqPndD30sMboOV0PPawxek7XQw9rjJ7T9dDDGqPndD30sMaQ04Xfnaq4hx6eWCjs23+kkf6WGr+S5de8/PaHwA9E+VkKvwPih344Cb+3S/eLjdNP9NJ9+klRSg96nX7D1/qRYvoRYyCfHoif38+HxfxGsBsPebSX5e+1BTrRyafNR3MpOKH9jGantkH8ADSPuWUX1u98dBeLdpqLpRfluuks/yPmTmVJW6jNbvy0/4B2qWjnacf2zYXC/gNHGpHBr2Ehzn0w263RvFO0I19/fqzs92btam7ns13oQJTjV8zzybYY1BaQH8zXfqTn2+5UdiW8LQbz0V8sf4H2vKjXic5jCd36pJtc7fV37thWKBw4cMCdjso4EDH3kTYf0UhfqtG2l+1Uvxu9fP58Tr0QjXxe/nMF3doFJ3ZCq/0T87rT7DaTg4XSOw1685UHC+XlEeXa6bfXX2jQ7YR2fdD/CyHKz8c3+QvJ1N7eQvwvRGc+dKvTzt/27dsLhaPHDjpXNN5JwKV2KuhWP4+ldlo72pUK8u13y+erXHm0y9apfh5FPqiaQ3v59pVCez7f48intV/nB5X2uqDbSmS+vvLyjfTNlQS2E+gCetgC6XysiQ9N0Ued88tlPiWY8jvFUS59QOrE/GqV63SfBsDOdDrF8b2a9r92u7T+T9+TaUektddvL5u33050on7kbd4kp/vylz/boCKZhMhcbBzgPkLcgzCK9vSI+dBQoFOZGAk75bXHna7z7beXCSyUF0oN/USItG757deEfB0u83nzhSjfnsZXsECnMqA7fxGzsmF70YrDuNvT83H6enTLGfNxcopEZz6nyztt3unmK39iepIF5PutE/L5zb5u+8tN89HoVBc0cn96qL1usw3FEZ77vO8oFD74wb9tVKt8RJZsvrzEJ91awsVIxz1KiTiUlDdqrvMNAL7IlE/LB+DXIsHH6Npj/oB7We0vlN9Hp8FXLr1YEs0sn/oL5ZeQV3ERubJ84vTH9xtWnZWBSX7OlIj5vmLE7fl1jDEXk75gfs4BYuAjjmtC7LMj5PPpA75QlfJYanIQ1orzTtE5X/Qzp+UP13Nwlv6IPeXhjz/aQlnaS/ft+cRIGjEaJI6DsvYDuPkO5DqVTybSKtd+T9y20FAZL+TIX4O8zSVg153Ld6sbccv+k7+0x+En3FP0DW/4yULh4x99X4NTSz7OCR2ETvSSkGn5lJTRKZ+Og4H5Qn6manfKyM87czAXTLc7PYNCvnxeqE5xp0Ej6SnVC3p5uqEs4najlb17HEZNTLlIJyY9Yui01498UOUzfNIhIe9QxCB/ny8T+fQDYJXMZ+UYeYl91czf4ZNTybXmpEe5RM+rexx8tcvXzn8+Pz/TRYwe87FqNPXart/2QZ44X6+9/ol0k/5Bum7FebTnzSmTLU/zae008vXyeegQyJL1j/hpi9Ezs2mxIPtS/NoffHOBDwd7RUYtr9ykhwFyk4hGnEY3rpOhYMDJSSh/4n1STIo7BToqGVEY39z71NHpvj1mFki0AfTmxonP+dPzMe22x7RDftLDiXRAtJ/kpUNa/HDfznfQTTHfuEx6ihC6y8fzBww9c0gci2scK7snDoPqGPt14iUf5wNp7aGVLxLOYwx8SX5i+Mvfd8pPs0SrnU7X+bRIj2tCS7/LC6G3ph6z68WGJnJ6Rb45es5iTxeK6iVWSYwfHkdovydE2TlpNKwAOSfZdh8B725Pi0DeYvLbA1857tRWBMrMlx/145o4ypJOaE+PsvlAHkEb42a5yJvvOp9W4DBDgT9LGtf5MOfPlubK8GVo7w/RySMZtMpAP5eXN5B8uu9p3DAUsjjtc9I9TpLPd6eJfMrqPow/7onDQdpDpIfTeHquHU/3AUODrjuEyuTan3MvhJz5kEc+rT12oOMs+OyUi+cLvo/LrvP0A/PqOrtWzbnIV5jjyfOBxnt47MMN+cQYw3d0yo/rNizKbgKd6C42juvHGOQxeN9yQ4bM6x+TYaXoRHMpoaNelxA60VxKeFwAOZYbVohOOl0oCOnfZSCNZnR8Dz2cImRG/GjEQrN9scFguYKwYsQy4VSFlaITzcUGoZNOlxI60l1KaB+JlxpWik40FxuakBzLDitEJ50uFIQ850tC2hQuYe3eCRkTT1i4/CvUYQ8rR9jhcuMOaD9cyYMhQ9FyQwYaX05oguvlhYbonMrQiafFh0An3a5FOBlg3F5JWCHog5UGdLHceMnhpEidIab7OdP+YgDzjyeEMeUDiDiPUyn7/CPx2qKTXtYAJ9hpcoilx0uHWvaNQTOk13BShyw0Rc6BC6Cy+TgL8cyjro1lkbdPdFutz6ZZwp/38ICXtxtoi+cevJFAmPt8hTTKpDcg0r2/pVAvqFaKeYTDdYmn/7TdMYjenJCeebXu6x54dYP79GyyVb/1rMy5dR54RBf8+RsIosebIP5WCHVUUtKngHoUNE96DP2SdN6oKYW3goKPTH6eX/E4Mv3xQZw0/sY5zKVy6RUu3tjnL65KF86XuFPgnjxCq28LVi71W3VWbSqDt4LSiwbivMQfo6Rs9AP0+MOQKS3eMKJePBTnmVp6hzY9cAbQC1AW2qkOfNKP6fU10l0/uva6sgv+yCMP+WmLNngpmnc009tFKq00f4bH4wz0mw9tqIomHNWk7FLfgFVEq0570gvXScfUgz9qJP3kdXVyA9K2AeED+ev50SJ2Ypxec4qn/7wnSGfEw9vUCRhGYoMy5EfHUYbr4IN7OiI6Pp8XNPP1I148svKZQfMgNhkdN7jY3MEgrx/PUwh5yuU+6+9Pf1Df63odyZlde8jKgzAwaMI3cqC7uA+5eZeVELJjHPF+K/rlr4RSL/QQOgi+aN9fBVM6v3AAlIU2NLiGLjTyf8w++Gy1m/qPe675a6V5WfgrpfAVoAwIeyCPv27Ka2DQIU5/MTX91Mvlqc7oWgOEyvJnk5HNBzYvr6FSZbuBMpSlDjwmx019AI/xOlw7OqWdLCQtrQgw1ykk9A30e2j+eVx1Ou+hISydX5mRgWhWwAD5c7ShJFhDwendvKS0ToiOJo5rFAqd6OiTiTzN6HwHM7cCBhXGT6wCmtHyfCQdeBAw8mRMMeonQ8HQki7KKoNxm4xm1kMyvPTmSTgG19TBkMLgCQC6lAmHimtAvXAc0rimr3AI/kY4CN4I5IPWNcafDLm/HydK9KpyLv7EcZQjJBpp9mT24k9RU448dBWBNHhAfvLgJWSDBvkRuiHKhD1Al5h28voApIP2+5ONzGKWjmCsGzAoRpjo0Hw9OovRLQwMIXFGyuKIxKTlleF5mbLoYPIiLcqSR4h6i0e7OpKzzwltI2PwAuAHOQjIlP+tHFGTRiM5DYEy1CMA6KEnjIWyIRfGQllCGA55GCLtcR2OThz1gy6gDAhnDEegPiCfOtTFYYjJDz0G/5FGfehHv05NTXkd7mmDvykeZUIX8EeMI61bt87zKJdmvTRwUIfAwARvDCzU86SsbsjdDSFrAB6dthDL1XbQBmElWKj+sp2uiWyEPyFkCKPq03qadXlaTjBa0VEwR2enn5AQ/NcOmvV8edY3mJZC2Io6gHSA0kKoyM/vofw+M7BlQfzlVUO7aY2flBkhjC+MEr5olxjjixksApSI8oH9WLXKoEFdDBSnxVBYVjHzp30aZSoVZrqKx2lZyf6rJr1VnId2Y3Q+tG8jsIpghQHIp0+ow4AY/NNX/LF78ttljPuoF3lhwOE01CcOx4+yXEc52qHdY8eO+R/nDz1BK2Y4eA494nDUw4G4pi7xYvo46sMTuqEO7dOnpMMfoP21wrKdLpjtBoREcALK5T6MCeVGp6EUlJpGpoKXnZiYVH7qjChDnXZF0dEoFFAmOprrlSE5eou3NIBEXiDSkI12KRuDDbx4wHGhJcMnPZbSgJhAehgdhhXGCKAdMwLGEjoJGiEz9dA195SBD+4BZUmDFjFlYgblHro4AeVpC1A2aIdO4xpeAPKOj4/7LAKibULoJs8D19TfsGGDbdu2rSlr3pGgQVl4g0fymX3dYbQ87SvP3TPOB+oGz8yYINFIS0za4DqQv14JFqLjv6dDCdEJrZAKhNKCSL4MKBbmjvgnhGyjPDg45MreuHGjnbH7LKdLvr9tnwVAGToBBc/MTOU6Ip2yERyaTamD8e7cucv5P3z4sJeDNqdf0SmJfvCZlBztx7XmBS/LJffMxiLpjpIGCQwT3rShn53OeMJQk5FQB14HB4ZdxnRqKOPMnNOvFQLQp35fOS3PqI9RIB97nXvve8DTwtAYWM4//3xv49Zbb03LJMnY39+X7etahn3GGWfYOWef5wPRvffea7fccou3AW10i8zhTNQJ3rlH5nPPPdcuu+wyO3DggH360592OpSDRln8hvFu2rTJXvKSl4nmrH3wgx9wnmgj0U6DD/VSv/V5egwK559/ob3gBS+wo5L1k5/6uKdxoilWsjpxUlm2l3zny2zHjp3isWJ7HnnIPvnJT3pf+IAhPQfgP7/KAgXRQTZR8vjKK662iy++2GUq9xXt//7D3/uBDbyhB0L0k9PLENfzxYH50qGLHl7z2h/L1kxrgFA4y7+h0VEb0EjaL0Mqa7QsSQFFGVZRSuxX+jp15o5du2Q8Z7mx0XnoARp0PHCh/DBCyzDRoB6gDEBxcb10QFdB9H25h/MqFH2JnJYmgWRQ6R7eaBcFU76gdJdLoSTZynT0gORR6BPPfYPiWYMAclN+UDL0SV5mEHjH+HA4AN1h6Y3DDdK5Z7RnSY4uYkSHB+pQrk9pF15yie+dQhfwij7DKLiHVsiB40Brs2Yg6NAWZaM8ccgJn1u27dCgt9NnOtLy/cPsFI4X18ygwcOmzZvtzDPP9DYrlWlPoz51KUM9+gD6I5J946bNdtGFl/igNjQ0kvTcBdCAFk5GfMEFF9hO2RZtj4pm0IB3kJd1tXAC1/kGT0bjIQwxAiaaGnWmK/bNr33Nbr3xRrvtphvsDo3edyrcffvttueBB2xSnVhQp5925tm2fv16pyGO/N+8glxptXTPNQEDQ8EJGMHc0W8uMKCWc8ZMCGgHQzBGXe0ZleGGSHrIwTWGQqAso+Yjex+WDPfbww8+YA89cK89oNnmvrvvtrtuv9PqPuqa7X34Ebvt5pvtrjvvdLnv0Gx0m8ItN9xg99xzj7cT+grDUWPeJoH9MW2SDg/MPrTPQQ/7oYLy+VQE+9NrrrlWNJAz6QsHCIcCIQ/30MgPVtDnnnwCswVOSVl3sGYaS82kD9qIOJyIa8qlDxHx7E1tK216ZtLbjHaizbxsLHnB+NiY3z/5yU/2/WD0FeVCLoBs7Ov919rKhz7lTz/9dA8Tsq1Z9sW+ikn7RGIAfdoOGVYDJzhdvqGT0Wh0VgutkQUjmJ6edIOZmprwpRQjJsvEvXv32iMPPuhlN27c7KMbygWhkNRJIcLJUVLQoEM4rKBjQQ0jwfHkgMx2aZY9Ub4kV8sRMa5qLe1lI6hASldAdlYABIwmGWbrOVvQnA/kYTAY2hyoPsvD40eP+qDFkgrdhjFDP/ZkS0WLnxPMZ1lYSD5kY7UzIz3doAGp3Nfv25MtW7Y09YTOALMZtLjP2wploHOJZn11lt11112+hOf6VGDlWsP48qED5jpdIDkJyimVGLkZddJMhWGw5GB0qsgRWT6uW7fBa6FAN3R3itVRWhgBvHjHyE/Yu7nj6T5mOxA8REy6B1Viz6UUT084ccZlluUEMpyUQH1+nZ1+ob0wwuAAuqFujNoMYjfccJOWtoO+rGLJST5tsBViVj4paCyjH7I6iDgnkJZLr0vnfbKR/lLZ7rnzLhs7clTLzE12/jnnWqOa9mEhE7bENXoIZwMMbOw/zz3nfM3+VbvttttVjvaXwfdJQGcvWQQQriM6ON7csi3Dw1D92yzZYwY+784rQjFKoch0OJJGMWYdSEGPwH7L0XR4wvIV2XLitKxzPjhmFx+MqgTawekS70muVr0Tr+l85HIn0r2/vuVxCjgI7aRyJ4ZuCF2EwwKvJ5rwed9999kBrRpGN2zwg5gYtIij/EnBsh1vfhmTY5hNTU77crlSqdq3vvUtN6FLL31SUz/oEFnoH9KQL/RC4OWCC86/yPplQxwsHTx4sPngf7VAu/Ohe692BYqJkCHnePn90omQoWg24NQKIyDALIYZyuONFehxEIERpdEsOST5+baWhHnrpZET2snpMNBk1O50QiFzFPZPeTg/WRyBUzbq+gedeF8Q5qUT9BK6SfUwkKych2Qw3RADACGMEH4xaE4A2X+5oYrWhRde6Mf0oevlLi/nIHOMJcHrLK7fYv8IcK7bbrvNKpq5dpx2mu3YscNXRCG7l1O/spdDPvqIPSQHSSyvWancdNNNTst1vgj9rgaWabEtA0skgkznDkjGlRcQh0mGjbKY3dxe/Lg3ZoQUQuEgHDHQus6LkV0v2hnnlgtD94fhvIKme3igbSaGBss5tdunvQVod4487y3+YkZRubYjbeomp2yNzEtF1MHIWA04PRkdwLH2799vt996u58YP+lJT/L9IyuHOHhYOTrofz64wy0O6A9ZYnCgD9hyPPDAA35/xRVXeJ8gB7KTjy6iHmnk7d6925ek1H3ooYd8j+jL8mb/nHy0+v5ELNYyV4RODLiBse/hq0+ZsaGofGC0ciNlb5QrA71Q8GqIkOgKakNMOB/x3C0MNY7oA+SlEDMYzpV4bQ/I5IMOmxYGGZaeCqyWoellAsg/x1ET3QC8tdpOBuc0Mt44wcTB7rjjDps4ftx2n3mmH9NT7uQ53eoB54g9KoBvDlSmJyZ8uczjBMogO3Knvkqyc43sHKBUVYbnm5Tl2V++79YaJ6HluUbQDh5+zwcUBQthKDFa5QPGrQsPlE9Ox222nDgBGS9uqPPz1TLkNMskZOrQLBmGn5yfB8d+1zx8KLGsy/2ZrDygl8LcvUWEfJloJ2LQXrYzEv/sF9EF1dNglZ6rkcA9X7NGh8xuGF1DS8+nPe1pdvzYuK0bTYdTLeTl6STbfPrMpy+gc8CetiM66xJZ0mER/VRzWe67/16ftdhynH32ma4nHIxZnqV1vG7IHnDz5s121lln+2HKzTff3Fyu4ox+MHYKcIKk+c7PX88PlJgPCUWWdxgvD5K195ELWTmbLfSPskSb5RuHI6aNcEPrb00QdJnGLP8TVBWN0n3Z859ZjtYZmVVP6rdqXWv2vnJarUTIZg4roMz2zkdUcaHsVqBMa6kL4JdX1MKhZmu0qU5tRMyBioxAvJf71XEikbWqymkflQ58RA/5FTSc0LL/58teBd8OKkYd6Mr3h+RlM1vitsVTXcvdhgIxRVNIvMdv03A+4Dp24075U1NjMrai3XPPXb7UHBpdb9dee50dPXrcy4NYitEq78lKwS5LCy19Jl2ltmAxsa4BJtNhDBY+kzcHtwTXuHhqeF6ilQ+dUOa3esiTBZzvG9/4mnLqduWVV2Z7fU6UGWAG3dm4Rh9XXPYkMVSz2265yWY18JSlP8LsND/zac2ga4lMcy2EwkD+ejmAePt078iUi5JpQv7FnXc8hpfqpLcXeB4D2DBz6OIngV4/LUHzSMaWT5ubPx+ana222cs5HwwWGb388s+NxsnyAu2Qionf5HJOJ3jywSZDVz36yNEJqueO2am+2sEIQZth58GbHrx5Af+1+qzdeeftLsDZZ5/rzz5xtL7+kk1MTPleh3Eo/Z2GkIW+y+hn7XTmp1NadyRaOTNk4GkiPaQOu6Ff2P+z3Ny37xE7euiQv6nCK2X83g5a/G2Kmr8IkB6d8KikppmSV+Goq7WSRGv4jJd15JojL+GqAENEcWnpk97aAHQ2Uz5AqdyzjCBQjumfpQGvTlWyZ3akhwODZue70SodA1XcsmHKZoHOVGAO8VkmQ96AuIZf3hd1o9PyA94IwX99VktMjZI8W2OE5Q+UxNF2OB10cFCZuad3BnwtB0szFPQNT1XJgqE98sgjdu8999jI+vX+fiUgLxl0OoTxQUgy5HWzLGQ6Xwzma6vJTwbKIcchOdztt2sAUV9dfvnlTbsI/dNnl156qb9ux8Pwgwf3u01RDhtzR87Z0lpi2a0utkMo54achQCK2759uz+0xLmY0bZu3Wq7/J3LMzxsUT6vTR09etgmp8a9HvQIeec7Ad7R3fJBKpfeOokZRWnQ1jXGSDthkATu6bAkV7bnFFLdVoyscb0wFuBzsVjAsOGZ/Rx8M0jA+0033WATx48a72VyEMEqglUF5TBa6kiAjMLaoN2xAvATfc01fcJAQpo/PhDvvNrF4wPyQQzsfrop+/nmN7/pjw1YlsZsx5tQq4mF+v4k9PjCiJEHJRFQmnpeo86gnz6dc+65ds5559vZ555nZ2jDu2v3bn/ZlrdTKlMTtnfvnvTKjoAxQAvl5TtpLvLpOE8WPL2VF/uiPJoDg9dJs1Y4WMhBQIbqTDr5w+ncSN1poy7tPTrAqI6zhe6JWTVwmgl4hMCyEscDyL+Ws8B8/Ri6ht9wJq4BcuBEPORmFuPVvIsuvNgqM+mkk3wccZ1mc2bEffv22cBg/Bqj0ZzxmsvzNcayNTu/0c9FKCGdQGWKy+qikDE51MTYcRs/fsyOHj5khw8c8DcoHnzwQbv77rs1yx31dxcxbF4Xk6u44sBcFmLZtZAis7zmHoilSzr04CCDmavJnwLX7M08zhqDj6ZhKvgSMxtZ4SvK5a/XFid2KTwyi8EPvDJQYKz79jxkp2lFweA3NjbmecgafRV6Xk1EG/O1FfaDrrkmwB8x8tx8861UtosuusjfMmHwwLme+tSn+sBx443fdpnYvvBTHg6d6Pe+fvVZ5syrgYX6ftlOF5C9zj0HwKBzG/vW7JSU6katMD4+5h3PEoGj7Ntvv83uuedue+DB+2zvPs1uY8nZ2BchAAEaIUyku7P5iDW3XVLmfp+yvVNTWa+J04lUsSwHY9aCrGjTsSy72KyPjChWWL9h1F8gZnaQcBKFH1Ty+QRG5DQywxejc/C6aOT4XyxixvaQu4+8OOiZzWYvThkLkvP4xLjdesftMuhZO//CC23bzh02y6v5Ku8ny0Evo9NOP8L8wLTyoTNq6lMCtOCTmFNQPw1VII+fSNF2lCv2lW16tmL9Q4O2RwM0v8oYHB2xiy69xCamp7Qt2eaDyaFDB+0W2VV5oOx1yxpsoImc3K/c+peHVW+WUQlDZErHCGMUjWVZzBTc8+YB9+Gg1ElLI2aW1g9tQSrTNIssZGkYbycDbqZHSEi0NLPxniWHKEDtFMWXaQ/ANQZL5xf4xbtGSflYhjjtTHR8UBGW5HQrWuZkjMwZ+VqoMksIzALoPu4ZNO6//37/NcfmzVv8NSlekAb0BX/6a8mDxjKAziLkQdsEZij4YTAM/rETP3WVbaBnlsrwy1IZW2LWAwzolA9bY7ZPfdV6pe9U4ASnyyv6ZCg9P/K3G2WARZymG6v7g7q6lVWmRB0UQzlmEHcKjXqUVx733lmMjzzz0VXMqtEGm+bms6KmE8JHOo0kpnV/cTr2YaLdULvTE2P+jt/05ISHqYkJLYPHfBnsgWvtjUD+B5XQJdD5bkhqdyV6pG7Un49OlJmbn4wYPjAwlsPoBz7hi2uM8ctf/rKX460NDrMwbEK80UJZ6LosAvWgBd3YB0IHp8YRWK7zuIdnfTwv42F1pKFreIEeezIR9aUgAbqEsIu4JvaBOuMZ/nGe0C9t8xIz2xAO5TjJvOqqq2xS/cXpJg4a8kcd2uf6VCGztBZCuSB/vVpY9CDfdJq5SMaGGMno4JlAh8WLypEWoONK5RNnqLjnrQ6H2mSG5W+yE9gPsBzDCDAwAo8O0hIzfaMl2sm3x8DQGZn6G/M7ZZ7O/IhubDnKYnHs2BG76aYb5ST9dt111zUNnbc1km5bAyYgJg3gdLPSAb+sx7hxAPLRD7MmzwdJQ7f5QwychtNGwF6S9GgjBgWcJNonTRdePg/K8WFaaPvBkMogw8DggN151+3iL/34NUD5FILW4vV0MhG9tWzgNHMcp8PpHYIuGTE7RWiC69Z9+kox9+psOUBV1zyL84fT4oPYr9UhfGW6pnzeZqGTCYCYjuXXA5pe/Z7ODr6984kJJGEgKodhMJqjRowu6uTrLgnznHrmaUE7QDoTuQfdN6+ho9VBgRVCPt+DP6lUGsH8t2rf+sY3bFLGf84559qZp+/W9bgcr9xccTTpqD7XvNHB/czklJ8gApwIR8PJ0Es4F7MMzukP6DPeWdr6Sw+636/lbdDl9R56i3uu83wDvyYtFyg/0Feyb379a1arzNiGjenVtpu+fcMcehHyssyn79XGsltdrFG1l1uWMS4Adxb2YlJvUcuYWMoQeDUo4rhm3+bXKscTbPghNGe9cEY5Os4cLyanwTZduxPKfoh5UI4aBweH/SNJIR408w6ilCwGInYyOrzjDIk+nLm29juDgyIeyfjPf4Rrr73W90oA5wHIkvTcOiAiMKPt27fXdXbNNdd42SNHjriDhfzQ4OdEOB51cUyey5551lkQ9lNqyqF/17vAPfUBsacrRBr5AXiAN5aLPtupzN2Koz2QL5+/1k12cfIRvHbCqXH1Tug6s0XIg/2YHA7ja9BpGsNrfCOkps7gYzhaBs7oulL3UJ2lAymrtb0pcC1wQBJOh5HkFcZ1hATxEPwpZonpl+p0jDU6dSGlrwTd6DaNSvEcA3OcWBejxSk4dNgjBzj9zDP9UAWELqCTl4uAoaMzfp/GEnvLlq12/fXPUoGC9lNT0q00xcvHGuBmpiv+Q9T0epbZC1/4Il3P2oED+/0gB4dpDnpZe9Bvb7sd5PM3FBgMJ7QH/9znPmMf/MAH7POf/7w7XP6Hqnk6C9FcCyzb6ZbKNApaDbA+5yOtHNezr+JQY2BgxPr7hm1wgD3FsDpmKAsDXsa/cSLR+YEjrxn6php51FE+OrpTYVwa6f06hXTwAlqjPYbJ9ztAeuiafWlrFZYu6DACmNMHC7U3ZxCbC5Z/LI3B17/+dX+DY9vOnTTWPGyIWY52o02uGWR4rYxHP9w/7/nPt+c+97l+oEEdBiRmNvZcvPnCA+sf+qEf8reNcLIvfOELPiOFw+XlietoW/94nG+fgP7hk8Cyld/a8b4p6fSlH7SpXKoELdqSQ8+vkpOCvCztWNOZrim8kL/ujDD2hcELrgXTclEj6oYNG23rlh0K223Tlm0ethBv3uofN9qwYZOtVxgZWS9D49N+6fGEL6eKBato9KWT6GiA4qLT8x2ev8fpMRwQhwaB7jJ2R6udlrPNB/IxNB84+J+4CzB4nINZgR94+i+rfeBpGQ3tuwGLPuXjHlkxbr5DyeMHFbDrn/Mce9Ob32yvfOUr7Ud/9EftB37gB+wNb3iDfd/3fZ+9FofTTAqdj33sY+6s6B6asUwkRFvRjsuRxaED8gAPvGcqfDawrGXyUdFKX62GZpTNgzSC09Sgeyqwhk6XKSCbOXwj7061fMPkUKNWr1q1pplG9PwDR33at0nxHAL4g26esyEljw/iWnnJkOGBEZ3RTx1R5YvKzGJQF2/iNTopH7zzOZGk41SuqpGVn48w2oq0l+k8u0R6yB5hAWR04mdITjurgwxhfFGO2dhDdiDUDRg5Bs/pLMf4N99yk42PH7fa7IzPUL6PVRtOSzNFzEpBOw5M3vveP7d/+qdP2YP33ed1zzj7bNu2fauddc45tnP3af5K1tixI3bbrbd62RtvvNEHqTTL8tHe9LHbvNNx3To5bv3gOUB50qMOvOHE7CnTmyedQVkCtnMqcMIXnhHAM9SX3BPSfSvOp8t856S1Bx6BNe85S8rlEebkdwjws9B9qciPXJUu6q2Y/1M557FZp62tOBQh30umsoRkvJmDKc5ft9JkjPK99nI4ZHvZZr72kZ3Sm/kdrjHvuWkt3jC6SNf/Kc7aAP5Wj66jfju//NyHuFUn7aUAMQbfrIvfUdavUxq6aj0+4dchQ3LeEYUNWu7xcsOAnJdPKxKOy0H5FUn6VUMsX3E6+qEdpEE/gTg5eqeyoFt60IstAAMKth8DVr5cO9rz5is7XzrtInPHLzyjyED++qQA4drDClGrq7P5rmQurjEyEitYQUZpGiE9cJ1Cq211eDLrtrAY+VWuXZ4m3XmwlLIZCs5HPrRA57Y6OMsTXXd8/wTgwkhG2BqgGGxwCmaK+HFswgmm4sDRqM+sRaAuD6offPB+fzjNLxp4TYuf1uCYKuqzEQ6X2lxYB7SfQhpc5qbN1UUnhGwt+aSZTLa8w60lOmvyMY6WESYld8JiO+1UQ5PpQs/OTzAqELItRr58PRD1cCZCN2C4lIMOjoTjseQMJySNwChPCH6jjW6gTF6WiJeLoLVY+VYDy3a6lQp/shAGR2fmDTAfepgfYYTRn6Gz0Gc34EiUzxsy19QlD8eLMpEfxr4Y+vn+Cx4jXgzyPAXyNFcLC/H4uJnpEDIf2oGiO4XHOuaTeSXyLUU/tNteHiNnKckBS7yVEuXyzhzOtxDytIP+ShA0Tha95WDZTneqGF4OHku8LhXtzgbCoBYr91LLtyN4oD4O1b7MjMcC4WyUj9ANed7yYbGgLO0utd5KsVBbj/mZLpQ5XwD5zl1sZz8RsRw9hTNRlhktPX5I76UykxFzH3ncg3DObgg+FsNLJ4QNgJXQOZl4zDtdvlPyCl3q/eMNi5Uvv+fJl88PWgshHIyycWgSM1osH8nDOfOnlrQVDrgQ8nwth7+on6cDFlt/NXCC0+UZOVVMrQXySs93RiCff7IR7XVqd6kIHpfLa37placRRtqNbtTv5LyRB0iLMktFO18Rx/Vi0Um+U4ETnC7PyKliqodHD5ZjAz27WRiP+eVlDz081rBsp+uNZj30MD8W8o/eTNdDD2uMZTtdtw12Dz08kbGQf/Rmuh56WGP0nK6HHtYYJzhdflrsLSFPPfihLuFUYTk20LObhXGC0+VPXXonlD0sxwZ6dtNDDz08imD2/wEE+UvoQSg2pgAAAABJRU5ErkJggg==',
      },
      styles: {
        header: {
          fontSize: 16,
          bold: true,
          margin: [10, -25, 0, 10]
        },
        kontaktHeader: {
          fontSize: 13,
          bold: false,
          margin: [0, 0, 0, 5]
        },
        kontaktTable: {
          fontSize: 8,
          bold: false,
          margin: [0, 20, 0, 5]
        },
        kontakt: {
          fontSize: 8,
          bold: false,
          margin: [0, -6, 0, 5]
        },
        tableExample: {
          margin: [-30, -15, 0, 15]
        },
        tableRoboteh: {
          margin: [10, -15, 0, 15]
        },
        tableKontaktHead: {
          margin: [ 300, -85, 0, 15]
        },
        tableKontakt: {
          margin: [ 300, -25, 0, 15]
        },
        tableKuka: {
          margin: [490, -87, 0, 15]
        },
        subproject: {
          fontSize: 11,
          margin: [10, -27, 0, 15]
        },
        description: {
          fontSize: 11,
          margin: [10, -15, 0, 15]
        },
        workers: {
          fontSize: 10,
          margin: [10, -15, 0, 15]
        },
        time: {
          fontSize: 10,
          margin: [10, -15, 0, 15]
        },
        konec: {
          margin: [10, -26, 0, 15]
        },
        zacetek: {
          margin: [10, 0, 0, 15]
        },
        stroskiheader: {
          fontSize: 11,
          bold: true,
          margin: [10, 0, 0, 10]
        },
        stroskiheaderPlus: {
          fontSize: 11,
          bold: false,
          margin: [352, -29, 0, 10]
        },
        stroski: {
          fontSize: 10,
          margin: [10, -11, 0, 15]
        },
        materialheader: {
          fontSize: 11,
          bold: true,
          margin: [10, 0, 0, 10]
        },
        material: {
          fontSize: 10,
          margin: [10, -11, 0, 15],
          alignment: 'center',
        },
        podpis: {
          fontSize: 11,
          margin: [10, 20, 0, 15]
        },
        noga: {
            fontSize: 10,
          margin: [-40, 1, 0, 15]
        },
        glava: {
            fontSize: 10,
          margin: [0, 0, 0, 15]
        },
        infotable: {
          fontSize: 11,
          margin: [130, -31, 0, 15]
        },
        typestable: {
          fontSize: 10,
          margin: [10, 0, 0, 15]
        },
        crta1: {
          margin: [ -40, -25, 0, 15]
        },
        crta2: {
          margin: [ -40, -52, 0, 15]
        },
        crta3: {
          margin: [ -40, -52, 0, 15]
        },
        tableHeader: {
          bold: true,
          fontSize: 13,
          color: 'black'
        }
      },
    };
    //pdfMake.createPdf(docDefinition).open();
    pdfMake.createPdf(docDefinition).download(woName+'.pdf');
  }
}