$(function(){
  setTimeout(()=>{
    $('#woFilesButton').on('click', openWorkOrderFiles);
    $('#workOrderFiles button.btn-secondary').on('click', closeWorkOrderFiles);
    $('#workOrderFiles button.btn-success').on('click', newDocumentModal);
    $('#btnDeleteFile').on('click', deleteFileConf);
    
    $("#addDocForm").submit(function (e) {
      //event.preventDefault();
      e.preventDefault();

      setTimeout(disableFunction, 1);
    
      var file = document.getElementById("docNew").files[0];
      var formData = new FormData();
      formData.append("docNew", file);
      formData.append("workOrderId", workOrderId);
      //console.log(formData);
      debugger;
      
      $.ajax({
        url: '/workorders/fileUpload',
        type: 'POST',
        data: formData,
        contentType: false,
        processData: false,
        success: function(res) {
          //console.log('success');
          $('#btnAddDoc').attr('disabled',false);
          $('#docNew').val('');
          //ADD SYSTEM CHANGE FOR EDITING WORK ORDER
          var workOrderFileId = res.file.id;
          //addSystemChange(23,1,workOrderId,workOrderFileId);
          addChangeSystem(1,23,null,null,null,null,null,null,null,null,null,workOrderId,null,null,null,null,null,workOrderFileId);
          $('#modalDocumentAddForm').modal('toggle')
          //add new file to the list of all files to this work order
          var downloadButton = `<a class="btn btn-primary mt-3" href="/workorders/sendMeDoc?filename=`+res.file.path_name+`"><i class="fas fa-download fa-lg"></i></a>`;
          var deleteButton = `<button class="btn btn-danger ml-auto mr-2 mt-3 ml-auto" id="fileButtonDelete`+res.file.id+`" data-toggle="tooltip" data-original-title="Odstrani"><i class="fas fa-trash fa-lg"></i></button>`;
          var fullDate = new Date(res.file.date).toLocaleString("sl");
          var date = fullDate.substring(0,11);
          var time = fullDate.substring(12,17);
          var fileSrc = '/file'+res.file.type+'.png'
          var fileWidth = '70';
          var tmpElem = '', tmpTag = '';
          if (res.file.base64 && res.file.type == 'IMG'){
            fileSrc = res.file.base64;
            //fileWidth = '150';
            tmpElem = `<div class="overlay-zoom">
              <div class="icon-overlay">
                <i class="fas fa-search-plus"></i>
              </div>
            </div>`;
            tmpTag = `<div class="overlay-tag"">
              <img class="img-fluid" src="/imgTag.png" />
            </div>`;
          }
          else if (res.file.base64 && res.file.type == 'PDF'){
            // convertPDFCanvasToImg(res.file.base64, res.file.id);
            tmpElem = `<div class="overlay-zoom">
              <div class="icon-overlay">
                <i class="fas fa-search-plus"></i>
              </div>
            </div>`;
            tmpTag = `<div class="overlay-tag"">
              <img class="img-fluid" src="/pdfTag.png" />
            </div>`;
          }
          var element = `<div class="list-group-item" id="file`+res.file.id+`">
            <div class="row">
              <div class="col-sm-3 justify-content-center d-flex">
                <div class="preview-icon">
                  <img class="img-fluid bg-light image-icon w-70px" src=` + fileSrc + ` alt="ikona">
                  ` + tmpElem + `
                  ` + tmpTag + `
                </div>
              </div>
              <div class="col-sm-7">
                <label class="row mt-2">`+res.file.original_name+`</label>
                <label class="row mt-2 small">Naloženo: `+date+` ob `+time+` Avtor: ` + loggedUser.name + ` ` + loggedUser.surname + `</label>
              </div>
              <div class="col-sm-2">
                <div class="d-flex">
                  `+deleteButton+`
                  `+downloadButton+`
                </div>
              </div>
            </div>
          </div>`;
          $('#fileList').append(element);
          $('#file' +res.file.id).find('.btn-danger').on('click', {id: res.file.id}, onEventDeleteFile);
          if (res.file.base64 && res.file.type == 'IMG')
            $('#file' + res.file.id).find('.overlay-zoom').on('click', {id: res.file.id, filePath: res.file.path_name}, onEventGetIMGData);
          else if (res.file.base64 && res.file.type == 'PDF')
            $('#file' + res.file.id).find('.overlay-zoom').on('click', {id: res.file.id, filePath: res.file.path_name}, onEventGetPDFData);
          workOrderFiles.push(res.file);
          //prepare modalMsg for info about success
          $('#msgImg').attr('src','/ok_sign.png');
          $('#msg').html('Uspešno nalaganje nove datoteke.');
          $('#modalMsg').modal('toggle');
          setTimeout(getWorkOrderChanges(),500)
          if (res.file.type == 'PDF' && res.file.base64){
            setTimeout(()=>{
              convertPDFCanvasToImg(res.file.base64, res.file.id);
            },500)
          }
          //getWorkOrderChanges();
        },
        error: function(res) {
          //console.log('error');
          $('#btnAddDoc').attr('disabled',false);
          $('#docNew').val('');
          $('#modalDocumentAddForm').modal('toggle')
          //prepare modalMsg for info about success
          $('#msgImg').attr('src','/warning_sign.png');
          $('#msg').html(res.responseJSON.message);
          $('#modalMsg').modal('toggle');
        }
      })
    });
  },300)
})
function getWorkOrderFiles(){
	$.get( "/workorders/files",{workOrderId}, function( data ) {
		//console.log('Test');
		if(data.success){
			workOrderFiles = data.data;
			if(workOrderFiles.length > 0)
				$('#woFilesButton').find('i').css('color', '#000000');
			else
				$('#woFilesButton').find('i').css('color', '#ffffff');
		}
		else{
			console.log('Napaka pri pridobivanju stroškov!');
			//open error modal
		}
	});
}
function closeWorkOrderFiles(){
	$('#workOrderFiles').toggle('collapse');
	$('#workOrderInfo').toggle('collapse');
}
function newDocumentModal(){
  $("#modalDocumentAddForm").modal();
}
function openWorkOrderFiles(){
	$('#fileList').empty();
	$('#workOrderId').val(workOrderId)
	for(var i = 0; i < workOrderFiles.length; i++){
    let deleteButton = ``;
    let downloadClassStyle = '';
    if (workOrderFiles[i].user_name == (loggedUser.name + ' ' + loggedUser.surname) || loggedUser.role == 'admin' || loggedUser.role == 'tajnik' || loggedUser.role == 'komercialist' || loggedUser.role == 'komerciala' || loggedUser.role == 'računovodstvo' || loggedUser.user == 'vodja projektov'){
      deleteButton = `<button class="btn btn-danger ml-auto mr-2 mt-3" id="fileButtonDelete`+workOrderFiles[i].id+`" data-toggle="tooltip" data-original-title="Odstrani">
        <i class="fas fa-trash fa-lg"></i>
      </button>`;
    }
    else{
      downloadClassStyle = ' ml-auto';
    }
    var downloadButton = `<a class="btn btn-primary mt-3`+downloadClassStyle+`" href="/workorders/sendMeDoc?filename=` + workOrderFiles[i].path_name + `"><i class="fas fa-download fa-lg"></i></a>`;
    if(workOrderFiles[i].active == false)
      downloadButton = `<a class="btn btn-primary mt-3 disabled`+downloadClassStyle+`" href=""><i class="fas fa-download fa-lg"></i></a>`;
    var fullDate = new Date(workOrderFiles[i].date).toLocaleString("sl");
    var date = fullDate.substring(0,11);
		var time = fullDate.substring(12,17);
		var fileSrc = '/file'+workOrderFiles[i].type+'.png'
		var fileWidth = '70';
    var element = `<div class="list-group-item" id="file`+workOrderFiles[i].id+`">
      <div class="row">
        <div class="col-sm-3 justify-content-center d-flex">
          <div class="preview-icon">
            <img class="img-fluid bg-light image-icon w-70px" src=` + fileSrc + ` alt="ikona" />
          </div>
        </div>
        <div class="col-sm-7">
          <label class="row mt-2">` + workOrderFiles[i].original_name + `</label>
          <label class="row mt-2 small">Naloženo: ` + date + ` ob ` + time + ` Avtor: ` + workOrderFiles[i].user_name + `</label>
        </div>
        <div class="col-sm-2">
          <div class="d-flex">
            ` + deleteButton + `
            ` + downloadButton + `
          </div>
        </div>
      </div>
    </div>`;
    $('#fileList').append(element);
    $('#file' +workOrderFiles[i].id).find('.btn-danger').on('click', {id: workOrderFiles[i].id}, onEventDeleteFile);
  }
	$('#workOrderInfo').toggle('collapse');
  $('#workOrderFiles').toggle('collapse');
  setTimeout(()=>{
    getFileThumbnails(workOrderFiles);
  },500)
}
function getFileThumbnails(allFiles, isTask) {
  for(var i = 0, l = allFiles.length; i < l; i++){
    if(allFiles[i].type == 'IMG' || allFiles[i].type == 'PDF'){
      var fileId = allFiles[i].id, filePath = allFiles[i].path_name, fileType = allFiles[i].type;
      var tmpIsTask = isTask ? true : false;
      $.get('/workorders/files/thumbnail', {fileId, filePath, fileType}, function(data){
        if (data.success){
          //tmpFileInfo = data.data;
          if(data.type == 'IMG'){
            if (isTask)
              $('#taskFile'+data.id).find('img').prop('src', data.data);
            else
              $('#file'+data.id).find('img').prop('src', data.data);
            var tmpElem = `<div class="overlay-zoom">
              <div class="icon-overlay">
                <i class="fas fa-search-plus"></i>
              </div>
            </div>`;
            var tmpTag = `<div class="overlay-tag"">
              <img class="img-fluid" src="/imgTag.png" />
            </div>`
            if (isTask){
              $('#taskFile' + data.id).find('div .preview-icon').append(tmpElem);
              $('#taskFile' + data.id).find('div .preview-icon').append(tmpTag);
              $('#taskFile' + data.id).find('.overlay-zoom').on('click', {id: data.id, filePath: data.filePath}, onEventGetIMGData);
            }
            else{
              $('#file' + data.id).find('div .preview-icon').append(tmpElem);
              $('#file' + data.id).find('div .preview-icon').append(tmpTag);
              $('#file' + data.id).find('.overlay-zoom').on('click', {id: data.id, filePath: data.filePath}, onEventGetIMGData);
            }
          }
          else if(data.type == 'PDF'){
            //tmpFileInfo = data;
            //tmpPDFThumbnailData.push({fileId: data.id, fileBase64: data.data});
            convertPDFCanvasToImg(data.data, data.id, tmpIsTask);
            var tmpElem = `<div class="overlay-zoom">
              <div class="icon-overlay">
                <i class="fas fa-search-plus"></i>
              </div>
            </div>`;
            var tmpTag = `<div class="overlay-tag"">
              <img class="img-fluid" src="/pdfTag.png" />
            </div>`;
            if (isTask){
              $('#taskFile' + data.id).find('div .preview-icon').append(tmpElem);
              $('#taskFile' + data.id).find('div .preview-icon').append(tmpTag);
              $('#taskFile' + data.id).find('.overlay-zoom').on('click', {id: data.id, filePath: data.filePath}, onEventGetPDFData);
            }
            else{
              $('#file' + data.id).find('div .preview-icon').append(tmpElem);
              $('#file' + data.id).find('div .preview-icon').append(tmpTag);
              $('#file' + data.id).find('.overlay-zoom').on('click', {id: data.id, filePath: data.filePath}, onEventGetPDFData);
            }
          }
        }
        else{
          if (data.type == 'IMG'){
            if (isTask)
              $('#taskFile'+data.id).find('img').prop('src', '/fileNoIMG.png');
            else
              $('#file'+data.id).find('img').prop('src', '/fileNoIMG.png');
          }
          else if(data.type == 'PDF'){
            if (isTask)
              $('#taskFile'+data.id).find('img').prop('src', '/fileNoPDF.png');
            else
              $('#file'+data.id).find('img').prop('src', '/fileNoPDF.png');
          }
          //console.log("error while getting thumnail or file does not exist");
        }
      });
    }
  }
}
function convertPDFCanvasToImg(pdfData, fileId, isTask) {
  var loadingTask = pdfjsLib.getDocument({ data: atob(pdfData), });
  loadingTask.promise.then(function(pdf) {
    var canvasdiv = document.getElementById('the-canvas');
    var totalPages = pdf.numPages
    var data = [];
      
    pdf.getPage(1).then(function(page) {
      var scale = 1.5;
      var viewport = page.getViewport({ scale: scale });

      var canvas = document.createElement('canvas');
      canvasdiv.appendChild(canvas);

      // Prepare canvas using PDF page dimensions
      var context = canvas.getContext('2d');
      canvas.height = viewport.height;
      canvas.width = viewport.width;

      // Render PDF page into canvas context
      var renderContext = { canvasContext: context, viewport: viewport };

      var renderTask = page.render(renderContext);
      renderTask.promise.then(function() {
        data.push(canvas.toDataURL('image/png'))
        //tmpData.push(data);
        if (isTask)
          $('#taskFile'+fileId).find('img.image-icon').prop('src', data[0]);
        else
          $('#file'+fileId).find('img.image-icon').prop('src', data[0]);
        //$('#file'+fileId).find('img.image-icon').css('width', '70px');
        //console.log(data.length + ' page(s) loaded in data')
      });
    })
  })
}
function onEventGetPDFData(event) {
  getPDFData(event.data.id, event.data.filePath);
}
function getPDFData(id, filePath) {
  var tmp = `<embed src="/workorders/files/resources/?fileName=` + filePath + `" class="pdfobject embeded-pdf" type="application/pdf">`
  var height = $('#file'+id).find('img').css('height').match(/[\d\.]+/);
  var width = $('#file'+id).find('img').css('width').match(/[\d\.]+/);
  if (Math.ceil(parseFloat(height)/parseFloat(width) * 100)/100 == 1.42){
    $("#pdfPreviewContainer").removeClass('document-content');
    $("#pdfPreviewContainer").addClass('document-content-vertical-pdf');
  }
  else {
    $("#pdfPreviewContainer").removeClass('document-content-vertical-pdf');
    $("#pdfPreviewContainer").addClass('document-content');
  }
  $("#pdfPreviewContainer").empty();
  $("#imgPreviewContainer").empty();
  $('#imgPreviewContainer').css('display', 'none');
  $('#pdfPreviewContainer').css('display', '');
  $('#modalPDFPreview').modal('toggle');
  $("#pdfPreviewContainer").append(tmp);
}
function onEventGetIMGData(event) {
  getIMGData(event.data.id, event.data.filePath);
}
function getIMGData(id, filePath) {
  var tmp = `<img class="modal-img img-fluid" src="/workorders/files/resources/?fileName=` + filePath + `">`
  $("#pdfPreviewContainer").empty();
  $("#imgPreviewContainer").empty();
  $('#imgPreviewContainer').css('display', '');
  $('#pdfPreviewContainer').css('display', 'none');
  $('#modalPDFPreview').modal('toggle');
  $("#imgPreviewContainer").append(tmp);
}
function onEventDeleteFile(event) {
  deleteFile(event.data.id);
}
function deleteFile(id) {
  debugger;
  savedFileId = id;
  $('#modalDeleteFile').modal('toggle');
}
function deleteFileConf() {
  //delete file with id that is same as savedFileId --> send savedFileId to complaints/file DELETE
  debugger;
  let data = {fileId:savedFileId};
  $.ajax({
    type: 'DELETE',
    url: '/workorders/files',
    contentType: 'application/json',
    data: JSON.stringify(data), // access in body
  }).done(function (resp) {
    //console.log('SUCCESS');
    debugger;
    if (resp.success){
      //file was successfully deleted and marked as deleted, remove all files listing in variables and DOM elements
      workOrderFiles = workOrderFiles.filter(pf => pf.id != resp.file.id);
      $('#file'+resp.file.id).remove();
      $('#modalDeleteFile').modal('toggle');
      addChangeSystem(3,23,null,null,null,null,null,null,null,null,null,workOrderId,null,null,null,null,null,resp.file.id);
    }
    else{
      $('#modalDeleteFile').modal('toggle');
      $('#modalError').modal('toggle');
    }
  }).fail(function (resp) {
    //console.log('FAIL');
    //debugger;
    $('#modalError').modal('toggle');
  }).always(function (resp) {
    //console.log('ALWAYS');
    //debugger;
  });
}
let workOrderFiles, savedFileId;