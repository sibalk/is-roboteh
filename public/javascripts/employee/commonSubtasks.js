$(function(){
  //SUBTASKS
  $('#subtasks-form').submit(function(e){
    e.preventDefault();
    $form = $(this);
    var newSubtask = subtaskInput.value;
    //debugger;
    $.post('/projects/subtask/add', { taskId, newSubtask }, function (resp) {
      //debugger;
      var checkedCB = '';
      var popoverText = "";
      var newId = resp.data.id;
      var deleteButton = ``;
      if(loggedUser.role == 'admin' || loggedUser.role.substring(0,5) == 'vodja'){
        deleteButton = `<button class="btn btn-danger task-button mr-2 mt-1" id="deleteSubtask`+newId+`">
          <i class="fas fa-trash fa-lg"></i>
        </button>`;
      }
      var listElement = `<row class="d-flex list-group-item" id=` + newId + `>
            <label class="p-1 mr-auto w-70">`+ newSubtask + `</label>
            <div class="custom-control custom-checkbox mt-2">
              <input class="custom-control-input" id="customCheck`+ newId + `" type="checkbox" ` + checkedCB + ` />
              <label class="custom-control-label" for="customCheck`+ newId + `"> </label>
            </div>
            <button class="invisible-button mr-1 mt-2">
              <i class="far fa-comments fa-lg"></i>
            </button>
            `+deleteButton+`
          </row>`
      $('#subtaskList').append(listElement);
      $('#deleteSubtask'+newId).on('click', {id: newId}, onClickDeleteSubtask);
      $('#customCheck'+newId).on('change', toggleSubtaskCheckbox);
      $('#'+newId).find('.invisible-button').on('click', {id: newId, type: 2}, onClickOpenMessagesModal);
      $('[data-toggle="popover"]').popover({ html: true });
      allSubtasks.push({ id: newId, id_task: taskId, name: newSubtask, note: null, completed: false });
      var numberOfCompleted = 0;
      for (var i = 0; i < allSubtasks.length; i++) {
        if (allSubtasks[i].completed)
          numberOfCompleted++;
      }
      var completion = Math.round((numberOfCompleted / allSubtasks.length) * 100)
      //console.log(completion);
      taskId = task.id;
      //$('#activeTaskProgress')[0].style.width = completion + '%';
      $('#activeTaskProgress').removeClass();
      completion == 100 ? $('#activeTaskProgress').addClass('progress-bar bg-success w-'+completion): $('#activeTaskProgress').addClass('progress-bar w-'+completion);
      task.completion = completion;
      //update timeline
      updateTimeline(completion);
      //fix colors for active task
      colorMyActiveTask();
      $.post('/projects/task/completion', { taskId, completion }, function (resp) {
        //alert(resp);
        //console.log("post response " + resp);
        if(task.id_project)
          fixCompletion();
      })
      $('#subtaskInput').val("");
      $("#modalSubtaskForm").toggle();
      //ADDING NEW CHANGE TO DB
      if(task.id_project)
        //addNewProjectChange(3,1,task.id_project,taskId,newId);
        addChangeSystem(1,3,task.id_project,taskId,newId);
      else
        //addNewServisChange(10,1,task.id_subscriber,taskId,newId);
        addChangeSystem(1,10,null,taskId,newId,null,null,null,task.id_subscriber);
    })
  })
  //cal event functions
  $('#taskButtonSubtask').on('click', getSubtasks);
  $('#btnOverrideSubtask').on('click', subtaskOverride);
})
//update/show subtasks of active task
function getSubtasks(){
  $.get('/projects/subtasks', {taskId}, function(data){
    //debugger;
    allSubtasks = data.data;
    allInactiveSubtasks = [];
    
    $('#subtaskList').empty();
    for(var i = 0; i<allSubtasks.length; i++){
      var checkedCB = '';
      if(allSubtasks[i].completed)
        checkedCB = 'checked=""';
      var inactiveClass = "";
      var deleteElement = '';
      var deleteActivity = '';
      var deleteIcon = 'trash';
      if(allSubtasks[i].active == false){
        deleteActivity = ',1';
        deleteIcon = 'trash-restore'
        inactiveClass = 'bg-dark';
      }
      //messages icon
      let commentsIcon = '<i class="far fa-comments fa-lg"></i>';
      if(allSubtasks[i].notes_count && allSubtasks[i].notes_count > 0)
        commentsIcon = '<i class="fas fa-comments fa-lg"></i>';
      if(loggedUser.role == 'admin' || loggedUser.role.substring(0,5) == 'vodja'){
        var listElement = `<div class="list-group-item `+inactiveClass+`" id=` + allSubtasks[i].id + `>
          <row class="d-flex">
            <label class="p-1 mr-auto w-70">`+ allSubtasks[i].name + `</label>
            <div class="custom-control custom-checkbox mt-2">
              <input class="custom-control-input" id="customCheck`+ allSubtasks[i].id + `" type="checkbox" ` + checkedCB + `/>
                <label class="custom-control-label" for="customCheck`+ allSubtasks[i].id + `"> </label>
            </div>
            <button class="invisible-button mr-1 mt-2">` + commentsIcon + `</button>
            <button class="btn btn-danger task-button mr-2 mt-1" id="deleteSubtask`+allSubtasks[i].id+`">
              <i class="fas fa-`+deleteIcon+` fa-lg"></i>
            </button>
          </row>
          <row class="d-flex" id="subtaskBadges"></row>
        </div>`;
      }
      else
        var listElement = `<div class="list-group-item" id=` + allSubtasks[i].id + `>
          <row class="d-flex">
            <label class="p-1 mr-auto w-70">`+ allSubtasks[i].name + `</label>
            <div class="custom-control custom-checkbox mt-2">
              <input class="custom-control-input" id="customCheck`+ allSubtasks[i].id + `" type="checkbox" ` + checkedCB + `/>
                <label class="custom-control-label" for="customCheck`+ allSubtasks[i].id + `"> </label>
            </div>
            <button class="invisible-button mr-1 mt-2">` + commentsIcon + `</button>
          </row>
          <row class="d-flex" id="subtaskBadges"></row>
        </div>`;
      if(allSubtasks[i].active == false){
        if(loggedUser.role == 'admin'){
          $('#subtaskList').append(listElement);
          $('#deleteSubtask'+allSubtasks[i].id).on('click', {id: allSubtasks[i].id, activity: deleteActivity}, onClickDeleteSubtask);
          $('#'+allSubtasks[i].id).find('.invisible-button').on('click', {id: allSubtasks[i].id, type: 2}, onClickOpenMessagesModal);
          $('#customCheck' + allSubtasks[i].id).on('change', toggleSubtaskCheckbox);
        }
        var id = allSubtasks[i].id;
        allInactiveSubtasks.push(allSubtasks[i]);
        allSubtasks = allSubtasks.filter(s => s.id != id);
        i--;
      }
      else{
        $('#subtaskList').append(listElement);
        $('#deleteSubtask'+allSubtasks[i].id).on('click', {id: allSubtasks[i].id, activity: deleteActivity}, onClickDeleteSubtask);
        $('#'+allSubtasks[i].id).find('.invisible-button').on('click', {id: allSubtasks[i].id, type: 2}, onClickOpenMessagesModal);
        $('#customCheck' + allSubtasks[i].id).on('change', toggleSubtaskCheckbox);
      }
      $('[data-toggle="popover"]').popover({ html: true });
    }
    $('#controlTask').empty();
    var isCompleted = '';
    if(task.completion == '100'){
      isCompleted = 'checked=""';
    }
    //messages icon
    let commentsIcon = '<i class="far fa-comments fa-lg"></i>';
    if(task.notes_count && task.notes_count > 0)
      commentsIcon = '<i class="fas fa-comments fa-lg"></i>';
    var controlElement = `<div class="custom-control custom-checkbox mt-2" id="taskCheckbox">
    <input class="custom-control-input" id="customTaskCheck" type="checkbox" `+ isCompleted + ` ` + ` />
    <label class="custom-control-label" for="customTaskCheck"> </label>
    </div>
    <button class="invisible-button mr-1 mt-2">` + commentsIcon + `</button>`;
    $('#controlTask').append(controlElement);
    $('#customTaskCheck').on('change', toggleTaskCheckbox);
    $('#myActiveTask').find('.invisible-button').on('click', {id: taskId, type: 1}, onClickOpenMessagesModal);
    $('[data-toggle="popover"]').popover({ html: true });
    if(allSubtasks.length != '0'){
      $('#customTaskCheck').prop('disabled',true);
      $('#taskCheckbox').hide();
    }
    else{
      $('#customTaskCheck').prop('disabled',false);
      $('#taskCheckbox').show();
    }
    $('#collapseFiles').collapse('hide');
    $('#collapseAbsences').collapse('hide');
    $('#collapseSubtasks').collapse('show');
  })
}
//check/uncheck subtask
function toggleSubtaskCheckbox(element){
  //element.checked = !element.checked;
  //debugger;
  subtaskId = parseInt(this.id.substring(11));
  var completed = this.checked;
  if((completed == true) && ($('#taskWorkorders').attr('data-content') == 'brez delovnega naloga') && (allSubtasks.filter(s => s.completed == false).length <= 1) && !task.id_project){
    //console.log('warning! no work order! add new work order to service');
    overrideSubtaskId = subtaskId;
    $('#customCheck'+subtaskId).prop('checked', false);
    $('#btnOverrideTask').hide();
    $('#btnOverrideSubtask').show();
    $('#modalWarningWO').modal('toggle');
  }
  else{
    $.post('/projects/subtask', {subtaskId, completed}, function(data){
      var oldCompletion = task.completion;
      //debugger;
      //calculate completion via subtask
      var temp = allSubtasks.find(s => s.id == subtaskId)
      if(temp){
        temp.completed = completed;
        var numberOfCompleted = 0;
        for(var i=0; i<allSubtasks.length; i++){
          if(allSubtasks[i].completed)
          numberOfCompleted++;
        }
        var completion = Math.round((numberOfCompleted/allSubtasks.length)*100)
        if(numberOfCompleted == '0' && allSubtasks.length == '0')
          completion = 0;
        //console.log(completion);
        taskId = task.id;
        $.post('/projects/task/completion', {taskId, completion}, function(resp){
          if(parseInt(task.completion) != 100 && completion == 100){
            addFinishedDate();
          }
          //$('#activeTaskProgress')[0].style.width = completion+'%';
          $('#activeTaskProgress').removeClass();
          completion == 100 ? $('#activeTaskProgress').addClass('progress-bar bg-success w-'+completion): $('#activeTaskProgress').addClass('progress-bar w-'+completion);
          task.completion = completion;
          //update timeline
          updateTimeline(completion);
          //fix colors for active task
          colorMyActiveTask();
          if(completion == 100 && oldCompletion != completion){
            if(!task.id_project)
              addChangeSystem(4,9,null,taskId,null,null,null,null,task.id_subscriber);
            else
              addChangeSystem(4,2,projectId,taskId);
          }
          else{
            if(oldCompletion == 100){
              if(!task.id_project)
                addChangeSystem(5,9,null,taskId,null,null,null,null,task.id_subscriber);
              else
                addChangeSystem(5,2,projectId,taskId);
            }
          }
          //alert(resp);
          //console.log("post response " + resp);
          if(task.id_project)
            fixCompletion();
          //addChange(0,subtaskId);
        })
        //debugger;
        //ADDING NEW CHANGE TO DB
        if(task.id_project){
          //project change
          if(completed)
            //addNewProjectChange(3,4,task.id_project,taskId,subtaskId);
            addChangeSystem(4,3,task.id_project,taskId,subtaskId);
          else
            //addNewProjectChange(3,5,task.id_project,taskId,subtaskId);
            addChangeSystem(5,3,task.id_project,taskId,subtaskId);
        }
        else{
          //servis change
          if(completed)
            //addNewServisChange(10,4,task.id_subscriber,taskId,subtaskId);
            addChangeSystem(4,10,null,taskId,subtaskId,null,null,null,task.id_subscriber);
          else
            //addNewServisChange(10,5,task.id_subscriber,taskId,subtaskId);
            addChangeSystem(5,10,null,taskId,subtaskId,null,null,null,task.id_subscriber);
        }
      }
      else{
        //its deleted subtask, only fix completion in table
        var tmp = allInactiveSubtasks.find(s => s.id == subtaskId);
        if(tmp){
          tmp.completed = completed;
        }
      }
    })
  }
}
function subtaskOverride(){
  var subtaskId = overrideSubtaskId;
  var completed = true;
  $('#customCheck'+subtaskId).prop('checked', true);
  $.post('/projects/subtask', {subtaskId, completed}, function(data){
    var oldCompletion = task.completion;
    //debugger;
    //calculate completion via subtask
    $('#modalWarningWO').modal('toggle');
    var temp = allSubtasks.find(s => s.id == subtaskId)
    if(temp){
      temp.completed = completed;
      var numberOfCompleted = 0;
      for(var i=0; i<allSubtasks.length; i++){
        if(allSubtasks[i].completed)
        numberOfCompleted++;
      }
      var completion = Math.round((numberOfCompleted/allSubtasks.length)*100)
      if(numberOfCompleted == '0' && allSubtasks.length == '0')
        completion = 0;
      //console.log(completion);
      taskId = task.id;
      $.post('/projects/task/completion', {taskId, completion}, function(resp){
        if(parseInt(task.completion) != 100 && completion == 100){
          addFinishedDate();
        }
        //$('#activeTaskProgress')[0].style.width = completion+'%';
        $('#activeTaskProgress').removeClass();
        completion == 100 ? $('#activeTaskProgress').addClass('progress-bar bg-success w-'+completion): $('#activeTaskProgress').addClass('progress-bar w-'+completion);
        task.completion = completion;
        //update timeline
        updateTimeline(completion);
        //fix colors for active task
        colorMyActiveTask();
        if(completion == 100 && oldCompletion != completion){
          if(!task.id_project)
            addChangeSystem(4,9,null,taskId,null,null,null,null,task.id_subscriber);
          else
            addChangeSystem(4,2,projectId,taskId);
        }
        //alert(resp);
        //console.log("post response " + resp);
        if(task.id_project)
          fixCompletion();
        //addChange(0,subtaskId);
      })
      //debugger;
      //ADDING NEW CHANGE TO DB
      //addNewServisChange(10,4,task.id_subscriber,taskId,subtaskId);
      addChangeSystem(4,10,null,taskId,subtaskId,null,null,null,task.id_subscriber);
    }
    else{
      //its deleted subtask, only fix completion in table
      var tmp = allInactiveSubtasks.find(s => s.id == subtaskId);
      if(tmp){
        tmp.completed = completed;
      }
    }
  })
}
function onClickOpenSubtaskModal(event) {
  openSubtaskModal(event.data.id);
}
function openSubtaskModal(id){
  taskId = id;
  $("#modalSubtasksForm").modal();
}
function onClickDeleteSubtask(event) {
  debugger;
  event.data.activity ? deleteSubtask(event.data.id, event.data.activity) : deleteSubtask(event.data.id);
}
function deleteSubtask(id, activity){
  //var taskId = taskId;
  var active = false;
  if(activity && activity == 1)
    active = true;
  //brisanje
  //debugger;
  $.post('/projects/subtask/delete', {id, active}, function(data){
    //debugger
    $('#deleteSubtask'+id).off('click');
    if(loggedUser.role == 'admin'){
      if(activity && activity == 1){
        //resurection
        $('#subtaskList').find('#'+id).removeClass('bg-dark');
        $('#deleteSubtask'+id).find('i').removeClass('fa-trash-restore');
        $('#deleteSubtask'+id).find('i').addClass('fa-trash');
        $('#deleteSubtask'+id).on('click', {id}, onClickDeleteSubtask);
        //$('#deleteSubtask'+id).attr('onclick', 'deleteSubtask('+id+')');
        //$('#deleteSubtask'+id).find('img').attr('src', '/delete.png');
        var tmp = allInactiveSubtasks.find(s => s.id == id);
        allSubtasks.push(tmp);
        allInactiveSubtasks = allInactiveSubtasks.filter(s => s.id != id);
        //ADDING NEW CHANGE TO DB
        if(task.id_project)
          //addNewProjectChange(3,6,task.id_project,task.id,id);
          addChangeSystem(6,3,task.id_project,task.id,id);
        else
          //addNewServisChange(10,6,task.id_subscriber,task.id,id);
          addChangeSystem(6,10,null,task.id,id,null,null,null,task.id_subscriber);
      }
      else{
        //deleting
        $('#subtaskList').find('#'+id).addClass('bg-dark');
        $('#deleteSubtask'+id).find('i').removeClass('fa-trash');
        $('#deleteSubtask'+id).find('i').addClass('fa-trash-restore');
        $('#deleteSubtask'+id).on('click', {id, activity:1}, onClickDeleteSubtask);
        //$('#deleteSubtask'+id).attr('onclick', 'deleteSubtask('+id+',1)');
        //$('#deleteSubtask'+id).find('img').attr('src', '/undelete1.png');
        var tmp = allSubtasks.find(s => s.id == id);
        allInactiveSubtasks.push(tmp);
        allSubtasks = allSubtasks.filter(s => s.id != id);
        //ADDING NEW CHANGE TO DB
        if(task.id_project)
          //addNewProjectChange(3,3,task.id_project,task.id,id);
          addChangeSystem(3,3,task.id_project,task.id,id);
        else
          //addNewServisChange(10,3,task.id_subscriber,task.id,id);
          addChangeSystem(3,10,null,task.id,id,null,null,null,task.id_subscriber);
      }
    }
    else{
      $('#subtaskList').find('#'+id).remove();
      var tmp = allSubtasks.find(s => s.id == id);
      allInactiveSubtasks.push(tmp);
      allSubtasks = allSubtasks.filter(s => s.id != id);
      //ADDING NEW CHANGE TO DB
      if(task.id_project)
        //addNewProjectChange(3,3,task.id_project,task.id,id);
        addChangeSystem(3,3,task.id_project,task.id,id);
      else
        //addNewServisChange(10,3,task.id_subscriber,task.id,id);
        addChangeSystem(3,10,null,task.id,id,null,null,null,task.id_subscriber);
    }
    var numberOfCompleted = 0;
    for(var i=0; i<allSubtasks.length; i++){
      if(allSubtasks[i].completed)
      numberOfCompleted++;
    }
    var completion = Math.round((numberOfCompleted/allSubtasks.length)*100)
    if(numberOfCompleted == 0 && allSubtasks.length == 0)
      completion = 0;
    //console.log(completion);
    taskId = task.id;
    //$('#activeTaskProgress')[0].style.width = completion+'%';
    $('#activeTaskProgress').removeClass();
    completion == 100 ? $('#activeTaskProgress').addClass('progress-bar bg-success w-'+completion): $('#activeTaskProgress').addClass('progress-bar w-'+completion);
    task.completion = completion;
    //update timeline
    updateTimeline(completion);
    //fix colors for active task
    colorMyActiveTask();
    $.post('/projects/task/completion', {taskId, completion}, function(resp){
      //alert(resp);
      //console.log("post response " + resp);
      if(task.id_project)
        fixCompletion();
    })
  })
}