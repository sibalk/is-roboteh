$(document).ready(function() {
  //debugger
  
  //$('.modal-dialog').draggable({
  //  handle: ".modal-header"
  //});
  var roles
  loggedUser = {name:$('#loggedUser').html().split(" (")[0].split(" ")[0], surname:$('#loggedUser').html().split(" (")[0].split(" ")[1], role:$('#loggedUser').html().split(" (")[1].split(")")[0].toLowerCase()};
  var username = $('#user_username').val();
  $('#user_username').on('change', onUsernameChange);
  
  $('#user_username_edit').on('change', onUsernameEditChange);

  function onUsernameChange(){
    username = $('#user_username').val();
    username = username.toLowerCase();
    //debugger;
    var conflict = usernames.find(u => u.username.toLowerCase() === username)
    if(conflict){
      $('#user_username').removeClass("is-valid").addClass("is-invalid");
      $('#btnAddEmployee').prop("disabled", true).removeClass("ui-state-enabled").addClass("ui-state-disabled");
    }
    else{
      $('#user_username').removeClass("is-invalid").addClass("is-valid");
      $('#btnAddEmployee').prop("disabled", false).removeClass("ui-state-disabled").addClass("ui-state-enabled");
    }
  }
  function onUsernameEditChange(){
    var newUsername = $('#user_username_edit').val();
    newUsername = newUsername.toLowerCase();
    debugger;
    var conflict = usernames.find(u => u.username.toLowerCase() === newUsername)
    if(conflict && newUsername != usernameEdit){
      $('#user_username_edit').removeClass("is-valid").addClass("is-invalid");
      $('#btnEditEmployee').prop("disabled", true).removeClass("ui-state-enabled").addClass("ui-state-disabled");
    }
    else{
      $('#user_username_edit').removeClass("is-invalid").addClass("is-valid");
      $('#btnEditEmployee').prop("disabled", false).removeClass("ui-state-disabled").addClass("ui-state-enabled");
    }
  }

  $('.btnadd').click(function(){
    //get roles
    $.get( "/employees/roles", function( data ) {
      roles = data.data;

      $(".select2-role").select2({
        data: roles,
        tags: false,
        dropdownParent: $('#modalAddEmployeeForm'),
      });
      // add user list to supervisor and filter only active users and users without containing 'info '
      var users = dataGet.filter(u => u.active && u.role.toLowerCase().substring(0,4) != 'info')
      var usersList = users.map(u => {return {id: u.id, text: u.name + ' ' + u.surname}})
      $("#user_supervisor").select2({
        data: usersList,
        tags: false,
        multiple: true,
        dropdownParent: $('#modalAddEmployeeForm'),
      });
      $('#user_role').val(4).trigger('change')
      $.get( "/employees/usernames", function( data ) {
        usernames = data.data;
        //console.log(usernames);
        debugger;
        
        $("#modalAddEmployeeForm").modal();
      });
    });
  })
  $('#addEmployeeform').submit(function(e)
  {
    e.preventDefault();
    $form = $(this);
    var userUsername = user_username.value;
    var userName = user_name.value;
    var userSurname = user_surname.value;
    var userRole = user_role.value;
    var userPassword = user_password.value;
    var userPasswordAgain = user_password_again.value;
    var supervisor = $('#user_supervisor').val().toString();
    var workMail = $('#user_work_mail').val();

    debugger;
    $.post('/employees/create', {userUsername, userName, userSurname, userRole, userPassword, userPasswordAgain, supervisor, workMail}, function(resp){
      if(resp.success){
        debugger;
        //add new user in table
        var selectedRole = roles.find(r => r.id === parseInt(userRole))
        tableEmployees.row.add({
          id: resp.id.id, 
          name: userName, 
          surname: userSurname, 
          role: selectedRole.text,
          active: true,
          work_mail: workMail,
          supervisors_ids: supervisor
        }).draw(false)
        //close modal
        $('#modalAddEmployeeForm').modal('toggle');
        $('#user_username').val("");
        $('#user_name').val("");
        $('#user_surname').val("");
        $('#user_password').val("");
        $('#user_password_again').val("");
        $('#user_work_mail').val("");
        $('#user_supervisor').val(null).trigger('change');
        $('#user_password_again').removeClass("is-valid");
        $('#user_username').removeClass("is-valid")
        //ADDING CHANGE TO DB
        addChangeSystem(1,14,null,null,null,null,null,null,null,null,null,null,null,null,null,null,resp.id.id);
      }
      else{
        //$('#messageAdd').append(resp.data);
        $('#modalError').modal('toggle');
        debugger;
      }
      //add user in list
      //debugger
      //toggle modal
    })
    
  });
  $('#editEmployeeform').submit(function(e){
    e.preventDefault();
    $form = $(this);
    var newName = user_name_edit.value;
    var newSurname = user_surname_edit.value;
    var newUsername = user_username_edit.value;
    var newWorkMail = user_work_mail_edit.value;
    var newSupervisors = $('#user_supervisor_edit').val().toString();
    var newActive
    if(typeof activeEdit === 'undefined')
      newActive = true;
    else
      newActive = activeEdit.checked;
    debugger;
    $.post('/employees/update', {userId, newName, newSurname, newActive, newUsername, newSupervisors, newWorkMail}, function(resp){
      if(resp.success){
        var temp = tableEmployees.row('#'+userId).data()
        temp.name = newName;
        temp.surname = newSurname;
        temp.username = newUsername;
        temp.active = newActive;
        temp.work_mail = newWorkMail;
        temp.supervisors_ids = newSupervisors;
        debugger;
        tableEmployees.row('#'+userId).data(temp).invalidate().draw(false);
        $('#modalEditEmployeeForm').modal('toggle');
        //ADDING CHANGE TO DB
        addChangeSystem(2,14,null,null,null,null,null,null,null,null,null,null,null,null,null,null,userId);
      }
      else{
        $('#modalError').modal('toggle');
      }
    })
  })
  $('#addRoleform').submit(function(e){
    e.preventDefault();
    $form = $(this);
    var role = addNewRole.value;
    debugger;
    $.post('/employees/addrole', {role, newRoleType}, function(resp){

      debugger;
      if(resp.success){
        $('#modalAddRoleForm').modal('toggle');
        if(newRoleType == 0)
          //ADDING CHANGE TO DB
          addChangeSystem(1,15,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,resp.roleId);
        else
          //ADDING CHANGE TO DB
          addChangeSystem(1,16,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,resp.workRoleId);
      }
      else{
        //alert("Pri kreiranju nove vloge je prišlo do napake.");
        $('#modalAddRoleForm').modal('toggle');
        $('#modalError').modal('toggle');
      }
    })
  })
  activeUser = 1;
  //Get users and display in dataTable
  $.get( "/employees/all", {activeUser}, function( data ) {
    //console.log(data)
    dataGet = data.data;
    //console.log(dataGet[2]);
    //debugger;
    tableEmployees = $('#employeesTable').DataTable( {
      data: dataGet,
      columns: [
          { title: "id", data: "id" },
          { data: "role",
          render: function ( data, type, row ) {
            if(data.toLowerCase() == 'delavec')
              return '<img class="img-fluid" src="/worker64.png" alt="ikona" />';
            else if(data.toLowerCase() == 'električar')
              return '<img class="img-fluid" src="/electrican64.png" alt="ikona" />';
            else if(data.toLowerCase() == 'strojnik' || data.toLowerCase() == 'strojni monter')
              return '<img class="img-fluid" src="/mechanic64.png" alt="ikona" />';
            else if(data.toLowerCase() == 'programer plc-jev')
              return '<img class="img-fluid" src="/plc64.png" alt="ikona" />';
            else if(data.toLowerCase() == 'programer robotov')
              return '<img class="img-fluid" src="/robot64.png" alt="ikona" />';
            else if(data.toLowerCase() == 'konstruktor' || data.toLowerCase() == 'konstrukter')
              return '<img class="img-fluid" src="/builder64.png" alt="ikona" />';
            else if(data.toLowerCase() == 'cnc' || data.toLowerCase() == 'cnc operater')
              return '<img class="img-fluid" src="/cnc64.png" alt="ikona" />';
            else if(data.toLowerCase() == 'vodja' || data.toLowerCase() == 'vodja projektov' || data.toLowerCase() == 'vodja projekta')
              return '<img class="img-fluid" src="/manager64.png" alt="ikona" />';
            else if(data.toLowerCase() == 'vodja strojnikov')
              return '<img class="img-fluid" src="/managerMech64.png" alt="ikona" />';
            else if(data.toLowerCase() == 'vodja CNC obdelave')
              return '<img class="img-fluid" src="/managerCNC64.png" alt="ikona" />';
            else if(data.toLowerCase() == 'vodja električarjev')
              return '<img class="img-fluid" src="/managerElec64.png" alt="ikona" />';
            else if(data.toLowerCase() == 'vodja programerjev')
              return '<img class="img-fluid" src="/managerPLC64.png" alt="ikona" />';
            else if(data.toLowerCase() == 'admin')
              return '<img class="img-fluid" src="/admin64.png" alt="ikona" />';
            else if(data.toLowerCase() == 'tajnik' || data.toLowerCase() == 'komercialist' || data.toLowerCase() == 'komercijalist' || data.toLowerCase() == 'komerciala' || data.toLowerCase() == 'računovodstvo')
              return '<img class="img-fluid" src="/secratery64.png" alt="ikona" />';
            else if(data.toLowerCase() == 'varilec')
              return '<img class="img-fluid" src="/welder02.png" alt="ikona" />';
            else if(data.toLowerCase() == 'student' || data.toLowerCase() == 'študent')
              return '<img class="img-fluid" src="/student03.png" alt="ikona" />';
            else if(data.toLowerCase() == 'serviser')
              return '<img class="img-fluid" src="/serviser.png" alt="ikona" />';
            else if(data.toLowerCase() == 'info')
              return '<img class="img-fluid" src="/infoUser.png" alt="ikona" />';
            else if(data.toLowerCase() == 'info elektro')
              return '<img class="img-fluid" src="/infoElec.png" alt="ikona" />';
            else if(data.toLowerCase() == 'info strojna')
              return '<img class="img-fluid" src="/infoMech.png" alt="ikona" />';
            else if(data.toLowerCase() == 'info konstrukcija')
              return '<img class="img-fluid" src="/infoBuild.png" alt="ikona" />';
            else if(data.toLowerCase() == 'info programiranje')
              return '<img class="img-fluid" src="/infoProgrammer.png" alt="ikona" />';
            else if(data.toLowerCase() == 'info plc')
              return '<img class="img-fluid" src="/infoPlc.png" alt="ikona" />';
            else if(data.toLowerCase() == 'info robot')
              return '<img class="img-fluid" src="/infoRobot.png" alt="ikona" />';
            else if(data.toLowerCase() == 'info nabava')
              return '<img class="img-fluid" src="/infoSales.png" alt="ikona" />';
            else if(data.toLowerCase() == 'info montaža')
              return '<img class="img-fluid" src="/infoAssembly.png" alt="ikona" />';
            else
              return '<img class="img-fluid" src="/worker64.png" alt="ikona" />';
          }},
          { title: "Ime", data: "name" },
          { title: "Priimek", data: "surname" },
          { title: "Vloga", data: "role" },
          { data: "id",
            render: function ( data, type, row ) {
              //debugger;
              if ((loggedUser.role == 'admin' || loggedUser.role == 'tajnik' || loggedUser.role == 'komercialist' || loggedUser.role == 'komerciala' || loggedUser.role == 'računovodstvo') && data != 1)
                return '<button class="btn btn-danger btn-delete" data-toggle="tooltip" data-original-title="Odstrani"><i class="fas fa-trash fa-lg"></i></button>';
              else
                return '<div></div>';
            }},
          { data: "id",
            render: function ( data, type, row ) {
              if (loggedUser.role == 'admin' || loggedUser.role == 'tajnik' || loggedUser.role == 'komercialist' || loggedUser.role == 'komerciala' || loggedUser.role == 'računovodstvo' || loggedUser.role.substring(0,5) == 'vodja' || (loggedUser.name == row.name && loggedUser.surname == row.surname))
                return '<a class="btn btn-programmer" href="/absences?userId='+data+'" data-toggle="tooltip" data-original-title="Odsotnosti"><i class="fas fa-clock fa-lg"></i></a>';
              else
                return '<div></div>';
            }},
          { data: "id",
            render: function ( data, type, row ) {
              if (loggedUser.role == 'admin' || loggedUser.role == 'tajnik' || loggedUser.role == 'komercialist' || loggedUser.role == 'komerciala' || loggedUser.role == 'računovodstvo')
                return '<button class="btn btn-primary btn-edit" data-toggle="tooltip" data-original-title="Uredi"><i class="fas fa-pen fa-lg"></i></button>';
              else
                return '<div></div>';
            }},
          { data: "id",
            render: function ( data, type, row ) {
              if (loggedUser.role == 'admin' || loggedUser.role == 'tajnik' || loggedUser.role == 'komercialist' || loggedUser.role == 'komerciala' || loggedUser.role == 'računovodstvo' || loggedUser.role.substring(0,5) == 'vodja' || (loggedUser.name == row.name && loggedUser.surname == row.surname))
                return '<a class="btn btn-info" href="/employees/userId?userId='+data+'" data-toggle="tooltip" data-original-title="Več"><i class="fas fa-chevron-right fa-lg"></i></a>';
              else
                return '<div></div>';
            }},
          { title: "Aktivno", data: "active"},
      ],
      "language": {
        "url": "/DataTables/Slovenian.json"
      },
      order: [],
      columnDefs: [
        {
          targets: [0,9],
          visible: false,
          searchable: false
        },
        { responsivePriority: 1, targets: 1 },
        { responsivePriority: 2, targets: -2 },
        { responsivePriority: 3, targets: 3 },
        { responsivePriority: 100001, targets: -4 },
      ],
      responsive: true,
      //info: false,
      //searching: false,
      //paging: false
      createdRow: function (row, data, index){
        //debugger
        $(row).attr('id', data.id);
        $(row).removeClass('table-dark');
        $(row).removeClass('table-info');
        if(!data.active){
          $(row).addClass('table-dark');
        }
        setTimeout(()=>{$('[data-toggle="tooltip"]').tooltip();},400)
      },
      rowCallback: function(row, data, index){
        $(row).removeClass('table-dark');
        $(row).removeClass('table-info');
        if(!data.active){
          $(row).addClass('table-dark');
        }
        setTimeout(()=>{$('[data-toggle="tooltip"]').tooltip();},400)
      },
      //dom: 'Bfrtip',
      // "dom":  "<'row'<'col-sm-12'B>>" +
      //         "<'row'<'col-sm-5'l><'col-sm-7'f>>" +
      //         "<'row'<'col-sm-12'tr>>" +
      //         "<'row'<'col-sm-5'i><'col-sm-7'p>>",
      // buttons: [
      //   {
      //     extend: 'pdfHtml5',
      //     exportOptions: {
      //         columns: [ 2, 3, 4 ]
      //     }
      //   }
      // ]
    });
  });
  setTimeout(()=>{$('[data-toggle="tooltip"]').tooltip();},400)
  $('#user_password').on('change', onPassChange);
  $('#user_password_again').on('change', onPassChange);
  setTimeout(()=>{
    $('#user_password').val('');
    $('#user_password').removeClass('is-invalid');
    $('#user_password').removeClass('is-valid');
    $('#user_username').val('');
    $('#user_username').removeClass('is-invalid');
    $('#user_username').removeClass('is-valid');
    $('#employeesTable tbody').on('click', 'button', function (event) {
      //var data = tableEmployees.row( this ).data();
      //debugger
      //alert( 'You clicked on '+data.id+'\'s row' );
      //event.target.offsetParent.id
      if ($(event.currentTarget).hasClass('btn-edit'))
        editUser(event.target.offsetParent.parentElement.id);
      else if ($(event.currentTarget).hasClass('btn-delete'))
        deleteUser(event.target.offsetParent.parentElement.id);
    });
  },700);

  //on event call functions
  $('#btnDeleteUser').on('click', deleteUserConfirm);
  $('#optionAll').on('click', {seeActive:0}, onClickChangeActive);
  $('#optionTrue').on('click', {seeActive:1}, onClickChangeActive);
  $('#optionFalse').on('click', {seeActive:2}, onClickChangeActive);
  $('#btnNewRole').on('click', {type:0}, onClickOpenNewRoleModal);
  $('#btnNewProjectRole').on('click', {type:1}, onClickOpenNewRoleModal);
});
function hasNumber(myString) {
  return /\d/.test(myString);
}
function hasLowerCase(str) {
  return (/[a-z]/.test(str));
}
function hasUpperrCase(str) {
  return (/[A-Z]/.test(str));
}
function onPassChange(){
  var password = $('#user_password').val();
  var passwordNew = $('#user_password_again').val();
  if(password){
    if(hasLowerCase(password) && hasUpperrCase(password) && hasNumber(password) && password.length >= 10){
      console.log('geslo ustreza zahtevam');
      $('#user_password').removeClass("is-invalid").addClass("is-valid");
      if(password && passwordNew){
        if(password != passwordNew){
          $('#user_password_again').removeClass("is-valid").addClass("is-invalid");
          $('#btnAddEmployee').prop("disabled", true).removeClass("ui-state-enabled").addClass("ui-state-disabled");
        }
        else{
          $('#user_password_again').removeClass("is-invalid").addClass("is-valid");
          $('#btnAddEmployee').prop("disabled", false).removeClass("ui-state-disabled").addClass("ui-state-enabled");
        }
      }
    }
    else{
      console.log('geslo ne ustreza zahtevam');
      $('#user_password').removeClass("is-valid").addClass("is-invalid");
      if(password && passwordNew){
        if(password != passwordNew){
          $('#user_password_again').removeClass("is-valid").addClass("is-invalid");
          $('#btnAddEmployee').prop("disabled", true).removeClass("ui-state-enabled").addClass("ui-state-disabled");
        }
        else{
          $('#user_password_again').removeClass("is-invalid").addClass("is-valid");
          $('#btnAddEmployee').prop("disabled", false).removeClass("ui-state-disabled").addClass("ui-state-enabled");
        }
      }
    }
  }
}
function editUser(id){
  userId = id;
  $("#activeEdit").prop('disabled', false);
  if(userId == 1)
  $("#activeEdit").prop('disabled', true);
  // add user list to supervisor edit and filter only active users and users without containing 'info '
  var users = dataGet.filter(u => u.active && u.role.toLowerCase().substring(0,4) != 'info')
  var usersList = users.map(u => {return {id: u.id, text: u.name + ' ' + u.surname}})
  $("#user_supervisor_edit").select2({
    data: usersList,
    tags: false,
    multiple: true,
    dropdownParent: $('#modalEditEmployeeForm'),
  });
  debugger
  $.get('/employees/usernames', function(data){
    debugger
    usernames = data.data;
    $.get( "/employees/user", {userId}, function( data ) {
      //console.log(usernames);
      debugger;
      
      $('#user_username_edit').val(data.data.username);
      $('#user_name_edit').val(data.data.name);
      $('#user_surname_edit').val(data.data.surname);
      $('#user_work_mail_edit').val(data.data.work_mail);
      let supervisorsIds = data.data.supervisors_ids ? data.data.supervisors_ids.split(',') : [];
      $('#user_supervisor_edit').val(supervisorsIds).trigger('change');
      usernameEdit = $('#user_username_edit').val();
      if(data.data.active){
        //debugger
        $("#activeEdit").prop('checked', 1);
      }
      else{
        //debugger
        $("#activeEdit").prop('checked', 0);
      }
      $("#modalEditEmployeeForm").modal();
    });
  })
}
function onClickDeleteUser(event) {
  
}
function deleteUser(id){
  userId = id;
  debugger
  $('#modalDeleteUser').modal('toggle');
}
function deleteUserConfirm(){
  $.post('/employees/delete', {userId}, function(resp){
    if(resp.success){
      $('#modalDeleteUser').modal('toggle');
      //odstrani iz tabele na strani
      var temp = tableEmployees.row('#'+userId+'').data();
      //console.log(temp.active);
      temp.active = false;
      tableEmployees.row('#'+userId).data(temp).invalidate().draw(false);
      addChangeSystem(3,14,null,null,null,null,null,null,null,null,null,null,null,null,null,null,userId);
    }
    else{
      $('#modalError').modal('toggle');
    }
  })

}
function onClickChangeActive(event) {
  changeActive(event.data.seeActive);
}
//CHANGE USER TO SEE ONLY ACTIVE OR ALL
function changeActive(seeActive){
  // $('#optionAll').removeClass('active');
  // $('#optionTrue').removeClass('active');
  // $('#optionFalse').removeClass('active');
  activeUser = seeActive;
  $.get( "/employees/all", {activeUser}, function( data ) {
    //console.log(data)
    dataGet = data.data;
    debugger
    //console.log(dataGet[2]);
    tableEmployees.clear();
    tableEmployees.rows.add(dataGet);
    tableEmployees.draw(true);
    // if(seeActive == 1)
    //   $('#optionTrue').addClass('active');
    // else if(seeActive == 2)
    //   $('#optionFalse').addClass('active');
    // else
    //   $('#optionAll').addClass('active');
  });
}
function onClickOpenNewRoleModal(event) {
  openNewRoleModal(event.data.type);
}
//OPEN MODAL TO ADD NEW ROLE
function openNewRoleModal(type){
  newRoleType = type;
  $("#modalAddRoleForm").modal();
}
//$('#user_password').on('change', onPassChange);
//$('#user_password_again').on('change', onPassChange);
var password = $('#user_password').val();
var passwordNew = $('#user_password_again').val();
var tableEmployees;
var loggedUser;
var activeUser;
var userId;
var usernames;
var usernameEdit;
var newRoleType;