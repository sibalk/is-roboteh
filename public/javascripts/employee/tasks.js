$(function(){
  //$('.modal-dialog').draggable({ handle: ".modal-header" });
  //$('.mypopover').popover({ html: true });
  //$('.workerspopover').popover({ html: true });
  $('[data-toggle="popover"]').popover({ html: true });
  loggedUser = { name: $('#loggedUser').html().split(" (")[0].split(" ")[0], surname: $('#loggedUser').html().split(" (")[0].split(" ")[1], role: $('#loggedUser').html().split(" (")[1].split(")")[0].toLowerCase() };
  
  /*
  $('button selector').click(function(){
    window.location.href='/projects/id?id'+task.id_project+'';
  })
  */
  //change active task to first on the list
  //debugger;
  
  if($('#taskList').children(":first").length > 0){
    if(taskId){
      $('#taskList').find('#'+taskId).toggleClass("active")
      changeActiveTask(parseInt(taskId));
    }
    else{
      $('#taskList').children(":first").toggleClass("active");
      changeActiveTask(parseInt($('#taskList').children(":first")[0].id));
    }
    selectedTaskId = urlParams.get('taskId');
    selectedSubtaskId = urlParams.get('subtaskId');
    selectedNoteId = urlParams.get('noteId');
    notificationType = urlParams.get('type');
    notificationStatus = urlParams.get('status');
    if(notificationType){
      changeActiveTask(parseInt(selectedTaskId));
      if(notificationType == '2'){
        setTimeout(()=>{
          anotherSmoothScroll('#taskList','#'+selectedTaskId); 
          flashAnimation('#'+selectedTaskId);
        })
      }
      else if(notificationType == '3'){
        setTimeout(()=>{
          anotherSmoothScroll('#taskList','#'+selectedTaskId); 
          //flashAnimation('#'+selectedTaskId);
          setTimeout(()=>{
            anotherSmoothScroll('#subtaskList','#'+selectedSubtaskId); 
            flashAnimation('#'+selectedSubtaskId);
          },500)
        })
      }
      else if(notificationType == '4'){
        if(selectedSubtaskId){
          setTimeout(()=>{
            anotherSmoothScroll('#taskList','#'+selectedTaskId); 
            //flashAnimation('#'+selectedTaskId);
            setTimeout(()=>{
              anotherSmoothScroll('#subtaskList','#'+selectedSubtaskId); 
              //flashAnimation('#'+selectedSubtaskId);
              setTimeout(()=>{
                openMessagesModal(selectedSubtaskId,2);
                setTimeout(()=>{
                  flashAnimation('#message'+selectedNoteId);
                },1000)
              })
            },200)
          })
        }
        else{
          setTimeout(()=>{
            anotherSmoothScroll('#taskList','#'+selectedTaskId); 
            //flashAnimation('#'+selectedTaskId);
            setTimeout(()=>{
              openMessagesModal(selectedTaskId,1);
              setTimeout(()=>{
                flashAnimation('#message'+selectedNoteId);
              },500)
            })
          })
        }
      }
      else if(notificationType == '9'){
        setTimeout(()=>{
          anotherSmoothScroll('#taskList','#'+selectedTaskId); 
          flashAnimation('#'+selectedTaskId);
        })
      }
      else if(notificationType == '10'){
        setTimeout(()=>{
          anotherSmoothScroll('#taskList','#'+selectedTaskId); 
          //flashAnimation('#'+selectedTaskId);
          setTimeout(()=>{
            anotherSmoothScroll('#subtaskList','#'+selectedSubtaskId); 
            flashAnimation('#'+selectedSubtaskId);
          },700)
        })
      }
      else if(notificationType == '11'){
        if(selectedSubtaskId){
          setTimeout(()=>{
            anotherSmoothScroll('#taskList','#'+selectedTaskId); 
            //flashAnimation('#'+selectedTaskId);
            setTimeout(()=>{
              anotherSmoothScroll('#subtaskList','#'+selectedSubtaskId); 
              //flashAnimation('#'+selectedSubtaskId);
              setTimeout(()=>{
                openMessagesModal(selectedSubtaskId,2);
                setTimeout(()=>{
                  flashAnimation('#message'+selectedNoteId);
                },1000)
              })
            },200)
          })
        }
        else{
          setTimeout(()=>{
            anotherSmoothScroll('#taskList','#'+selectedTaskId); 
            //flashAnimation('#'+selectedTaskId);
            setTimeout(()=>{
              openMessagesModal(selectedTaskId,1);
              setTimeout(()=>{
                flashAnimation('#message'+selectedNoteId);
              },500)
            })
          })
        }
      }
    }
    else if(selectedTaskId){
      setTimeout(()=>{
        anotherSmoothScroll('#taskList','#'+selectedTaskId); 
        flashAnimation('#'+selectedTaskId);
      })
    }
  }
  //GET ALL TASKS -> DRAW VIZ OF TASKS
  $.get('/employees/projectTasks', {projectId, userId}, function(data){
    //debugger;
    if(projectId && userId)
      allMyTasks = data.data;
    else
      allMyTasks = data.data[0];
    //drawTimeline();
    //readyProjectsData();
  })
  // $('#changePassBtn').on('click', {id:userId}, onEventOpenChangePassModal);
  // $('#changeRoleBtn').on('click', {id:userId}, onEventOpenChangeRoleModal);
})
var urlParams = new URLSearchParams(window.location.search);
var dataViz;
var chart;
var taskId = urlParams.get('taskId');;
var projectId = urlParams.get('projectId');
var task;
var selectedTaskId;
var selectedSubtaskId;
var selectedNoteId;
var taskWorkers;
var workers;
var allSubtasks;
var allInactiveSubtasks = [];
var subtaskId;
var loggedUser;
var allMyTasks;
var taskIsServis = false;
var userId = urlParams.get('userId');
var activeTaskId;
var overrideSubtaskId, overrideTaskId;