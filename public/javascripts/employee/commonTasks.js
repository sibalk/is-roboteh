$(function(){
  $('.list-group-item').on('click', function() {
    var $this = $(this);
    var id = parseInt(this.id);
    //debugger
  
    $('.active').removeClass('active');
    $this.toggleClass('active')
  
    changeActiveTask(id);
  })
  $('#taskHistory').on('click', getTaskChanges);
  $('#btnOverrideTask').on('click', taskOverride);
})
//swtich task information to the selected from the list of tasks
function changeActiveTask(id) {
  taskId = id;
  activeTaskId = taskId;
  //debugger
  //console.log(id);
  //Get task info 
  $.get('/projects/task', {id}, function(data){
    task = data.data;
    projectId = task.id_project;
    //debugger;
    if(task.project_name){
      $('#activeProjectName').html(task.project_number + ' - ' + task.project_name);
      taskIsServis = false;
    }
    else{
      $('#activeProjectName').html(task.subscriber + " (servis - storitev)");
      taskIsServis = true;
    }
    $('#activeTaskName').html(task.task_name);
    var categoryClass = ''; 
    var category = task.category.toUpperCase();
    if(category == 'NABAVA')
      categoryClass = 'badge-purchase';
    else if(category == 'KONSTRUKCIJA')
      categoryClass = 'badge-construction';
    else if(category == 'STROJNA IZDELAVA')
      categoryClass = 'badge-mechanic';
    else if(category == 'ELEKTRO IZDELAVA')
      categoryClass = 'badge-electrican';
    else if(category == 'MONTAŽA')
      categoryClass = 'badge-assembly';
    else if(category == 'PROGRAMIRANJE')
      categoryClass = 'badge-programmer';
    else if(category == 'BREZ')
      category = '';
    var categoryElem = `<span class="badge badge-pill `+categoryClass+` ml-2">`+category+`</span>`;
    $('#activeTaskCategory').empty();
    $('#activeTaskCategory').append(categoryElem);
    $('#activeTaskProgress').removeClass();
    $('#activeTaskProgress').addClass('progress-bar w-'+task.completion);
    //$('#activeTaskProgress')[0].style.width = task.completion+'%';
    var taskStart = task.task_start ? new Date(task.task_start).toLocaleDateString('sl') : '/';
    var taskFinish = task.task_finish ? new Date(task.task_finish).toLocaleDateString('sl') : '/';
    $('#activeTaskDate').html(taskStart + " - " + taskFinish);
    colorMyActiveTask();
    //workorders
    if(!task.id_project){
      $('#taskWorkorders').show();
      $('#taskWorkorders').find('i').removeClass('text-danger');
      $('#taskWorkorders').find('i').removeClass('text-success');
      if(task.workorders_id){
        $('#taskWorkorders').attr('data-content', task.workorders);
        $('#taskWorkorders').find('i').addClass('text-success');
      }
      else{
        $('#taskWorkorders').attr('data-content', 'brez delovnega naloga');
        $('#taskWorkorders').find('i').addClass('text-danger');
      }
    }
    else
      $('#taskWorkorders').hide();
    //Get workers on project
    projectId = task.id_project;
    if(projectId){
      $.get('/projects/workerid', {projectId}, function(data){
        workers = data.data;
        //debugger;
        var element = "";
        for(var i=0; i<workers.length; i++){
          element = element + workers[i].workRole + ": " + workers[i].name + " " + workers[i].surname + "<br />";
        }
        $('.mypopover').attr('data-content', element);
        //get workers for this task
        $.get('/employees/workers', {taskId}, function(data){
          taskWorkers = data.data;
          //debugger;
          var element = "";
          for(var i=0; i<taskWorkers.length; i++){
            element = element + taskWorkers[i].name + " " + taskWorkers[i].surname + "<br />";
          }
          $('.workerspopover').attr('data-content', element);
          //get workers for this task
         // debugger
        })
       // debugger
      })
    }
    else{
      //get workers on servis
      $.get('/projects/workerid', {taskId}, function(data){
        taskWorkers = data.data;
        //debugger;
        var element = "";
        for(var i=0; i<taskWorkers.length; i++){
          element = element + taskWorkers[i].name + " " + taskWorkers[i].surname + "<br />";
        }
        $('.workerspopover').attr('data-content', element);
        $('.mypopover').attr('data-content', "");
       // debugger
      })
    }
    //load subtasks
    //taskId = task.id;
    //debugger;
    $('#controlButtons').empty();
    var element;
    if(projectId){
      element = '<a class="btn btn-info mr-2 mt-1" href="/projects/id?id='+task.id_project+'">Na projekt</a>';
      $('#controlButtons').append(element);
    }
    if(loggedUser.role == 'admin' || loggedUser.role.substring(0,5) == 'vodja' || loggedUser.role == 'programer plc-jev' || loggedUser.role == 'programer robotov' || loggedUser.role == 'konstrukter' || loggedUser.role == 'kontruktor'){
      element = '<button class="btn btn-success mr-2 mt-1" id="openSubtaskModalBtn"><i class="fas fa-plus fa-lg"></i></button>';
      $('#controlButtons').append(element);
      $('#openSubtaskModalBtn').on('click', {id:task.id}, onClickOpenSubtaskModal);
    }
    //var position = 'ml-auto';
    getSubtasks();
  })
}
//fix colors on page for active task
function colorMyActiveTask(){
  $('#myActiveTask').removeClass('border-low-priority');
  $('#myActiveTask').removeClass('border-medium-priority');
  $('#myActiveTask').removeClass('border-high-priority');
  $('#'+task.id).removeClass('border-completed');
  $('#'+task.id).removeClass('border-low-priority');
  $('#'+task.id).removeClass('border-medium-priority');
  $('#'+task.id).removeClass('border-high-priority');
  //$('#activeTaskDate').removeClass('text-danger');
  if(task.completion == 100){
    $('#'+task.id).addClass('border-completed');
    $('#activeTaskProgress').addClass('bg-success');
    $('#activeTaskDate').removeClass('text-danger');
    $('#'+task.id).find('#date').removeClass('text-danger');
  }
  else{
    $('#activeTaskProgress').removeClass('bg-success');
    $('#activeTaskDate').removeClass('text-danger');
    if(task.task_finish){
      if(new Date(task.task_finish).getTime() < new Date().getTime()){
        $('#activeTaskDate').addClass('text-danger');
        $('#'+task.id).find('#date').addClass('text-danger');
      }
    }
    if(task.priority === 'nizka')
      $('#'+task.id).addClass('border-low-priority');
    else if(task.priority === 'srednja')
      $('#'+task.id).addClass('border-medium-priority');
    else if(task.priority === 'visoka')
      $('#'+task.id).addClass('border-high-priority');
  }
  if(task.priority === 'nizka')
    $('#myActiveTask').addClass('border-low-priority');
  else if(task.priority === 'srednja')
    $('#myActiveTask').addClass('border-medium-priority');
  else if(task.priority === 'visoka')
    $('#myActiveTask').addClass('border-high-priority');
  // buttons color
  $('#taskButtonSubtask').find('i').removeClass('text-body');
  $('#taskButtonFile').find('i').removeClass('text-body');
  $('#taskButtonAbsence').find('i').removeClass('text-body');
  if(task.subtask_count > 0) $('#taskButtonSubtask').find('i').addClass('text-body');
  if(task.files_count > 0) $('#taskButtonFile').find('i').addClass('text-body');
  if(task.task_absence_count > 0 || task.full_absence_count > 0) $('#taskButtonAbsence').find('i').addClass('text-body');
}
//toggle task, if successfull update information on page and save change in system database
function toggleTaskCheckbox(element){
  //debugger;
  var completion = this.checked ? 100 : 0;
  if((this.checked == true) && ($('#taskWorkorders').attr('data-content') == 'brez delovnega naloga' && !task.id_project)){
    $('#customTaskCheck').prop('checked', false);
    $('#btnOverrideTask').show();
    $('#btnOverrideSubtask').hide();
    $('#modalWarningWO').modal('toggle');
  }
  else{
    $.post('/projects/task/completion', {taskId, completion}, function(resp){
      //alert(resp);
      //console.log("post response " + resp);
      if(parseInt(task.completion) != 100 && completion == 100){
        addFinishedDate();
      }
      //$('#activeTaskProgress')[0].style.width = completion+'%';
      $('#activeTaskProgress').removeClass();
      completion == 100 ? $('#activeTaskProgress').addClass('progress-bar bg-success w-'+completion): $('#activeTaskProgress').addClass('progress-bar w-'+completion);
      
      task.completion = completion;
      //update timeline
      updateTimeline(completion);
      //fix colors for active task
      colorMyActiveTask();
      //$('#MyTaskTable').find('tr#'+taskId).find('td#completionTask').html(completion);
      if(task.id_project)
        fixCompletion();
      //ADDING NEW CHANGE TO DB
      if(task.id_project){
        //project change
        if(this.checked)
          //addNewProjectChange(2,4,task.id_project,taskId);
          addChangeSystem(4,2,task.id_project,taskId);
        else
          //addNewProjectChange(2,5,task.id_project,taskId);
          addChangeSystem(5,2,task.id_project,taskId);
      }
      else{
        //servis change
        if(element.checked)
          //addNewServisChange(9,4,task.id_subscriber,taskId);
          addChangeSystem(4,9,null,taskId,null,null,null,null,task.id_subscriber);
        else
          //addNewServisChange(9,5,task.id_subscriber,taskId);
          addChangeSystem(5,9,null,taskId,null,null,null,null,task.id_subscriber);
      }
      //addChange(0);
    })
  }
}
//in case if service has no work orders but admins/leaders/secrateries want to toggle it anyway, in case service has work order on paper
function taskOverride(){
  var taskId = task.id;
  var completion = 100;
  $.post('/projects/task/completion', {taskId, completion}, function(resp){
    $('#customTaskCheck').prop('checked', true);
    $('#modalWarningWO').modal('toggle');
    //alert(resp);
    //console.log("post response " + resp);
    if(parseInt(task.completion) != 100 && completion == 100){
      addFinishedDate();
    }
    //$('#activeTaskProgress')[0].style.width = completion+'%';
    $('#activeTaskProgress').removeClass();
    completion == 100 ? $('#activeTaskProgress').addClass('progress-bar bg-success w-'+completion): $('#activeTaskProgress').addClass('progress-bar w-'+completion);
    
    task.completion = completion;
    //update timeline
    updateTimeline(completion);
    //fix colors for active task
    colorMyActiveTask();
    //$('#MyTaskTable').find('tr#'+taskId).find('td#completionTask').html(completion);
    if(task.id_project)
      fixCompletion();
    //ADDING NEW CHANGE TO DB
    //addNewServisChange(9,4,task.id_subscriber,taskId);
    addChangeSystem(4,9,null,taskId,null,null,null,null,task.id_subscriber);
  })
}
//FIX COMPLETION OF THE PROJECT, task percentage has changed
function fixCompletion(){
  projectId = task.id_project;
  $.post('/projects/completion', {projectId}, function(resp){
    if(resp.success == true)
      console.log("Successfully updated project percentage." );
    else if(resp.success == false)
      console.log("Unsuccessfully updated project percentage, there was no projectId" );
  })
}
function openProjectPage(){
  window.location.href='/projects/id?id='+task.id_project+'';
}
//ADD FINISHED DATE WHEN TASK WAS GIVEN 100 COMPLETION
function addFinishedDate(){
  //debugger;
  $.post('/projects/task/finished', {taskId}, function(resp){
    //debugger;
  })
}
//get task's changes from system db, user gets info who made the tasks to get more information if needed
function getTaskChanges(){
  //debugger;
  var taskId = task.id;
  let data = {taskId};
  $.ajax({
    type: 'GET',
    url: '/servis/changes',
    contentType: 'application/json',
    data: data, // access in body
  }).done(function (resp) {
    if(resp.success){
      //debugger;
      var taskChanges = resp.taskChanges;
      console.log('Successfully getting all the changes of selected work order.');
      var element = "";
      var fileLabel = "";
        for(var i=0; i<taskChanges.length; i++){
          if(taskChanges[i].id_type == 13)
            fileLabel = " datoteko";
          else if(taskChanges[i].id_type == 11){
            if(taskChanges[i].subtask)
              fileLabel = " sporočilo nalogi " + taskChanges[i].subtask;
            else
              fileLabel = " sporočilo";
          }
          else if(taskChanges[i].id_type == 10)
            fileLabel = " nalogo " + taskChanges[i].subtask;
          else if(taskChanges[i].id_type == 12)
            fileLabel = " odsotnost";
          else
            fileLabel = "";
          element = new Date(taskChanges[i].date).toLocaleDateString('sl') + ' ' + new Date(taskChanges[i].date).toLocaleTimeString('sl').substring(0,5) + ", " + taskChanges[i].name + " " + taskChanges[i].surname + ', ' + taskChanges[i].status + fileLabel + "<br />" + element;
        }
        //$('.mypopover').attr('data-content', element);
        $('#taskHistory').attr('data-content', element);
        $('#taskHistory').popover('show');
        //$('[data-toggle="popover"]').popover({trigger: "hover"}); 
    }
    else{
      $('#modalError').modal('toggle');
    }
  }).fail(function (resp) {
    //console.log('FAIL');
    //debugger;
    $('#modalError').modal('toggle');
  }).always(function (resp) {
    //console.log('ALWAYS');
    //debugger;
  });
}