$(function(){
  //$('[data-toggle="tooltip"]').tooltip();
  //$('[data-toggle="popover"]').popover()
  $('[data-toggle="tooltip"]').tooltip({trigger : 'hover'});
  $('[rel="tooltip"]').on('click', function () {
    $(this).tooltip('hide')
  })
  $('#modalTaskDocumentAddForm').on('hidden.bs.modal', function () {
    $("#modalTaskFilesForm").toggle();
    $("#modalTaskFilesForm").focus();
  })
  $("#addTaskDocForm").submit(function (e) {
    //event.preventDefault();
    e.preventDefault();
    setTimeout(disableFunction, 1);
    var file = document.getElementById("docNew").files[0];
    var formData = new FormData();
    if (projectId){
      formData.append("docTaskNew", file);
      formData.append("taskId", activeTaskId);
      formData.append("projectId", projectId);
    }
    else{
      formData.append("docNew", file);
      formData.append("taskId", activeTaskId);
    }
    //console.log(formData);
    let urlPost = projectId ? '/projects/taskFileUpload' : '/servis/taskFileUpload';
    debugger;
    $.ajax({
      url: urlPost,
      type: 'POST',
      data: formData,
      contentType: false,
      processData: false,
      success: function(res) {
        //console.log('success');
        $('#btnAddTaskDoc').attr('disabled',false);
        $('#docNew').val('');
        //addSystemChange(23,1,workOrderId,workOrderFileId);
        //addChangeSystem(1,23,null,null,null,null,null,null,null,null,null,workOrderId,null,null,null,null,null,workOrderFileId);
        $('#modalTaskDocumentAddForm').modal('toggle')
        //add new file to the list of all files to this work order
        var fullDate = new Date(res.file.date).toLocaleString("sl");
        var date = fullDate.substring(0,11);
        var time = fullDate.substring(12,17);
        var fileSrc = '/file'+res.file.type+'.png'
        
        let tmpElem = '', tmpTag = '';
        if (res.file.base64 && res.file.type == 'IMG'){
          fileSrc = res.file.base64;
          tmpElem = `<div class="overlay-zoom">
            <div class="icon-overlay">
              <i class="fas fa-search-plus"></i>
            </div>
          </div>`;
          tmpTag = `<div class="overlay-tag"">
            <img class="img-fluid" src="/imgTag.png" />
          </div>`;
        }
        else if (res.file.base64 && res.file.type == 'PDF'){
          // convertPDFCanvasToImg(res.file.base64, res.file.id);
          tmpElem = `<div class="overlay-zoom">
            <div class="icon-overlay">
              <i class="fas fa-search-plus"></i>
            </div>
          </div>`;
          tmpTag = `<div class="overlay-tag"">
            <img class="img-fluid" src="/pdfTag.png" />
          </div>`;
        }
        let listElement = `<div class="list-group-item" id="taskFile`+res.file.id+`">
          <div class="row">
            <div class="col-sm-3 justify-content-center d-flex">
              <div class="preview-icon">
                <img class="img-fluid bg-light image-icon w-70px" src=` + fileSrc + ` alt="ikona" />
                ` + tmpElem + `
                ` + tmpTag + `
              </div>
            </div>
            <div class="col-sm-7">
              <label class="row mt-2">`+res.file.original_name+`</label>
              <label class="row mt-2 small">Avtor: `+loggedUser.name+" "+loggedUser.surname+`</label>
            </div>
            <div class="col-sm-1"><a class="btn btn-primary mt-3" href="/servis/sendMeDoc?filename=`+res.file.path_name+`&projectId=`+projectId+`"><i class="fas fa-download fa-lg"></i></a></div>
          </div>
        </div>`;
        $('#fileList').prepend(listElement);
        if (res.file.base64 && res.file.type == 'IMG')
          $('#taskFile' + res.file.id).find('.overlay-zoom').on('click', {id: res.file.id, filePath: res.file.path_name}, onClickGetIMGData);
        else if (res.file.base64 && res.file.type == 'PDF')
          $('#taskFile' + res.file.id).find('.overlay-zoom').on('click', {id: res.file.id, filePath: res.file.path_name}, onClickGetPDFData);
        //prepare modalMsg for info about success
        $('#msgImg').attr('src','/ok_sign.png');
        $('#msg').html('Uspešno nalaganje nove datoteke.');
        $('#modalMsg').modal('toggle');
      },
      error: function(res) {
        //console.log('error');
        $('#btnAddTaskDoc').attr('disabled',false);
        $('#docNew').val('');
        $('#modalTaskDocumentAddForm').modal('toggle')
        //prepare modalMsg for info about success
        $('#msgImg').attr('src','/warning_sign.png');
        $('#msg').html(res.responseJSON.message);
        $('#modalMsg').modal('toggle');
      }
    })
  });
  //get project files
  //no need, every time new file is added page is reloaded so server can already fetch files
  $('#taskButtonFile').on('click', getFiles);
  $('#addFileBtn').on('click', newTaskDocumentModal);
})
function newTaskDocumentModal(){
  //open modal for adding files that are connected to task
  //$("#modalTaskFilesForm").toggle();
  $("#modalTaskDocumentAddForm").modal();
  $('#taskIdTaskFile').val(taskId);
  debugger;
}
function disableFunction(){
  //$('#btnAddDoc').attr('disabled','disabled');
  $('#btnAddTaskDoc').attr('disabled','disabled');
}
function disableFunctionTask(){
  $('#btnAddTaskDoc').attr('disabled','disabled');
}
function getFiles(){
  //debugger;
  $('#collapseSubtasks').collapse('hide');
  $('#collapseAbsences').collapse('hide');
  $('#collapseFiles').collapse('show');
  $.get('/projects/taskFiles', {taskId}, function(data){
    $('#fileList').empty();
    let files = data.data;
    //debugger;
    $('#fileList'+taskId).empty();
    for(var i = 0; i < files.length; i++){
      var date = new Date(files[i].date).toLocaleDateString('sl');
      var time = new Date(files[i].date).toLocaleTimeString('sl').substring(0,5);
      var fileSrc = '/file'+files[i].type+'.png'
		  var fileWidth = '70';
      
      var listElement = `<div class="list-group-item" id="taskFile`+files[i].id+`">
        <div class="row">
          <div class="col-sm-3 justify-content-center d-flex">
            <div class="preview-icon">  
              <img class="img-fluid bg-light image-icon w-70px" src=` + fileSrc + ` alt="ikona" />
            </div>
          </div>
          <div class="col-sm-7">
            <label class="row mt-2">`+files[i].original_name+`</label>
            <label class="row mt-2 small">Avtor: `+files[i].name+" "+files[i].surname+`</label>
          </div>
          <div class="col-sm-1"><a class="btn btn-primary mt-3" href="/projects/sendMeDoc?filename=`+files[i].path_name+`&projectId=`+projectId+`"><i class="fas fa-download fa-lg"></i></a></div>
        </div>
      </div>`;
      
      $('#fileList').append(listElement);
    }
    allTaskFiles = files;
    setTimeout(()=>{
      getFileThumbnails(files, true);
    },500)
  });
}
function getFileThumbnails(allFiles, isTask) {
  for(var i = 0, l = allFiles.length; i < l; i++){
    if(allFiles[i].type == 'IMG' || allFiles[i].type == 'PDF'){
      var fileId = allFiles[i].id, filePath = allFiles[i].path_name, fileType = allFiles[i].type;
      var tmpIsTask = isTask ? true : false;
      $.get('/projects/files/thumbnail', {fileId, filePath, fileType}, function(data){
        if (data.success){
          //tmpFileInfo = data.data;
          if(data.type == 'IMG'){
            if (isTask)
              $('#taskFile'+data.id).find('img').prop('src', data.data);
            else
              $('#file'+data.id).find('img').prop('src', data.data);
            var tmpElem = `<div class="overlay-zoom">
              <div class="icon-overlay">
                <i class="fas fa-search-plus"></i>
              </div>
            </div>`;
            var tmpTag = `<div class="overlay-tag"">
              <img class="img-fluid" src="/imgTag.png" />
            </div>`
            if (isTask){
              $('#taskFile' + data.id).find('div .preview-icon').append(tmpElem);
              $('#taskFile' + data.id).find('div .preview-icon').append(tmpTag);
              $('#taskFile' + data.id).find('.overlay-zoom').on('click', {id: data.id, filePath: data.filePath}, onClickGetIMGData);
            }
            else{
              $('#file' + data.id).find('div .preview-icon').append(tmpElem);
              $('#file' + data.id).find('div .preview-icon').append(tmpTag);
              $('#file' + data.id).find('.overlay-zoom').on('click', {id: data.id, filePath: data.filePath}, onClickGetIMGData);
            }
          }
          else if(data.type == 'PDF'){
            //tmpFileInfo = data;
            //tmpPDFThumbnailData.push({fileId: data.id, fileBase64: data.data});
            convertPDFCanvasToImg(data.data, data.id, tmpIsTask);
            var tmpElem = `<div class="overlay-zoom">
              <div class="icon-overlay">
                <i class="fas fa-search-plus"></i>
              </div>
            </div>`;
            var tmpTag = `<div class="overlay-tag"">
              <img class="img-fluid" src="/pdfTag.png" />
            </div>`;
            if (isTask){
              $('#taskFile' + data.id).find('div .preview-icon').append(tmpElem);
              $('#taskFile' + data.id).find('div .preview-icon').append(tmpTag);
              $('#taskFile' + data.id).find('.overlay-zoom').on('click', {id: data.id, filePath: data.filePath}, onClickGetPDFData);
            }
            else{
              $('#file' + data.id).find('div .preview-icon').append(tmpElem);
              $('#file' + data.id).find('div .preview-icon').append(tmpTag);
              $('#file' + data.id).find('.overlay-zoom').on('click', {id: data.id, filePath: data.filePath}, onClickGetPDFData);
            }
          }
        }
        else{
          if (data.type == 'IMG'){
            if (isTask)
              $('#taskFile'+data.id).find('img').prop('src', '/fileNoIMG.png');
            else
              $('#file'+data.id).find('img').prop('src', '/fileNoIMG.png');
          }
          else if(data.type == 'PDF'){
            if (isTask)
              $('#taskFile'+data.id).find('img').prop('src', '/fileNoPDF.png');
            else
              $('#file'+data.id).find('img').prop('src', '/fileNoPDF.png');
          }
          //console.log("error while getting thumnail or file does not exist");
        }
      });
    }
  }
}
function convertPDFCanvasToImg(pdfData, fileId, isTask) {
  var loadingTask = pdfjsLib.getDocument({ data: atob(pdfData), });
  loadingTask.promise.then(function(pdf) {
    var canvasdiv = document.getElementById('the-canvas');
    var totalPages = pdf.numPages
    var data = [];
      
    pdf.getPage(1).then(function(page) {
      var scale = 1.5;
      var viewport = page.getViewport({ scale: scale });

      var canvas = document.createElement('canvas');
      canvasdiv.appendChild(canvas);

      // Prepare canvas using PDF page dimensions
      var context = canvas.getContext('2d');
      canvas.height = viewport.height;
      canvas.width = viewport.width;

      // Render PDF page into canvas context
      var renderContext = { canvasContext: context, viewport: viewport };

      var renderTask = page.render(renderContext);
      renderTask.promise.then(function() {
        data.push(canvas.toDataURL('image/png'))
        //tmpData.push(data);
        if (isTask)
          $('#taskFile'+fileId).find('img.image-icon').prop('src', data[0]);
        else
          $('#file'+fileId).find('img.image-icon').prop('src', data[0]);
        //$('#file'+fileId).find('img.image-icon').css('width', '70px');
        //console.log(data.length + ' page(s) loaded in data')
      });
    })
  })
}
function onClickGetPDFData(event) {
  getPDFData(event.data.id, event.data.filePath)
}
function getPDFData(id, filePath) {
  var tmp = `<embed src="/projects/files/resources/?fileName=` + filePath + `" class="pdfobject embeded-pdf" type="application/pdf">`
  var height = $('#taskFile'+id).find('img').css('height').match(/[\d\.]+/);
  var width = $('#taskFile'+id).find('img').css('width').match(/[\d\.]+/);
  if (Math.round(parseFloat(height)/parseFloat(width) * 100)/100 == 1.41){
    $("#pdfPreviewContainer").removeClass('document-content');
    $("#pdfPreviewContainer").addClass('document-content-vertical-pdf');
  }
  else {
    $("#pdfPreviewContainer").removeClass('document-content-vertical-pdf');
    $("#pdfPreviewContainer").addClass('document-content');
  }
  $("#pdfPreviewContainer").empty();
  $("#imgPreviewContainer").empty();
  $('#imgPreviewContainer').css('display', 'none');
  $('#pdfPreviewContainer').css('display', '');
  $('#modalPDFPreview').modal('toggle');
  $("#pdfPreviewContainer").append(tmp);
}
function onClickGetIMGData(event) {
  getIMGData(event.data.id, event.data.filePath)
}
function getIMGData(id, filePath) {
  var tmp = `<img class="modal-img img-fluid" src="/projects/files/resources/?fileName=` + filePath + `">`
  $("#pdfPreviewContainer").empty();
  $("#imgPreviewContainer").empty();
  $('#imgPreviewContainer').css('display', '');
  $('#pdfPreviewContainer').css('display', 'none');
  $('#modalPDFPreview').modal('toggle');
  $("#imgPreviewContainer").append(tmp);
}
let allTaskFiles;