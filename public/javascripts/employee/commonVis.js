$(function(){
  
})
///////VIZUALIZATION////////
function drawTimeline(){
  //debugger;
  makeVizData();
  //debugger;
  if(dataViz.length == 0)
    return
  chart = TimelinesChart()
      .data(dataViz)
      .width(1068)
      .timeFormat("%d.%m.%Y")
      .zQualitative(true)
      (document.getElementById('myPlot'));
  $('#myPlot').addClass('timeline-chart-overflow');
}
function makeVizData(){
  dataViz = [
    {group:"Nabava", data:[]},
    {group:"Konstrukcija", data:[]},
    {group:"Strojna izdelava", data:[]},
    {group:"Elektro izdelava", data:[]},
    {group:"Montaža", data:[]},
    {group:"Programiranje", data:[]},
    {group:"Brez", data:[]}
  ];
  var delavec;
  for(var i=0; i<allMyTasks.length; i++){
    //debugger;
    if(allMyTasks[i].active != true)
      continue;
    if(allMyTasks[i].project_name){
      delavec = allMyTasks[i].project_name;
    }
    else
      delavec = "storitev"
    var start = allMyTasks[i].task_start;
    var finish = allMyTasks[i].task_finish;
     if(!allMyTasks[i].task_start && !allMyTasks[i].task_finish)
      continue;
    else if(!allMyTasks[i].task_start)
      start = allMyTasks[i].task_finish;
    else if(!allMyTasks[i].task_finish)
      finish = allMyTasks[i].task_start;
    
    if(allMyTasks[i].category === 'Nabava'){
      //debugger;
      dataViz[0].data.push(
        {
          label: allMyTasks[i].task_name,
          data: [
            {
              timeRange:[start, finish],
              val: delavec,
              workers: delavec,
              taskName: allMyTasks[i].task_name,
              completion: allMyTasks[i].completion,
            }
          ]
        }
      );
    }
    else if(allMyTasks[i].category === 'Konstrukcija'){
      //debugger;
      dataViz[1].data.push(
        {
          label: allMyTasks[i].task_name,
          data: [
            {
              timeRange:[start, finish],
              val: delavec,
              workers: delavec,
              taskName: allMyTasks[i].task_name,
              completion: allMyTasks[i].completion,
            }
          ]
        }
      );
    }
    else if(allMyTasks[i].category === 'Strojna izdelava'){
      dataViz[2].data.push(
        {
          label: allMyTasks[i].task_name,
          data: [
            {
              timeRange:[start, finish],
              val: delavec,
              workers: delavec,
              taskName: allMyTasks[i].task_name,
              completion: allMyTasks[i].completion,
            }
          ]
        }
      );
    }
    else if(allMyTasks[i].category === 'Elektro izdelava'){
      dataViz[3].data.push(
        {
          label: allMyTasks[i].task_name,
          data: [
            {
              timeRange:[start, finish],
              val: delavec,
              workers: delavec,
              taskName: allMyTasks[i].task_name,
              completion: allMyTasks[i].completion,
            }
          ]
        }
      );
    }
    else if(allMyTasks[i].category === 'Montaža'){
      dataViz[4].data.push(
        {
          label: allMyTasks[i].task_name,
          data: [
            {
              timeRange:[start, finish],
              val: delavec,
              workers: delavec,
              taskName: allMyTasks[i].task_name,
              completion: allMyTasks[i].completion,
            }
          ]
        }
      );
    }
    else if(allMyTasks[i].category === 'Programiranje'){
      dataViz[5].data.push(
        {
          label: allMyTasks[i].task_name,
          data: [
            {
              timeRange:[start, finish],
              val: delavec,
              workers: delavec,
              taskName: allMyTasks[i].task_name,
              completion: allMyTasks[i].completion,
            }
          ]
        }
      );
    }
    else if(allMyTasks[i].category === 'Brez'){
      dataViz[6].data.push(
        {
          label: allMyTasks[i].task_name,
          data: [
            {
              timeRange:[start, finish],
              val: delavec,
              workers: delavec,
              taskName: allMyTasks[i].task_name,
              completion: allMyTasks[i].completion,
            }
          ]
        }
      );
    }
  }
  if(dataViz[6].data.length == 0) dataViz.splice(6,1)
  if(dataViz[5].data.length == 0) dataViz.splice(5,1)
  if(dataViz[4].data.length == 0) dataViz.splice(4,1)
  if(dataViz[3].data.length == 0) dataViz.splice(3,1)
  if(dataViz[2].data.length == 0) dataViz.splice(2,1)
  if(dataViz[1].data.length == 0) dataViz.splice(1,1)
  if(dataViz[0].data.length == 0) dataViz.splice(0,1)
  //debugger;
}