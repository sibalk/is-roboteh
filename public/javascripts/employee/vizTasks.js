$(document).ready(function(){
  $('#moveTLLeft').on('click', {percentage: 0.2}, onEventMoveProjects);
  $('#moveTLRight').on('click', {percentage: -0.2}, onEventMoveProjects);
  $('#homeTL').on('click', showCurrentWeekProjects);
  $('#dayTL').on('click', showDayProjects);
  $('#weekTL').on('click', showWeekProjects);
  $('#monthTL').on('click', showMonthProjects);
  $('#lowerTLFont').on('click', lowerFontProjects);
  $('#higherTLFont').on('click', higherFontProjects);
  $('#refreshTL').on('click', refreshTimelineProjects);
  $('#resizeTL').on('click', resizeTimeline);
  $('#TimelineBtn').on('click', firstTimelineDraw);
})
function readyProjectsData() {
  //prepare data for y axis to get all projects from allMyTasks
  minDate = new Date();
  maxDate = new Date();
  allMyTasks.forEach(task => {
    var tmp = allMyProjects.find(p => p.id == task.project_id);
    if (!tmp){
      //make new allMyProjects element for new project
      //debugger;
      allMyProjects.push({id:task.project_id, content: task.project_number})
    }
    if (task.project_id){
      task.group = task.project_id;
      task.content = task.project_number + ' - ' + task.task_name;
      task.title = task.task_name + ' (' + task.project_number + ' - ' + task.project_name + ', ' + task.completion + '%)';
    }
    else{
      task.group = 0;
      task.content = task.task_name;
      task.title = task.task_name + ' (storitev - ' + task.subscriber + ', ' + task.completion + '%)';
    }
    task.start = task.task_start ? new Date(task.task_start) : null;
    task.end = task.task_finish ? new Date(task.task_finish) : null;
    //correct time to full day
    if (task.start && resetStartEndTime)
      task.start = new Date(task.start).setHours(0,0,0,0);
    if (task.end && resetStartEndTime)
      task.end = new Date(task.end).setHours(24,0,0,0);
    if (task.category == 'Brez'){
      task.visibleFrameTemplate = '<div class=""><div class="progress-vis progress-brez w-' + task.completion + '"></div></div>';
    }
    if (task.category == 'Nabava'){
      task.className = 'purchase'
      task.visibleFrameTemplate = '<div class=""><div class="progress-vis progress-purchase w-' + task.completion + '"></div></div>';
    }
    if (task.category == 'Konstrukcija'){
      task.className = 'builder';
      task.visibleFrameTemplate = '<div class=""><div class="progress-vis progress-builder w-' + task.completion + '"></div></div>';
    }
    if (task.category == 'Strojna izdelava'){
      task.className = 'mechanic';
      task.visibleFrameTemplate = '<div class=""><div class="progress-vis progress-mechanic w-' + task.completion + '"></div></div>';
    }
    if (task.category == 'Elektro izdelava'){
      task.className = 'electrican';
      task.visibleFrameTemplate = '<div class=""><div class="progress-vis progress-electrican w-' + task.completion + '"></div></div>';
    }
    if (task.category == 'Montaža'){
      task.className = 'assembly';
      task.visibleFrameTemplate = '<div class=""><div class="progress-vis progress-assembly w-' + task.completion + '"></div></div>';
    }
    if (task.category == 'Programiranje'){
      task.className = 'plc';
      task.visibleFrameTemplate = '<div class=""><div class="progress-vis progress-plc w-' + task.completion + '"></div></div>';
    }
    if (task.completion == 100 && ((!task.start || !task.end)))
      task.className += ' completed'; 
    if (!task.start && task.end){
      task.start = task.end;
      task.end = undefined;
    }
    //FIND MIN START AND MAX END DATES 
    if (new Date(task.start) < new Date(minDate)) minDate = task.start;
    if (new Date(task.end) > new Date(maxDate)) maxDate = task.end;
  });
  //sort allMyTasks by project number
  var subscriberTasksRow;
  if (!allMyProjects[0].content) subscriberTasksRow = allMyProjects.shift();
  var tmp1 = allMyProjects.filter(p => p.content.substring(0,2) == 'RT' );
  var tmp2 = allMyProjects.filter(p => p.content.substring(0,3) == 'STR' );
  //var tmp3 = allMyProjects.filter(p => p.content.substring(0,3) != 'STR' && p.content.substring(0,2) != 'RT');
  tmp1.forEach(p => p.number_order = parseInt(p.content.substring(2)));
  tmp2.forEach(p => p.number_order = parseInt(p.content.substring(3)));
  var sortedTmp1 = tmp1.sort(function(a,b){
    if (isNaN(a.number_order))
      return 1;
    else if (isNaN(b.number_order))
      return -1;
    return b.number_order - a.number_order;
  });
  var sortedTmp2 = tmp2.sort(function(a,b){
    if (isNaN(a.number_order))
      return 1;
    else if (isNaN(b.number_order))
      return -1;
    return b.number_order - a.number_order;
  });
  //allRTProjects = sortedTmp1, allSTRProjects = sortedTmp2;
  allMyProjects = sortedTmp2.concat(sortedTmp1);
  //if subscriberTasksRow exist unshift it to allMyProjects
  if(subscriberTasksRow) allMyProjects.unshift({id:0, content: 'servis'});
  //coloring the ood rows
  allMyProjects.forEach((projects,i)=>{
    if(i % 2 == 0){
      //debugger;
      projects.style =  "background: rgba(82, 226, 233, 0.2);";
      allMyTasks.push({
        group : projects.id,
        start : moment(minDate).add(-100, 'days').format("YYYY-MM-DD") + ' 00:00',
        end : moment(maxDate).add(100, 'days').format("YYYY-MM-DD") + ' 23:59',
        type : 'background',
        className : 'odd'
      });
    }
  })
  drawProjects();
}
function addDays(date, days) {
  var result = new Date(date);
  result.setDate(result.getDate() + days);
  return result;
}
function setFontSize(){
  $('.vis-panel').css('font-size', fontSize);
  if(timeline){
    //moveProjects(0.001);
    timeline.redraw()
  }
}
function drawProjects() {
  var container = document.getElementById("projectsVis");
  var options = {
    visibleFrameTemplate: function (item) {
      if (item.visibleFrameTemplate) {
        return item.visibleFrameTemplate;
      }
    },
    orientation: {
      axis: 'both'
    },
    locale: "sl",
  };
  timeline = new vis.Timeline(container, allMyTasks, allMyProjects, options);
  setTimeout(()=>{
    setFontSize();
    setTimeout(()=>{
      homeRangeProjects = timeline.getWindow();
    },300);
  },1000)
}
function updateTimeline(completion) {
  //$('#projectsVis').empty();
  //console.log('posodabljanje casovnice - ' + completion);
  //debugger;
  var tmp = allMyTasks.find(t => t.id == task.id)
  if(tmp){
    tmp.completion = completion;
    if (tmp.category == 'Brez'){
      tmp.visibleFrameTemplate = '<div class=""><div class="progress-vis progress-brez w-' + tmp.completion + '"></div></div>';
    }
    if (tmp.category == 'Nabava'){
      tmp.className = 'purchase'
      tmp.visibleFrameTemplate = '<div class=""><div class="progress-vis progress-purchase w-' + tmp.completion + '"></div></div>';
    }
    if (tmp.category == 'Konstrukcija'){
      tmp.className = 'builder';
      tmp.visibleFrameTemplate = '<div class=""><div class="progress-vis progress-builder w-' + tmp.completion + '"></div></div>';
    }
    if (tmp.category == 'Strojna izdelava'){
      tmp.className = 'mechanic';
      tmp.visibleFrameTemplate = '<div class=""><div class="progress-vis progress-mechanic w-' + tmp.completion + '"></div></div>';
    }
    if (tmp.category == 'Elektro izdelava'){
      tmp.className = 'electrican';
      tmp.visibleFrameTemplate = '<div class=""><div class="progress-vis progress-electrican w-' + tmp.completion + '"></div></div>';
    }
    if (tmp.category == 'Montaža'){
      tmp.className = 'assembly';
      tmp.visibleFrameTemplate = '<div class=""><div class="progress-vis progress-assembly w-' + tmp.completion + '"></div></div>';
    }
    if (tmp.category == 'Programiranje'){
      tmp.className = 'plc';
      tmp.visibleFrameTemplate = '<div class=""><div class="progress-vis progress-plc w-' + tmp.completion + '"></div></div>';
    }
    if (tmp.completion == 100 && ((!tmp.start || !tmp.end)))
      tmp.className += ' completed'; 
    if(tmp.project_id){
      tmp.title = tmp.task_name + ' (' + tmp.project_number + ' - ' + tmp.project_name + ', ' + completion + '%)';
    }
    else{
      tmp.title = tmp.task_name + ' (storitev - ' + tmp.subscriber + ', ' + completion + '%)';
    }
  }
  /*
  var container = document.getElementById("projectsVis");
  var options = {
    visibleFrameTemplate: function (item) {
      if (item.visibleFrameTemplate) {
        return item.visibleFrameTemplate;
      }
    },
    orientation: {
      axis: 'both'
    },
    locale: "sl",
  };
  timeline = new vis.Timeline(container, allMyTasks, allMyProjects, options);
  */
 if (timeline)
  timeline.setItems(allMyTasks);
}
function firstTimelineDraw() {
  if(!$('#projectsVis').html())
    readyProjectsData();
}
function resizeTimeline() {
  if ($('#timelineCollapse').hasClass('row')){
    $('#timelineCollapse').removeClass('row');
    $('#timelineCollapse').addClass('full-width');
  }
  else{
    $('#timelineCollapse').removeClass('full-width');
    $('#timelineCollapse').addClass('row');
  }
}
function refreshTimelineProjects() {
  timeline.setItems(allMyTasks);
}
function lowerFontProjects(){
  $('.vis-panel').css('font-size', --fontSize);
  if(timeline){
    //moveProjects(0.001);
    timeline.redraw()
  }
}
function higherFontProjects(){
  $('.vis-panel').css('font-size', ++fontSize);
  if(timeline){
    timeline.redraw()
  }
}
function onEventMoveProjects(event) {
  moveProjects(event.data.percentage);
}
function moveProjects(percentage) {
  var range = timeline.getWindow();
  var interval = range.end - range.start;

  timeline.setWindow({
    start: range.start.valueOf() - interval * percentage,
    end: range.end.valueOf() - interval * percentage,
  });
}
function showCurrentWeekProjects(){
  //var today = new Date();
  timeline.setWindow({
    start: homeRangeProjects.start.valueOf(),
    end: homeRangeProjects.end.valueOf(),
  });
}
function showDayProjects(){
  var today = new Date();

  setTimeout(()=>{
    timeline.setWindow(moment().format("YYYY-MM-DD"), moment().add(1, 'days').format("YYYY-MM-DD"));
  },100);
}
function showWeekProjects(){
  var today = new Date();

  setTimeout(()=>{
    timeline.setWindow(moment().format("YYYY-MM-DD"), moment().add(7, 'days').format("YYYY-MM-DD"));
  },100);
}
function showMonthProjects(){
  var today = new Date();

  setTimeout(()=>{
    timeline.setWindow(moment().format("YYYY-MM-DD"), moment().add(30, 'days').format("YYYY-MM-DD"));
  },100);
}
var allMyProjects = [];
var allRTProjects, allSTRProjects, homeRangeProjects;
var minDate, maxDate;
var timeline;
var fontSize = 14;
var resetStartEndTime = true;