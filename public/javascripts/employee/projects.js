$(function(){
  loggedUser = { name: $('#loggedUser').html().split(" (")[0].split(" ")[0], surname: $('#loggedUser').html().split(" (")[0].split(" ")[1], role: $('#loggedUser').html().split(" (")[1].split(")")[0].toLowerCase() };
  if($('#projectList').children(":first").length > 0){
    $('#projectList').children(":first").toggleClass("active");
    changeProject(parseInt($('#projectList').children(":first")[0].id));
  }
  $('.list-group-item').on('click', function() {
    var $this = $(this);
    projectId = parseInt(this.id);
    //debugger
    //$('#taskList').find('a#46').find('#task_name').html();
    // task_name,project_name,date
  
    $('.active').removeClass('active');
    $this.toggleClass('active')
  
    changeProject(projectId);
  })
  // $('#changePassBtn').on('click', {id:userId}, onEventOpenChangePassModal);
  // $('#changeRoleBtn').on('click', {id:userId}, onEventOpenChangeRoleModal);
})
function changeProject(id){
  projectId = id;
  //debugger;
  //get project info
  $.get("/projects/info", {projectId}, function(data){
    project = data.data;
    $('#activeProjectSubscriber').html(project.name);
    $('#activeProjectName').html(project.project_name);
    $('#activeProjectProgress').removeClass();
    $('#activeProjectProgress').addClass('progress-bar w-'+project.completion);
    $('#myActiveProject').removeClass("border-success");
    if(project.completion == '100'){
      $('#myActiveProject').addClass("border-success");
      $('#activeProjectProgress').addClass('bg-success');
    }
    else{
      $('#activeProjectProgress').removeClass('bg-success');
    }
    $('#activeProjectButtons').empty();
    var element = '<a class="btn btn-primary mt-2" href="/projects/id?id='+projectId+'">Na projekt</a>';
    $('#activeProjectButtons').append(element);
    var projectStart = project.start ? new Date(project.start).toLocaleDateString('sl') : '/';
    var projectFinish = project.finish ? new Date(project.finish).toLocaleDateString('sl') : '/';
    $('#activeTaskDate').html(projectStart + " - " + projectFinish);
    //debugger;
    //get project's tasks for specific user
    $.get("/employees/projectTasks", {projectId, userId}, function(data){
      tasks = data.data;
      //debugger;
      $('#projectTasks').empty();
      for(var i = 0; i<data.data.length; i++){
        var taskStart = data.data[i].task_start ? new Date(data.data[i].task_start).toLocaleDateString('sl') : '/';
        var taskFinish = data.data[i].task_finish ? new Date(data.data[i].task_finish).toLocaleDateString('sl') : '/';
        var date = taskStart + " - " + taskFinish;
        var expired = "";
        if(data.data[i].completion != '100' && data.data[i].expired)
          expired = '<span class="badge badge-danger mr-2 ml-2 p-2">ZAMUDA</span>';
        var listElement = `<row class="d-flex list-group-item" id=` + data.data[i].id + `>
          <div class="mr-auto w-60">
            <label class="row ml-1">`+ data.data[i].task_name + `</label>
            <label class="row ml-1">`+ date + `</label>
            <div id="taskItemBadges">` + expired + `</div>
          </div>
          <div class="ml-auto progress mt-3 w-30">
            <div class="progress-bar w-`+data.data[i].completion+`" id="activeProjectTaskProgress"></div>
          </div><a class="btn btn-primary task-button ml-2 mt-1" href="/employees/tasks?userId=`+userId+`&amp;projectId=`+projectId+`&amp;taskId=`+data.data[i].id+`">
            <i class="fas fa-chevron-right fa-lg"></i>
          </a>
        </row>`;
        $('#projectTasks').append(listElement);
      }
      //displayTaskChanges();
    })
    //get workers on project
    $.get( "/projects/workerid", {projectId}, function( data ) {
      workers = data.data;
      //debugger;
      $('#projectWorkers').empty();
      for(var i = 0; i < workers.length; i++){
        var image = '';
        if(workers[i].workRole.toLowerCase() == 'konstruktor' || workers[i].workRole.toLowerCase() == 'konstrukter')
          image = '<img class="img-fluid" src="/builder64.png" alt="ikona" />';
        else if(workers[i].workRole.toLowerCase() == 'vodja' || workers[i].workRole.toLowerCase() == 'vodja projekta')
          image = '<img class="img-fluid" src="/manager64.png" alt="ikona" />';
        else if(workers[i].workRole.toLowerCase() == 'vodja strojnikov')
          image = '<img class="img-fluid" src="/managerMech64.png" alt="ikona" />';
        else if(workers[i].workRole.toLowerCase() == 'vodja cnc obdelave')
          image = '<img class="img-fluid" src="/managerCNC64.png" alt="ikona" />';
        else if(workers[i].workRole.toLowerCase() == 'vodja električarjev')
          image = '<img class="img-fluid" src="/managerElec64.png" alt="ikona" />';
        else if(workers[i].workRole.toLowerCase() == 'vodja programerjev')
          image = '<img class="img-fluid" src="/managerPLC64.png" alt="ikona" />';
        else if(workers[i].workRole.toLowerCase() == 'električar')
          image = '<img class="img-fluid" src="/electrican64.png" alt="ikona" />';
        else if(workers[i].workRole.toLowerCase() == 'strojnik')
          image = '<img class="img-fluid" src="/mechanic64.png" alt="ikona" />';
        else if(workers[i].workRole.toLowerCase() == 'programer plc-jev')
          image = '<img class="img-fluid" src="/plc64.png" alt="ikona" />';
        else if(workers[i].workRole.toLowerCase() == 'programer robotov')
          image = '<img class="img-fluid" src="/robot64.png" alt="ikona" />';
        else if(workers[i].workRole.toLowerCase() == 'cnc' || workers[i].workRole.toLowerCase() == 'cnc operater')
          image = '<img class="img-fluid" src="/cnc64.png" alt="ikona" />';
        else if(workers[i].workRole.toLowerCase() == 'student' || workers[i].workRole.toLowerCase() == 'študent')
          image = '<img class="img-fluid" src="/student03.png" alt="ikona" />';
        else if(workers[i].workRole.toLowerCase() == 'varilec')
          image = '<img class="img-fluid" src="/welder02.png" alt="ikona" />';
        else if(workers[i].workRole.toLowerCase() == 'serviser')
          image = '<img class="img-fluid" src="/serviser.png" alt="ikona" />';
        else if(workers[i].workRole.toLowerCase() == 'tajnik' || workers[i].workRole.toLowerCase() == 'komercialist' || workers[i].workRole.toLowerCase() == 'komercijalist' || workers[i].workRole.toLowerCase() == 'nabavnik' || workers[i].workRole.toLowerCase() == 'računovodstvo' || workers[i].workRole.toLowerCase() == 'komerciala')
          image = '<img class="img-fluid" src="/secratery64.png" alt="ikona" />';
        else
          image = '<img class="img-fluid" src="/worker64.png" alt="ikona" />';
        var element = `<div class="col-sm-4 mt-2">
          <div class="row">
            <div class="col-sm-4" id="worker">`+image+`</div>
            <div class="col-sm-8">
              <div id="workerName">` + workers[i].name + ` ` + workers[i].surname + `</div>
              <div id="workerRole">` + workers[i].workRole + `</div>
            </div>
          </div>
        </div>`;
        $('#projectWorkers').append(element);
      }
    })
  })
}
var urlParams = new URLSearchParams(window.location.search);
var userId = urlParams.get('userId');
var projectId;
var project;
var tasks;
var workers;
var loggedUser;