
$(function(){
  //$('.modal-dialog').draggable({ handle: ".modal-header" });
  $('#changePasswordform').submit(function(e){
    e.preventDefault();
    $form = $(this);
    var userPassword = user_password.value;
    var userPasswordAgain = user_password_again.value;

    debugger;

    $.post('/employees/password', {userId, userPassword, userPasswordAgain}, function(resp){
      if(resp.success){
        debugger;
        //close modal
        $('#modalChangePassword').modal('toggle');
        $('#user_password').val("");
        $('#user_password_again').val("");
        $('#user_password_again').removeClass("is-valid");
        //ADDING CHANGE TO DB
        addChangeSystem(2,17,null,null,null,null,null,null,null,null,null,null,null,null,null,null,userId);
      }
      else{
        $('#messagePass').append(resp.data);
        $('#modalError').modal('toggle');
        debugger;
      }
      //add user in list
      //debugger
      //toggle modal
    })
  })
  $('#changeRoleform').submit(function(e){
    e.preventDefault();
    $form = $(this);
    var userRole = user_role.value;

    debugger;

    $.post('/employees/role', {userId, userRole}, function(resp){
      if(resp.success){
        //ADDING CHANGE TO DB
        addChangeSystem(2,15,null,null,null,null,null,null,null,null,null,null,null,null,null,null,userId);
        var selectedRole = roles.find(r => r.id === parseInt(userRole))
        $('#modalChangeRole').modal('toggle');
        if($('#loggedUser').html().split(" (")[0] == $('#employeeName').html()){
          if(selectedRole){
            $('#loggedUser').html($('#loggedUser').html().split(" (")[0]+' ('+selectedRole.text+')');
          }
        }
        if(selectedRole){
          $('#imgEmployee').empty()
          switch (selectedRole.text.toLowerCase()) {
            case 'električar': $('#imgEmployee').append('<img class="img-fluid" src="/electrican64.png" alt="ikona" />'); break;
            case 'strojnik': //fall-through
            case 'strojni monter': $('#imgEmployee').append('<img class="img-fluid" src="/mechanic64.png" alt="ikona" />'); break;
            case 'cnc': //fall-through
            case 'cnc operater': $('#imgEmployee').append('<img class="img-fluid" src="/cnc64.png" alt="ikona" />'); break;
            case 'varilec': $('#imgEmployee').append('<img class="img-fluid" src="/welder02.png" alt="ikona" />'); break;
            case 'student': //fall-through
            case 'študnet': $('#imgEmployee').append('<img class="img-fluid" src="/student03.png" alt="ikona" />'); break;
            case 'serviser': $('#imgEmployee').append('<img class="img-fluid" src="/serviser.png" alt="ikona" />'); break;
            case 'programer robotov': $('#imgEmployee').append('<img class="img-fluid" src="/robot64.png" alt="ikona" />'); break;
            case 'programer plc-jev': $('#imgEmployee').append('<img class="img-fluid" src="/plc64.png" alt="ikona" />'); break;
            case 'konstruktor': //fall-through
            case 'konstrukter': $('#imgEmployee').append('<img class="img-fluid" src="/builder64.png" alt="ikona" />'); break;
            case 'vodja': //fall-through
            case 'vodja projekta': //fall-through
            case 'vodja projetkov': $('#imgEmployee').append('<img class="img-fluid" src="/manager64.png" alt="ikona" />'); break;
            case 'vodja strojnikov': $('#imgEmployee').append('<img class="img-fluid" src="/managerMech64.png" alt="ikona" />'); break;
            case 'vodja cnc obdelave': $('#imgEmployee').append('<img class="img-fluid" src="/managerCNC64.png" alt="ikona" />'); break;
            case 'vodja električarjev': $('#imgEmployee').append('<img class="img-fluid" src="/managerElec64.png" alt="ikona" />'); break;
            case 'vodja programerjev': $('#imgEmployee').append('<img class="img-fluid" src="/managerPLC64.png" alt="ikona" />'); break;
            case 'admin': $('#imgEmployee').append('<img class="img-fluid" src="/admin64.png" alt="ikona" />'); break;
            case 'nabavnik': //fall-through
            case 'komercialist': //fall-through
            case 'komercijalist': //fall-through
            case 'komerciala': //fall-through
            case 'računovodstvo': //fall-through
            case 'tajnik': $('#imgEmployee').append('<img class="img-fluid" src="/secratery64.png" alt="ikona" />'); break;
            case 'info': $('#imgEmployee').append('<img class="img-fluid" src="/infoUser.png" alt="ikona" />'); break;
            case 'info elektro': $('#imgEmployee').append('<img class="img-fluid" src="/infoElec.png" alt="ikona" />'); break;
            case 'info strojna': $('#imgEmployee').append('<img class="img-fluid" src="/infoMech.png" alt="ikona" />'); break;
            case 'info konstrukcija': $('#imgEmployee').append('<img class="img-fluid" src="/infoBuild.png" alt="ikona" />'); break;
            case 'info programiranje': $('#imgEmployee').append('<img class="img-fluid" src="/infoProgrammer.png" alt="ikona" />'); break;
            case 'info plc': $('#imgEmployee').append('<img class="img-fluid" src="/infoPlc.png" alt="ikona" />'); break;
            case 'info robot': $('#imgEmployee').append('<img class="img-fluid" src="/infoRobot.png" alt="ikona" />'); break;
            case 'info nabava': $('#imgEmployee').append('<img class="img-fluid" src="/infoSales.png" alt="ikona" />'); break;
            case 'info montaža': $('#imgEmployee').append('<img class="img-fluid" src="/infoAssembly.png" alt="ikona" />'); break;
            default: $('#imgEmployee').append('<img class="img-fluid" src="/worker64.png" alt="ikona" />'); break;
          }
        }
        $('#user_role').val("");
      }
      else{
        $('#modalError').modal('Error');
      }
    })
  })
  $('#user_password').on('change', onPassChange);
  $('#user_password_again').on('change', onPassChange);
  $('#changePassBtn').on('click', {id:userId}, onEventOpenChangePassModal);
  $('#changeRoleBtn').on('click', {id:userId}, onEventOpenChangeRoleModal);
})
function openChangePassModal(id){
  debugger;
  userId = id;
  $("#modalChangePassword").modal();
}
function openChangeRoleModal(id){
  debugger;
  userId = id;
  $.get( "/employees/roles", function( data ) {
    roles = data.data;

    $(".select2-role").select2({
      data: roles,
      tags: false,
      dropdownParent: $('#modalChangeRole'),
    });
    $("#modalChangeRole").modal();
  });
}
function hasNumber(myString) {
  return /\d/.test(myString);
}
function hasLowerCase(str) {
  return (/[a-z]/.test(str));
}
function hasUpperrCase(str) {
  return (/[A-Z]/.test(str));
}
function onPassChange(){
  password = $('#user_password').val();
  passwordNew = $('#user_password_again').val();
  if(password){
    if(hasLowerCase(password) && hasUpperrCase(password) && hasNumber(password) && password.length >= 10){
      console.log('geslo ustreza zahtevam');
      $('#user_password').removeClass("is-invalid").addClass("is-valid");
      if(password && passwordNew){
        if(password != passwordNew){
          $('#user_password_again').removeClass("is-valid").addClass("is-invalid");
          $('#btnChangePass').prop("disabled", true).removeClass("ui-state-enabled").addClass("ui-state-disabled");
        }
        else{
          $('#user_password_again').removeClass("is-invalid").addClass("is-valid");
          $('#btnChangePass').prop("disabled", false).removeClass("ui-state-disabled").addClass("ui-state-enabled");
        }
      }
    }
    else{
      console.log('geslo ne ustreza zahtevam');
      $('#user_password').removeClass("is-valid").addClass("is-invalid");
      if(password && passwordNew){
        if(password != passwordNew){
          $('#user_password_again').removeClass("is-valid").addClass("is-invalid");
          $('#btnChangePass').prop("disabled", true).removeClass("ui-state-enabled").addClass("ui-state-disabled");
        }
        else{
          $('#user_password_again').removeClass("is-invalid").addClass("is-valid");
          $('#btnChangePass').prop("disabled", false).removeClass("ui-state-disabled").addClass("ui-state-enabled");
        }
      }
    }
  }
}
function onEventOpenChangePassModal(event) {
  openChangePassModal(event.data.id);
}
function onEventOpenChangeRoleModal(event) {
  openChangeRoleModal(event.data.id);
}
//$('#user_password').on('change', onPassChange);
//$('#user_password_again').on('change', onPassChange);
var password = $('#user_password').val();
var passwordNew = $('#user_password_again').val();
var userId;
var roles;