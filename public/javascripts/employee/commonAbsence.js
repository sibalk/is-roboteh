$(function(){
  $('#taskButtonAbsence').on('click', getAbsences);
})
//FILL ABSENCE LIST
function getAbsences(){
  //debugger;
  var workersId = '';
  if(task.id_project)
    taskWorkers.forEach(w => workersId += ','+w.id_user);
  else
    taskWorkers.forEach(w => workersId += ','+w.id);
  workersId = workersId.substring(1);
  var start = task.task_start;
  var finish = task.task_finish;
  $('#absenceList').empty();
  $('#collapseSubtasks').collapse('hide');
  $('#collapseFiles').collapse('hide');
  $('#collapseAbsences').collapse('show');
  if(start && finish && (task.task_absence_count > 0 || task.full_absence_count > 0)){
    $.post('/projects/absence', {workersId, taskId, start, finish}, function(data){
      //debugger;
      var allAbsence = data.data;
      for(var i = 0; i < allAbsence.length; i++){
        for(var j = 0; j < allAbsence[i].length; j++){
          var start = new Date(allAbsence[i][j].start);
          var finish = new Date(allAbsence[i][j].finish);
          var listElement = `<div class="list-group-item" id="absence`+allAbsence[i][j].id+`">
            <div class="row">
              <div class="col-md-3 mt-2">
                <label>` + allAbsence[i][j].name + " " + allAbsence[i][j].surname + `</label>
              </div>
              <div class="col-md-4 mt-2">
                <label>`+ start.toLocaleDateString('sl') +" - "+ finish.toLocaleDateString('sl') +`</label>
              </div>
              <div class="col-md-4 mt-2 d-flex">
                <label class="ml-auto">`+allAbsence[i][j].reason+`</label>
              </div>
              <div class="col-md-1">
              </div>
            </div>
          </div>`;
          $('#absenceList').append(listElement);
        }
      }
      $('#collapseAbsences').collapse("toggle");
    })
  }
}