$(function(){
  loggedUser = {name:$('#loggedUser').html().split(" (")[0].split(" ")[0], surname:$('#loggedUser').html().split(" (")[0].split(" ")[1], role:$('#loggedUser').html().split(" (")[1].split(")")[0].toLowerCase()};
  getAllCompStatus();
  getAllProjects();
  getAllSubscribers();
  // getAllUsers();
  $('#compSubDateNew').width('93%');
  $('#compSubFinishedNew').width('93%');
  // if(typeof InstallTrigger !== 'undefined'){
  //   //firefox browser -> expand input date row inside table
  //   // $('#activityFirstDeadlineNew').width('108px');
  //   // $('#activitySecondDeadlineNew').width('108px');
  //   // $('#activityThirdDeadlineNew').width('108px');
  // }
  $('#btnCollapseCompSub').click(toggleCompSubCollapse);
  $('#btnCollapseCompSup').click(toggleCompSupCollapse);
  $('#btnNavbarCollapseCompSub').click(toggleCompSubCollapse);
  $('#btnNavbarCollapseCompSup').click(toggleCompSupCollapse);
})
function getAllProjects() {
  let data = {activeProject:1};
  $.ajax({
    type: 'GET',
    url: '/projects/all',
    contentType: 'application/x-www-form-urlencoded',
    data: JSON.stringify(data), // access in body
  }).done(function (resp) {
    if (resp.success){
      //debugger;
      // allCompSub = resp.allCompSub;
      // drawCompSubs();
      allProjects = resp.data;
      // sort projects based on their RT and STR number
      let tmp1 = allProjects.filter(p => p.project_number.substring(0,2) == 'RT' );
      let tmp2 = allProjects.filter(p => p.project_number.substring(0,3) == 'STR' );
      let tmp3 = allProjects.filter(p => p.project_number.substring(0,3) != 'STR' && p.project_number.substring(0,2) != 'RT');
      tmp1.forEach(p => p.number_order = parseInt(p.project_number.substring(2)));
      tmp2.forEach(p => p.number_order = parseInt(p.project_number.substring(3)));
      let sortedTmp1 = tmp1.sort(function(a,b){
        if (isNaN(a.number_order))
          return 1;
        else if (isNaN(b.number_order))
          return -1;
        return b.number_order - a.number_order;
      });
      let sortedTmp2 = tmp2.sort(function(a,b){
        if (isNaN(a.number_order))
          return 1;
        else if (isNaN(b.number_order))
          return -1;
        return b.number_order - a.number_order;
      });
      allRTProjects = sortedTmp1, allSTRProjects = sortedTmp2;
      allProjects = sortedTmp2.concat(sortedTmp1,tmp3);
      // add text to projects so select2 can show their name
      for (const project of allProjects) {
        project.text = project.project_number + ' - ' + project.project_name + ' (' + project.name + ')';
      }
      // allProjects.unshift({id:0,text:'Projekt'});
      $(".select2-projects").prepend('<option selected></option>').select2({
        data: allProjects,
        placeholder: "Projekt",
        allowClear: true,
      });
    }
    else{
      $('#modalAddMeeting').modal('toggle');
      $('#modalError').modal('toggle');
    }
  }).fail(function (resp) {
    //console.log('FAIL');
    //debugger;
    $('#modalAddMeeting').modal('toggle');
    $('#modalError').modal('toggle');
  }).always(function (resp) {
    //console.log('ALWAYS');
    //debugger;
  });
}
function getAllSubscribers() {
  let data = {};
  $.ajax({
    type: 'GET',
    url: '/subscribers/all',
    contentType: 'application/x-www-form-urlencoded',
    data: JSON.stringify(data), // access in body
  }).done(function (resp) {
    if (resp.success){
      //debugger;
      // allCompSub = resp.allCompSub;
      // drawCompSubs();
      allSubscribers = resp.data;
      // allSubscribers.unshift({id:0,text:'Naročnik'});
      $(".select2-subscribers").prepend('<option selected></option>').select2({
        data: allSubscribers,
        placeholder: "Naročnik",
        allowClear: true,
      });
    }
    else{
      $('#modalAddMeeting').modal('toggle');
      $('#modalError').modal('toggle');
    }
  }).fail(function (resp) {
    //console.log('FAIL');
    //debugger;
    $('#modalAddMeeting').modal('toggle');
    $('#modalError').modal('toggle');
  }).always(function (resp) {
    //console.log('ALWAYS');
    //debugger;
  });
}
// function getAllUsers() {
//   let data = {activeUser:1};
//   $.ajax({
//     type: 'GET',
//     url: '/employees/all',
//     contentType: 'application/x-www-form-urlencoded',
//     data: JSON.stringify(data), // access in body
//   }).done(function (resp) {
//     if (resp.success){
//       //debugger;
//       // allCompSub = resp.allCompSub;
//       // drawCompSubs();
//       allUsers = resp.data;
//       for (const user of allUsers) {
//         user.text = user.name + ' ' + user.surname;
//       }
//       //allUsers.unshift({id:0,text:'Napotena oseba za reševanje reklamacije'});
//       $(".select2-users").select2({
//         data: allUsers,
//         multiple: true,
//         placeholder: "Napotena oseba za reševanje reklamacije",
//       });
//     }
//     else{
//       $('#modalAddMeeting').modal('toggle');
//       $('#modalError').modal('toggle');
//     }
//   }).fail(function (resp) {
//     //console.log('FAIL');
//     //debugger;
//     $('#modalAddMeeting').modal('toggle');
//     $('#modalError').modal('toggle');
//   }).always(function (resp) {
//     //console.log('ALWAYS');
//     //debugger;
//   });
// }
function getAllCompStatus() {
  let data = {};
  $.ajax({
    type: 'GET',
    url: '/complaints/status',
    contentType: 'application/x-www-form-urlencoded',
    data: JSON.stringify(data), // access in body
  }).done(function (resp) {
    if (resp.success){
      //debugger;
      allCompStatus = resp.allCompStatus;
      for (const compStatus of allCompStatus) {
        compStatus.text = compStatus.status;
      }
      $('.select2-sub-status').select2({
        placeholder: "Status",
        data: allCompStatus,
        minimumResultsForSearch: Infinity
      });
      $('.select2-sup-status').select2({
        placeholder: "Status",
        data: allCompStatus,
        minimumResultsForSearch: Infinity
      });
    }
    else{
      $('#modalError').modal('toggle');
    }
  }).fail(function (resp) {
    //console.log('FAIL');
    //debugger;
    $('#modalError').modal('toggle');
  }).always(function (resp) {
    //console.log('ALWAYS');
    //debugger;
  });
}
function toggleCompSubCollapse() {
  if(compSubCollapseOpen){
    $('#collapseCompSub').collapse('toggle');
    compSubCollapseOpen = false;
    $('#btnCollapseCompSub').removeClass('active');
    $('#btnNavbarCollapseCompSub').removeClass('active');
  }
  else{
    if(compSupCollapseOpen){
      $('#collapseCompSup').collapse('toggle');
      compSupCollapseOpen = false;
      $('#btnCollapseCompSup').removeClass('active');
      $('#btnNavbarCollapseCompSup').removeClass('active');
    }
    $('#collapseCompSub').collapse('toggle');
    compSubCollapseOpen = true;
    $('#btnCollapseCompSub').addClass('active');
    $('#btnNavbarCollapseCompSub').addClass('active');
  }
}
function toggleCompSupCollapse() {
  if(compSupCollapseOpen){
    $('#collapseCompSup').collapse('toggle');
    compSupCollapseOpen = false;
    $('#btnCollapseCompSup').removeClass('active');
    $('#btnNavbarCollapseCompSup').removeClass('active');
  }
  else{
    if(compSubCollapseOpen){
      $('#collapseCompSub').collapse('toggle');
      compSubCollapseOpen = false;
      $('#btnCollapseCompSub').removeClass('active');
      $('#btnNavbarCollapseCompSub').removeClass('active');
    }
    $('#collapseCompSup').collapse('toggle');
    compSupCollapseOpen = true;
    $('#btnCollapseCompSup').addClass('active');
    $('#btnNavbarCollapseCompSup').addClass('active');
  }
}
function getCompChanges(compSubId, compSupId){
  debugger;
  let data = {compSubId, compSupId};
  $.ajax({
    type: 'GET',
    url: '/complaints/changes',
    contentType: 'application/json',
    data: data, // access in body
  }).done(function (resp) {
    if(resp.success){
      debugger;
      var compChanges = resp.compChanges;
      console.log('Successfully getting all the changes of selected complaint.');
      var element = "";
      var fileLabel = "";
        for(var i=0; i<compChanges.length; i++){
          if(compChanges[i].id_status == 11)
            fileLabel = " na: " + compChanges[i].comp_status;
          else
            fileLabel = "";
          element = new Date(compChanges[i].date).toLocaleDateString('sl') + ' ' + new Date(compChanges[i].date).toLocaleTimeString('sl').substring(0,5) + ", " + compChanges[i].name + " " + compChanges[i].surname + ', ' + compChanges[i].status + fileLabel + "<br />" + element;
        }
        //$('.mypopover').attr('data-content', element);
        if(compSubId){
          $('#compSubHistory'+compSubId).attr('data-content', element);
          $('[data-toggle="popover"]').popover({ html: true });
          $('#compSubHistory'+compSubId).popover('show');
        }
        else if (compSupId){
          $('#compSupHistory'+compSupId).attr('data-content', element);
          $('[data-toggle="popover"]').popover({ html: true });
          $('#compSupHistory'+compSupId).popover('show');
        }
        //$('[data-toggle="popover"]').popover({trigger: "hover"}); 
    }
    else{
      $('#modalError').modal('toggle');
    }
  }).fail(function (resp) {
    //console.log('FAIL');
    //debugger;
    $('#modalError').modal('toggle');
  }).always(function (resp) {
    //console.log('ALWAYS');
    //debugger;
  });
}
let compSubCollapseOpen = true, compSupCollapseOpen = false;
let loggedUser;
let allProjects, allSubscribers, allUsers, allCompStatus;