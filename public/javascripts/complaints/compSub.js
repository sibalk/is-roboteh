$(function(){
  //get all complaints subscribers
  //changeActiveCompSub(compSubActivity);
  getAllCompJustification();
  getCompSubWithFilters(compSubActivity, errorTypeFilter);
  getAllErrorTypes();
  $('#complaintsSubTableBody').on('change', 'select', function (event) {
    debugger
    if ($('#'+this.id).hasClass('select2-sub-status')){
      //save change status and its change
      let compSubId = this.id.substring(13);
      let compSubStatusId = $('#'+this.id).val();
      var tmpIndex = allCompSub.findIndex(c => c.id == compSubId);
      let data = {compSubId, compSubStatusId};
      $.ajax({
        type: 'PUT',
        url: '/complaints/subscribers/status',
        contentType: 'application/json',
        data: JSON.stringify(data), // access in body
      }).done(function (resp) {
        if(resp.success){
          debugger;
          //update object
          allCompSub[tmpIndex].status_id = compSubStatusId;
          allCompSub[tmpIndex].status = $('#compSubStatus'+compSubId+' option:selected').text();
          //add system change
          addChangeSystem(11,33,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,compSubId,null,null,compSubStatusId);
        }
        else{
          //change status back
          $('#compSubStatus'+compSubId).val(allCompSub[tmpIndex].status_id);
          $('#modalError').modal('toggle');
        }
      }).fail(function (resp) {
        //debugger;
        $('#compSubStatus'+compSubId).val(allCompSub[tmpIndex].status_id);
        $('#modalError').modal('toggle');
      }).always(function (resp) {
        //debugger;
      });
    }
  });
  //get changes for complaints sub
  $('#complaintsSubTableBody').on('click', 'a', function (event) {
    //debugger;
    if ($('#'+this.id).hasClass('mypopover')){
      getCompChanges(this.id.substring(14));
    }
  });
  // setTimeout(() => {
  //   //$('#projectNumber').on('change', onNewProjectNumberChange);
  // }, 200);
  let today = new Date();
  $('#compSubDateNew').val(moment().format("YYYY-MM-DD"));
  //CONF ADD COMPLAINT SUBSCRIBER - in form for req control
  $('#addCompSubForm').submit(function (e) {
    e.preventDefault();
    $form = $(this);
    //debugger;
    let number = compSubNumberNew.value;
    let date = compSubDateNew.value + ' ' + new Date().toISOString().split('T')[1].substring(0,5);
    let subscriberId = compSubSubscriberNew.value;
    let contact = compSubContactNew.value;
    let projectId = compSubProjectNew.value;
    let description = compSubDescriptionNew.value;
    let errorTypeId = compSubErrorTypeNew.value;
    //let contactUsers = $('#compSubUsersNew').val();
    let solver = compSubSolverNew.value;
    let cause = compSubCauseNew.value;
    let measure = compSubMeasureNew.value;
    let finished = (compSubFinishedNew.value) ? compSubFinishedNew.value + ' 15:00' : compSubFinishedNew.value;
    let justification = compSubJustificationNew.value;
    let cost = compSubCostNew.value
    let notes = compSubNotesNew.value;
    let status = compSubStatusNew.value;
    //POST info to route for adding new activity
    debugger;
    let data = {number, date, subscriberId, contact, projectId, description, errorTypeId, solver, cause, measure, finished, justification, cost, notes, status};
    $.ajax({
      type: 'POST',
      url: '/complaints/subscribers',
      contentType: 'application/json',
      data: JSON.stringify(data), // access in body
    }).done(function (resp) {
      if(resp.success){
        debugger;
        // //contact users for resolving the complaint
        // resp.compSub.contact_users_id = (contactUsers.length == 0) ? null : contactUsers.toString();
        // let tmpContactUsers = '';
        // resp.contUsers.forEach(cu => { tmpContactUsers += ', ' + allUsers.find(u => cu.user_id == u.id).text })
        // resp.compSub.contact_users = (contactUsers.length == 0) ? null : tmpContactUsers.substring(2);

        //subscriber label
        resp.compSub.subscriber = subscriberId ? allSubscribers.find(s => s.id == subscriberId).text : null;
        let subscriberLabel = subscriberId ? resp.compSub.subscriber : '';
        //project label
        let tmpProject = allProjects.find(p => p.id == projectId);
        resp.compSub.project_name = projectId ? tmpProject.project_name : null;
        resp.compSub.project_number = projectId ? tmpProject.project_number : null;
        let projectLabel = projectId ? (resp.compSub.project_number + ' - ' + resp.compSub.project_name) : '';
        //error type label
        resp.compSub.error_type = errorTypeId ? allErrorTypes.find(et => et.id == errorTypeId).text : null;
        let errorTypeLabel = errorTypeId ? resp.compSub.error_type : null;
        //justification label
        resp.compSub.justification = justification ? allCompJustification.find(cj => cj.id == justification).text : null;
        let justificationLabel = justification ? resp.compSub.justification : null;
        if((compSubActivity == 0 || compSubActivity == 1) && (errorTypeFilter == 0 || errorTypeFilter == errorTypeId)){
          //allCompSub.push(resp.compSub);
          //add new element on table complaints subscribers
          let finishedDateLabel = (resp.compSub.finished) ? new Date(resp.compSub.finished).toLocaleDateString('sl') : ' ';
          let deleteIcon = 'trash';
          let buttonsElem = `
          <a class="mypopover p-1" data-toggle="popover" data-trigger="focus" tabindex="0" data-content="ni zabeleženih sprememb" id="compSubHistory` + resp.compSub.id + `" data-original-title="" title="">
            <i class="fas fa-history fa-lg text-dark"></i>
          </a>
          <button class="btn btn-purchase ml-1 mb-n1" id="compSubButtonFile` + resp.compSub.id + `" data-toggle="tooltip" data-original-title="Datoteke", aria-label="Datoteke">
            <i class="fas fa-file fa-lg"></i>
          </button>`;
          if (loggedUser.role == 'admin' || loggedUser.role.substring(0,5) == 'vodja' || loggedUser.role == 'tajnik' || loggedUser.role == 'komercialist' || loggedUser.role == 'komercijalist' || loggedUser.role == 'komerciala' || loggedUser.role == 'računovodstvo'){
            buttonsElem = `
            <a class="mypopover p-1" data-toggle="popover" data-trigger="focus" tabindex="0" data-content="ni zabeleženih sprememb" id="compSubHistory` + resp.compSub.id + `" data-original-title="" title="">
              <i class="fas fa-history fa-lg text-dark"></i>
            </a>
            <button class="btn btn-danger ml-2 mb-n1" id="compSubButtonDelete` + resp.compSub.id + `" data-toggle="tooltip" data-original-title="Odstrani", aria-label="Odstrani">
              <i class="fas fa-`+deleteIcon+` fa-lg"></i>
            </button>
            <a class="btn btn-info ml-1 mb-n1" id="btnToCompSubPDF" href="/complaints/pdfs/?compSubId=` + resp.compSub.id + `">
              <i class="fas fa-file-pdf fa-lg"></i>
            </a>
            <button class="btn btn-purchase ml-1 mb-n1" id="compSubButtonFile` + resp.compSub.id + `" data-toggle="tooltip" data-original-title="Datoteke", aria-label="Datoteke">
              <i class="fas fa-file fa-lg"></i>
            </button>
            <button class="btn btn-primary ml-1 mb-n1" id="compSubButtonEdit` + resp.compSub.id + `" data-toggle="tooltip" data-original-title="Uredi", aria-label="Uredi">
              <i class="fas fa-pen fa-lg"></i>
            </button>`;
          }
          let selectOptions = '';
          let disabledProp = (loggedUser.role == 'admin' || loggedUser.role.substring(0,5) == 'vodja' || loggedUser.role == 'tajnik' || loggedUser.role == 'komercialist' || loggedUser.role == 'komerciala' || loggedUser.role == 'računovodstvo') ? '' : ' disabled';
          let statusLabel = '';
          allCompStatus.forEach(s => {
            if(resp.compSub.status_id == s.id){
              selectOptions += '<option value="'+s.id+'" selected="">'+s.status+'</option>';
              statusLabel = s.status;
            }
            else selectOptions += '<option value="'+s.id+'">'+s.status+'</option>';
          })
          resp.compSub.status = statusLabel;
          let element = `<tr id="compSub` + resp.compSub.id + `" class="">
            <td id="compSubNumber` + resp.compSub.id + `" class="text-center">` + number + `</td>
            <td id="compSubDate` + resp.compSub.id + `" class="text-center">` + new Date(resp.compSub.date).toLocaleDateString('sl') + `</td>
            <td id="compSubSubscriber` + resp.compSub.id + `">` + subscriberLabel + `</td>
            <td id="compSubContact` + resp.compSub.id + `">` + contact + `</td>
            <td id="compSubProject` + resp.compSub.id + `">` + projectLabel + `</td>
            <td id="compSubDescription` + resp.compSub.id + `">` + description + `</td>
            <td id="compSubErrorType` + resp.compSub.id + `">` + errorTypeLabel + `</td>
            <td id="compSubSolver` + resp.compSub.id + `">` + solver + `</td>
            <td id="compSubCause` + resp.compSub.id + `">` + cause + `</td>
            <td id="compSubMeasure` + resp.compSub.id + `">` + measure + `</td>
            <td id="compSubFinished` + resp.compSub.id + `">` + finishedDateLabel + `</td>
            <td id="compSubJustification` + resp.compSub.id + `">` + justificationLabel + `</td>
            <td id="compSubCost` + resp.compSub.id + `">` + cost + `</td>
            <td id="compSubNotes` + resp.compSub.id + `">` + notes + `</td>
            <td id="compSubStatusCell` + resp.compSub.id + `">
              <select class="custom-select select2-sub-status w-100" id="compSubStatus` + resp.compSub.id + `" ` + disabledProp + `>
                ` + selectOptions + `
              </select>
            </td>
            <td id="compSubButtons` + resp.compSub.id + `">
              `+buttonsElem+`
            </td>
          </tr>`;
          $('#complaintsSubTableBody').append(element);
          allCompSub.push(resp.compSub);
          $('#compSubButtonFile'+resp.compSub.id).on('click', { subId: resp.compSub.id, supId: 0 }, onClickOpenFileModal);
          if (loggedUser.role == 'admin' || loggedUser.role.substring(0,5) == 'vodja' || loggedUser.role == 'tajnik' || loggedUser.role == 'komercialist' || loggedUser.role == 'komercijalist' || loggedUser.role == 'komerciala' || loggedUser.role == 'računovodstvo'){
            $('#compSubButtonEdit'+resp.compSub.id).on('click', { id: resp.compSub.id }, onClickEditCompSub);
            $('#compSubButtonDelete'+resp.compSub.id).on('click', { id: resp.compSub.id }, onClickDeleteCompSub);
          }
        }
        //empty values of inputs in insert row
        number = number.split('/')[0], n = number.length;
        number = parseInt(number)+1 + '';
        for(let i = 0, l = n - number.length; i < l; i++) number = '0'+number;
        //let tmpNumber = number + '/' + new Date().getFullYear();
        let tmpNumber = number;
        $('#compSubNumberNew').val(tmpNumber);// make next number like in the first loading after getting all comp sub
        $('#compSubDateNew').val(moment().format('YYYY-MM-DD'));
        $('#compSubSubscriberNew').val('').trigger('change');
        $('#compSubContactNew').val('');
        $('#compSubProjectNew').val('').trigger('change');
        $('#compSubDescriptionNew').val('');
        $('#compSubErrorTypeNew').val('').trigger('change');
        $('#compSubSolverNew').val('');
        $('#compSubCauseNew').val('');
        $('#compSubMeasureNew').val('');
        $('#compSubFinishedNew').val('');
        $('#compSubJustificationNew').val('').trigger('change');
        $('#compSubCostNew').val('');
        $('#compSubNotesNew').val('');
        $('#compSubNumberNew').focus();
        //add new change
        addChangeSystem(1,33,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,resp.compSub.id);
      }
      else{
        if (resp.msg){
          $('#msgImg').attr('src','/warning_sign.png');
          $('#msg').html(resp.msg);
          $('#modalMsg').modal('toggle');
          if (resp.msg == 'Številka reklamacije že obstaja v bazi reklamacij naročnikov.'){
            isCompSubNumberFree($('#compSubNumberNew').val());
          }
          //number is already in use, get refreshed list of all complaints subscribers and redo check for used number -- NO GO, USER WILL GET CONFUSED IF HE HAS FILTER AND WILL OTHER COMPLAINTS BE SHOWN ON ITS OWN
          //getCompSubWithFilters(compSubActivity, errorTypeFilter);
        }
        else{ $('#modalError').modal('toggle'); }
      }
    }).fail(function (resp) { //console.log('FAIL'); //debugger;
      $('#modalError').modal('toggle');
    }).always(function (resp) { //console.log('ALWAYS'); //debugger;
    });
  })
  //call event functions
  $('#btnDeleteCompSub').click(deleteCompSubConf);
  $('#option0').on('click', { activity: null, errorType: 0 }, onClickFilterCompSub);
  $('#option1').on('click', { activity: null, errorType: 1 }, onClickFilterCompSub);
  $('#option2').on('click', { activity: null, errorType: 2 }, onClickFilterCompSub);
  $('#option3').on('click', { activity: null, errorType: 3 }, onClickFilterCompSub);
  $('#optionAllActivities').on('click', { activity: 0, errorType: null }, onClickFilterCompSub);
  $('#optionTrueActivities').on('click', { activity: 1, errorType: null }, onClickFilterCompSub);
  $('#optionFalseActivities').on('click', { activity: 2, errorType: null }, onClickFilterCompSub);
})
function onClickEditCompSub(params) {
  editCompSub(params.data.id)
}
//make selected compSub row editable, change from html elements to input elements
function editCompSub(id){
  editCompSubId = id;
  let tmpCompSub = allCompSub.find(cs => cs.id == id);
  $('#compSubNumber'+id).empty();//NUMBER
  let numberInput = `<div class="form-group ml-n2 mr-n2 mb-n2">
    <input class="form-control" id="compSubNumberEdit`+id+`" type="text" name="compSubNumberEdit" required="" autocomplete="off">
  </div>`;
  $('#compSubNumber'+id).append(numberInput);
  $('#compSubNumberEdit'+id).on('change', checkEditCompSubNumber);
  $('#compSubNumberEdit'+id).val(tmpCompSub.complaint_number);
  $('#compSubDate'+id).empty();//DATE
  let disableProp = (loggedUser.role != 'admin') ? 'disabled=""' : ''; // for disabling date change if its not allowed
  let dateInput = `<div class="form-group ml-n2 mr-n2 mb-n2">
    <input class="form-control hide-calendar-icon" id="compSubDateEdit`+id+`" type="date" name="compSubDateEdit" required="">
  </div>`;
  $('#compSubDate'+id).append(dateInput);
  $('#compSubDateEdit'+id).val(moment(tmpCompSub.date).format('YYYY-MM-DD'));
  $('#compSubSubscriber'+id).empty();//SUBSCRIBER
  let subscriberInput = `<div class="form-group ml-n2 mr-n2 mb-n2">
    <select class="custom-select mr-sm-2 form-control select2-subscribers-edit w-100" id="compSubSubscriberEdit`+id+`" name="compSubSubscriberEdit"></select>
  </div>`;
  $('#compSubSubscriber'+id).append(subscriberInput);
  $("#compSubSubscriberEdit"+id).prepend('<option selected></option>').select2({
    data: allSubscribers,
    placeholder: "Naročnik",
    allowClear: true,
  });
  if (tmpCompSub.subscriber) $('#compSubSubscriberEdit'+id).val(tmpCompSub.subscriber_id).trigger('change');
  $('#compSubContact'+id).empty();//CONTACT
  let contactInput = `<div class="form-group ml-n2 mr-n2 mb-n2">
    <input class="form-control" id="compSubContactEdit`+id+`" type="text" name="compSubContactEdit" autocomplete="off">
  </div>`;
  $('#compSubContact'+id).append(contactInput);
  $('#compSubContactEdit'+id).val(tmpCompSub.subscriber_contact);
  $('#compSubProject'+id).empty();//PROJECT
  let projectInput = `<div class="form-group ml-n2 mr-n2 mb-n2">
    <select class="custom-select mr-sm-2 form-control select2-projects-edit w-100" id="compSubProjectEdit`+id+`" name="compSubProjectEdit"></select>
  </div>`;
  $('#compSubProject'+id).append(projectInput);
  $("#compSubProjectEdit"+id).prepend('<option selected></option>').select2({
    data: allProjects,
    placeholder: "Projekt",
    allowClear: true,
  });
  if (tmpCompSub.project_id) $('#compSubProjectEdit'+id).val(tmpCompSub.project_id).trigger('change');
  $('#compSubDescription'+id).empty();//DESCRIPTION
  let descriptionInput = `<div class="form-group ml-n2 mr-n2 mb-n2">
    <input class="form-control" id="compSubDescriptionEdit`+id+`" type="text" name="compSubDescriptionEdit" autocomplete="off">
  </div>`;
  $('#compSubDescription'+id).append(descriptionInput);
  $('#compSubDescriptionEdit'+id).val(tmpCompSub.description);
  $('#compSubErrorType'+id).empty();//ERROR TYPE
  let errorTypeInput = `<div class="form-group ml-n2 mr-n2 mb-n2">
    <select class="custom-select mr-sm-2 form-control select2-error-type-edit w-100" id="compSubErrorTypeEdit`+id+`" name="compSubErrorTypeEdit"></select>
  </div>`;
  $('#compSubErrorType'+id).append(errorTypeInput);
  $("#compSubErrorTypeEdit"+id).prepend('<option selected></option>').select2({
    data: allErrorTypes,
    placeholder: "Vrsta napake",
    allowClear: true,
    minimumResultsForSearch: Infinity,
  });
  if (tmpCompSub.error_type_id) $('#compSubErrorTypeEdit'+id).val(tmpCompSub.error_type_id).trigger('change');
  // $('#compSubUsers'+id).empty();//USERS CONTACT
  // let usersInput = `<div class="form-group ml-n2 mr-n2 mb-n2">
  //   <select class="custom-select mr-sm-2 form-control select2-users-edit w-100" id="compSubUsersEdit`+id+`" name="compSubUsersEdit"></select>
  // </div>`;
  // $('#compSubUsers'+id).append(usersInput);
  // $("#compSubUsersEdit"+id).select2({
  //   data: allUsers,
  //   multiple: true,
  //   placeholder: "Napotena oseba za reševanje reklamacije",
  // });
  // if (tmpCompSub.contact_users_id) $('#compSubUsersEdit'+id).val(tmpCompSub.contact_users_id.split(',')).trigger('change');
  $('#compSubSolver'+id).empty();//SOLVER
  let solverInput = `<div class="form-group ml-n2 mr-n2 mb-n2">
    <input class="form-control" id="compSubSolverEdit`+id+`" type="text" name="compSubSolverEdit" autocomplete="off">
  </div>`;
  $('#compSubSolver'+id).append(solverInput);
  $('#compSubSolverEdit'+id).val(tmpCompSub.solver);
  $('#compSubCause'+id).empty();//CAUSE
  let causeInput = `<div class="form-group ml-n2 mr-n2 mb-n2">
    <input class="form-control" id="compSubCauseEdit`+id+`" type="text" name="compSubCauseEdit" autocomplete="off">
  </div>`;
  $('#compSubCause'+id).append(causeInput);
  $('#compSubCauseEdit'+id).val(tmpCompSub.cause);
  $('#compSubMeasure'+id).empty();//MEASURE
  let measureInput = `<div class="form-group ml-n2 mr-n2 mb-n2">
    <input class="form-control" id="compSubMeasureEdit`+id+`" type="text" name="compSubMeasureEdit" autocomplete="off">
  </div>`;
  $('#compSubMeasure'+id).append(measureInput);
  $('#compSubMeasureEdit'+id).val(tmpCompSub.measure);
  $('#compSubFinished'+id).empty();//DEADLINE 1
  let finishedInput = `<div class="form-group ml-n2 mr-n2 mb-n2">
    <input class="form-control hide-calendar-icon w-100" id="compSubFinishedEdit`+id+`" type="date" name="compSubFinishedEdit">
  </div>`;
  $('#compSubFinished'+id).append(finishedInput);
  if (tmpCompSub.finished) $('#compSubFinishedEdit'+id).val(moment(tmpCompSub.finished).format("YYYY-MM-DD"));
  $('#compSubJustification'+id).empty();//JUSTIFICATION
  let justificationInput = `<div class="form-group ml-n2 mr-n2 mb-n2">
    <select class="custom-select mr-sm-2 form-control select2-justification-edit w-100" id="compSubJustificationEdit`+id+`" name="compSubJustificationEdit"></select>
  </div>`;
  $('#compSubJustification'+id).append(justificationInput);
  $("#compSubJustificationEdit"+id).prepend('<option selected></option>').select2({
    data: allCompJustification,
    placeholder: "Upravičenost",
    allowClear: true,
    minimumResultsForSearch: Infinity,
  });
  if (tmpCompSub.justification_id) $('#compSubJustificationEdit'+id).val(tmpCompSub.justification_id).trigger('change');
  $('#compSubCost'+id).empty();//COST
  let costInput = `<div class="form-group ml-n2 mr-n2 mb-n2">
    <input class="form-control" id="compSubCostEdit`+id+`" type="text" name="compSubCostEdit" autocomplete="off">
  </div>`;
  $('#compSubCost'+id).append(costInput);
  $('#compSubCostEdit'+id).val(tmpCompSub.cost);
  $('#compSubNotes'+id).empty();//NOTES
  let notesInput = `<div class="form-group ml-n2 mr-n2 mb-n2">
    <input class="form-control" id="compSubNotesEdit`+id+`" type="text" name="compSubNotesEdit" autocomplete="off">
  </div>`;
  $('#compSubNotes'+id).append(notesInput);
  $('#compSubNotesEdit'+id).val(tmpCompSub.notes);
  $('#compSubButtons'+id).empty();//BUTTONS
  let editButtons = `
  <button class="btn btn-danger ml-2 mb-n1" id="compSubButtonCancel`+id+`" data-toggle="tooltip" data-original-title="Prekliči"><i class="fas fa-times fa-lg"></i></button>
  <button class="btn btn-success ml-2 mb-n1" id="compSubButtonEditConf`+id+`" data-toggle="tooltip" data-original-title="Potrdi"><i class="fas fa-check fa-lg"></i></button>`;
  $('#compSubButtons'+id).append(editButtons);
  
  $('#compSubButtonCancel'+id).on('click', { id: id }, onClickCancelEditCompSub);
  $('#compSubButtonEditConf'+id).on('click', { id: id }, onClickEditCompSubConf);
}
function onClickCancelEditCompSub(params) {
  cancelEditCompSub(params.data.id);
}
function cancelEditCompSub(id){
  debugger;
  let tmpCompSub = allCompSub.find(cs => cs.id == id);
  $('#compSubNumber'+id).empty();//NUMBER
  $('#compSubNumber'+id).html(tmpCompSub.complaint_number);
  $('#compSubDate'+id).empty();//DATE
  $('#compSubDate'+id).html(new Date(tmpCompSub.date).toLocaleDateString('sl'));
  $('#compSubSubscriber'+id).empty();//SUBSCRIBER
  if (tmpCompSub.subscriber) $('#compSubSubscriber'+id).html(tmpCompSub.subscriber); else $('#compSubSubscriber'+id).html('');
  $('#compSubContact'+id).empty();//CONTACT
  if (tmpCompSub.subscriber_contact) $('#compSubContact'+id).html(tmpCompSub.subscriber_contact); else $('#compSubContact'+id).html('');
  $('#compSubProject'+id).empty();//PROJECT
  if (tmpCompSub.project_id) $('#compSubProject'+id).html(tmpCompSub.project_number + ' - ' + tmpCompSub.project_name); else $('#compSubProject'+id).html('');
  $('#compSubDescription'+id).empty();//DESCRIPTION
  if (tmpCompSub.description) $('#compSubDescription'+id).html(tmpCompSub.description); else $('#compSubDescription'+id).html('');
  $('#compSubErrorType'+id).empty();//ERROR TYPE
  if (tmpCompSub.error_type) $('#compSubErrorType'+id).html(tmpCompSub.error_type); else $('#compSubErrorType'+id).html('');
  // $('#compSubUsers'+id).empty();//USERS CONTACT
  // if (tmpCompSub.contact_users) $('#compSubUsers'+id).html(tmpCompSub.contact_users); else $('#compSubUsers'+id).html('');
  $('#compSubSolver'+id).empty();//SOLVER
  if (tmpCompSub.solver) $('#compSubSolver'+id).html(tmpCompSub.solver); else $('#compSubSolver'+id).html('');
  $('#compSubCause'+id).empty();//CAUSE
  if (tmpCompSub.cause) $('#compSubCause'+id).html(tmpCompSub.cause); else $('#compSubCause'+id).html('');
  $('#compSubMeasure'+id).empty();//MEASURE
  if (tmpCompSub.measure) $('#compSubMeasure'+id).html(tmpCompSub.measure); else $('#compSubMeasure'+id).html('');
  $('#compSubFinished'+id).empty();//FINISHED
  if (tmpCompSub.finished) $('#compSubFinished'+id).html(new Date(tmpCompSub.finished).toLocaleDateString('sl')); else $('#compSubFinished'+id).html('');
  $('#compSubJustification'+id).empty();//JUSTIFICATION
  if (tmpCompSub.justification) $('#compSubJustification'+id).html(tmpCompSub.justification); else $('#compSubJustification'+id).html('');
  $('#compSubCost'+id).empty();//COST
  if (tmpCompSub.cost) $('#compSubCost'+id).html(tmpCompSub.cost); else $('#compSubCost'+id).html('');
  $('#compSubNotes'+id).empty();//NOTES
  if (tmpCompSub.notes) $('#compSubNotes'+id).html(tmpCompSub.notes); else $('#compSubNotes'+id).html('');
  $('#compSubButtons'+id).empty();//BUTTONS
  let filesColor = '';
  if (tmpCompSub.files_count && tmpCompSub.files_count > 0)
    filesColor = ' text-dark';
  let compSubButtons = `
  <a class="mypopover p-1" data-toggle="popover" data-trigger="focus" tabindex="0" data-content="ni zabeleženih sprememb" id="compSubHistory` + id + `" data-original-title="" title=""><i class="fas fa-history fa-lg text-dark"></i></a>
  <button class="btn btn-danger ml-2 mb-n1" id="compSubButtonDelete` + id + `" data-toggle="tooltip" data-original-title="Odstrani", aria-label="Odstrani"><i class="fas fa-trash fa-lg"></i></button>
  <a class="btn btn-info ml-1 mb-n1" id="btnToCompSubPDF" href="/complaints/pdfs/?compSubId=` + id + `"><i class="fas fa-file-pdf fa-lg"></i></a>
  <button class="btn btn-purchase ml-1 mb-n1" id="compSubButtonFile` + id + `" data-toggle="tooltip" data-original-title="Datoteke", aria-label="Datoteke"><i class="fas fa-file fa-lg` + filesColor + `"></i></button>
  <button class="btn btn-primary ml-1 mb-n1" id="compSubButtonEdit` + id + `" data-toggle="tooltip" data-original-title="Uredi", aria-label="Uredi"><i class="fas fa-pen fa-lg"></i></button>`;
  $('#compSubButtons'+id).append(compSubButtons);
  
  $('#compSubButtonFile'+id).on('click', { subId: id, supId: 0 }, onClickOpenFileModal);
  $('#compSubButtonEdit'+id).on('click', { id: id }, onClickEditCompSub);
  $('#compSubButtonDelete'+id).on('click', { id: id }, onClickDeleteCompSub);
}
function onClickEditCompSubConf(params) {
  editCompSubConf(params.data.id);
}
function editCompSubConf(id){
  let tmpCompSub = allCompSub.find(cs => cs.id == id);
  let number = $('#compSubNumberEdit'+id).val();
  let date = ($('#compSubDateEdit'+id).val()) ? $('#compSubDateEdit'+id).val() + ' ' + new Date(tmpCompSub.date).toLocaleTimeString('sl').substring(0,5) : $('#compSubDateEdit'+id).val();//time problem on change, do i take current edit time or original time on created
  let subscriberId = $('#compSubSubscriberEdit'+id).val();
  let contact = $('#compSubContactEdit'+id).val();
  let projectId = $('#compSubProjectEdit'+id).val();
  let description = $('#compSubDescriptionEdit'+id).val();
  let errorTypeId = $('#compSubErrorTypeEdit'+id).val();
  //let contactUsers = $('#compSubUsersEdit'+id).val();
  let solver = $('#compSubSolverEdit'+id).val();
  let cause = $('#compSubCauseEdit'+id).val();
  let measure = $('#compSubMeasureEdit'+id).val();
  let finished = ($('#compSubFinishedEdit'+id).val()) ? $('#compSubFinishedEdit'+id).val() + ' 15:00' : $('#compSubFinishedEdit'+id).val();
  let justificationId = $('#compSubJustificationEdit'+id).val();
  let cost = $('#compSubCostEdit'+id).val();
  let notes = $('#compSubNotesEdit'+id).val();
  debugger;
  if(!date || !number || !subscriberId || !errorTypeId){
    //missing req inputs
    $('#msgImg').attr('src','/warning_sign.png');
    $('#msg').html('Manjkajoči podatki! Prosimo vnesite vse potrebne podatke (številko reklamacije, datum, naročnika in tip napake) za uspešno posodobitev reklamacije.');
    $('#modalMsg').modal('toggle');
  }
  else{
    //send ajax put method on server
    //prepare add and delete contact users
    // let addContactUsers = [];
    // let deleteContactUsers = [];
    // let oldContactUsers = (tmpCompSub.contact_users_id) ? tmpCompSub.contact_users_id.split(',') : [];
    // oldContactUsers = oldContactUsers.map(x => parseInt(x));
    // let newContactUsers = contactUsers;
    // newContactUsers = newContactUsers.map(x => parseInt(x));
    // addContactUsers = newContactUsers.filter(x => !oldContactUsers.includes(x));
    // deleteContactUsers = oldContactUsers.filter(x => !newContactUsers.includes(x));
    debugger
    let data = {id, number, date, subscriberId, contact, projectId, description, errorTypeId, cause, measure, finished, justificationId, cost, notes, solver};
    $.ajax({
      type: 'PUT',
      url: '/complaints/subscribers',
      contentType: 'application/json',
      data: JSON.stringify(data), // access in body
    }).done(function (resp) {
      if(resp.success){
        debugger;
        tmpCompSub = resp.compSub;
        //contact users for resolving complaint
        // if (contactUsers.length > 0){
        //   tmpCompSub.contact_users_id = contactUsers.toString();
        //   let contactUsersString = '';
        //   contactUsers.forEach(c => { contactUsersString += ', ' + allUsers.find(u => c == u.id).text });
        //   tmpCompSub.contact_users = contactUsersString.substring(2);
        // }
        // else{ tmpCompSub.contact_users = null; tmpCompSub.contact_users_id = null; }
        //subscriber name
        if (subscriberId) tmpCompSub.subscriber = allSubscribers.find(s => s.id == subscriberId).text;
        else{ tmpCompSub.subscriber = null;}
        // error type label
        if (errorTypeId) tmpCompSub.error_type = allErrorTypes.find(et => et.id == errorTypeId).text;
        else{ tmpCompSub.error_type = null;}
        // justification label
        if (justificationId) tmpCompSub.justification = allCompJustification.find(cj => cj.id == justificationId).text;
        else{ tmpCompSub.justification = null;}
        // project name
        if(projectId){
          let tmpProject = allProjects.find(p => p.id == projectId);
          tmpCompSub.project_name = tmpProject.project_name;
          tmpCompSub.project_number = tmpProject.project_number;
        }
        else{ tmpCompSub.project_name = null; tmpCompSub.project_number = null;}
        debugger;
        $('#compSubNumber'+id).empty();//NUMBER
        $('#compSubNumber'+id).html(tmpCompSub.complaint_number);
        $('#compSubDate'+id).empty();//DATE
        $('#compSubDate'+id).html(new Date(tmpCompSub.date).toLocaleDateString('sl'));
        $('#compSubSubscriber'+id).empty();//SUBSCRIBER
        if (tmpCompSub.subscriber) $('#compSubSubscriber'+id).html(tmpCompSub.subscriber); else $('#compSubSubscriber'+id).html('');
        $('#compSubContact'+id).empty();//CONTACT
        if (tmpCompSub.subscriber_contact) $('#compSubContact'+id).html(tmpCompSub.subscriber_contact); else $('#compSubContact'+id).html('');
        $('#compSubProject'+id).empty();//PROJECT
        if (tmpCompSub.project_id) $('#compSubProject'+id).html(tmpCompSub.project_number + ' - ' + tmpCompSub.project_name); else $('#compSubProject'+id).html('');
        $('#compSubDescription'+id).empty();//DESCRIPTION
        if (tmpCompSub.description) $('#compSubDescription'+id).html(tmpCompSub.description); else $('#compSubDescription'+id).html('');
        $('#compSubErrorType'+id).empty();//ERROR TYPE
        if (tmpCompSub.error_type) $('#compSubErrorType'+id).html(tmpCompSub.error_type); else $('#compSubErrorType'+id).html('');
        // $('#compSubUsers'+id).empty();//USERS CONTACT
        // if (tmpCompSub.contact_users) $('#compSubUsers'+id).html(tmpCompSub.contact_users); else $('#compSubUsers'+id).html('');
        $('#compSubSolver'+id).empty();//SOLVER
        if (tmpCompSub.solver) $('#compSubSolver'+id).html(tmpCompSub.solver); else $('#compSubSolver'+id).html('');
        $('#compSubCause'+id).empty();//CAUSE
        if (tmpCompSub.cause) $('#compSubCause'+id).html(tmpCompSub.cause); else $('#compSubCause'+id).html('');
        $('#compSubMeasure'+id).empty();//MEASURE
        if (tmpCompSub.measure) $('#compSubMeasure'+id).html(tmpCompSub.measure); else $('#compSubMeasure'+id).html('');
        $('#compSubFinished'+id).empty();//FINISHED
        if (tmpCompSub.finished) $('#compSubFinished'+id).html(new Date(tmpCompSub.finished).toLocaleDateString('sl')); else $('#compSubFinished'+id).html('');
        $('#compSubJustification'+id).empty();//JUSTIFICATION
        if (tmpCompSub.justification) $('#compSubJustification'+id).html(tmpCompSub.justification); else $('#compSubJustification'+id).html('');
        $('#compSubCost'+id).empty();//COST
        if (tmpCompSub.cost) $('#compSubCost'+id).html(tmpCompSub.cost); else $('#compSubCost'+id).html('');
        $('#compSubNotes'+id).empty();//NOTES
        if (tmpCompSub.notes) $('#compSubNotes'+id).html(tmpCompSub.notes); else $('#compSubNotes'+id).html('');
        $('#compSubButtons'+id).empty();//BUTTONS
        let compSubButtons = `
        <a class="mypopover p-1" data-toggle="popover" data-trigger="focus" tabindex="0" data-content="ni zabeleženih sprememb" id="compSubHistory` + id + `" data-original-title="" title=""><i class="fas fa-history fa-lg text-dark"></i></a>
        <button class="btn btn-danger ml-2 mb-n1" id="compSubButtonDelete` + id + `" data-toggle="tooltip" data-original-title="Odstrani", aria-label="Odstrani"><i class="fas fa-trash fa-lg"></i></button>
        <a class="btn btn-info ml-1 mb-n1" id="btnToCompSubPDF" href="/complaints/pdfs/?compSubId=` + id + `"><i class="fas fa-file-pdf fa-lg"></i></a>
        <button class="btn btn-purchase ml-1 mb-n1" id="compSubButtonFile` + id + `" data-toggle="tooltip" data-original-title="Datoteke", aria-label="Datoteke"><i class="fas fa-file fa-lg"></i></button>
        <button class="btn btn-primary ml-1 mb-n1" id="compSubButtonEdit` + id + `" data-toggle="tooltip" data-original-title="Uredi", aria-label="Uredi"><i class="fas fa-pen fa-lg"></i></button>`;
        $('#compSubButtons'+id).append(compSubButtons);

        $('#compSubButtonFile'+id).on('click', { subId: id, supId: 0 }, onClickOpenFileModal);
        $('#compSubButtonEdit'+id).on('click', { id: id }, onClickEditCompSub);
        $('#compSubButtonDelete'+id).on('click', { id: id }, onClickDeleteCompSub);

        let tmp = allCompSub.findIndex(cs => cs.id == id);
        allCompSub[tmp] = tmpCompSub;
        //add new change
        addChangeSystem(2,33,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,id);
      }
      else{
        if (resp.msg){
          $('#msgImg').attr('src','/warning_sign.png');
          $('#msg').html(resp.msg);
          $('#modalMsg').modal('toggle');
          if (resp.msg == 'Številka reklamacije že obstaja v bazi reklamacij naročnikov.'){
            isCompSubNumberFree($('#compSubNumberEdit'+id).val(), id);
          }
          //number is already in use, get refreshed list of all complaints subscribers and redo check for used number
          //getCompSubWithFilters(compSubActivity, errorTypeFilter);
        }
        else
          $('#modalError').modal('toggle');
      }
    }).fail(function (resp) {//debugger;
      $('#modalError').modal('toggle');
    }).always(function (resp) {//debugger;
    });
  }
}
function onClickDeleteCompSub(params) {
  deleteCompSub(params.data.id);
}
function deleteCompSub(id){
  compSubId = id;
  let tmpCompSub = allCompSub.find(cs => cs.id == compSubId);
  if (tmpCompSub.active == true){
    $('#deleteCompSubModalMsg').html('Ste prepričani, da želite odstraniti to reklamacijo?');
    $('#btnDeleteCompSub').html('Odstrani');
  }
  else{
    $('#deleteCompSubModalMsg').html('Ste prepričani, da želite ponovno dodati to reklamacijo?');
    $('#btnDeleteCompSub').html('Ponovno dodaj');
  }
  $('#modalDeleteCompSub').modal('toggle');
}
function deleteCompSubConf(){
  let tmpCompSub = allCompSub.find(cs => cs.id == compSubId);
  let active =  (tmpCompSub.active == true) ? false : true; // IF current active is true THEN false ELSE true
  
  let data = {compSubId, active};
  $.ajax({
    type: 'DELETE',
    url: '/complaints/subscribers',
    contentType: 'application/json',
    data: JSON.stringify(data), // access in body
  }).done(function (resp) {
    //console.log('SUCCESS');
    debugger;
    if (resp.success){
      if (loggedUser.role == 'admin'){
        //marked new active on complaints subscribers table and update variables //DEPENDS ON WHICH ACTIVITY TABEL IS SHOWN
        if (compSubActivity == '0'){
          //true and false, all complaints sub are shown, update variables and  class & icon on table
          tmpCompSub.active = active;
          if (active){
            $('#compSub'+compSubId).removeClass('bg-secondary');
            $('#compSubButtonDelete'+compSubId).find('i').removeClass('fa-trash-restore');
            $('#compSubButtonDelete'+compSubId).find('i').addClass('fa-trash');
          }
          else{
            $('#compSub'+compSubId).addClass('bg-secondary');
            $('#compSubButtonDelete'+compSubId).find('i').removeClass('fa-trash');
            $('#compSubButtonDelete'+compSubId).find('i').addClass('fa-trash-restore');
          }
        }
        else if (compSubActivity == '1' || compSubActivity == '2'){
          //true, only active complaints sub are shown, same as any other role user // same for false, only non active complaints sub are shown
          allCompSub = allCompSub.filter(cs => cs.id != compSubId);
          $('#compSub'+compSubId).remove();
        }
      }
      else{
        //remove deleted complaint sub from complaints sub table and remove activity from variables
        allCompSub = allCompSub.filter(cs => cs.id != compSubId);
        $('#compSub' + compSubId).remove();
      }
      //add new change
      if (active) addChangeSystem(6,33,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,compSubId);
      else addChangeSystem(3,33,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,compSubId);

      $('#modalDeleteCompSub').modal('toggle');
    }
    else{
      $('#modalDeleteCompSub').modal('toggle');
      $('#modalError').modal('toggle');
    }
  }).fail(function (resp) {
    //console.log('FAIL');
    //debugger;
    $('#modalError').modal('toggle');
  }).always(function (resp) {
    //console.log('ALWAYS');
    //debugger;
  });
}
function onClickFilterCompSub(params) {
  getCompSubWithFilters(params.data.activity, params.data.errorType);
}
function getCompSubWithFilters(activity,errorType){
  errorTypeFilter = (errorType !== null) ? errorType : errorTypeFilter;
  compSubActivity = (activity !== null) ? activity : compSubActivity;
  let data = {compSubActivity, errorTypeFilter};
  //let data = {};
  $.ajax({
    type: 'GET',
    url: '/complaints/subscribers',
    contentType: 'application/x-www-form-urlencoded',
    data: data, // access in body
  }).done(function (resp) {
    if (resp.success){
      allCompSub = resp.allCompSub;
      drawCompSubs();
      // $('#option0').removeClass('active');
      // $('#option1').removeClass('active');
      // $('#option2').removeClass('active');
      // $('#option3').removeClass('active');
      // if(errorType == 0) $('#option0').addClass('active');
      // else if(errorType == 1) $('#option1').addClass('active');
      // else if(errorType == 2) $('#option2').addClass('active');
      // else if(errorType == 3) $('#option3').addClass('active');
      //set new number in not set yet (for first time only)
      if(!newNumberCompSubSet){
        newNumberCompSubSet = true;
        let tmpNumber;
        // if (allCompSub.length == 0) tmpNumber = '001/' + new Date().getFullYear();
        // else{
        //   let number = allCompSub[allCompSub.length - 1].complaint_number.split('/')[0], n = number.length;
        //   number = parseInt(number)+1 + '';
        //   for(let i = 0, l = n - number.length; i < l; i++) number = '0'+number;
        //   tmpNumber = number + '/' + new Date().getFullYear();
        // }
        if (allCompSub.length == 0) tmpNumber = '001';
        else{
          let number = allCompSub[allCompSub.length - 1].complaint_number.split('/')[0], n = number.length;
          number = parseInt(number)+1 + '';
          for(let i = 0, l = n - number.length; i < l; i++) number = '0'+number;
          tmpNumber = number;
        }
        $('#compSubNumberNew').val(tmpNumber);
        isCompSubNumberFree($('#compSubNumberNew').val());
        $('#compSubNumberNew').on('change', checkNewCompSubNumber);
      }
    }
    else{ $('#modalError').modal('toggle'); }
  }).fail(function (resp) {//console.log('FAIL');//debugger;
    $('#modalError').modal('toggle');
  }).always(function (resp) {//console.log('ALWAYS');//debugger;
  });
}
function getAllErrorTypes() {
  let data = {};
  $.ajax({
    type: 'GET',
    url: '/complaints/subscribers/errorTypes',
    contentType: 'application/x-www-form-urlencoded',
    data: JSON.stringify(data), // access in body
  }).done(function (resp) {
    if (resp.success){
      //debugger;
      allErrorTypes = resp.allErrorTypes;
      for (const errorType of allErrorTypes) {
        errorType.text = errorType.error_type;
      }
      //allErrorTypes.unshift({id:0, text:'Vrsta napake', error_type:null});
      $('.select2-error-types').prepend('<option selected></option>').select2({
        placeholder: "Vrsta napake",
        allowClear: true,
        data: allErrorTypes,
        minimumResultsForSearch: Infinity
      });
    }
    else{
      $('#modalError').modal('toggle');
    }
  }).fail(function (resp) {
    //console.log('FAIL');
    //debugger;
    $('#modalError').modal('toggle');
  }).always(function (resp) {
    //console.log('ALWAYS');
    //debugger;
  });
}
function drawCompSubs() {
  //let data = {compSubActivity, errorTypeFilter};
  let tmpAllCompSub;
  switch (compSubActivity) {
    case 0: tmpAllCompSub = allCompSub.filter(cs => cs.active == cs.active); break;
    case 1: tmpAllCompSub = allCompSub.filter(cs => cs.active == true); break;
    case 2: tmpAllCompSub = allCompSub.filter(cs => cs.active == false); break;
    default: tmpAllCompSub = allCompSub.filter(cs => cs.active == true); break;
  }
  switch (errorTypeFilter) {
    case 0: tmpAllCompSub = tmpAllCompSub.filter(cs => cs.error_type_id == cs.error_type_id); break;
    case 1: tmpAllCompSub = tmpAllCompSub.filter(cs => cs.error_type_id == 1); break;
    case 2: tmpAllCompSub = tmpAllCompSub.filter(cs => cs.error_type_id == 2); break;
    case 3: tmpAllCompSub = tmpAllCompSub.filter(cs => cs.error_type_id == 3); break;
    default: tmpAllCompSub = tmpAllCompSub.filter(cs => cs.error_type_id == cs.error_type_id); break;
  }
  $('#complaintsSubTableBody').empty();
  for (const compSub of tmpAllCompSub) {
    let date = new Date(compSub.date);
    let finishedDate = (compSub.finished) ? new Date(compSub.finished).toLocaleDateString('sl') : ' ';
    let compSubSubscriber = (compSub.subscriber) ? compSub.subscriber: '';
    let compSubContact = (compSub.subscriber_contact) ? compSub.subscriber_contact : '';
    let compSubProject = (compSub.project_number) ? (compSub.project_number + ' - ' + compSub.project_name) : '';
    let compSubDescription = (compSub.description) ? compSub.description : '';
    let compSubErrorType = (compSub.error_type) ? compSub.error_type : '';
    //let compSubUsers = (compSub.contact_users) ? compSub.contact_users : '';
    let compSubSolver = (compSub.solver) ? compSub.solver : '';
    let compSubCause = (compSub.cause) ? compSub.cause : '';
    let compSubMeasure = (compSub.measure) ? compSub.measure : '';
    let compSubJustification = (compSub.justification) ? compSub.justification : '';
    let compSubCost = (compSub.cost) ? compSub.cost : '';
    let compSubNotes = (compSub.notes) ? compSub.notes : '';
    let activeLabel = (compSub.active) ? '' : ' bg-secondary';
    let deleteIcon = (compSub.active) ? 'trash' : 'trash-restore';
    let filesColor = '';
    if (compSub.files_count && compSub.files_count > 0)
      filesColor = ' text-dark';
    let buttonsElem = `
    <a class="mypopover p-1" data-toggle="popover" data-trigger="focus" tabindex="0" data-content="ni zabeleženih sprememb" id="compSubHistory` + compSub.id + `" data-original-title="" title="">
      <i class="fas fa-history fa-lg text-dark"></i>
    </a>
    <button class="btn btn-purchase ml-1 mb-n1" id="compSubButtonFile` + compSub.id + `" data-toggle="tooltip" data-original-title="Datoteke", aria-label="Datoteke">
      <i class="fas fa-file fa-lg` + filesColor + `"></i>
    </button>`;
    if (loggedUser.role == 'admin' || loggedUser.role.substring(0,5) == 'vodja' || loggedUser.role == 'tajnik' || loggedUser.role == 'komercialist' || loggedUser.role == 'komercijalist'  || loggedUser.role == 'komerciala' || loggedUser.role == 'računovodstvo'){
      buttonsElem = `
      <a class="mypopover p-1" data-toggle="popover" data-trigger="focus" tabindex="0" data-content="ni zabeleženih sprememb" id="compSubHistory` + compSub.id + `" data-original-title="" title="">
        <i class="fas fa-history fa-lg text-dark"></i>
      </a>
      <button class="btn btn-danger ml-2 mb-n1" id="compSubButtonDelete` + compSub.id + `" data-toggle="tooltip" data-original-title="Odstrani", aria-label="Odstrani">
        <i class="fas fa-`+deleteIcon+` fa-lg"></i>
      </button>
      <a class="btn btn-info ml-1 mb-n1" id="btnToCompSubPDF" href="/complaints/pdfs/?compSubId=` + compSub.id + `">
        <i class="fas fa-file-pdf fa-lg"></i>
      </a>
      <button class="btn btn-purchase ml-1 mb-n1" id="compSubButtonFile` + compSub.id + `" data-toggle="tooltip" data-original-title="Datoteke", aria-label="Datoteke">
        <i class="fas fa-file fa-lg` + filesColor + `"></i>
      </button>
      <button class="btn btn-primary ml-1 mb-n1" id="compSubButtonEdit` + compSub.id + `" data-toggle="tooltip" data-original-title="Uredi", aria-label="Uredi">
        <i class="fas fa-pen fa-lg"></i>
      </button>`;
    }
    var selectOptions = '';
    var disabledProp = (loggedUser.role == 'admin' || loggedUser.role.substring(0,5) == 'vodja' || loggedUser.role == 'tajnik' || loggedUser.role == 'komercialist' || loggedUser.role == 'komerciala' || loggedUser.role == 'računovodstvo') ? '' : ' disabled';
    allCompStatus.forEach(s => {
      if(compSub.status_id == s.id) selectOptions += '<option value="'+s.id+'" selected="">'+s.status+'</option>';
      else selectOptions += '<option value="'+s.id+'">'+s.status+'</option>';
    })
    let element = `<tr id="compSub` + compSub.id + `" class="` + activeLabel + `">
      <td id="compSubNumber` + compSub.id + `" class="text-center">` + compSub.complaint_number + `</td>
      <td id="compSubDate` + compSub.id + `" class="text-center">` + date.toLocaleDateString('sl') + `</td>
      <td id="compSubSubscriber` + compSub.id + `">` + compSubSubscriber + `</td>
      <td id="compSubContact` + compSub.id + `">` + compSubContact + `</td>
      <td id="compSubProject` + compSub.id + `">` + compSubProject + `</td>
      <td id="compSubDescription` + compSub.id + `">` + compSubDescription + `</td>
      <td id="compSubErrorType` + compSub.id + `">` + compSubErrorType + `</td>
      <td id="compSubSolver` + compSub.id + `">` + compSubSolver + `</td>
      <td id="compSubCause` + compSub.id + `">` + compSubCause + `</td>
      <td id="compSubMeasure` + compSub.id + `">` + compSubMeasure + `</td>
      <td id="compSubFinished` + compSub.id + `">` + finishedDate + `</td>
      <td id="compSubJustification` + compSub.id + `">` + compSubJustification + `</td>
      <td id="compSubCost` + compSub.id + `">` + compSubCost + `</td>
      <td id="compSubNotes` + compSub.id + `">` + compSubNotes + `</td>
      <td id="compSubStatusCell` + compSub.id + `">
          <select class="custom-select status-select select2-sub-status w-100" id="compSubStatus` + compSub.id + `"`+disabledProp+`>
            `+selectOptions+`
          </select>
      </td>
      <td id="compSubButtons` + compSub.id + `">
        `+buttonsElem+`
      </td>
    </tr>`;
    $('#complaintsSubTableBody').append(element);

    $('#compSubButtonFile'+compSub.id).on('click', { subId: compSub.id, supId: 0 }, onClickOpenFileModal);
    if (loggedUser.role == 'admin' || loggedUser.role.substring(0,5) == 'vodja' || loggedUser.role == 'tajnik' || loggedUser.role == 'komercialist' || loggedUser.role == 'komercijalist' || loggedUser.role == 'komerciala' || loggedUser.role == 'računovodstvo'){
      $('#compSubButtonEdit'+compSub.id).on('click', { id: compSub.id }, onClickEditCompSub);
      $('#compSubButtonDelete'+compSub.id).on('click', { id: compSub.id }, onClickDeleteCompSub);
    }
  }
}
// search if new is already used
function isCompSubNumberFree(number, id){
  let compSubActivity = 0, errorTypeFilter = 0;
  let data = {compSubActivity, errorTypeFilter};
  //let data = {};
  $.ajax({
    type: 'GET',
    url: '/complaints/subscribers',
    contentType: 'application/x-www-form-urlencoded',
    data: data, // access in body
  }).done(function (resp) {
    if (resp.success){
      // allCompSub = resp.allCompSub;
      let tmpConflict;
      if (id){
        tmpConflict = resp.allCompSub.find(cs => cs.complaint_number == number && cs.id != id);
        if (tmpConflict){
          $('#compSubNumberEdit' + id).removeClass('is-valid');
          $('#compSubNumberEdit' + id).addClass('is-invalid');
          $('#compSubButtonEditConf' + id).prop('disabled', true);
        }
        else {
          $('#compSubNumberEdit' + id).removeClass('is-invalid');
          $('#compSubNumberEdit' + id).addClass('is-valid');
          $('#compSubButtonEditConf' + id).prop('disabled', false);
        }
      }
      else {
        tmpConflict = resp.allCompSub.find(cs => cs.complaint_number == number);
        if (tmpConflict){
          $('#compSubNumberNew').removeClass('is-valid');
          $('#compSubNumberNew').addClass('is-invalid');
        }
        else {
          $('#compSubNumberNew').removeClass('is-invalid');
          $('#compSubNumberNew').addClass('is-valid');
        }
      }
    }
    else{ $('#modalError').modal('toggle'); }
  }).fail(function (resp) {//console.log('FAIL');//debugger;
    $('#modalError').modal('toggle');
  }).always(function (resp) {//console.log('ALWAYS');//debugger;
  });
}
function checkNewCompSubNumber() {
  isCompSubNumberFree($('#compSubNumberNew').val());
}
function checkEditCompSubNumber() {
  let tmpId = this.id.replace( /^\D+/g, '');
  isCompSubNumberFree($('#compSubNumberEdit'+tmpId).val(), tmpId);
}
function getAllCompJustification() {
  let data = {};
  $.ajax({
    type: 'GET',
    url: '/complaints/subscribers/justification',
    contentType: 'application/x-www-form-urlencoded',
    data: JSON.stringify(data), // access in body
  }).done(function (resp) {
    if (resp.success){
      //debugger;
      allCompJustification = resp.allCompJustification;
      for (const elem of allCompJustification) {
        elem.text = elem.justification;
      }
      //allErrorTypes.unshift({id:0, text:'Vrsta napake', error_type:null});
      $('.select2-justification').prepend('<option selected></option>').select2({
        placeholder: "Upravičenost",
        allowClear: true,
        data: allCompJustification,
        minimumResultsForSearch: Infinity,
      });
    }
    else{
      $('#modalError').modal('toggle');
    }
  }).fail(function (resp) {
    //console.log('FAIL');
    //debugger;
    $('#modalError').modal('toggle');
  }).always(function (resp) {
    //console.log('ALWAYS');
    //debugger;
  });
}
let compSubActivity = 1, errorTypeFilter = 0;
let allCompSub, allErrorTypes;
let compSubId, editCompSubId;
let newNumberCompSubSet = false;
let allCompJustification;