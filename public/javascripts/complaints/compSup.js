$(function(){
  //get all complaints subscribers
  //changeActiveCompSub(compSubActivity);
  getCompSup(compSupActivity);
  let today = new Date();
  $('#compSupDateNew').val(moment().format("YYYY-MM-DD"));
  $('#complaintsSupTableBody').on('change', 'select', function (event) {
    debugger
    if ($('#'+this.id).hasClass('select2-sup-status')){
      //save change status and its change
      let compSupId = this.id.substring(13);
      let compSupStatusId = $('#'+this.id).val();
      var tmpIndex = allCompSup.findIndex(c => c.id == compSupId);
      let data = {compSupId, compSupStatusId};
      $.ajax({
        type: 'PUT',
        url: '/complaints/suppliers/status',
        contentType: 'application/json',
        data: JSON.stringify(data), // access in body
      }).done(function (resp) {
        if(resp.success){
          debugger;
          //update object
          allCompSup[tmpIndex].status_id = compSupStatusId;
          allCompSup[tmpIndex].status = $('#compSupStatus'+compSupId+' option:selected').text();
          //add system change
          addChangeSystem(11,34,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,compSupId,null,compSupStatusId);
        }
        else{
          //change status back
          $('#compSupStatus'+compSupId).val(allCompSup[tmpIndex].status_id);
          $('#modalError').modal('toggle');
        }
      }).fail(function (resp) {
        //debugger;
        $('#compSupStatus'+compSupId).val(allCompSup[tmpIndex].status_id);
        $('#modalError').modal('toggle');
      }).always(function (resp) {
        //debugger;
      });
    }
  });
  //get changes for complaints sup 
  $('#complaintsSupTableBody').on('click', 'a', function (event) {
    //debugger;
    if ($('#'+this.id).hasClass('mypopover')){
      getCompChanges(null, this.id.substring(14));
    }
  });
  //CONF ADD COMPLAINT SUPPLIER - in form for req control
  $('#addCompSupForm').submit(function (e) {
    e.preventDefault();
    $form = $(this);
    //debugger;
    let number = compSupNumberNew.value;
    let date = compSupDateNew.value + ' ' + new Date().toISOString().split('T')[1].substring(0,5);
    let supplier = compSupSupplierNew.value;
    let material = compSupMaterialNew.value;
    let compType = compSupCompTypeNew.value;
    let justification = compSupJustificationNew.value;
    let quantity = compSupQuantityNew.value;
    let value = compSupValueNew.value;
    let status = compSupStatusNew.value;
    //POST info to route for adding new activity
    debugger;
    let data = {number, date, supplier, material, compType, quantity, value, status, justification};
    $.ajax({
      type: 'POST',
      url: '/complaints/suppliers',
      contentType: 'application/json',
      data: JSON.stringify(data), // access in body
    }).done(function (resp) {
      if(resp.success){
        debugger;
        if(compSupActivity == 0 || compSupActivity == 1){
          //allCompSup.push(resp.compSup);
          //add new element on table complaints suppliers
          let deleteIcon = 'trash';
          let buttonsElem = `
          <a class="mypopover p-1" data-toggle="popover" data-trigger="focus" tabindex="0" data-content="ni zabeleženih sprememb" id="compSupHistory` + resp.compSup.id + `" data-original-title="" title="">
            <i class="fas fa-history fa-lg text-dark"></i>
          </a>
          <button class="btn btn-purchase ml-1 mb-n1" id="compSupButtonFile` + resp.compSup.id + `" data-toggle="tooltip" data-original-title="Datoteke", aria-label="Datoteke">
            <i class="fas fa-file fa-lg"></i>
          </button>`;
          if (loggedUser.role == 'admin' || loggedUser.role.substring(0,5) == 'vodja' || loggedUser.role == 'tajnik' || loggedUser.role == 'komercialist' || loggedUser.role == 'komercijalist' || loggedUser.role == 'komerciala' || loggedUser.role == 'računovodstvo'){
            buttonsElem = `
            <a class="mypopover p-1" data-toggle="popover" data-trigger="focus" tabindex="0" data-content="ni zabeleženih sprememb" id="compSupHistory` + resp.compSup.id + `" data-original-title="" title="">
              <i class="fas fa-history fa-lg text-dark"></i>
            </a>
            <button class="btn btn-danger ml-2 mb-n1" id="compSupButtonDelete` + resp.compSup.id + `" data-toggle="tooltip" data-original-title="Odstrani", aria-label="Odstrani">
              <i class="fas fa-`+deleteIcon+` fa-lg"></i>
            </button>
            <a class="btn btn-info ml-1 mb-n1" id="btnToCompSupPDF" href="/complaints/pdfs/?compSupId=` + resp.compSup.id + `">
              <i class="fas fa-file-pdf fa-lg"></i>
            </a>
            <button class="btn btn-purchase ml-1 mb-n1" id="compSupButtonFile` + resp.compSup.id + `" data-toggle="tooltip" data-original-title="Datoteke", aria-label="Datoteke">
              <i class="fas fa-file fa-lg"></i>
            </button>
            <button class="btn btn-primary ml-1 mb-n1" id="compSupButtonEdit` + resp.compSup.id + `" data-toggle="tooltip" data-original-title="Uredi", aria-label="Uredi">
              <i class="fas fa-pen fa-lg"></i>
            </button>`;
          }
          let selectOptions = '';
          let disabledProp = (loggedUser.role == 'admin' || loggedUser.role.substring(0,5) == 'vodja' || loggedUser.role == 'tajnik' || loggedUser.role == 'komercialist' || loggedUser.role == 'komerciala' || loggedUser.role == 'računovodstvo') ? '' : ' disabled';
          let statusLabel = '';
          allCompStatus.forEach(s => {
            if(resp.compSup.status_id == s.id){
              selectOptions += '<option value="'+s.id+'" selected="">'+s.status+'</option>';
              statusLabel = s.status;
            }
            else selectOptions += '<option value="'+s.id+'">'+s.status+'</option>';
          })
          resp.compSup.status = statusLabel;
          //justification label
          resp.compSup.justification = justification ? allCompJustification.find(cj => cj.id == justification).text : null;
          let justificationLabel = justification ? resp.compSup.justification : null;
          let element = `<tr id="compSup` + resp.compSup.id + `" class="">
            <td id="compSupNumber` + resp.compSup.id + `" class="text-center">` + number + `</td>
            <td id="compSupDate` + resp.compSup.id + `" class="text-center">` + new Date(resp.compSup.date).toLocaleDateString('sl') + `</td>
            <td id="compSupSupplier` + resp.compSup.id + `">` + supplier + `</td>
            <td id="compSupMaterial` + resp.compSup.id + `">` + material + `</td>
            <td id="compSupCompType` + resp.compSup.id + `">` + compType + `</td>
            <td id="compSupJustification` + resp.compSup.id + `">` + justificationLabel + `</td>
            <td id="compSupQuantity` + resp.compSup.id + `">` + quantity + `</td>
            <td id="compSupValue` + resp.compSup.id + `">` + value + `</td>
            <td id="compSupStatusCell` + resp.compSup.id + `">
              <select class="custom-select select2-sup-status w-100" id="compSupStatus` + resp.compSup.id + `" ` + disabledProp + `>
                ` + selectOptions + `
              </select>
            </td>
            <td id="compSupButtons` + resp.compSup.id + `">
              `+buttonsElem+`
            </td>
          </tr>`;
          $('#complaintsSupTableBody').append(element);
          allCompSup.push(resp.compSup);
          //add on click events
          $('#compSupButtonFile'+resp.compSup.id).on('click', { subId: 0, supId: resp.compSup.id }, onClickOpenFileModal);
          if (loggedUser.role == 'admin' || loggedUser.role.substring(0,5) == 'vodja' || loggedUser.role == 'tajnik' || loggedUser.role == 'komercialist' || loggedUser.role == 'komercijalist' || loggedUser.role == 'komerciala' || loggedUser.role == 'računovodstvo'){
            $('#compSupButtonEdit'+resp.compSup.id).on('click', { id: resp.compSup.id }, onClickEditCompSup);
            $('#compSupButtonDelete'+resp.compSup.id).on('click', { id: resp.compSup.id }, onClickDeleteCompSup);
          }
        }
        //empty values of inputs in insert row
        number = number.split('/')[0], n = number.length;
        number = parseInt(number)+1 + '';
        for(let i = 0, l = n - number.length; i < l; i++) number = '0'+number;
        let tmpNumber = number;
        $('#compSupNumberNew').val(tmpNumber);// make next number like in the first loading after getting all comp sub
        $('#compSupDateNew').val(moment().format("YYYY-MM-DD"));
        $('#compSupSupplierNew').val('');
        $('#compSupMaterialNew').val('');
        $('#compSupCompTypeNew').val('');
        $('#compSupJustificationNew').val('').trigger('change');
        $('#compSupQuantityNew').val('');
        $('#compSupValueNew').val('');
        $('#compSupStatusNew').val('');
        $('#compSupNumberNew').focus();
        //add new change
        addChangeSystem(1,34,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,resp.compSup.id);
      }
      else{
        if (resp.msg){
          $('#msgImg').attr('src','/warning_sign.png');
          $('#msg').html(resp.msg);
          $('#modalMsg').modal('toggle');
          if (resp.msg == 'Številka reklamacije že obstaja v bazi reklamacij dobaviteljem.'){
            isCompSupNumberFree($('#compSupNumberNew').val());
          }
          //number is already in use, get refreshed list of all complaints suppliers and redo check for used number -- NO GO, USER WILL GET CONFUSED IF HE HAS FILTER AND WILL OTHER COMPLAINTS BE SHOWN ON ITS OWN
          //getCompSup(compSupActivity);
        }
        else{ $('#modalError').modal('toggle'); }
      }
    }).fail(function (resp) { //console.log('FAIL'); //debugger;
      $('#modalError').modal('toggle');
    }).always(function (resp) { //console.log('ALWAYS'); //debugger;
    });
  })
  //call event functions
  $('#btnDeleteCompSup').click(deleteCompSupConf);
  $('#optionAllCompSup').on('click', { activity: 0 }, onClickFilterCompSup);
  $('#optionTrueCompSup').on('click', { activity: 1 }, onClickFilterCompSup);
  $('#optionFalseCompSup').on('click', { activity: 2 }, onClickFilterCompSup);
})
//on click function for editing complaint supplier
function onClickEditCompSup(params) {
  editCompSup(params.data.id);
}
//make selected compSup row editable, change from html elements to input elements
function editCompSup(id){
  editCompSupId = id;
  let tmpCompSup = allCompSup.find(cs => cs.id == id);
  $('#compSupNumber'+id).empty();//NUMBER
  let numberInput = `<div class="form-group ml-n2 mr-n2 mb-n2">
    <input class="form-control" id="compSupNumberEdit`+id+`" type="text" name="compSupNumberEdit" required="" autocomplete="off">
  </div>`;
  $('#compSupNumber'+id).append(numberInput);
  $('#compSupNumberEdit'+id).on('change', checkEditCompSupNumber);
  $('#compSupNumberEdit'+id).val(tmpCompSup.complaint_number);
  $('#compSupDate'+id).empty();//DATE
  let disableProp = (loggedUser.role != 'admin') ? 'disabled=""' : ''; // for disabling date change if its not allowed
  let dateInput = `<div class="form-group ml-n2 mr-n2 mb-n2">
    <input class="form-control hide-calendar-icon" id="compSupDateEdit`+id+`" type="date" name="compSupDateEdit" required="">
  </div>`;
  $('#compSupDate'+id).append(dateInput);
  $('#compSupDateEdit'+id).val(moment(tmpCompSup.date).format("YYYY-MM-DD"));
  $('#compSupSupplier'+id).empty();//SUPPLIER
  let supplierInput = `<div class="form-group ml-n2 mr-n2 mb-n2">
    <input class="form-control" id="compSupSupplierEdit`+id+`" type="text" name="compSupSupplierEdit" autocomplete="off">
  </div>`;
  $('#compSupSupplier'+id).append(supplierInput);
  $('#compSupSupplierEdit'+id).val(tmpCompSup.supplier);
  $('#compSupMaterial'+id).empty();//MATERIAL
  let materialInput = `<div class="form-group ml-n2 mr-n2 mb-n2">
    <input class="form-control" id="compSupMaterialEdit`+id+`" type="text" name="compSupMaterialEdit" autocomplete="off">
  </div>`;
  $('#compSupMaterial'+id).append(materialInput);
  $('#compSupMaterialEdit'+id).val(tmpCompSup.material);
  $('#compSupCompType'+id).empty();//COMPLAINT TYPE
  let compTypeInput = `<div class="form-group ml-n2 mr-n2 mb-n2">
    <input class="form-control" id="compSupCompTypeEdit`+id+`" type="text" name="compSupCompTypeEdit" autocomplete="off">
  </div>`;
  $('#compSupCompType'+id).append(compTypeInput);
  $('#compSupCompTypeEdit'+id).val(tmpCompSup.complaint_type);
  $('#compSupJustification'+id).empty();//JUSTIFICATION
  let justificationInput = `<div class="form-group ml-n2 mr-n2 mb-n2">
    <select class="custom-select mr-sm-2 form-control select2-justification-edit w-100" id="compSupJustificationEdit`+id+`" name="compSupJustificationEdit"></select>
  </div>`;
  $('#compSupJustification'+id).append(justificationInput);
  $("#compSupJustificationEdit"+id).prepend('<option selected></option>').select2({
    data: allCompJustification,
    placeholder: "Upravičenost",
    allowClear: true,
    minimumResultsForSearch: Infinity,
  });
  if (tmpCompSup.justification_id) $('#compSupJustificationEdit'+id).val(tmpCompSup.justification_id).trigger('change');
  $('#compSupQuantity'+id).empty();//QUANTITY
  let quantityInput = `<div class="form-group ml-n2 mr-n2 mb-n2">
    <input class="form-control" id="compSupQuantityEdit`+id+`" type="text" name="compSupQuantityEdit" autocomplete="off">
  </div>`;
  $('#compSupQuantity'+id).append(quantityInput);
  $('#compSupQuantityEdit'+id).val(tmpCompSup.quantity);
  $('#compSupValue'+id).empty();//VALUE
  let valueInput = `<div class="form-group ml-n2 mr-n2 mb-n2">
    <input class="form-control" id="compSupValueEdit`+id+`" type="text" name="compSupValueEdit" autocomplete="off">
  </div>`;
  $('#compSupValue'+id).append(valueInput);
  $('#compSupValueEdit'+id).val(tmpCompSup.value);
  // $('#compSupStatus'+id).empty();//STATUS
  // let statusInput = `<div class="form-group ml-n2 mr-n2 mb-n2">
  //   <input class="form-control" id="compSupStatusEdit`+id+`" type="text" name="compSupStatusEdit" autocomplete="off">
  // </div>`;
  // $('#compSupStatus'+id).append(statusInput);
  // $('#compSupStatusEdit'+id).val(tmpCompSup.status);
  $('#compSupButtons'+id).empty();//BUTTONS
  let editButtons = `
  <button class="btn btn-danger ml-2 mb-n1" id="compSupButtonCancel`+id+`" data-toggle="tooltip" data-original-title="Prekliči"><i class="fas fa-times fa-lg"></i></button>
  <button class="btn btn-success ml-2 mb-n1" id="compSupButtonEditConf`+id+`" data-toggle="tooltip" data-original-title="Potrdi"><i class="fas fa-check fa-lg"></i></button>`;
  $('#compSupButtons'+id).append(editButtons);
  $('#compSupButtonCancel'+id).on('click', { id: id }, onClickCancelEditCompSup);
  $('#compSupButtonEditConf'+id).on('click', { id: id }, onClickEditCompSupConf);
}
function onClickCancelEditCompSup(params) {
  cancelEditCompSup(params.data.id);
}
function cancelEditCompSup(id){
  debugger;
  let tmpCompSup = allCompSup.find(cs => cs.id == id);
  $('#compSupNumber'+id).empty();//NUMBER
  $('#compSupNumber'+id).html(tmpCompSup.complaint_number);
  $('#compSupDate'+id).empty();//DATE
  $('#compSupDate'+id).html(new Date(tmpCompSup.date).toLocaleDateString('sl'));
  $('#compSupSupplier'+id).empty();//DOBAVITELJ
  if (tmpCompSup.supplier) $('#compSupSupplier'+id).html(tmpCompSup.supplier); else $('#compSupSupplier'+id).html('');
  $('#compSupMaterial'+id).empty();//MATERIAL
  if (tmpCompSup.material) $('#compSupMaterial'+id).html(tmpCompSup.material); else $('#compSupMaterial'+id).html('');
  $('#compSupCompType'+id).empty();//COMPLAINT TYPE
  if (tmpCompSup.complaint_type) $('#compSupCompType'+id).html(tmpCompSup.complaint_type); else $('#compSupCompType'+id).html('');
  $('#compSupJustification'+id).empty();//JUSTIFICATION
  if (tmpCompSup.justification) $('#compSupJustification'+id).html(tmpCompSup.justification); else $('#compSupJustification'+id).html('');
  $('#compSupQuantity'+id).empty();//QUANTITY
  if (tmpCompSup.quantity) $('#compSupQuantity'+id).html(tmpCompSup.quantity); else $('#compSupQuantity'+id).html('');
  $('#compSupValue'+id).empty();//VALUE
  if (tmpCompSup.value) $('#compSupValue'+id).html(tmpCompSup.value); else $('#compSupValue'+id).html('');
  // $('#compSupStatus'+id).empty();//STATUS
  // if (tmpCompSup.status) $('#compSupStatus'+id).html(tmpCompSup.status); else $('#compSupStatus'+id).html('');
  $('#compSupButtons'+id).empty();//BUTTONS
  let compSupButtons = `
  <a class="mypopover p-1" data-toggle="popover" data-trigger="focus" tabindex="0" data-content="ni zabeleženih sprememb" id="compSupHistory` + id + `" data-original-title="" title=""><i class="fas fa-history fa-lg text-dark"></i></a>
  <button class="btn btn-danger ml-2 mb-n1" id="compSupButtonDelete` + id + `" data-toggle="tooltip" data-original-title="Odstrani", aria-label="Odstrani"><i class="fas fa-trash fa-lg"></i></button>
  <a class="btn btn-info ml-1 mb-n1" id="btnToCompSupPDF" href="/complaints/pdfs/?compSupId=` + id + `"><i class="fas fa-file-pdf fa-lg"></i></a>
  <button class="btn btn-purchase ml-1 mb-n1" id="compSupButtonFile` + id + `" data-toggle="tooltip" data-original-title="Datoteke", aria-label="Datoteke"><i class="fas fa-file fa-lg"></i></button>
  <button class="btn btn-primary ml-1 mb-n1" id="compSupButtonEdit` + id + `" data-toggle="tooltip" data-original-title="Uredi", aria-label="Uredi"><i class="fas fa-pen fa-lg"></i></button>`;
  $('#compSupButtons'+id).append(compSupButtons);
  $('#compSupButtonFile'+id).on('click', { subId: 0, supId: id }, onClickOpenFileModal);
  $('#compSupButtonEdit'+id).on('click', { id: id }, onClickEditCompSup);
  $('#compSupButtonDelete'+id).on('click', { id: id }, onClickDeleteCompSup);
}
function onClickEditCompSupConf(params) {
  editCompSupConf(params.data.id);
}
function editCompSupConf(id){
  let tmpCompSup = allCompSup.find(cs => cs.id == id);
  let number = $('#compSupNumberEdit'+id).val();
  let date = ($('#compSupDateEdit'+id).val()) ? $('#compSupDateEdit'+id).val() + ' ' + new Date(tmpCompSup.date).toLocaleTimeString('sl').substring(0,5) : $('#compSupDateEdit'+id).val();//time problem on change, do i take current edit time or original time on created
  let supplier = $('#compSupSupplierEdit'+id).val();
  let material = $('#compSupMaterialEdit'+id).val();
  let compType = $('#compSupCompTypeEdit'+id).val();
  let justificationId = $('#compSupJustificationEdit'+id).val();
  let quantity = $('#compSupQuantityEdit'+id).val();
  let value = $('#compSupValueEdit'+id).val();
  //let status = $('#compSupStatusEdit'+id).val();
  debugger;
  if(!date || !number || !supplier){
    //missing req inputs
    $('#msgImg').attr('src','/warning_sign.png');
    $('#msg').html('Manjkajoči podatki! Prosimo vnesite vse potrebne podatke (številko reklamacije, datum, dobavitelja) za uspešno posodobitev reklamacije.');
    $('#modalMsg').modal('toggle');
  }
  else{
    //send ajax put method on server
    debugger
    let data = {id, number, date, supplier, material, compType, quantity, value, status, justificationId};
    $.ajax({
      type: 'PUT',
      url: '/complaints/suppliers',
      contentType: 'application/json',
      data: JSON.stringify(data), // access in body
    }).done(function (resp) {
      if(resp.success){
        debugger;
        tmpCompSup = resp.compSup;
        debugger;
        $('#compSupNumber'+id).empty();//NUMBER
        $('#compSupNumber'+id).html(tmpCompSup.complaint_number);
        $('#compSupDate'+id).empty();//DATE
        $('#compSupDate'+id).html(new Date(tmpCompSup.date).toLocaleDateString('sl'));
        $('#compSupSupplier'+id).empty();//SUPPLIER
        if (tmpCompSup.supplier) $('#compSupSupplier'+id).html(tmpCompSup.supplier); else $('#compSupSupplier'+id).html('');
        $('#compSupMaterial'+id).empty();//MATERIAL
        if (tmpCompSup.material) $('#compSupMaterial'+id).html(tmpCompSup.material); else $('#compSupMaterial'+id).html('');
        $('#compSupCompType'+id).empty();//COMP TYPE
        if (tmpCompSup.complaint_type) $('#compSupCompType'+id).html(tmpCompSup.complaint_type); else $('#compSupCompType'+id).html('');
        $('#compSupJustification'+id).empty();//JUSTIFICATION
        if (tmpCompSup.justification) $('#compSupJustification'+id).html(tmpCompSup.justification); else $('#compSupJustification'+id).html('');
        $('#compSupQuantity'+id).empty();//QUANTITY
        if (tmpCompSup.quantity) $('#compSupQuantity'+id).html(tmpCompSup.quantity); else $('#compSupQuantity'+id).html('');
        $('#compSupValue'+id).empty();//VALUE
        if (tmpCompSup.value) $('#compSupValue'+id).html(tmpCompSup.value); else $('#compSupValue'+id).html('');
        // $('#compSupStatus'+id).empty();//STATUS
        // if (tmpCompSup.status) $('#compSupStatus'+id).html(tmpCompSup.status); else $('#compSupStatus'+id).html('');
        $('#compSupButtons'+id).empty();//BUTTONS
        let compSupButtons = `
        <a class="mypopover p-1" data-toggle="popover" data-trigger="focus" tabindex="0" data-content="ni zabeleženih sprememb" id="compSupHistory` + id + `" data-original-title="" title=""><i class="fas fa-history fa-lg text-dark"></i></a>
        <button class="btn btn-danger ml-2 mb-n1" id="compSupButtonDelete` + id + `" data-toggle="tooltip" data-original-title="Odstrani", aria-label="Odstrani"><i class="fas fa-trash fa-lg"></i></button>
        <a class="btn btn-info ml-1 mb-n1" id="btnToCompSupPDF" href="/complaints/pdfs/?compSupId=` + id + `"><i class="fas fa-file-pdf fa-lg"></i></a>
        <button class="btn btn-purchase ml-1 mb-n1" id="compSupButtonFile` + id + `" data-toggle="tooltip" data-original-title="Datoteke", aria-label="Datoteke"><i class="fas fa-file fa-lg"></i></button>
        <button class="btn btn-primary ml-1 mb-n1" id="compSupButtonEdit` + id + `" data-toggle="tooltip" data-original-title="Uredi", aria-label="Uredi"><i class="fas fa-pen fa-lg"></i></button>`;
        $('#compSupButtons' + id).append(compSupButtons);
        //adding on click events to buttons
        $('#compSupButtonFile' + id).on('click', { subId: 0, supId: id }, onClickOpenFileModal);
        $('#compSupButtonEdit' + id).on('click', { id: id }, onClickEditCompSup);
        $('#compSupButtonDelete' + id).on('click', { id: id }, onClickDeleteCompSup);

        let tmp = allCompSup.findIndex(cs => cs.id == id);
        allCompSup[tmp] = tmpCompSup;
        //add new change
        addChangeSystem(2,34,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,id);
      }
      else{
        if (resp.msg){
          $('#msgImg').attr('src','/warning_sign.png');
          $('#msg').html(resp.msg);
          $('#modalMsg').modal('toggle');
          if (resp.msg == 'Številka reklamacije že obstaja v bazi reklamacij dobaviteljem.'){
            isCompSupNumberFree($('#compSupNumberEdit'+id).val(), id);
          }
          //number is already in use, get refreshed list of all complaints subscribers and redo check for used number
          //getCompSubWithFilters(compSubActivity, errorTypeFilter);
        }
        else
          $('#modalError').modal('toggle');
      }
    }).fail(function (resp) {//debugger;
      $('#modalError').modal('toggle');
    }).always(function (resp) {//debugger;
    });
  }
}
//on click function for deleting complaint supplier
function onClickDeleteCompSup(params) {
  deleteCompSup(params.data.id);
}
//open delete modal and correct message and button
function deleteCompSup(id){
  compSupId = id;
  let tmpCompSup = allCompSup.find(cs => cs.id == compSupId);
  if (tmpCompSup.active == true){
    $('#deleteCompSupModalMsg').html('Ste prepričani, da želite odstraniti to reklamacijo?');
    $('#btnDeleteCompSup').html('Odstrani');
  }
  else{
    $('#deleteCompSupModalMsg').html('Ste prepričani, da želite ponovno dodati to reklamacijo?');
    $('#btnDeleteCompSup').html('Ponovno dodaj');
  }
  $('#modalDeleteCompSup').modal('toggle');
}
function deleteCompSupConf(){
  let tmpCompSup = allCompSup.find(cs => cs.id == compSupId);
  let active =  (tmpCompSup.active == true) ? false : true; // IF current active is true THEN false ELSE true
  
  let data = {compSupId, active};
  $.ajax({
    type: 'DELETE',
    url: '/complaints/suppliers',
    contentType: 'application/json',
    data: JSON.stringify(data), // access in body
  }).done(function (resp) {
    //console.log('SUCCESS');
    debugger;
    if (resp.success){
      if (loggedUser.role == 'admin'){
        //marked new active on complaints subscribers table and update variables //DEPENDS ON WHICH ACTIVITY TABEL IS SHOWN
        if (compSupActivity == '0'){
          //true and false, all complaints sub are shown, update variables and  class & icon on table
          tmpCompSup.active = active;
          if (active){
            $('#compSup'+compSupId).removeClass('bg-secondary');
            $('#compSupButtonDelete'+compSupId).find('i').removeClass('fa-trash-restore');
            $('#compSupButtonDelete'+compSupId).find('i').addClass('fa-trash');
          }
          else{
            $('#compSup'+compSupId).addClass('bg-secondary');
            $('#compSupButtonDelete'+compSupId).find('i').removeClass('fa-trash');
            $('#compSupButtonDelete'+compSupId).find('i').addClass('fa-trash-restore');
          }
        }
        else if (compSupActivity == '1' || compSupActivity == '2'){
          //true, only active complaints sub are shown, same as any other role user // same for false, only non active complaints sub are shown
          allCompSup = allCompSup.filter(cs => cs.id != compSupId);
          $('#compSup'+compSupId).remove();
        }
      }
      else{
        //remove deleted complaint sub from complaints sub table and remove activity from variables
        allCompSup = allCompSup.filter(cs => cs.id != compSupId);
        $('#compSup' + compSupId).remove();
      }
      //add new change
      if (active) addChangeSystem(6,34,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,compSupId);
      else addChangeSystem(3,34,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,compSupId);

      $('#modalDeleteCompSup').modal('toggle');
    }
    else{
      $('#modalDeleteCompSup').modal('toggle');
      $('#modalError').modal('toggle');
    }
  }).fail(function (resp) {
    //console.log('FAIL');
    //debugger;
    $('#modalError').modal('toggle');
  }).always(function (resp) {
    //console.log('ALWAYS');
    //debugger;
  });
}
function onClickFilterCompSup(params) {
  getCompSup(params.data.activity);
}
function getCompSup(activity){
  compSupActivity = (activity !== null) ? activity : compSupActivity;
  let data = {compSupActivity};
  $.ajax({
    type: 'GET',
    url: '/complaints/suppliers',
    contentType: 'application/x-www-form-urlencoded',
    data: data, // access in body
  }).done(function (resp) {
    if (resp.success){
      allCompSup = resp.allCompSup;
      drawCompSups();
      // $('#optionAllCompSup').removeClass('active');
      // $('#optionTrueCompSup').removeClass('active');
      // $('#optionFalseCompSup').removeClass('active');
      // if(activity == 0) $('#optionAllCompSup').addClass('active');
      // else if(activity == 1) $('#optionTrueCompSup').addClass('active');
      // else if(activity == 2) $('#optionFalseCompSup').addClass('active');
      //set new number in not set yet (for first time only)
      if(!newNumberCompSupSet){
        newNumberCompSupSet = true;
        let tmpNumber;
        if (allCompSup.length == 0) tmpNumber = '001';
        else{
          let number = allCompSup[allCompSup.length - 1].complaint_number.split('/')[0], n = number.length;
          number = parseInt(number)+1 + '';
          for(let i = 0, l = n - number.length; i < l; i++) number = '0'+number;
          //tmpNumber = number + '/' + new Date().getFullYear();
          tmpNumber = number;
        }
        $('#compSupNumberNew').val(tmpNumber);
        isCompSupNumberFree($('#compSupNumberNew').val());
        $('#compSupNumberNew').on('change', checkNewCompSupNumber);
      }
    }
    else{ $('#modalError').modal('toggle'); }
  }).fail(function (resp) {//console.log('FAIL');//debugger;
    $('#modalError').modal('toggle');
  }).always(function (resp) {//console.log('ALWAYS');//debugger;
  });
}
function drawCompSups() {
  let tmpAllCompSup;
  switch (compSupActivity) {
    case 0: tmpAllCompSup = allCompSup.filter(cs => cs.active == cs.active); break;
    case 1: tmpAllCompSup = allCompSup.filter(cs => cs.active == true); break;
    case 2: tmpAllCompSup = allCompSup.filter(cs => cs.active == false); break;
    default: tmpAllCompSup = allCompSup.filter(cs => cs.active == true); break;
  }
  $('#complaintsSupTableBody').empty();
  for (const compSup of tmpAllCompSup) {
    let date = new Date(compSup.date);
    let compSupSupplier = (compSup.supplier) ? compSup.supplier: '';
    let compSupMaterial = (compSup.material) ? compSup.material : '';
    let compSupCompType = (compSup.complaint_type) ? compSup.complaint_type : '';
    let compSupJustification = (compSup.justification) ? compSup.justification : '';
    let compSupQuantity = (compSup.quantity) ? compSup.quantity : '';
    let compSupValue = (compSup.value) ? compSup.value : '';
    let compSupStatus = (compSup.status) ? compSup.status : '';
    let activeLabel = (compSup.active) ? '' : ' bg-secondary';
    let deleteIcon = (compSup.active) ? 'trash' : 'trash-restore';
    let filesColor = '';
    if (compSup.files_count && compSup.files_count > 0)
      filesColor = ' text-dark';
    let buttonsElem = `
    <a class="mypopover p-1" data-toggle="popover" data-trigger="focus" tabindex="0" data-content="ni zabeleženih sprememb" id="compSupHistory` + compSup.id + `" data-original-title="" title="">
      <i class="fas fa-history fa-lg text-dark"></i>
    </a>
    <button class="btn btn-purchase ml-1 mb-n1" id="compSupButtonFile` + compSup.id + `" data-toggle="tooltip" data-original-title="Datoteke", aria-label="Datoteke">
      <i class="fas fa-file fa-lg` + filesColor + `"></i>
    </button>`;
    if (loggedUser.role == 'admin' || loggedUser.role.substring(0,5) == 'vodja' || loggedUser.role == 'tajnik' || loggedUser.role == 'komercialist' || loggedUser.role == 'komercijalist' || loggedUser.role == 'komerciala' || loggedUser.role == 'računovodstvo'){
      buttonsElem = `
      <a class="mypopover p-1" data-toggle="popover" data-trigger="focus" tabindex="0" data-content="ni zabeleženih sprememb" id="compSupHistory` + compSup.id + `" data-original-title="" title="">
        <i class="fas fa-history fa-lg text-dark"></i>
      </a>
      <button class="btn btn-danger ml-2 mb-n1" id="compSupButtonDelete` + compSup.id + `" data-toggle="tooltip" data-original-title="Odstrani", aria-label="Odstrani">
        <i class="fas fa-`+deleteIcon+` fa-lg"></i>
      </button>
      <a class="btn btn-info ml-1 mb-n1" id="btnToCompSupPDF" href="/complaints/pdfs/?compSupId=` + compSup.id + `">
        <i class="fas fa-file-pdf fa-lg"></i>
      </a>
      <button class="btn btn-purchase ml-1 mb-n1" id="compSupButtonFile` + compSup.id + `" data-toggle="tooltip" data-original-title="Datoteke", aria-label="Datoteke">
        <i class="fas fa-file fa-lg` + filesColor + `"></i>
      </button>
      <button class="btn btn-primary ml-1 mb-n1" id="compSupButtonEdit` + compSup.id + `" data-toggle="tooltip" data-original-title="Uredi", aria-label="Uredi">
        <i class="fas fa-pen fa-lg"></i>
      </button>`;
    }
    var selectOptions = '';
    var disabledProp = (loggedUser.role == 'admin' || loggedUser.role.substring(0,5) == 'vodja' || loggedUser.role == 'tajnik' || loggedUser.role == 'komercialist' || loggedUser.role == 'komerciala' || loggedUser.role == 'računovodstvo') ? '' : ' disabled';
    allCompStatus.forEach(s => {
      if(compSup.status_id == s.id) selectOptions += '<option value="'+s.id+'" selected="">'+s.status+'</option>';
      else selectOptions += '<option value="'+s.id+'">'+s.status+'</option>';
    })
    let element = `<tr id="compSup` + compSup.id + `" class="` + activeLabel + `">
      <td id="compSupNumber` + compSup.id + `" class="text-center">` + compSup.complaint_number + `</td>
      <td id="compSupDate` + compSup.id + `" class="text-center">` + date.toLocaleDateString('sl') + `</td>
      <td id="compSupSupplier` + compSup.id + `">` + compSupSupplier + `</td>
      <td id="compSupMaterial` + compSup.id + `">` + compSupMaterial + `</td>
      <td id="compSupCompType` + compSup.id + `">` + compSupCompType + `</td>
      <td id="compSupJustification` + compSup.id + `">` + compSupJustification + `</td>
      <td id="compSupQuantity` + compSup.id + `">` + compSupQuantity + `</td>
      <td id="compSupValue` + compSup.id + `">` + compSupValue + `</td>
      <td id="compSupStatusCell` + compSup.id + `">
        <select class="custom-select status-select select2-sup-status w-100" id="compSupStatus` + compSup.id + `"`+disabledProp+`>
          `+selectOptions+`
        </select>
      </td>
      <td id="compSupButtons` + compSup.id + `" class="text-center">
        `+buttonsElem+`
      </td>
    </tr>`;
    $('#complaintsSupTableBody').append(element);
    $('#compSupButtonFile'+compSup.id).on('click', { subId: 0, supId: compSup.id }, onClickOpenFileModal);
    if (loggedUser.role == 'admin' || loggedUser.role.substring(0,5) == 'vodja' || loggedUser.role == 'tajnik' || loggedUser.role == 'komercialist' || loggedUser.role == 'komercijalist' || loggedUser.role == 'komerciala' || loggedUser.role == 'računovodstvo'){
      $('#compSupButtonEdit'+compSup.id).on('click', { id: compSup.id }, onClickEditCompSup);
      $('#compSupButtonDelete'+compSup.id).on('click', { id: compSup.id }, onClickDeleteCompSup);
    }
  }
}
// search if number is already used
function isCompSupNumberFree(number, id){
  let compSupActivity = 1;
  let data = {compSupActivity};
  $.ajax({
    type: 'GET',
    url: '/complaints/suppliers',
    contentType: 'application/x-www-form-urlencoded',
    data: data, // access in body
  }).done(function (resp) {
    if (resp.success){
      // allCompSup = resp.allCompSup;
      let tmpConflict;
      if (id){
        tmpConflict = resp.allCompSup.find(cs => cs.complaint_number == number && cs.id != id);
        if (tmpConflict){
          $('#compSupNumberEdit' + id).removeClass('is-valid');
          $('#compSupNumberEdit' + id).addClass('is-invalid');
          $('#compSupButtonEditConf' + id).prop('disabled', true);
        }
        else {
          $('#compSupNumberEdit' + id).removeClass('is-invalid');
          $('#compSupNumberEdit' + id).addClass('is-valid');
          $('#compSupButtonEditConf' + id).prop('disabled', false);
        }
      }
      else {
        tmpConflict = resp.allCompSup.find(cs => cs.complaint_number == number);
        if (tmpConflict){
          $('#compSupNumberNew').removeClass('is-valid');
          $('#compSupNumberNew').addClass('is-invalid');
        }
        else {
          $('#compSupNumberNew').removeClass('is-invalid');
          $('#compSupNumberNew').addClass('is-valid');
        }
      }
    }
    else{ $('#modalError').modal('toggle'); }
  }).fail(function (resp) {//console.log('FAIL');//debugger;
    $('#modalError').modal('toggle');
  }).always(function (resp) {//console.log('ALWAYS');//debugger;
  });
}
function checkNewCompSupNumber() {
  isCompSupNumberFree($('#compSupNumberNew').val());
}
function checkEditCompSupNumber() {
  let tmpId = this.id.replace( /^\D+/g, '');
  isCompSupNumberFree($('#compSupNumberEdit'+tmpId).val(), tmpId);
}
let compSupActivity = 1;
let allCompSup;
let compSupId, editCompSupId;
let newNumberCompSupSet = false;