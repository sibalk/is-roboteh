$(function(){
  // submit new file for complaint
  $("#addDocForm").submit(function (e) {
    //event.preventDefault();
    e.preventDefault();
  
    let file = document.getElementById("docNew").files[0];
    let formData = new FormData();
    formData.append("docNew", file);
    //formData.append("taskId", activeTaskId);
    formData.append("compSubFileId", compSubFileId);
    formData.append("compSupFileId", compSupFileId);
    //console.log(formData);
    debugger;
    
    $.ajax({
      url: '/complaints/fileUpload',
      type: 'POST',
      data: formData,
      contentType: false,
      processData: false,
      success: function(res) {
        //console.log('success');
        $('#btnAddDoc').attr('disabled',false);
        $('#docNew').val('');
        //addSystemChange(23,1,workOrderId,workOrderFileId);
        let compFileId = res.file.id;
        addChangeSystem(13,35,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,compSubFileId,compSupFileId,compFileId);
        //color files icon to black and +1 for files_count if exist otherwise set to 1
        if (compSubFileId){
          $('#compSubButtonFile'+compSubFileId).find('i').css('color', '#000000');
          let tmp = allCompSub.find(cs => cs.id == compSubFileId);
          if (tmp.files_count){
            tmp.files_count = (parseInt(tmp.files_count) + 1) + '';
          }
          else
            tmp.files_count = '1';
        }
        else if (compSupFileId){
          $('#compSupButtonFile'+compSupFileId).find('i').css('color', '#000000');
          let tmp = allCompSup.find(cs => cs.id == compSupFileId);
          if (tmp.files_count){
            tmp.files_count = (parseInt(tmp.files_count) + 1) + '';
          }
          else
            tmp.files_count = '1';
        }
        $('#modalDocumentAddForm').modal('toggle');
        //add new file to the list of all files to this work order
        let fullDate = new Date(res.file.date).toLocaleString("sl");
        let date = fullDate.substring(0,11);
        let time = fullDate.substring(11,16);
        let fileSrc = '/file'+res.file.type+'.png'
        let tmpElem = '', tmpTag = '';
        if (res.file.base64 && res.file.type == 'IMG'){
          fileSrc = res.file.base64;
          tmpElem = `<div class="overlay-zoom">
            <div class="icon-overlay">
              <i class="fas fa-search-plus"></i>
            </div>
          </div>`;
          tmpTag = `<div class="overlay-tag"">
            <img class="img-fluid" src="/imgTag.png" />
          </div>`;
        }
        else if (res.file.base64 && res.file.type == 'PDF'){
          // convertPDFCanvasToImg(res.file.base64, res.file.id);
          tmpElem = `<div class="overlay-zoom">
            <div class="icon-overlay">
              <i class="fas fa-search-plus"></i>
            </div>
          </div>`;
          tmpTag = `<div class="overlay-tag"">
            <img class="img-fluid" src="/pdfTag.png" />
          </div>`;
        }
        let listElement = `<div class="list-group-item" id="file`+res.file.id+`">
          <div class="row">
            <div class="col-sm-3 justify-content-center d-flex">
              <div class="preview-icon">
                <img class="img-fluid bg-light image-icon w-70px" src=` + fileSrc + ` alt="ikona" />
                ` + tmpElem + `
                ` + tmpTag + `
              </div>
            </div>
            <div class="col-sm-7">
              <label class="row mt-2">`+res.file.original_name+`</label>
              <label class="row mt-2 small">Naloženo: `+date+` ob `+time+` Avtor: `+loggedUser.name+" "+loggedUser.surname+`</label>
            </div>
            <div class="col-sm-2">
              <div class="d-flex">
                <button class="btn btn-danger ml-auto mr-2 mt-3" id="fileButtonDelete`+res.file.id+`" data-toggle="tooltip" data-original-title="Odstrani">
                  <i class="fas fa-trash fa-lg"></i>
                </button>
                <a class="btn btn-primary mt-3" href="/complaints/sendMeDoc?filename=`+res.file.path_name+`">
                  <i class="fas fa-download fa-lg"></i>
                </a>
              </div>
            </div>
          </div>
        </div>`;
        $('#fileList').prepend(listElement);
        if (res.file.base64 && res.file.type == 'IMG')
          $('#file' + res.file.id).find('.overlay-zoom').on('click', {id: res.file.id}, onClickGetIMGData);
        else if (res.file.base64 && res.file.type == 'PDF')
          $('#file' + res.file.id).find('.overlay-zoom').on('click', {id: res.file.id}, onClickGetPDFData);
        $('#file' + res.file.id).find('.btn-danger').on('click', {id: res.file.id}, onClickDeleteFile);
        allFiles.unshift({
          active: res.file.active,
          author: res.file.author,
          date: res.file.date,
          id: res.file.id,
          comp_sub_id: res.file.comp_sub_id,
          comp_sup_id: res.file.comp_sup_id,
          name: loggedUser.name,
          note: res.file.note,
          original_name: res.file.original_name,
          path_name: res.file.path_name,
          surname: loggedUser.surname,
          type: res.file.type,
        });
        $('#modalFiles').modal('toggle');
        //prepare modalMsg for info about success
        //$('#msgImg').attr('src','/ok_sign.png');
        //$('#msg').html('Uspešno nalaganje nove datoteke.');
        //$('#modalMsg').modal('toggle');
        if (res.file.type == 'PDF' && res.file.base64){
          setTimeout(()=>{
            convertPDFCanvasToImg(res.file.base64, res.file.id);
          },500)
        }
      },
      error: function(res) {
        //console.log('error');
        $('#btnAddDoc').attr('disabled',false);
        $('#docNew').val('');
        $('#modalDocumentAddForm').modal('toggle')
        //prepare modalMsg for info about success
        $('#msgImg').attr('src','/warning_sign.png');
        $('#msg').html(res.responseJSON.message);
        $('#modalMsg').modal('toggle');
      }
    })
  });
  $('#btnDeleteFile').on('click', deleteFileConf);
  $('#btnAddNewFile').click(addNewFileModal);
  $('#btnAddDoc').click(onClickDisableFunction);
})
function onClickOpenFileModal(params) {
  openFileModal(params.data.subId, params.data.supId);
}
function openFileModal(subId,supId) { //depends on which complaint was clicked (complaints for subscribers or suppliers) and get files for correct complaint type id
  if (compSubFileId == subId || compSupFileId == supId){
    //only toggle modal cuz files are already loaded
    $('#modalFiles').modal('toggle');
    //compSupFileId = supId ? supId : null;
    //compSubFileId = subId ? subId : null;
  }
  else {
    // new id, get files, empty modal, fill it with new files then open modal
    compSupFileId = (supId != 0) ? supId : null;
    compSubFileId = (subId != 0) ? subId : null;
    //get files
    let data = {compSubFileId, compSupFileId};
    $.ajax({
      type: 'GET',
      url: '/complaints/files',
      contentType: 'application/x-www-form-urlencoded',
      data: data, // access in body
    }).done(function (resp) {
      if (resp.success){
        //allFiles = resp.data;
        drawFiles(resp.data)
      }
      else{ $('#modalError').modal('toggle'); }
    }).fail(function (resp) {//console.log('FAIL');//debugger;
      $('#modalError').modal('toggle');
    }).always(function (resp) {//console.log('ALWAYS');//debugger;
    });
  }
}
function drawFiles(files) {
  $('#fileList').empty();
  for(let i = 0; i < files.length; i++){
    let deleteButton = ``;
    let downloadClassStyle = '';
    if ((files[i].name + ' ' + files[i].surname) == (loggedUser.name + ' ' + loggedUser.surname) || loggedUser.role == 'admin' || loggedUser.role == 'tajnik' || loggedUser.role == 'komercialist' || loggedUser.role == 'komerciala' || loggedUser.role == 'računovodstvo'){
      deleteButton = `<button class="btn btn-danger ml-auto mr-2 mt-3" id="fileButtonDelete`+files[i].id+`" data-toggle="tooltip" data-original-title="Odstrani">
        <i class="fas fa-trash fa-lg"></i>
      </button>`;
    }
    else{
      downloadClassStyle = ' ml-auto';
    }
    let downloadButton = `<a class="btn btn-primary mt-3`+downloadClassStyle+`" href="/complaints/sendMeDoc?filename=`+files[i].path_name+`">
      <i class="fas fa-download fa-lg"></i>
    </a>`;
    if(files[i].active == false)
      downloadButton = `<a class="btn btn-primary mt-3 disabled`+downloadClassStyle+`" href=""><i class="fas fa-download fa-lg"></i></a>`;
    let fullDate = new Date(files[i].date).toLocaleString("sl");
    let date = fullDate.substring(0,11);
    let time = fullDate.substring(12,17);
    let fileSrc = '/file'+files[i].type+'.png'
    let element = `<div class="list-group-item" id="file`+files[i].id+`">
      <div class="row">
        <div class="col-sm-3 justify-content-center d-flex">
          <div class="preview-icon">
            <img class="img-fluid bg-light image-icon w-70px" src=` + fileSrc + ` alt="ikona">
          </div>
        </div>
        <div class="col-sm-7">
          <label class="row mt-2">`+files[i].original_name+`</label>
          <label class="row mt-2 small">Naloženo: `+date+` ob `+time+` Avtor: ` + files[i].name + " " + files[i].surname + `</label>
        </div>
        <div class="col-sm-2">
          <div class="d-flex">
            `+deleteButton+`
            `+downloadButton+`
          </div>
        </div>
      </div>
    </div>`;
    $('#fileList').append(element);
    $('#file' + files[i].id).find('.btn-danger').on('click', {id: files[i].id}, onClickDeleteFile);
    //if (i == 0 && files[i].type == 'IMG'){
    //  getFileThumbnail(files[i].id, files[i].path_name, files[i].type);
    //}
  }
  allFiles = files;
  $('#modalFiles').modal('toggle');
  setTimeout(()=>{
    getFileThumbnails(allFiles);
  },500)
}
function addNewFileModal() {
  $('#modalDocumentAddForm').modal('toggle');
  $('#modalFiles').modal('toggle');
}
function onClickDisableFunction() {
  setTimeout(disableFunction, 1);
}
function disableFunction(){
  $('#btnAddDoc').attr('disabled','disabled');
}
function getFileThumbnails(allFiles, isTask) {
  for(let i = 0, l = allFiles.length; i < l; i++){
    if(allFiles[i].type == 'IMG' || allFiles[i].type == 'PDF'){
      let fileId = allFiles[i].id, filePath = allFiles[i].path_name, fileType = allFiles[i].type;
      $.get('/complaints/files/thumbnail', {fileId, filePath, fileType}, function(data){
        if (data.success){
          //tmpFileInfo = data.data;
          if(data.type == 'IMG'){
            $('#file'+data.id).find('img').prop('src', data.data);
            let tmpElem = `<div class="overlay-zoom">
              <div class="icon-overlay">
                <i class="fas fa-search-plus"></i>
              </div>
            </div>`;
            let tmpTag = `<div class="overlay-tag"">
              <img class="img-fluid" src="/imgTag.png" />
            </div>`
            $('#file' + data.id).find('div .preview-icon').append(tmpElem);
            $('#file' + data.id).find('div .preview-icon').append(tmpTag);
            $('#file' + data.id).find('.overlay-zoom').on('click', {id: data.id}, onClickGetIMGData);
          }
          else if(data.type == 'PDF'){
            //tmpFileInfo = data;
            //tmpPDFThumbnailData.push({fileId: data.id, fileBase64: data.data});
            convertPDFCanvasToImg(data.data, data.id);
            let tmpElem = `<div class="overlay-zoom">
              <div class="icon-overlay">
                <i class="fas fa-search-plus"></i>
              </div>
            </div>`;
            let tmpTag = `<div class="overlay-tag"">
              <img class="img-fluid" src="/pdfTag.png" />
            </div>`;
            $('#file' + data.id).find('div .preview-icon').append(tmpElem);
            $('#file' + data.id).find('div .preview-icon').append(tmpTag);
            $('#file' + data.id).find('.overlay-zoom').on('click', {id: data.id}, onClickGetPDFData);
          }
        }
        else{
          if (data.type == 'IMG'){
            if (isTask)
              $('#taskFile'+data.id).find('img').prop('src', '/fileNoIMG.png');
            else
              $('#file'+data.id).find('img').prop('src', '/fileNoIMG.png');
          }
          else if(data.type == 'PDF'){
            if (isTask)
              $('#taskFile'+data.id).find('img').prop('src', '/fileNoPDF.png');
            else
              $('#file'+data.id).find('img').prop('src', '/fileNoPDF.png');
          }
          //console.log("error while getting thumnail or file does not exist");
        }
      });
    }
  }
}
function convertPDFCanvasToImg(pdfData, fileId) {
  let loadingTask = pdfjsLib.getDocument({ data: atob(pdfData), });
  loadingTask.promise.then(function(pdf) {
    let canvasdiv = document.getElementById('the-canvas');
    let totalPages = pdf.numPages
    let data = [];
      
    pdf.getPage(1).then(function(page) {
      let scale = 1.5;
      let viewport = page.getViewport({ scale: scale });

      let canvas = document.createElement('canvas');
      canvasdiv.appendChild(canvas);

      // Prepare canvas using PDF page dimensions
      let context = canvas.getContext('2d');
      canvas.height = viewport.height;
      canvas.width = viewport.width;

      // Render PDF page into canvas context
      let renderContext = { canvasContext: context, viewport: viewport };

      let renderTask = page.render(renderContext);
      renderTask.promise.then(function() {
        data.push(canvas.toDataURL('image/png'))
        //tmpData.push(data);
        $('#file'+fileId).find('img.image-icon').prop('src', data[0]);
        //$('#file'+fileId).find('img.image-icon').css('width', '70px');
        //console.log(data.length + ' page(s) loaded in data')
      });
    })
  })
}
function onClickGetPDFData(event) {
  getPDFData(event.data.id, event.data.filePath);
}
function getPDFData(id) {
  //for testing, try getting data of pdf to show it
  let tmp = allFiles.find(f => f.id == id);
  let fileId = id, filePath = tmp.path_name, fileType = tmp.type;
  tmp = `<embed src="/complaints/files/resources/?fileName=` + filePath + `" class="pdfobject embeded-pdf" type="application/pdf">`
  let height = $('#file'+id).find('img').css('height').match(/[\d\.]+/);
  let width = $('#file'+id).find('img').css('width').match(/[\d\.]+/);
  if (Math.ceil(parseFloat(height)/parseFloat(width) * 100)/100 == 1.42){
    $("#pdfPreviewContainer").removeClass('document-content');
    $("#pdfPreviewContainer").addClass('document-content-vertical-pdf');
  }
  else {
    $("#pdfPreviewContainer").removeClass('document-content-vertical-pdf');
    $("#pdfPreviewContainer").addClass('document-content');
  }
  $("#pdfPreviewContainer").empty();
  $("#imgPreviewContainer").empty();
  $('#imgPreviewContainer').css('display', 'none');
  $('#pdfPreviewContainer').css('display', '');
  $('#modalPDFPreview').modal('toggle');
  $("#pdfPreviewContainer").append(tmp);
}
function onClickGetIMGData(event) {
  getIMGData(event.data.id, event.data.filePath);
}
function getIMGData(id) {
  //for testing, try getting data of pdf to show it
  let tmp = allFiles.find(f => f.id == id);
  let fileId = id, filePath = tmp.path_name, fileType = tmp.type;
  tmp = `<img class="modal-img img-fluid" src="/complaints/files/resources/?fileName=` + filePath + `">`
  $("#pdfPreviewContainer").empty();
  $("#imgPreviewContainer").empty();
  $('#imgPreviewContainer').css('display', '');
  $('#pdfPreviewContainer').css('display', 'none');
  $('#modalPDFPreview').modal('toggle');
  $("#imgPreviewContainer").append(tmp);
}
function onClickDeleteFile(event) {
  deleteFile(event.data.id);
}
function deleteFile(id) {
  debugger;
  savedFileId = id;
  $('#modalFiles').modal('toggle');
  $('#modalDeleteFile').modal('toggle');
}
function deleteFileConf() {
  //delete file with id that is same as savedFileId --> send savedFileId to complaints/file DELETE
  debugger;
  let data = {fileId:savedFileId};
  $.ajax({
    type: 'DELETE',
    url: '/complaints/files',
    contentType: 'application/json',
    data: JSON.stringify(data), // access in body
  }).done(function (resp) {
    //console.log('SUCCESS');
    debugger;
    if (resp.success){
      //file was successfully deleted and marked as deleted, remove all files listing in variables and DOM elements
      allFiles = allFiles.filter(pf => pf.id != resp.file.id);
      $('#file'+resp.file.id).remove();
      $('#modalDeleteFile').modal('toggle');
      $('#modalFiles').modal('toggle');
      addChangeSystem(14,35,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,resp.file.comp_sub_id,resp.file.comp_sup_id,resp.file.id);
    }
    else{
      $('#modalDeleteFile').modal('toggle');
      $('#modalError').modal('toggle');
    }
  }).fail(function (resp) {
    //console.log('FAIL');
    //debugger;
    $('#modalError').modal('toggle');
  }).always(function (resp) {
    //console.log('ALWAYS');
    //debugger;
  });
}
let compSubFileId = null, compSupFileId = null, filedId;
let allFiles, savedFileId;