//IN THIS SCRIPT//
//// refresh changes body, drawing changes in body, draw changes based on input in control section ////

$(function(){
  activeUserRole = $('#loggedUser').html().split(" (")[1].split(")")[0].toLowerCase();
  //get user info from page
  loggedUser = {name:$('#loggedUser').html().split(" (")[0].split(" ")[0], surname:$('#loggedUser').html().split(" (")[0].split(" ")[1], role:$('#loggedUser').html().split(" (")[1].split(")")[0].toLowerCase()};
  $.get( "/projects/users", function( data ) {
    allUsers = data.data;
    userId = allUsers.find(u => u.text == (loggedUser.name+" "+loggedUser.surname)).id;
    $(".select2-users").select2({
      data: allUsers,
    });
    $(".select2-special-users").select2({
      data: allUsers,
      multiple: true
    });
  });
  $.get( "/changes/types", function( data ) {
    allTypes = data.data;
    allTypes.unshift({id:0, text: "Vsi"});
    $(".select2-type").select2({
      data: allTypes,
      placeholder: "Tip obvestila"
    });
  });
  $.get( "/changes/status", function( data ) {
    allStatus = data.data;
    allStatus.unshift({id:0, text: "Vsi"});
    $(".select2-status").select2({
      data: allStatus,
      search: false,
    });
  });
  setTimeout(()=>{
    $('#userSelect').val(userId).trigger('change');
    setTimeout(()=>{
      getInputChanges();
    },100);
  },100);
  $('#change_start').on('change', ctrlStartControl);
  $('#change_finish').on('change', ctrlFinishControl);
  $('#radioAllUsers').on('change', specialRadioDisable);
  $('#radioSpecificUsers').on('change', specialRadioEnable);
  $('#addSpecialNotificationForm').submit(function(e){
    e.preventDefault();
    $form = $(this);
    
    debugger;
    var note = $('#specialNote').val();
    var notify; //will send id users through users since it will be always 1
    var control;
    if($('#radioAllUsers').prop('checked')){
      notify = JSON.stringify(allUsers);
      control = true;
    }
    else if($('#radioSpecificUsers').prop('checked')){
      notify = JSON.stringify($('#specialNoteUsers').val());
      control = false;
    }
    if(note.length >= 500){
      $('#specialNote').addClass('is-invalid');
    }
    else{
      $('#specialNote').removeClass('is-invalid');
      if(!control && $('#specialNoteUsers').val() == 0){
        $('#specialNoteUsers').addClass('is-invalid');
      }
      else{
        $('#specialNoteUsers').removeClass('is-invalid');
        $.post('/changes/special', {notify,note,control}, function(resp){
          if(resp.success){
            console.log("Pomembno obvestilo se je uspešno dodalo.");
            $('#modalAddSpecialNotification').modal('toggle');
          }
          else
            console.log("Pri dodajanju pomembnega obvestila je prišlo do napake.");
        })
      }
    }
  })
  // add event on click functions
  $('#scpecialNotificationBtn').on('click', addSpecialNotification);
  $('#optionAll').on('click', changeActivity);
  $('#optionSeen').on('click', changeActivity);
  $('#optionUnseen').on('click', changeActivity);
  $('#optionReverse').on('click', changeReverse);
  $('#getChangesBtn').on('click', getInputChanges);
  $('#changesBody').on('click', 'button', function (event) {
    //debugger
    changeChangeActivity(event.currentTarget.offsetParent.id.substring(6));
  });
})
//get new changes based on input and call drawing function
function getInputChanges(){
  var userId = $('#userSelect').val();
  var type = $('#change_type').val();
  var status = $('#change_status').val();
  var start = $('#change_start').val();
  var finish = $('#change_finish').val();
  var rev = reverseChanges;
  var active;
  if($('#optionAll').hasClass('active'))
    active = "all";
  else if($('#optionSeen').hasClass('active'))
    active = false;
  else if($('#optionUnseen').hasClass('active'))
    active = true;
  if(type == 0)
    type = null;
  if(status == 0)
    status = null;
  //debugger;
  $.get('/changes/get', {userId,type,status,start,finish,active,rev,first}, function(data){
    first = false;
    ctrlChanges = data.data;
    $('#changesBody').empty();
    drawInputChanges(ctrlChanges);
  })
}
//draw new Notifications in changesBody
function drawInputChanges(changes){
  changes.sort(compareValues('id', 'asc'));
  for(var i = 0; i < changes.length; i++){
    var element = ``;
    var link = ``;
    var forLabel = ``;
    var colorLabelSeen = '-dark';
    if(reverseChanges)
      forLabel = ' za <strong>' + changes[i].for_name + ' ' + changes[i].for_surname + '</strong>';
    var activeLabel= `<button class="close" type="button"><i class="fas fa-eye fa-lg"></i></button>`;
    if(changes[i].active){
      activeLabel = `<button class="close" type="button"><i class="fas fa-eye-slash fa-lg"></i></button>`;
      colorLabelSeen = '';
    }
    var colorLabel;
    if(changes[i].status == 1 || changes[i].status == 6) colorLabel = 'alert-add';
    else if(changes[i].status == 2) colorLabel = 'alert-edit';
    else if(changes[i].status == 3 || changes[i].status == 5) colorLabel = 'alert-delete';
    else if(changes[i].status == 4) colorLabel = 'alert-complete';
    if(changes[i].type == 1){
      //project
      if(activeUserRole == 'admin' || activeUserRole.substring(0,5) == 'vodja')
        link = `/projects/id?id=`+changes[i].id_project+`&type=`+changes[i].type+`&status=`+changes[i].status;
      else
        link = `/employees/tasks?userId=`+changes[i].for+`&projectId=`+changes[i].id_project+`&taskId=`+changes[i].id_task;
      element = `<div class="alert alert-dismissible notification-alert `+colorLabel+colorLabelSeen+`" id=change`+changes[i].id+`>
        `+activeLabel+`
        <strong>` + changes[i].user_name + ' ' + changes[i].user_surname + `</strong>
        je `+changes[i].status_label+' '+changes[i].type_label+`  
        <a class="info-link" href="`+link+`">`+changes[i].project_name+`</a>
        `+forLabel+`.
        <div class="card-subtitle-message sender notification-date">`+new Date(changes[i].date).toLocaleString('sl')+`</div>
      </div>`;
      $('#changesBody').prepend(element);
    }
    else if(changes[i].type == 2){
      //task
      if(activeUserRole == 'admin' || activeUserRole.substring(0,5) == 'vodja')
        link = `/projects/id?id=`+changes[i].id_project+`&taskId=`+changes[i].id_task+`&type=`+changes[i].type+`&status=`+changes[i].status;
      else
        link = `/employees/tasks?userId=`+changes[i].for+`&projectId=`+changes[i].id_project+`&taskId=`+changes[i].id_task+`&type=`+changes[i].type+`&status=`+changes[i].status;
      element = `<div class="alert alert-dismissible notification-alert `+colorLabel+colorLabelSeen+`" id=change`+changes[i].id+`>
      `+activeLabel+`
        <strong>` + changes[i].user_name + ' ' + changes[i].user_surname + `</strong>
        je `+changes[i].status_label+' '+changes[i].type_label+`  
        <a class="info-link" href="`+link+`">`+changes[i].task_name+`</a>
        na projektu <strong>`+changes[i].project_name+`</strong>`+forLabel+`.
        <div class="card-subtitle-message sender notification-date">`+new Date(changes[i].date).toLocaleString('sl')+`</div>
      </div>`;
      $('#changesBody').prepend(element);
    }
    else if(changes[i].type == 3){
      //subtask
      if(activeUserRole == 'admin' || activeUserRole == 'vodja'.substring(0,5))
        link = `/projects/id?id=`+changes[i].id_project+`&taskId=`+changes[i].id_task+`&subtaskId=`+changes[i].id_subtask+`&type=`+changes[i].type+`&status=`+changes[i].status;
      else
        link = `/employees/tasks?userId=`+changes[i].for+`&projectId=`+changes[i].id_project+`&taskId=`+changes[i].id_task+`&subtaskId=`+changes[i].id_subtask+`&type=`+changes[i].type+`&status=`+changes[i].status;
      element = `<div class="alert alert-dismissible notification-alert `+colorLabel+colorLabelSeen+`" id=change`+changes[i].id+`>
      `+activeLabel+`
        <strong>` + changes[i].user_name + ' ' + changes[i].user_surname + `</strong>
        je `+changes[i].status_label+' '+changes[i].type_label+`  
        <a class="info-link" href="`+link+`">`+changes[i].subtask_name+`</a>
        pri opravilu <strong>`+changes[i].task_name+`</strong> 
        na projektu <strong>`+changes[i].project_name+`</strong>`+forLabel+`.
        <div class="card-subtitle-message sender notification-date">`+new Date(changes[i].date).toLocaleString('sl')+`</div>
      </div>`;
      $('#changesBody').prepend(element);
    }
    else if(changes[i].type == 4){
      //note
      //show notification
      //console.log("show notification; notification and modal ids dont match");
      colorLabel = 'alert-note';
      if(changes[i].id_subtask){
        //subtask note
        if(activeUserRole == 'admin' || activeUserRole == 'vodja'.substring(0,5))
          link = `/projects/id?id=`+changes[i].id_project+`&taskId=`+changes[i].id_task+`&subtaskId=`+changes[i].id_subtask+`&noteId=`+changes[i].id_note+`&type=`+changes[i].type+`&status=`+changes[i].status;
        else
          link = `/employees/tasks?userId=`+changes[i].for+`&projectId=`+changes[i].id_project+`&taskId=`+changes[i].id_task+`&subtaskId=`+changes[i].id_subtask+`&noteId=`+changes[i].id_note+`&type=`+changes[i].type+`&status=`+changes[i].status;
        element = `<div class="alert alert-dismissible notification-alert `+colorLabel+colorLabelSeen+`" id=change`+changes[i].id+`>
        `+activeLabel+`
          <strong>` + changes[i].user_name + ' ' + changes[i].user_surname + `</strong>
          je `+changes[i].status_label+' '+changes[i].type_label+` pri nalogi
          <a class="info-link" href="`+link+`">`+changes[i].subtask_name+`</a>
          opravila <strong>`+changes[i].task_name+`</strong> na projektu <strong>`+changes[i].project_name+`</strong>`+forLabel+`.
          <div class="card-subtitle-message sender notification-date">`+new Date(changes[i].date).toLocaleString('sl')+`</div>
        </div>`;
        $('#changesBody').prepend(element);
      }
      else{
        //task note
        if(activeUserRole == 'admin' || activeUserRole.substring(0,5) == 'vodja')
          link = `/projects/id?id=`+changes[i].id_project+`&taskId=`+changes[i].id_task+`&noteId=`+changes[i].id_note+`&type=`+changes[i].type+`&status=`+changes[i].status;
        else
          link = `/employees/tasks?userId=`+changes[i].for+`&projectId=`+changes[i].id_project+`&taskId=`+changes[i].id_task+`&noteId=`+changes[i].id_note+`&type=`+changes[i].type+`&status=`+changes[i].status;
        element = `<div class="alert alert-dismissible notification-alert `+colorLabel+colorLabelSeen+`" id=change`+changes[i].id+`>
        `+activeLabel+`
          <strong>` + changes[i].user_name + ' ' + changes[i].user_surname + `</strong>
          je `+changes[i].status_label+' '+changes[i].type_label+` pri opravilu
          <a class="info-link" href="`+link+`">`+changes[i].task_name+`</a>
          na projektu <strong>`+changes[i].project_name+`</strong>`+forLabel+`.
          <div class="card-subtitle-message sender notification-date">`+new Date(changes[i].date).toLocaleString('sl')+`</div>
        </div>`;
        $('#changesBody').prepend(element);
      }
    }
    else if(changes[i].type == 5){
      //absence

    }
    else if(changes[i].type == 6){
      //special note
      colorLabel = 'alert-special';
      //admin, leaders or secretaries can make special note so everyone can see it
      //probably will add another case where user can decide who to notify
      element = `<div class="alert alert-dismissible notification-alert `+colorLabel+colorLabelSeen+`" id=change`+changes[i].id+`>
      `+activeLabel+`
        <strong>` + changes[i].user_name + ' ' + changes[i].user_surname + `</strong>
        je `+forLabel+' '+changes[i].status_label+` <strong>pomembno obvestilo</strong> z sporočilom: `+changes[i].note_label+`.
        <div class="card-subtitle-message sender notification-date">`+new Date(changes[i].date).toLocaleString('sl')+`</div>
      </div>`;
      $('#changesBody').prepend(element);
    }
    else if(changes[i].type == 7){
      //file
      colorLabel = 'alert-file';
      if(changes[i].id_task){
        //task file
        link = `/projects/id?id=`+changes[i].id_project+`&taskId=`+changes[i].id_task+`&fileId=`+changes[i].id_file+`&type=`+changes[i].type+`&status=`+changes[i].status;
        element = `<div class="alert alert-dismissible notification-alert `+colorLabel+colorLabelSeen+`" id=change`+changes[i].id+`>
        `+activeLabel+`
        <strong>` + changes[i].user_name + ' ' + changes[i].user_surname + `</strong>
        je `+changes[i].status_label+' '+changes[i].type_label+` pri opravilu
        <a class="info-link" href="`+link+`">`+changes[i].task_name+`</a>
        na projektu <strong>`+changes[i].project_name+`</strong>`+forLabel+`.
        <div class="card-subtitle-message sender notification-date">`+new Date(changes[i].date).toLocaleString('sl')+`</div>
      </div>`;
        $('#changesBody').prepend(element);
      }
      else{
        //project file
        link = `/projects/id?id=`+changes[i].id_project+`&fileId=`+changes[i].id_file+`&type=`+changes[i].type+`&status=`+changes[i].status;
        element = `<div class="alert alert-dismissible notification-alert `+colorLabel+colorLabelSeen+`" id=change`+changes[i].id+`>
        `+activeLabel+`
          <strong>` + changes[i].user_name + ' ' + changes[i].user_surname + `</strong>
          je `+changes[i].status_label+' '+changes[i].type_label+` na projektu
          <a class="info-link" href="`+link+`">`+changes[i].project_name+`</a>
          `+forLabel+`.
          <div class="card-subtitle-message sender notification-date">`+new Date(changes[i].date).toLocaleString('sl')+`</div>
        </div>`;
        $('#changesBody').prepend(element);
      }
    }
    else if(changes[i].type == 8){
      //subscriber

    }
    else if(changes[i].type == 9){
      //servis
      if(activeUserRole == 'admin' || activeUserRole.substring(0,5) == 'vodja')
        link = `/servis/?taskId=`+changes[i].id_task+`&type=`+changes[i].type+`&status=`+changes[i].status;
      else
        link = `/employees/tasks?userId=`+changes[i].for+`&taskId=`+changes[i].id_task+`&type=`+changes[i].type+`&status=`+changes[i].status;
      element = `<div class="alert alert-dismissible `+colorLabel+` notification-alert" id=change`+changes[i].id+`>
        `+activeLabel+`
        <strong>` + changes[i].user_name + ' ' + changes[i].user_surname + `</strong>
        je `+changes[i].status_label+' '+changes[i].type_label+`  
        <a class="info-link" href="`+link+`">`+changes[i].task_name+`</a>
        pri naročniku <strong>`+changes[i].subscriber_label+`</strong>`+forLabel+`.
        <div class="card-subtitle-message sender notification-date">`+new Date(changes[i].date).toLocaleString('sl')+`</div>
      </div>`;
      $('#changesBody').prepend(element);
    }
    else if(changes[i].type == 10){
      //servis subtask
      if(activeUserRole == 'admin' || activeUserRole.substring(0,5) == 'vodja')
        link = `/servis/?taskId=`+changes[i].id_task+`&subtaskId=`+changes[i].id_subtask+`&type=`+changes[i].type+`&status=`+changes[i].status;
      else
        link = `/employees/tasks?userId=`+changes[i].for+`&taskId=`+changes[i].id_task+`&subtaskId=`+changes[i].id_subtask+`&type=`+changes[i].type+`&status=`+changes[i].status;
      element = `<div class="alert alert-dismissible `+colorLabel+` notification-alert" id=change`+changes[i].id+`>
        `+activeLabel+`
        <strong>` + changes[i].user_name + ' ' + changes[i].user_surname + `</strong>
        je `+changes[i].status_label+' '+changes[i].type_label+`  
        <a class="info-link" href="`+link+`">`+changes[i].subtask_name+`</a>
        za servis <strong>`+changes[i].task_name+`</strong> 
        pri naročniku <strong>`+changes[i].subscriber_label+`</strong>`+forLabel+`.
        <div class="card-subtitle-message sender notification-date">`+new Date(changes[i].date).toLocaleString('sl')+`</div>
      </div>`;
      $('#changesBody').prepend(element);
    }
    else if(changes[i].type == 11){
      //servis message
      colorLabel = 'alert-note';
      if(changes[i].id_subtask){
        //subtask note
        if(activeUserRole == 'admin' || activeUserRole.substring(0,5) == 'vodja')
          link = `/servis/?taskId=`+changes[i].id_task+`&subtaskId=`+changes[i].id_subtask+`&noteId=`+changes[i].id_note+`&type=`+changes[i].type+`&status=`+changes[i].status;
        else
          link = `/employees/tasks?userId=`+changes[i].for+`&taskId=`+changes[i].id_task+`&subtaskId=`+changes[i].id_subtask+`&noteId=`+changes[i].id_note+`&type=`+changes[i].type+`&status=`+changes[i].status;
        element = `<div class="alert alert-dismissible `+colorLabel+` notification-alert" id=change`+changes[i].id+`>
          `+activeLabel+`
          <strong>` + changes[i].user_name + ' ' + changes[i].user_surname + `</strong>
          je `+changes[i].status_label+' '+changes[i].type_label+` pri nalogi
          <a class="info-link" href="`+link+`">`+changes[i].subtask_name+`</a>
          servisa <strong>`+changes[i].task_name+`</strong> pri naročniku <strong>`+changes[i].subscriber_label+`</strong>`+forLabel+`.
          <div class="card-subtitle-message sender notification-date">`+new Date(changes[i].date).toLocaleString('sl')+`</div>
        </div>`;
        $('#changesBody').prepend(element);
      }
      else{
        //task note
        if(activeUserRole == 'admin' || activeUserRole.substring(0,5) == 'vodja')
          link = `/servis/?taskId=`+changes[i].id_task+`&noteId=`+changes[i].id_note+`&type=`+changes[i].type+`&status=`+changes[i].status;
        else
          link = `/employees/tasks?userId=`+changes[i].for+`&taskId=`+changes[i].id_task+`&noteId=`+changes[i].id_note+`&type=`+changes[i].type+`&status=`+changes[i].status;
        element = `<div class="alert alert-dismissible `+colorLabel+` notification-alert" id=change`+changes[i].id+`>
          `+activeLabel+`
          <strong>` + changes[i].user_name + ' ' + changes[i].user_surname + `</strong>
          je `+changes[i].status_label+' '+changes[i].type_label+` za servis
          <a class="info-link" href="`+link+`">`+changes[i].task_name+`</a>
          pri naročniku <strong>`+changes[i].subscriber_label+`</strong>`+forLabel+`.
          <div class="card-subtitle-message sender notification-date">`+new Date(changes[i].date).toLocaleString('sl')+`</div>
        </div>`;
        $('#changesBody').prepend(element);
      }
    }
    else if(changes[i].type == 12){
      //servis absence

    }
    else if(changes[i].type == 13){
      //servis file
      colorLabel = 'alert-file';
      link = `/servis/?taskId=`+changes[i].id_task+`&fileId=`+changes[i].id_file+`&type=`+changes[i].type+`&status=`+changes[i].status;
      element = `<div class="alert alert-dismissible `+colorLabel+` notification-alert" id=change`+changes[i].id+`>
        `+activeLabel+`
        <strong>` + changes[i].user_name + ' ' + changes[i].user_surname + `</strong>
        je `+changes[i].status_label+' '+changes[i].type_label+` za servis
        <a class="info-link" href="`+link+`">`+changes[i].task_name+`</a>`+forLabel+`.
        <div class="card-subtitle-message sender notification-date">`+new Date(changes[i].date).toLocaleString('sl')+`</div>
      </div>`;
      $('#changesBody').prepend(element);
    }
    else if(changes[i].type == 14){
      //user

    }
    else if(changes[i].type == 15){
      //system role

    }
    else if(changes[i].type == 16){
      //project role

    }
    else if(changes[i].type == 17){
      //password

    }
    else if(changes[i].type == 18){
      //work order
      if(!changes[i].for)
        activeLabel = '';
      link = `/workorders/?id=`+changes[i].id_work_order+`&type=`+changes[i].type+`&status=`+changes[i].status;
      element = `<div class="alert alert-dismissible notification-alert `+colorLabel+colorLabelSeen+`" id=change`+changes[i].id+`>
      `+activeLabel+`
        <strong>` + changes[i].user_name + ' ' + changes[i].user_surname + `</strong>
        je `+changes[i].status_label+' '+changes[i].type_label+`  
        <a class="info-link" href="`+link+`">`+changes[i].work_order_label+`</a>.
        <div class="card-subtitle-message sender notification-date">`+new Date(changes[i].date).toLocaleString('sl')+`</div>
      </div>`;
      $('#changesBody').prepend(element);
    }
    else if(changes[i].type == 19){
      //report

    }
    else if(changes[i].type == 20){
      //build parts

    }
    else if(changes[i].type == 21){
      //build part faze

    }
    else if(changes[i].type == 22){
      //build part with csv

    }
    else if(changes[i].type == 23){
      //work order file
      if(!changes[i].for)
        activeLabel = '';
      link = `/workorders/?id=`+changes[i].id_work_order+`&type=`+changes[i].type+`&status=`+changes[i].status;
      element = `<div class="alert alert-dismissible notification-alert `+colorLabel+colorLabelSeen+`" id=change`+changes[i].id+`>
      `+activeLabel+`
        <strong>` + changes[i].user_name + ' ' + changes[i].user_surname + `</strong>
        je `+changes[i].status_label+' '+changes[i].type_label+`  
        <a class="info-link" href="`+link+`">`+changes[i].work_order_label+`</a>.
        <div class="card-subtitle-message sender notification-date">`+new Date(changes[i].date).toLocaleString('sl')+`</div>
      </div>`;
      $('#changesBody').prepend(element);
    }
  }
}
function changeChangeActivity(changeId){
  //debugger;
  var tmpChange = ctrlChanges.find(c => c.id == changeId);
  var id = changeId;
  var forUser = tmpChange.for;
  var active = tmpChange.active;
  $.post('/changes/modify', {id,forUser,active}, function(data){
    if(data.success){
      var classes = $('#change'+changeId).attr('class').split(' ')[3];
      if(data.type == 0)
        console.log("Successfully dismissed notification");
      else if(data.type == 1)
        console.log("Successfully modified own notification");
      else if(data.type == 2)
        console.log("Successfully modified change");
      if(tmpChange.active){
        //$('#change'+changeId).find('img').attr('src','/seen.png');
        $('#change'+changeId).find('i').removeClass('fa-eye-slash');
        $('#change'+changeId).find('i').addClass('fa-eye');
        tmpChange.active = false;
        //$('#change'+changeId).removeClass(classes);
        //$('#change'+changeId).addClass(classes.substring(0,classes.length-5));
        $('#change'+changeId).removeClass(classes);
        $('#change'+changeId).addClass(classes+'-dark');
      }
      else{
        //$('#change'+changeId).find('img').attr('src','/unseen1.png');
        $('#change'+changeId).find('i').removeClass('fa-eye');
        $('#change'+changeId).find('i').addClass('fa-eye-slash');
        tmpChange.active = true;
        $('#change'+changeId).removeClass(classes);
        $('#change'+changeId).addClass(classes.substring(0,classes.length-5));
        //$('#change'+changeId).removeClass(classes);
        //$('#change'+changeId).addClass(classes+'-dark');
      }
    }
    else{
      console.log("You are not allowed to modify other changes");
    }
  })
}
function addSpecialNotification(){
  $('#modalAddSpecialNotification').modal('toggle');
}
function changeActivity(){
  $('#optionAll').removeClass('active');
  $('#optionSeen').removeClass('active');
  $('#optionUnseen').removeClass('active');
  setTimeout(()=>{
    getInputChanges();
  },100);
}
function changeReverse(){
  if(reverseChanges)
    reverseChanges = false;
  else
    reverseChanges = true;
  setTimeout(()=>{
    getInputChanges();
  },100);
}
function ctrlStartControl(){
  var start = $('#change_start').val();
  var finish = $('#change_finish').val();
  if(start && finish){
    if(new Date($('#change_start').val()) >= new Date($('#change_finish').val())){
      $('#change_start').val($('#change_finish').val());
    }
  }
}
function ctrlFinishControl(){
  var start = $('#change_start').val();
  var finish = $('#change_finish').val();
  if(start && finish){
    if(new Date($('#change_start').val()) >= new Date($('#change_finish').val())){
      $('#change_finish').val($('#change_start').val());
    }
  }
}
function specialRadioEnable(){
  debugger;
  $('#specialNoteUsers').prop('disabled', false);
}

function specialRadioDisable(){
  debugger;
  $('#specialNoteUsers').prop('disabled', true);
}
var ctrlChanges;
var userId;
var loggedUser;
var allUsers;
var allTypes;
var allStatus;
var activeUserRole;
var reverseChanges = false;
var first = true;
//$('#change_start').on('change', ctrlStartControl);
//$('#change_finish').on('change', ctrlFinishControl);
//$('#radioAllUsers').on('change', specialRadioDisable);
//$('#radioSpecificUsers').on('change', specialRadioEnable);