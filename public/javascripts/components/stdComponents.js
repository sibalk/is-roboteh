$(function(){
  loggedUser = { name: $('#loggedUser').html().split(" (")[0].split(" ")[0], surname: $('#loggedUser').html().split(" (")[0].split(" ")[1], role: $('#loggedUser').html().split(" (")[1].split(")")[0].toLowerCase() };
  if ($('#msg').html() != ''){
    $("#modalMsg").modal();
  }
  //debugger;
  $('#openNewStdCompModal').on('click', function () {
    $('#modalAddStdComp').modal('toggle');
  })
  //get all components and show them in datatable
  $.get("/components/all", function( data ) {
    //console.log(data)
    stdComponents = data.stdComponents;
    //console.log(dataGet[2]);

    if (loggedUser.role == 'admin' || loggedUser.role.substring(0,5) == 'vodja' || loggedUser.role == 'tajnik' || loggedUser.role == 'komercialist' || loggedUser.role == 'komerciala' || loggedUser.role == 'računovodstvo'){
      tableComponents = $('#stdComponentsTable').DataTable( {
        data: stdComponents,
        columns: [
            { title: "stdCompId", data: "id" },
            { title: "Proizvajalec", data: "manufacturer" },
            { title: "Naziv", data: "name" },
            { title: "Tip", data: "type" },
            { title: "Naročniška št.", data: "order_number" },
            { title: "Naša cena", data: "our_price", width: "100px" },
            { title: "List price", data: "list_price", width: "100px" },
            { data: "id", width: "110px",
            render: function ( data, type, row ) {
              // return `<button class="btn btn-danger btn-delete" data-toggle="tooltip" data-original-title="Odstrani">
              //             <i class="fas fa-trash fa-lg"></i>
              //           </button>
              //           <button class="btn btn-purchase btn-files" data-toggle="tooltip" data-original-title="Datoteke">
              //             <i class="fas fa-file fa-lg"></i>
              //           </button>
              //           <button class="btn btn-primary btn-edit" data-toggle="tooltip" data-original-title="Uredi">
              //             <i class="fas fa-pen fa-lg"></i>
              //           </button>`;
              if(loggedUser.role == 'admin' || loggedUser.role.substring(0,5) == 'vodja' || loggedUser.role == 'tajnik' || loggedUser.role == 'komercialist' || loggedUser.role == 'komerciala' || loggedUser.role == 'računovodstvo'){
                // debugger;
                let trashIcon = row.active ? 'fa-trash' : 'fa-trash-restore';
                return `<button class="btn btn-danger btn-delete" data-toggle="tooltip" data-original-title="Odstrani">
                          <i class="fas `+trashIcon+` fa-lg"></i>
                        </button>
                        <button class="btn btn-primary btn-edit" data-toggle="tooltip" data-original-title="Uredi">
                          <i class="fas fa-pen fa-lg"></i>
                        </button>`;
              }
              else return '<div></div>';
            }},
        ],
        "language": {
          "url": "/DataTables/Slovenian.json"
        },
        order: [[0, "asc"]],
        columnDefs: [
          {
            targets: [0],
            visible: false,
            searchable: false
          },
        ],
        "createdRow": function ( row, data, index ) {
          $(row).attr('id', data.id);
          //setTimeout(()=>{$('[data-toggle="tooltip"]').tooltip();},400)
        },
        rowCallback: function(row, data, index){
          $(row).removeClass('table-dark');
          if(!data.active){
            $(row).addClass('table-dark');
            // debugger;
          }
          else{
            // debugger;
          }
          //setTimeout(()=>{$('[data-toggle="tooltip"]').tooltip();},400)
        },
        //responsive: true,
        //info: false,
        //searching: false,
        //scrollY: "500px",
        //paging: false,
      });
    }
    else{
      tableComponents = $('#stdComponentsTable').DataTable( {
        data: stdComponents,
        columns: [
            { title: "stdCompId", data: "id" },
            { title: "Proizvajalec", data: "manufacturer" },
            { title: "Naziv", data: "name" },
            { title: "Tip", data: "type" },
            { title: "Naročniška št.", data: "order_number" },
        ],
        "language": {
          "url": "/DataTables/Slovenian.json"
        },
        order: [[1, "asc"]],
        "lengthMenu": [[10, 20, 25, 50, -1], [10, 20, 25, 50, "All"]],
        columnDefs: [
          {
            targets: [0],
            visible: false,
            searchable: false
          },
        ],
        "createdRow": function ( row, data, index ) {
          $(row).attr('id', data.id);
          //setTimeout(()=>{$('[data-toggle="tooltip"]').tooltip();},400)
        },
        rowCallback: function(row, data, index){
          $(row).removeClass('table-dark');
          if(!data.active){
            $(row).addClass('table-dark');
          }
          //setTimeout(()=>{$('[data-toggle="tooltip"]').tooltip();},400)
        },
        //responsive: true,
        //info: false,
        //searching: false,
        //scrollY: "500px",
        //paging: false,
      });
    }
    //add on event functions on table buttons
    setTimeout(()=>{$('[data-toggle="tooltip"]').tooltip();},400)
    $('#stdComponentsTable tbody').on('click', 'button', function (event) {
      //var data = tableProjects.row( this ).data();
      debugger
      //alert( 'You clicked on '+data.id+'\'s row' );
      //event.target.offsetParent.id
      if ($(event.currentTarget).hasClass('btn-edit'))
        editStdComp(event.currentTarget.offsetParent.parentElement.id);
      else if ($(event.currentTarget).hasClass('btn-delete'))
        openDeleteStdCompModal(event.currentTarget.offsetParent.parentElement.id);
    });
  });

  //add new standard compontent via modal form
  $('#modalAddStdComp').submit(function (e) {
    e.preventDefault();
    $form = $(this);
    debugger
    let manufacturer = newManufacturer.value;
    let name = newName.value;
    let type = newType.value;
    let orderNumber = newOrderNumber.value;
    let ourPrice = newOurPrice.value;
    let listPrice = newListPrice.value;

    let data = {manufacturer, name, type, orderNumber, ourPrice, listPrice};
    $.ajax({
      type: 'POST',
      url: '/components',
      contentType: 'application/json',
      data: JSON.stringify(data), // access in body
    }).done(function (resp) {
      if (resp.success){
        debugger;
        $('#newManufacturer').val('');
        $('#newName').val('');
        $('#newType').val('');
        $('#newOrderNumber').val('');
        $('#newOurPrice').val('');
        $('#newListPrice').val('');

        if (loggedUser.role == 'admin' || loggedUser.role == 'tajnik' || loggedUser.role == 'komercialist' || loggedUser.role == 'komerciala' || loggedUser.role == 'računovodstvo' || loggedUser.role.substring(0,5) == 'vodja'){
          tableComponents.row.add({
            id: resp.stdComp.id,
            manufacturer: resp.stdComp.manufacturer,
            name: resp.stdComp.name,
            type: resp.stdComp.type,
            order_number: resp.stdComp.order_number,
            our_price: resp.stdComp.our_price,
            list_price: resp.stdComp.list_price,
            active: true
          }).draw(false)
        }
        else{
          tableComponents.row.add({
            id: resp.stdComp.id,
            manufacturer: resp.stdComp.manufacturer,
            name: resp.stdComp.name,
            type: resp.stdComp.type,
            order_number: resp.stdComp.order_number,
            active: true
          }).draw(false)
        }
        //stdComponents.push({id: resp.subscriber.id, name: resp.subscriber.name, icon: resp.subscriber.icon, active:true});
        stdComponents.push(resp.stdComp);
        $('#modalAddStdComp').modal('toggle');
        
        //add new change
        addChangeSystem(1,40,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,resp.stdComp.id);
      }
      else{
        $('#modalError').modal('toggle');
      }
    }).fail(function (resp) {
      //console.log('FAIL');
      //debugger;
      $('#modalError').modal('toggle');
    }).always(function (resp) {
      //console.log('ALWAYS');
      //debugger;
    });
  })
  //edit standard compontent via modal form
  $('#modalEditStdComp').submit(function (e) {
    e.preventDefault();
    $form = $(this);
    debugger
    let manufacturer = editManufacturer.value;
    let name = editName.value;
    let type = editType.value;
    let orderNumber = editOrderNumber.value;
    let ourPrice = editOurPrice.value;
    let listPrice = editListPrice.value;
    let stdCompId = savedStdCompId;

    let data = {stdCompId, manufacturer, name, type, orderNumber, ourPrice, listPrice};
    $.ajax({
      type: 'PUT',
      url: '/components',
      contentType: 'application/json',
      data: JSON.stringify(data), // access in body
    }).done(function (resp) {
      if (resp.success){
        debugger;
        $('#editManufacturer').val('');
        $('#editName').val('');
        $('#editType').val('');
        $('#editOrderNumber').val('');
        $('#editOurPrice').val('');
        $('#editListPrice').val('');

        if (loggedUser.role == 'admin' || loggedUser.role == 'tajnik' || loggedUser.role == 'komercialist' || loggedUser.role == 'komerciala' || loggedUser.role == 'računovodstvo' || loggedUser.role.substring(0,5) == 'vodja'){
          var tmp = tableComponents.row('#' + stdCompId).data();
          tmp.manufacturer = manufacturer;
          tmp.name = name;
          tmp.type = type;
          tmp.order_number = orderNumber;
          tmp.our_price = ourPrice;
          tmp.list_price = listPrice;
          tableComponents.row('#' + stdCompId).data(tmp).invalidate().draw(false);
        }
        else{
        }
        //stdComponents.push({id: resp.subscriber.id, name: resp.subscriber.name, icon: resp.subscriber.icon, active:true});
        //stdComponents.push(resp.stdComp);
        $('#modalEditStdComp').modal('toggle');
        //add new change
        addChangeSystem(2,40,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,stdCompId);
      }
      else{
        $('#modalError').modal('toggle');
      }
    }).fail(function (resp) {
      //console.log('FAIL');
      //debugger;
      $('#modalError').modal('toggle');
    }).always(function (resp) {
      //console.log('ALWAYS');
      //debugger;
    });
  })

  //add on event functions
  $('#btnDeleteStdComp').on('click', deleteStdCompConfirm);
  $('#optionAllActivities').on('click', { activity: 0 }, onClickFilterStdCompActivity);
  $('#optionTrueActivities').on('click', { activity: 1 }, onClickFilterStdCompActivity);
  $('#optionFalseActivities').on('click', { activity: 2 }, onClickFilterStdCompActivity);
})
//open edit modal and fill inputs with correct data
function editStdComp(id) {
  debugger;
  let tmpStdComp = stdComponents.find(sc => sc.id == id);
  savedStdCompId = id;

  $('#editManufacturer').val(tmpStdComp.manufacturer);
  $('#editName').val(tmpStdComp.name);
  $('#editType').val(tmpStdComp.type);
  $('#editOrderNumber').val(tmpStdComp.order_number);
  if (loggedUser.role == 'admin' || loggedUser.role == 'tajnik' || loggedUser.role == 'komercialist' || loggedUser.role == 'komerciala' || loggedUser.role == 'računovodstvo' || loggedUser.role.substring(0,5) == 'vodja'){
    $('#editOurPrice').val(tmpStdComp.our_price);
    $('#editListPrice').val(tmpStdComp.list_price);
  }

  $('#modalEditStdComp').modal('toggle');
}
//open delete modal and save std comp id
function openDeleteStdCompModal(id) {
  debugger;
  savedStdCompId = id;
  let tmpStdComp = stdComponents.find(sc => sc.id == savedStdCompId);
  if (tmpStdComp.active == true){
    $('#deleteModalMsg').html('Ste prepričani, da želite odstraniti to standardno komponento?');
    $('#btnDeleteStdComp').html('Odstrani');
  }
  else{
    $('#deleteModalMsg').html('Ste prepričani, da želite ponovno dodati to standardno komponento?');
    $('#btnDeleteStdComp').html('Ponovno dodaj');
  }

  $('#modalDeleteStdComp').modal('toggle');
}
// comfirm deleting standard component
function deleteStdCompConfirm() {
  // $.post('/projects/delete', {projectId}, function(resp){
  //   if(resp.success){
  //     console.log('Successfully deleting project');
  //     $('#modalDeleteProject').modal('toggle');
  //     var temp = tableProjects.row('#'+projectId).data();
  //     temp.active = false;
  //     tableProjects.row('#'+projectId).data(temp).invalidate().draw(false);
  //     //ADDING NEW CHANGE TO DB
  //     addChangeSystem(3,1,projectId);
  //     //addNewProjectChange(1,3,projectId);
  //   }
  //   else{
  //     console.log('Error on deleting project');
  //     //console.log(resp);
  //     $('#modalError').modal('toggle');
  //   }
  // })
  let stdCompId = savedStdCompId;
  let tmpStdComp = stdComponents.find(sc => sc.id == stdCompId);
  let active =  (tmpStdComp.active == true) ? false : true;
  let data = {stdCompId, active};
    $.ajax({
      type: 'DELETE',
      url: '/components',
      contentType: 'application/json',
      data: JSON.stringify(data), // access in body
    }).done(function (resp) {
      if (resp.success){
        debugger;

        if (loggedUser.role == 'admin'){
          //marked new active on complaints subscribers table and update variables //DEPENDS ON WHICH ACTIVITY TABEL IS SHOWN
          if (stdCompActivity == '0'){
            //true and false, all complaints sub are shown, update variables and  class & icon on table
            tmpStdComp.active = active;
            tableComponents.clear();
            tableComponents.rows.add(stdComponents).draw();
          }
          else if (stdCompActivity == '1' || stdCompActivity == '2'){
            //true, only active complaints sub are shown, same as any other role user // same for false, only non active complaints sub are shown
            stdComponents = stdComponents.filter(sc => sc.id != stdCompId);
            tableComponents.clear();
            tableComponents.rows.add(stdComponents).draw();
          }
        }
        else{
          //remove deleted complaint sub from complaints sub table and remove activity from variables
          stdComponents = stdComponents.filter(sc => sc.id != stdCompId);
          tableComponents.clear();
          tableComponents.rows.add(stdComponents).draw();
        }

        //stdComponents.push({id: resp.subscriber.id, name: resp.subscriber.name, icon: resp.subscriber.icon, active:true});
        //stdComponents.push(resp.stdComp);
        $('#modalDeleteStdComp').modal('toggle');
        //add new change
        if(active) addChangeSystem(6,40,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,savedStdCompId);
        else addChangeSystem(3,40,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,savedStdCompId);
      }
      else{
        $('#modalDeleteStdComp').modal('toggle');
        $('#modalError').modal('toggle');
      }
    }).fail(function (resp) {
      //console.log('FAIL');
      //debugger;
      $('#modalDeleteStdComp').modal('toggle');
      $('#modalError').modal('toggle');
    }).always(function (resp) {
      //console.log('ALWAYS');
      //debugger;
    });
}
function onClickFilterStdCompActivity(params) {
  getStdCompWithFilters(params.data.activity);
}
function getStdCompWithFilters(activity){
  stdCompActivity = (activity !== null) ? activity : stdCompActivity;
  let data = {stdCompActivity};
  //let data = {};
  $.ajax({
    type: 'GET',
    url: '/components/all',
    contentType: 'application/x-www-form-urlencoded',
    data: data, // access in body
  }).done(function (resp) {
    if (resp.success){
      stdComponents = resp.stdComponents;
      // redraw std comp table
      // tableComponents.row(row).data(stdComponents).draw();
      tableComponents.clear();
      tableComponents.rows.add(stdComponents).draw();
    }
    else{ $('#modalError').modal('toggle'); }
  }).fail(function (resp) {//console.log('FAIL');//debugger;
    $('#modalError').modal('toggle');
  }).always(function (resp) {//console.log('ALWAYS');//debugger;
  });
}

let loggedUser;
let stdComponents;
let tableComponents;
let savedStdCompId;
let stdCompActivity = 1;