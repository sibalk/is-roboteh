document.addEventListener('DOMContentLoaded', function(event) {
})
if(document.getElementById('messageLabel')){
  let message = document.getElementById('messageLabel').innerText.split('.')[0] + '.';
  //console.log(message);
  if (message.substring(0,27) == 'Preveč neuspešnih poizkusov'){
    console.log(document.getElementById('hours').innerHTML)
    let hours = document.getElementById('hours').innerHTML;
    let minutes = document.getElementById('minutes').innerHTML;
    let seconds = document.getElementById('seconds').innerHTML;
    countDownDate = new Date();
    countDownDate.setSeconds(countDownDate.getSeconds() + parseInt(seconds));
    countDownDate.setMinutes(countDownDate.getMinutes() + parseInt(minutes));
    countDownDate.setHours(countDownDate.getHours() + parseInt(hours));

    var x = setInterval(function() {

      // Get today's date and time
      var now = new Date().getTime();
    
      // Find the distance between now and the count down date
      var distance = countDownDate - now;
    
      // Time calculations for days, hours, minutes and seconds
      //var days = Math.floor(distance / (1000 * 60 * 60 * 24));
      var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
      var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
      var seconds = Math.floor((distance % (1000 * 60)) / 1000);

      let hLabel = '', mLabel = '', sLabel;
      switch (hours) {
        case 1: hLabel = ' uro, '; break;
        case 2: hLabel = ' uri, '; break;
        case 3 || 4: hLabel = ' ure, '; break;
        default: hLabel = ' ur, '; break;
      }
      switch (minutes) {
        case 1: mLabel = ' minuto '; break;
        case 2: mLabel = ' minuti '; break;
        case 3 || 4: mLabel = ' minute '; break;
        default: mLabel = ' minut '; break;
      }
      switch (seconds) {
        case 1: sLabel = ' sekundo.'; break;
        case 2: sLabel = ' sekundi.'; break;
        case 3 || 4: sLabel = ' sekunde.'; break;
        default: sLabel = ' sekund.'; break;
      }
      message = document.getElementById('messageLabel').innerText.split('.')[0] + '.';
      message += ' Ponovno lahko poizkusite čez ' + hours + hLabel + minutes + mLabel + ' in ' + seconds + sLabel;
      document.getElementById('messageLabel').innerText = message;
      //console.log(message);
      // Display the result in the element with id="demo"
      //document.getElementById("demo").innerHTML = days + "d " + hours + "h "
      //+ minutes + "m " + seconds + "s ";

    
      // If the count down is finished, write some text
      if (distance < 0) {
        clearInterval(x);
        document.getElementById('messageAlert').remove();
        //document.getElementById("demo").innerHTML = "EXPIRED";
      }
    }, 1000);
  }
}
//let hours;
//let minutes;
//let seconds;
var countDownDate;