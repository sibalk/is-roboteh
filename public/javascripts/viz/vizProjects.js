$(document).ready(function(){
  // on event call functions
  $('#moveLeftProjectsBtn').on('click', function(){ moveProjects(0.2); });
  $('#homeProjectsBtn').on('click', showCurrentMonthProjects);
  $('#moveRightProjectsBtn').on('click', function(){ moveProjects(-0.2); });
  $('#filtersProjectsBtn').on('click', openFilterProjectsModal);
  $('#dayProjectsBtn').on('click', showDayProjects);
  $('#weekProjectsBtn').on('click', showWeekProjects);
  $('#monthProjectsBtn').on('click', showMonthProjects);
  $('#lowerFontProjectsBtn').on('click', lowerFontProjects);
  $('#higherFontProjectsBtn').on('click', higherFontProjects);
  $('#refreshProjectsBtn').on('click', refreshTimelineProjects);
  $('#btnConfProjectFilter').on('click', filterProjects);
  $('#projectsTabBtn').on('click', makeFirstProjectsVis);
  makeFirstProjectsVis();
})
function makeFirstProjectsVis(){
  if (!$('#projectsVis').html()){
    let data = {};
    $.ajax({
      type: 'GET',
      url: '/viz/allprojects',
      contentType: 'application/x-www-form-urlencoded',
      data: data, // access in body
    }).done(function (resp) {
      //debugger;
      if(resp.success){
        allActiveProjects = resp.data;
        allActiveProjectsData = resp.altData;
        var tmp1 = allActiveProjects.filter(p => p.project_number.substring(0,2) == 'RT' );
        var tmp2 = allActiveProjects.filter(p => p.project_number.substring(0,3) == 'STR' );
        var tmp3 = allActiveProjects.filter(p => p.project_number.substring(0,3) != 'STR' && p.project_number.substring(0,2) != 'RT');
        tmp1.forEach(p => p.number_order = parseInt(p.project_number.substring(2)));
        tmp2.forEach(p => p.number_order = parseInt(p.project_number.substring(3)));
        var sortedTmp1 = tmp1.sort(function(a,b){
          if (isNaN(a.number_order))
            return 1;
          else if (isNaN(b.number_order))
            return -1;
          return b.number_order - a.number_order;
        });
        var sortedTmp2 = tmp2.sort(function(a,b){
          if (isNaN(a.number_order))
            return 1;
          else if (isNaN(b.number_order))
            return -1;
          return b.number_order - a.number_order;
        });
        allRTProjects = sortedTmp1, allSTRProjects = sortedTmp2;
        allActiveProjects = sortedTmp2.concat(sortedTmp1,tmp3);
        //debugger;
        drawProjects();
      }
      else{
        $('#modalError').modal('toggle');  
      }
    }).fail(function (resp) {
      $('#modalError').modal('toggle');
    }).always(function (resp) {
    });
  }
}
function drawProjects(){
  $('#projectsVis').empty();
  var minDate, maxDate;
  var container = document.getElementById("projectsVis");
  let chartHeight = window.innerHeight -350 +'px';
  var options = {
    visibleFrameTemplate: function (item) {
      if (item.visibleFrameTemplate) {
        return item.visibleFrameTemplate;
      }
    },
    orientation: {
      axis: 'both'
    },
    locale: "sl",
    verticalScroll: true,
    maxHeight: chartHeight,
    zoomKey: 'ctrlKey',
    format: {
      minorLabels: {
        millisecond:'SSS',
        second:     's',
        minute:     'HH:mm',
        hour:       'HH:mm',
        weekday:    'ddd D',
        day:        'D [KW]W',
        week:       'W',
        month:      'MMM [KW]W',
        year:       'YYYY'
      },
      majorLabels: {
        millisecond:'HH:mm:ss',
        second:     'D MMMM HH:mm [KW]W',
        minute:     'ddd D MMMM [KW]W',
        hour:       'ddd D MMMM [KW]W',
        weekday:    'MMMM YYYY [KW]W',
        day:        'MMMM YYYY [KW]W',
        week:       'MMMM YYYY [KW]W',
        month:      'YYYY',
        year:       ' '
      }
    }
  };
  //apply filters from modal window for filtering projects and remove projects with no start && finish
  //apply tag filter
  if($('#radioAllTags').prop('checked'))
    allShownProjects = allActiveProjects;
  else if($('#radioRTTags').prop('checked'))
    allShownProjects = allRTProjects;
  else if($('#radioSTRTags').prop('checked'))
    allShownProjects = allSTRProjects;
  //apply completion filter
  if($('#radioAllCompletion').prop('checked'))
    allShownProjects = allShownProjects;
  else if($('#radioFinishedCompletion').prop('checked'))
    allShownProjects = allShownProjects.filter(p => p.completion == 100);
  else if($('#radioUnfinishedCompletion').prop('checked'))
    allShownProjects = allShownProjects.filter(p => p.completion != 100);
  // apply my project filter
  if($('#radioAllProjects').prop('checked'))
    allShownProjects = allShownProjects;
  else if($('#radioMyProjects').prop('checked'))
    allShownProjects = allShownProjects.filter(p => p.my_project);
  else if($('#radioOtherProjects').prop('checked'))
    allShownProjects = allShownProjects.filter(p => p.my_project == null);
  //remove projects with no start && end
  allShownProjects = allShownProjects.filter(p => !(!p.start && !p.end));
  allShownProjectsData = allActiveProjectsData.filter(p => !(!p.start && !p.end));
  //for different y axe and shorter name
  //allShownProjectsData = allShownProjects.filter(p => p);
  //apply style for odd rows //PROBLEM, if style for odd rows wants to be applied then y and x axeses need to be different
  if(allShownProjectsData && allShownProjectsData.length > 0){
    minDate = allShownProjectsData[0].start; 
    maxDate = allShownProjectsData[0].end;
  }
  else{
    minDate = new Date();
    maxDate = new Date();
  }
  allShownProjectsData.forEach(item => {
    //correct time to full day
    if (item.start && resetStartEndTime)
      item.start = new Date(item.start).setHours(0,0,0,0);
    if (item.end && resetStartEndTime)
      item.end = new Date(item.end).setHours(24,0,0,0);
    
    if(!item.start && item.end){
      item.start = item.end;
      item.end = undefined;
    }
    item.visibleFrameTemplate = '<div class=""><div class="progress-vis progress-brez w-' + item.completion + '"></div></div>';
    //FIND MIN START AND MAX END DATES 
    if (new Date(item.start) < new Date(minDate)) minDate = item.start;
    if (new Date(item.end) > new Date(maxDate)) maxDate = item.end;

  })
  allShownProjects.forEach((projects,i)=>{
    projects.content = projects.project_number;
    if(i % 2 == 0){
      //debugger;
      projects.style =  "background: rgba(82, 226, 233, 0.2);";
      allShownProjectsData.push({
        group : projects.id,
        start : moment(minDate).add(-100, 'days').format("YYYY-MM-DD") + ' 00:00',
        end : moment(maxDate).add(100, 'days').format("YYYY-MM-DD") + ' 23:59',
        type : 'background',
        className : 'odd'
      });
    }
  })
  timelineProjects = new vis.Timeline(container, allShownProjectsData, allShownProjects, options);

  var today = new Date();
  setTimeout(()=>{
    setFontSize();
    setTimeout(()=>{
      homeRangeProjects = timelineProjects.getWindow();
      timelineProjects._setScrollTop(0);
    },300);
  },1000)
}
function filterProjects(){
  drawProjects();
  $('#modalFilterProjects').modal('toggle');
}
function openFilterProjectsModal(){
  $('#modalFilterProjects').modal('toggle');
}
function moveProjects(percentage) {
  var range = timelineProjects.getWindow();
  var interval = range.end - range.start;

  timelineProjects.setWindow({
    start: range.start.valueOf() - interval * percentage,
    end: range.end.valueOf() - interval * percentage,
  });
}
function showCurrentMonthProjects(){
  //var today = new Date();
  timelineProjects.setWindow({
    start: homeRangeProjects.start.valueOf(),
    end: homeRangeProjects.end.valueOf(),
  });
}
function refreshTimelineProjects(){
  //$('#projectsVis').empty();
  //makeFirstProjectsVis();
  var minDate, maxDate;
  let data = {};
  $.ajax({
    type: 'GET',
    url: '/viz/allprojects',
    contentType: 'application/x-www-form-urlencoded',
    data: data, // access in body
  }).done(function (resp) {
    //debugger;
    if(resp.success){
      allActiveProjects = resp.data;
      allActiveProjectsData = resp.altData;
      var tmp1 = allActiveProjects.filter(p => p.project_number.substring(0,2) == 'RT' );
      var tmp2 = allActiveProjects.filter(p => p.project_number.substring(0,3) == 'STR' );
      var tmp3 = allActiveProjects.filter(p => p.project_number.substring(0,3) != 'STR' && p.project_number.substring(0,2) != 'RT');
      tmp1.forEach(p => p.number_order = parseInt(p.project_number.substring(2)));
      tmp2.forEach(p => p.number_order = parseInt(p.project_number.substring(3)));
      var sortedTmp1 = tmp1.sort(function(a,b){
        if (isNaN(a.number_order))
          return 1;
        else if (isNaN(b.number_order))
          return -1;
        return b.number_order - a.number_order;
      });
      var sortedTmp2 = tmp2.sort(function(a,b){
        if (isNaN(a.number_order))
          return 1;
        else if (isNaN(b.number_order))
          return -1;
        return b.number_order - a.number_order;
      });
      allRTProjects = sortedTmp1, allSTRProjects = sortedTmp2;
      allActiveProjects = sortedTmp2.concat(sortedTmp1,tmp3);
      //debugger;
      //drawProjects();
      //apply tag filter
      if($('#radioAllTags').prop('checked'))
        allShownProjects = allActiveProjects;
      else if($('#radioRTTags').prop('checked'))
        allShownProjects = allRTProjects;
      else if($('#radioSTRTags').prop('checked'))
        allShownProjects = allSTRProjects;
      //apply completion filter
      if($('#radioAllCompletion').prop('checked'))
        allShownProjects = allShownProjects;
      else if($('#radioFinishedCompletion').prop('checked'))
        allShownProjects = allShownProjects.filter(p => p.completion == 100);
      else if($('#radioUnfinishedCompletion').prop('checked'))
        allShownProjects = allShownProjects.filter(p => p.completion != 100);
      //remove projects with no start && end
      allShownProjects = allShownProjects.filter(p => !(!p.start && !p.end));
      allShownProjectsData = allActiveProjectsData.filter(p => !(!p.start && !p.end));
      //for different y axe and shorter name
      //allShownProjectsData = allShownProjects.filter(p => p);
      //apply style for odd rows //PROBLEM, if style for odd rows wants to be applied then y and x axeses need to be different
      if(allShownProjectsData && allShownProjectsData.length > 0){
        minDate = allShownProjectsData[0].start; 
        maxDate = allShownProjectsData[0].end;
      }
      else{
        minDate = new Date();
        maxDate = new Date();
      }
      allShownProjectsData.forEach(item => {
        //correct time to full day
        if (item.start && resetStartEndTime)
          item.start = new Date(item.start).setHours(0,0,0,0);
        if (item.end && resetStartEndTime)
          item.end = new Date(item.end).setHours(24,0,0,0);

        if(!item.start && item.end){
          item.start = item.end;
          item.end = undefined;
        }
        item.visibleFrameTemplate = '<div class=""><div class="progress-vis progress-brez w-' + item.completion + '""></div></div>';
        //FIND MIN START AND MAX END DATES 
        if (new Date(item.start) < new Date(minDate)) minDate = item.start;
        if (new Date(item.end) > new Date(maxDate)) maxDate = item.end;

      })
      allShownProjects.forEach((projects,i)=>{
        projects.content = projects.project_number;
        if(i % 2 == 0){
          //debugger;
          projects.style =  "background: rgba(82, 226, 233, 0.2);";
          allShownProjectsData.push({
            group : projects.id,
            start : moment(minDate).add(-100, 'days').format("YYYY-MM-DD") + ' 00:00',
            end : moment(maxDate).add(100, 'days').format("YYYY-MM-DD") + ' 23:59',
            type : 'background',
            className : 'odd'
          });
        }
      })
      timelineProjects.setItems(allShownProjectsData);
    }
    else{
      $('#modalError').modal('toggle');  
    }
  }).fail(function (resp) {
    $('#modalError').modal('toggle');
  }).always(function (resp) {
  });
}
function lowerFontProjects(){
  $('.vis-panel').css('font-size', --fontSize);
  if(timelineProjects){
    //moveProjects(0.001);
    timelineProjects.redraw()
  }
  if(timelineProject){
    timelineProject.redraw()
  }
  if(timeline){
    //move(0.001);
    timeline.redraw();
  }
}
function higherFontProjects(){
  $('.vis-panel').css('font-size', ++fontSize);
  if(timelineProjects){
    //moveProjects(0.001);
    timelineProjects.redraw()
  }
  if(timelineProject){
    timelineProject.redraw()
  }
  if(timeline){
    //move(0.001);
    timeline.redraw();
  }
}
function showDayProjects(){
  var today = new Date();

  setTimeout(()=>{
    timelineProjects.setWindow(moment().format("YYYY-MM-DD"), moment().add(1, 'days').format("YYYY-MM-DD"));
  },100);
}
function showWeekProjects(){
  var today = new Date();

  setTimeout(()=>{
    timelineProjects.setWindow(moment().format("YYYY-MM-DD"), moment().add(7, 'days').format("YYYY-MM-DD"));
  },100);
}
function showMonthProjects(){
  var today = new Date();

  setTimeout(()=>{
    timelineProjects.setWindow(moment().format("YYYY-MM-DD"), moment().add(30, 'days').format("YYYY-MM-DD"));
  },100);
}
var allActiveProjects, allActiveProjectsData, allShownProjects, allShownProjectsData;
var allRTProjects, allSTRProjects;
var timelineProjects;
var homeRangeProjects;