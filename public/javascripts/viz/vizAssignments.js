$(document).ready(function(){
  // on event call functions
  $('#moveLeftAssignmentsBtn').on('click', function(){ move(0.2); });
  $('#homeAssignmentsBtn').on('click', showCurrentWeek);
  $('#moveRightAssignmentsBtn').on('click', function(){ move(-0.2); });
  $('#filtersAssignmentsBtn').on('click', openFilterModal);
  $('#dayAssignmentsBtn').on('click', showDay);
  $('#weekAssignmentsBtn').on('click', showWeek);
  $('#monthAssignmentsBtn').on('click', showMonth);
  $('#lowerFontAssignmentsBtn').on('click', lowerFont);
  $('#higherFontAssignmentsBtn').on('click', higherFont);
  $('#refreshAssignmentsBtn').on('click', refreshTimeline);
  $('#assignmentsTabBtn').on('click', makeFirstAssignmentsVis);
})
function getAssignments(){
  $.get( "/viz/assignments", {mechAssemblyRole, cncOperaterRole, electricanRole, mechanicRole, welderRole, serviserRole, plcRole, robotRole, builderRole, leaderRole, workerRole, studentRole, secrataryRole, adminRole}, function( data ) {
    if(data.success){
      allAssignments = data.data;
      //debugger
      //console.log('Successfully getting all services.');
    }
    else{
      console.log('Unsuccessfully getting all assignments.');
      //open error modal
    }
  });
}
function makeFirstAssignmentsVis(){
  if(!$('#employeesVis').html()){
    
    $.get('/viz/users', {mechAssemblyRole, cncOperaterRole, electricanRole, mechanicRole, welderRole, serviserRole, plcRole, robotRole, builderRole, leaderRole, workerRole, studentRole, secrataryRole, adminRole}, function(data) {
      if(data.success){
        //fix user data
        allUsers = data.data;
        //allUsers[0].style = "background: rgba(82, 226, 233, 0.2);"
        /*
        allUsers.forEach(u => u.content = u.text);
        const mapEmployeesToUsers = allUsers.map(item => {
          const container = {};
      
          container.id = item.id;
          container.content = item.name + ' ' + item.surname;
      
          return container;
        })
        allUsers = mapEmployeesToUsers;
        */
        $.get( "/viz/assignments", {mechAssemblyRole, cncOperaterRole, electricanRole, mechanicRole, welderRole, serviserRole, plcRole, robotRole, builderRole, leaderRole, workerRole, studentRole, secrataryRole, adminRole}, function( data ) {
          if(data.success){
            allAssignments = data.data;
            //map function for style and correction
            var minDate, maxDate;
            if(allAssignments && allAssignments.length > 0){
              minDate = allAssignments[0].start; 
              maxDate = allAssignments[0].end;
            }
            else{
              minDate = new Date();
              maxDate = new Date();
            }
            allAssignments.forEach(item => {
              if (item.start && resetStartEndTime)
                item.start = new Date(item.start).setHours(0,0,0,0);
              if (item.end && resetStartEndTime)
                item.end = new Date(item.end).setHours(24,0,0,0);
              //const container = {};
              //container = item;
              if (item.id_reason){//absence
                if (item.id_project){//project absence
                  if (item.reason_name){
                    item.title = item.content + ' (' + item.reason_name + ', ' + item.project_name + ', ' + item.task_name + ')';
                    item.content = item.content + ' - ' + item.reason_name;
                  }
                  else{
                    item.title = item.content + ' (' + item.project_name + ', ' + item.task_name + ')';
                    item.content = item.content;
                  }
                }
                else{//general absence
                  if (item.reason_name){
                    item.title = item.content + ' (' + item.reason_name + ')';
                    item.content = item.content + ' - ' + item.reason_name;
                  }
                  else{
                    item.title = item.content + ' (' + item.project_name + ', ' + item.task_name + ')';
                    item.content = item.content;
                  }
                }
              }
              else if (item.id_project){//project task assignment
                item.title = item.content + ' (' + item.project_label + ', ' + item.subscriber_label + ', ' + item.completion + '%)';
                item.content = item.project_name + ' - ' + item.content;
              }
              else{//service assignment
                item.title = item.content + ' (servis, ' + item.subscriber_label + ')';
              }
              if(item.role == 'programer plc-jev')
                item.className = 'plc';
              else if(item.role == 'programer robotov')
                item.className = 'robot';
              else if(item.role == 'električar')
                item.className = 'electrican';
              else if(item.role == 'mehanik' || item.role == 'cnc' || item.role == 'cnc operater' || item.role == 'strojni monter' || item.role == 'varilec' || item.role == 'strojnik')
                item.className = 'mechanic';
              else if(item.role == 'konstrukter' || item.role == 'konstruktor')
                item.className = 'builder';
              if (item.id_reason)
                item.className = 'absence';
              //FIND MIN START AND MAX END DATES 
              if (new Date(item.start) < new Date(minDate)) minDate = item.start;
              if (new Date(item.end) > new Date(maxDate)) maxDate = item.end;
              //container.id = item.id;
              //container.content = item.text;
            })
            //var today = new Date();
            if(new Date() > new Date(maxDate)) maxDate = new Date();
            //for making odd rows different colors
            allUsers.forEach((users,i)=>{
              if(i % 2 == 0){
                users.style =  "background: rgba(82, 226, 233, 0.2);";
                allAssignments.push({
                  group : users.id,
                  start : moment(minDate).add(-100, 'days').format("YYYY-MM-DD") + ' 00:00',
                  end : moment(maxDate).add(100, 'days').format("YYYY-MM-DD") + ' 23:59',
                  type : 'background',
                  className : 'odd'
                });
              }
            })
            drawUserAssignments();
            //debugger
            //console.log('Successfully getting all services.');
          }
          else{
            console.log('Unsuccessfully getting all assignments.');
            //open error modal
          }
        });
      }
      else{
        console.log('Unsuccessfully getting all users for assignment visualisation');
      }
    })
  }
}
function addDays(date, days) {
  var result = new Date(date);
  result.setDate(result.getDate() + days);
  return result;
}
function move(percentage) {
  var range = timeline.getWindow();
  var interval = range.end - range.start;

  timeline.setWindow({
    start: range.start.valueOf() - interval * percentage,
    end: range.end.valueOf() - interval * percentage,
  });
}
function showCurrentWeek(){
  var today = new Date();
  timeline.setWindow(moment().format("YYYY-MM-DD"), moment().add(7, 'days').format("YYYY-MM-DD"));  
}
function refreshTimeline(){
  $.get( "/viz/assignments", {mechAssemblyRole, cncOperaterRole, electricanRole, mechanicRole, welderRole, serviserRole, plcRole, robotRole, builderRole, leaderRole, workerRole, studentRole, secrataryRole, adminRole}, function( data ) {
    if(data.success){
      allAssignments = data.data;
      //map function for style and correction
      var minDate, maxDate;
      if(allAssignments && allAssignments.length > 0){
        minDate = allAssignments[0].start; 
        maxDate = allAssignments[0].end;
      }
      else{
        minDate = new Date();
        maxDate = new Date();
      }
      allAssignments.forEach(item => {
        if (item.start && resetStartEndTime)
          item.start = new Date(item.start).setHours(0,0,0,0);
        if (item.end && resetStartEndTime)
          item.end = new Date(item.end).setHours(24,0,0,0);
        //const container = {};
        //container = item;
        if (item.id_reason){//absence
          if (item.id_project){//project absence
            if (item.reason_name){
              item.title = item.content + ' (' + item.reason_name + ', ' + item.project_name + ', ' + item.task_name + ')';
              item.content = item.content + ' - ' + item.reason_name;
            }
            else{
              item.title = item.content + ' (' + item.project_name + ', ' + item.task_name + ')';
              item.content = item.content;
            }
          }
          else{//general absence
            if (item.reason_name){
              item.title = item.content + ' (' + item.reason_name + ')';
              item.content = item.content + ' - ' + item.reason_name;
            }
            else{
              item.title = item.content + ' (' + item.project_name + ', ' + item.task_name + ')';
              item.content = item.content;
            }
          }
        }
        else if (item.id_project){//project task assignment
          item.title = item.content + ' (' + item.project_label + ', ' + item.subscriber_label + ', ' + item.completion + '%)';
          item.content = item.project_name + ' - ' + item.content;
        }
        else{//service assignment
          item.title = item.content + ' (servis, ' + item.subscriber_label + ')';
        }
        if(item.role == 'programer plc-jev')
          item.className = 'plc';
        else if(item.role == 'programer robotov')
          item.className = 'robot';
        else if(item.role == 'električar')
          item.className = 'electrican';
        else if(item.role == 'mehanik' || item.role == 'cnc' || item.role == 'cnc operater' || item.role == 'strojni monter' || item.role == 'varilec' || item.role == 'strojnik')
          item.className = 'mechanic';
        else if(item.role == 'konstrukter' || item.role == 'konstruktor')
          item.className = 'builder';
        if (item.id_reason)
          item.className = 'absence';
        //FIND MIN START AND MAX END DATES 
        if (new Date(item.start) < new Date(minDate)) minDate = item.start;
        if (new Date(item.end) > new Date(maxDate)) maxDate = item.end;
        //container.id = item.id;
        //container.content = item.text;
      })
      if(new Date() > new Date(maxDate)) maxDate = new Date();
      allUsers.forEach((users,i)=>{
        if(i % 2 == 0){
          //users.style =  "background: rgba(82, 226, 233, 0.2);";
          allAssignments.push({
            group : users.id,
            start : moment(minDate).add(-100, 'days').format("YYYY-MM-DD") + ' 00:00',
            end : moment(maxDate).add(100, 'days').format("YYYY-MM-DD") + ' 23:59',
            type : 'background',
            className : 'odd'
          });
        }
      })
      timeline.setItems(allAssignments);
      //debugger
      //console.log('Successfully getting all services.');
    }
    else{
      console.log('Unsuccessfully getting all assignments.');
      //open error modal
    }
  });
}
function drawUserAssignments(){
  $('#employeesVis').empty();
  // create a DataSet with items
  /*
  const mapEmployeesToUsers = employees.map(item => {
    const container = {};

    container.id = item.id;
    container.content = item.text;

    return container;
  })
  */
  //allUsers = mapEmployeesToUsers;
  var container = document.getElementById("employeesVis");
  let chartHeight = window.innerHeight -350 +'px';
  var options = {
    orientation: {
      axis: 'both'
    },
    locale: "sl",
    verticalScroll: true,
    maxHeight: chartHeight,
    zoomKey: 'ctrlKey',
    format: {
      minorLabels: {
        millisecond:'SSS',
        second:     's',
        minute:     'HH:mm',
        hour:       'HH:mm',
        weekday:    'ddd D',
        day:        'D [KW]W',
        week:       'W',
        month:      'MMM [KW]W',
        year:       'YYYY'
      },
      majorLabels: {
        millisecond:'HH:mm:ss',
        second:     'D MMMM HH:mm [KW]W',
        minute:     'ddd D MMMM [KW]W',
        hour:       'ddd D MMMM [KW]W',
        weekday:    'MMMM YYYY [KW]W',
        day:        'MMMM YYYY [KW]W',
        week:       'MMMM YYYY [KW]W',
        month:      'YYYY',
        year:       ' '
      }
    }
  };

  timeline = new vis.Timeline(container, allAssignments, allUsers, options);

  var today = new Date();

  setTimeout(()=>{
    setFontSize();
    setTimeout(()=>{
      timeline.setWindow(moment().format("YYYY-MM-DD"), moment().add(7, 'days').format("YYYY-MM-DD"));
    },300);
  },1000)
}
function openFilterModal(){
  $('#modalFilterUsers').modal('toggle');
}
function filterUsersByRole(){
  mechAssemblyRole = $('#checkboxMechAssembly').prop('checked');
  cncOperaterRole = $('#checkboxCNC').prop('checked');
  electricanRole = $('#checkboxElectrican').prop('checked');
  mechanicRole = $('#checkboxMechanic').prop('checked');
  welderRole = $('#checkboxWelder').prop('checked');
  serviserRole = $('#checkboxServiser').prop('checked');
  plcRole = $('#checkboxPlc').prop('checked');
  robotRole = $('#checkboxRobot').prop('checked');
  builderRole = $('#checkboxBuilder').prop('checked');
  leaderRole = $('#checkboxLeader').prop('checked');
  workerRole = $('#checkboxWorker').prop('checked');
  studentRole = $('#checkboxStudent').prop('checked');
  secrataryRole = $('#checkboxSecratary').prop('checked');
  adminRole = $('#checkboxAdmin').prop('checked');
  debugger;
  //redraw timeline with new all users and new all assignments -> empty timeline and call first drawing of timeline function
  $('#employeesVis').empty();
  makeFirstAssignmentsVis();
  $('#modalFilterUsers').modal('toggle');
}
function checkAllRoles(){
  checkAll = false;
  $('#checkboxMechAssembly').prop('checked', true);
  $('#checkboxCNC').prop('checked', true);
  $('#checkboxElectrican').prop('checked', true);
  $('#checkboxMechanic').prop('checked', true);
  $('#checkboxWelder').prop('checked', true);
  $('#checkboxServiser').prop('checked', true);
  $('#checkboxPlc').prop('checked', true);
  $('#checkboxRobot').prop('checked', true);
  $('#checkboxBuilder').prop('checked', true);
  $('#checkboxLeader').prop('checked', true);
  $('#checkboxWorker').prop('checked', true);
  $('#checkboxStudent').prop('checked', true);
  $('#checkboxSecratary').prop('checked', true);
  $('#checkboxAdmin').prop('checked', true);
}
function uncheckAllRoles(){
  checkAll = true;
  $('#checkboxMechAssembly').prop('checked', false);
  $('#checkboxCNC').prop('checked', false);
  $('#checkboxElectrican').prop('checked', false);
  $('#checkboxMechanic').prop('checked', false);
  $('#checkboxWelder').prop('checked', false);
  $('#checkboxServiser').prop('checked', false);
  $('#checkboxPlc').prop('checked', false);
  $('#checkboxRobot').prop('checked', false);
  $('#checkboxBuilder').prop('checked', false);
  $('#checkboxLeader').prop('checked', false);
  $('#checkboxWorker').prop('checked', false);
  $('#checkboxStudent').prop('checked', false);
  $('#checkboxSecratary').prop('checked', false);
  $('#checkboxAdmin').prop('checked', false);
}
function switchAllRoles(){
  checkAll ? checkAllRoles() : uncheckAllRoles();
}
function lowerFont(){
  $('.vis-panel').css('font-size', --fontSize);
  if(timelineProjects){
    //moveProjects(0.001);
    timelineProjects.redraw()
  }
  if(timelineProject){
    timelineProject.redraw()
  }
  if(timeline){
    //move(0.001);
    timeline.redraw();
  }
}
function higherFont(){
  $('.vis-panel').css('font-size', ++fontSize);
  if(timelineProjects){
    //moveProjects(0.001);
    timelineProjects.redraw()
  }
  if(timelineProject){
    timelineProject.redraw()
  }
  if(timeline){
    //move(0.001);
    timeline.redraw();
  }
}
function showDay(){
  var today = new Date();

  setTimeout(()=>{
    timeline.setWindow(moment().format("YYYY-MM-DD"), moment().add(1, 'days').format("YYYY-MM-DD"));
  },100);
}
function showWeek(){
  var today = new Date();

  setTimeout(()=>{
    timeline.setWindow(moment().format("YYYY-MM-DD"), moment().add(7, 'days').format("YYYY-MM-DD"));
  },100);
}
function showMonth(){
  var today = new Date();

  setTimeout(()=>{
    timeline.setWindow(moment().format("YYYY-MM-DD"), moment().add(30, 'days').format("YYYY-MM-DD"));
  },100);
}
var allAssignments;
var allUsers;
var timeline;
var checkAll = true;
var mechAssemblyRole = true, cncOperaterRole = true, electricanRole = true, mechanicRole = true, welderRole = true, serviserRole = true, plcRole = true, robotRole = true, builderRole = true, leaderRole = true, workerRole = true, studentRole = true, secrataryRole = true, adminRole = true;
var resetStartEndTime = true;