This is a commercial Mobiscroll package, licensed to Klemen Sibal <klemen.sibal@roboteh.si> with license key af5e0a00-ec73-4a06-b7f3-409cc5faf6b6.

In case of perpetual licenses please refer to the EULA licensing terms (present in the license folder) or https://mobiscroll.com/EULA for more info.

In case of SaaS licenses please refer to the license agreement you agreed to.