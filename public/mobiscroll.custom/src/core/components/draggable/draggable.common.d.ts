/** @jsxRuntime classic */
/** @jsx createElement */
import { DraggableBase, MbscDraggableOptions } from './draggable';
export declare function template(content: any): any;
export declare class Draggable extends DraggableBase {
    protected _template(s: MbscDraggableOptions): any;
}
