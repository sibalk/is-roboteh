import { CalendarNav, CalendarNext, CalendarPrev, CalendarToday } from '../../../preact/shared/calendar-header';
import { CalendarBase, MbscCalendarOptions } from './calendar';
import './calendar.scss';
export { CalendarNext, CalendarPrev, CalendarToday, CalendarNav };
export declare function template(s: MbscCalendarOptions, inst: CalendarBase): any;
/**
 * The Calendar component.
 *
 * Usage:
 *
 * ```
 * <Calendar />
 * ```
 */
export declare class Calendar extends CalendarBase {
    protected _template(s: MbscCalendarOptions): any;
}
