import { CalendarNav, CalendarNext, CalendarPrev, CalendarToday } from '../../../preact/shared/calendar-header';
import { ICalendarViewHost } from '../../shared/calendar-view/calendar-view.types';
import { InstanceServiceBase } from '../../shared/instance-service';
import { EventcalendarBase } from './eventcalendar';
import { MbscEventcalendarOptions, MbscEventcalendarState } from './eventcalendar.types';
import './eventcalendar.scss';
export { CalendarNext, CalendarPrev, CalendarToday, CalendarNav };
export declare function template(s: MbscEventcalendarOptions, state: MbscEventcalendarState, inst: EventcalendarBase): any;
/**
 * The Eventcalendar component.
 *
 * Usage:
 *
 * ```
 * <Eventcalendar />
 * ```
 */
export declare class Eventcalendar extends EventcalendarBase implements ICalendarViewHost {
    _instanceService: InstanceServiceBase;
    protected _template(s: MbscEventcalendarOptions, state: MbscEventcalendarState): any;
}
