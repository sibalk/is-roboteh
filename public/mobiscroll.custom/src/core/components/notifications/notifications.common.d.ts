import { MbscAlertOptions, MbscConfirmOptions, MbscPromptOptions, MbscSnackbarOptions, MbscToastOptions } from './notifications';
import './notifications.scss';
/**
 * Returns content for alert and confirm.
 * @param options
 */
export declare function getAlertContent(options: MbscAlertOptions | MbscConfirmOptions): any;
/**
 * Returns content for prompt
 * @param options
 */
export declare function getPromptContent(options: MbscPromptOptions, onInputChange: (event: any) => void): any;
/**
 * Returns content for toast.
 * @param options
 */
export declare function getToastContent(options: MbscToastOptions): any;
/**
 * Returns content for snackbar.
 * @param options
 */
export declare function getSnackbarContent(options: MbscSnackbarOptions, onButtonClick: () => void): any;
export declare function toast(options: MbscToastOptions): Promise<undefined>;
export declare function snackbar(options: MbscSnackbarOptions): Promise<undefined>;
export declare function alert(options: MbscAlertOptions): Promise<undefined>;
export declare function confirm(options: MbscConfirmOptions): Promise<boolean>;
export declare function prompt(options: MbscPromptOptions): Promise<string | null>;
