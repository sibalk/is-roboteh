import { DatepickerBase, MbscDatepickerOptions } from './datepicker';
import './datepicker.scss';
export { CalendarNext, CalendarPrev, CalendarToday, CalendarNav } from '../../../preact/shared/calendar-header';
export declare function template(s: MbscDatepickerOptions, inst: DatepickerBase, content: any): any;
export declare class Datepicker extends DatepickerBase {
    protected _template(s: MbscDatepickerOptions): any;
}
