import { IWheelItemProps, WheelItemBase } from './wheel-item';
/** @hidden */
export declare function template(s: IWheelItemProps, inst: WheelItemBase): any;
export declare class WheelItem extends WheelItemBase {
    protected _template(s: IWheelItemProps): any;
}
