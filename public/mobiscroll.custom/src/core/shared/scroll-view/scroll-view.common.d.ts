import { IScrollviewBaseProps, ScrollviewBase } from './scroll-view';
export declare function template(s: IScrollviewBaseProps, inst: ScrollviewBase, content: any): any;
export declare class Scrollview extends ScrollviewBase {
    protected _template(s: IScrollviewBaseProps): any;
}
