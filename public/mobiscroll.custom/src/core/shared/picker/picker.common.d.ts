import { PickerBase } from './picker';
import { IPickerProps, IPickerState } from './picker.types';
export declare function pickerTemplate(inst: PickerBase<IPickerProps, IPickerState>, s: IPickerProps, content: any): any;
