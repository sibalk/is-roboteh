const { Client } = require('pg');

//console.log(process.env.DB_HOST);

const client = new Client({
  user: process.env.DB_USERNAME,
  host: process.env.DB_HOST,
  database: process.env.DB_DATABASE,
  password: process.env.DB_PASSWORD,
  port: process.env.DB_PORT
})
client.connect();

//Create system role
module.exports.addNewRole = function(role){
  return new Promise((resolve,reject)=>{
    let query = `INSERT INTO role(role)
    VALUES ($1)
    RETURNING id`;
    let params = [role];

    client.query(query,params,(err,res)=>{
      if(err) return reject(err);
      return resolve(res.rows[0]);
    })
  })
}
//Get user roles
module.exports.getUserRoles = function(){
  return new Promise((resolve,reject)=>{
    let query = `SELECT id, role as text
    FROM role
    ORDER BY role`;

    client.query(query,(err,res)=>{
      if(err) return reject(err);
      return resolve(res.rows);
    })
  })
}
//Get all roles with supervisors
module.exports.getRolesWithSupervisors = function(loggedUserId){
  return new Promise((resolve,reject)=>{
    let query = `SELECT r.id, r.role, STRING_AGG(u.name || ' ' || u.surname, ', ' ORDER BY u.name, u.surname) AS supervisors, STRING_AGG(CAST(u.id AS TEXT), ', ' ORDER BY u.name, u.surname) AS supervisor_ids, BOOL_OR(CASE WHEN rs.id_user = $1 THEN TRUE ELSE FALSE END) AS is_supervisor
    FROM role r
    LEFT JOIN role_supervision rs ON r.id = rs.id_role
    LEFT JOIN public.user u ON rs.id_user = u.id
    GROUP BY r.id, r.role`;
    params = [loggedUserId];

    client.query(query, params,(err,res)=>{
      if(err) return reject(err);
      return resolve(res.rows);
    })
  })
}