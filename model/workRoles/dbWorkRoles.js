const { Client } = require('pg');

//console.log(process.env.DB_HOST);

const client = new Client({
  user: process.env.DB_USERNAME,
  host: process.env.DB_HOST,
  database: process.env.DB_DATABASE,
  password: process.env.DB_PASSWORD,
  port: process.env.DB_PORT
})
client.connect();

//Create project role
module.exports.addNewProjectRole = function(role){
  return new Promise((resolve,reject)=>{
    let query = `INSERT INTO work_role("workRole")
    VALUES ($1)
    RETURNING id`;
    let params = [role];

    client.query(query,params,(err,res)=>{
      if(err) return reject(err);
      return resolve(res.rows[0]);
    })
  })
}
//Get roles for projects
module.exports.getRoles = function(){
  return new Promise((resolve,reject)=>{
    let query = `SELECT id, "workRole" as text
    FROM work_role
    ORDER BY "workRole"`;

    client.query(query,(err,res)=>{
      if(err) return reject(err);
      return resolve(res.rows);
    })
  })
}