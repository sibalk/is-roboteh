const { Client } = require('pg');

//console.log(process.env.DB_HOST);

const client = new Client({
  user: process.env.DB_USERNAME,
  host: process.env.DB_HOST,
  database: process.env.DB_DATABASE,
  password: process.env.DB_PASSWORD,
  port: process.env.DB_PORT
})
client.connect();

//Get categories
module.exports.getCategories = function(){
  return new Promise((resolve, reject)=>{
    let query = `SELECT id, name, name as text
    FROM category`;

    client.query(query,(err,res)=>{
      if(err) return reject(err);
      return resolve(res.rows);
    })
  })
}

//Get departments
module.exports.getDepartments = function(){
  return new Promise((resolve, reject)=>{
    let query = `SELECT *
    FROM departments
    ORDER BY id`;

    client.query(query,(err,res)=>{
      if(err) return reject(err);
      return resolve(res.rows);
    })
  })
}