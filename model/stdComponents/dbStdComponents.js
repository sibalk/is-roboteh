const { Client } = require('pg');

//console.log(process.env.DB_HOST);

const client = new Client({
  user: process.env.DB_USERNAME,
  host: process.env.DB_HOST,
  database: process.env.DB_DATABASE,
  password: process.env.DB_PASSWORD,
  port: process.env.DB_PORT
})
client.connect();

//get all standard components types
module.exports.getAllStdCompTypes = function(){
  return new Promise((resolve,reject)=>{
    let query = `SELECT *
    FROM standard_components_type
    ORDER BY id`;

    client.query(query,(err,res)=>{
      if(err) return reject(err);
      return resolve(res.rows[0]);
    })
  })
}
//get all standard components
module.exports.getAllStdComponents = function(activity){
  return new Promise((resolve,reject)=>{
    let stdCompActivity;
    switch(activity) {
      case '0': stdCompActivity = 'active'; break;
      case '1': stdCompActivity = 'true'; break;
      case '2': stdCompActivity = 'false'; break;
      default: stdCompActivity = 'true';
    }

    let query = `SELECT *
    FROM standard_components
    WHERE active = ` + stdCompActivity + ``;

    client.query(query,(err,res)=>{
      if(err) return reject(err);
      return resolve(res.rows);
    })
  })
}
//add new standard component (if empty get null)
module.exports.addStdComponent = function(manufacturer, name, type, orderNumber, ourPrice, listPrice){
  return new Promise((resolve, reject)=>{
    let query = `INSERT INTO standard_components(name, order_number, our_price, list_price, type, manufacturer)
    VALUES ($1, $2, $3, $4, $5, $6)
    RETURNING *`;
    let params = [name, orderNumber, ourPrice, listPrice, type, manufacturer]

    client.query(query,params,(err,res)=>{
      if(err) return reject(err);
      return resolve(res.rows[0]);
    })
  })
}
//edit work order
module.exports.editStdComponent = function(stdCompId, manufacturer, name, type, orderNumber, ourPrice, listPrice){
  return new Promise((resolve,reject)=>{
    let params = [stdCompId, name, orderNumber, ourPrice, listPrice, type, manufacturer]
    let query = `UPDATE standard_components
    SET (name, order_number, our_price, list_price, type, manufacturer) = ($2, $3, $4, $5, $6, $7)
    WHERE id = $1`;

    client.query(query,params,(err,res)=>{
      if(err) return reject(err);
      return resolve(res.rows[0]);
    })
  })
}
//delete std component (update active to true/false)
module.exports.deleteStdComponent = function(stdCompId, active){
  return new Promise((resolve,reject)=>{
    let query = `UPDATE standard_components
    SET active = $2
    WHERE id = $1`;
    let params = [stdCompId, active];

    client.query(query,params,(err,res)=>{
      if(err) return reject(err);
      return resolve(res.rows);
    })
  })
}