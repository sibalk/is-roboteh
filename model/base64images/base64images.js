const { Client } = require('pg');

//console.log(process.env.DB_HOST);

const client = new Client({
  user: process.env.DB_USERNAME,
  host: process.env.DB_HOST,
  database: process.env.DB_DATABASE,
  password: process.env.DB_PASSWORD,
  port: process.env.DB_PORT
})
client.connect();

// slika logo Roboteha, ki je uporabljena v vseh pdf dokumentih
module.exports.getRobotehImage = function(){
  return new Promise((resolve,reject)=>{

    let query = `SELECT *
    FROM base64_images
    WHERE name = 'robotehImage'`;

    client.query(query,(err,res)=>{
      if(err) return reject(err);
      return resolve(res.rows[0]);
    })
  })
}
// slika kuka offical partner, ki se uporablja na delovnih nalogih
module.exports.getKukaPartnerImage = function(){
  return new Promise((resolve,reject)=>{

    let query = `SELECT *
    FROM base64_images
    WHERE name = 'kukaPartnerImage'`;

    client.query(query,(err,res)=>{
      if(err) return reject(err);
      return resolve(res.rows[0]);
    })
  })
}
// slika kuka, ki se uporablja na predaji dokumentacije seznama kosovnice, ki gre v izdelavo
module.exports.getKukaImage = function(){
  return new Promise((resolve,reject)=>{

    let query = `SELECT *
    FROM base64_images
    WHERE name = 'kukaImage'`;

    client.query(query,(err,res)=>{
      if(err) return reject(err);
      return resolve(res.rows[0]);
    })
  })
}
// slika ikone vloge vodja
module.exports.getManagerIcon = function(){
  return new Promise((resolve,reject)=>{

    let query = `SELECT *
    FROM base64_images
    WHERE name = 'imgManagerIcon'`;

    client.query(query,(err,res)=>{
      if(err) return reject(err);
      return resolve(res.rows[0]);
    })
  })
}
// slika ikone vloge robot programer
module.exports.getRobotProgrammerIcon = function(){
  return new Promise((resolve,reject)=>{

    let query = `SELECT *
    FROM base64_images
    WHERE name = 'imgRobotIcon'`;

    client.query(query,(err,res)=>{
      if(err) return reject(err);
      return resolve(res.rows[0]);
    })
  })
}
// slika ikone vloge plc programer
module.exports.getPlcProgrammerIcon = function(){
  return new Promise((resolve,reject)=>{

    let query = `SELECT *
    FROM base64_images
    WHERE name = 'imgPlcIcon'`;

    client.query(query,(err,res)=>{
      if(err) return reject(err);
      return resolve(res.rows[0]);
    })
  })
}
// slika ikone vloge mehanik
module.exports.getMechanicIcon = function(){
  return new Promise((resolve,reject)=>{

    let query = `SELECT *
    FROM base64_images
    WHERE name = 'imgMechanicIcon'`;

    client.query(query,(err,res)=>{
      if(err) return reject(err);
      return resolve(res.rows[0]);
    })
  })
}
// slika ikone vloge elektricar
module.exports.getElectricanIcon = function(){
  return new Promise((resolve,reject)=>{

    let query = `SELECT *
    FROM base64_images
    WHERE name = 'imgElectricanIcon'`;

    client.query(query,(err,res)=>{
      if(err) return reject(err);
      return resolve(res.rows[0]);
    })
  })
}
// slika ikone vloge konstrukter
module.exports.getBuilderIcon = function(){
  return new Promise((resolve,reject)=>{

    let query = `SELECT *
    FROM base64_images
    WHERE name = 'imgBuilderIcon'`;

    client.query(query,(err,res)=>{
      if(err) return reject(err);
      return resolve(res.rows[0]);
    })
  })
}