const { Client } = require('pg');

//console.log(process.env.DB_HOST);

const client = new Client({
  user: process.env.DB_USERNAME,
  host: process.env.DB_HOST,
  database: process.env.DB_DATABASE,
  password: process.env.DB_PASSWORD,
  port: process.env.DB_PORT
})
client.connect();

//Add new file for project
module.exports.addFileForProject = function(fileName, pathName, projectId, userId, type){
  return new Promise((resolve,reject)=>{
    let query = `INSERT INTO files(original_name, path_name, id_project, author, type)
    VALUES ($1, $2, $3, $4, $5)
    RETURNING *`;
    let params = [fileName, pathName, projectId, userId, type];
    
    client.query(query,params,(err,res)=>{
      if(err) return reject(err);
      return resolve(res.rows[0]);
    })
  })
}
//Add new file for project modification
module.exports.addFileForProjectMod = function(fileName, pathName, projectId, userId, type, modId){
  return new Promise((resolve,reject)=>{
    let query = `INSERT INTO files(original_name, path_name, id_project, author, type, id_project_mod)
    VALUES ($1, $2, $3, $4, $5, $6)
    RETURNING *`;
    let params = [fileName, pathName, projectId, userId, type, modId];
    
    client.query(query,params,(err,res)=>{
      if(err) return reject(err);
      return resolve(res.rows[0]);
    })
  })
}
//Add new file for task (projectId and taskId)
module.exports.addFileForTask = function(fileName, pathName, projectId, userId, type, taskId){
  return new Promise((resolve,reject)=>{
    let query = `INSERT INTO files(original_name, path_name, id_project, author, type, id_task)
    VALUES ($1, $2, $3, $4, $5, $6)
    RETURNING *`;
    let params = [fileName, pathName, projectId, userId, type, taskId];
    
    client.query(query,params,(err,res)=>{
      if(err) return reject(err);
      return resolve(res.rows[0]);
    })
  })
}
//Add new file for service (taskId with no projectId)
module.exports.addFileForService = function(fileName, pathName, userId, type, taskId){
  return new Promise((resolve,reject)=>{
    let query = `INSERT INTO files(original_name, path_name, author, type, id_task)
    VALUES ($1, $2, $3, $4, $5)
    RETURNING *`;
    let params = [fileName, pathName, userId, type, taskId];

    client.query(query,params,(err,res)=>{
      if(err) return reject(err);
      return resolve(res.rows[0]);
    })
  })
}
//get project files
module.exports.getProjectFiles = function(projectId){
  return new Promise((resolve,reject)=>{
    let query = `SELECT files.id, original_name, path_name, id_project, id_task, id_subtask, author, name, surname, date, type, note, files.active, deleted
    FROM files, "user"
    WHERE id_project = $1 AND
          author = "user".id AND
          deleted = false
    ORDER BY date desc `;
    let params = [projectId];

    client.query(query,params,(err,res)=>{
      if(err) return reject(err);
      return resolve(res.rows);
    })
  })
}
//get task's files
module.exports.getTaskFiles = function(taskId){
  return new Promise((resolve,reject)=>{
    let query = `SELECT files.id, original_name, path_name, id_project, id_task, id_subtask, author, name, surname, date, type, note, files.active, deleted
    FROM files, "user"
    WHERE id_task = $1 AND
          author = "user".id AND
          deleted = false
    ORDER BY date desc`;
    let params = [taskId];

    client.query(query,params,(err,res)=>{
      if(err) return reject(err);
      return resolve(res.rows);
    })
  })
}
//get project modification's files
module.exports.getProjectModificationFiles = function(modId){
  return new Promise((resolve,reject)=>{
    let query = `SELECT files.id, original_name, path_name, id_project, id_task, id_subtask, id_project_mod, author, name, surname, date, type, note, files.active, deleted
    FROM files, "user"
    WHERE id_project_mod = $1 AND
          author = "user".id AND
          deleted = false
    ORDER BY date desc`;
    let params = [modId];

    client.query(query,params,(err,res)=>{
      if(err) return reject(err);
      return resolve(res.rows);
    })
  })
}
//get file info
module.exports.getFileInfo = function(fileName){
  return new Promise((resolve,reject)=>{
    let query = `SELECT files.id, original_name, path_name, id_project, id_task, id_subtask, author, name, surname, date, type, note, files.active, deleted
    FROM files, "user"
    WHERE files.path_name = $1 AND
          author = "user".id
    ORDER BY date desc `;
    let params = [fileName];

    client.query(query,params,(err,res)=>{
      if(err) return reject(err);
      return resolve(res.rows[0]);
    })
  })
}
//get file info
module.exports.getFile = function(fileId){
  return new Promise((resolve,reject)=>{
    let query = `SELECT files.id, original_name, path_name, id_project, id_task, id_subtask, author, name, surname, date, type, note, files.active, deleted
    FROM files, "user"
    WHERE files.id = $1 AND
          author = "user".id
    ORDER BY date desc `;
    let params = [fileId];

    client.query(query,params,(err,res)=>{
      if(err) return reject(err);
      return resolve(res.rows[0]);
    })
  })
}
//update file active to false, file no longer exist
module.exports.updateActiveFile = function(filename){
  return new Promise((resolve,reject)=>{
    let query = `UPDATE files
    SET active = false
    WHERE path_name = $1`;
    let params = [filename];

    client.query(query,params,(err,res)=>{
      if(err) return reject(err);
      return resolve(res.rows);
    })
  })
}
//mark file as deleted after it was successfully deleted
module.exports.deleteFile = function(id){
  return new Promise((resolve,reject)=>{
    let query = `UPDATE files
    SET deleted = true
    WHERE id = $1
    RETURNING *`;
    let params = [id];

    client.query(query,params,(err,res)=>{
      if(err) return reject(err);
      return resolve(res.rows[0]);
    })
  })
}