const { Client } = require('pg');

//console.log(process.env.DB_HOST);

const client = new Client({
  user: process.env.DB_USERNAME,
  host: process.env.DB_HOST,
  database: process.env.DB_DATABASE,
  password: process.env.DB_PASSWORD,
  port: process.env.DB_PORT
})
client.connect();

//Get today's tasks
module.exports.getDailyTasks = function(date, category){
  return new Promise((resolve,reject)=>{
    if(!date)
      date = 'now()';
    let params = [date];
    let categoryQuery = `category`;
    if(category){
      categoryQuery = '$2';
      params.push(category);
    }
    let query = `SELECT project_task.id, id_project, project_name, task_finish <= $1 as expired, task_name, task_duration, task_start, task_finish, "user".name || ' ' || surname as worker, project_task.completion, project_task.active, p2.active as active_project, c.name as category, p.priority, finished, "user".id as worker_id, subtasks, task_note, "user".active as isActive, s.name as subscriber, s2.name as subscriberServis
    FROM  project_task
    LEFT JOIN working_task wt
          INNER JOIN  "user"
          ON "user".id = wt.id_user
    ON project_task.id = wt.id_task
    RIGHT JOIN category c ON project_task.category = c.id
    RIGHT JOIN priority p ON project_task.priority = p.id
    LEFT JOIN project p2 on project_task.id_project = p2.id
    LEFT JOIN subscriber s on p2.subscriber = s.id
    LEFT JOIN subscriber s2 on project_task.id_subscriber = s2.id
    WHERE category = c.id AND
          project_task.active = project_task.active AND
          project_task.completion != '100' AND
          project_task.active = true AND
          ((task_start::date <= $1::date AND
          task_finish::date >= $1::date) OR
           task_finish::date <= $1) AND
          category = `+categoryQuery+`
    ORDER BY s2.id isnull, category!='2', category!='3', category!='4', category!='5', category!='6', category!='7', category!='1', project_task.active desc , task_start`;
    
    client.query(query,params,(err,res)=>{
      if(err) return reject(err);
      return resolve(res.rows);
    })
  })
}
//get weekly tasks
module.exports.getWeeklyTasks = function(date, category){
  return new Promise((resolve,reject)=>{
    if(!date)
      date = 'now()';
    let params = [date];
    let categoryQuery = `category`;
    if(category){
      categoryQuery = '$2';
      params.push(category);
    }
    let query = `SELECT project_task.id, id_project, project_name, task_finish <= $1  as expired, task_name, task_duration, task_start, task_finish, "user".name || ' ' || surname as worker, project_task.completion, project_task.active, p2.active as active_project, c.name as category, p.priority, finished, "user".id as worker_id, subtasks, task_note, "user".active as isActive, s.name as subscriber, s2.name as subscriberServis
    FROM  project_task
    LEFT JOIN working_task wt
          INNER JOIN  "user"
          ON "user".id = wt.id_user
    ON project_task.id = wt.id_task
    RIGHT JOIN category c ON project_task.category = c.id
    RIGHT JOIN priority p ON project_task.priority = p.id
    LEFT JOIN project p2 on project_task.id_project = p2.id
    LEFT JOIN subscriber s on p2.subscriber = s.id
    LEFT JOIN subscriber s2 on project_task.id_subscriber = s2.id
    WHERE category = c.id AND
          project_task.active = project_task.active AND
          project_task.completion != '100' AND
          project_task.active = true AND
          ((task_start::date <= $1::date AND
          task_finish::date >= $1::date+7) OR
          (task_start::date >= $1::date AND
          task_start::date <= $1::date+7) OR
          (task_finish::date >= $1::date AND
          task_finish::date <= $1::date+7) OR
           (task_finish::date <= $1::date)) AND
          category = `+categoryQuery+`
    ORDER BY s2.id isnull, category!='2', category!='3', category!='4', category!='5', category!='6', category!='7', category!='1', project_task.active desc , task_start`;

    client.query(query,params,(err,res)=>{
      if(err) return reject(err);
      return resolve(res.rows);
    })
  })
}