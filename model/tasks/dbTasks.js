const { Client } = require('pg');

//console.log(process.env.DB_HOST);

const client = new Client({
  user: process.env.DB_USERNAME,
  host: process.env.DB_HOST,
  database: process.env.DB_DATABASE,
  password: process.env.DB_PASSWORD,
  port: process.env.DB_PORT
})
client.connect();

//get task data by ID
module.exports.getTaskById = function(id){
  return new Promise((resolve,reject)=>{
    let query = `SELECT pt.id, id_project, task_name, task_duration, task_start, task_finish, pt.completion, category as cat_id, c.name as category, pt.priority as pri_id, p.priority, project_number, project_name, subtasks, task_note, id_subscriber, s.name as subscriber, stc.count as subtask_count, fc.count as files_count, a.count as full_absence_count, aj.count as task_absence_count, workorders, workorders_id, nc.count as notes_count
    FROM project_task as pt
    RIGHT JOIN category c on pt.category = c.id
    RIGHT JOIN priority p on pt.priority = p.id
    LEFT JOIN project pr on pt.id_project = pr.id
    LEFT JOIN subscriber s on pt.id_subscriber = s.id
    LEFT JOIN ( SELECT id_task, count(id_task)
            FROM subtask
            WHERE active = true
            GROUP BY id_task ) AS stc ON pt.id = stc.id_task
    LEFT JOIN ( SELECT id_task, count(id_task)
                FROM files
                WHERE deleted = false
                GROUP BY id_task) AS fc ON pt.id = fc.id_task
    LEFT JOIN ( SELECT working_task.id_task, count(working_task.id_task)
                FROM working_task, absence, project_task
                WHERE absence.id_user = working_task.id_user AND
                      absence.id_task is null and
                      absence.active = true and
                      absence.approved = true and
                      working_task.id_task = project_task.id and
                      ((start::date <= task_finish::date AND start::date >= task_start::date) OR
                      (finish::date <= task_finish::date AND finish::date >= task_start::date))
                GROUP BY working_task.id_task ) AS a ON a.id_task = pt.id
    LEFT JOIN ( SELECT id_task, count(id_task)
                FROM absence as a
                WHERE id_task is not null and
                      approved = true and
                      active = true
                GROUP BY id_task ) AS aj ON aj.id_task = pt.id
    LEFT JOIN ( SELECT task_id, string_agg(concat("left"(name::text,1), "left"(surname::text,1), '-', wo.id, '/', date_part('year', date)), ', ') as workorders, string_agg(wo.id::text, ',') as workorders_id
            FROM work_order as wo, "user" as u
            WHERE task_id is not null AND
                  wo.user_id = u.id AND
                  wo.active = true
            GROUP BY task_id ) AS wo ON pt.id = wo.task_id
    LEFT JOIN ( SELECT id_task, count(id_task)
                FROM notes
                GROUP BY id_task ) AS nc ON pt.id = nc.id_task
    WHERE pt.id = $1`;
    let params = [id];
    
    client.query(query,params,(err,res)=>{
      if(err) return reject(err);
      return resolve(res.rows[0]);
    })
  })
}
//Update task's note
module.exports.updateTaskNote = function(id, note){
  return new Promise((resolve,reject)=>{
    let query = `UPDATE project_task
    SET task_note = $2
    WHERE id = $1`;
    let params = [id, note];

    client.query(query,params,(err,res)=>{
      if(err) return reject(err);
      return resolve(res.rows);
    })
  })
}
//Fix count subtasks of task
module.exports.fixCountOfSubtasks = function(taskId, count){
  return new Promise((resolve,reject)=>{
    let query;
    let params;
    if(count == '+'){
      query = `UPDATE project_task
      SET subtasks = (subtasks + 1)
      WHERE id = $1`;
      params = [taskId];
    }
    else if(count == '-'){
      query = `UPDATE project_task
      SET subtasks = (subtasks -1)
      WHERE id = $1`;
      params = [taskId];
    }
    else{
      query = `UPDATE project_task
      SET subtasks = $2
      WHERE id = $1`;
      params = [taskId, count];
    }

    client.query(query,params,(err,res)=>{
      if(err) return reject(err);
      return resolve(res.rows[0]);
    })
  })
}
//Get project's tasks FIXED
module.exports.getTasksByProjectIdFixed = function(projectId, category, active){
  return new Promise((resolve,reject)=>{
    let params = [projectId];
    let categoryQuery = `pt.category`;
    let activityQuery = `pt.active`;
    let i = 2;
    if(category){
      categoryQuery = '$'+i;
      i++;
      params.push(category);
    }
    if(active){
      activityQuery = '$'+i;
      i++;
      if(active == 1)
        active = true;
      else
        active = false;
      params.push(active);
    }
    let query = `SELECT pt.id, task_name, task_duration, task_start, task_finish, completion, pt.active, c.id as category_id, c.name as category, p.id as priority_id, p.priority, finished, task_note, weekend, w.workers, w.workers_id, stc.count as subtask_count, fc.count as files_count, a.count as full_absence_count, aj.count as task_absence_count, bpc.count as build_parts_count, cs."from" as author_id, concat(cs.name,' ', cs.surname) as author, nc.count as notes_count, pt.id_project as project_id
    FROM project_task as pt
    RIGHT JOIN category c ON pt.category = c.id
    RIGHT JOIN priority p ON pt.priority = p.id
    LEFT JOIN ( SELECT *
                FROM changes_system
                LEFT JOIN "user" u2 on changes_system."from" = u2.id
                WHERE status = 1 AND
                      type = 2 AND
                      id_task IS NOT NULL) AS cs ON cs.id_task = pt.id
    LEFT JOIN ( SELECT id_task, count(id_task)
                FROM subtask
                WHERE active = true
                GROUP BY id_task ) AS stc ON pt.id = stc.id_task
    LEFT JOIN ( SELECT id_task, count(id_task)
                FROM files
                WHERE deleted = false
                GROUP BY id_task) AS fc ON pt.id = fc.id_task
    LEFT JOIN ( SELECT id_task, string_agg(id_user::text, ',') as workers_id, string_agg(concat(name::text, concat(' ', surname::text)), ', ') as workers
                FROM working_task, "user"
                WHERE id_user = "user".id
                GROUP BY id_task ) AS w ON pt.id = w.id_task
    LEFT JOIN ( SELECT working_task.id_task, count(working_task.id_task)
                FROM working_task, absence, project_task
                WHERE absence.id_user = working_task.id_user AND
                      absence.id_task is null and
                      absence.active = true and
                      absence.approved = true and
                      working_task.id_task = project_task.id and
                      ((start::date <= task_finish::date AND start::date >= task_start::date) OR
                      (finish::date <= task_finish::date AND finish::date >= task_start::date))
                GROUP BY working_task.id_task ) AS a ON a.id_task = pt.id
    LEFT JOIN ( SELECT id_task, count(id_task)
                FROM absence as a
                WHERE id_task is not null and
                      approved = true and
                      active = true
                GROUP BY id_task ) AS aj ON aj.id_task = pt.id
    LEFT JOIN ( SELECT task_id, count(task_id)
                FROM build_parts
                WHERE active = true
                GROUP BY task_id ) AS bpc ON pt.id = bpc.task_id
    LEFT JOIN ( SELECT id_task, count(id_task)
                FROM notes
                GROUP BY id_task ) AS nc ON pt.id = nc.id_task
    WHERE pt.id_project = $1 AND
          pt.active = `+activityQuery+` AND
          pt.category = `+categoryQuery+`
    ORDER BY category!='2', category!='3', category!='4', category!='5', category!='6', category!='7', category!='1', active desc, task_start`;
    //for last note id
    // LEFT JOIN ( SELECT id_task, max(id)
    //             FROM notes n
    //             WHERE id_task IS NOT NULL
    //             GROUP BY id_task) AS nli ON pt.id = nli.id_task
    
    client.query(query,params,(err,res)=>{
      if(err) return reject(err);
      return resolve(res.rows);
    })
  })
}
//Get all tasks inside selected time frame (to view all tasks inside calendar view)
module.exports.getAllTasksInsideTimeFrame = function(start, finish, category, active){
  return new Promise((resolve,reject)=>{
    let params = [start, finish];
    let categoryQuery = `pt.category`;
    let activityQuery = `pt.active`;
    let i = 3;
    if(category){
      categoryQuery = '$'+i;
      i++;
      params.push(category);
    }
    if(active){
      activityQuery = '$'+i;
      i++;
      if(active == 1)
        active = true;
      else
        active = false;
      params.push(active);
    }
    let query = `SELECT pt.id, task_name, task_duration, task_start, task_finish, pt.completion, pt.active, c.id as category_id, c.name as category, p.id as priority_id, p.priority, finished, task_note, weekend, w.workers, w.workers_id, cs."from" as author_id, concat(cs.name,' ', cs.surname) as author, cs.date as created, pro.id as project_id, project_number, project_name, s.id as subscriber_id, s.name as subscriber, pt.id_subscriber as sub_id, sub.name as sub_name, bpc.count as build_parts_count, stc.count as subtask_count
    FROM project_task as pt
    RIGHT JOIN category c ON pt.category = c.id
    RIGHT JOIN priority p ON pt.priority = p.id
    LEFT JOIN project pro ON pt.id_project = pro.id
    LEFT JOIN subscriber s ON pro.subscriber = s.id
    LEFT JOIN subscriber sub ON pt.id_subscriber = sub.id
    LEFT JOIN ( SELECT *
                FROM changes_system
                LEFT JOIN "user" u2 on changes_system."from" = u2.id
                WHERE status = 1 AND
                      type = 2 AND
                      id_task IS NOT NULL) AS cs ON cs.id_task = pt.id
    LEFT JOIN ( SELECT id_task, string_agg(id_user::text, ',') as workers_id, string_agg(concat(name::text, concat(' ', surname::text)), ', ') as workers
                FROM working_task, "user"
                WHERE id_user = "user".id
                GROUP BY id_task ) AS w ON pt.id = w.id_task
    LEFT JOIN ( SELECT task_id, count(task_id)
                FROM build_parts
                WHERE active = true
                GROUP BY task_id ) AS bpc ON pt.id = bpc.task_id
    LEFT JOIN ( SELECT id_task, count(id_task)
                FROM subtask
                WHERE active = true
                GROUP BY id_task ) AS stc ON pt.id = stc.id_task
    WHERE ((task_start <= $1 AND task_finish >= $2) OR (task_finish >= $1 AND task_finish <= $2) OR (task_start >= $1 AND task_start <= $2)) AND
          pt.active = `+activityQuery+` AND
          pro.active = `+activityQuery+` AND
          pt.category = `+categoryQuery+`
    ORDER BY category!='2', category!='3', category!='4', category!='5', category!='6', category!='7', category!='1', active desc, task_start`;
    
    client.query(query,params,(err,res)=>{
      if(err) return reject(err);
      return resolve(res.rows);
    })
  })
}
//Get specific task for mobiscroll calendar (to get same data as getAllTasksInsideTimeFrame)
module.exports.getTaskData = function(id){
  return new Promise((resolve,reject)=>{
    let params = [id];
    let query = `SELECT pt.id, task_name, task_duration, task_start, task_finish, pt.completion, pt.active, c.id as category_id, c.name as category, p.id as priority_id, p.priority, finished, task_note, weekend, w.workers, w.workers_id, cs."from" as author_id, concat(cs.name,' ', cs.surname) as author, cs.date as created, pro.id as project_id, project_number, project_name, s.id as subscriber_id, s.name as subscriber, pt.id_subscriber as sub_id, sub.name as sub_name
    FROM project_task as pt
    RIGHT JOIN category c ON pt.category = c.id
    RIGHT JOIN priority p ON pt.priority = p.id
    LEFT JOIN project pro ON pt.id_project = pro.id
    LEFT JOIN subscriber s ON pro.subscriber = s.id
    LEFT JOIN subscriber sub ON pt.id_subscriber = sub.id
    LEFT JOIN ( SELECT *
                FROM changes_system
                LEFT JOIN "user" u2 on changes_system."from" = u2.id
                WHERE status = 1 AND
                      type = 2 AND
                      id_task IS NOT NULL) AS cs ON cs.id_task = pt.id
    LEFT JOIN ( SELECT id_task, string_agg(id_user::text, ',') as workers_id, string_agg(concat(name::text, concat(' ', surname::text)), ', ') as workers
                FROM working_task, "user"
                WHERE id_user = "user".id
                GROUP BY id_task ) AS w ON pt.id = w.id_task
    WHERE pt.id = $1`;
    
    client.query(query,params,(err,res)=>{
      if(err) return reject(err);
      return resolve(res.rows[0]);
    })
  })
}
//Get user conflicts with other project's tasks
module.exports.getWorkerConflicts= function(userId, projectId, start, finish){
  return new Promise((resolve,reject)=>{
    let query = `SELECT wt.id, id_user, u.name, surname, task_name, task_start, task_finish, category, priority, p.project_name, id_subscriber, s.name as subscriber, pt.id_project, pt.id as id_task
    FROM working_task AS wt
    LEFT JOIN "user" u on wt.id_user = u.id
    LEFT JOIN project_task pt on wt.id_task = pt.id
    LEFT JOIN project p on pt.id_project = p.id
    LEFT JOIN subscriber s on pt.id_subscriber = s.id
    WHERE u.active = true AND
          pt.active = true AND
          pt.completion != 100 AND
          id_user = $1 AND
          (id_project != $2 OR id_project is null) AND
          ((task_start::date >= $3::date AND task_start::date <= $4::date) OR
          (task_finish::date >= $3::date AND task_finish::date <= $4::date) OR
          (task_start::date <= $3::date AND task_finish::date >= $4::date))
    UNION
    SELECT a.id as id, id_user, u.name, u.surname, a.name, start, finish, r.id, role, r.name, id_project, null::varchar, id_project, id_project
    FROM absence as a
    LEFT JOIN "user" u on a.id_user = u.id
    LEFT JOIN reason r on a.id_reason = r.id
    WHERE a.active = true AND
          a.approved = true AND
          a.id_user = $1 AND
          a.id_project is null AND
          ((start::date >= $3::date AND start::date <= $4::date) OR
          (finish::date >= $3::date AND finish::date <= $4::date) OR
          (start::date <= $3::date AND finish::date >= $4::date))`;
    let params = [userId, projectId, start, finish];
    
    client.query(query,params,(err,res)=>{
      if(err) return reject(err);
      return resolve(res.rows);
    })
  })
}
//get worker's tasks
module.exports.getWorkerTasks = function(userId){
  return new Promise((resolve,reject)=>{
    let query = `SELECT pt.id as id, pt.id_project as project_id, project_number, task_finish <= now() as expired ,p.project_name, s.id as subscriber_id, s.name as subscriber, pt.task_name, pt.task_duration, pt.task_start, pt.task_finish, pt.completion, pt.priority, id_user, pt.active, c.name as category
    FROM working_task
    RIGHT JOIN "user" u on working_task.id_user = u.id
    RIGHT JOIN project_task pt on working_task.id_task = pt.id
    RIGHT JOIN category c on pt.category = c.id
    LEFT JOIN project p on pt.id_project = p.id
    LEFT JOIN subscriber s on pt.id_subscriber = s.id
    WHERE id_user = $1 AND
          ((id_project ISNULL AND p.active ISNULL) OR (id_project NOTNULL AND p.active = true)) AND
          pt.completion != 100 AND
          pt.active = true
    ORDER BY s.id isnull, expired desc , priority!=4, priority!=3, priority!=2, priority!=1`;
    let params = [userId];
    
    client.query(query,params,(err,res)=>{
      if(err) return reject(err);
      return resolve(res.rows);
    })
  })
}
//Add task to the project
module.exports.addTask = function(projectId, taskName, taskDuration, start, finish, category, priority, taskCompletion, taskWeekend, taskNote){
  return new Promise((resolve,reject)=>{
    let completion = 0;
    if(taskCompletion)
      completion = taskCompletion
    let params = [projectId, taskName, category, priority, completion, taskWeekend];
    let i = 7;
    let durationQuery = ``;
    let durationSet = ``;
    if(taskDuration){
      durationQuery = `, task_duration`;
      durationSet = `, $` + i;
      i++;
      params.push(taskDuration);
    }
    let startQuery = ``;
    let startSet = ``;
    if(start){
      startQuery = `, task_start`;
      startSet = `, $` + i;
      i++;
      params.push(start);
    }
    let finishQuery = ``;
    let finishSet = ``;
    if(finish){
      finishQuery = `, task_finish`;
      finishSet = `, $` + i;
      i++;
      params.push(finish);
    }
    let noteQuery = ``;
    let noteSet = ``;
    if(taskNote){
      noteQuery = `, task_note`;
      noteSet = `, $` + i;
      i++;
      params.push(taskNote);
    }
    let query = `INSERT INTO project_task(id_project, task_name, category, priority, completion, weekend`+durationQuery+startQuery+finishQuery+noteQuery+`)
    VALUES ($1, $2, $3, $4, $5, $6`+durationSet+startSet+finishSet+noteSet+`)
    RETURNING *`;

    client.query(query,params,(err,res)=>{
      if(err) return reject(err);
      return resolve(res.rows[0]);
    })
  })
}
//Update completion of task
module.exports.updateTaskCompletion = function(taskId, completion){
  return new Promise((resolve,reject)=>{
    let query = `UPDATE project_task
    SET completion = $2
    WHERE id = $1
    RETURNING *`;
    let params = [taskId, completion];

    client.query(query,params,(err,res)=>{
      if(err) return reject(err);
      return resolve(res.rows[0]);
    })
  })
}
//Add finished date of task
module.exports.finishTask = function(taskId){
  return new Promise((resolve,reject)=>{
    let query = `UPDATE project_task
    SET finished = now()
    WHERE id = $1`;
    let params = [taskId];

    client.query(query,params,(err,res)=>{
      if(err) return reject(err);
      return resolve(res.rows);
    })
  })
}
//Assign task to worker
module.exports.assignTask = function(task, workerId){
  return new Promise((resolve,reject)=>{
    let query = `INSERT INTO working_task(id_task, id_user)
    VALUES ($1, $2)`;
    let params = [task, workerId];

    client.query(query,params,(err,res)=>{
      if(err) return reject(err);
      return resolve(res.rows);
    })
  })
}
//remove assignment of the task / task is now without worker
module.exports.removeAssignTask = function(taskId){
  return new Promise((resolve,reject)=>{
    let query = `DELETE FROM working_task
    WHERE id_task = $1`;
    let params = [taskId];

    client.query(query,params,(err,res)=>{
      if(err) return reject(err);
      return resolve(res.rows);
    })
  })
}
//remove task / update task to not active
module.exports.removeTask = function(taskId, active){
  return new Promise((resolve,reject)=>{
    let activity = active ? active : false;
    let params = [taskId, activity];
    let query = `UPDATE project_task
    SET active = $2
    WHERE id = $1
    RETURNING *`;

    client.query(query,params,(err,res)=>{
      if(err) return reject(err);
      return resolve(res.rows[0]);
    })
  })
}
//update task
module.exports.updateTask = function(taskId, taskName, taskDuration, taskStart, taskFinish, taskCompletion, taskActive, category, priority, taskWeekend, taskNote){
  return new Promise((resolve,reject)=>{
    let params = [taskId, taskName, taskActive, category, priority, taskWeekend];
    let i = 7;
    let durationQuery = ``;
    let durationSet = ``;
    if(taskDuration){
      durationQuery = `, task_duration`;
      durationSet = `, $` + i;
      i++;
      params.push(taskDuration);
    }
    let startQuery = ``;
    let startSet = ``;
    if(taskStart){
      startQuery = `, task_start`;
      startSet = `, $` + i;
      i++;
      params.push(taskStart);
    }
    else{
      startQuery = `, task_start`;
      startSet = `, $` + i;
      i++;
      taskStart = null;
      params.push(taskStart);
    }
    let finishQuery = ``;
    let finishSet = ``;
    if(taskFinish){
      finishQuery = `, task_finish`;
      finishSet = `, $` + i;
      i++;
      params.push(taskFinish);
    }
    else{
      finishQuery = `, task_finish`;
      finishSet = `, $` + i;
      i++;
      taskFinish = null;
      params.push(taskFinish);
    }
    let completionQuery = ``;
    let completionSet = ``;
    if(taskCompletion){
      completionQuery = `, completion`;
      completionSet = `, $` + i;
      i++;
      params.push(taskCompletion);
    }
    let noteQuery = ``;
    let noteSet = ``;
    if(taskNote){
      noteQuery = `, task_note`;
      noteSet = `, $` + i;
      i++;
      params.push(taskNote);
    }
    else{
      noteQuery = `, task_note`;
      noteSet = `, $` + i;
      i++;
      taskNote = null;
      params.push(taskNote);
    }
    let query = `UPDATE project_task
    SET (task_name, active, category, priority, weekend`+durationQuery+startQuery+finishQuery+completionQuery+noteQuery+`) = ($2, $3, $4, $5, $6`+durationSet+startSet+finishSet+completionSet+noteSet+`)
    WHERE id = $1
    RETURNING *`;

    client.query(query,params,(err,res)=>{
      if(err) return reject(err);
      return resolve(res.rows[0]);
    })
  })
}
//get users who are working on specific task
module.exports.getTaskWorkers = function(id){
  return new Promise((resolve,reject)=>{
    let query = `SELECT id_user, name, surname
    FROM working_task, "user"
    WHERE id_user = "user".id AND
          active = true AND
          id_task = $1`;
    let params = [id];

    client.query(query,params,(err,res)=>{
      if(err) return reject(err);
      return resolve(res.rows);
    })
  })
}