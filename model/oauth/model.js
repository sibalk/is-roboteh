
/**
 * Module dependencies.
 */

var pg = require('pg-promise')(process.env.DATABASE_URL);
const { Client } = require('pg');

//console.log(process.env.DB_HOST);

const client = new Client({
  user: process.env.DB_USERNAME,
  host: process.env.DB_HOST,
  database: process.env.DB_DATABASE,
  password: process.env.DB_PASSWORD,
  port: process.env.DB_PORT
})
client.connect();



////////////////////////AUTHORIZATION CODES//////////////////
module.exports.saveAuthorizationCode = (code, klient, user) => {
  let query = `INSERT INTO oauth_auth_codes (user_id, client_id, authorization_code, scope, expires_at)
  VALUES ($1, $2, $3, $4, $5)
  RETURNING id, user_id, client_id, authorization_code, expires_at, created_at, scope`;
  let params = [user.id,klient.id,code.authorizationCode,[code.scope],code.expiresAt];
  return client.query(query,params)
    .then(res => {
      return {
        authorizationCode: res.rows[0].authorization_code
      };
    })
    .catch(e => {
      return e;
    })
};
module.exports.getAuthorizationCode = (code) => {
  let query = `SELECT oac.id, user_id, oac.client_id, authorization_code, expires_at, oac.created_at, scope, redirect_uris
  FROM oauth_auth_codes as oac
  LEFT JOIN oauth_clients oc on oac.client_id = oc.id
  WHERE authorization_code = $1`;
  let params = [code];
  return client.query(query,params)
    .then(res => {
      return {
        id: res.rows[0].id,
        userId: res.rows[0].user_id,
        client:{
          id: res.rows[0].client_id,
        },
        authorizationCode: res.rows[0].authorization_code,
        expiresAt: res.rows[0].expires_at,
        createdAt: res.rows[0].created_at,
        scope: res.rows[0].scope,
        redirectUri: res.rows[0].redirect_uris[0],
        user: {
          id: res.rows[0].user_id
        }
      };
    })
    .catch(e => {
      return e;
    })
};
module.exports.revokeAuthorizationCode = (code) => {
  let query = `DELETE FROM oauth_auth_codes
  WHERE authorization_code = $1`;
  let params = [code.authorizationCode];
  return client.query(query,params)
    .then(res => {
      if(res.rowCount > 0) return true;
      else return false;
    })
    .catch(e => {
      return e;
    })
};
/*
 * Get access token.
 */

module.exports.getAccessToken = function(bearerToken) {
  return client.query('SELECT access_token, access_token_expires_on, client_id, refresh_token, refresh_token_expires_on, user_id FROM oauth_tokens WHERE access_token = $1', [bearerToken])
    .then(function(result) {
      var token = result.rows[0];

      return {
        accessToken: token.access_token,
        client: {id: token.client_id},
        accessTokenExpiresAt: token.access_token_expires_on,
        user: {id: token.user_id}, // could be any object
      };
    });
};

/**
 * Get client.
 */

module.exports.getClient = function *(clientId, clientSecret) {
  return client.query('SELECT id, client_id, client_secret, redirect_uris, grants FROM oauth_clients WHERE client_id = $1 AND client_secret = $2', [clientId, clientSecret])
    .then(function(result) {
      var oAuthClient = result.rows[0];

      if (oAuthClient) {
        return {
          id: oAuthClient.id,
          clientId: oAuthClient.client_id,
          clientSecret: oAuthClient.client_secret,
          grants: oAuthClient.grants,
          redirectUris: oAuthClient.redirect_uris
           // the list of OAuth2 grant types that should be allowed
        };
      }
      else
        return;
    });
};
/*
module.exports.getClient = function *(clientId, clientSecret) {
  return pg.query('SELECT client_id, client_secret, redirect_uri FROM oauth_clients WHERE client_id = $1 AND client_secret = $2', [clientId, clientSecret])
    .then(function(result) {
      var oAuthClient = result.rows[0];

      if (!oAuthClient) {
        return;
      }

      return {
        clientId: oAuthClient.client_id,
        clientSecret: oAuthClient.client_secret,
        grants: ['password'], // the list of OAuth2 grant types that should be allowed
      };
    });
};
*/
/**
 * Get refresh token.
 */

module.exports.getRefreshToken = function *(token) {
  return client.query('SELECT access_token, access_token_expires_on, client_id, refresh_token, refresh_token_expires_on, user_id, scope, created_at FROM oauth_tokens WHERE refresh_token = $1', [token])
    .then(function(res) {
      if(res.rowCount)
        return {
        id: res.rows[0].id,
        userId: res.rows[0].user_id,
        client:{
          id: res.rows[0].client_id,
        },
        accessToken: res.rows[0].access_token,
        refreshTokenExpiresAt: res.rows[0].refresh_token_expires_on,
        createdAt: res.rows[0].created_at,
        scope: res.rows[0].scope,
        user: {
          id: res.rows[0].user_id
        }
      };
      else return false;
    });
};

/*
 * Get user.
 */

module.exports.getUser = function (username) {
  return client.query('SELECT id, username, name, password, role, surname, active FROM "user" WHERE active = true AND username = $1', [username])
  .then(res => {return res.rows[0];})
  .catch(e => {return e;})
};

/**
 * Save token.
 */

module.exports.saveToken = function *(token, klient, user) {
  return client.query('INSERT INTO oauth_tokens(access_token, access_token_expires_on, client_id, refresh_token, refresh_token_expires_on, user_id, scope) VALUES ($1, $2, $3, $4, $5, $6, $7)', [
    token.accessToken,
    token.accessTokenExpiresAt,
    klient.id,
    token.refreshToken,
    token.refreshTokenExpiresAt,
    user.id,
    token.scope
  ]).then(function(result) {
    return {
      accessToken: token.accessToken,
      client: klient.id,
      refreshToken: token.refreshToken,
      user: user.id,
      accessTokenExpiresAt: token.accessTokenExpiresAt,
      refreshTokenExpiresAt: token.refreshTokenExpiresAt
    }; // TODO return object with client: {id: clientId} and user: {id: userId} defined
  });
};
module.exports.revokeToken = function *(token) {
  let query = `DELETE FROM oauth_tokens
  WHERE access_token = $1`;
  let params = [token.accessToken];
  return client.query(query,params)
    .then(res => {
      if(res.rowCount > 0) return true;
      else return false;
    })
    .catch(e => {
      return e;
    })
};