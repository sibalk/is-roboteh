const { Client } = require('pg');

//console.log(process.env.DB_HOST);

const client = new Client({
  user: process.env.DB_USERNAME,
  host: process.env.DB_HOST,
  database: process.env.DB_DATABASE,
  password: process.env.DB_PASSWORD,
  port: process.env.DB_PORT
})
client.connect();

function isDST(d) {
  let jan = new Date(d.getFullYear(), 0, 1).getTimezoneOffset();
  let jul = new Date(d.getFullYear(), 6, 1).getTimezoneOffset();
  return Math.max(jan, jul) !== d.getTimezoneOffset();    
}

// Get events
module.exports.getEvents = function(userId, start, end){
  return new Promise((resolve, reject)=>{
    let query = `SELECT e.id, start , "end", title, resource, location, notes, "allDay", e.color, unvalid, owner, concat(u.name, ' ', u.surname) as owner_name, (u.id = $1) as my_event, created, e.active, completion
    FROM events e
    LEFT JOIN "user" u on e.owner = u.id
    WHERE e.active = true`;
    let params = [userId];
    if (start && end){
      query = `SELECT e.id, start , "end", title, resource, location, notes, "allDay", e.color, unvalid, owner, concat(u.name, ' ', u.surname) as owner_name, (u.id = $1) as my_event, created, e.active, completion
      FROM events e
      LEFT JOIN "user" u on e.owner = u.id
      WHERE e.active = true AND
            ((start <= $2 AND "end" >= $3) OR
             ("end" >= $2 AND "end" <= $3) OR
             (start >= $2 AND start <= $3))`;
      // params.push(new Date(start).toISOString());
      // params.push(new Date(end).toISOString());
      params.push(start);
      params.push(end);
    }

    client.query(query,params,(err,res)=>{
      if(err) return reject(err);
      return resolve(res.rows);
    })
  })
}

// Get event by id
module.exports.getEventById = function(eventId){
  return new Promise((resolve, reject)=>{
    let query = `SELECT *
    FROM events
    WHERE id = $1`;
    let params = [eventId];

    client.query(query,params,(err,res)=>{
      if(err) return reject(err);
      return resolve(res.rows[0]);
    })
  })
}

//add new event
module.exports.addNewEvent = function(start, end, title, resource, location, notes, allDay, color, userId){
  return new Promise((resolve,reject)=>{
    // let DSTLabelStart = (isDST(new Date(start))) ? 'UTC-4' : 'UTC-2';
    // let DSTLabelEnd = (isDST(new Date(end))) ? 'UTC-4' : 'UTC-2';
    // nimam pojma zakaj ampak dela

    // let query = `INSERT INTO events(start, "end", title, resource, location, notes, "allDay", color, owner)
    // VALUES (timezone('` + DSTLabelStart + `', $1),timezone('` + DSTLabelEnd + `', $2), $3, $4, $5, $6, $7, $8, $9)
    // RETURNING *`;
    let query = `INSERT INTO events(start, "end", title, resource, location, notes, "allDay", color, owner)
    VALUES ($1, $2, $3, $4, $5, $6, $7, $8, $9)
    RETURNING *`;
    let tmpColor = color ? color : 'DEFAULT';
    let params = [start, end, title, resource, location, notes, allDay, tmpColor, userId];
    // example in datagrip
    // INSERT INTO events(start, "end", title, resource, location, notes, "allDay", color, owner)
    // VALUES ('2022-01-10T05:00','2022-01-13T16:00', 'Test create', '{user1,user6}', 'Roboteh', 'brez pripomb', true, DEFAULT, 1)
    // RETURNING *

    client.query(query,params,(err,res)=>{
      if(err) return reject(err);
      return resolve(res.rows[0]);
    })
  })
}
//edit event
module.exports.editEvent = function(eventId, start, end, title, resource, location, notes, allDay, color, unvalid){
  return new Promise((resolve,reject)=>{
    // let DSTLabelStart = (isDST(new Date(start))) ? 'UTC-4' : 'UTC-2';
    // let DSTLabelEnd = (isDST(new Date(end))) ? 'UTC-4' : 'UTC-2';
    // nimam pojma zakaj ampak dela

    // let query = `UPDATE events
    // SET (start, "end", title, resource, location, notes, "allDay", color) = (timezone('` + DSTLabelStart + `', $2),timezone('` + DSTLabelEnd + `', $3),$4,$5,$6,$7,$8,$9)
    // WHERE id = $1
    // RETURNING *`;
    // let query = `UPDATE events
    // SET (start, "end", title, resource, location, notes, "allDay", color, unvalid) = ($2, $3, $4, $5, $6, $7, $8, $9, $10)
    // WHERE id = $1
    // RETURNING *`;
    let query = `UPDATE events
    SET (start, "end", title, resource, location, notes, "allDay", color, unvalid) = ($2, $3, $4, $5, $6, $7, $8, $9, $10)
    WHERE id = $1
    RETURNING *`;
    let params = [eventId, start, end, title, resource, location, notes, allDay, color, unvalid]

    client.query(query,params,(err,res)=>{
      if(err) return reject(err);
      return resolve(res.rows[0]);
    })
  })
}
// update completion of event
module.exports.updateEventCompletion = function(id, completion){
  return new Promise((resolve,reject)=>{
    let query = `UPDATE events
    SET completion = $2
    WHERE id = $1
    RETURNING *`;
    let params = [id, completion];

    client.query(query,params,(err,res)=>{
      if(err) return reject(err);
      return resolve(res.rows[0]);
    })
  })
}
// delete event (update active to false or true)
module.exports.deleteEvent = function(eventId, activity){
  return new Promise((resolve,reject)=>{
    let params = [eventId, activity]
    let query = `UPDATE events
    SET active = $2
    WHERE id = $1
    RETURNING *`;

    client.query(query,params,(err,res)=>{
      if(err) return reject(err);
      return resolve(res.rows[0]);
    })
  })
}
//get event changes system
module.exports.getEventChangesSystem = function(eventId){
  return new Promise((resolve,reject)=>{
    let query = `SELECT c.id, c.id_event, c.date, "from", u.name, surname, cs.id as id_status, cs.status, ct.id as id_type, ct.type, notes
    FROM changes_system as c
    LEFT JOIN "user" u on c."from" = u.id
    LEFT JOIN change_status cs on c.status = cs.id
    LEFT JOIN change_type ct on c.type = ct.id
    WHERE c.id_event = $1
    ORDER BY date`;
    let params = [eventId];
    
    client.query(query,params,(err,res)=>{
      if(err) return reject(err);
      return resolve(res.rows);
    })
  })
}