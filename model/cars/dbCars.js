const { Client } = require('pg');

//console.log(process.env.DB_HOST);

const client = new Client({
  user: process.env.DB_USERNAME,
  host: process.env.DB_HOST,
  database: process.env.DB_DATABASE,
  password: process.env.DB_PASSWORD,
  port: process.env.DB_PORT
})
client.connect();

// Get cars
module.exports.getCars = function(){
  return new Promise((resolve, reject)=>{
    let query = `SELECT c.id, manufacturer, model, year, license_plate, notes, active, malfunction, maintenance, wheels, inspection, department_id, department, registration_expiration, vignette_expiration
    FROM cars c
    LEFT JOIN departments d on d.id = c.department_id
    ORDER BY c.id`;
    // let params = [userId];

    client.query(query,(err,res)=>{
      if(err) return reject(err);
      return resolve(res.rows);
    })
  })
}

// Get car by id
module.exports.getCarById = function(carId){
  return new Promise((resolve, reject)=>{
    let query = `SELECT c.id, manufacturer, model, year, license_plate, notes, active, malfunction, maintenance, wheels, inspection, department_id, department, registration_expiration, vignette_expiration
    FROM cars c
    LEFT JOIN departments d on d.id = c.department_id
    WHERE c.id = $1`;
    let params = [carId];

    client.query(query,params,(err,res)=>{
      if(err) return reject(err);
      return resolve(res.rows[0]);
    })
  })
}
// add new car
module.exports.addCar = function(manufacturer, model, year, licensePlate, notes, departmentId, registrationExpiration, vignetteExpiration){
  return new Promise((resolve, reject)=>{
    let query = `INSERT INTO cars (manufacturer, model, year, license_plate, notes, department_id, registration_expiration, vignette_expiration)
    VALUES ($1, $2, $3, $4, $5, $6, $7, $8)
    RETURNING *`;
    let params = [manufacturer, model, year, licensePlate, notes, departmentId, registrationExpiration, vignetteExpiration];

    client.query(query,params,(err,res)=>{
      if(err) return reject(err);
      return resolve(res.rows[0]);
    })
  })
}
//edit car 
module.exports.editCar = function(carId, manufacturer, model, year, licensePlate, notes, departmentId, registrationExpiration, vignetteExpiration){
  return new Promise((resolve,reject)=>{
    let query = `UPDATE cars
    SET (manufacturer, model, year, license_plate, notes, department_id, registration_expiration, vignette_expiration) = ($2, $3, $4, $5, $6, $7, $8, $9)
    WHERE id = $1
    RETURNING *`;
    let params = [carId, manufacturer, model, year, licensePlate, notes, departmentId, registrationExpiration, vignetteExpiration];

    client.query(query,params,(err,res)=>{
      if(err) return reject(err);
      return resolve(res.rows[0]);
    })
  })
}
// delete car (update active to false or true)
module.exports.deleteCar = function(carId, activity){
  return new Promise((resolve,reject)=>{
    let params = [carId, activity]
    let query = `UPDATE cars
    SET active = $2
    WHERE id = $1
    RETURNING *`;

    client.query(query,params,(err,res)=>{
      if(err) return reject(err);
      return resolve(res.rows[0]);
    })
  })
}

///////////////////////////////////////////////// CAR RESERVATIONS ///////////////////////////////////////////
// its a own table but is strongly connected to cars and right now the only functionality page cars have is to show their reservations
// also i have way too many db models and every db model creates its own connection and its own process and there are already too many to control (but i can find problem with a db model faster this way)

// Get car reservation by id
module.exports.getCarReservationById = function(carRevId){
  return new Promise((resolve, reject)=>{
    let query = `SELECT cr.id, created, owner, concat(o.name, ' ', o.surname) as owner_name, (o.id = $1) as my_creation, "user", concat(u.name, ' ', u.surname) as user_name, (u.id = $1) as my_reservation, car, keys, "allDay", start, "end", finished, cr.active, cr.color, location
    FROM cars_reservations cr
    LEFT JOIN "user" o on o.id = cr.owner
    LEFT JOIN "user" u on u.id = cr."user"
    WHERE cr.id = $1`;
    let params = [carRevId];

    client.query(query,params,(err,res)=>{
      if(err) return reject(err);
      return resolve(res.rows[0]);
    })
  })
}
// Get cars reservations
module.exports.getCarReservation = function(userId, start, end){
  return new Promise((resolve, reject)=>{
    let query = `SELECT cr.id, created, owner, concat(o.name, ' ', o.surname) as owner_name, (o.id = $1) as my_creation, "user", concat(u.name, ' ', u.surname) as user_name, (u.id = $1) as my_reservation, car, keys, "allDay", start, "end", finished, cr.active, cr.color, location
    FROM cars_reservations cr
    LEFT JOIN "user" o on o.id = cr.owner
    LEFT JOIN "user" u on u.id = cr."user"
    WHERE cr.active = true`;
    let params = [userId];
    if (start && end){
      query = `SELECT cr.id, created, owner, concat(o.name, ' ', o.surname) as owner_name, (o.id = $1) as my_creation, "user", concat(u.name, ' ', u.surname) as user_name, (u.id = $1) as my_reservation, car, keys, "allDay", start, "end", finished, cr.active, cr.color, location
      FROM cars_reservations cr
      LEFT JOIN "user" o on o.id = cr.owner
      LEFT JOIN "user" u on u.id = cr."user"
      WHERE cr.active = true AND
            ((start <= $2 AND "end" >= $3) OR
             ("end" >= $2 AND "end" <= $3) OR
             (start >= $2 AND start <= $3))`;
      // params.push(new Date(start).toISOString());
      // params.push(new Date(end).toISOString());
      params.push(start);
      params.push(end);
    }

    client.query(query,params,(err,res)=>{
      if(err) return reject(err);
      return resolve(res.rows);
    })
  })
}
//add new car reservation
module.exports.addNewCarRev = function(start, end, ownerId, userId, carId, color){
  return new Promise((resolve,reject)=>{
    let query = `INSERT INTO cars_reservations(start, "end", owner, "user", car, color)
    VALUES ($1, $2, $3, $4, $5, $6)
    RETURNING *`;
    let tmpColor = color ? color : '#1a73e8';
    let params = [start, end, ownerId, userId, carId, tmpColor];

    client.query(query,params,(err,res)=>{
      if(err) return reject(err);
      return resolve(res.rows[0]);
    })
  })
}
//edit car reservation
module.exports.editCarRev = function(carRevId, start, end, keys, location, userId, status, color, car){
  return new Promise((resolve,reject)=>{
    let query = `UPDATE cars_reservations
    SET (start, "end", keys, location, "user", finished, color, car) = ($2, $3, $4, $5, $6, $7, $8, $9)
    WHERE id = $1
    RETURNING *`;
    let params = [carRevId, start, end, keys, location, userId, status, color, car];

    client.query(query,params,(err,res)=>{
      if(err) return reject(err);
      return resolve(res.rows[0]);
    })
  })
}
// delete car reservation (update active to false or true)
module.exports.deleteCarRev = function(carRevId, activity){
  return new Promise((resolve,reject)=>{
    let params = [carRevId, activity]
    let query = `UPDATE cars_reservations
    SET active = $2
    WHERE id = $1
    RETURNING *`;

    client.query(query,params,(err,res)=>{
      if(err) return reject(err);
      return resolve(res.rows[0]);
    })
  })
}

///////////////////////////////////////////////// CAR NOTE STATUS ///////////////////////////////////////////

// Get all car status
module.exports.getCarNotesStatus = function(){
  return new Promise((resolve, reject)=>{
    let query = `SELECT *
    FROM cars_notes_status
    ORDER BY id`;

    client.query(query,(err,res)=>{
      if(err) return reject(err);
      return resolve(res.rows);
    })
  })
}

///////////////////////////////////////////////// CAR NOTES ///////////////////////////////////////////

// Get car notes by car id
module.exports.getCarNotesById = function(carId){
  return new Promise((resolve, reject)=>{
    let query = `SELECT cn.id, author_id, concat(u.name, ' ', u.surname) as author, status_id, status, car_id, created, start, "end", note, cn.active
    FROM cars_notes cn
    LEFT JOIN "user" u on u.id = cn.author_id
    LEFT JOIN cars_notes_status cns on cn.status_id = cns.id
    WHERE car_id = $1
    ORDER BY cn.id`;
    let params = [carId];

    client.query(query,params,(err,res)=>{
      if(err) return reject(err);
      return resolve(res.rows);
    })
  })
}
// Get all car notes
module.exports.getCarNotes = function(){
  return new Promise((resolve, reject)=>{
    let query = `SELECT cn.id, author_id, concat(u.name, ' ', u.surname) as author, status_id, status, car_id, created, start, "end", note, cn.active
    FROM cars_notes cn
    LEFT JOIN "user" u on u.id = cn.author_id
    LEFT JOIN cars_notes_status cns on cn.status_id = cns.id
    ORDER BY cn.id`;

    client.query(query,(err,res)=>{
      if(err) return reject(err);
      return resolve(res.rows);
    })
  })
}
// Get car note by id
module.exports.getCarNoteById = function(noteId){
  return new Promise((resolve, reject)=>{
    let query = `SELECT cn.id, author_id, concat(u.name, ' ', u.surname) as author, status_id, status, car_id, created, start, "end", note, cn.active
    FROM cars_notes cn
    LEFT JOIN "user" u on u.id = cn.author_id
    LEFT JOIN cars_notes_status cns on cn.status_id = cns.id
    WHERE cn.id = $1`;
    let params = [noteId];

    client.query(query,params,(err,res)=>{
      if(err) return reject(err);
      return resolve(res.rows[0]);
    })
  })
}
// Add new car note
module.exports.addNewCarNote = function(userId, statusId, carId, start, end, note){
  return new Promise((resolve, reject) => {
    let query = `INSERT INTO cars_notes(author_id, status_id, car_id, start, "end", note)
    VALUES ($1, $2, $3, $4, $5, $6)
    RETURNING *`;
    let params = [userId, statusId, carId, start, end, note];

    client.query(query,params,(err,res)=>{
      if(err) return reject(err);
      return resolve(res.rows[0]);
    })
  })
}
//edit car reservation
module.exports.editCarNote = function(carNoteId, statusId, start, end, note){
  return new Promise((resolve,reject)=>{
    let query = `UPDATE cars_notes
    SET (status_id, start, "end", note) = ($2, $3, $4, $5)
    WHERE id = $1
    RETURNING *`;
    let params = [carNoteId, statusId, start, end, note];

    client.query(query,params,(err,res)=>{
      if(err) return reject(err);
      return resolve(res.rows[0]);
    })
  })
}
// delete car note (update active to false or true)
module.exports.deleteCarNote = function(carNoteId, activity){
  return new Promise((resolve,reject)=>{
    let params = [carNoteId, activity]
    let query = `UPDATE cars_notes
    SET active = $2
    WHERE id = $1
    RETURNING *`;

    client.query(query,params,(err,res)=>{
      if(err) return reject(err);
      return resolve(res.rows[0]);
    })
  })
}