const { Client } = require('pg');

//console.log(process.env.DB_HOST);

const client = new Client({
  user: process.env.DB_USERNAME,
  host: process.env.DB_HOST,
  database: process.env.DB_DATABASE,
  password: process.env.DB_PASSWORD,
  port: process.env.DB_PORT
})
client.connect();

//get all project dates
module.exports.getProjectDates = function(projectId){
  return new Promise((resolve,reject)=>{
    let query = `SELECT *
    FROM project_dates
    WHERE id_project = $1
    ORDER BY date`;
    let params = [projectId];

    client.query(query,params,(err,res)=>{
      if(err) return reject(err);
      return resolve(res.rows);
    })
  })
}
//add new date for project
module.exports.addProjectDate = function(projectId, name, date){
  return new Promise((resolve,reject)=>{
    let query = `INSERT INTO project_dates(name, date, id_project)
    VALUES ($1, $2, $3)
    RETURNING id`;
    let params = [name, date, projectId];

    client.query(query,params,(err,res)=>{
      if(err) return reject(err);
      return resolve(res.rows);
    })
  })
}
//add new date for project
module.exports.editProjectDate = function(dateId, name, date){
  return new Promise((resolve,reject)=>{
    let query = `UPDATE project_dates
    SET (name,date) = ($2,$3)
    WHERE id = $1`;
    let params = [dateId, name, date];

    client.query(query,params,(err,res)=>{
      if(err) return reject(err);
      return resolve(res.rows);
    })
  })
}
//delete project date
module.exports.removeProjectDate = function(id){
  return new Promise((resolve,reject)=>{
    let query = `DELETE FROM project_dates
    WHERE id = $1`;
    let params = [id];

    client.query(query,params,(err,res)=>{
      if(err) return reject(err);
      return resolve(res.rows);
    })
  })
}