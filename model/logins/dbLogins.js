const { Client } = require('pg');

//console.log(process.env.DB_HOST);

const client = new Client({
  user: process.env.DB_USERNAME,
  host: process.env.DB_HOST,
  database: process.env.DB_DATABASE,
  password: process.env.DB_PASSWORD,
  port: process.env.DB_PORT
})
client.connect();


//get user
module.exports.getUser = function(username){
  return new Promise((resolve,reject)=>{
    let query = `SELECT u.id, username, name, password, u.role, surname, active, r.role as type
    FROM "user" u
    LEFT JOIN role r on u.role = r.id
    WHERE lower(username) = lower($1)`;
    let params = [username];

    client.query(query, params,(err,res)=>{
      if(err) return reject(err);
      return resolve(res.rows[0]);
    })
  })
}
//get last login with same ip address within given date
module.exports.getLastLoginWithIPAddress = function(userIp, date, n){
  return new Promise((resolve,reject)=>{
    let nbip = n ? 'n_bad_ip <= '+n+' AND': '';
    let query = `SELECT *
    FROM logins
    WHERE date > $2 AND
          `+nbip+`
          ip_address = $1
    ORDER BY id desc
    LIMIT 1`;
    let params = [userIp, date];

    client.query(query, params,(err,res)=>{
      if(err) return reject(err);
      return resolve(res.rows[0]);
    })
  })
}
//get last login with same username within given date
module.exports.getLastLoginWithUsername = function(username, date, n){
  return new Promise((resolve,reject)=>{
    let nbu = n ? 'n_bad_username <= '+n+' AND': '';
    let query = `SELECT *
    FROM logins
    WHERE date > $2 AND
          `+nbu+`
          username = $1
    ORDER BY id desc
    LIMIT 1`;
    let params = [username, date];

    client.query(query, params,(err,res)=>{
      if(err) return reject(err);
      return resolve(res.rows[0]);
    })
  })
}
//save login (nbip - n_bad_ip, nbu - n_bad_username)
module.exports.saveLogin = function(userIp, username, nbip, nbu, success){
  return new Promise((resolve,reject)=>{
    let params = [userIp, username, nbip, nbu];
    let successQuery = '', successSet = '';
    if (success) {
      successQuery = ', success';
      successSet = ', $5';
      params.push(success);
    }
    let query = `INSERT INTO logins(ip_address, username, n_bad_ip, n_bad_username` + successQuery + `)
    VALUES ($1, $2, $3, $4` + successSet + `)
    RETURNING *`;
    
    client.query(query,params,(err,res)=>{
      if(err) return reject(err);
      return resolve(res.rows[0]);
    })
  })
}