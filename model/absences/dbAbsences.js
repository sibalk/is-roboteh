const { Client } = require('pg');

//console.log(process.env.DB_HOST);

const client = new Client({
  user: process.env.DB_USERNAME,
  host: process.env.DB_HOST,
  database: process.env.DB_DATABASE,
  password: process.env.DB_PASSWORD,
  port: process.env.DB_PORT
})
client.connect();

// get all active absences inside time frame
module.exports.getAllAbsencesInsideTimeFrame = function(start, finish){
  return new Promise((resolve,reject)=>{
    let query = `SELECT a.id, a.name, a.id_user, id_reason, r.name as reason, a.start, a.finish, approved, a.created, a.active, u.name as user_name, u.surname as user_surname, role.role, pending_note, resolve_note, cs."from" as author_id, concat(cs.name,' ', cs.surname) as author, cs.date as created
    FROM absence AS a
    LEFT JOIN "user" u on a.id_user = u.id
    LEFT JOIN role role on u.role = role.id
    LEFT JOIN reason r on a.id_reason = r.id
    LEFT JOIN ( SELECT *
                FROM changes_system
                LEFT JOIN "user" u2 on changes_system."from" = u2.id
                WHERE status = 1 AND
                      type = 24 AND
                      id_absence IS NOT NULL) AS cs ON cs.id_absence = a.id
    WHERE a.active = TRUE AND
          a.approved = TRUE AND
          (a.start::date <= $2::date AND a.finish::date >= $1::date)
    ORDER BY id DESC`;
    let params = [start, finish];

    client.query(query,params,(err,res)=>{
      if(err) return reject(err);
      return resolve(res.rows);
    })
  })
}

//get all absences
module.exports.getAbsences = function(userId, date, roleGroup, status, active, limit, userRole, loggedUserId){
  return new Promise((resolve,reject)=>{
    let absencesStatus;
    switch (status) {
      case '0': absencesStatus = ''; break;
      case '1': absencesStatus = ' AND approved IS TRUE'; break;
      case '2': absencesStatus = ' AND approved IS FALSE'; break;
      case '3': absencesStatus = ' AND approved IS NULL'; break;
      default: absencesStatus = '';
    }
    let absencesRole;
    switch(roleGroup) {
      case 'konstrukterji': absencesRole = ` AND role.role ILIKE '%konstruk%'`; break;
      case 'električarji': absencesRole = ` AND role.role ILIKE '%elektri%'`; break;
      case 'strojniki': absencesRole = ` AND (role.role ILIKE '%strojni%' OR role.role LIKE '%strojni monter%' OR role.role LIKE '%CNC operater%')`; break;
      case 'varilci': absencesRole = ` AND role.role ILIKE '%varilec%'`; break;
      case 'cnc operaterji': absencesRole = ` AND role.role ILIKE '%cnc operater%'`; break;
      case 'serviserji': absencesRole = ` AND role.role ILIKE '%serviser%'`; break;
      case 'študenti': absencesRole = ` AND role.role ILIKE '%študent%'`; break;
      case 'programerji': absencesRole = ` AND role.role ILIKE '%programer%'`; break;
      case 'vodje': absencesRole = ` AND role.role ILIKE '%vodja%'`; break;
      case 'ostali': absencesRole = ` AND (role.role NOT ILIKE '%vodja%' AND role.role NOT ILIKE '%konstruk%' AND role.role NOT ILIKE '%elektri%' AND role.role NOT ILIKE '%strojn%' AND role.role NOT ILIKE '%programer%' AND role.role NOT ILIKE '%varilec%' AND role.role NOT ILIKE '%cnc operater%' AND role.role NOT ILIKE '%serviser%' AND role.role NOT ILIKE '%info%' AND role.role NOT ILIKE '%študent%')`; break;
      default: absencesRole = '';
    }
    let absencesUser = userId ? userId : 'id_user';
    let absencesActivity;
    switch(active) {
      case '0': absencesActivity = 'a.active'; break;
      case '1': absencesActivity = 'true'; break;
      case '2': absencesActivity = 'false'; break;
      default: absencesActivity = 'true';
    }
    let absencesLimit;
    let absenceDateQuery = '';
    if(date){
      absencesLimit = 'null';
      let tmpDate = new Date(date+'-02');
      let month = tmpDate.getMonth()+1;
      let year = tmpDate.getFullYear();
      absenceDateQuery = ` AND (date_part('month', a.start) = ` + month + ` AND date_part('year', a.start) = ` + year + ` OR date_part('month', a.finish) = ` + month + ` AND date_part('year', a.finish) = ` + year + `)`;
    }
    else{
      switch(limit) {
        case '10': absencesLimit = '10'; break;
        case '20': absencesLimit = '20'; break;
        case '30': absencesLimit = '30'; break;
        case '40': absencesLimit = '40'; break;
        case '50': absencesLimit = '50'; break;
        case '60': absencesLimit = '60'; break;
        case '70': absencesLimit = '70'; break;
        case '100': absencesLimit = '100'; break;
        case '-1': absencesLimit = 'null'; break;
        default: absencesLimit = '20';
      }
    }

    let userFilter = '';
    // only if there is loggedUserId, in case where all absences are requested, loggedUserId is not defined
    if (loggedUserId !== undefined && userRole !== 'admin' ) {
      userFilter = ` AND (a.id_user = ` + loggedUserId + `
        OR a.id_user IN (SELECT id_user FROM user_supervision WHERE user_supervision.id_supervisor = ` + loggedUserId + `)
        OR role.id IN (SELECT id_role FROM role_supervision WHERE role_supervision.id_user = ` + loggedUserId + `))`;
    }

    let query = `SELECT a.id, a.name, id_user, id_reason, r.name as reason, a.start, a.finish, approved, a.created, a.active, u.name as user_name, u.surname as user_surname, role.role, pending_note, resolve_note, notification_mails, reminder_mails, admin_mails, notification_date, reminder_date, admin_date
    FROM absence AS a
    LEFT JOIN "user" u on a.id_user = u.id
    LEFT JOIN role role on u.role = role.id
    LEFT JOIN reason r on a.id_reason = r.id
    WHERE id_user = ` + absencesUser + ` AND
          u.active = TRUE AND
          a.active = ` + absencesActivity + absencesRole + absenceDateQuery + absencesStatus + userFilter + `
    ORDER BY id DESC
    LIMIT ` + absencesLimit;
    //let params = [userId];

    client.query(query,(err,res)=>{
      if(err) return reject(err);
      return resolve(res.rows);
    })
  })
}
//get all absences
module.exports.getAbsenceById = function(absenceId){
  return new Promise((resolve,reject)=>{

    let query = `SELECT a.id, a.name, id_user, id_reason, r.name as reason, a.start, a.finish, approved, a.created, a.active, u.name as user_name, u.surname as user_surname, role.role, pending_note, resolve_note
    FROM absence AS a
    LEFT JOIN "user" u on a.id_user = u.id
    LEFT JOIN role role on u.role = role.id
    LEFT JOIN reason r on a.id_reason = r.id
    WHERE a.id = $1`;
    let params = [absenceId];

    client.query(query,params,(err,res)=>{
      if(err) return reject(err);
      return resolve(res.rows[0]);
    })
  })
}
//add new absence
module.exports.addNewAbsence = function(start, finish, reason, user, name, approved, note){
  return new Promise((resolve,reject)=>{
    let params = [start, finish, reason, user];
    let nameQuery = '';
    let nameSet = '';
    let approveQuery = '';
    let approveSet = '';
    let i = 5;
    if(name){
      nameQuery = ', name';
      nameSet = ', $'+i;
      i++;
      params.push(name);
    }
    if(approved){
      approveQuery = ', approved';
      approveSet = ', $'+i;
      i++;
      params.push(approved)
    }
    let noteQuery = '';
    let noteSet = '';
    if(note){
      noteQuery = ', pending_note';
      noteSet = ', $'+i;
      i++;
      params.push(note);
    }
    let query = `INSERT INTO absence(start, finish, id_reason, id_user`+nameQuery+approveQuery+noteQuery+`)
    VALUES ($1, $2, $3, $4`+nameSet+approveSet+noteSet+`)
    RETURNING *`;

    client.query(query,params,(err,res)=>{
      if(err) return reject(err);
      return resolve(res.rows[0]);
    })
  })
}
//get absences for specific user and specific task
module.exports.getUserAbsenceForTask = function(userId, taskId, start, finish){
  return new Promise((resolve,reject)=>{
    let query = `SELECT absence.id, absence.id_user, absence.id_project, absence.id_task, absence.id_reason, absence.start, absence.finish, absence.approved, reason.name as reason, "user".name, "user".surname
    FROM absence, reason, "user"
    WHERE id_user = $1 AND
          id_reason = reason.id AND
          id_user = "user".id AND
          ((id_task = $2) OR
          (id_project isnull AND
          id_task isnull )) AND
          approved = true AND
          absence.active = true AND
          ((start::date <= $4::date AND
          start::date >= $3::date) OR
          (finish::date <= $4::date AND
          finish::date >= $3::date))`;
    let params = [userId, taskId, start, finish];

    client.query(query,params,(err,res)=>{
      if(err) return reject(err);
      return resolve(res.rows);
    })
  })
}
//get all user absences
module.exports.getUserAbsence = function(userId){
  return new Promise((resolve,reject)=>{
    let query = `SELECT a.id, a.name, id_user, a.id_project, id_task, project_name, task_name, id_reason, r.name as reason, a.start, a.finish, approved, a.created, a.active
    FROM absence AS a
    RIGHT JOIN "user" u on a.id_user = u.id
    RIGHT JOIN reason r on a.id_reason = r.id
    LEFT JOIN  project p on a.id_project = p.id
    LEFT JOIN project_task pt on a.id_task = pt.id
    WHERE id_user = $1 AND
          (p.active IS NULL OR p.active = TRUE) AND
          (pt.active IS NULL  OR pt.active = TRUE)
    ORDER BY id DESC `;
    let params = [userId];

    client.query(query,params,(err,res)=>{
      if(err) return reject(err);
      return resolve(res.rows);
    })
  })
}
//create new absence
module.exports.createAbsence = function(start, finish, reason, user, projectId, taskId, name, approved){
  return new Promise((resolve,reject)=>{
    let params = [start, finish, reason, user];
    let projectQuery = '';
    let projectSet = '';
    let taskQuery = '';
    let taskSet = '';
    let nameQuery = '';
    let nameSet = '';
    let approveQuery = '';
    let approveSet = '';
    let i = 5;
    if(taskId){
      taskQuery = ', id_task';
      taskSet = ', $'+i;
      i++;
      params.push(taskId);
    }
    if(projectId){
      projectQuery = ', id_project';
      projectSet = ', $'+i;
      i++;
      params.push(projectId);
    }
    if(name){
      nameQuery = ', name';
      nameSet = ', $'+i;
      i++;
      params.push(name);
    }
    if(approved){
      approveQuery = ', approved';
      approveSet = ', $'+i;
      i++;
      params.push(approved)
    }
    let query = `INSERT INTO absence(start, finish, id_reason, id_user`+taskQuery+projectQuery+nameQuery+approveQuery+`)
    VALUES ($1, $2, $3, $4`+taskSet+projectSet+nameSet+approveSet+`)
    RETURNING id`;

    client.query(query,params,(err,res)=>{
      if(err) return reject(err);
      return resolve(res.rows[0]);
    })
  })
}
//update absence
module.exports.updateAbsence = function(id, start, finish, reason, name, note){
  return new Promise((resolve,reject)=>{
    let params = [id, start, finish, reason];
    let nameQuery = '';
    let nameSet = '';
    if(name){
      nameQuery = ', name';
      nameSet = ', $5';
      params.push(name);
    }
    else{
      nameQuery = ',name';
      nameSet = ', NULL';
    }
    let noteQuery = '';
    let noteSet = '';
    if(note){
      noteQuery = ', pending_note';
      noteSet = ', $6';
      params.push(note);
    }
    else{
      noteQuery = ', pending_note';
      noteSet = ', NULL';
    }
    let query = `UPDATE absence
    SET (start, finish, id_reason`+nameQuery+noteQuery+`) = ($2, $3, $4`+nameSet+noteSet+`)
    WHERE id = $1
    RETURNING *`;

    client.query(query,params,(err,res)=>{
      if(err) return reject(err);
      return resolve(res.rows[0]);
    })
  })
}
//get all reasons for absence (id, text) for select2
module.exports.getReasons = function(){
  return new Promise((resolve,reject)=>{
    let query = `SELECT id, name as reason, name as text
    FROM reason`;

    client.query(query,(err,res)=>{
      if(err) return reject(err);
      return resolve(res.rows);
    })
  })
}
//create new reason for absence
module.exports.createReason = function(name){
  return new Promise((resolve,rejct)=>{
    let query = `INSERT INTO reason(name)
    VALUES ($1)
    RETURNING id`;
    let params = [name];

    client.query(query,params,(err,res)=>{
      if(err) return reject(err);
      return resolve(res.rows[0]);
    })
  })
}
//Delete absence by id
module.exports.deleteAbsence = function(id, active){
  return new Promise((resolve,reject)=>{
    // let statusQuery = 'FALSE';
    // if(status){
    //   if(status == 1)
    //     statusQuery = 'TRUE';
    //   else
    //     statusQuery = 'FALSE';
    // }
    let query = `UPDATE absence
    SET active = $2
    WHERE id = $1
    RETURNING *`;
    let params = [id, active];

    client.query(query,params,(err,res)=>{
      if(err) return reject(err);
      return resolve(res.rows[0]);
    })
  })
}
//approve absence by id
module.exports.approveAbsence = function(id, status, note){
  return new Promise((resolve,reject)=>{
    // let statusQuery = 'TRUE';
    // if(status){
    //   if(status == 1)
    //     statusQuery = 'TRUE';
    //   else
    //     statusQuery = 'FALSE';
    // }
    let params = [id, status];
    let noteSet = '';
    let i = 3;
    if(note){
      noteSet = ', $'+i;
      params.push(note);
      i++;
    }
    else{
      noteSet = ', resolve_note';
    }
    let query = `UPDATE absence
    SET (approved, resolve_note) = ($2` + noteSet + `)
    WHERE id = $1
    RETURNING *`;

    client.query(query,params,(err,res)=>{
      if(err) return reject(err);
      return resolve(res.rows[0]);
    })
  })
}
//Get daily absences (for today or for specific date)
module.exports.getDailyAbsences = function(date){
  return new Promise((resolve,reject)=>{
    if(!date)
      date = 'now()';
    let params = [date];
    let query = `SELECT a.id, id_user, a.id_project, id_task, id_reason, r.name as reason, a.start, a.finish
    FROM absence AS a
    RIGHT JOIN "user" u on a.id_user = u.id
    RIGHT JOIN reason r on a.id_reason = r.id
    LEFT JOIN  project p on a.id_project = p.id
    LEFT JOIN project_task pt on a.id_task = pt.id
    WHERE (p.active IS NULL OR p.active = TRUE) AND
          (pt.active IS NULL  OR pt.active = TRUE) AND
          u.active = true and
          a.active = true and
          a.approved = true and
          (a.start::date <= $1::date AND a.finish::date >= $1::date)
    ORDER BY id DESC`;

    client.query(query,params,(err,res)=>{
      if(err) return reject(err);
      return resolve(res.rows);
    })
  })
}
// get absence changes system
module.exports.getAbsenceChangesSystem = function(absenceId){
  return new Promise((resolve,reject)=>{
    let query = `SELECT c.id, id_project, id_task, id_subtask, id_working_project, id_working_task, c.date, "from", id_note, id_subscriber, id_absence, u.name, surname, cs.id as id_status, cs.status, ct.id as id_type, ct.type
    FROM changes_system as c
    LEFT JOIN "user" u on c."from" = u.id
    LEFT JOIN change_status cs on c.status = cs.id
    LEFT JOIN change_type ct on c.type = ct.id
    WHERE id_absence = $1
    ORDER BY date`;
    let params = [absenceId];
    
    client.query(query,params,(err,res)=>{
      if(err) return reject(err);
      return resolve(res.rows);
    })
  })
}
// update absence notifaction mails with date and all the supervisor emails that need to be notified
module.exports.updateAbsenceNotificationMails = function(absenceId, notificationMails, notificationDate){
  return new Promise((resolve,reject)=>{
    let query = `UPDATE absence
    SET (notification_mails, notification_date) = ($2, $3)
    WHERE id = $1
    RETURNING *`;
    let params = [absenceId, notificationMails, notificationDate];

    client.query(query,params,(err,res)=>{
      if(err) return reject(err);
      return resolve(res.rows[0]);
    })
  })
}
// update absence reminder mails with date and all the supervisor emails that need to be reminded
module.exports.updateAbsenceReminderMails = function(absenceId, reminderMails, reminderDate){
  return new Promise((resolve,reject)=>{
    let query = `UPDATE absence
    SET (reminder_mails, reminder_date) = ($2, $3)
    WHERE id = $1
    RETURNING *`;
    let params = [absenceId, reminderMails, reminderDate];

    client.query(query,params,(err,res)=>{
      if(err) return reject(err);
      return resolve(res.rows[0]);
    })
  })
}
// update absence admin mails with date and all the admin emails that need to be notified
module.exports.updateAbsenceAdminMails = function(absenceId, adminMails, adminDate){
  return new Promise((resolve,reject)=>{
    let query = `UPDATE absence
    SET (admin_mails, admin_date) = ($2, $3)
    WHERE id = $1
    RETURNING *`;
    let params = [absenceId, adminMails, adminDate];

    client.query(query,params,(err,res)=>{
      if(err) return reject(err);
      return resolve(res.rows[0]);
    })
  })
}