const { Client } = require('pg');

//console.log(process.env.DB_HOST);

const client = new Client({
  user: process.env.DB_USERNAME,
  host: process.env.DB_HOST,
  database: process.env.DB_DATABASE,
  password: process.env.DB_PASSWORD,
  port: process.env.DB_PORT
})
client.connect();

//get all user reports
module.exports.getUserReportsAPI = function(userId) {
  return new Promise((resolve, reject) => {
    let query = `SELECT *
    FROM report
    WHERE user_id = $1
    ORDER BY id DESC`;
    let params = [userId];
    
    client.query(query,params,(err,res)=>{
      if(err) return reject(err);
      return resolve(res.rows);
    })
  })
}
//get all user reports
module.exports.getUserReportsForProjectAPI = function(userId, projectId) {
  return new Promise((resolve, reject)=>{
    let query = `SELECT *
    FROM report
    WHERE user_id = $1 AND project_id = $2`;
    let params = [userId, projectId];
    
    client.query(query,params,(err,res)=>{
      if(err) return reject(err);
      return resolve(res.rows);
    })
  })
}
//get report
module.exports.getReportAPI = function(reportId) {
  return new Promise((resolve, reject)=>{
    let query = `SELECT *
    FROM report
    WHERE id = $1`;
    let params = [reportId];
    
    client.query(query,params,(err,res)=>{
      if(err) return reject(err);
      return resolve(res.rows);
    })
  })
}
/*
//add new report
module.exports.addNewReport = function(userId,date,time,start,finish,projectId,report_type,other,description, radio){
  let params = [userId,date,time,start,finish,description,report_type]
  let radioQuery = '';
  if(radio == '0'){
    radioQuery = ', project_id';
    params.push(projectId);
  }
  else{
    radioQuery = ', other';
    params.push(other);
  }
  params.push(true);
  //let projectQuery = ', project_id';
  //if(projectId)
  let query = `INSERT INTO report(user_id, date, time, start, finish, description, report_type`+radioQuery+`, ma)
  VALUES ($1,$2,$3,$4,$5,$6,$7,$8,$9)
  RETURNING *`;
  return client.query(query,params)
  .then(res => {return res.rows[0];})
  .catch(e => {return e;})
}
*/
module.exports.addNewReport = function(userId,date,time,start,finish,projectId,report_type,other,description, radio){
  return new Promise((resolve, reject)=>{
    let params = [userId,date,time,start,finish,description,report_type]
    let radioQuery = '';
    if(radio == '0'){
      radioQuery = ', project_id';
      params.push(projectId);
    }
    else{
      radioQuery = ', other';
      params.push(other);
    }
    params.push(true);
    //let projectQuery = ', project_id';
    //if(projectId)
    let query = `INSERT INTO report(user_id, date, time, start, finish, description, report_type`+radioQuery+`, ma)
    VALUES ($1,$2,$3,$4,$5,$6,$7,$8,$9)
    RETURNING *`;

    client.query(query,params, (err,res)=>{
      if(err) return reject(err);
      return resolve(res.rows);
    })
  })
};

//edit report
module.exports.editReport = function(reportId,userId,date,time,start,finish,projectId,report_type,other,description, radio){
  return new Promise((resolve, reject)=>{
    let params = [reportId,userId,date,time,start,finish,description,report_type]
    let radioQuery = '';
    if(radio == '0'){
      radioQuery = ', project_id, other';
      params.push(projectId);
      params.push(null);
    }
    else{
      radioQuery = ', other, project_id';
      params.push(other);
      params.push(null);
    }
    let query = `UPDATE report
    SET (user_id, date, time, start, finish, description, report_type`+radioQuery+`) = ($2,$3,$4,$5,$6,$7,$8,$9,$10)
    WHERE id = $1
    RETURNING *`;
    client.query(query,params, (err,res)=>{
      if(err) return reject(err);
      return resolve(res.rows[0]);
    })
  })
}
//get report types
module.exports.getReportTypesAPI = function() {
  return new Promise((resolve, reject)=>{
    let query = `SELECT *
    FROM report_type`;
    
    client.query(query, (err,res)=>{
      if(err) return reject(err);
      return resolve(res.rows);
    })
  })
}
//add new report
module.exports.deleteReport = function(reportId){
  return new Promise((resolve, reject)=>{
    let params = [reportId]
    let query = `UPDATE report
    SET active = false
    WHERE id = $1
    RETURNING *`;
    
    client.query(query,params,(err,res)=>{
      if(err) return reject(err);
      return resolve(res.rows[0]);
    })
  })
}