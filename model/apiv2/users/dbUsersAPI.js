const { Client } = require('pg');

//console.log(process.env.DB_HOST);

const client = new Client({
  user: process.env.DB_USERNAME,
  host: process.env.DB_HOST,
  database: process.env.DB_DATABASE,
  password: process.env.DB_PASSWORD,
  port: process.env.DB_PORT
})
client.connect();

//get all user reports
module.exports.getUsersAPI = function() {
  return new Promise((resolve,reject)=>{
    let query = `SELECT id, concat(name, ' ', surname) as name, active
    FROM "user"
    WHERE name NOT ILIKE 'info'
    ORDER BY surname, name`;
    
    client.query(query,(err,res)=>{
      if(err) return reject(err);
      return resolve(res.rows);
    })
  })
}
//get all user reports
module.exports.getUserAPI = function(userId) {
  return new Promise((resolve,reject)=>{
    let query = `SELECT id, concat(name, ' ', surname) as name, active
    FROM "user"
    WHERE name NOT ILIKE 'info' AND
          id = $1
    ORDER BY surname, name`;
    let params = [userId];
  
    client.query(query,params,(err,res)=>{
      if(err) return reject(err);
      return resolve(res.rows[0]);
    })
  })
}