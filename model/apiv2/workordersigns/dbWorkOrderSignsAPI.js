const { Client } = require('pg');

//console.log(process.env.DB_HOST);

const client = new Client({
  user: process.env.DB_USERNAME,
  host: process.env.DB_HOST,
  database: process.env.DB_DATABASE,
  password: process.env.DB_PASSWORD,
  port: process.env.DB_PORT
})
client.connect();

//Add new file for project
module.exports.addFileForUnknownWorkOrder = function(fileName, pathName, userId, type){
  return new Promise((resolve,reject)=>{
    let query = `INSERT INTO work_order_signs(original_name, path_name, user_id, type)
    VALUES ($1, $2, $3, $4)
    RETURNING *`;
    let params = [fileName, pathName, userId, type];

    client.query(query,params,(err,res)=>{
      if(err) return reject(err);
      return resolve(res.rows[0]);
    })
  })
}
//update file active to false, file no longer exist
module.exports.updateActiveFile = function(filename){
  return new Promise((resolve,reject)=>{
    let query = `UPDATE work_order_signs
    SET active = false
    WHERE path_name = $1`;
    let params = [filename];

    client.query(query,params,(err,res)=>{
      if(err) return reject(err);
      return resolve(res.rows);
    })
  })
}