const { Client } = require('pg');

//console.log(process.env.DB_HOST);

const client = new Client({
  user: process.env.DB_USERNAME,
  host: process.env.DB_HOST,
  database: process.env.DB_DATABASE,
  password: process.env.DB_PASSWORD,
  port: process.env.DB_PORT
})
client.connect();

module.exports.getSubscribersAPI = function() {
  return new Promise((resolve,reject)=>{
    let query = `SELECT *
    FROM subscriber`;
    
    client.query(query,(err,res)=>{
      if(err) return reject(err);
      return resolve(res.rows);
    })
  })
}
module.exports.getSubscriberAPI = function(subscriberId) {
  return new Promise((resolve,reject)=>{
    let query = `SELECT *
    FROM subscriber
    WHERE id = $1`;
    let params = [subscriberId];
    
    client.query(query,params,(err,res)=>{
      if(err) return reject(err);
      return resolve(res.rows);
    })
  })
}
module.exports.addSubscribersAPI = function(name) {
  return new Promise((resolve,reject)=>{
    let query = `INSERT INTO subscriber (name) VALUES ($1) RETURNING *`;
    let params = [name];
    
    client.query(query,params,(err,res)=>{
      if(err) return reject(err);
      return resolve(res.rows);
    })
  })
}