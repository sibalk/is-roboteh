const { Client } = require('pg');

//console.log(process.env.DB_HOST);

const client = new Client({
  user: process.env.DB_USERNAME,
  host: process.env.DB_HOST,
  database: process.env.DB_DATABASE,
  password: process.env.DB_PASSWORD,
  port: process.env.DB_PORT
})
client.connect();

module.exports.getUserProjectsAPI = function(userId) {
  return new Promise((resolve,reject)=>{
    let query = `SELECT DISTINCT project.id as project_id, project_number, project_name, subscriber.name as subscriber_name, subscriber.id as subscriber_id, icon as subscriber_icon, completion, completion = 100 as finished, start, finish, notes, project.active as active
    FROM "user", project, working_project, subscriber
    WHERE id_user = "user".id AND
          id_project = project.id AND
          project.subscriber = subscriber.id AND
          "user".id = $1
    ORDER BY finished, project.id desc`;
    let params = [userId];
    
    client.query(query,params,(err,res)=>{
      if(err) return reject(err);
      return resolve(res.rows);
    })
  })
}
module.exports.getAllProjectsAPI = function(userId) {
  return new Promise((resolve,reject)=>{
    let query = `SELECT p.id as project_id, project_number, project_name, s.name as subscriber_name, s.id as subscriber_id, icon as subscriber_icon, completion, completion = 100 as finished, start, finish, notes, p.active as active
    FROM project AS p
    LEFT JOIN subscriber AS s ON p.subscriber = s.id
    ORDER BY finished, p.id desc`;
    //let params = [userId];
    
    client.query(query,(err,res)=>{
      if(err) return reject(err);
      return resolve(res.rows);
    })
  })
}
module.exports.getProjectAPI = function(projectId) {
  return new Promise((resolve,reject)=>{
    let query = `SELECT DISTINCT project.id as project_id, project_number, project_name, subscriber.name as subscriber_name, subscriber.id as subscriber_id, icon as subscriber_icon, completion, completion = 100 as finished, start, finish, notes, project.active as active
    FROM project, subscriber
    WHERE project.subscriber = subscriber.id AND
          project.id = $1
    ORDER BY finished, project.id desc`;
    let params = [projectId];
    
    client.query(query,params,(err,res)=>{
      if(err) return reject(err);
      return resolve(res.rows);
    })
  })
}