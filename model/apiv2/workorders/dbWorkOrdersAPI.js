const { Client } = require('pg');

//console.log(process.env.DB_HOST);

const client = new Client({
  user: process.env.DB_USERNAME,
  host: process.env.DB_HOST,
  database: process.env.DB_DATABASE,
  password: process.env.DB_PASSWORD,
  port: process.env.DB_PORT
})
client.connect();

//get all user work orders
module.exports.getUserWorkOrdersAPI = function(userId) {
  return new Promise((resolve,reject)=>{
    let query = `SELECT wo.id, subscriber_id, project_id, date, location, description, vehicle, arrival, departure, work_order_type_id, wo.active, user_id, representative, created, location_cord, ma, concat("left"(u.name,1), "left"(surname,1),'-',wo.id,'/',date_part('year', date)) as number, user_id = $1 as author, email, status_id, status_id !=1 as  closed
    FROM work_order as wo
    LEFT JOIN work_order_workers wow on wo.id = wow.work_order_id
    LEFT JOIN "user" u on wo.user_id = u.id
    WHERE wow.worker_id = $1 OR user_id = $1
    GROUP BY wo.id, name, surname`;
    let params = [userId];
    
    client.query(query,params,(err,res)=>{
      if(err) return reject(err);
      return resolve(res.rows);
    })
  })
}
//get all user work orders
module.exports.getUserWorkOrdersForSubscriberAPI = function(userId, subscriberId) {
  return new Promise((resolve,reject)=>{
    let query = `SELECT wo.id, subscriber_id, project_id, date, location, description, vehicle, arrival, departure, work_order_type_id, wo.active, user_id, representative, created, location_cord, ma, concat("left"(u.name,1), "left"(surname,1),'-',wo.id,'/',date_part('year', date)) as number, user_id = $1 as author, email
    FROM work_order as wo
    LEFT JOIN work_order_workers wow on wo.id = wow.work_order_id
    LEFT JOIN "user" u on wo.user_id = u.id
    WHERE (wow.worker_id = $1 OR user_id = $1) AND subscriber_id = $2
    GROUP BY wo.id, name, surname`;
    let params = [userId, subscriberId];
    
    client.query(query,params,(err,res)=>{
      if(err) return reject(err);
      return resolve(res.rows);
    })
  })
}
//get work order
module.exports.getWorkOrderAPI = function(workOrderId,userId) {
  return new Promise((resolve,reject)=>{
    let query = `SELECT wo.id, subscriber_id, project_id, number, date, location, description, vehicle, arrival, departure, work_order_type_id, wo.active, user_id, representative, created, location_cord, ma, concat("left"(u.name,1), "left"(surname,1),'-',wo.id,'/',date_part('year', date)) as number, user_id = $2 as author, email
    FROM work_order as wo
    LEFT JOIN "user" u on wo.user_id = u.id
    WHERE wo.id = $1`;
    let params = [workOrderId, userId];
    
    client.query(query,params,(err,res)=>{
      if(err) return reject(err);
      return resolve(res.rows);
    })
  })
}
//get work order types
module.exports.getWorkOrderTypesAPI = function() {
  return new Promise((resolve,reject)=>{
    let query = `SELECT *
    FROM work_order_type`;
    
    client.query(query,(err,res)=>{
      if(err) return reject(err);
      return resolve(res.rows);
    })
  })
}
//get work order workers
module.exports.getWorkOrderWorkersAPI = function(workOrderId) {
  return new Promise((resolve,reject)=>{
    let query = `SELECT wow.worker_id as id, concat(name,' ',surname) as worker_name
    FROM work_order_workers as wow
    LEFT JOIN "user" u on wow.worker_id = u.id
    WHERE work_order_id = $1`;
    let params = [workOrderId]
    
    client.query(query,params,(err,res)=>{
      if(err) return reject(err);
      return resolve(res.rows);
    })
  })
}
//get work order files
module.exports.getWorkOrderFilesAPI = function(workOrderId) {
  return new Promise((resolve,reject)=>{
    let query = `SELECT wof.id, original_name, path_name, user_id, date, type, note, active
    FROM work_order_files_keys as wofk
    LEFT JOIN work_order_files wof on wofk.work_order_file_id = wof.id
    WHERE work_order_id = $1`;
    let params = [workOrderId]
    
    client.query(query,params,(err,res)=>{
      if(err) return reject(err);
      return resolve(res.rows);
    })
  })
}
//get work order signs
module.exports.getWorkOrderSignsAPI = function(workOrderId) {
  return new Promise((resolve,reject)=>{
    let query = `SELECT wos.id, original_name, path_name, user_id, date, type, note, active
    FROM work_order_signs_keys as wosk
    LEFT JOIN work_order_signs wos on wosk.work_order_sign_id = wos.id
    WHERE work_order_id = $1`;
    let params = [workOrderId]
    
    client.query(query,params,(err,res)=>{
      if(err) return reject(err);
      return resolve(res.rows);
    })
  })
}
// get work order sign with signId
module.exports.getSignWithIdAPI = function(signId) {
  return new Promise((resolve,reject)=>{
    let query = `SELECT id, original_name, path_name, user_id, date, type, note, active
    FROM work_order_signs
    WHERE id = $1`;
    let params = [signId]
    
    client.query(query,params,(err,res)=>{
      if(err) return reject(err);
      return resolve(res.rows);
    })
  })
}
//add new work order
module.exports.addNewWorkOrder = function(userId,number,type,date,location,projectId,subscriberId,description,vehicle,arrival,departure,representative,locationCord,email){
  return new Promise((resolve,reject)=>{
    if(projectId == 0){
      projectId = null;
    }
    if(subscriberId == 0){
      subscriberId = null;
    }
    if(!locationCord)
      locationCord = null;
    if(!email)
      email = null;
    let params = [userId,number,type,date,location,subscriberId,projectId,description,vehicle,arrival,departure,representative,locationCord,email]
    //let projectQuery = ', project_id';
    //if(projectId)
    let query = `INSERT INTO work_order(subscriber_id, project_id, number, date, location, description, vehicle, arrival, departure, work_order_type_id, user_id, representative, location_cord, email, ma)
    VALUES ($6,$7,$2,$4,$5,$8,$9,$10,$11,$3,$1,$12,$13,$14,true)
    RETURNING *`;

    client.query(query,params,(err,res)=>{
      if(err) return reject(err);
      return resolve(res.rows[0]);
    })
  })
}
//edit work order
module.exports.editWorkOrder = function(woId,number,type,date,location,projectId,subscriberId,description,vehicle,arrival,departure,representative,locationCord,email){
  return new Promise((resolve,reject)=>{
    if(projectId == 0){
      projectId = null;
    }
    if(subscriberId == 0){
      subscriberId = null;
    }
    if(!locationCord)
      locationCord = null;
    if(!email)
      email = null;
    let params = [number,type,date,location,subscriberId,projectId,description,vehicle,arrival,departure,representative,woId,locationCord,email]
    //let projectQuery = ', project_id';
    //if(projectId)
    let query = `UPDATE work_order
    SET (subscriber_id, project_id, number, date, location, description, vehicle, arrival, departure, work_order_type_id, representative, location_cord, email) = ($5,$6,$1,$3,$4,$7,$8,$9,$10,$2,$11,$13,$14)
    WHERE id = $12
    RETURNING *`;

    client.query(query,params,(err,res)=>{
      if(err) return reject(err);
      return resolve(res.rows[0]);
    })
  })
}
//add new worker for work order id
module.exports.addWorker = function(woId, userId){
  return new Promise((resolve,reject)=>{
    let query = `INSERT INTO work_order_workers(worker_id, work_order_id)
    VALUES ($1,$2)`;
    let params = [userId, woId];

    client.query(query,params,(err,res)=>{
      if(err) return reject(err);
      return resolve(res.rows);
    })
  })
}
//add new worker for work order id
module.exports.deleteWorker = function(wowId, userId){
  return new Promise((resolve,reject)=>{
    let query = `DELETE FROM work_order_workers
    WHERE work_order_id = $1 AND worker_id = $2`;
    let params = [wowId, userId];

    client.query(query,params,(err,res)=>{
      if(err) return reject(err);
      return resolve(res.rows);
    })
  })
}
//delete workers for work order id
module.exports.deleteWorkers = function(wowId){
  return new Promise((resolve,reject)=>{
    let query = `DELETE FROM work_order_workers
    WHERE work_order_id = $1`;
    let params = [wowId];

    client.query(query,params,(err,res)=>{
      if(err) return reject(err);
      return resolve(res.rows);
    })
  })
}
//add new file for work order id
module.exports.addFile = function(woId, fileId){
  return new Promise((resolve,reject)=>{
    let query = `INSERT INTO work_order_files_keys(work_order_file_id, work_order_id)
    VALUES ($1,$2)`;
    let params = [fileId, woId];

    client.query(query,params,(err,res)=>{
      if(err) return reject(err);
      return resolve(res.rows);
    })
  })
}
//delete files for work order id
module.exports.deleteFile = function(wowId, fileId){
  return new Promise((resolve,reject)=>{
    let query = `DELETE FROM work_order_files_keys
    WHERE work_order_id = $1 AND work_order_file_id = $2`;
    let params = [wowId, fileId];

    client.query(query,params,(err,res)=>{
      if(err) return reject(err);
      return resolve(res.rows);
    })
  })
}
//delete files for work order id
module.exports.deleteFiles = function(wowId){
  return new Promise((resolve,reject)=>{
    let query = `DELETE FROM work_order_files_keys
    WHERE work_order_id = $1`;
    let params = [wowId];

    client.query(query,params,(err,res)=>{
      if(err) return reject(err);
      return resolve(res.rows);
    })
  })
}
//add new sign for work order id
module.exports.addSign = function(woId, signId){
  return new Promise((resolve,reject)=>{
    let query = `INSERT INTO work_order_signs_keys(work_order_sign_id, work_order_id)
    VALUES ($1,$2)`;
    let params = [signId, woId];

    client.query(query,params,(err,res)=>{
      if(err) return reject(err);
      return resolve(res.rows);
    })
  })
}
//delete signs for work order id
module.exports.deleteSign = function(wowId, signId){
  return new Promise((resolve,reject)=>{
    let query = `DELETE FROM work_order_signs_keys
    WHERE work_order_id = $1 AND work_order_sign_id = $2`;
    let params = [wowId, signId];

    client.query(query,params,(err,res)=>{
      if(err) return reject(err);
      return resolve(res.rows);
    })
  })
}
//delete signs for work order id
module.exports.deleteSigns = function(wowId){
  return new Promise((resolve,reject)=>{
    let query = `DELETE FROM work_order_signs_keys
    WHERE work_order_id = $1`;
    let params = [wowId];

    client.query(query,params,(err,res)=>{
      if(err) return reject(err);
      return resolve(res.rows);
    })
  })
}
//add new report
module.exports.deleteWorkOrder = function(workOrderId){
  return new Promise((resolve,reject)=>{
    let params = [workOrderId]
    let query = `UPDATE work_order
    SET active = false
    WHERE id = $1
    RETURNING *`;

    client.query(query,params,(err,res)=>{
      if(err) return reject(err);
      return resolve(res.rows[0]);
    })
  })
}
//get work order expenses
module.exports.getWorkOrderExpenses = function(workOrderId){
  return new Promise((resolve, reject)=>{
    let query = `SELECT *
    FROM work_order_expenses
    WHERE work_order_id = $1`;
    let params = [workOrderId];

    client.query(query,params, (err,res)=>{
      if(err) return reject(err);
      return resolve(res.rows);
    })
  })
};
//get work order materials
module.exports.getWorkOrderMaterial = function(workOrderId){
  return new Promise((resolve,reject)=>{
    let params = [workOrderId];
    let query = `SELECT *
    FROM work_order_material
    WHERE work_order_id = $1`;

    client.query(query,params,(err,res)=>{
      if(err) return reject(err);
      return resolve(res.rows);
    })
  })
}
//save email for selected work order
module.exports.saveEMail = function(woId, email, notificationMail, officeMail){
  return new Promise((resolve,reject)=>{
    let query = `UPDATE work_order
    SET email = $2
    Where id = $1`;
    let params = [woId,email];
    if (notificationMail) {
      query = `UPDATE work_order
      SET notification_mail = $2
      Where id = $1`;
    }
    else if (officeMail) {
      query = `UPDATE work_order
      SET office_mail = $2
      Where id = $1`;
    }

    client.query(query,params,(err,res)=>{
      if(err) return reject(err);
      return resolve(res.rows);
    })
  })
}