const { Client } = require('pg');
const { param } = require('../../routes/changes');

//console.log(process.env.DB_HOST);

const client = new Client({
  user: process.env.DB_USERNAME,
  host: process.env.DB_HOST,
  database: process.env.DB_DATABASE,
  password: process.env.DB_PASSWORD,
  port: process.env.DB_PORT
})
client.connect();

//get changes for id from date1 to date2 with type, status, or reverse(for is from)
module.exports.getUserChanges = function(userId,start,finish,type,status,activity,reverse){
  return new Promise((resolve,reject)=>{
    let params = [userId];
    let startQuery = 'c.date';
    let finishQuery = 'c.date';
    let typeQuery = 'c.type';
    let statusQuery = 'c.status';
    let activityQuery = 'c.active';
    let reverseQuery = 'c."for"';
    let i = 2;
    if(start){
      startQuery = '$'+i;
      params.push(start);
      i++;
    }
    if(finish){
      finishQuery = '$'+i;
      params.push(finish);
      i++; 
    }
    if(type){
      typeQuery = '$'+i;
      params.push(type);
      i++;
    }
    if(status){
      statusQuery = '$'+i;
      params.push(status);
      i++;
    }
    if(activity){
      activityQuery = '$'+i;
      params.push(activity);
      i++;
    }
    if(reverse == "true") reverseQuery = 'c."from"';
  
    let query = `SELECT c.id, c.id_project, c.id_task, c.id_subtask, c.id_working_project, c.id_working_task, c.id_note, c.id_absence, c.id_subscriber, c.id_file, c.id_work_order, c.date, c.status, c."for", c."from", c.type, c.active, u.name as user_name, u.surname as user_surname, u1.name as for_name, u1.surname as for_surname, cs.status as status_label, ct.type as type_label, s.name as subscriber_label, p.project_name, pt.task_name, st.name as subtask_name, f.original_name as file_label, a.name as absence_label, n.note as note_label, concat(left(u.name,1),left(u.surname,1),'-',wo.id, '/', date_part('year', wo.date)) as work_order_label
    FROM changes as c
    LEFT JOIN "user" u on c."from" = u.id
    LEFT JOIN "user" u1 on c."for" = u1.id
    LEFT JOIN change_status cs on c.status = cs.id
    LEFT JOIN change_type ct on c.type = ct.id
    LEFT JOIN project_task pt on c.id_task = pt.id
    LEFT JOIN subtask st on c.id_subtask = st.id
    LEFT JOIN absence a on c.id_absence = a.id
    LEFT JOIN files f on c.id_file = f.id
    LEFT JOIN subscriber s on c.id_subscriber = s.id
    LEFT JOIN project p on c.id_project = p.id
    LEFT JOIN notes n on c.id_note = n.id
    LEFT JOIN work_order wo on c.id_work_order = wo.id
    WHERE `+reverseQuery+` = $1 AND
          (c.date::date >= `+startQuery+`::date AND c.date::date <= `+finishQuery+`::date) AND
          c.type = `+typeQuery+` AND
          c.status = `+statusQuery+` AND
          c.active = `+activityQuery+`
    ORDER BY  date`;

    client.query(query,params,(err,res)=>{
      if(err) return reject(err)
      return resolve(res.rows);
    })
  })
}
//get notify changes for id from date1 to date2 with type, status, or reverse(for is from)
module.exports.getUserNotify = function(userId,start,finish,type,status,activity,reverse){
  return new Promise((resolve,reject)=>{
    let params = [userId];
    let startQuery = 'cs.date';
    let finishQuery = 'cs.date';
    let typeQuery = '= cs.type';
    let statusQuery = 'cs.status';
    let activityQuery = 'cn.active';
    let reverseQuery = 'cn."for"';
    let limitAddOnQuery = 'LIMIT 20';
    let i = 2;
    if (start){
      startQuery = '$'+i;
      params.push(start);
      i++;
    }
    if (finish){
      finishQuery = '$'+i;
      params.push(finish);
      i++; 
    }
    if (type){
      if (type == '-1'){
        typeQuery = '<= $'+i;
        params.push(14);
      }
      else{
        typeQuery = '= $'+i;
        params.push(type);
      }
      i++;
    }
    if (status){
      statusQuery = '$'+i;
      params.push(status);
      i++;
    }
    if (activity){
      activityQuery = '$'+i;
      params.push(activity);
      i++;
    }
    if (reverse == "true") reverseQuery = 'cs."from"';
    if (start && finish) limitAddOnQuery = 'LIMIT 200';
  
    let query = `SELECT cn.id, cs.id_project, cs.id_task, cs.id_subtask, cs.id_working_project, cs.id_working_task, cs.id_note, cs.id_absence, cs.id_subscriber, cs.id_file, cs.id_work_order, cs.date, cs.status, cn."for", cs."from", cs.type, cn.active, u.name as user_name, u.surname as user_surname, u1.name as for_name, u1.surname as for_surname, cstatus.status as status_label, ctype.type as type_label, s.name as subscriber_label, p.project_name, pt.task_name, st.name as subtask_name, f.original_name as file_label, a.name as absence_label, n.note as note_label, concat(left(u.name,1),left(u.surname,1),'-',wo.id, '/', date_part('year', wo.date)) as work_order_label
    FROM changes_notify as cn
    LEFT JOIN changes_system cs on cn.id_change = cs.id
    LEFT JOIN "user" u on cs."from" = u.id
    LEFT JOIN "user" u1 on cn."for" = u1.id
    LEFT JOIN change_status cstatus on cs.status = cstatus.id
    LEFT JOIN change_type ctype on cs.type = ctype.id
    LEFT JOIN project_task pt on cs.id_task = pt.id
    LEFT JOIN subtask st on cs.id_subtask = st.id
    LEFT JOIN absence a on cs.id_absence = a.id
    LEFT JOIN files f on cs.id_file = f.id
    LEFT JOIN subscriber s on cs.id_subscriber = s.id
    LEFT JOIN project p on cs.id_project = p.id
    LEFT JOIN notes n on cs.id_note = n.id
    LEFT JOIN work_order wo on cs.id_work_order = wo.id
    WHERE `+reverseQuery+` = $1 AND
          (cs.date::date >= `+startQuery+`::date AND cs.date::date <= `+finishQuery+`::date) AND
          cs.type `+typeQuery+` AND
          cs.status = `+statusQuery+` AND
          cn.active = `+activityQuery+`
    ORDER BY  date desc
    `+limitAddOnQuery;

    client.query(query,params,(err,res)=>{
      if(err) return reject(err)
      return resolve(res.rows);
    })
  })
}
//get notify changes for id for last 100 notifications, first draw notification on notification control page
module.exports.getUserNotifyLast100 = function(userId){
  return new Promise((resolve,reject)=>{
    let params = [userId];
    let query = `SELECT *
    FROM (  SELECT cn.id, cs.id_project, cs.id_task, cs.id_subtask, cs.id_working_project, cs.id_working_task, cs.id_note, cs.id_absence, cs.id_subscriber, cs.id_file, cs.id_work_order, cs.date, cs.status, cn."for", cs."from", cs.type, cn.active, u.name as user_name, u.surname as user_surname, u1.name as for_name, u1.surname as for_surname, cstatus.status as status_label, ctype.type as type_label, s.name as subscriber_label, p.project_name, pt.task_name, st.name as subtask_name, f.original_name as file_label, a.name as absence_label, n.note as note_label, concat(left(u.name,1),left(u.surname,1),'-',wo.id, '/', date_part('year', wo.date)) as work_order_label
            FROM changes_notify as cn
            LEFT JOIN changes_system cs on cn.id_change = cs.id
            LEFT JOIN "user" u on cs."from" = u.id
            LEFT JOIN "user" u1 on cn."for" = u1.id
            LEFT JOIN change_status cstatus on cs.status = cstatus.id
            LEFT JOIN change_type ctype on cs.type = ctype.id
            LEFT JOIN project_task pt on cs.id_task = pt.id
            LEFT JOIN subtask st on cs.id_subtask = st.id
            LEFT JOIN absence a on cs.id_absence = a.id
            LEFT JOIN files f on cs.id_file = f.id
            LEFT JOIN subscriber s on cs.id_subscriber = s.id
            LEFT JOIN project p on cs.id_project = p.id
            LEFT JOIN notes n on cs.id_note = n.id
            LEFT JOIN work_order wo on cs.id_work_order = wo.id
            WHERE cn."for" = $1 AND
                  (cs.date::date >= cs.date::date AND cs.date::date <= cs.date::date) AND
                  cs.type = cs.type AND
                  cs.status = cs.status AND
                  cn.active = cn.active
            ORDER BY  date desc
            LIMIT 100) as a
    ORDER BY date`;

    client.query(query,params,(err,res)=>{
      if(err) return reject(err)
      return resolve(res.rows);
    })
  })
}
//dismiss change (mark change as inactive)
module.exports.dismissChange = function(id){
  return new Promise((resolve,reject)=>{
    let query = `UPDATE changes
    SET active = false
    WHERE id = $1`;
    let params = [id];

    client.query(query,params,(err,res)=>{
      if(err) return reject(err);
      return resolve(res.rows);
    })
  })
}
//dismiss notify (mark change as inactive)
module.exports.dismissNotify = function(id){
  return new Promise((resolve,reject)=>{
    let query = `UPDATE changes_notify
    SET (active, noticed) = (false, now())
    WHERE id = $1`;
    let params = [id];

    client.query(query,params,(err,res)=>{
      if(err) return reject(err);
      return resolve(res.rows);
    })
  })
}
//change activity of change (and record who did it and when)
module.exports.modifyChange = function(changeId, userId, activity){
  return new Promise((resolve,reject)=>{
    let query = `UPDATE changes
    SET (modified_user_id, modified_date, active) = ($2,now(), $3)
    WHERE id = $1`;
    let params = [changeId,userId,activity];

    client.query(query,params,(err,res)=>{
      if(err) return reject(err);
      return resolve(res.rows);
    })
  })
}
//change activity of notify change (and record who did it and when)
module.exports.modifyNotify = function(changeId, userId, activity){
  return new Promise((resolve,reject)=>{
    let query = `UPDATE changes_notify
    SET (modified_user_id, modified_date, active) = ($2,now(), $3)
    WHERE id = $1`;
    let params = [changeId,userId,activity];

    client.query(query,params,(err,res)=>{
      if(err) return reject(err);
      return resolve(res.rows);
    })
  })
}
//get users to notify of specific task (project leaders and all task workers)
module.exports.getTaskNotifyUsers = function(projectId, taskId, userId){
  return new Promise((resolve,reject)=>{
    let query = `SELECT pt.id, w.workers, w.workers_id as task_workers, w2.workers, w2.workers_id as project_workers
    FROM project_task as pt
    LEFT JOIN ( SELECT id_task, string_agg(id_user::text, ',') as workers_id, string_agg(concat(name::text, concat(' ', surname::text)), ', ') as workers
                FROM working_task, "user"
                WHERE id_user = "user".id AND
                      id_user != $1
                GROUP BY id_task ) AS w ON pt.id = w.id_task
    LEFT JOIN (SELECT id_project, string_agg(id_user::text, ',') as workers_id, string_agg(concat(name::text, concat(' ', surname::text)), ', ') as workers
                FROM working_project, "user", work_role
                WHERE id_user = "user".id AND
                      id_user != $1 AND
                      "id_workingRole" = work_role.id AND
                      (work_role."workRole" = 'vodja' OR work_role."workRole" = 'vodja projekta' OR work_role."workRole" = 'vodja strojnikov' OR work_role."workRole" = 'vodja CNC obdelave' OR work_role."workRole" = 'vodja električarjev' OR work_role."workRole" = 'vodja programerjev')
                GROUP BY id_project) AS w2 ON pt.id_project = w2.id_project
    WHERE pt.id_project = $2 AND
          pt.id = $3`;
    let params = [userId, projectId, taskId];

    client.query(query,params,(err,res)=>{
      if(err) return reject(err);
      return resolve(res.rows[0]);
    })
  })
}
//get users to notify of specific task (project leaders and all task workers)
module.exports.getServisNotifyUsers = function(servisId, userId){
  return new Promise((resolve,reject)=>{
    let query = `SELECT pt.id, w.workers, w.workers_id as task_workers, w2.workers, w2.workers_id as project_workers
    FROM project_task as pt
    LEFT JOIN ( SELECT id_task, string_agg(id_user::text, ',') as workers_id, string_agg(concat(name::text, concat(' ', surname::text)), ', ') as workers
                FROM working_task, "user"
                WHERE id_user = "user".id AND
                      id_user != $1
                GROUP BY id_task ) AS w ON pt.id = w.id_task
    LEFT JOIN (SELECT id_task, string_agg("user".id::text, ',') as workers_id, string_agg(concat(name::text, concat(' ', surname::text)), ', ') as workers
                FROM (SELECT DISTINCT id_task, "from" workers_id
                FROM changes, "user"
                WHERE "from" = "user".id AND
                      "from" != $1 AND
                      id_task = $2)as foo, "user"
                WHERE "user".id = foo.workers_id
                GROUP BY id_task) AS w2 ON pt.id = w2.id_task
    WHERE pt.id = $2`;
    let params = [userId, servisId];

    client.query(query,params,(err,res)=>{
      if(err) return reject(err)
      return resolve(res.rows[0]);
    })
  })
}
//get users to notify of specific project (all project workers except for logged user)
module.exports.getProjectNotifyUsers = function(projectId, userId){
  return new Promise((resolve,reject)=>{
    let query = `SELECT DISTINCT ON (id_user) wp.id, id_user, name as user_name, surname as user_surname
    FROM working_project as wp, "user" as u
    WHERE id_user = u.id AND
          id_project = $2 AND
          id_user != $1`;
    let params = [userId, projectId];

    client.query(query,params,(err,res)=>{
      if(err) return reject(err);
      return resolve(res.rows);
    })
  })
}
//get project leaders (problem because there can be more roles for some user and upper function is not enough)
module.exports.getProjectNotifyLeaders = function(projectId, userId){
  return new Promise((resolve,reject)=>{
    let query = `SELECT DISTINCT ON (id_user) wp.id, id_user, name as user_name, surname as user_surname
    FROM working_project as wp, "user" as u, work_role as wr
    WHERE id_user = u.id AND
          wp."id_workingRole" = wr.id AND
          (wr."workRole" = 'vodja' OR wr."workRole" = 'vodja projekta' OR wr."workRole" = 'vodja strojnikov' OR wr."workRole" = 'vodja CNC obdelave' OR wr."workRole" = 'vodja električarjev' OR wr."workRole" = 'vodja programerjev') AND
          id_project = $2 AND
          id_user != $1`;
    let params = [userId, projectId];

    client.query(query,params,(err,res)=>{
      if(err) return reject(err);
      return resolve(res.rows);
    })
  })
}
//add new change (modular)
module.exports.addNewChange = function(type,status,forUser,fromUser,projectId,taskId,subtaskId,noteId,subscriberId,absenceId,fileId){
  return new Promise((resolve,reject)=>{
    let params = [type,status,forUser,fromUser];
    let i = 5;
    let projectSet = '';
    let projectQuery = '';
    if(projectId){
      projectQuery = ',id_project';
      projectSet = ',$'+i;
      params.push(projectId);
      i++;
    }
    let taskSet='';
    let taskQuery='';
    if(taskId){
      taskQuery = ',id_task';
      taskSet = ',$'+i;
      params.push(taskId);
      i++;
    }
    let subtaskSet='';
    let subtaskQuery='';
    if(subtaskId){
      subtaskQuery = ',id_subtask';
      subtaskSet = ',$'+i;
      params.push(subtaskId);
      i++;
    }
    let noteSet='';
    let noteQuery='';
    if(noteId){
      noteQuery = ',id_note';
      noteSet = ',$'+i;
      params.push(noteId);
      i++;
    }
    let fileSet='';
    let fileQuery='';
    if(fileId){
      fileQuery = ',id_file';
      fileSet = ',$'+i;
      params.push(fileId);
      i++;
    }
    let subscriberSet='';
    let subscriberQuery='';
    if(subscriberId){
      subscriberQuery = ',id_subscriber';
      subscriberSet = ',$'+i;
      params.push(subscriberId);
      i++;
    }
    let query = `INSERT INTO changes(type,status,"for","from"`+projectQuery+taskQuery+subtaskQuery+noteQuery+fileQuery+subscriberQuery+`) 
    VALUES ($1,$2,$3,$4`+projectSet+taskSet+subtaskSet+noteSet+fileSet+subscriberSet+`)
    RETURNING id`;

    client.query(query,params,(err,res)=>{
      if(err) return reject(err);
      return resolve(res.rows);
    })
  })
}
//add new change (modular)
module.exports.addNewSystemChange = function(type,status,fromUser,projectId,taskId,subtaskId,noteId,subscriberId,absenceId,fileId,workOrderId,reportId,buildPartId,buildPhaseId){
  return new Promise((resolve,reject)=>{
    let params = [type,status,fromUser];
    let i = 4;
    let projectSet = '';
    let projectQuery = '';
    if(projectId){
      projectQuery = ',id_project';
      projectSet = ',$'+i;
      params.push(projectId);
      i++;
    }
    let taskSet='';
    let taskQuery='';
    if(taskId){
      taskQuery = ',id_task';
      taskSet = ',$'+i;
      params.push(taskId);
      i++;
    }
    let subtaskSet='';
    let subtaskQuery='';
    if(subtaskId){
      subtaskQuery = ',id_subtask';
      subtaskSet = ',$'+i;
      params.push(subtaskId);
      i++;
    }
    let noteSet='';
    let noteQuery='';
    if(noteId){
      noteQuery = ',id_note';
      noteSet = ',$'+i;
      params.push(noteId);
      i++;
    }
    let fileSet='';
    let fileQuery='';
    if(fileId){
      fileQuery = ',id_file';
      fileSet = ',$'+i;
      params.push(fileId);
      i++;
    }
    let subscriberSet='';
    let subscriberQuery='';
    if(subscriberId){
      subscriberQuery = ',id_subscriber';
      subscriberSet = ',$'+i;
      params.push(subscriberId);
      i++;
    }
    let workOrderSet='';
    let workOrderQuery='';
    if(workOrderId){
      workOrderQuery = ',id_work_order';
      workOrderSet = ',$'+i;
      params.push(workOrderId);
      i++;
    }
    let query = `INSERT INTO changes(type,status,"from"`+projectQuery+taskQuery+subtaskQuery+noteQuery+fileQuery+subscriberQuery+workOrderQuery+`) 
    VALUES ($1,$2,$3`+projectSet+taskSet+subtaskSet+noteSet+fileSet+subscriberSet+workOrderSet+`)
    RETURNING id`;

    client.query(query,params,(err,res)=>{
      if(err) return reject(err);
      return resolve(res.rows);
    })
  })
}
//add new change for table change_system
module.exports.addNewChangeSystem = function(status,type,fromUser,projectId,taskId,subtaskId,workingProjectId,workingTaskId,noteId,subscriberId,absenceId,fileId,workOrderId,buildPartId,buildPhaseId,projectDateId,reportId,userId,workOrderFileId,roleId,workRoleId,workOrderSignId,meetingId,activityId,modificationId,modStatusId,compSubId,compSupId,compFileId,compStatusId,stdCompId,eventId,notes,carId,carReservationId,carNoteId){
  //id_user, id_work_order_file, id_role, id_work_role
  return new Promise((resolve,reject)=>{
    let params = [status,type,fromUser];
    let i = 4;
    let projectSet='';
    let projectQuery='';
    if(projectId){
      projectQuery = ',id_project';
      projectSet = ',$'+i;
      params.push(projectId);
      i++;
    }
    let taskSet='';
    let taskQuery='';
    if(taskId){
      taskQuery = ',id_task';
      taskSet = ',$'+i;
      params.push(taskId);
      i++;
    }
    let subtaskSet='';
    let subtaskQuery='';
    if(subtaskId){
      subtaskQuery = ',id_subtask';
      subtaskSet = ',$'+i;
      params.push(subtaskId);
      i++;
    }
    let workingProjectSet='';
    let workingProjectQuery='';
    if(workingProjectId){
      workingProjectQuery = ',id_working_project';
      workingProjectSet = ',$'+i;
      params.push(workingProjectId);
      i++;
    }
    let workingTaskSet='';
    let workingTaskQuery='';
    if(workingTaskId){
      workingTaskQuery = ',id_working_task';
      workingTaskSet = ',$'+i;
      params.push(workingTaskId);
      i++;
    }
    let noteSet='';
    let noteQuery='';
    if(noteId){
      noteQuery = ',id_note';
      noteSet = ',$'+i;
      params.push(noteId);
      i++;
    }
    let subscriberSet='';
    let subscriberQuery='';
    if(subscriberId){
      subscriberQuery = ',id_subscriber';
      subscriberSet = ',$'+i;
      params.push(subscriberId);
      i++;
    }
    let absenceSet='';
    let absenceQuery='';
    if(absenceId){
      absenceQuery = ',id_absence';
      absenceSet = ',$'+i;
      params.push(absenceId);
      i++;
    }
    let fileSet='';
    let fileQuery='';
    if(fileId){
      fileQuery = ',id_file';
      fileSet = ',$'+i;
      params.push(fileId);
      i++;
    }
    let workOrderSet='';
    let workOrderQuery='';
    if(workOrderId){
      workOrderQuery = ',id_work_order';
      workOrderSet = ',$'+i;
      params.push(workOrderId);
      i++;
    }
    let buildPartSet='';
    let buildPartQuery='';
    if(buildPartId){
      buildPartQuery = ',id_build_part';
      buildPartSet = ',$'+i;
      params.push(buildPartId);
      i++;
    }
    let buildPhaseSet='';
    let buildPhaseQuery='';
    if(buildPhaseId){
      buildPhaseQuery = ',id_build_phase';
      buildPhaseSet = ',$'+i;
      params.push(buildPhaseId);
      i++;
    }
    let projectDateSet='';
    let projectDateQuery='';
    if(projectDateId){
      projectDateQuery = ',id_project_date';
      projectDateSet = ',$'+i;
      params.push(projectDateId);
      i++;
    }
    let reportSet='';
    let reportQuery='';
    if(reportId){
      reportQuery = ',id_report';
      reportSet = ',$'+i;
      params.push(reportId);
      i++;
    }
    let userSet='';
    let userQuery='';
    if(userId){
      userQuery = ',id_user';
      userSet = ',$'+i;
      params.push(userId);
      i++;
    }
    let workOrderFileSet='';
    let workOrderFileQuery='';
    if(workOrderFileId){
      workOrderFileQuery = ',id_work_order_file';
      workOrderFileSet = ',$'+i;
      params.push(workOrderFileId);
      i++;
    }
    let roleSet='';
    let roleQuery='';
    if(roleId){
      roleQuery = ',id_role';
      roleSet = ',$'+i;
      params.push(roleId);
      i++;
    }
    let workRoleSet='';
    let workRoleQuery='';
    if(workRoleId){
      workRoleQuery = ',id_work_role';
      workRoleSet = ',$'+i;
      params.push(workRoleId);
      i++;
    }
    let workOrderSignSet='';
    let workOrderSignQuery='';
    if(workOrderSignId){
      workOrderSignQuery = ',id_work_order_sign';
      workOrderSignSet = ',$'+i;
      params.push(workOrderSignId);
      i++;
    }
    let meetingSet = '';
    let meetingQuery = '';
    if(meetingId){
      meetingQuery = ',id_meeting';
      meetingSet = ',$'+i;
      params.push(meetingId);
      i++;
    }
    let activitySet = '';
    let activityQuery = '';
    if(activityId){
      activityQuery = ',id_activity';
      activitySet = ',$'+i;
      params.push(activityId);
      i++;
    }
    let modificationSet = '';
    let modificationQuery = '';
    if(modificationId){
      modificationQuery = ',id_modification';
      modificationSet = ',$'+i;
      params.push(modificationId);
      i++;
    }
    let modStatusSet = '';
    let modStatusQuery = '';
    if(modStatusId){
      modStatusQuery = ',id_mod_status';
      modStatusSet = ',$'+i;
      params.push(modStatusId);
      i++;
    }
    let compSubSet = '';
    let compSubQuery = '';
    if(compSubId){
      compSubQuery = ',id_comp_sub';
      compSubSet = ',$'+i;
      params.push(compSubId);
      i++;
    }
    let compSupSet = '';
    let compSupQuery = '';
    if(compSupId){
      compSupQuery = ',id_comp_sup';
      compSupSet = ',$'+i;
      params.push(compSupId);
      i++;
    }
    let compFileSet = '';
    let compFileQuery = '';
    if(compFileId){
      compFileQuery = ',id_comp_file';
      compFileSet = ',$'+i;
      params.push(compFileId);
      i++;
    }
    let compStatusSet = '';
    let compStatusQuery = '';
    if(compStatusId){
      compStatusQuery = ',id_comp_status';
      compStatusSet = ',$'+i;
      params.push(compStatusId);
      i++;
    }
    let stdCompSet = '';
    let stdCompQuery = '';
    if(stdCompId){
      stdCompQuery = ',id_std_comp';
      stdCompSet = ',$'+i;
      params.push(stdCompId);
      i++;
    }
    let eventSet = '';
    let eventQuery = '';
    if(eventId){
      eventQuery = ',id_event';
      eventSet = ',$'+i;
      params.push(eventId);
      i++;
    }
    let notesSet = '';
    let notesQuery = '';
    if(notes){
      notesQuery = ',notes';
      notesSet = ',$'+i;
      params.push(notes);
      i++;
    }
    let carSet = '';
    let carQuery = '';
    if(carId){
      carQuery = ',id_car';
      carSet = ',$'+i;
      params.push(carId);
      i++;
    }
    let carReservationSet = '';
    let carReservationQuery = '';
    if(carReservationId){
      carReservationQuery = ',id_car_reservation';
      carReservationSet = ',$'+i;
      params.push(carReservationId);
      i++;
    }
    let carNoteSet = '';
    let carNoteQuery = '';
    if(carNoteId){
      carNoteQuery = ',id_car_note';
      carNoteSet = ',$'+i;
      params.push(carNoteId);
      i++;
    }
    let query = `INSERT INTO changes_system(status, type, "from"`+projectQuery+taskQuery+subtaskQuery+workingProjectQuery+workingTaskQuery+noteQuery+subscriberQuery+absenceQuery+fileQuery+workOrderQuery+buildPartQuery+buildPhaseQuery+projectDateQuery+reportQuery+userQuery+workOrderFileQuery+roleQuery+workRoleQuery+workOrderSignQuery+meetingQuery+activityQuery+modificationQuery+modStatusQuery+compSubQuery+compSupQuery+compFileQuery+compStatusQuery+stdCompQuery+eventQuery+notesQuery+carQuery+carReservationQuery+carNoteQuery+`)
    VALUES ($1,$2,$3`+projectSet+taskSet+subtaskSet+workingProjectSet+workingTaskSet+noteSet+subscriberSet+absenceSet+fileSet+workOrderSet+buildPartSet+buildPhaseSet+projectDateSet+reportSet+userSet+workOrderFileSet+roleSet+workRoleSet+workOrderSignSet+meetingSet+activitySet+modificationSet+modStatusSet+compSubSet+compSupSet+compFileSet+compStatusSet+stdCompSet+eventSet+notesSet+carSet+carReservationSet+carNoteSet+`)
    RETURNING id`;

    client.query(query,params,(err,res)=>{
      if(err) return reject(err);
      return resolve(res.rows[0]);
    })
  })
}
//add new change (modular)
module.exports.addNewChangeNotify = function(changeId,userId){
  return new Promise((resolve,reject)=>{
    let params = [changeId,userId];
    let query = `INSERT INTO changes_notify(id_change, "for")
    VALUES ($1,$2)
    RETURNING id`;

    client.query(query,params,(err,res)=>{
      if(err) return reject(err);
      return resolve(res.rows);
    })
  })
}
//get all change types (id,text)
module.exports.getChangeTypes = function(){
  return new Promise((resolve,reject)=>{
    let query = `SELECT id, type as text
    FROM change_type
    ORDER BY id`;

    client.query(query,(err,res)=>{
      if(err) return reject(err);
      return resolve(res.rows);
    })
  })
}
//get all change status (id,text)
module.exports.getChangeStatus = function(){
  return new Promise((resolve,reject)=>{
    let query = `SELECT id, status as text
    FROM change_status
    ORDER BY id`;

    client.query(query,(err,res)=>{
      if(err) return reject(err);
      return resolve(res.rows);
    })
  })
}