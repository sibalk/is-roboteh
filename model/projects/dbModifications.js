const { Client } = require('pg');

//console.log(process.env.DB_HOST);

const client = new Client({
  user: process.env.DB_USERNAME,
  host: process.env.DB_HOST,
  database: process.env.DB_DATABASE,
  password: process.env.DB_PASSWORD,
  port: process.env.DB_PORT
})
client.connect();

//get mods statuses
module.exports.getModStatus = function(){
  return new Promise((resolve,reject)=>{
    let query = `SELECT *
    FROM project_modification_status`;

    client.query(query,(err,res)=>{
      if(err) return reject(err);
      return resolve(res.rows);
    })
  })
}
//Get project modifications
module.exports.getProjectModifications = function(projectId){
  return new Promise((resolve,reject)=>{
    let query = `SELECT pm.id, mod_status_id, modification, date, project_id, concat(p.project_number, ' - ', p.project_name) as project, category_id, c.name as category, user_id, concat(u.name, ' ', u.surname) as user_name, pm.active, fc.count as files_count
    FROM project_modification as pm
    LEFT JOIN category c on c.id = pm.category_id
    LEFT JOIN "user" u on pm.user_id = u.id
    LEFT JOIN project p on pm.project_id = p.id
    LEFT JOIN ( SELECT id_project_mod, count(id_project_mod)
                FROM files
                WHERE deleted = false
                GROUP BY id_project_mod) AS fc ON pm.id = fc.id_project_mod
    WHERE project_id = $1
    ORDER BY id`;
    let params = [projectId];

    client.query(query,params,(err,res)=>{
      if(err) return reject(err);
      return resolve(res.rows);
    })
  })
};
//Add new modification
module.exports.addModification = function(modName, modStatusId, modCategoryId, userId, projectId){
  return new Promise((resolve,reject)=>{
    let params = [modName, modCategoryId, modStatusId, userId, projectId];
    let query = `INSERT INTO project_modification(modification, category_id, mod_status_id, user_id, project_id)
    VALUES ($1, $2, $3, $4, $5)
    RETURNING *`;
    
    client.query(query,params,(err,res)=>{
      if(err) return reject(err);
      return resolve(res.rows[0]);
    })
  })
}
//update modification status
module.exports.updateModStatus = function(modId, statusId){
  return new Promise((resolve,reject)=>{
    let query = `UPDATE project_modification
    SET mod_status_id = $2
    WHERE id = $1
    RETURNING *`;
    let params = [modId, statusId];
    
    client.query(query,params,(err,res)=>{
      if(err) return reject(err);
      return resolve(res.rows[0]);
    })
  })
}
//update modification
module.exports.updateModification = function(modId, modName, categoryId){
  return new Promise((resolve,reject)=>{
    let query = `UPDATE project_modification
    SET (modification, category_id) = ($2, $3)
    WHERE id = $1
    RETURNING *`;
    let params = [modId, modName, categoryId];
    
    client.query(query,params,(err,res)=>{
      if(err) return reject(err);
      return resolve(res.rows[0]);
    })
  })
}
//delete modification (update active attribute of modification)
module.exports.deleteModification = function(modId, active){
  return new Promise((resolve,reject)=>{
    let query = `UPDATE project_modification
    SET active = $2
    WHERE id = $1
    RETURNING *`;
    let params = [modId, active];
    
    client.query(query,params,(err,res)=>{
      if(err) return reject(err);
      return resolve(res.rows[0]);
    })
  })
}
//get modifications changes system
module.exports.getModsChangesSystem = function(modificationId){
  return new Promise((resolve,reject)=>{
    let query = `SELECT c.id, c.id_project, c.date, "from", u.name, surname, cs.id as id_status, cs.status, ct.id as id_type, ct.type, pm.modification, id_mod_status, pms.status as mod_status
    FROM changes_system as c
    LEFT JOIN "user" u on c."from" = u.id
    LEFT JOIN change_status cs on c.status = cs.id
    LEFT JOIN change_type ct on c.type = ct.id
    LEFT JOIN project_modification pm on c.id_modification = pm.id
    LEFT JOIN project_modification_status pms ON c.id_mod_status = pms.id
    WHERE c.id_modification = $1
    ORDER BY date`;
    let params = [modificationId];
    
    client.query(query,params,(err,res)=>{
      if(err) return reject(err);
      return resolve(res.rows);
    })
  })
}