const { Client } = require('pg');

//console.log(process.env.DB_HOST);

const client = new Client({
  user: process.env.DB_USERNAME,
  host: process.env.DB_HOST,
  database: process.env.DB_DATABASE,
  password: process.env.DB_PASSWORD,
  port: process.env.DB_PORT
})
client.connect();

//Get all projects (for select2 (id, text))
module.exports.getProjectsSelect = function(active){
  return new Promise((resolve,reject)=>{
    let activeProject = '';
    if(active && active == 1)
      activeProject =  ' AND completion < 100';
    let query = `SELECT id, project_name as text
    FROM project
    WHERE active = true`+activeProject+`
    ORDER BY id desc`;

    client.query(query,(err,res)=>{
      if(err) return reject(err);
      return resolve(res.rows);
    })
  })
};
//Get project list of project number conflicts
module.exports.getProjectConflicts = function(projectId, projectNumber){
  return new Promise((resolve,reject)=>{
    let params, query;
    if (projectId){
      params = [projectId, projectNumber];
      query = `SELECT *
      FROM project
      WHERE id != $1 AND project_number = $2`;
    }
    else{
      params = [ projectNumber];
      query = `SELECT *
      FROM project
      WHERE project_number = $1`;
    }
    
    client.query(query,params,(err,res)=>{
      if(err) return reject(err);
      return resolve(res.rows);
    })
  })
};
//Get all projects; list of all projects
module.exports.getProjects = function(active, loggedUser){
  return new Promise((resolve,reject)=>{
    let activity;
    if(active == 1)
        activity = true;
      else
        activity = false;
    let params = [loggedUser];
    let activityQuery = `p.active`;
    if(active){
      activityQuery = `$2`;
      params.push(activity);
    }
    let query = `SELECT p.id, project_number, project_name, name, name as subscriber, s.id as id_subscriber, start, finish, completion, p.active, p.created, my_project
    FROM project p
    LEFT JOIN subscriber s on p.subscriber = s.id
    LEFT JOIN ( SELECT DISTINCT on (id_user, id_project) id, id_user, id_project, true as my_project
                FROM working_project
                WHERE id_user = $1) wp on wp.id_project = p.id
    WHERE p.active = `+activityQuery+`
    ORDER BY p.id DESC`;
    
    client.query(query,params,(err,res)=>{
      if(err) return reject(err);
      return resolve(res.rows);
    })
  })
};
//Get all projects; list of all projects
module.exports.getProjectsFixed = function(active, loggedUser, start, finish, completed){
  return new Promise((resolve,reject)=>{
    let activityQuery = 'p.active';
    if(active == 1 || active == 0){
      active = active == 1 ? true : false;
      activityQuery = active;
    }
    let params = [loggedUser];
    let i = 2;
    if ( start ){
      let startQuery = ` AND p.finish >= $` + i;
      params.push(start);
      i++;
      activityQuery += startQuery;
    }
    if ( finish ){
      let finishQuery = ` AND p.start <= $` + i;
      params.push(finish);
      i++;
      activityQuery += finishQuery;
    }
    if ( completed ){
      let completedQuery = completed == 100 || completed == true ? ` AND p.completion = 100` : completed == 0 || completed == false ? ` AND p.completion < 100` : ``;
      activityQuery += completedQuery;
    }

    let query = `SELECT p.id, project_number, project_name, name, name as subscriber, s.id as id_subscriber, start, finish, completion, p.active, p.created, my_project
    FROM project p
    LEFT JOIN subscriber s on p.subscriber = s.id
    LEFT JOIN ( SELECT DISTINCT on (id_user, id_project) id, id_user, id_project, true as my_project
                FROM working_project
                WHERE id_user = $1) wp on wp.id_project = p.id
    WHERE p.active = `+activityQuery+`
    ORDER BY p.id DESC`;
    
    client.query(query,params,(err,res)=>{
      if(err) return reject(err);
      return resolve(res.rows);
    })
  })
};
//Get project by Id
module.exports.getProjectById = function(projectId){
  return new Promise((resolve,reject)=>{
    let query = `SELECT p.id as id, project_number, project_name, p.subscriber, created, start, finish, completion, p.active, name, notes, icon
      FROM  project p
      LEFT JOIN subscriber s on p.subscriber = s.id
      WHERE p.id = $1`;
    let params = [projectId];

    client.query(query,params,(err,res)=>{
      if(err) return reject(err);
      return resolve(res.rows[0]);
    })
  })
};
//get tasks for specific project and for specific user
module.exports.getProjectUserTasks = function(projectId, userId){
  return new Promise((resolve,reject)=>{
    let query = `SELECT project_task.id, task_finish <= now() as expired, id_project as project_id, project_number, project_name, task_name, task_duration, task_start, task_finish, project_task.completion, priority, id_user, project_task.active, name as category
    FROM project_task, working_task, project,category
    WHERE id_project = $1 AND
          id_user = $2 AND
          id_project = project.id AND
          project_task.category = category.id AND
          project_task.id = id_task AND
          project_task.active = true`;
    let params = [projectId, userId];

    client.query(query,params,(err,res)=>{
      if(err) return reject(err);
      return resolve(res.rows);
    })
  })
}
//Remove project / update project to not active
module.exports.removeProject = function(projectId){
  return new Promise((resolve,reject)=>{
    let query = `UPDATE project
    SET active = false
    WHERE id = $1
    RETURNING *`;
    let params = [projectId];

    client.query(query,params,(err,res)=>{
      if(err) return reject(err);
      return resolve(res.rows[0]);
    })
  })
}
//Create project
module.exports.createProject = function(projectNumber, projectName, subscriber, start, finish, notes){
  return new Promise((resolve,reject)=>{
    let params = [projectNumber, projectName, subscriber];
    let i = 4;
    let startQuery = ``;
    let startSet = ``;
    if(start){
      startQuery = `, start`;
      startSet = `, $` + i;
      i++;
      params.push(start);
    }
    let finishQuery = ``;
    let finishSet = ``;
    if(finish){
      finishQuery = `, finish`;
      finishSet = `, $` + i;
      i++;
      params.push(finish);
    }
    let noteQuery = ``;
    let noteSet = ``;
    if(notes){
      noteQuery = `, notes`;
      noteSet = `, $` + i;
      i++;
      params.push(notes);
    }
    let query = `INSERT INTO project(project_number, project_name, subscriber`+startQuery+finishQuery+noteQuery+`)
    VALUES ( $1, $2, $3`+startSet+finishSet+noteSet+`)
    RETURNING *`;

    client.query(query,params,(err,res)=>{
      if(err) return reject(err);
      return resolve(res.rows[0]);
    })
  })
}
//Update project
module.exports.updateProject = function(projectId,projectNumber,projectName,subscriber,start,finish,notes,active){
  return new Promise((resolve,reject)=>{
    switch (active) {
      case 'true': active = true; break;
      case 'false': active = false; break;
      default: active = true; break;
    }
    let params = [projectId, projectNumber, projectName, subscriber, active];
    let i = 6;
    let startQuery = ``;
    let startSet = ``;
    if(start){
      startQuery = `, start`;
      startSet = `, $` + i;
      i++;
      params.push(start);
    }
    else{
      startQuery = `, start`;
      startSet = `, $` + i;
      i++;
      start = null;
      params.push(start);
    }
    let finishQuery = ``;
    let finishSet = ``;
    if(finish){
      finishQuery = `, finish`;
      finishSet = `, $` + i;
      i++;
      params.push(finish);
    }
    else{
      finishQuery = `, finish`;
      finishSet = `, $` + i;
      i++;
      finish = null;
      params.push(finish);
    }
    let noteQuery = ``;
    let noteSet = ``;
    if(notes){
      noteQuery = `, notes`;
      noteSet = `, $` + i;
      i++;
      params.push(notes);
    }
    else{
      noteQuery = `, notes`;
      noteSet = `, $` + i;
      i++;
      notes = null;
      params.push(notes);
    }
    let query = `UPDATE project
    SET (project_number, project_name, subscriber, active`+startQuery+finishQuery+noteQuery+`) = ($2,$3,$4,$5`+startSet+finishSet+noteSet+`)
    WHERE id = $1
    RETURNING *`;

    client.query(query,params,(err,res)=>{
      if(err) return reject(err);
      return resolve(res.rows);
    })
  })
}
//Get project's workers
module.exports.getWorkersByProjectId = function(projectId){
  return new Promise((resolve,reject)=>{
    let query = `SELECT working_project.id as id_wp, "user".id, name, surname, "workRole",role.role
    FROM  project, working_project, "user", work_role, role
    WHERE project.id = id_project AND
        "user".id = id_user AND
        "id_workingRole" = work_role.id AND
        "user".role = role.id AND
        project.id = $1
    ORDER BY "workRole"!='vodja projekta', "workRole"!='vodja', "workRole"!='vodja strojnikov', "workRole"!='vodja CNC obdelave', "workRole"!='vodja električarjev', "workRole"!='vodja programerjev', "workRole"!='konstruktor', "workRole"!='konstrukter', "workRole"!='strojnik', "workRole"!='električar', "workRole"!='programer robotov', "workRole"!='programer plc-jev', surname, name`;
    let params = [projectId];

    client.query(query,params,(err,res)=>{
      if(err) return reject(err);
      return resolve(res.rows);
    })
  })
}
//Get workers on project for select2(id,text)
module.exports.getWorkersOnProject = function(projectId){
  return new Promise((resolve,reject)=>{
    let query = `SELECT DISTINCT "user".id, name || ' ' || surname as text
    FROM  project, working_project, "user"
    WHERE project.id = id_project AND
        "user".id = id_user AND
        project.id = $1`;
    let params = [projectId];

    client.query(query,params,(err,res)=>{
      if(err) return reject(err);
      return resolve(res.rows);
    })
  })
}
//Get users by id, name and surname
module.exports.getWorkers = function(){
  return new Promise((resolve,reject)=>{
    let query = `SELECT "user".id, name || ' ' || surname as text, role.role
    FROM "user", role
    WHERE active = true AND
          "user".role = role.id
    ORDER BY surname,name`;

    client.query(query,(err,res)=>{
      if(err) return reject(err);
      return resolve(res.rows);
    })
  })
}
//Add worker to the project
module.exports.addWorker = function(userId, roleId, projectId){
  return new Promise((resolve,reject)=>{
    let query = `INSERT INTO working_project(id_user, "id_workingRole", id_project)
    VALUES ($1,$2,$3)
    RETURNING id`;
    let params = [userId, roleId, projectId];

    client.query(query,params,(err,res)=>{
      if(err) return reject(err);
      return resolve(res.rows[0]);
    })
  })
}
//Remove worker on the project
module.exports.removeWorker = function(id){
  return new Promise((resolve,reject)=>{
    let query = `DELETE FROM working_project
    WHERE id = $1`;
    let params = [id];

    client.query(query,params,(err,res)=>{
      if(err) return reject(err);
      return resolve(res.rows);
    })
  })
}
//Update completion of project
module.exports.updateCompletion = function(projectId, completion){
  return new Promise((resolve,reject)=>{
    let query = `UPDATE project
    SET completion = $2
    WHERE id = $1
    RETURNING *`;
    let params = [projectId, completion];

    client.query(query,params,(err,res)=>{
      if(err) return reject(err);
      return resolve(res.rows);
    })
  })
}
//get all projects info with specific workers for excel file
module.exports.getProjectsForExcel = function(status, numberCheck, subCheck, startCheck, finishCheck, completionCheck, leaderCheck, builderCheck, mechanicCheck, electricanCheck, plcCheck, robotCheck, otherCheck){
  return new Promise((resolve,reject)=>{
    let statusSet = '';
    if(status == 1)
      statusSet = `AND
      completion != 100`;
    else if(status == 2)
      statusSet = `AND
      completion = 100`;
    let numberSet = ``;
    if(numberCheck) numberSet = `, project_number`;
    let subSet = '';
    if(subCheck) subSet = `, name`;
    let startSet = '';
    if(startCheck) startSet = ', start';
    let finishSet = '';
    if(finishCheck) finishSet = ', finish';
    let completionSet = '';
    if(completionCheck) completionSet = ', completion';
    let leaderSet = '';
    let leaderJoint = '';
    if(leaderCheck){
      leaderSet = ', vodja';
      leaderJoint = `LEFT JOIN ( SELECT id_project, string_agg(id_user::text, ',') as vodja_id, string_agg(concat(name::text, concat(' ', surname::text)), ', ') as vodja
      FROM working_project, "user", work_role
      WHERE id_user = "user".id AND
            "id_workingRole" = work_role.id AND
            "workRole" = 'vodja'
      GROUP BY id_project ) as leaders ON p.id = leaders.id_project`;
    }
    let builderSet = '';
    let builderJoint = '';
    if(builderCheck){
      builderSet = ', konstrukter';
      builderJoint = `LEFT JOIN ( SELECT id_project, string_agg(id_user::text, ',') as konstrukter_id, string_agg(concat(name::text, concat(' ', surname::text)), ', ') as konstrukter
      FROM working_project, "user", work_role
      WHERE id_user = "user".id AND
            "id_workingRole" = work_role.id AND
            ("workRole" = 'konstruktor' OR "workRole" = 'konstrukter')
      GROUP BY id_project ) as builders ON p.id = builders.id_project`;
    }
    let mechanicSet = '';
    let mechanicJoint = '';
    if(mechanicCheck){
      mechanicSet = ', strojnik';
      mechanicJoint = `LEFT JOIN ( SELECT id_project, string_agg(id_user::text, ',') as strojnik_id, string_agg(concat(name::text, concat(' ', surname::text)), ', ') as strojnik
      FROM working_project, "user", work_role
      WHERE id_user = "user".id AND
            "id_workingRole" = work_role.id AND
            ("workRole" = 'strojnik')
      GROUP BY id_project ) as mechanic ON p.id = mechanic.id_project`;
    }
    let electricanSet = '';
    let electricanJoint = '';
    if(electricanCheck){
      electricanSet = ', elektricar';
      electricanJoint = `LEFT JOIN ( SELECT id_project, string_agg(id_user::text, ',') as elektricar_id, string_agg(concat(name::text, concat(' ', surname::text)), ', ') as elektricar
      FROM working_project, "user", work_role
      WHERE id_user = "user".id AND
            "id_workingRole" = work_role.id AND
            ("workRole" = 'elektricar')
      GROUP BY id_project ) as electrican ON p.id = electrican.id_project`;
    }
    let plcSet = '';
    let plcJoint = '';
    if(plcCheck){
      plcSet = ', plc';
      plcJoint = `LEFT JOIN ( SELECT id_project, string_agg(id_user::text, ',') as plc_id, string_agg(concat(name::text, concat(' ', surname::text)), ', ') as plc
      FROM working_project, "user", work_role
      WHERE id_user = "user".id AND
            "id_workingRole" = work_role.id AND
            ("workRole" = 'programer plc-jev')
      GROUP BY id_project ) as programmer_plc ON p.id = programmer_plc.id_project`;
    }
    let robotSet = '';
    let robotJoint = '';
    if(robotCheck){
      robotSet = ', robot';
      robotJoint = `LEFT JOIN ( SELECT id_project, string_agg(id_user::text, ',') as robot_id, string_agg(concat(name::text, concat(' ', surname::text)), ', ') as robot
      FROM working_project, "user", work_role
      WHERE id_user = "user".id AND
            "id_workingRole" = work_role.id AND
            ("workRole" = 'programer robotov')
      GROUP BY id_project ) as programmer_robot ON p.id = programmer_robot.id_project`;
    }
    let otherSet = '';
    let otherJoint = '';
    if(otherCheck){
      otherSet = ', nabavnik';
      otherJoint = `LEFT JOIN ( SELECT id_project, string_agg(id_user::text, ',') as nabavnik_id, string_agg(concat(name::text, concat(' ', surname::text)), ', ') as nabavnik
      FROM working_project, "user", work_role
      WHERE id_user = "user".id AND
            "id_workingRole" = work_role.id AND
            ("workRole" = 'nabavnik' OR "workRole" = 'komercialist' OR "workRole" = 'komercialist' OR "workRole" = 'tajnik' OR "workRole" = 'tajnica' OR "workRole" = 'komerciala' OR "workRole" = 'računovodstvo')
      GROUP BY id_project ) as commercialist ON p.id = commercialist.id_project`;
    }
    let query = `SELECT p.id`+numberSet+`, project_name`+subSet+startSet+finishSet+completionSet+leaderSet+builderSet+mechanicSet+electricanSet+plcSet+robotSet+otherSet+`
    FROM project as p
    RIGHT JOIN subscriber ON p.subscriber = subscriber.id
    `+leaderJoint+`
    `+builderJoint+`
    `+mechanicJoint+`
    `+electricanJoint+`
    `+plcJoint+`
    `+robotJoint+`
    `+otherJoint+`
    WHERE p.active = true `+statusSet+`
    ORDER BY id DESC`;
    //let params = [];

    client.query(query,(err,res)=>{
      if(err) return reject(err);
      return resolve(res.rows);
    })
  })
}
//Get project's workers
module.exports.getWorkersByTaskId = function(taskId){
  return new Promise((resolve,reject)=>{
    let query = `SELECT working_task.id as id_wp,"user".id, name, surname, username
    FROM "user", project_task, working_task
    WHERE "user".id = working_task.id_user AND
          working_task.id_task = project_task.id AND
          project_task.id = $1
    ORDER BY surname, name`;
    let params = [taskId];

    client.query(query,params,(err,res)=>{
      if(err) return reject(err);
      return resolve(res.rows);
    })
  })
}
// get project changes
module.exports.getProjectChanges = function(projectId){
  return new Promise((resolve,reject)=>{
    let query = `SELECT c.id, c.id_project, id_subtask, c.date, "from", u.name, surname, cs.id as id_status, cs.status, ct.id as id_type, ct.type
    FROM changes_system as c
    LEFT JOIN "user" u on c."from" = u.id
    LEFT JOIN change_status cs on c.status = cs.id
    LEFT JOIN change_type ct on c.type = ct.id
    WHERE c.id_project = $1 AND
          c.type = 1
    ORDER BY date`;
    let params = [projectId];

    client.query(query,params,(err,res)=>{
      if(err) return reject(err);
      return resolve(res.rows);
    })
  })
}