const { Client } = require('pg');

//console.log(process.env.DB_HOST);

const client = new Client({
  user: process.env.DB_USERNAME,
  host: process.env.DB_HOST,
  database: process.env.DB_DATABASE,
  password: process.env.DB_PASSWORD,
  port: process.env.DB_PORT
})
client.connect();

//get activities
module.exports.getAllActivities = function(activity, status, category){
  return new Promise((resolve,reject)=>{
    let activitiesCategory;
    let categoryAdditionalInfo = '';
    switch(category) {
      case '0': activitiesCategory = 'ma.category_id IS NULL'; break;
      case '1': activitiesCategory = 'ma.category_id = 1'; break;
      case '2': activitiesCategory = 'ma.category_id = 2'; break;
      case '3': activitiesCategory = 'ma.category_id = 3'; break;
      case '4': activitiesCategory = 'ma.category_id = 4'; break;
      case '5': activitiesCategory = 'ma.category_id = 5'; break;
      case '6': activitiesCategory = 'ma.category_id = 6'; break;
      case '7': activitiesCategory = 'ma.category_id = 7'; break;
      default: activitiesCategory = 'ma.category_id IS NULL';
    }
    if(activitiesCategory != 'ma.category_id IS NULL')
      categoryAdditionalInfo = `LEFT JOIN ( SELECT id as id_project, project_number, project_name
                                            FROM project
                                            WHERE active = true) AS p ON ma.project_id = p.id_project
                                LEFT JOIN ( SELECT id as id_category, name as category
                                            FROM category) AS c ON ma.category_id = c.id_category`;
    let activitiesActivity;
    switch(activity) {
      case '0': activitiesActivity = 'ma.active'; break;
      case '1': activitiesActivity = 'true'; break;
      case '2': activitiesActivity = 'false'; break;
      default: activitiesActivity = 'true';
    }
    let activitiesStatus;
    switch(status) {
      case '0': activitiesStatus = 'ma.status_id = ma.status_id'; break;
      case '1': activitiesStatus = 'ma.status_id = 1'; break;
      case '2': activitiesStatus = 'ma.status_id = 2'; break;
      case '3': activitiesStatus = 'ma.status_id != 1 AND ma.status_id != 2'; break;
      default: activitiesStatus = 'ma.status_id = ma.status_id';
    }
    let query = `SELECT *
    FROM managerial_activity as ma
    LEFT JOIN ( SELECT activity_id, string_agg(user_id::text, ',') as responsible_users_id, string_agg(concat(name::text, concat(' ', surname::text)), ', ') as responsible_users
                FROM managerial_liable, "user"
                WHERE user_id = "user".id
                GROUP BY activity_id ) AS w ON ma.id = w.activity_id
    ` + categoryAdditionalInfo + `
    WHERE ` + activitiesCategory + ` AND
          ma.active = ` + activitiesActivity + ` AND
          `+activitiesStatus+`
    ORDER BY id`;
    //let params = [activitiesActivity];

    client.query(query,(err,res)=>{
      if(err) return reject(err);
      return resolve(res.rows);
    })
  })
}
//get statuses
module.exports.getStatus = function(){
  return new Promise((resolve,reject)=>{
    let query = `SELECT *
    FROM managerial_status`;
    //let params = [taskId];

    client.query(query,(err,res)=>{
      if(err) return reject(err);
      return resolve(res.rows);
    })
  })
}
//get all meetings
module.exports.getMeetings = function(activity, category){
  return new Promise((resolve,reject)=>{
    let meetingsCategory;
    let categoryAdditionalInfo = '';
    switch(category) {
      case '0': meetingsCategory = 'mm.category_id IS NULL AND'; break;
      case '1': meetingsCategory = 'mm.category_id = 1 AND'; break;
      case '2': meetingsCategory = 'mm.category_id = 2 AND'; break;
      case '3': meetingsCategory = 'mm.category_id = 3 AND'; break;
      case '4': meetingsCategory = 'mm.category_id = 4 AND'; break;
      case '5': meetingsCategory = 'mm.category_id = 5 AND'; break;
      case '6': meetingsCategory = 'mm.category_id = 6 AND'; break;
      case '7': meetingsCategory = 'mm.category_id = 7 AND'; break;
      default: meetingsCategory = 'mm.category_id IS NULL AND';
    }
    if(meetingsCategory != 'mm.category_id IS NULL AND')
      categoryAdditionalInfo = `LEFT JOIN ( SELECT id as id_category, name as category
                                            FROM category) AS c ON mm.category_id = c.id_category`;
    let meetingsActivity;
    switch(activity) {
      case '0': meetingsActivity = `mm.active`; break;
      case '1': meetingsActivity = 'true'; break;
      case '2': meetingsActivity = 'false'; break;
      default: meetingsActivity = 'true';
    }
    let query = `SELECT id, date, finished, active, category_id, meeting_id, present_users_id, present_users, row_number() over (ORDER BY date) as counter
    FROM managerial_meeting as mm
    LEFT JOIN ( SELECT meeting_id, string_agg(user_id::text, ',') as present_users_id, string_agg(concat(name::text, concat(' ', surname::text)), ', ') as present_users
                FROM managerial_presence, "user"
                WHERE user_id = "user".id
                GROUP BY meeting_id ) AS p ON mm.id = p.meeting_id
    ` + categoryAdditionalInfo + `
    WHERE ` + meetingsCategory + `
          mm.active = ` + meetingsActivity + `
    ORDER BY date`;
    //let params = [meetingsActivity];

    client.query(query,(err,res)=>{
      if(err) return reject(err);
      return resolve(res.rows);
    })
  })
}
//Add new meeting
module.exports.addNewMeeting = function(date, time, category_id){
  return new Promise((resolve,reject)=>{
    let dateTime = date + ' ' + time;
    let category = category_id != '0' ? category_id : null;
    let query = `INSERT INTO managerial_meeting(date, category_id)
    VALUES ($1, $2)
    RETURNING *`;
    let params = [dateTime, category];
    
    client.query(query,params,(err,res)=>{
      if(err) return reject(err);
      return resolve(res.rows[0]);
    })
  })
}
//Add presence to the meeting
module.exports.addPresenceToMeeting = function(userId, meetingId){
  return new Promise((resolve,reject)=>{
    let query = `INSERT INTO managerial_presence(user_id, meeting_id)
    VALUES ($1,$2)
    RETURNING *`;
    let params = [userId, meetingId];
    
    client.query(query,params,(err,res)=>{
      if(err) return reject(err);
      return resolve(res.rows[0]);
    })
  })
}
//Delete presence to the meeting
module.exports.deletePresenceToMeeting = function(userId, meetingId){
  return new Promise((resolve,reject)=>{
    let query = `DELETE FROM managerial_presence
    WHERE user_id = $1 AND meeting_id = $2`;
    let params = [userId, meetingId];
    
    client.query(query,params,(err,res)=>{
      if(err) return reject(err);
      return resolve(res.rows[0]);
    })
  })
}
//update meeting
module.exports.updateMeeting = function(meetingId, date, finished){
  return new Promise((resolve,reject)=>{
    let query = `UPDATE managerial_meeting
    SET (date, finished) = ($2,$3)
    WHERE id = $1
    RETURNING *`;
    if(!finished)
      finished = null;
    let params = [meetingId, date, finished];
    
    client.query(query,params,(err,res)=>{
      if(err) return reject(err);
      return resolve(res.rows[0]);
    })
  })
}
//update meeting (only finished)
module.exports.updateMeetingFinished = function(meetingId){
  return new Promise((resolve,reject)=>{
    let query = `UPDATE managerial_meeting
    SET finished = now()
    WHERE id = $1
    RETURNING *`;
    let params = [meetingId];
    
    client.query(query,params,(err,res)=>{
      if(err) return reject(err);
      return resolve(res.rows[0]);
    })
  })
}
//delete meeting (update active attribute of meeting)
module.exports.deleteMeeting = function(meetingId, active){
  return new Promise((resolve,reject)=>{
    let query = `UPDATE managerial_meeting
    SET active = $2
    WHERE id = $1
    RETURNING *`;
    let params = [meetingId, active];
    
    client.query(query,params,(err,res)=>{
      if(err) return reject(err);
      return resolve(res.rows[0]);
    })
  })
}
/////////////////////////////////////////////////////////////////////////////ACTIVITIES
//delete activity (update active attribute of activity)
module.exports.deleteActivity = function(activityId, active){
  return new Promise((resolve,reject)=>{
    let query = `UPDATE managerial_activity
    SET active = $2
    WHERE id = $1
    RETURNING *`;
    let params = [activityId, active];
    
    client.query(query,params,(err,res)=>{
      if(err) return reject(err);
      return resolve(res.rows[0]);
    })
  })
}
//Add new activity
module.exports.addActivity = function(activity, deadline1, deadline2, deadline3, statusId, note, respAll, category_id, project_id){
  return new Promise((resolve,reject)=>{
    let category = category_id != '0' ? category_id : null;
    let params = [activity, deadline1, statusId, respAll, category, project_id];
    let i = 6;
    let deadline2Query = '';
    let deadline2Set = '';
    if(deadline2){
      deadline2Query = ', deadline2';
      deadline2Set = ', $' + ++i;
      params.push(deadline2);
    }
    let deadline3Query = '';
    let deadline3Set = '';
    if(deadline3){
      deadline3Query = ', deadline3';
      deadline3Set = ', $' + ++i;
      params.push(deadline3);
    }
    let noteQuery = '';
    let noteSet = '';
    if(note){
      noteQuery = ', note';
      noteSet = ', $' + ++i;
      params.push(note);
    }
    let finishedQuery = '';
    let finishedSet = '';
    if(statusId == 2){
      finishedQuery = ', finished';
      finishedSet = ', now()';
    }
    let query = `INSERT INTO managerial_activity(date, activity, deadline1, status_id, responsible_all, category_id, project_id` + deadline2Query + deadline3Query + noteQuery + finishedQuery + `)
    VALUES (now(), $1, $2, $3, $4, $5, $6` + deadline2Set + deadline3Set + noteSet + finishedSet + `)
    RETURNING *`;
    
    client.query(query,params,(err,res)=>{
      if(err) return reject(err);
      return resolve(res.rows[0]);
    })
  })
}
//update activity
module.exports.updateActivity = function(activityId, date, name, deadline1, deadline2, deadline3, note, respAll, project_id){
  return new Promise((resolve,reject)=>{
    let query = `UPDATE managerial_activity
    SET (date, activity, deadline1, deadline2, deadline3, note, responsible_all, project_id) = ($2, $3, $4, $5, $6, $7, $8, $9)
    WHERE id = $1
    RETURNING *`;
    if(!deadline2)
      deadline2 = null;
    if(!deadline3)
      deadline3 = null;
    if(!note)
      note = null;
    let params = [activityId, date, name, deadline1, deadline2, deadline3, note, respAll, project_id];
    
    client.query(query,params,(err,res)=>{
      if(err) return reject(err);
      return resolve(res.rows[0]);
    })
  })
}
//update activity status
module.exports.updateActivityStatus = function(activityId, status){
  return new Promise((resolve,reject)=>{
    let query = '';
    if(status == '2'){
      query = `UPDATE managerial_activity
      SET (status_id,finished) = ($2, now())
      WHERE id = $1
      RETURNING *`;
    }
    else{
      query = `UPDATE managerial_activity
      SET status_id = $2
      WHERE id = $1
      RETURNING *`;
    }
    let params = [activityId, status];
    
    client.query(query,params,(err,res)=>{
      if(err) return reject(err);
      return resolve(res.rows[0]);
    })
  })
}
//Add responsible to the activity
module.exports.addResponsibleToActivity = function(userId, activityId){
  return new Promise((resolve,reject)=>{
    let query = `INSERT INTO managerial_liable(user_id, activity_id)
    VALUES ($1,$2)
    RETURNING *`;
    let params = [userId, activityId];
    
    client.query(query,params,(err,res)=>{
      if(err) return reject(err);
      return resolve(res.rows[0]);
    })
  })
}
//Delete responsible to the activity
module.exports.deleteResponsibleToActivity = function(userId, activityId){
  return new Promise((resolve,reject)=>{
    let query = `DELETE FROM managerial_liable
    WHERE user_id = $1 AND activity_id = $2`;
    let params = [userId, activityId];
    
    client.query(query,params,(err,res)=>{
      if(err) return reject(err);
      return resolve(res.rows[0]);
    })
  })
}