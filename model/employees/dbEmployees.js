const { Client } = require('pg');

//console.log(process.env.DB_HOST);

const client = new Client({
  user: process.env.DB_USERNAME,
  host: process.env.DB_HOST,
  database: process.env.DB_DATABASE,
  password: process.env.DB_PASSWORD,
  port: process.env.DB_PORT
})
client.connect();

//Get user by username (userId)
module.exports.getUserById = function(userId, loggedUserId){
  return new Promise((resolve, reject)=>{
    let query = `SELECT u.id, u.name, u.surname, r.role, u.role AS role_id, u.username, u.work_mail, u.active, STRING_AGG(s.name || ' ' || s.surname, ', ' ORDER BY s.name, s.surname) AS supervisors, STRING_AGG(CAST(s.id AS TEXT), ',' ORDER BY s.name, s.surname) AS supervisors_ids, BOOL_OR(CASE WHEN us.id_supervisor = $2 THEN TRUE ELSE FALSE END) AS is_supervisor
      FROM public.user u
      LEFT JOIN role r ON u.role = r.id
      LEFT JOIN user_supervision us ON u.id = us.id_user
      LEFT JOIN public.user s ON us.id_supervisor = s.id
      WHERE u.id = $1
      GROUP BY u.id, u.name, u.surname, r.role, u.role, u.username, u.work_mail`;
    let params = [userId, loggedUserId];

    client.query(query,params,(err,res)=>{
      if(err) return reject(err);
      return resolve(res.rows[0]);
    })
  })
}
//Is user account still active
module.exports.isActive = function(username){
  return new Promise((resolve,reject)=>{
    let query = `SELECT active
    FROM "user"
    WHERE username = $1`;
    let params = [username];

    client.query(query,params,(err,res)=>{
      if(err) return reject(err);
      return resolve(res.rows[0]);
    })
  })
}
//Get all users; list of all users
module.exports.getUsers = function(active){
  return new Promise((resolve,reject)=>{
    let query;
    let params;
    if(active){
      query = `SELECT "user".id, name, surname, role.role, username, active, color, work_mail
      FROM "user", role
      WHERE "user".role = role.id AND active = $1
      ORDER BY role.role='konstrukter' desc, role.role='strojnik' desc, role.role='električar' desc, role.role='programer plc-jev' desc, role.role='programer robotov' desc, role.role='vodja' desc, role.role='vodja projektov' desc, role.role='vodja strojnikov' desc, role.role='vodja CNC obdelave' desc, role.role='vodja električarjev' desc, role.role='vodja programerjev' desc, role.role='tajnik' desc, role.role='komercialist' desc, role.role='računovodstvo' desc, role.role ='komercialist' desc, role.role='admin' desc, role.role='info' desc, role.role='študent' desc, surname, name`;
      if(active == 1)
          active = true;
        else
          active = false;
      params = [active];
    }
    else{
      query = `SELECT "user".id, name, surname, role.role, username, active, color, work_mail
      FROM "user", role
      WHERE "user".role = role.id AND
      active = active
      ORDER BY role.role='konstrukter' desc, role.role='strojnik' desc, role.role='električar' desc, role.role='programer plc-jev' desc, role.role='programer robotov' desc, role.role='vodja' desc, role.role='vodja projektov' desc, role.role='vodja strojnikov' desc, role.role='vodja CNC obdelave' desc, role.role='vodja električarjev' desc, role.role='vodja programerjev' desc, role.role='tajnik' desc, role.role='komercialist' desc, role.role='računovodstvo' desc, role.role ='komercialist' desc, role.role='admin' desc, role.role='info' desc, role.role='študent' desc, surname, name`;
      params = [];
    }

    client.query(query,params,(err,res)=>{
      if(err) return reject(err);
      return resolve(res.rows);
    })
  })
}
//Get all users; list of all users
module.exports.getUsersWithSupervisors = function(loggedUserId, active){
  return new Promise((resolve,reject)=>{
    let query;
    let params = [loggedUserId];
    if(active){
      query = `SELECT u.id, u.name, u.surname, r.role, u.role AS role_id, u.username, u.work_mail, u.active, STRING_AGG(s.name || ' ' || s.surname, ', ' ORDER BY s.name, s.surname) AS supervisors, STRING_AGG(CAST(s.id AS TEXT), ',' ORDER BY s.name, s.surname) AS supervisors_ids, BOOL_OR(CASE WHEN us.id_supervisor = $1 THEN TRUE ELSE FALSE END) AS is_supervisor
        FROM public.user u
        LEFT JOIN role r ON u.role = r.id
        LEFT JOIN user_supervision us ON u.id = us.id_user
        LEFT JOIN public.user s ON us.id_supervisor = s.id
        WHERE u.active = $2
        GROUP BY u.id, u.name, u.surname, r.role, u.role, u.username, u.work_mail
        ORDER BY u.surname, u.name`;
      if(active == 1)
          active = true;
        else
          active = false;
      params.push(active);
    }
    else{
      query = `SELECT u.id, u.name, u.surname, r.role, u.role AS role_id, u.username, u.work_mail, u.active, STRING_AGG(s.name || ' ' || s.surname, ', ' ORDER BY s.name, s.surname) AS supervisors, STRING_AGG(CAST(s.id AS TEXT), ',' ORDER BY s.name, s.surname) AS supervisors_ids, BOOL_OR(CASE WHEN us.id_supervisor = $1 THEN TRUE ELSE FALSE END) AS is_supervisor
        FROM public.user u
        LEFT JOIN role r ON u.role = r.id
        LEFT JOIN user_supervision us ON u.id = us.id_user
        LEFT JOIN public.user s ON us.id_supervisor = s.id
        GROUP BY u.id, u.name, u.surname, r.role, u.role, u.username, u.work_mail
        ORDER BY u.surname, u.name`;
    }

    client.query(query,params,(err,res)=>{
      if(err) return reject(err);
      return resolve(res.rows);
    })
  })
}
// get all roles with their supervisors
module.exports.getRolesWithSupervisors = function(loggedUserId){
  return new Promise((resolve,reject)=>{
    let query = `SELECT r.id, r.role, STRING_AGG(u.name || ' ' || u.surname, ', ' ORDER BY u.name, u.surname) AS supervisors, STRING_AGG(CAST(u.id AS TEXT), ', ' ORDER BY u.name, u.surname) AS supervisor_ids, BOOL_OR(CASE WHEN rs.id_user = $1 THEN TRUE ELSE FALSE END) AS is_supervisor
      FROM role r
      LEFT JOIN role_supervision rs ON r.id = rs.id_role
      LEFT JOIN public.user u ON rs.id_user = u.id
      GROUP BY r.id, r.role`;
    params = [loggedUserId];

    client.query(query, params,(err,res)=>{
      if(err) return reject(err);
      return resolve(res.rows);
    })
  })
}
// get role by id with supervisors
module.exports.getRoleById = function(roleId, loggedUserId){
  return new Promise((resolve,reject)=>{
    let query = `SELECT r.id, r.role, STRING_AGG(u.name || ' ' || u.surname, ', ' ORDER BY u.name, u.surname) AS supervisors, STRING_AGG(CAST(u.id AS TEXT), ', ' ORDER BY u.name, u.surname) AS supervisor_ids, BOOL_OR(CASE WHEN rs.id_user = $2 THEN TRUE ELSE FALSE END) AS is_supervisor
      FROM role r
      LEFT JOIN role_supervision rs ON r.id = rs.id_role
      LEFT JOIN public.user u ON rs.id_user = u.id
      WHERE r.id = $1
      GROUP BY r.id, r.role`;
    let params = [roleId, loggedUserId];

    client.query(query, params,(err,res)=>{
      if(err) return reject(err);
      return resolve(res.rows[0]);
    })
  })
}
// add supervisor to role
module.exports.addRoleSupervisor = function(roleId, supervisorId){
  return new Promise((resolve,reject)=>{
    let query = `INSERT INTO role_supervision(id_role, id_user)
      VALUES ($1, $2)`;
    let params = [roleId, supervisorId];

    client.query(query, params,(err,res)=>{
      if(err) return reject(err);
      return resolve(res.rows);
    })
  })
}
// remove supervisor from role
module.exports.removeRoleSupervisor = function(roleId, supervisorId){
  return new Promise((resolve,reject)=>{
    let query = `DELETE FROM role_supervision
      WHERE id_role = $1 AND id_user = $2`;
    let params = [roleId, supervisorId];

    client.query(query, params,(err,res)=>{
      if(err) return reject(err);
      return resolve(res.rows);
    })
  })
}
//Get all user's projects, projects which he work on
module.exports.getUserProjects = function(userId) {
  return new Promise((resolve,reject)=>{
    let query = `SELECT DISTINCT project_number, project_name, subscriber.name, completion, icon, project.id, completion = 100 as finished
    FROM "user", project, working_project, subscriber
    WHERE id_user = "user".id AND
          id_project = project.id AND
          project.subscriber = subscriber.id AND
          "user".id = $1 AND
          project.active = true
    ORDER BY finished, id desc `;
    let params = [userId];

    client.query(query,params,(err,res)=>{
      if(err) return reject(err);
      return resolve(res.rows);
    })
  })
}
//Create user
module.exports.createUser = function(username, name, surname, role, password, workMail){
  return new Promise((resolve,reject)=>{
    let query = `INSERT INTO "user"(username, name, surname, role, password, work_mail)
    VALUES ($1,$2,$3,$4,$5, $6)
    RETURNING id`;
    let params = [username, name, surname, role, password, workMail];

    client.query(query,params,(err,res)=>{
      if(err) return reject(err);
      return resolve(res.rows[0]);
    })
  })
}
//Get all usernames
module.exports.getUsernames = function(){
  return new Promise((resolve,reject)=>{
    let query = `SELECT username
    FROM "user"`;

    client.query(query,(err,res)=>{
      if(err) return reject(err);
      return resolve(res.rows);
    })
  })
}
//remove user / update user to not active
module.exports.removeUser = function(userId){
  return new Promise((resolve,reject)=>{
    let query = `UPDATE "user"
    SET active = false
    WHERE id = $1`;
    let params = [userId];

    client.query(query,params,(err,res)=>{
      if(err) return reject(err);
      return resolve(res.rows);
    })
  })
}
//Update user (name, surname, active)
module.exports.updateUser = function(id, name, surname, active, username, workMail){
  return new Promise((resolve,reject)=>{
    let query = `UPDATE "user"
    SET (name, surname, active, username, work_mail) = ($2, $3, $4, $5, $6)
    WHERE id = $1`;
    let params = [id, name, surname, active, username, workMail];

    client.query(query,params,(err,res)=>{
      if(err) return reject(err);
      return resolve(res.rows);
    })
  })
}
//Update user's password
module.exports.updatePassword = function(id, password){
  return new Promise((resolve,reject)=>{
    let query = `UPDATE "user"
    SET password = $2
    WHERE id = $1`;
    let params = [id, password];

    client.query(query,params,(err,res)=>{
      if(err) return reject(err);
      return resolve(res.rows);
    })
  })
}
//Update user's role
module.exports.updateRole = function(id, role){
  return new Promise((resolve,reject)=>{
    let query = `UPDATE "user"
    SET role = $2
    WHERE id = $1`;
    let params = [id, role];

    client.query(query,params,(err,res)=>{
      if(err) return reject(err);
      return resolve(res.rows);
    })
  })
}
// Add supervisor to user
module.exports.addSupervisor = function(userId, supervisorId){
  return new Promise((resolve,reject)=>{
    let query = `INSERT INTO user_supervision(id_user, id_supervisor)
    VALUES ($1,$2)`;
    let params = [userId, supervisorId];

    client.query(query,params,(err,res)=>{
      if(err) return reject(err);
      return resolve(res.rows);
    })
  })
}
// Remove supervisor from user
module.exports.removeSupervisor = function(userId, supervisorId){
  return new Promise((resolve,reject)=>{
    let query = `DELETE FROM user_supervision
    WHERE id_user = $1 AND id_supervisor = $2`;
    let params = [userId, supervisorId];

    client.query(query,params,(err,res)=>{
      if(err) return reject(err);
      return resolve(res.rows);
    })
  })
}