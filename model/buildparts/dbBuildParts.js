const { Client } = require('pg');

//console.log(process.env.DB_HOST);

const client = new Client({
  user: process.env.DB_USERNAME,
  host: process.env.DB_HOST,
  database: process.env.DB_DATABASE,
  password: process.env.DB_PASSWORD,
  port: process.env.DB_PORT
})
client.connect();

//get all build parts for task id
module.exports.getTaskBuildParts = function(taskId) {
  return new Promise((resolve,reject)=>{
    let query = `SELECT bp.id, position, name, quantity, mark, bm.id as mark_id,bm.material as mark_material, bm.supplier as mark_supplier, bm.screw_quality as mark_screw, bs.standard, bs.id as standard_id, note, tag, active, completion, bpc.count as build_phases_count
    FROM build_parts as bp
    LEFT JOIN build_material bm on bp.material_id = bm.id
    LEFT JOIN build_standard bs on bp.standard_id = bs.id
    LEFT JOIN ( SELECT build_part_id, count(build_part_id)
                FROM build_phase
                WHERE active = true
                GROUP BY build_part_id) AS bpc ON bp.id = bpc.build_part_id
    WHERE task_id = $1
    ORDER BY position desc`;
    let params = [taskId];
    
    client.query(query,params,(err,res)=>{
      if(err) return reject(err);
      return resolve(res.rows);
    })
  })
}
//get all build parts fixed (without connection to build_materials and build_standards)
module.exports.getTaskBuildPartsFixed = function(taskId) {
  return new Promise((resolve,reject)=>{
    let query = `SELECT bp.id, position, name, quantity, material, standard, note, tag, active, completion, bpc.count as build_phases_count, sestav, parent_id, cs.date
    FROM build_parts as bp
    LEFT JOIN changes_system cs on bp.id = cs.id_build_part AND cs.status = 1 AND (cs.type = 22 OR cs.type = 20)
    LEFT JOIN ( SELECT build_part_id, count(build_part_id)
                FROM build_phase
                WHERE active = true
                GROUP BY build_part_id) AS bpc ON bp.id = bpc.build_part_id
    WHERE task_id = $1
    ORDER BY bp.standard desc`;
    let params = [taskId];
    
    client.query(query,params,(err,res)=>{
      if(err) return reject(err);
      return resolve(res.rows);
    })
  })
}
//get build part
module.exports.getBuildPart = function(buildPartId) {
  return new Promise((resolve,reject)=>{
    let query = `SELECT *
    FROM build_parts as bp
    WHERE id = $1`;
    let params = [buildPartId];
    
    client.query(query,params,(err,res)=>{
      if(err) return reject(err);
      return resolve(res.rows[0]);
    })
  })
}
//add new build part
module.exports.addNewBuildPart = function(taskId,name,standard,position,quantity,material,note,tag) {
  return new Promise((resolve,reject)=>{
    let noteVal = null, tagVal = null;
    if(note) noteVal = note;
    if(tag) tagVal = tag;
    let query = `INSERT INTO build_parts(task_id, position, name, quantity, material_id, standard_id, note, tag)
    VALUES ($1,$2,$3,$4,$5,$6,$7,$8)
    RETURNING *`;
    let params = [taskId,position,name,quantity,material,standard,noteVal,tagVal];
    
    client.query(query,params,(err,res)=>{
      if(err) return reject(err);
      return resolve(res.rows[0]);
    })
  })
}
//add new build part with material & standard varchar
module.exports.addNewBuildPartFixed = function(taskId,name,standard,position,quantity,material,note,tag, parent, sestav) {
  return new Promise((resolve,reject)=>{
    let noteVal = null, tagVal = null, parentId = null, sestavVal = false;
    if(note) noteVal = note;
    if(tag) tagVal = tag;
    if(parent){
      parentId = parent;
      sestavVal = false;
    }
    if(sestav == 'true')
      sestavVal = true;
    let query = `INSERT INTO build_parts(task_id, position, name, quantity, material, standard, note, tag, parent_id, sestav)
    VALUES ($1,$2,$3,$4,$5,$6,$7,$8,$9,$10)
    RETURNING *`;
    let params = [taskId,position,name,quantity,material,standard,noteVal,tagVal, parentId, sestavVal];
    
    client.query(query,params,(err,res)=>{
      if(err) return reject(err);
      return resolve(res.rows[0]);
    })
  })
}
//edit build part
module.exports.editBuildPart = function(buildPartId,name,standard,position,quantity,material,note,tag) {
  return new Promise((resolve,reject)=>{
    let noteVal = null, tagVal = null;
    if(note) noteVal = note;
    if(tag) tagVal = tag;
    let query = `UPDATE build_parts
    SET (position,name,quantity,material_id,standard_id,note,tag) = ($2,$3,$4,$5,$6,$7,$8)
    WHERE id = $1
    RETURNING *`;
    let params = [buildPartId,position,name,quantity,material,standard,noteVal,tagVal];
    
    client.query(query,params,(err,res)=>{
      if(err) return reject(err);
      return resolve(res.rows[0]);
    })
  })
}
//edit build part
module.exports.editBuildPartFixed = function(buildPartId,name,standard,position,quantity,material,note,tag) {
  return new Promise((resolve,reject)=>{
    let noteVal = null, tagVal = null;
    if(note) noteVal = note;
    if(tag) tagVal = tag;
    let query = `UPDATE build_parts
    SET (position,name,quantity,material,standard,note,tag) = ($2,$3,$4,$5,$6,$7,$8)
    WHERE id = $1
    RETURNING *`;
    let params = [buildPartId,position,name,quantity,material,standard,noteVal,tagVal];
    
    client.query(query,params,(err,res)=>{
      if(err) return reject(err);
      return resolve(res.rows[0]);
    })
  })
}
//create new standard and return new id
module.exports.createNewStandard = function(name){
  return new Promise((resolve,reject)=>{
    let query = `INSERT INTO build_standard(standard)
    VALUES ($1)
    RETURNING id, standard as text`;
    let params = [name];

    client.query(query,params,(err,res)=>{
      if(err) return reject(err);
      return resolve(res.rows[0]);
    })
  })
}
//create new material and return new id
module.exports.createNewMaterial = function(name){
  return new Promise((resolve,reject)=>{
    let query = `INSERT INTO build_material(mark)
    VALUES ($1)
    RETURNING id, mark as text, description, material, supplier, screw_quality`;
    let params = [name];

    client.query(query,params,(err,res)=>{
      if(err) return reject(err);
      return resolve(res.rows[0]);
    })
  })
}
//Delete buildpart by id
module.exports.deleteBuildPart = function(id,active){
  return new Promise((resolve,reject)=>{
    let params = [id];
    let activeSet = false;
    if(active)
      activeSet = active;
    params.push(activeSet);
    let query = `UPDATE build_parts
    SET active = $2
    WHERE id = $1`;

    client.query(query,params,(err,res)=>{
      if(err) return reject(err);
      return resolve(res.rows);
    })
  })
}
//Update completion of build part
module.exports.updateBuildPartCompletion = function(buildPartId, completion){
  return new Promise((resolve,reject)=>{
    let query = `UPDATE build_parts
    SET completion = $2
    WHERE id = $1`;
    let params = [buildPartId, completion];

    client.query(query,params,(err,res)=>{
      if(err) return reject(err);
      return resolve(res.rows);
    })
  })
}
//get all build part standards
module.exports.getStandards = function() {
  return new Promise((resolve,reject)=>{
    let query = `select id,standard as text
    from build_standard`;

    client.query(query,(err,res)=>{
      if(err) return reject(err);
      return resolve(res.rows);
    })
  })
}
//get all build part materials
module.exports.getMaterials = function() {
  return new Promise((resolve,reject)=>{
    let query = `select id, mark as text, description, material, supplier, screw_quality 
    from build_material`;
    
    client.query(query,(err,res)=>{
      if(err) return reject(err);
      return resolve(res.rows);
    })
  })
}
//get all active project build parts for filtering out parts that are already in production or in order for production
module.exports.getAllActiveProjectBuildParts = function(projectId){
  return new Promise((resolve,reject)=>{
    let query = `SELECT max(bp.id) as id, max(task_id) as task_id, max(position) as position, max(name) as name, max(quantity) as quantity, max(material) as material, standard, max(note) as note, max(tag) as tag, max(bp.completion) as completion
    FROM build_parts bp
    LEFT JOIN project_task pt on bp.task_id = pt.id
    WHERE bp.active = true AND
          pt.active = true AND
          standard LIKE 'RT%' AND
          id_project = $1
    GROUP BY standard
    ORDER BY id`;
    let params = [projectId];

    client.query(query,params,(err,res)=>{
      if(err) return reject(err);
      return resolve(res.rows);
    })
  })
}
//find existing child - before adding new parent build part check if he already exist (sestav = false)
module.exports.findExistingChild = function(standard, taskId) {
  return new Promise((resolve,reject)=>{
    let query = `SELECT *
    FROM build_parts
    WHERE task_id = $2 AND
          active = true AND
          sestav = false AND
          standard = $1`;
    let params = [standard, taskId];

    client.query(query,params,(err,res)=>{
      if(err) return reject(err);
      return resolve(res.rows[0]);
    })
  })
}
//find existing parent - before adding new child build part check if he already exist as parent build part (sestav = true)
module.exports.findExistingParent = function(standard, taskId) {
  return new Promise((resolve,reject)=>{
    let query = `SELECT *
    FROM build_parts
    WHERE task_id = $2 AND
          active = true AND
          sestav = true AND
          standard = $1`;
    let params = [standard, taskId];

    client.query(query,params,(err,res)=>{
      if(err) return reject(err);
      return resolve(res.rows[0]);
    })
  })
}
//promote build part to parent with sestav = true and use his id for parent id 
module.exports.promoteBuildPartToSestav = function(buildPartId) {
  return new Promise((resolve,reject)=>{
    let query = `UPDATE build_parts
    SET (sestav, note) = (true, concat(note, '_SESTAV'))
    WHERE id = $1
    RETURNING *`;
    let params = [buildPartId];

    client.query(query,params,(err,res)=>{
      if(err) return reject(err);
      return resolve(res.rows[0]);
    })
  })
}
//link child sestav to higher parent sestav --> child build part has no parent id which now exist
module.exports.linkChildToParent = function(buildPartId, parentId, name, note) {
  return new Promise((resolve,reject)=>{
    if(name){
      let query = `UPDATE build_parts
      SET (parent_id, name) = ($2, $3)
      WHERE id = $1
      RETURNING *`;
      let params = [buildPartId,parentId, name];
      if (note){
        query = `UPDATE build_parts
        SET (parent_id, name, note) = ($2, $3, concat('` + note + `', '_SESTAV'))
        WHERE id = $1
        RETURNING *`;
        params = [buildPartId, parentId, name];
      }
  
      client.query(query,params,(err,res)=>{
        if(err) return reject(err);
        return resolve(res.rows[0]);
      })
    }
    else{
      let query = `UPDATE build_parts
      SET parent_id = $2
      WHERE id = $1
      RETURNING *`;
      let params = [buildPartId,parentId];
  
      client.query(query,params,(err,res)=>{
        if(err) return reject(err);
        return resolve(res.rows[0]);
      })
    }
  })
}