const { Client } = require('pg');

//console.log(process.env.DB_HOST);

const client = new Client({
  user: process.env.DB_USERNAME,
  host: process.env.DB_HOST,
  database: process.env.DB_DATABASE,
  password: process.env.DB_PASSWORD,
  port: process.env.DB_PORT
})
client.connect();

//get all build phases for build part id
module.exports.getBuildPartPhases = function(buildPartId) {
  return new Promise((resolve,reject)=>{
    let query = `SELECT bp.id, build_part_id, build_phase_type_id, build_phase_type, location_id, location, build_supplier_id, supplier, priority_id, priority, start, finish, finished, completed, active, created, w.workers, w.workers_id
    FROM build_phase as bp
    LEFT JOIN build_phase_type bpt on bp.build_phase_type_id = bpt.id
    LEFT JOIN build_phase_location bpl on bp.location_id = bpl.id
    LEFT JOIN build_phase_supplier bps on bp.build_supplier_id = bps.id
    LEFT JOIN priority p on bp.priority_id = p.id
    LEFT JOIN ( SELECT build_phase_id, string_agg(user_id::text, ',') as workers_id, string_agg(concat(name::text, concat(' ', surname::text)), ', ') as workers
                FROM build_phase_workers
                LEFT JOIN "user" u on user_id = u.id
                WHERE user_id = u.id
                GROUP BY build_phase_id ) AS w ON bp.id = w.build_phase_id
    WHERE build_part_id = $1
    ORDER BY bp.id`;
    let params = [buildPartId];
    
    client.query(query,params,(err,res)=>{
      if(err) return reject(err);
      return resolve(res.rows);
    })
  })
}
//add new build phase
module.exports.addNewBuildPhase = function(buildPartId,type,start,finish,supplier,priority) {
  return new Promise((resolve,reject)=>{
    let query = `INSERT INTO build_phase(build_part_id, build_phase_type_id, build_supplier_id, priority_id, start, finish)
    VALUES ($1,$2,$3,$4,$5,$6)
    RETURNING *`;
    let params = [buildPartId,type,supplier,priority,start,finish];
    
    client.query(query,params,(err,res)=>{
      if(err) return reject(err);
      return resolve(res.rows[0]);
    })
  })
}
//update build phase
module.exports.updateBuildPhase = function(buildPhaseId,type,start,finish,supplier,priority,finished,location) {
  return new Promise((resolve,reject)=>{
    let params = [buildPhaseId,type,supplier,priority,start,finish];
    let locationQuery = '';
    let locationSet = '';
    if(location){
      params.push(finished,location)
      locationQuery = ',$7,$8';
      locationSet = ', finished, location_id';
    }
    let query = `UPDATE build_phase
    SET (build_phase_type_id, build_supplier_id, priority_id, start, finish`+locationSet+`) = ($2,$3,$4,$5,$6`+locationQuery+`)
    WHERE id = $1
    RETURNING *`;
    
    client.query(query,params,(err,res)=>{
      if(err) return reject(err);
      return resolve(res.rows[0]);
    })
  })
}
//add new worker for build phase
module.exports.addWorker = function(buildPhaseId, userId){
  return new Promise((resolve,reject)=>{
    let query = `INSERT INTO build_phase_workers(user_id, build_phase_id)
    VALUES ($1,$2)`;
    let params = [userId, buildPhaseId];

    client.query(query,params,(err,res)=>{
      if(err) return reject(err);
      return resolve(res.rows);
    })
  })
}
//delete workers for build phase
module.exports.deleteWorkers = function(buildPhaseId){
  return new Promise((resolve,reject)=>{
    let query = `DELETE FROM build_phase_workers
    WHERE build_phase_id = $1`;
    let params = [buildPhaseId];

    client.query(query,params,(err,res)=>{
      if(err) return reject(err);
      return resolve(res.rows);
    })
  })
}
//Delete buildpart by id
module.exports.deleteBuildPhase = function(id,active){
  return new Promise((resolve,reject)=>{
    let params = [id];
    let activeSet = false;
    if(active)
      activeSet = active;
    params.push(activeSet);
    let query = `UPDATE build_phase
    SET active = $2
    WHERE id = $1`;

    client.query(query,params,(err,res)=>{
      if(err) return reject(err);
      return resolve(res.rows);
    })
  })
}
//get build phases location
module.exports.getLocations = function() {
  return new Promise((resolve,reject)=>{
    let query = `SELECT id, location as text
    FROM build_phase_location`;
    
    client.query(query,(err,res)=>{
      if(err) return reject(err);
      return resolve(res.rows);
    })
  })
}
//get build phases suppliers
module.exports.getPhaseSuppliers = function() {
  return new Promise((resolve,reject)=>{
    let query = `SELECT id, supplier as text
    FROM build_phase_supplier`;
    
    client.query(query,(err,res)=>{
      if(err) return reject(err);
      return resolve(res.rows);
    })
  })
}
//get build phases types
module.exports.getPhaseTypes = function() {
  return new Promise((resolve,reject)=>{
    let query = `SELECT id, build_phase_type as text
    FROM build_phase_type`;
    
    client.query(query,(err,res)=>{
      if(err) return reject(err);
      return resolve(res.rows);
    })
  })
}
//get build phases workers
module.exports.getPhaseWorkers = function() {
  return new Promise((resolve,reject)=>{
    let query = `SELECT u.id,concat(u.name,' ',u.surname) as text, u.name, u.surname, u.role as role_id, r.role as role
    FROM "user" as u
    LEFT JOIN role r on u.role = r.id
    WHERE (r.role ILIKE 'strojnik' OR r.role ILIKE 'serviser' OR r.role ILIKE 'delavec' OR r.role ILIKE 'študent' OR r.role ILIKE 'cnc' OR r.role ILIKE 'cnc operater' OR r.role ILIKE 'varilec' OR r.role ILIKE 'strojni monter') AND
          active = true`;
    
    client.query(query,(err,res)=>{
      if(err) return reject(err);
      return resolve(res.rows);
    })
  })
}
//create new phase type and return new id
module.exports.createNewPhaseType = function(type){
  return new Promise((resolve,reject)=>{
    let query = `INSERT INTO build_phase_type(build_phase_type)
    VALUES ($1)
    RETURNING id`;
    let params = [type];

    client.query(query,params,(err,res)=>{
      if(err) return reject(err);
      return resolve(res.rows[0]);
    })
  })
}
//create new phase supplier and return new id
module.exports.createNewPhaseSupplier = function(supplier){
  return new Promise((resolve,reject)=>{
    let query = `INSERT INTO build_phase_supplier(supplier)
    VALUES ($1)
    RETURNING id`;
    let params = [supplier];

    client.query(query,params,(err,res)=>{
      if(err) return reject(err);
      return resolve(res.rows[0]);
    })
  })
}
//create new phase supplier and return new id
module.exports.createNewPhaseLocation = function(location){
  return new Promise((resolve,reject)=>{
    let query = `INSERT INTO build_phase_location(location)
    VALUES ($1)
    RETURNING id`;
    let params = [location];

    client.query(query,params,(err,res)=>{
      if(err) return reject(err);
      return resolve(res.rows[0]);
    })
  })
}
//finish buildpart by id
module.exports.updateBuildPhaseLocation = function(id,locationId){
  return new Promise((resolve,reject)=>{
    let params = [id,locationId];
    let query = `UPDATE build_phase
    SET (location_id,finished,completed) = ($2,now(),true)
    WHERE id = $1`;

    client.query(query,params,(err,res)=>{
      if(err) return reject(err);
      return resolve(res.rows);
    })
  })
}
//unfinish buildpart by id
module.exports.unfinishBuildPhase = function(id,locationId){
  return new Promise((resolve,reject)=>{
    let params = [id];
    let query = `UPDATE build_phase
    SET (location_id,completed) = (null,false)
    WHERE id = $1`;

    client.query(query,params,(err,res)=>{
      if(err) return reject(err);
      return resolve(res.rows);
    })
  })
}
//save buildpart location
module.exports.saveBuildPhaseLocation = function(id,locationId){
  return new Promise((resolve,reject)=>{
    let params = [id,locationId];
    let query = `UPDATE build_phase
    SET location_id = $2
    WHERE id = $1`;

    client.query(query,params,(err,res)=>{
      if(err) return reject(err);
      return resolve(res.rows);
    })
  })
}
//create new phase location and return new id
module.exports.createNewPhaseLocation = function(location){
  return new Promise((resolve,reject)=>{
    let query = `INSERT INTO build_phase_location(location)
    VALUES ($1)
    RETURNING id`;
    let params = [location];

    client.query(query,params,(err,res)=>{
      if(err) return reject(err);
      return resolve(res.rows[0]);
    })
  })
}