const { Client } = require('pg');

//console.log(process.env.DB_HOST);

const client = new Client({
  user: process.env.DB_USERNAME,
  host: process.env.DB_HOST,
  database: process.env.DB_DATABASE,
  password: process.env.DB_PASSWORD,
  port: process.env.DB_PORT
})
client.connect();

//get active tasks for next 2 week for given date or today
module.exports.getCalendarTasksFor2Weeks = function(date, category, project, seeAll, count, userId){
  return new Promise((resolve,reject)=>{
    if(!date)
      date = 'now()';
    let params = [date];
    let categoryQuery = `pt.category`;
    let i = 2;
    if(category){
      if(category == 10){
        categoryQuery = '$'+i;
        i++;
        params.push(6)
        categoryQuery += ' OR pt.category = $'+i;
        i++;
        params.push(4);
      }
      else{
        categoryQuery = '$'+i;
        i++;
        params.push(category);
      }
    }
    let projectQuery = '';
    let sort = 'id_project';
    if(project != 0){
      sort = `category!='2', category!='3', category!='4', category!='5', category!='6', category!='7', category!='1'`;
      projectQuery = ' AND pt.id_project = $'+i;
      i++;
      params.push(project);
    }
    let userQuery = '';
    if(userId){
      userQuery = ` AND string_to_array(w.workers_id, ',')::int[] @> array[`+userId+`]`;
    }
    let completionCheck = 'pt.completion != 100 AND';
    if(seeAll)
      completionCheck = '';
    let countQuery = '14';
    countQuery = (count*7)+'';
    let query = `SELECT pt.id, task_name, task_duration, task_start, task_finish, pt.completion, pt.active, pt.weekend, c.name as category, p.priority, finished, w.workers, w.workers_id, sub.name as subscriber, pro.project_name as project, pro.project_number, pt.id_project, id_subscriber, workorders, workorders_id
    FROM project_task as pt
    RIGHT JOIN category c ON pt.category = c.id
    RIGHT JOIN priority p ON pt.priority = p.id
    LEFT JOIN subscriber sub ON pt.id_subscriber = sub.id
    LEFT JOIN project pro ON pt.id_project = pro.id
    LEFT JOIN ( SELECT id_task, string_agg(id_user::text, ',') as workers_id, string_agg(concat(name::text, concat(' ', surname::text)), ', ') as workers
                FROM working_task, "user"
                WHERE id_user = "user".id
                GROUP BY id_task ) AS w ON pt.id = w.id_task
    LEFT JOIN ( SELECT task_id, string_agg(concat("left"(name::text,1), "left"(surname::text,1), '-', wo.id, '/', date_part('year', date)), ', ') as workorders, string_agg(wo.id::text, ',') as workorders_id
                FROM work_order as wo, "user" as u
                WHERE task_id is not null AND
                      wo.user_id = u.id AND
                      wo.active = true
                GROUP BY task_id ) AS wo ON pt.id = wo.task_id
    WHERE pt.active = true AND
          (pro.active = true OR pt.id_subscriber IS NOT NULL) AND
          (pt.category = `+categoryQuery+`) AND
          `+completionCheck+`
          ((task_start::date <= ($1::date - cast(extract(dow from $1::date) as int) + 1) AND
          task_finish::date >= ($1::date - cast(extract(dow from $1::date) as int) + `+countQuery+`)) OR
          (task_finish::date >= ($1::date - cast(extract(dow from $1::date) as int) + 1) AND
          task_finish::date <= ($1::date - cast(extract(dow from $1::date) as int) + `+countQuery+`)) OR
          (task_start::date >= ($1::date - cast(extract(dow from $1::date) as int) + 1) AND
          task_start::date <= ($1::date - cast(extract(dow from $1::date) as int) + `+countQuery+`)))`+projectQuery+userQuery+`
    ORDER BY `+sort+`, active desc, task_start`;

    client.query(query,params,(err,res)=>{
      if(err) return reject(err)
      return resolve(res.rows);
    })
  })
}
module.exports.getCalendarUsers = function(userRole){
  return new Promise((resolve,reject)=>{
    let queryFilter = '';
    if(userRole == 'info elektro'){
      queryFilter = `AND (r.role = 'električar' OR r.role = 'študent')`;
    }
    else if(userRole == 'info strojna'){
      queryFilter = `AND (r.role = 'cnc' OR r.role = 'cnc operater' OR r.role = 'študent')`;
    }
    else if(userRole == 'info montaža'){
      queryFilter = `AND (r.role = 'strojnik' OR r.role = 'študent')`;
    }
    else if(userRole == 'info konstrukcija'){
      queryFilter = `AND (r.role = 'konstrukter' OR r.role = 'konstruktor' OR r.role = 'študent')`;
    }
    else if(userRole == 'info programiranje'){
      queryFilter = `AND (r.role = 'programer plc-jev' OR r.role = 'programer robotov' OR r.role = 'študent')`;
    }
    else if(userRole == 'info plc'){
      queryFilter = `AND (r.role = 'programer plc-jev' OR r.role = 'študent')`;
    }
    else if(userRole == 'info programer robotov'){
      queryFilter = `AND (r.role = 'programer robotov' OR r.role = 'študent')`;
    }
    let query = `SELECT u.id, username, name, surname, r.role, u.role as role_id, active
    FROM "user" as u
    LEFT JOIN role r on u.role = r.id
    WHERE active = true `+queryFilter+`
    ORDER BY r.role !='delavec', r.role != 'konstrukter', r.role != 'konstruktor', r.role != 'strojnik', r.role != 'cnc', r.role != 'cnc operater', r.role != 'električar', r.role != 'programer plc-jev', r.role != 'programer robotov', r.role != 'tajnik', r.role != 'komerciala', r.role != 'računovodstvo', r.role != 'komercialist', r.role != 'vodja', r.role!='vodja projektov', r.role!='vodja strojnikov', r.role!='vodja CNC obdelave', r.role!='vodja električarjev', r.role!='vodja programerjev', r.role != 'admin', r.role != 'info'`;
    
    client.query(query,(err,res)=>{
      if(err) return reject(err);
      return resolve(res.rows);
    })
  })
}
//get active tasks that expired, they are unfinished tasks that can not be put on calendar
module.exports.getExpiredTasks = function(date, category, project, userId){
  return new Promise((resolve, reject)=>{
    date = 'now()';
    let params = [date];
    let categoryQuery = `pt.category`;
    let i = 2;
    if(category){
      if(category == 10){
        categoryQuery = '$'+i;
        i++;
        params.push(6)
        categoryQuery += ' OR pt.category = $'+i;
        i++;
        params.push(4);
      }
      else{
        categoryQuery = '$'+i;
        i++;
        params.push(category);
      }
    }
    
    let projectQuery = '';
    if(project != 0){
      projectQuery = ' AND pt.id_project = $'+i;
      i++;
      params.push(project);
    }
    let userQuery = '';
    if(userId){
      userQuery = ` AND string_to_array(w.workers_id, ',')::int[] @> array[`+userId+`]`;
    }
    let query = `SELECT pt.id, task_name, task_duration, task_start, task_finish, pt.completion, pt.active, c.name as category, p.priority, finished, w.workers, w.workers_id, sub.name as subscriber, pro.project_name as project, pro.project_number
    FROM project_task as pt
    RIGHT JOIN category c ON pt.category = c.id
    RIGHT JOIN priority p ON pt.priority = p.id
    LEFT JOIN subscriber sub ON pt.id_subscriber = sub.id
    LEFT JOIN project pro ON pt.id_project = pro.id
    LEFT JOIN ( SELECT id_task, string_agg(id_user::text, ',') as workers_id, string_agg(concat(name::text, concat(' ', surname::text)), ', ') as workers
                FROM working_task, "user"
                WHERE id_user = "user".id
                GROUP BY id_task ) AS w ON pt.id = w.id_task
    WHERE pt.active = true AND
          pro.active = true AND
          pt.category = `+categoryQuery+` AND
          pt.completion != 100 AND
          task_finish::date < ($1::date - cast(extract(dow from $1::date) as int) + 1)`+projectQuery+userQuery+`
    ORDER BY category!='2', category!='3', category!='4', category!='5', category!='6', category!='7', category!='1', active desc, task_start`;
    
    client.query(query,params,(err,res)=>{
      if(err) return reject(err)
      return resolve(res.rows);
    })
  })
}