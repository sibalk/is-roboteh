const { Client } = require('pg');

//console.log(process.env.DB_HOST);

const client = new Client({
  user: process.env.DB_USERNAME,
  host: process.env.DB_HOST,
  database: process.env.DB_DATABASE,
  password: process.env.DB_PASSWORD,
  port: process.env.DB_PORT
})
client.connect();

//create subscriber with no icon, when creating new project with new subscriber
module.exports.createSubscriberNoIcon = function(name){
  return new Promise((resolve,reject)=>{
    let query = `INSERT INTO subscriber(name)
    VALUES ($1)
    RETURNING id`;
    let params = [name];

    client.query(query,params,(err,res)=>{
      if(err) return reject(err);
      return resolve(res.rows[0]);
    })
  })
}
//create subscriber with icon
module.exports.createSubscriber = function(subName, imgName){
  return new Promise((resolve,reject)=>{
    let params = [subName];
    let imgNameQuery = "";
    let imgNameSet = "";
    if(imgName){
      imgNameQuery = ",icon";
      imgNameSet = ",$2";
      params.push(imgName);
    }
    let query = `INSERT INTO subscriber(name`+imgNameQuery+`)
    VALUES ($1`+imgNameSet+`)
    RETURNING *`;

    client.query(query,params,(err,res)=>{
      if(err) return reject(err);
      return resolve(res.rows[0]);
    })
  })
}
//update subscriber
module.exports.updateSubscriber = function(id, subName, imgName){
  return new Promise((resolve,reject)=>{
    let params = [id,subName];
    let query = '';
    let imgNameQuery = "";
    let imgNameSet = "";
    if(imgName){
      imgNameQuery = ",icon";
      imgNameSet = ",$3";
      params.push(imgName);
      query = `UPDATE subscriber
      SET (name`+imgNameQuery+`) = ($2`+imgNameSet+`)
      WHERE id = $1
      RETURNING *`;
    }
    else{
      query = `UPDATE subscriber
      SET name = $2
      WHERE id = $1
      RETURNING *`;
    }

    client.query(query,params,(err,res)=>{
      if(err) return reject(err);
      return resolve(res.rows[0]);
    })
  })
}
//Get all subscribers (id,text) => for select2
module.exports.getSubscribers = function(){
  return new Promise((resolve,reject)=>{
    let query = `SELECT id, name as text
    FROM subscriber
    ORDER BY name`;

    client.query(query,(err,res)=>{
      if(err) return reject(err);
      return resolve(res.rows);
    })
  })
};
//Get all subscribers (with all info)
module.exports.getAllSubscribers = function(){
  return new Promise((resolve,reject)=>{
    let query = `SELECT *
    FROM subscriber
    ORDER BY name`;

    client.query(query,(err,res)=>{
      if(err) return reject(err);
      return resolve(res.rows);
    })
  })
};