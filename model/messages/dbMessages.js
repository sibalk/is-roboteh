const { Client } = require('pg');

//console.log(process.env.DB_HOST);

const client = new Client({
  user: process.env.DB_USERNAME,
  host: process.env.DB_HOST,
  database: process.env.DB_DATABASE,
  password: process.env.DB_PASSWORD,
  port: process.env.DB_PORT
})
client.connect();

//get all messages depends on projectId, taskId, subtaskId
module.exports.getMessages = function(projectId, taskId, subtaskId){
  return new Promise((resolve,reject)=>{
    let condition = '';
    let params = [];
    if(projectId){
      condition = 'id_project';
      params.push(projectId);
    }
    else if(taskId){
      condition = 'id_task';
      params.push(taskId)
    }
    else if(subtaskId){
      condition = 'id_subtask'
      params.push(subtaskId)
    }
    let query = `SELECT notes.id, note, id_user, id_task, id_subtask, id_project, date, name, role, surname
    FROM notes, "user"
    WHERE `+condition+` = $1 AND
          id_user = "user".id
    ORDER BY date`;

    client.query(query,params,(err,res)=>{
      if(err) return reject(err);
      return resolve(res.rows);
    })
  })
}
//Add new message and get back date
module.exports.addNewMessage = function(note, userId, projectId, taskId, subtaskId){
  return new Promise((resolve,reject)=>{
    let params = [note, userId];
    let querySelect = '';
    let querySet = '';
    if(projectId){
      params.push(projectId);
      querySelect = ', id_project'
      querySet = ', $3';
    }
    else if(taskId){
      params.push(taskId);
      querySelect = ', id_task';
      querySet = ', $3';
    }
    else if(subtaskId){
      params.push(subtaskId);
      querySelect = ', id_subtask';
      querySet = ', $3';
    }
    let query = `INSERT INTO notes(note, id_user`+querySelect+`)
    VALUES ($1, $2`+querySet+`)
    returning id,date,id_user`;

    client.query(query,params,(err,res)=>{
      if(err) return reject(err);
      return resolve(res.rows[0]);
    })
  })
}
//check if there were new messages
module.exports.refreshMessages = function(projectId,taskId,subtaskId,lastMessageId){
  return new Promise((resolve,reject)=>{
    let condition = '';
    let params = [lastMessageId];
    if(projectId){
      condition = 'id_project';
      params.push(projectId);
    }
    else if(taskId){
      condition = 'id_task';
      params.push(taskId)
    }
    else if(subtaskId){
      condition = 'id_subtask'
      params.push(subtaskId)
    }
    let query = `SELECT notes.id, note, id_user, id_task, id_subtask, id_project, date, name, role, surname
    FROM notes, "user"
    WHERE `+condition+` = $2 AND
          id_user = "user".id AND
          notes.id > $1
    ORDER BY date`;

    client.query(query,params,(err,res)=>{
      if(err) return reject(err);
      return resolve(res.rows);
    })
  })
}