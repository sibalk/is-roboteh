const { Client } = require('pg');

//console.log(process.env.DB_HOST);

const client = new Client({
  user: process.env.DB_USERNAME,
  host: process.env.DB_HOST,
  database: process.env.DB_DATABASE,
  password: process.env.DB_PASSWORD,
  port: process.env.DB_PORT
})
client.connect();

//get all user reports
module.exports.getUsersAPI = function() {
  let query = `SELECT id, concat(name, ' ', surname) as name, active
  FROM "user"
  WHERE name NOT ILIKE 'info'
  ORDER BY surname, name`;
  
  return client.query(query)
  .then(res => {return res.rows;})
  .catch(e => {return e;})
}
//get all user reports
module.exports.getUserAPI = function(userId) {
  let query = `SELECT id, concat(name, ' ', surname) as name, active
  FROM "user"
  WHERE name NOT ILIKE 'info' AND
        id = $1
  ORDER BY surname, name`;
  let params = [userId];

  return client.query(query,params)
  .then(res => {return res.rows[0];})
  .catch(e => {return e;})
}