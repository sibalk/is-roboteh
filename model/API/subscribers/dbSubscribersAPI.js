const { Client } = require('pg');

//console.log(process.env.DB_HOST);

const client = new Client({
  user: process.env.DB_USERNAME,
  host: process.env.DB_HOST,
  database: process.env.DB_DATABASE,
  password: process.env.DB_PASSWORD,
  port: process.env.DB_PORT
})
client.connect();

module.exports.getSubscribersAPI = function() {
  let query = `SELECT *
  FROM subscriber`;
  //let params = [];
  
  return client.query(query)
  .then(res => {return res.rows;})
  .catch(e => {return e;})
}
module.exports.getSubscriberAPI = function(subscriberId) {
  let query = `SELECT *
  FROM subscriber
  WHERE id = $1`;
  let params = [subscriberId];
  
  return client.query(query,params)
  .then(res => {return res.rows;})
  .catch(e => {return e;})
}
module.exports.addSubscribersAPI = function(name) {
  let query = `INSERT INTO subscriber (name) VALUES ($1) RETURNING *`;
  let params = [name];
  
  return client.query(query,params)
  .then(res => {return res.rows;})
  .catch(e => {return e;})
}