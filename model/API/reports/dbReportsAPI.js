const { Client } = require('pg');

//console.log(process.env.DB_HOST);

const client = new Client({
  user: process.env.DB_USERNAME,
  host: process.env.DB_HOST,
  database: process.env.DB_DATABASE,
  password: process.env.DB_PASSWORD,
  port: process.env.DB_PORT
})
client.connect();

//get all user reports
module.exports.getUserReportsAPI = function(userId) {
  let query = `SELECT *
  FROM report
  WHERE user_id = $1`;
  let params = [userId];
  
  return client.query(query,params)
  .then(res => {return res.rows;})
  .catch(e => {return e;})
}
//get all user reports
module.exports.getUserReportsForProjectAPI = function(userId, projectId) {
  let query = `SELECT *
  FROM report
  WHERE user_id = $1 AND project_id = $2`;
  let params = [userId, projectId];
  
  return client.query(query,params)
  .then(res => {return res.rows;})
  .catch(e => {return e;})
}
//get report
module.exports.getReportAPI = function(reportId) {
  let query = `SELECT *
  FROM report
  WHERE id = $1`;
  let params = [reportId];
  
  return client.query(query,params)
  .then(res => {return res.rows;})
  .catch(e => {return e;})
}
//add new report
module.exports.addNewReport = function(userId,date,time,start,finish,projectId,report_type,other,description, radio){
  let params = [userId,date,time,start,finish,description,report_type]
  let radioQuery = '';
  if(radio == '0'){
    radioQuery = ', project_id';
    params.push(projectId);
  }
  else{
    radioQuery = ', other';
    params.push(other);
  }
  params.push(true);
  //let projectQuery = ', project_id';
  //if(projectId)
  let query = `INSERT INTO report(user_id, date, time, start, finish, description, report_type`+radioQuery+`, ma)
  VALUES ($1,$2,$3,$4,$5,$6,$7,$8,$9)
  RETURNING *`;
  return client.query(query,params)
  .then(res => {return res.rows[0];})
  .catch(e => {return e;})
}
//edit report
module.exports.editReport = function(reportId,userId,date,time,start,finish,projectId,report_type,other,description, radio){
  let params = [reportId,userId,date,time,start,finish,description,report_type]
  let radioQuery = '';
  if(radio == '0'){
    radioQuery = ', project_id, other';
    params.push(projectId);
    params.push(null);
  }
  else{
    radioQuery = ', other, project_id';
    params.push(other);
    params.push(null);
  }
  let query = `UPDATE report
  SET (user_id, date, time, start, finish, description, report_type`+radioQuery+`) = ($2,$3,$4,$5,$6,$7,$8,$9,$10)
  WHERE id = $1
  RETURNING *`;
  return client.query(query,params)
  .then(res => {return res.rows[0];})
  .catch(e => {return e;})
}
//get report types
module.exports.getReportTypesAPI = function() {
  let query = `SELECT *
  FROM report_type`;
  
  return client.query(query)
  .then(res => {return res.rows;})
  .catch(e => {return e;})
}
//add new report
module.exports.deleteReport = function(reportId){
  let params = [reportId]
  let query = `UPDATE report
  SET active = false
  WHERE id = $1
  RETURNING *`;
  return client.query(query,params)
  .then(res => {return res.rows[0];})
  .catch(e => {return e;})
}