const { Client } = require('pg');

//console.log(process.env.DB_HOST);

const client = new Client({
  user: process.env.DB_USERNAME,
  host: process.env.DB_HOST,
  database: process.env.DB_DATABASE,
  password: process.env.DB_PASSWORD,
  port: process.env.DB_PORT
})
client.connect();

//get all user reports
module.exports.getUserReportsAPI = function(userId) {
  let query = `SELECT *
  FROM report
  WHERE user_id = $1`;
  let params = [userId];
  
  return client.query(query,params)
  .then(res => {return res.rows;})
  .catch(e => {return e;})
}
//get report
module.exports.getReportAPI = function(reportId) {
  let query = `SELECT *
  FROM report
  WHERE id = $1`;
  let params = [reportId];
  
  return client.query(query,params)
  .then(res => {return res.rows;})
  .catch(e => {return e;})
}
//Add new file for project
module.exports.addFileForUnknownWorkOrder = function(fileName, pathName, userId, type){
  let query = `INSERT INTO work_order_files(original_name, path_name, user_id, type)
  VALUES ($1, $2, $3, $4)
  RETURNING *`;
  let params = [fileName, pathName, userId, type];
  return client.query(query,params)
  .then(res => {return res.rows[0];})
  .catch(e => {return e;})
}
//update file active to false, file no longer exist
module.exports.updateActiveFile = function(filename){
  let query = `UPDATE work_order_files
  SET active = false
  WHERE path_name = $1`;
  let params = [filename];
  return client.query(query,params)
  .then(res => {return res.rows;})
  .catch(e => {return e;})
}