const { Client } = require('pg');

//console.log(process.env.DB_HOST);

const client = new Client({
  user: process.env.DB_USERNAME,
  host: process.env.DB_HOST,
  database: process.env.DB_DATABASE,
  password: process.env.DB_PASSWORD,
  port: process.env.DB_PORT
})
client.connect();

module.exports.getUserProjectsAPI = function(userId) {
  let query = `SELECT DISTINCT project.id as project_id, project_number, project_name, subscriber.name as subscriber_name, subscriber.id as subscriber_id, icon as subscriber_icon, completion, completion = 100 as finished, start, finish, notes, project.active as active
  FROM "user", project, working_project, subscriber
  WHERE id_user = "user".id AND
        id_project = project.id AND
        project.subscriber = subscriber.id AND
        "user".id = $1
  ORDER BY finished, project.id desc`;
  let params = [userId];
  
  return client.query(query,params)
  .then(res => {return res.rows;})
  .catch(e => {return e;})
}
module.exports.getProjectAPI = function(projectId) {
  let query = `SELECT DISTINCT project.id as project_id, project_number, project_name, subscriber.name as subscriber_name, subscriber.id as subscriber_id, icon as subscriber_icon, completion, completion = 100 as finished, start, finish, notes, project.active as active
  FROM project, subscriber
  WHERE project.subscriber = subscriber.id AND
        project.id = $1
  ORDER BY finished, project.id desc`;
  let params = [projectId];
  
  return client.query(query,params)
  .then(res => {return res.rows;})
  .catch(e => {return e;})
}