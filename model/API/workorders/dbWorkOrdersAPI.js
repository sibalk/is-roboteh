const { Client } = require('pg');

//console.log(process.env.DB_HOST);

const client = new Client({
  user: process.env.DB_USERNAME,
  host: process.env.DB_HOST,
  database: process.env.DB_DATABASE,
  password: process.env.DB_PASSWORD,
  port: process.env.DB_PORT
})
client.connect();

//get all user work orders
module.exports.getUserWorkOrdersAPI = function(userId) {
  let query = `SELECT *
  FROM work_order
  WHERE user_id = $1`;
  let params = [userId];
  
  return client.query(query,params)
  .then(res => {return res.rows;})
  .catch(e => {return e;})
}
//get all user work orders
module.exports.getUserWorkOrdersForSubscriberAPI = function(userId, subscriberId) {
  let query = `SELECT *
  FROM work_order
  WHERE user_id = $1 AND subscriber_id = $2`;
  let params = [userId, subscriberId];
  
  return client.query(query,params)
  .then(res => {return res.rows;})
  .catch(e => {return e;})
}
//get work order
module.exports.getWorkOrderAPI = function(workOrderId) {
  let query = `SELECT *
  FROM work_order
  WHERE id = $1`;
  let params = [workOrderId];
  
  return client.query(query,params)
  .then(res => {return res.rows;})
  .catch(e => {return e;})
}
//get work order types
module.exports.getWorkOrderTypesAPI = function() {
  let query = `SELECT *
  FROM work_order_type`;
  
  return client.query(query)
  .then(res => {return res.rows;})
  .catch(e => {return e;})
}
//get work order workers
module.exports.getWorkOrderWorkersAPI = function(workOrderId) {
  let query = `SELECT wow.worker_id as id, concat(name,' ',surname) as worker_name
  FROM work_order_workers as wow
  LEFT JOIN "user" u on wow.worker_id = u.id
  WHERE work_order_id = $1`;
  let params = [workOrderId]
  
  return client.query(query,params)
  .then(res => {return res.rows;})
  .catch(e => {return e;})
}
//get work order files
module.exports.getWorkOrderFilesAPI = function(workOrderId) {
  let query = `SELECT wof.id, original_name, path_name, user_id, date, type, note, active
  FROM work_order_files_keys as wofk
  LEFT JOIN work_order_files wof on wofk.work_order_file_id = wof.id
  WHERE work_order_id = $1`;
  let params = [workOrderId]
  
  return client.query(query,params)
  .then(res => {return res.rows;})
  .catch(e => {return e;})
}
//add new work order
module.exports.addNewWorkOrder = function(userId,number,type,date,location,projectId,subscriberId,description,vehicle,arrival,departure,representative,locationCord){
  if(projectId == 0){
    projectId = null;
  }
  if(subscriberId == 0){
    subscriberId = null;
  }
  if(!locationCord)
    locationCord = null;
  let params = [userId,number,type,date,location,subscriberId,projectId,description,vehicle,arrival,departure,representative,locationCord]
  //let projectQuery = ', project_id';
  //if(projectId)
  let query = `INSERT INTO work_order(subscriber_id, project_id, number, date, location, description, vehicle, arrival, departure, work_order_type_id, user_id, representative, location_cord, ma)
  VALUES ($6,$7,$2,$4,$5,$8,$9,$10,$11,$3,$1,$12,$13,true)
  RETURNING *`;
  return client.query(query,params)
  .then(res => {return res.rows[0];})
  .catch(e => {return e;})
}
//edit work order
module.exports.editWorkOrder = function(woId,userId,number,type,date,location,projectId,subscriberId,description,vehicle,arrival,departure,representative,locationCord){
  if(projectId == 0){
    projectId = null;
  }
  if(subscriberId == 0){
    subscriberId = null;
  }
  let params = [userId,number,type,date,location,subscriberId,projectId,description,vehicle,arrival,departure,representative,woId,locationCord]
  //let projectQuery = ', project_id';
  //if(projectId)
  let query = `UPDATE work_order
  SET (subscriber_id, project_id, number, date, location, description, vehicle, arrival, departure, work_order_type_id, user_id, representative, location_cord) = ($6,$7,$2,$4,$5,$8,$9,$10,$11,$3,$1,$12,$14)
  WHERE id = $13
  RETURNING *`;
  return client.query(query,params)
  .then(res => {return res.rows[0];})
  .catch(e => {return e;})
}
//add new worker for work order id
module.exports.addWorker = function(woId, userId){
  let query = `INSERT INTO work_order_workers(worker_id, work_order_id)
  VALUES ($1,$2)`;
  let params = [userId, woId];
  return client.query(query,params)
  .then(res => {return res.rows;})
  .catch(e => {return e;})
}
//add new worker for work order id
module.exports.deleteWorkers = function(wowId){
  let query = `DELETE FROM work_order_workers
  WHERE work_order_id = $1`;
  let params = [wowId];
  return client.query(query,params)
  .then(res => {return res.rows;})
  .catch(e => {return e;})
}
//add new worker for work order id
module.exports.addFile = function(woId, fileId){
  let query = `INSERT INTO work_order_files_keys(work_order_file_id, work_order_id)
  VALUES ($1,$2)`;
  let params = [fileId, woId];
  return client.query(query,params)
  .then(res => {return res.rows;})
  .catch(e => {return e;})
}
//add new worker for work order id
module.exports.deleteFiles = function(wowId){
  let query = `DELETE FROM work_order_files_keys
  WHERE work_order_id = $1`;
  let params = [wowId];
  return client.query(query,params)
  .then(res => {return res.rows;})
  .catch(e => {return e;})
}
//add new report
module.exports.deleteWorkOrder = function(workOrderId){
  let params = [workOrderId]
  let query = `UPDATE work_order
  SET active = false
  WHERE id = $1
  RETURNING *`;
  return client.query(query,params)
  .then(res => {return res.rows[0];})
  .catch(e => {return e;})
}