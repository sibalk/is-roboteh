const { Client } = require('pg');

//console.log(process.env.DB_HOST);

const client = new Client({
  user: process.env.DB_USERNAME,
  host: process.env.DB_HOST,
  database: process.env.DB_DATABASE,
  password: process.env.DB_PASSWORD,
  port: process.env.DB_PORT
})
client.connect();

//Get project's tasks (services) with no projectId (FIXED)
module.exports.getServicesFixed = function(category, active){
  return new Promise((resolve,reject)=>{
    let params = [];
    let categoryQuery = `pt.category`;
    let activityQuery = `pt.active`;
    let i = 1;
    if(category){
      categoryQuery = '$'+i;
      i++;
      params.push(category);
    }
    if(active){
      activityQuery = '$'+i;
      i++;
      if(active == 1)
        active = true;
      else
        active = false;
      params.push(active);
    }
    let query = `SELECT pt.id, pt.id_subscriber, sub.name as subscriber, task_name, task_duration, task_start, task_finish, completion, pt.active, pt.weekend, c.name as category, p.priority, finished, task_note, w.workers, w.workers_id, stc.count as subtask_count, fc.count as files_count, a.count as full_absence_count, aj.count as task_absence_count, workorders, workorders_id, cs."from" as author_id, concat(cs.name,' ', cs.surname) as author, nc.count as notes_count
    FROM project_task as pt
    RIGHT JOIN category c ON pt.category = c.id
    RIGHT JOIN priority p ON pt.priority = p.id
    RIGHT JOIN subscriber sub ON pt.id_subscriber = sub.id
    LEFT JOIN ( SELECT *
                FROM changes_system
                LEFT JOIN "user" u2 on changes_system."from" = u2.id
                WHERE status = 1 AND
                      type = 9 AND
                      id_task IS NOT NULL) AS cs ON cs.id_task = pt.id
    LEFT JOIN ( SELECT id_task, count(id_task)
                FROM subtask
                WHERE active = true
                GROUP BY id_task ) AS stc ON pt.id = stc.id_task
    LEFT JOIN ( SELECT id_task, count(id_task)
                FROM files
                WHERE deleted = false
                GROUP BY id_task) AS fc ON pt.id = fc.id_task
    LEFT JOIN ( SELECT id_task, string_agg(id_user::text, ',') as workers_id, string_agg(concat(name::text, concat(' ', surname::text)), ', ') as workers
                FROM working_task, "user"
                WHERE id_user = "user".id
                GROUP BY id_task ) AS w ON pt.id = w.id_task
    LEFT JOIN ( SELECT working_task.id_task, count(working_task.id_task)
                FROM working_task, absence, project_task
                WHERE absence.id_user = working_task.id_user AND
                      absence.id_task is null and
                      absence.active = true and
                      absence.approved = true and
                      working_task.id_task = project_task.id and
                      ((start::date <= task_finish::date AND start::date >= task_start::date) OR
                      (finish::date <= task_finish::date AND finish::date >= task_start::date))
                GROUP BY working_task.id_task ) AS a ON a.id_task = pt.id
    LEFT JOIN ( SELECT id_task, count(id_task)
                FROM absence as a
                WHERE id_task is not null and
                      approved = true and
                      active = true
                GROUP BY id_task ) AS aj ON aj.id_task = pt.id
    LEFT JOIN ( SELECT task_id, string_agg(concat("left"(name::text,1), "left"(surname::text,1), '-', wo.id, '/', date_part('year', date)), ', ') as workorders, string_agg(wo.id::text, ',') as workorders_id
                FROM work_order as wo, "user" as u
                WHERE task_id is not null AND
                      wo.user_id = u.id AND
                      wo.active = true
                GROUP BY task_id ) AS wo ON pt.id = wo.task_id
    LEFT JOIN ( SELECT id_task, count(id_task)
                FROM notes
                GROUP BY id_task ) AS nc ON pt.id = nc.id_task
    WHERE pt.id_project is null AND
          pt.active = `+activityQuery+` AND
          pt.category = `+categoryQuery+`
    ORDER BY id desc, active desc, task_start`;
    
    client.query(query,params,(err,res)=>{
      if(err) return reject(err);
      return resolve(res.rows);
    })
  })
}
//add new servis REDESIGN
module.exports.addServisNew = function(subscriberId, taskName, taskDuration, start, finish, category, priority, taskCompletion, taskWeekend){
  return new Promise((resolve,reject)=>{
    let completion = 0;
    if(taskCompletion)
      completion = taskCompletion
    let params = [subscriberId, taskName, category, priority, completion, taskWeekend];
    let i = 7;
    let durationQuery = ``;
    let durationSet = ``;
    if(taskDuration){
      durationQuery = `, task_duration`;
      durationSet = `, $` + i;
      i++;
      params.push(taskDuration);
    }
    let startQuery = ``;
    let startSet = ``;
    if(start){
      startQuery = `, task_start`;
      startSet = `, $` + i;
      i++;
      params.push(start);
    }
    let finishQuery = ``;
    let finishSet = ``;
    if(finish){
      finishQuery = `, task_finish`;
      finishSet = `, $` + i;
      i++;
      params.push(finish);
    }
    let query = `INSERT INTO project_task(id_subscriber, task_name, category, priority, completion, weekend`+durationQuery+startQuery+finishQuery+`)
    VALUES ($1, $2, $3, $4, $5, $6`+durationSet+startSet+finishSet+`)
    RETURNING id`;

    client.query(query,params,(err,res)=>{
      if(err) return reject(err);
      return resolve(res.rows[0]);
    })
  })
}
//update servis new
module.exports.updateServisNew = function(taskId, taskName, taskDuration, taskStart, taskFinish, taskSubscriber, taskActive, category, priority, taskWeekend){
  return new Promise((resolve,reject)=>{
    let params = [taskId, taskName, taskActive, category, priority, taskWeekend];
    let i = 7;
    let durationQuery = ``;
    let durationSet = ``;
    if(taskDuration){
      durationQuery = `, task_duration`;
      durationSet = `, $` + i;
      i++;
      params.push(taskDuration);
    }
    let startQuery = ``;
    let startSet = ``;
    if(taskStart){
      startQuery = `, task_start`;
      startSet = `, $` + i;
      i++;
      params.push(taskStart);
    }
    else{
      startQuery = `, task_start`;
      startSet = `, $` + i;
      i++;
      taskStart = null;
      params.push(taskStart);
    }
    let finishQuery = ``;
    let finishSet = ``;
    if(taskFinish){
      finishQuery = `, task_finish`;
      finishSet = `, $` + i;
      i++;
      params.push(taskFinish);
    }
    else{
      finishQuery = `, task_finish`;
      finishSet = `, $` + i;
      i++;
      taskFinish = null;
      params.push(taskFinish);
    }
    let completionQuery = ``;
    let completionSet = ``;
    if(taskSubscriber){
      completionQuery = `, id_subscriber`;
      completionSet = `, $` + i;
      i++;
      params.push(taskSubscriber);
    }
    let query = `UPDATE project_task
    SET (task_name, active, category, priority, weekend`+durationQuery+startQuery+finishQuery+completionQuery+`) = ($2, $3, $4, $5, $6`+durationSet+startSet+finishSet+completionSet+`)
    WHERE id = $1`;

    client.query(query,params,(err,res)=>{
      if(err) return reject(err);
      return resolve(res.rows);
    })
  })
}
//Get user conflicts with other project's tasks
module.exports.getWorkerServiceConflicts= function(userId, start, finish, taskId){
  return new Promise((resolve,reject)=>{
    let params = [userId, start, finish];
    let updateTaskQuery = '';
    if(taskId){
      updateTaskQuery = ' pt.id != $4 AND';
      params.push(taskId);
    }
    let query = `SELECT wt.id, id_user, u.name, surname, task_name, task_start, task_finish, category, priority, p.project_name, id_subscriber, s.name as subscriber, pt.id_project, pt.id as id_task
    FROM working_task AS wt
    LEFT JOIN "user" u on wt.id_user = u.id
    LEFT JOIN project_task pt on wt.id_task = pt.id
    LEFT JOIN project p on pt.id_project = p.id
    LEFT JOIN subscriber s on pt.id_subscriber = s.id
    WHERE u.active = true AND
          pt.active = true AND
          pt.completion != 100 AND
          id_user = $1 AND`+updateTaskQuery+`
          ((task_start::date >= $2::date AND task_start::date <= $3::date) OR
          (task_finish::date >= $2::date AND task_finish::date <= $3::date) OR
          (task_start::date <= $2::date AND task_finish::date >= $3::date))
      UNION
      SELECT a.id as id, id_user, u.name, u.surname, a.name, start, finish, r.id, role, r.name, id_project, null::varchar, id_project, id_project
      FROM absence as a
      LEFT JOIN "user" u on a.id_user = u.id
      LEFT JOIN reason r on a.id_reason = r.id
      WHERE a.active = true AND
            a.approved = true AND
            a.id_user = $1 AND
            a.id_project is null AND
            ((start::date >= $2::date AND start::date <= $3::date) OR
            (finish::date >= $2::date AND finish::date <= $3::date) OR
              (start::date <= $2::date AND finish::date >= $3::date))`;
    
    client.query(query,params,(err,res)=>{
      if(err) return reject(err);
      return resolve(res.rows);
    })
  })
}
//get task changes system (service)
module.exports.getTaskChangesSystem = function(taskId){
  return new Promise((resolve,reject)=>{
    let query = `SELECT c.id, c.id_project, c.id_task, id_subtask, id_working_project, id_working_task, c.date, "from", id_note, c.id_subscriber, id_absence, u.name, surname, cs.id as id_status, cs.status, ct.id as id_type, ct.type, s.name as subtask, id_build_part, notes, bp.name as buildpart
    FROM changes_system as c
    LEFT JOIN "user" u on c."from" = u.id
    LEFT JOIN change_status cs on c.status = cs.id
    LEFT JOIN change_type ct on c.type = ct.id
    LEFT JOIN project_task pt on c.id_task = pt.id
    LEFT JOIN subtask s on c.id_subtask = s.id
    LEFT JOIN build_parts bp on c.id_build_part = bp.id
    WHERE c.id_task = $1
    ORDER BY date`;
    let params = [taskId];
    
    client.query(query,params,(err,res)=>{
      if(err) return reject(err);
      return resolve(res.rows);
    })
  })
}
//get subtask changes system
module.exports.getSubtaskChangesSystem = function(subtaskId){
  return new Promise((resolve,reject)=>{
    let query = `SELECT c.id, id_project, c.id_task, id_subtask, id_working_project, id_working_task, c.date, "from", id_note, id_subscriber, id_absence, u.name, surname, cs.id as id_status, cs.status, ct.id as id_type, ct.type
    FROM changes_system as c
    LEFT JOIN "user" u on c."from" = u.id
    LEFT JOIN change_status cs on c.status = cs.id
    LEFT JOIN change_type ct on c.type = ct.id
    LEFT JOIN subtask s on c.id_subtask = s.id
    WHERE id_subtask = $1
    ORDER BY date`;
    let params = [subtaskId];
    
    client.query(query,params,(err,res)=>{
      if(err) return reject(err);
      return resolve(res.rows);
    })
  })
}
// get absence changes system
module.exports.getAbsenceChangesSystem = function(absenceId){
  return new Promise((resolve,reject)=>{
    let query = `SELECT c.id, id_project, id_task, id_subtask, id_working_project, id_working_task, c.date, "from", id_note, id_subscriber, id_absence, u.name, surname, cs.id as id_status, cs.status, ct.id as id_type, ct.type
    FROM changes_system as c
    LEFT JOIN "user" u on c."from" = u.id
    LEFT JOIN change_status cs on c.status = cs.id
    LEFT JOIN change_type ct on c.type = ct.id
    WHERE id_absence = $1
    ORDER BY date`;
    let params = [absenceId];
    
    client.query(query,params,(err,res)=>{
      if(err) return reject(err);
      return resolve(res.rows);
    })
  })
}