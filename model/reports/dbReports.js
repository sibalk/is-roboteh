const { Client } = require('pg');

//console.log(process.env.DB_HOST);

const client = new Client({
  user: process.env.DB_USERNAME,
  host: process.env.DB_HOST,
  database: process.env.DB_DATABASE,
  password: process.env.DB_PASSWORD,
  port: process.env.DB_PORT
})
client.connect();

//get last 100 reports for user 
module.exports.getUserLastReports = function(userId){
  return new Promise((resolve, reject) => {
    let params = [userId];
    let query = `SELECT report.id, report.project_id, p.project_name, p.project_number, report.subscriber_id, s.name, report.date, report.time, report.start, report.finish, report.description, report.report_type as report_type_id, report_type.report_type, report.active, report.ma, report.created, report.other
    FROM report
    LEFT JOIN report_type on report.report_type = report_type.id
    LEFT JOIN project p on report.project_id = p.id
    LEFT JOIN subscriber s on report.subscriber_id = s.id
    WHERE user_id = $1
    ORDER BY report.id desc LIMIT 100`;
  
    client.query(query,params,(err,res)=>{
      if(err) return reject(err);
      return resolve(res.rows);
    })
  })
};
//get dates for monthly reports 
module.exports.getDatesForMonthlyReports = function(userId){
  return new Promise((resolve, reject) => {
    let params = [];
    let userQuery = '';
    if(userId){
      params.push(userId);
      userQuery = ' AND user_id = $1';
    }
    let query = `SELECT DISTINCT date_part('year', date) as year, date_part('month', date) as month
    FROM report
    WHERE active = true`+userQuery+`
    GROUP BY  date_part('year', date), date_part('month', date)
    ORDER BY date_part('year', date) DESC , date_part('month', date) DESC`;
  
    client.query(query,params,(err,res)=>{
      if(err) return reject(err);
      return resolve(res.rows);
    })
  })
};
//get users for monthly reports with chosen month and year
module.exports.getUsersForMonthlyReports = function(month, year){
  return new Promise((resolve, reject) => {
    let startMonth = month, startYear = year, finishMonth = parseInt(month)+1, finishYear = year;
    if(month == 12){
      finishMonth = 1;
      finishYear = parseInt(year)+1;
    }
    if(parseInt(startMonth) < 10)
      startMonth = '0'+startMonth;
    if(parseInt(finishMonth) < 10)
      finishMonth = '0'+finishMonth;
    let query = `SELECT DISTINCT ON (name, surname) user_id, name, surname
    FROM report
    LEFT JOIN "user" u on report.user_id = u.id
    WHERE date >= '`+startYear+`-`+startMonth+`-01' AND date < '`+finishYear+`-`+finishMonth+`-01' AND report.active = true
    ORDER BY surname ASC, name ASC `;
  
    client.query(query,(err,res)=>{
      if(err) return reject(err);
      return resolve(res.rows);
    })
  })
};
//get reports for monthly reports with chosen month and year
module.exports.getReportsForMonthlyReports = function(month, year, userId){
  return new Promise((resolve, reject) => {
    let params =[userId]
    let startMonth = month, startYear = year, finishMonth = parseInt(month)+1, finishYear = year;
    if(month == 12){
      finishMonth = 1;
      finishYear = parseInt(year)+1;
    }
    if(parseInt(startMonth) < 10)
      startMonth = '0'+startMonth;
    if(parseInt(finishMonth) < 10)
      finishMonth = '0'+finishMonth;
    let query = `SELECT report.id, report.project_id, p.project_name, p.project_number, report.subscriber_id, s.name, report.date, report.time, report.start, report.finish, report.description, report.report_type as report_type_id, report_type.report_type, report.active, report.ma, report.created, report.other
    FROM report
    LEFT JOIN report_type on report.report_type = report_type.id
    LEFT JOIN project p on report.project_id = p.id
    LEFT JOIN subscriber s on report.subscriber_id = s.id
    WHERE date >= '`+startYear+`-`+startMonth+`-01' AND date < '`+finishYear+`-`+finishMonth+`-01' AND report.active = true AND user_id = $1
    ORDER BY date`;
  
    client.query(query,params,(err,res)=>{
      if(err) return reject(err);
      return resolve(res.rows);
    })
  })
};
//get reports for user 
module.exports.getUserReports = function(userId,dateStart,dateFinish,radio,projectId,other,reportType){
  return new Promise((resolve,reject)=>{
    let params;
    let query;
    if(dateStart && dateFinish){
      //get every report based on search input
      params = [userId,dateStart,dateFinish]
      let i = 4;
      let typeQuery = 'report.report_type'
      if(reportType != '0'){
        typeQuery = '$'+i;
        params.push(reportType);
        i++;
      }
      let filterQuery = '';
      if(radio == '0'){
        filterQuery = 'AND report.project_id = report.project_id';
        if(projectId != '0'){
          filterQuery = 'AND report.project_id = $'+i;
          params.push(projectId);
          i++;
        }
      }
      else{
        if(other){
          other = '%'+other+'%'
          filterQuery = 'AND lower(other) LIKE lower($'+i+')';
          params.push(other);
          i++;
        }
        else{
          filterQuery = 'AND report.project_id IS NULL'
        }
      }
      query = `SELECT report.id, report.project_id, p.project_name, p.project_number, report.subscriber_id, s.name, report.date, report.time, report.start, report.finish, report.description, report.report_type as report_type_id, report_type.report_type, report.active, report.ma, report.created, report.other
      FROM report
      LEFT JOIN report_type on report.report_type = report_type.id
      LEFT JOIN project p on report.project_id = p.id
      LEFT JOIN subscriber s on report.subscriber_id = s.id
      WHERE user_id = $1 AND
            (report.date::date >= $2::date AND report.date::date <= $3::date) AND
            report.report_type = `+typeQuery+`
            `+filterQuery+`
      ORDER BY report.id desc`;
    }
    else{
      //last 100 reports
      params = [userId];
      query = `SELECT report.id, report.project_id, p.project_name, p.project_number, report.subscriber_id, s.name, report.date, report.time, report.start, report.finish, report.description, report.report_type as report_type_id, report_type.report_type, report.active, report.ma, report.created, report.other
      FROM report
      LEFT JOIN report_type on report.report_type = report_type.id
      LEFT JOIN project p on report.project_id = p.id
      LEFT JOIN subscriber s on report.subscriber_id = s.id
      WHERE user_id = $1
      ORDER BY report.id desc LIMIT 100`;
    }
  
    client.query(query,params,(err,res)=>{
      if(err) return reject(err);
      return resolve(res.rows);
    })
  })
};
//get all report types (id,text)
module.exports.getReportsTypes = function(){
  return new Promise((resolve, reject)=>{
    let query = `SELECT id, report_type as text
    FROM report_type
    ORDER BY report_type`;

    client.query(query,(err,res)=>{
      if(err) return reject(err);
      return resolve(res.rows);
    })
  })
}
//add new report types (id,text)
module.exports.addReportsType = function(type){
  return new Promise((resolve, reject)=>{
    let query = `INSERT INTO report_type(report_type)
    VALUES ($1)
    RETURNING id`;
    let params = [type]

    client.query(query,params,(err,res)=>{
      if(err) return reject(err);
      return resolve(res.rows[0]);
    })
  })
}
//add new report
module.exports.addNewReport = function(userId,date,time,start,finish,projectId,report_type,other,description, radio){
  return new Promise((resolve, reject)=>{
    let params = [userId,date,time,start,finish,description,report_type]
    let radioQuery = '';
    if(radio == '0'){
      radioQuery = ', project_id';
      params.push(projectId);
    }
    else{
      radioQuery = ', other';
      params.push(other);
    }
    //let projectQuery = ', project_id';
    //if(projectId)
    let query = `INSERT INTO report(user_id, date, time, start, finish, description, report_type`+radioQuery+`)
    VALUES ($1,$2,$3,$4,$5,$6,$7,$8)
    RETURNING id`;

    client.query(query,params,(err,res)=>{
      if(err) return reject(err);
      return resolve(res.rows[0]);
    })
  })
}
//edit report
module.exports.editReport = function(reportId,userId,date,time,start,finish,projectId,report_type,other,description, radio){
  return new Promise((resolve, reject)=>{
    let params = [reportId,userId,date,time,start,finish,description,report_type]
    let radioQuery = '';
    if(radio == '0'){
      radioQuery = ', project_id, other';
      params.push(projectId);
      params.push(null);
    }
    else{
      radioQuery = ', other, project_id';
      params.push(other);
      params.push(null);
    }
    let query = `UPDATE report
    SET (user_id, date, time, start, finish, description, report_type`+radioQuery+`) = ($2,$3,$4,$5,$6,$7,$8,$9,$10)
    WHERE id = $1`;

    client.query(query,params,(err,res)=>{
      if(err) return reject(err);
      return resolve(res.rows[0]);
    })
  })
}
//delete report (update active)
module.exports.deleteReport = function(reportId,active){
  return new Promise((resolve, reject)=>{
    let params = [reportId,active]
    let query = `UPDATE report
    SET active = $2
    WHERE id = $1`;

    client.query(query,params,(err,res)=>{
      if(err) return reject(err);
      return resolve(res.rows[0]);
    })
  })
}
//Get all projects (for select2 (id, text)) with RT number and project name
module.exports.getProjectsSelect = function(active){
  return new Promise((resolve,reject)=>{
    let activeProject = '';
    if(active && active == 1)
      activeProject =  ' AND completion < 100';
    let query = `SELECT id, concat(project_number, ' - ', project_name) as text
    FROM project
    WHERE active = true`+activeProject+`
    ORDER BY id desc`;

    client.query(query,(err,res)=>{
      if(err) return reject(err);
      return resolve(res.rows);
    })
  })
};