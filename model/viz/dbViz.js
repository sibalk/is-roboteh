const { Client } = require('pg');

//console.log(process.env.DB_HOST);

const client = new Client({
  user: process.env.DB_USERNAME,
  host: process.env.DB_HOST,
  database: process.env.DB_DATABASE,
  password: process.env.DB_PASSWORD,
  port: process.env.DB_PORT
})
client.connect();

//get all unfinished tasks assignment sorted by user roles
module.exports.getUnfinishedAssignmentsByUserRole = function(mechAssemblyRole, cncOperaterRole, electricanRole, mechanicRole, welderRole, serviserRole, plcRole, robotRole, builderRole, leaderRole, workerRole, studentRole, secrataryRole, adminRole){
  return new Promise((resolve, reject)=>{
    let mechAsseblyQuery = '';
    if(mechAssemblyRole == 'false'){
      mechAsseblyQuery = ` AND r.role != 'strojni monter'`;
    }
    let cncOperaterQuery = '';
    if(cncOperaterRole == 'false'){
      cncOperaterQuery = ` AND r.role != 'cnc operater' AND r.role != 'cnc'`;
    }
    let electricanQuery = '';
    if(electricanRole == 'false'){
      electricanQuery = ` AND r.role != 'električar'`;
    }
    let mechanicQuery = '';
    if(mechanicRole == 'false'){
      mechanicQuery = ` AND r.role != 'strojnik'`;
    }
    let welderQuery = '';
    if(welderRole == 'false'){
      welderQuery = ` AND r.role != 'varilec'`;
    }
    let serviserQuery = '';
    if(serviserRole == 'false'){
      serviserQuery = ` AND r.role != 'serviser'`;
    }
    let plcQuery = '';
    if(plcRole == 'false'){
      plcQuery = ` AND r.role != 'programer plc-jev'`;
    }
    let robotQuery = '';
    if(robotRole == 'false'){
      robotQuery = ` AND r.role != 'programer robotov'`;
    }
    let builderQuery = '';
    if(builderRole == 'false'){
      builderQuery = ` AND r.role != 'konstrukter' AND r.role != 'konstruktor'`;
    }
    let leaderQuery = '';
    if(leaderRole == 'false'){
      leaderQuery = ` AND r.role NOT LIKE 'vodja%'`;
    }
    let workerQuery = '';
    if(workerRole == 'false'){
      workerQuery = ` AND r.role != 'delavec'`;
    }
    let studentQuery = '';
    if(studentRole == 'false'){
      studentQuery = ` AND r.role != 'študent'`;
    }
    let secrataryQuery = '';
    if(secrataryRole == 'false'){
      secrataryQuery = ` AND r.role != 'tajnik' AND r.role != 'komercialist' AND r.role != 'komerciala' AND r.role != 'računovodstvo'`;
    }
    let adminQuery = '';
    if(adminRole == 'false'){
      adminQuery = ` AND r.role != 'admin'`;
    }
    let query = `SELECT wt.id, wt.id_task, wt.id_user, pt.id_project, pt.task_name as content, pt.task_name as title, pt.task_duration, pt.task_start as start, pt.task_finish as end, pt.completion, pt.category, pt.priority, pt.subtasks, pt.id_subscriber as id_subscriber, u.id as group, u.name, u.role, u.surname, concat(project_number, ' - ', project_name) as project_label, s.name as subscriber_label, r.role as role, project_number, project_name
    FROM working_task as wt
    LEFT JOIN project_task pt on wt.id_task = pt.id
    LEFT JOIN "user" u on wt.id_user = u.id
    LEFT JOIN project p on pt.id_project = p.id
    LEFT JOIN subscriber s on pt.id_subscriber = s.id or p.subscriber = s.id
    LEFT JOIN role r on u.role = r.id
    WHERE u.active = true AND
          pt.active = true AND
          pt.completion != 100 AND
          pt.task_start is not null AND
          pt.task_finish is not null` + mechAsseblyQuery + cncOperaterQuery + electricanQuery + mechanicQuery + welderQuery + serviserQuery + plcQuery + robotQuery + builderQuery + leaderQuery + workerQuery + studentQuery + secrataryQuery + adminQuery + `
    ORDER BY r.role='konstrukter' desc, r.role='konstruktor' desc, r.role='strojnik' desc, r.role='strojni monter' desc, r.role='cnc operater' desc, r.role='cnc' desc, r.role='varilec' desc, r.role='električar' desc, r.role='programer plc-jev' desc, r.role='programer robotov' desc, r.role='serviser' desc, r.role='delavec' desc, r.role='študent' desc, r.role='delavec' desc, surname, name`;
    let params = [];

    client.query(query,(err,res)=>{
      if(err) return reject(err);
      return resolve(res.rows);
    })
  })
}
//get all absences sorted by user roles
module.exports.getAbsencesByUserRole = function(mechAssemblyRole, cncOperaterRole, electricanRole, mechanicRole, welderRole, serviserRole, plcRole, robotRole, builderRole, leaderRole, workerRole, studentRole, secrataryRole, adminRole){
  return new Promise((resolve, reject)=>{
    let mechAsseblyQuery = '';
    if(mechAssemblyRole == 'false'){
      mechAsseblyQuery = ` AND r.role != 'strojni monter'`;
    }
    let cncOperaterQuery = '';
    if(cncOperaterRole == 'false'){
      cncOperaterQuery = ` AND r.role != 'cnc operater' AND r.role != 'cnc'`;
    }
    let electricanQuery = '';
    if(electricanRole == 'false'){
      electricanQuery = ` AND r.role != 'električar'`;
    }
    let mechanicQuery = '';
    if(mechanicRole == 'false'){
      mechanicQuery = ` AND r.role != 'strojnik'`;
    }
    let welderQuery = '';
    if(welderRole == 'false'){
      welderQuery = ` AND r.role != 'varilec'`;
    }
    let serviserQuery = '';
    if(serviserRole == 'false'){
      serviserQuery = ` AND r.role != 'serviser'`;
    }
    let plcQuery = '';
    if(plcRole == 'false'){
      plcQuery = ` AND r.role != 'programer plc-jev'`;
    }
    let robotQuery = '';
    if(robotRole == 'false'){
      robotQuery = ` AND r.role != 'programer robotov'`;
    }
    let builderQuery = '';
    if(builderRole == 'false'){
      builderQuery = ` AND r.role != 'konstrukter' AND r.role != 'konstruktor'`;
    }
    let leaderQuery = '';
    if(leaderRole == 'false'){
      leaderQuery = ` AND r.role NOT LIKE 'vodja%'`;
    }
    let workerQuery = '';
    if(workerRole == 'false'){
      workerQuery = ` AND r.role != 'delavec'`;
    }
    let studentQuery = '';
    if(studentRole == 'false'){
      studentQuery = ` AND r.role != 'študent'`;
    }
    let secrataryQuery = '';
    if(secrataryRole == 'false'){
      secrataryQuery = ` AND r.role != 'tajnik' AND r.role != 'komercialist' AND r.role != 'komerciala' AND r.role != 'računovodstvo'`;
    }
    let adminQuery = '';
    if(adminRole == 'false'){
      adminQuery = ` AND r.role != 'admin'`;
    }
    let query = `SELECT concat('absence',a.id) as id, a.name as reason_name, a.id_user, a.id_project, a.id_task, a.id_reason, a.start as start, a.finish as end, res.name as title, res.name as content, project_name, pt.task_name, u.name, surname, r.role as role, u.id as group
    FROM absence a
    LEFT JOIN reason res on a.id_reason = res.id
    LEFT JOIN project p on a.id_project = p.id
    LEFT JOIN project_task pt on a.id_task = pt.id
    LEFT JOIN "user" u on a.id_user = u.id
    LEFT JOIN role r on u.role = r.id
    WHERE a.active = TRUE AND
          a.approved = TRUE` + mechAsseblyQuery + cncOperaterQuery + electricanQuery + mechanicQuery + welderQuery + serviserQuery + plcQuery + robotQuery + builderQuery + leaderQuery + workerQuery + studentQuery + secrataryQuery + adminQuery + `
    ORDER BY r.role='konstrukter' desc, r.role='konstruktor' desc, r.role='strojnik' desc, r.role='strojni monter' desc, r.role='cnc operater' desc, r.role='cnc' desc, r.role='varilec' desc, r.role='električar' desc, r.role='programer plc-jev' desc, r.role='programer robotov' desc, r.role='serviser' desc, r.role='delavec' desc, r.role='študent' desc, r.role='delavec' desc, surname, name`;
    let params = [];

    client.query(query,(err,res)=>{
      if(err) return reject(err);
      return resolve(res.rows);
    })
  })
}
//get all users (except info users) order by roles
module.exports.getUsersOrderByRoles = function(mechAssemblyRole, cncOperaterRole, electricanRole, mechanicRole, welderRole, serviserRole, plcRole, robotRole, builderRole, leaderRole, workerRole, studentRole, secrataryRole, adminRole){
  return new Promise((resolve, reject)=>{
    let mechAsseblyQuery = '';
    if(mechAssemblyRole == 'false'){
      mechAsseblyQuery = ` AND r.role != 'strojni monter'`;
    }
    let cncOperaterQuery = '';
    if(cncOperaterRole == 'false'){
      cncOperaterQuery = ` AND r.role != 'cnc operater' AND r.role != 'cnc'`;
    }
    let electricanQuery = '';
    if(electricanRole == 'false'){
      electricanQuery = ` AND r.role != 'električar'`;
    }
    let mechanicQuery = '';
    if(mechanicRole == 'false'){
      mechanicQuery = ` AND r.role != 'strojnik'`;
    }
    let welderQuery = '';
    if(welderRole == 'false'){
      welderQuery = ` AND r.role != 'varilec'`;
    }
    let serviserQuery = '';
    if(serviserRole == 'false'){
      serviserQuery = ` AND r.role != 'serviser'`;
    }
    let plcQuery = '';
    if(plcRole == 'false'){
      plcQuery = ` AND r.role != 'programer plc-jev'`;
    }
    let robotQuery = '';
    if(robotRole == 'false'){
      robotQuery = ` AND r.role != 'programer robotov'`;
    }
    let builderQuery = '';
    if(builderRole == 'false'){
      builderQuery = ` AND r.role != 'konstrukter' AND r.role != 'konstruktor'`;
    }
    let leaderQuery = '';
    if(leaderRole == 'false'){
      leaderQuery = ` AND r.role NOT LIKE 'vodja%'`;
    }
    let workerQuery = '';
    if(workerRole == 'false'){
      workerQuery = ` AND r.role != 'delavec'`;
    }
    let studentQuery = '';
    if(studentRole == 'false'){
      studentQuery = ` AND r.role != 'študent'`;
    }
    let secrataryQuery = '';
    if(secrataryRole == 'false'){
      secrataryQuery = ` AND r.role != 'tajnik' AND r.role != 'komercialist' AND r.role != 'komerciala' AND r.role != 'računovodstvo'`;
    }
    let adminQuery = '';
    if(adminRole == 'false'){
      adminQuery = ` AND r.role != 'admin'`;
    }
    let query = `SELECT "user".id, concat(name, ' ', surname) as content, r.role
    FROM "user"
    LEFT JOIN role r on "user".role = r.id
    WHERE active = true AND
          name NOT LIKE 'Info%' AND
          name NOT LIKE 'ADMIN'` + mechAsseblyQuery + cncOperaterQuery + electricanQuery + mechanicQuery + welderQuery + serviserQuery + plcQuery + robotQuery + builderQuery + leaderQuery + workerQuery + studentQuery + secrataryQuery + adminQuery + `
    ORDER BY r.role='strojnik' desc, r.role='strojni monter' desc, r.role='cnc operater' desc, r.role='cnc' desc, r.role='varilec' desc, r.role='električar' desc, r.role='programer plc-jev' desc, r.role='programer robotov' desc, r.role='serviser' desc, r.role='delavec' desc, r.role='konstrukter' desc, r.role='konstruktor' desc, r.role='študent' desc, r.role='delavec' desc, surname, name`;
    let params = [];

    client.query(query,(err,res)=>{
      if(err) return reject(err);
      return resolve(res.rows);
    })
  })
}
//get all active projects with content and title ready
module.exports.getAllActiveProjects = function(loggedUser){
  return new Promise((resolve, reject)=>{
    let loggedUserQueryJoinAdd = '';
    let loggedUserQuerySelectAdd = '';
    if (loggedUser){
      loggedUserQuerySelectAdd = ', my_project';
      loggedUserQueryJoinAdd = `LEFT JOIN ( SELECT DISTINCT on (id_user, id_project) id, id_user, id_project, true as my_project
        FROM working_project
        WHERE id_user = ` + loggedUser + `) wp on wp.id_project = p.id`;
    }
    let query = `SELECT p.id, p.project_number, p.project_name, p.subscriber as subscriber_id, p.created, p.start, p.finish as end, p.completion, p.notes, p.active, s.name as subscriber, concat(project_number, ' - ', project_name) as content, concat(project_number, ' - ', project_name, ', ', s.name, ', ', completion, '%') as title,concat(project_number, ' - ', project_name) as text, p.id as group` + loggedUserQuerySelectAdd + `
    FROM project as p
    LEFT JOIN subscriber s on p.subscriber = s.id
    ` + loggedUserQueryJoinAdd + `
    WHERE p.active = true`;
    let params = [];

    client.query(query,(err,res)=>{
      if(err) return reject(err);
      return resolve(res.rows);
    })
  })
}