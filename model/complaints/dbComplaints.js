const { Client } = require('pg');

//console.log(process.env.DB_HOST);

const client = new Client({
  user: process.env.DB_USERNAME,
  host: process.env.DB_HOST,
  database: process.env.DB_DATABASE,
  password: process.env.DB_PASSWORD,
  port: process.env.DB_PORT
})
client.connect();

// //get activities
// module.exports.getAllActivities = function(activity, status){
//   return new Promise((resolve,reject)=>{
//     let activitiesActivity;
//     switch(activity) {
//       case '0': activitiesActivity = 'ma.active'; break;
//       case '1': activitiesActivity = 'true'; break;
//       case '2': activitiesActivity = 'false'; break;
//       default: activitiesActivity = 'true';
//     }
//     let activitiesStatus;
//     switch(status) {
//       case '0': activitiesStatus = 'ma.status_id = ma.status_id'; break;
//       case '1': activitiesStatus = 'ma.status_id = 1'; break;
//       case '2': activitiesStatus = 'ma.status_id = 2'; break;
//       case '3': activitiesStatus = 'ma.status_id != 1 AND ma.status_id != 2'; break;
//       default: activitiesStatus = 'ma.status_id = ma.status_id';
//     }
//     let query = `SELECT *
//     FROM managerial_activity as ma
//     LEFT JOIN ( SELECT activity_id, string_agg(user_id::text, ',') as responsible_users_id, string_agg(concat(name::text, concat(' ', surname::text)), ', ') as responsible_users
//                 FROM managerial_liable, "user"
//                 WHERE user_id = "user".id
//                 GROUP BY activity_id ) AS w ON ma.id = w.activity_id
//     WHERE ma.active = ` + activitiesActivity + ` AND
//           `+activitiesStatus+`
//     ORDER BY id`;
//     //let params = [activitiesActivity];

//     client.query(query,(err,res)=>{
//       if(err) return reject(err);
//       return resolve(res.rows);
//     })
//   })
// }
//get complaints status
module.exports.getCompStatus = function(){
  return new Promise((resolve,reject)=>{
    let query = `SELECT *
    FROM complaints_status
    ORDER BY id`;

    client.query(query,(err,res)=>{
      if(err) return reject(err);
      return resolve(res.rows);
    })
  })
}
//get complaints justification
module.exports.getCompJustification = function(){
  return new Promise((resolve,reject)=>{
    let query = `SELECT *
    FROM complaints_justification
    ORDER BY id`;

    client.query(query,(err,res)=>{
      if(err) return reject(err);
      return resolve(res.rows);
    })
  })
}
////////////////////////////////////////////////////////////////////////////////////COMPLAINTS SUBSCRIBERS - REKLAMACIJA NAROCNIKOv
//update comp sub status
module.exports.updateCompSubStatus = function(compSubId, statusId){
  return new Promise((resolve,reject)=>{
    let query = `UPDATE complaints_subscribers
    SET status_id = $2
    WHERE id = $1
    RETURNING *`;
    let params = [compSubId, statusId];
    
    client.query(query,params,(err,res)=>{
      if(err) return reject(err);
      return resolve(res.rows[0]);
    })
  })
}
//get complaint subscriber
module.exports.getComplaintsSubscriberId = function(id){
  return new Promise((resolve,reject)=>{
    let query = `SELECT cs.id, cs.complaint_number, cs.date, cs.subscriber_id, s.name as subscriber, cs.subscriber_contact, cs.project_id, p.project_number, p.project_name, cs.description, cs.error_type_id, cet.error_type, cs.cause, cs.measure, cs.finished, cs.cost, cs.notes, cs.active, cs.solver, fc.count as files_count, css.id as status_id, css.status as status, cj.id as justification_id, cj.justification as justification
    FROM complaints_subscribers as cs
    LEFT JOIN complaints_error_type cet on cs.error_type_id = cet.id
    LEFT JOIN project p on cs.project_id = p.id
    LEFT JOIN subscriber s on cs.subscriber_id = s.id
    LEFT JOIN complaints_status css on css.id = cs.status_id
    LEFT JOIN complaints_justification cj on cj.id = cs.justification_id
    LEFT JOIN ( SELECT comp_sub_id, count(comp_sub_id)
                FROM complaints_files
                WHERE deleted = false
                GROUP BY comp_sub_id) AS fc ON cs.id = fc.comp_sub_id
    WHERE cs.id = $1`;
    let params = [id];

    client.query(query, params, (err,res)=>{
      if(err) return reject(err);
      return resolve(res.rows[0]);
    })
  })
}
//get complaint subscriber author
module.exports.getComplaintsSubscriberAuthor = function(id){
  return new Promise((resolve,reject)=>{
    let query = `SELECT u.name, u.surname, date
    FROM changes_system as cs
    LEFT JOIN "user" u on u.id = cs."from"
    WHERE id_comp_sub = $1 AND
          status = 1
    ORDER BY cs.id`;
    let params = [id];

    client.query(query, params, (err,res)=>{
      if(err) return reject(err);
      return resolve(res.rows[0]);
    })
  })
}
//get complaints for subscribers
module.exports.getComplaintsSubscribers = function(activity, errorType){
  return new Promise((resolve,reject)=>{
    let complaintsActivity;
    switch(activity) {
      case '0': complaintsActivity = 'cs.active'; break;
      case '1': complaintsActivity = 'true'; break;
      case '2': complaintsActivity = 'false'; break;
      default: complaintsActivity = 'true';
    }
    let complaintsErrorType;
    switch(errorType) {
      case '0': complaintsErrorType = 'cs.error_type_id = cs.error_type_id'; break;
      case '1': complaintsErrorType = 'cs.error_type_id = 1'; break;//mehanske
      case '2': complaintsErrorType = 'cs.error_type_id = 2'; break;//elektro
      case '3': complaintsErrorType = 'cs.error_type_id = 3'; break;//programske
      default: complaintsErrorType = 'cs.error_type_id = cs.error_type_id';
    }
    // let query = `SELECT cs.id, cs.complaint_number, cs.date, cs.subscriber_id, s.name as subscriber, cs.subscriber_contact, cs.project_id, p.project_number, p.project_name, cs.description, cs.error_type_id, cet.error_type, cs.cause, cs.measure, cs.finished, cs.cost, cs.notes, cs.active, contact_users_id, contact_users, cs.solver, fc.count as files_count, css.id as status_id, css.status as status, cj.id as justification_id, cj.justification as justification
    // FROM complaints_subscribers as cs
    // LEFT JOIN ( SELECT complaint_sub_id, string_agg(user_id::text, ',') as contact_users_id, string_agg(concat(name::text, concat(' ', surname::text)), ', ') as contact_users
    //             FROM complaints_subs_users, "user"
    //             WHERE user_id = "user".id
    //             GROUP BY complaint_sub_id) AS w ON cs.id = w.complaint_sub_id
    // LEFT JOIN complaints_error_type cet on cs.error_type_id = cet.id
    // LEFT JOIN project p on cs.project_id = p.id
    // LEFT JOIN subscriber s on cs.subscriber_id = s.id
    // LEFT JOIN complaints_status css on css.id = cs.status_id
    // LEFT JOIN complaints_justification cj on cj.id = cs.justification_id
    // LEFT JOIN ( SELECT comp_sub_id, count(comp_sub_id)
    //             FROM complaints_files
    //             WHERE deleted = false
    //             GROUP BY comp_sub_id) AS fc ON cs.id = fc.comp_sub_id
    // WHERE cs.active = ` + complaintsActivity + ` AND
    //       ` + complaintsErrorType + `
    // ORDER BY cs.id`;
    let query = `SELECT cs.id, cs.complaint_number, cs.date, cs.subscriber_id, s.name as subscriber, cs.subscriber_contact, cs.project_id, p.project_number, p.project_name, cs.description, cs.error_type_id, cet.error_type, cs.cause, cs.measure, cs.finished, cs.cost, cs.notes, cs.active, cs.solver, fc.count as files_count, css.id as status_id, css.status as status, cj.id as justification_id, cj.justification as justification
    FROM complaints_subscribers as cs
    LEFT JOIN complaints_error_type cet on cs.error_type_id = cet.id
    LEFT JOIN project p on cs.project_id = p.id
    LEFT JOIN subscriber s on cs.subscriber_id = s.id
    LEFT JOIN complaints_status css on css.id = cs.status_id
    LEFT JOIN complaints_justification cj on cj.id = cs.justification_id
    LEFT JOIN ( SELECT comp_sub_id, count(comp_sub_id)
                FROM complaints_files
                WHERE deleted = false
                GROUP BY comp_sub_id) AS fc ON cs.id = fc.comp_sub_id
    WHERE cs.active = ` + complaintsActivity + ` AND
          ` + complaintsErrorType + `
    ORDER BY cs.id`;
    //let params = [activitiesActivity];

    client.query(query,(err,res)=>{
      if(err) return reject(err);
      return resolve(res.rows);
    })
  })
}
//get error type for subscriber's complaints
module.exports.getErrorTypes = function(){
  return new Promise((resolve,reject)=>{
    let query = `SELECT *
    FROM complaints_error_type`;
    //let params = [activitiesActivity];

    client.query(query,(err,res)=>{
      if(err) return reject(err);
      return resolve(res.rows);
    })
  })
}
//Add new complaint sub
module.exports.addCompSub = function(number, date, subscriberId, contact, projectId, description, errorTypeId, cause, measure, finished, justification, cost, notes, status, solver){
  return new Promise((resolve,reject)=>{
    let params = [number, date];
    let i = 2;
    let subscriberIdQuery = '', subscriberIdSet = '';
    if(subscriberId){
      subscriberIdQuery = ', subscriber_id';
      subscriberIdSet = ', $' + ++i;
      params.push(subscriberId);
    }
    let contactQuery = '', contactSet = '';
    if(contact){
      contactQuery = ', subscriber_contact';
      contactSet = ', $' + ++i;
      params.push(contact);
    }
    let projectIdQuery = '', projectIdSet = '';
    if(projectId){
      projectIdQuery = ', project_id';
      projectIdSet = ', $' + ++i;
      params.push(projectId);
    }
    let descriptionQuery = '', descriptionSet = '';
    if(description){
      descriptionQuery = ', description';
      descriptionSet = ', $' + ++i;
      params.push(description);
    }
    let errorTypeIdQuery = '', errorTypeIdSet = '';
    if(errorTypeId){
      errorTypeIdQuery = ', error_type_id';
      errorTypeIdSet = ', $' + ++i;
      params.push(errorTypeId);
    }
    let causeQuery = '', causeSet = '';
    if(cause){
      causeQuery = ', cause';
      causeSet = ', $' + ++i;
      params.push(cause);
    }
    let measureQuery = '', measureSet = '';
    if(measure){
      measureQuery = ', measure';
      measureSet = ', $' + ++i;
      params.push(measure);
    }
    let finishedQuery = '', finishedSet = '';
    if(finished){
      finishedQuery = ', finished';
      finishedSet = ', $' + ++i;
      params.push(finished);
    }
    let justificationQuery = '', justificationSet = '';
    if(justification){
      justificationQuery = ', justification_id';
      justificationSet = ', $' + ++i;
      params.push(justification);
    }
    let costQuery = '', costSet = '';
    if(cost){
      costQuery = ', cost';
      costSet = ', $' + ++i;
      params.push(cost);
    }
    let notesQuery = '', notesSet = '';
    if(notes){
      notesQuery = ', notes';
      notesSet = ', $' + ++i;
      params.push(notes);
    }
    let statusQuery = '', statusSet = '';
    if(status){
      statusQuery = ', status_id';
      statusSet = ', $' + ++i;
      params.push(status);
    }
    let solverQuery = '', solverSet = '';
    if(solver){
      solverQuery = ', solver';
      solverSet = ', $' + ++i;
      params.push(solver);
    }
    let query = `INSERT INTO complaints_subscribers(complaint_number, date` + subscriberIdQuery + contactQuery + projectIdQuery + descriptionQuery + errorTypeIdQuery + causeQuery + measureQuery + finishedQuery + justificationQuery + costQuery + notesQuery + statusQuery + solverQuery + `)
    VALUES ($1, $2` + subscriberIdSet + contactSet + projectIdSet + descriptionSet + errorTypeIdSet + causeSet + measureSet + finishedSet + justificationSet + costSet + notesSet + statusSet + solverSet + `)
    RETURNING *`;
    
    client.query(query,params,(err,res)=>{
      if(err) return reject(err);
      return resolve(res.rows[0]);
    })
  })
}
//update complaint subscriber
module.exports.updateCompSub = function(compSubId, number, date, subscriberId, contact, projectId, description, errorTypeId, cause, measure, finished, justification, cost, notes, solver){
  return new Promise((resolve,reject)=>{
    let query = `UPDATE complaints_subscribers
    SET (complaint_number ,date, subscriber_id, subscriber_contact, project_id, description, error_type_id, cause, measure, finished, justification_id, cost, notes, solver) = ($2, $3, $4, $5, $6, $7, $8, $9, $10, $11, $12, $13, $14, $15)
    WHERE id = $1
    RETURNING *`;
    if (!subscriberId)
      subscriberId = null;
    if (!contact)
      contact = null;
    if (!projectId)
      projectId = null;
    if (!description)
      description = null;
    if (!errorTypeId)
      errorTypeId = null;
    if (!cause)
      cause = null;
    if (!measure)
      measure = null;
    if (!finished)
      finished = null;
    if (!justification)
      justification = null;
    if (!cost)
      cost = null;
    if (!notes)
      notes = null;
    if (!solver)
      solver = null;
    let params = [compSubId, number, date, subscriberId, contact, projectId, description, errorTypeId, cause, measure, finished, justification, cost, notes, solver];
    
    client.query(query,params,(err,res)=>{
      if(err) return reject(err);
      return resolve(res.rows[0]);
    })
  })
}
// //Add contact users for resolve to the complaint subscriber
// module.exports.addUserToCompSub = function(userId, compSubId){
//   return new Promise((resolve,reject)=>{
//     let query = `INSERT INTO complaints_subs_users(user_id, complaint_sub_id)
//     VALUES ($1,$2)
//     RETURNING *`;
//     let params = [userId, compSubId];
    
//     client.query(query,params,(err,res)=>{
//       if(err) return reject(err);
//       return resolve(res.rows[0]);
//     })
//   })
// }
// //Delete contact users for resolve to the complaint subscriber
// module.exports.deleteUserToCompSub = function(userId, compSubId){
//   return new Promise((resolve,reject)=>{
//     let query = `DELETE FROM complaints_subs_users
//     WHERE user_id = $1 AND complaint_sub_id = $2`;
//     let params = [userId, compSubId];
    
//     client.query(query,params,(err,res)=>{
//       if(err) return reject(err);
//       return resolve(res.rows[0]);
//     })
//   })
// }
//delete comp sub (update active attribute of comp sub)
module.exports.deleteCompSub = function(compSubId, active){
  return new Promise((resolve,reject)=>{
    let query = `UPDATE complaints_subscribers
    SET active = $2
    WHERE id = $1
    RETURNING *`;
    let params = [compSubId, active];
    
    client.query(query,params,(err,res)=>{
      if(err) return reject(err);
      return resolve(res.rows[0]);
    })
  })
}
////////////////////////////////////////////////////////////////////////////////////COMPLAINTS SUPPLIERS - REKLAMACIJA DOBAVITELJEM
//update comp sup status
module.exports.updateCompSupStatus = function(compSupId, statusId){
  return new Promise((resolve,reject)=>{
    let query = `UPDATE complaints_suppliers
    SET status_id = $2
    WHERE id = $1
    RETURNING *`;
    let params = [compSupId, statusId];
    
    client.query(query,params,(err,res)=>{
      if(err) return reject(err);
      return resolve(res.rows[0]);
    })
  })
}
//get complaint supplier
module.exports.getComplaintsSupplierId = function(id){
  return new Promise((resolve,reject)=>{
    let query = `SELECT cs.id, complaint_number, date, supplier, material, complaint_type, quantity, value, active, fc.count as files_count, css.id as status_id, css.status as status, cj.id as justification_id, cj.justification as justification
    FROM complaints_suppliers as cs
    LEFT JOIN ( SELECT comp_sup_id, count(comp_sup_id)
                FROM complaints_files
                WHERE deleted = false
                GROUP BY comp_sup_id) AS fc ON cs.id = fc.comp_sup_id
    LEFT JOIN complaints_status css on css.id = cs.status_id
    LEFT JOIN complaints_justification cj on cj.id = cs.justification_id
    WHERE cs.id = $1`;
    let params = [id];

    client.query(query, params, (err,res)=>{
      if(err) return reject(err);
      return resolve(res.rows[0]);
    })
  })
}
//get complaint supplier author
module.exports.getComplaintsSupplierAuthor = function(id){
  return new Promise((resolve,reject)=>{
    let query = `SELECT u.name, u.surname, date, date
    FROM changes_system as cs
    LEFT JOIN "user" u on u.id = cs."from"
    WHERE id_comp_sup = $1 AND
          status = 1
    ORDER BY cs.id`;
    let params = [id];

    client.query(query, params, (err,res)=>{
      if(err) return reject(err);
      return resolve(res.rows[0]);
    })
  })
}
//get complaints for suppliers
module.exports.getComplaintsSuppliers = function(activity){
  return new Promise((resolve,reject)=>{
    let complaintsActivity;
    switch(activity) {
      case '0': complaintsActivity = 'active'; break;
      case '1': complaintsActivity = 'true'; break;
      case '2': complaintsActivity = 'false'; break;
      default: complaintsActivity = 'true';
    }
    let query = `SELECT cs.id, complaint_number, date, supplier, material, complaint_type, quantity, value, active, fc.count as files_count, css.id as status_id, css.status as status, cj.id as justification_id, cj.justification as justification
    FROM complaints_suppliers as cs
    LEFT JOIN ( SELECT comp_sup_id, count(comp_sup_id)
                FROM complaints_files
                WHERE deleted = false
                GROUP BY comp_sup_id) AS fc ON cs.id = fc.comp_sup_id
    LEFT JOIN complaints_status css on css.id = cs.status_id
    LEFT JOIN complaints_justification cj on cj.id = cs.justification_id
    WHERE active = ` + complaintsActivity + `
    ORDER BY cs.id`;
    //let params = [activitiesActivity];

    client.query(query,(err,res)=>{
      if(err) return reject(err);
      return resolve(res.rows);
    })
  })
}
//Add new complaint sup
module.exports.addCompSup = function(number, date, supplier, material, compType, quantity, value, status, justification){
  return new Promise((resolve,reject)=>{
    let params = [number, date];
    let i = 2;
    let supplierQuery = '', supplierSet = '';
    if(supplier){
      supplierQuery = ', supplier';
      supplierSet = ', $' + ++i;
      params.push(supplier);
    }
    let materialQuery = '', materialSet = '';
    if(material){
      materialQuery = ', material';
      materialSet = ', $' + ++i;
      params.push(material);
    }
    let compTypeQuery = '', compTypeSet = '';
    if(compType){
      compTypeQuery = ', complaint_type';
      compTypeSet = ', $' + ++i;
      params.push(compType);
    }
    let quantityQuery = '', quantitySet = '';
    if(quantity){
      quantityQuery = ', quantity';
      quantitySet = ', $' + ++i;
      params.push(quantity);
    }
    let valueQuery = '', valueIdSet = '';
    if(value){
      valueQuery = ', value';
      valueIdSet = ', $' + ++i;
      params.push(value);
    }
    let statusQuery = '', statusSet = '';
    if(status){
      statusQuery = ', status_id';
      statusSet = ', $' + ++i;
      params.push(status);
    }
    let justificationQuery = '', justificationSet = '';
    if(justification){
      justificationQuery = ', justification_id';
      justificationSet = ', $' + ++i;
      params.push(justification);
    }
    let query = `INSERT INTO complaints_suppliers(complaint_number, date` + supplierQuery + materialQuery + compTypeQuery + quantityQuery + valueQuery + statusQuery + justificationQuery + `)
    VALUES ($1, $2` + supplierSet + materialSet + compTypeSet + quantitySet + valueIdSet + statusSet + justificationSet + `)
    RETURNING *`;
    
    client.query(query,params,(err,res)=>{
      if(err) return reject(err);
      return resolve(res.rows[0]);
    })
  })
}
//update complaint supplier
module.exports.updateCompSup = function(compSubId, number, date, supplier, material, compType, quantity, value, status, justification){
  return new Promise((resolve,reject)=>{
    let query = `UPDATE complaints_suppliers
    SET (complaint_number, date, supplier, material, complaint_type, quantity, value, status, justification_id) = ($2, $3, $4, $5, $6, $7, $8, $9, $10) 
    WHERE id = $1
    RETURNING *`;
    if (!material)
      material = null;
    if (!compType)
      compType = null;
    if (!quantity)
      quantity = null;
    if (!value)
      value = null;
    if (!status)
      status = null;
    if (!justification)
      justification = null;
    let params = [compSubId, number, date, supplier, material, compType, quantity, value, status, justification];
    
    client.query(query,params,(err,res)=>{
      if(err) return reject(err);
      return resolve(res.rows[0]);
    })
  })
}
//delete comp sup (update active attribute of comp sup)
module.exports.deleteCompSup = function(compSupId, active){
  return new Promise((resolve,reject)=>{
    let query = `UPDATE complaints_suppliers
    SET active = $2
    WHERE id = $1
    RETURNING *`;
    let params = [compSupId, active];
    
    client.query(query,params,(err,res)=>{
      if(err) return reject(err);
      return resolve(res.rows[0]);
    })
  })
}
////////////////////////////////////////////////////////////////////////////////////COMPLAINTS FILES
// based on id get files either for complaints subscriber or complaints supplier
module.exports.getComplaintFiles = function(compSubFileId, compSupFileId){
  return new Promise((resolve,reject)=>{
    let query, params;
    if (compSubFileId){
      query = `SELECT cf.id, original_name, path_name, comp_sub_id, comp_sup_id, author, name, surname, date, type, note, cf.active, deleted
      FROM complaints_files as cf, "user"
      WHERE comp_sub_id = $1 AND
            author = "user".id AND
            deleted = false
      ORDER BY date desc`;
      params = [compSubFileId];
    }
    else{
      query = `SELECT cf.id, original_name, path_name, comp_sub_id, comp_sup_id, author, name, surname, date, type, note, cf.active, deleted
      FROM complaints_files as cf, "user"
      WHERE comp_sup_id = $1 AND
            author = "user".id AND
            deleted = false
      ORDER BY date desc`;
      params = [compSupFileId];
    }

    client.query(query,params,(err,res)=>{
      if(err) return reject(err);
      return resolve(res.rows);
    })
  })
}
//Add new file for complaint (complaint subscriber or complaint supplier)
module.exports.addFileForComplaint = function(fileName, pathName, userId, type, compSubFileId, compSupFileId){
  return new Promise((resolve,reject)=>{
    let query, params;
    if (compSubFileId){
      query = `INSERT INTO complaints_files(original_name, path_name, author, type, comp_sub_id)
      VALUES ($1, $2, $3, $4, $5)
      RETURNING *`;
      params = [fileName, pathName, userId, type, compSubFileId];
    }
    else{
      query = `INSERT INTO complaints_files(original_name, path_name, author, type, comp_sup_id)
      VALUES ($1, $2, $3, $4, $5)
      RETURNING *`;
      params = [fileName, pathName, userId, type, compSupFileId];
    }
    
    client.query(query,params,(err,res)=>{
      if(err) return reject(err);
      return resolve(res.rows[0]);
    })
  })
}
//get complaint file info
module.exports.getComplaintFileInfo = function(fileName){
  return new Promise((resolve,reject)=>{
    let query = `SELECT cf.id, original_name, path_name, author, date, type, note, cf.active, concat(name, ' ', surname) as user_name, deleted
    FROM complaints_files cf
    LEFT JOIN "user" u on cf.author = u.id
    WHERE cf.path_name = $1`;
    let params = [fileName]
    
    client.query(query,params,(err,res)=>{
      if(err) return reject(err);
      return resolve(res.rows[0]);
    })
  })
}
//update file active to false, file no longer exist
module.exports.updateActiveFile = function(filename){
  return new Promise((resolve,reject)=>{
    let query = `UPDATE complaints_files
    SET active = false
    WHERE path_name = $1`;
    let params = [filename];
    
    client.query(query,params,(err,res)=>{
      if(err) return reject(err);
      return resolve(res.rows);
    })
  })
}
//get file info
module.exports.getFile = function(fileId){
  return new Promise((resolve,reject)=>{
    let query = `SELECT cf.id, original_name, path_name, author, date, type, note, cf.active, concat(name, ' ', surname) as user_name, deleted
    FROM complaints_files cf
    LEFT JOIN "user" u on cf.author = u.id
    WHERE cf.id = $1`;
    let params = [fileId];

    client.query(query,params,(err,res)=>{
      if(err) return reject(err);
      return resolve(res.rows[0]);
    })
  })
}
//mark file as deleted after it was successfully deleted
module.exports.deleteFile = function(id){
  return new Promise((resolve,reject)=>{
    let query = `UPDATE complaints_files
    SET deleted = true
    WHERE id = $1
    RETURNING *`;
    let params = [id];

    client.query(query,params,(err,res)=>{
      if(err) return reject(err);
      return resolve(res.rows[0]);
    })
  })
}
////////////////////////////////////////////////////////////CHANGES FOR COMPLAINTS SUBSCRIBER AND SUPPLIER
//get complaints changes system
module.exports.getCompChangesSystem = function(compSubId, compSupId){
  return new Promise((resolve,reject)=>{
    let query = '';
    let params = [];
    if (compSubId){
      query = `SELECT c.id, c.date, "from", u.name, surname, cs.id as id_status, cs.status, c.type, s.status as comp_status
      FROM changes_system as c
      LEFT JOIN "user" u on c."from" = u.id
      LEFT JOIN change_status cs on c.status = cs.id
      LEFT JOIN complaints_status s on c.id_comp_status = s.id
      WHERE id_comp_sub = $1
      ORDER BY date`;
      params = [compSubId];
    }
    else{
      query = `SELECT c.id, c.date, "from", u.name, surname, cs.id as id_status, cs.status, c.type, s.status as comp_status
      FROM changes_system as c
      LEFT JOIN "user" u on c."from" = u.id
      LEFT JOIN change_status cs on c.status = cs.id
      LEFT JOIN complaints_status s on c.id_comp_status = s.id
      WHERE id_comp_sup = $1
      ORDER BY date`;
      params = [compSupId];
    }
    
    client.query(query,params,(err,res)=>{
      if(err) return reject(err);
      return resolve(res.rows);
    })
  })
}