const { Client } = require('pg');

//console.log(process.env.DB_HOST);

const client = new Client({
  user: process.env.DB_USERNAME,
  host: process.env.DB_HOST,
  database: process.env.DB_DATABASE,
  password: process.env.DB_PASSWORD,
  port: process.env.DB_PORT
})
client.connect();

//Get subtasks data
module.exports.getSubTask = function(id){
  return new Promise((resolve,reject)=>{
    let params = [id];
    let query = `SELECT s.id, s.id_task, name, s.note, completed, active, nc.count as notes_count
    FROM subtask s
    LEFT JOIN ( SELECT id_subtask, count(id_subtask)
                FROM notes
                GROUP BY id_subtask ) AS nc ON s.id = nc.id_subtask
    WHERE s.id = $1`;

    client.query(query,params,(err,res)=>{
      if(err) return reject(err);
      return resolve(res.rows[0]);
    })
  })
}
//Get task's subtasks
module.exports.getSubTasksById = function(id,active){
  return new Promise((resolve,reject)=>{
    let params = [id];
    let query = `SELECT s.id, s.id_task, name, s.note, completed, active, nc.count as notes_count
    FROM subtask s
    LEFT JOIN ( SELECT id_subtask, count(id_subtask)
                FROM notes
                GROUP BY id_subtask ) AS nc ON s.id = nc.id_subtask
    WHERE s.id_task = $1
    ORDER BY id`;

    client.query(query,params,(err,res)=>{
      if(err) return reject(err);
      return resolve(res.rows);
    })
  })
}
//Post subtask completion (true/false)
module.exports.isSubtaskCompleted = function(id, completed){
  return new Promise((resolve,reject)=>{
    let query = `UPDATE subtask
    SET completed = $2
    WHERE id = $1
    RETURNING *`;
    let params = [id, completed];

    client.query(query,params,(err,res)=>{
      if(err) return reject(err);
      return resolve(res.rows[0]);
    })
  })
}
//Delete subtask by id
module.exports.deleteSubtask = function(id,active){
  return new Promise((resolve,reject)=>{
    let params = [id];
    let activeSet = false;
    if(active)
      activeSet = active;
    params.push(activeSet);
    let query = `UPDATE subtask
    SET active = $2
    WHERE id = $1`;

    client.query(query,params,(err,res)=>{
      if(err) return reject(err);
      return resolve(res.rows);
    })
  })
}
//Create subtask
module.exports.createSubtask = function(taskId, name){
  return new Promise((resolve,reject)=>{
    let query = `INSERT INTO subtask(id_task ,name)
    VALUES ($1, $2)
    RETURNING id`;
    let params = [taskId, name];

    client.query(query,params,(err,res)=>{
      if(err) return reject(err);
      return resolve(res.rows[0]);
    })
  })
}
//Update subtask's note
module.exports.updateSubtask = function(id, name){
  return new Promise((resolve,reject)=>{
    let query = `UPDATE subtask
    SET name = $2
    WHERE id = $1`;
    let params = [id, name];

    client.query(query,params,(err,res)=>{
      if(err) return reject(err);
      return resolve(res.rows);
    })
  })
}
//Update subtask's note
module.exports.updateSubtaskNote = function(id, note){
  return new Promise((resolve,reject)=>{
    let query = `UPDATE subtask
    SET note = $2
    WHERE id = $1`;
    let params = [id, note];

    client.query(query,params,(err,res)=>{
      if(err) return reject(err);
      return resolve(res.rows);
    })
  })
}