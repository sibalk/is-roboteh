const { Client } = require('pg');

//console.log(process.env.DB_HOST);

const client = new Client({
  user: process.env.DB_USERNAME,
  host: process.env.DB_HOST,
  database: process.env.DB_DATABASE,
  password: process.env.DB_PASSWORD,
  port: process.env.DB_PORT
})
client.connect();

//Get users by id, name and surname
module.exports.getFeedback = function(){
  return new Promise((resolve,reject)=>{
    let query = `SELECT *
    FROM feedback
    ORDER BY created`;

    client.query(query,(err,res)=>{
      if(err) return reject(err);
      return resolve(res.rows);
    })
  })
}
//Create feedback
module.exports.createFeedback = function(feedback, optional){
  return new Promise((resolve,reject)=>{
    let query = `INSERT INTO feedback(feedback, optional)
    VALUES ($1, $2)`;
    let params = [feedback, optional];

    client.query(query,params,(err,res)=>{
      if(err) return reject(err);
      return resolve(res.rows);
    })
  })
}
//Handle feedback, mark feedback as handled
module.exports.handleFeedback = function(feedbackId, handled){
  return new Promise((resolve,reject)=>{
    let query = `UPDATE feedback
    SET handled = $2
    WHERE id = $1`;
    let params = [feedbackId, handled];

    client.query(query,params,(err,res)=>{
      if(err) return reject(err);
      return resolve(res.rows);
    })
  })
}