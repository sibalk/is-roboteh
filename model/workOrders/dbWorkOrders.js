const { Client } = require('pg');

//console.log(process.env.DB_HOST);

const client = new Client({
  user: process.env.DB_USERNAME,
  host: process.env.DB_HOST,
  database: process.env.DB_DATABASE,
  password: process.env.DB_PASSWORD,
  port: process.env.DB_PORT
})
client.connect();

//get last 100 reports for user 
module.exports.getLastWorkOrders = function(userId, role){
  return new Promise((resolve,reject)=>{
    let params = [];
    let query;
    let adminQuery = 'true';
    if(role == 'admin')
      adminQuery = 'wo.active';
    if(userId){
      params.push(userId)
      query = `SELECT wo.id, wo.active, subscriber_id, s.name as subscriber_name, project_id, project_name, number, date, location, description, vehicle, arrival, departure, work_order_type_id, work_order_type, user_id, representative, concat("left"(u.name,1), "left"(surname,1),'-',wo.id,'/',date_part('year', date)) as wo_number, user_id = $1 as author, status_id, status, location_cord, task_id, contact_id, notification_mail, office_mail, email
      FROM work_order as wo
      LEFT JOIN work_order_type wot on wo.work_order_type_id = wot.id
      LEFT JOIN work_order_workers wow on wo.id = wow.work_order_id
      LEFT JOIN project p on wo.project_id = p.id
      LEFT JOIN subscriber s on wo.subscriber_id = s.id
      LEFT JOIN "user" u on wo.user_id = u.id
      LEFT JOIN work_order_status wos on wo.status_id = wos.id
      WHERE (wow.worker_id = $1 OR wo.user_id = $1) AND
            wo.active = `+adminQuery+`
      GROUP BY wo.id, s.name, project_name, work_order_type, u.name, surname, wos.status
      ORDER BY id desc LIMIT 100`;
    }
    else{
      query = `SELECT wo.id, wo.active, subscriber_id, s.name as subscriber_name, project_id, project_name, number, date, location, description, vehicle, arrival, departure, work_order_type_id, work_order_type, user_id, representative,concat("left"(u.name,1), "left"(surname,1),'-',wo.id,'/',date_part('year', date)) as wo_number, status_id, status, location_cord, task_id, contact_id, notification_mail, office_mail, email
      FROM work_order as wo
      LEFT JOIN work_order_type wot on wo.work_order_type_id = wot.id
      LEFT JOIN project p on wo.project_id = p.id
      LEFT JOIN subscriber s on wo.subscriber_id = s.id
      LEFT JOIN "user" u on wo.user_id = u.id
      LEFT JOIN work_order_status wos on wo.status_id = wos.id
      WHERE wo.active = `+adminQuery+`
      ORDER BY id desc LIMIT 100`;
    }

    client.query(query,params, (err,res)=>{
      if(err) return reject(err);
      return resolve(res.rows);
    })
  })
};
//get selected work order
module.exports.getWorkOrder = function(workOrderId){
  return new Promise((resolve,reject)=>{
    let params = [workOrderId];
    let query = `SELECT wo.id, subscriber_id, s.name as subscriber_name, project_id, project_name, number, date, location, description, vehicle, arrival, departure, work_order_type_id, work_order_type, user_id, representative,concat("left"(u.name,1), "left"(surname,1),'-',wo.id,'/',date_part('year', date)) as wo_number, status_id, status, location_cord, task_id, concat(u.name, ' ', u.surname) as user_name, contact_id, full_name as contact_name, phone_number as contact_phone, notification_mail, office_mail, email
    FROM work_order as wo
    LEFT JOIN work_order_type wot on wo.work_order_type_id = wot.id
    LEFT JOIN project p on wo.project_id = p.id
    LEFT JOIN subscriber s on wo.subscriber_id = s.id
    LEFT JOIN "user" u on wo.user_id = u.id
    LEFT JOIN work_order_status wos on wo.status_id = wos.id
    LEFT JOIN work_order_contact woc on wo.contact_id = woc.id
    WHERE wo.id = $1`;

    client.query(query,params, (err,res)=>{
      if(err) return reject(err);
      return resolve(res.rows[0]);
    })
  })
};
//get reports for user 
module.exports.getUserWorkOrders = function(userId,start,finish,projectId,subscriberId,typeId,role,statusId){
  return new Promise((resolve,reject)=>{
    let params = [];
    let query;
    let userQuery = '';
    let authorSelect = '';
    let projectQuery = '';
    let subscriberQuery = '';
    let typeQuery = '';
    let statusQuery = '';
    let startQuery = 'wo.date';
    let finishQuery = 'wo.date';
    let limitQuery = '';
    if (!start || !finish){
      limitQuery = 'LIMIT 100';
    }
    let i = 1;
    if (start){
      startQuery = '$' + i;
      i++;
      params.push(start);
    }
    if (finish){
      finishQuery = '$' + i;
      i++;
      params.push(finish);
    }
    if(userId){
      userQuery = ' AND (wow.worker_id = $' + i + ' OR wo.user_id = $' + i + ')';
      authorSelect = ', user_id = $' + i + ' as author';
      i++;
      params.push(userId);
    }
    if(projectId){
      projectQuery = ' AND wo.project_id = $' + i;
      i++;
      params.push(projectId);
    }
    if(subscriberId){
      subscriberQuery = ' AND wo.subscriber_id = $' + i;
      i++;
      params.push(subscriberId);
    }
    if(typeId){
      typeQuery = ' AND wo.work_order_type_id = $' + i;
      i++;
      params.push(typeId);
    }
    if(statusId){
      statusQuery = ' AND wo.status_id = $' + i;
      i++;
      params.push(statusId);
    }
    let adminQuery = 'true';
    if(role == 'admin')
      adminQuery = 'wo.active';
    query = `SELECT wo.id, wo.active, subscriber_id, s.name as subscriber_name, project_id, project_name, number, date, location, description, vehicle, arrival, departure, work_order_type_id, work_order_type, user_id, representative, concat("left"(u.name,1), "left"(surname,1),'-',wo.id,'/',date_part('year', date)) as wo_number`+authorSelect+`, status_id, status, location_cord, task_id, contact_id, notification_mail, office_mail, email
      FROM work_order as wo
      LEFT JOIN work_order_type wot on wo.work_order_type_id = wot.id
      LEFT JOIN work_order_workers wow on wo.id = wow.work_order_id
      LEFT JOIN project p on wo.project_id = p.id
      LEFT JOIN subscriber s on wo.subscriber_id = s.id
      LEFT JOIN "user" u on wo.user_id = u.id
      LEFT JOIN work_order_status wos on wo.status_id = wos.id
      WHERE wo.active = `+adminQuery+` AND
            (wo.date::date >= `+startQuery+`::date AND wo.date::date <= `+finishQuery+`::date)`+userQuery+projectQuery+subscriberQuery+typeQuery+statusQuery+`
      GROUP BY wo.id, s.name, project_name, work_order_type, u.name, surname, wos.status
      ORDER BY id desc
      ` + limitQuery;

      client.query(query,params, (err,res)=>{
        if(err) return reject(err);
        return resolve(res.rows);
      })
  })
};
//get work order workers
module.exports.getWorkOrderWorkers = function(workOrderId){
  return new Promise((resolve,reject)=>{
    let params = [workOrderId];
    let query = `SELECT wow.id, worker_id, work_order_id, name, surname
    FROM work_order_workers as wow
    LEFT JOIN "user" u on wow.worker_id = u.id
    LEFT JOIN work_order wo on wow.work_order_id = wo.id
    WHERE wo.id = $1`;

    client.query(query,params, (err,res)=>{
      if(err) return reject(err);
      return resolve(res.rows);
    })    
  })
}
//get work order files
module.exports.getWorkOrderFiles = function(workOrderId){
  return new Promise((resolve,reject)=>{
    let query = `SELECT wof.id, original_name, path_name, user_id, date, type, note, wof.active, concat(name, ' ', surname) as user_name, deleted
    FROM work_order_files_keys as wofk
    LEFT JOIN work_order_files wof on wofk.work_order_file_id = wof.id
    LEFT JOIN "user" u on wof.user_id = u.id
    WHERE work_order_id = $1 AND
          deleted = false`;
    let params = [workOrderId]
    
    client.query(query,params,(err,res)=>{
      if(err) return reject(err);
      return resolve(res.rows);
    })
  })
}
//get work order file info
module.exports.getWorkOrderFileInfo = function(fileName){
  return new Promise((resolve,reject)=>{
    let query = `SELECT wof.id, original_name, path_name, user_id, date, type, note, wof.active, concat(name, ' ', surname) as user_name, deleted
    FROM work_order_files wof
    LEFT JOIN "user" u on wof.user_id = u.id
    WHERE wof.path_name = $1`;
    let params = [fileName]
    
    client.query(query,params,(err,res)=>{
      if(err) return reject(err);
      return resolve(res.rows[0]);
    })
  })
}
//add file to work order
module.exports.addWorkOrderFile = function(fileName, pathName, userId, type){
  return new Promise((resolve,reject)=>{
    let query = `INSERT INTO work_order_files(original_name, path_name, user_id, type)
    VALUES ($1, $2, $3, $4)
    RETURNING *`;
    let params = [fileName, pathName, userId, type];

    client.query(query,params,(err,res)=>{
      if(err) return reject(err);
      return resolve(res.rows[0]);
    })
  })
}
//add file for work order id(keys)
module.exports.addFile = function(woId, fileId){
  return new Promise((resolve,reject)=>{
    let query = `INSERT INTO work_order_files_keys(work_order_file_id, work_order_id)
    VALUES ($1,$2)`;
    let params = [fileId, woId];

    client.query(query,params,(err,res)=>{
      if(err) return reject(err);
      return resolve(res.rows);
    })
  })
}
//delete file for work order id
module.exports.deleteFiles = function(wowId){
  return new Promise((resolve,reject)=>{
    let query = `DELETE FROM work_order_files_keys
    WHERE work_order_id = $1`;
    let params = [wowId];

    client.query(query,params,(err,res)=>{
      if(err) return reject(err);
      return resolve(res.rows);
    })
  })
}
//update file active to false, file no longer exist
module.exports.updateActiveFile = function(filename){
  return new Promise((resolve,reject)=>{
    let query = `UPDATE work_order_files
    SET active = false
    WHERE path_name = $1`;
    let params = [filename];
    
    client.query(query,params,(err,res)=>{
      if(err) return reject(err);
      return resolve(res.rows);
    })
  })
}
//get work order signs
module.exports.getWorkOrderSigns = function(workOrderId) {
  return new Promise((resolve,reject)=>{
    let query = `SELECT wos.id, original_name, path_name, user_id, date, type, note, active
    FROM work_order_signs_keys as wosk
    LEFT JOIN work_order_signs wos on wosk.work_order_sign_id = wos.id
    WHERE work_order_id = $1`;
    let params = [workOrderId]
    
    client.query(query,params,(err,res)=>{
      if(err) return reject(err);
      return resolve(res.rows);
    })
  })
}
//get file info
module.exports.getFile = function(fileId){
  return new Promise((resolve,reject)=>{
    let query = `SELECT wof.id, original_name, path_name, user_id, date, type, note, wof.active, concat(name, ' ', surname) as user_name, deleted
    FROM work_order_files wof
    LEFT JOIN "user" u on wof.user_id = u.id
    WHERE wof.id = $1`;
    let params = [fileId];

    client.query(query,params,(err,res)=>{
      if(err) return reject(err);
      return resolve(res.rows[0]);
    })
  })
}
//mark file as deleted after it was successfully deleted
module.exports.deleteFile = function(id){
  return new Promise((resolve,reject)=>{
    let query = `UPDATE work_order_files
    SET deleted = true
    WHERE id = $1
    RETURNING *`;
    let params = [id];

    client.query(query,params,(err,res)=>{
      if(err) return reject(err);
      return resolve(res.rows[0]);
    })
  })
}
/*
//get work order expenses
module.exports.getWorkOrderExpenses = function(workOrderId){
  let params = [workOrderId];
  let query = `SELECT *
  FROM work_order_expenses
  WHERE work_order_id = $1`;
  return client.query(query,params)
  .then(res => {return res.rows;})
  .catch(e => {return e;})
}
*/
module.exports.getWorkOrderExpenses = function(workOrderId){
  return new Promise((resolve, reject)=>{
    let query = `SELECT *
    FROM work_order_expenses
    WHERE work_order_id = $1`;
    let params = [workOrderId];

    client.query(query,params, (err,res)=>{
      if(err) return reject(err);
      return resolve(res.rows);
    })
  })
};
//add work order expenses
module.exports.addWorkOrderExpenses = function(workOrderId,timeQuantity,timePrice,timeSum,distanceQuantity,distancePrice,distanceSum,materialQuantity,materialPrice,materialSum,otherQuantity,otherPrice,otherSum){
  return new Promise((resolve,reject)=>{
    let params = [workOrderId,timeQuantity,timePrice,timeSum,distanceQuantity,distancePrice,distanceSum,materialQuantity,materialPrice,materialSum,otherQuantity,otherPrice,otherSum];
    let query = `INSERT INTO work_order_expenses(work_order_id, time_quantity, time_price, time_sum, distance_quantity, distance_price, distance_sum, material_quantity, material_price, material_sum, other_quantity, other_price, other_sum)
    VALUES ($1,$2,$3,$4,$5,$6,$7,$8,$9,$10,$11,$12,$13)
    RETURNING id`;

    client.query(query,params,(err,res)=>{
      if(err) return reject(err);
      return resolve(res.rows);
    })
  })
}
//update work order expenses
module.exports.updateWorkOrderExpenses = function(woeId,timeQuantity,timePrice,timeSum,distanceQuantity,distancePrice,distanceSum,materialQuantity,materialPrice,materialSum,otherQuantity,otherPrice,otherSum){
  return new Promise((resolve,reject)=>{
    let params = [woeId,timeQuantity,timePrice,timeSum,distanceQuantity,distancePrice,distanceSum,materialQuantity,materialPrice,materialSum,otherQuantity,otherPrice,otherSum];
    let query = `UPDATE work_order_expenses
    SET (time_quantity, time_price, time_sum, distance_quantity, distance_price, distance_sum, material_quantity, material_price, material_sum, other_quantity, other_price, other_sum) = ($2,$3,$4,$5,$6,$7,$8,$9,$10,$11,$12,$13)
    WHERE work_order_id = $1`;

    client.query(query,params,(err,res)=>{
      if(err) return reject(err);
      return resolve(res.rows);
    })
  })
}
//get work order materials
module.exports.getWorkOrderMaterial = function(workOrderId){
  return new Promise((resolve,reject)=>{
    let params = [workOrderId];
    let query = `SELECT *
    FROM work_order_material
    WHERE work_order_id = $1`;

    client.query(query,params,(err,res)=>{
      if(err) return reject(err);
      return resolve(res.rows);
    })
  })
}
//add work order materials
module.exports.addWorkOrderMaterials = function(workOrderId,name1,quantity1,price1,sum1,name2,quantity2,price2,sum2,name3,quantity3,price3,sum3,name4,quantity4,price4,sum4,name5,quantity5,price5,sum5,name6,quantity6,price6,sum6,name7,quantity7,price7,sum7){
  return new Promise((resolve,reject)=>{
    let params = [workOrderId,name1,quantity1,price1,sum1,name2,quantity2,price2,sum2,name3,quantity3,price3,sum3,name4,quantity4,price4,sum4,name5,quantity5,price5,sum5,name6,quantity6,price6,sum6,name7,quantity7,price7,sum7];
    let query = `INSERT INTO work_order_material(work_order_id,name1,quantity1,price1,sum1,name2,quantity2,price2,sum2,name3,quantity3,price3,sum3,name4,quantity4,price4,sum4,name5,quantity5,price5,sum5,name6,quantity6,price6,sum6,name7,quantity7,price7,sum7)
    VALUES ($1,$2,$3,$4,$5,$6,$7,$8,$9,$10,$11,$12,$13,$14,$15,$16,$17,$18,$19,$20,$21,$22,$23,$24,$25,$26,$27,$28,$29)
    RETURNING id`;

    client.query(query,params,(err,res)=>{
      if(err) return reject(err);
      return resolve(res.rows);
    })
  })
}
//update work order materials
module.exports.updateWorkOrderMaterials = function(workOrderId,name1,quantity1,price1,sum1,name2,quantity2,price2,sum2,name3,quantity3,price3,sum3,name4,quantity4,price4,sum4,name5,quantity5,price5,sum5,name6,quantity6,price6,sum6,name7,quantity7,price7,sum7){
  return new Promise((resolve,reject)=>{
    let params = [workOrderId,name1,quantity1,price1,sum1,name2,quantity2,price2,sum2,name3,quantity3,price3,sum3,name4,quantity4,price4,sum4,name5,quantity5,price5,sum5,name6,quantity6,price6,sum6,name7,quantity7,price7,sum7];
    let query = `UPDATE work_order_material
    SET (name1,quantity1,price1,sum1,name2,quantity2,price2,sum2,name3,quantity3,price3,sum3,name4,quantity4,price4,sum4,name5,quantity5,price5,sum5,name6,quantity6,price6,sum6,name7,quantity7,price7,sum7) = ($2,$3,$4,$5,$6,$7,$8,$9,$10,$11,$12,$13,$14,$15,$16,$17,$18,$19,$20,$21,$22,$23,$24,$25,$26,$27,$28,$29)
    WHERE work_order_id = $1
    RETURNING *`;

    client.query(query,params,(err,res)=>{
      if(err) return reject(err);
      return resolve(res.rows[0]);
    })
  })
}
//get all work order types (id,text)
module.exports.getWorkOrderTypes = function(){
  return new Promise((resolve,reject)=>{
    let query = `SELECT id, work_order_type as text
    FROM work_order_type`;
    
    client.query(query,(err,res)=>{
      if(err) return reject(err);
      return resolve(res.rows);
    })
  })
}
//get all work order status (id,text)
module.exports.getWorkOrderStatus = function(){
  return new Promise((resolve, reject)=>{
    let query = `SELECT id, status as text
    FROM work_order_status`;

    client.query(query, (err,res)=>{
      if(err) return reject(err);
      return resolve(res.rows);
    })
  })
}
//update work order status
module.exports.updateWorkOrderStatus = function(woId, statusId){
  return new Promise((resolve, reject)=>{
    let query = `UPDATE work_order
    SET status_id = $2
    WHERE id = $1`;
    let params = [woId, statusId];

    client.query(query,params, (err,res)=>{
      if(err) return reject(err);
      return resolve(res.rows);
    })
  })
}
//add new report types (id,text)
module.exports.addReportsType = function(type){
  return new Promise((resolve,reject)=>{
    let query = `INSERT INTO report_type(report_type)
    VALUES ($1)
    RETURNING id`;
    let params = [type]
    
    client.query(query,params,(err,res)=>{
      if(err) return reject(err);
      return resolve(res.rows[0]);
    })
  })
}
//delete work order (update active to false)
module.exports.deleteWorkOrder = function(woId, active){
  return new Promise((resolve,reject)=>{
    let query = `UPDATE work_order
    SET active = $2
    WHERE id = $1`;
    let params = [woId,active];

    client.query(query,params,(err,res)=>{
      if(err) return reject(err);
      return resolve(res.rows);
    })
  })
}
//add new worker for work order id
module.exports.addWorker = function(woId, userId){
  return new Promise((resolve,reject)=>{
    let query = `INSERT INTO work_order_workers(worker_id, work_order_id)
    VALUES ($1,$2)`;
    let params = [userId, woId];

    client.query(query,params,(err,res)=>{
      if(err) return reject(err);
      return resolve(res.rows);
    })
  })
}
//add new worker for work order id
module.exports.deleteWorker = function(wowId){
  return new Promise((resolve,reject)=>{
    let query = `DELETE FROM work_order_workers
    WHERE id = $1`;
    let params = [wowId];

    client.query(query,params,(err,res)=>{
      if(err) return reject(err);
      return resolve(res.rows);
    })
  })
}
//add new work order
module.exports.addNewWorkOrder = function(userId,type,date,location,projectId,subscriberId,description,vehicle,arrival,departure,representative,locationCord,contactId){
  return new Promise((resolve,reject)=>{
    if(projectId == 0){
      projectId = null;
    }
    if(subscriberId == 0){
      subscriberId = null;
    }
    if(!locationCord)
      locationCord = null;
    if(!arrival)
      arrival = null;
    if(!departure)
      departure = null;
    if(!contactId)
      contactId = null;
    let params = [userId,type,date,location,subscriberId,projectId,description,vehicle,arrival,departure,representative,locationCord,contactId]
    //let projectQuery = ', project_id';
    //if(projectId)
    let query = `INSERT INTO work_order(subscriber_id, project_id, date, location, description, vehicle, arrival, departure, work_order_type_id, user_id, representative, location_cord, contact_id)
    VALUES ($5,$6,$3,$4,$7,$8,$9,$10,$2,$1,$11,$12,$13)
    RETURNING id`;

    client.query(query,params,(err,res)=>{
      if(err) return reject(err);
      return resolve(res.rows[0]);
    })
  })
}
//edit work order
module.exports.editWorkOrder = function(woId,userId,type,date,location,projectId,subscriberId,description,vehicle,arrival,departure,representative,contactId){
  return new Promise((resolve,reject)=>{
    if(projectId == 0){
      projectId = null;
    }
    if(subscriberId == 0){
      subscriberId = null;
    }
    if(!arrival)
      arrival = null;
    if(!departure)
      departure = null;
    if(!contactId)
      contactId = null;
    let params = [userId,type,date,location,subscriberId,projectId,description,vehicle,arrival,departure,representative,woId,contactId]
    //let projectQuery = ', project_id';
    //if(projectId)
    let query = `UPDATE work_order
    SET (subscriber_id, project_id, date, location, description, vehicle, arrival, departure, work_order_type_id, user_id, representative, contact_id) = ($5,$6,$3,$4,$7,$8,$9,$10,$2,$1,$11,$13)
    WHERE id = $12`;

    client.query(query,params,(err,res)=>{
      if(err) return reject(err);
      return resolve(res.rows[0]);
    })
  })
}
//get tasks with no projectId (services) for select2 for work orders (sorted by completion, id)
module.exports.getServiseForWorkOrder = function(){
  return new Promise((resolve,reject)=>{
    //let params = [workOrderId];
    let query = `SELECT project_task.id, id_project, task_name, task_duration, task_start, task_finish, completion, project_task.active, category, priority, finished, subtasks, task_note, id_subscriber, weekend, concat(task_name, ' (', s.name, ')') as text
    FROM project_task
    LEFT JOIN subscriber s on s.id = project_task.id_subscriber
    WHERE id_project is null AND project_task.active = true
    ORDER BY completion, id DESC`;

    client.query(query, (err,res)=>{
      if(err) return reject(err);
      return resolve(res.rows);
    })
  })
};
//save tasks with no projectId (services) for work orders
module.exports.saveServiseForWorkOrder = function(workOrderId,taskId){
  return new Promise((resolve,reject)=>{
    if(!taskId) taskId = null
    let params = [workOrderId,taskId];
    let query = `UPDATE work_order
    SET task_id = $2
    WHERE id = $1`;

    client.query(query,params, (err,res)=>{
      if(err) return reject(err);
      return resolve(res.rows);
    })
  })
};
/*
//edit report
module.exports.editReport = function(reportId,userId,date,time,projectId,report_type,other,description, radio){
  let params = [reportId,userId,date,time,description,report_type]
  let radioQuery = '';
  if(radio == '0'){
    radioQuery = ', project_id, other';
    params.push(projectId);
    params.push(null);
  }
  else{
    radioQuery = ', other, project_id';
    params.push(other);
    params.push(null);
  }
  let query = `UPDATE report
  SET (user_id, date, time, description, report_type`+radioQuery+`) = ($2,$3,$4,$5,$6,$7,$8)
  WHERE id = $1`;
  return client.query(query,params)
  .then(res => {return res.rows[0];})
  .catch(e => {return e;})
}
*/
//Get all projects (for select2 (id, text)) with RT number and project name
module.exports.getProjectsSelect = function(active){
  return new Promise((resolve,reject)=>{
    let activeProject = '';
    if(active && active == 1)
      activeProject =  ' AND completion < 100';
    let query = `SELECT id, concat(project_number, ' - ', project_name) as text
    FROM project
    WHERE active = true`+activeProject+`
    ORDER BY id desc`;

    client.query(query,(err,res)=>{
      if(err) return reject(err);
      return resolve(res.rows);
    })
  })
};
////////////////////////////CHANGES FOR WORK ORDERS
//get work order changes
module.exports.getWorkOrderChanges = function(workOrderId){
  return new Promise((resolve,reject)=>{
    let query = `SELECT c.id, id_project, id_task, id_subtask, id_working_project, id_working_task, c.date, "for", "from", id_note, c.active, id_subscriber, id_absence, id_file, modified_user_id, modified_date, id_work_order, name, surname, cs.id as id_status, cs.status, ct.id as id_type, ct.type
    FROM changes as c
    LEFT JOIN "user" u on c."from" = u.id
    LEFT JOIN change_status cs on c.status = cs.id
    LEFT JOIN change_type ct on c.type = ct.id
    LEFT JOIN work_order wo on c.id_work_order = wo.id
    WHERE id_work_order = $1
    ORDER BY  date`;
    let params = [workOrderId]
    
    client.query(query,params,(err,res)=>{
      if(err) return reject(err);
      return resolve(res.rows);
    })
  })
}
//get work order changes system
module.exports.getWorkOrderChangesSystem = function(workOrderId){
  return new Promise((resolve,reject)=>{
    let query = `SELECT c.id, id_project, id_task, id_subtask, id_working_project, id_working_task, c.date, "from", id_note, id_subscriber, id_absence, id_file, id_work_order, name, surname, cs.id as id_status, cs.status, ct.id as id_type, ct.type
    FROM changes_system as c
    LEFT JOIN "user" u on c."from" = u.id
    LEFT JOIN change_status cs on c.status = cs.id
    LEFT JOIN change_type ct on c.type = ct.id
    LEFT JOIN work_order wo on c.id_work_order = wo.id
    WHERE id_work_order = $1
    ORDER BY  date`;
    let params = [workOrderId]
    
    client.query(query,params,(err,res)=>{
      if(err) return reject(err);
      return resolve(res.rows);
    })
  })
}
//////////////////////////// WORK ORDER CONTACTS
module.exports.getAllContacts = function(){
  return new Promise((resolve,reject)=>{
    let query = `SELECT *
    FROM work_order_contact
    ORDER BY full_name`;
    // let params = [workOrderId]
    
    client.query(query,(err,res)=>{
      if(err) return reject(err);
      return resolve(res.rows);
    })
  })
}
module.exports.getContactById = function(id){
  return new Promise((resolve,reject)=>{
    let query = `SELECT *
    FROM work_order_contact
    WHERE id = $1`;
    let params = [id]
    
    client.query(query,params,(err,res)=>{
      if(err) return reject(err);
      return resolve(res.rows[0]);
    })
  })
}
module.exports.addNewWOContact = function(name, phone){
  return new Promise((resolve,reject)=>{
    let query = `INSERT INTO work_order_contact(full_name, phone_number)
    VALUES ($1, $2)
    RETURNING *`;
    let params = [name, phone]
    
    client.query(query,params,(err,res)=>{
      if(err) return reject(err);
      return resolve(res.rows[0]);
    })
  })
}
module.exports.editWOContact = function(id, phone){
  return new Promise((resolve,reject)=>{
    let query = `UPDATE work_order_contact
    SET phone_number = $2
    WHERE id = $1
    RETURNING *`;
    let params = [id, phone]
    
    client.query(query,params,(err,res)=>{
      if(err) return reject(err);
      return resolve(res.rows[0]);
    })
  })
}