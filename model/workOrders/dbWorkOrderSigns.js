const { Client } = require('pg');

//console.log(process.env.DB_HOST);

const client = new Client({
  user: process.env.DB_USERNAME,
  host: process.env.DB_HOST,
  database: process.env.DB_DATABASE,
  password: process.env.DB_PASSWORD,
  port: process.env.DB_PORT
})
client.connect();

//Add new file for project
module.exports.addFileForUnknownWorkOrder = function(fileName, pathName, userId, type){
  return new Promise((resolve,reject)=>{
    let query = `INSERT INTO work_order_signs(original_name, path_name, user_id, type)
    VALUES ($1, $2, $3, $4)
    RETURNING *`;
    let params = [fileName, pathName, userId, type];

    client.query(query,params,(err,res)=>{
      if(err) return reject(err);
      return resolve(res.rows[0]);
    })
  })
}
//update file active to false, file no longer exist
module.exports.updateActiveFile = function(filename){
  return new Promise((resolve,reject)=>{
    let query = `UPDATE work_order_signs
    SET active = false
    WHERE path_name = $1`;
    let params = [filename];

    client.query(query,params,(err,res)=>{
      if(err) return reject(err);
      return resolve(res.rows);
    })
  })
}
//add new sign for work order id
module.exports.addSign = function(woId, signId){
  return new Promise((resolve,reject)=>{
    let query = `INSERT INTO work_order_signs_keys(work_order_sign_id, work_order_id)
    VALUES ($1,$2)`;
    let params = [signId, woId];

    client.query(query,params,(err,res)=>{
      if(err) return reject(err);
      return resolve(res.rows);
    })
  })
}
//get sign file info
module.exports.getSign = function(fileId){
  return new Promise((resolve,reject)=>{
    let query = `SELECT wos.id, original_name, path_name, user_id, date, type, note, wos.active, concat(name, ' ', surname) as user_name, deleted
    FROM work_order_signs wos
    LEFT JOIN "user" u on wos.user_id = u.id
    WHERE wos.id = $1`;
    let params = [fileId];

    client.query(query,params,(err,res)=>{
      if(err) return reject(err);
      return resolve(res.rows[0]);
    })
  })
}
//mark sign as deleted after it was successfully deleted
module.exports.deleteSign = function(id){
  return new Promise((resolve,reject)=>{
    let query = `UPDATE work_order_signs
    SET deleted = true
    WHERE id = $1
    RETURNING *`;
    let params = [id];

    client.query(query,params,(err,res)=>{
      if(err) return reject(err);
      return resolve(res.rows[0]);
    })
  })
}
//get work order signs
module.exports.getWorkOrderSigns = function(workOrderId) {
  return new Promise((resolve,reject)=>{
    let query = `SELECT wos.id, original_name, path_name, user_id, date, type, note, active
    FROM work_order_signs_keys as wosk
    LEFT JOIN work_order_signs wos on wosk.work_order_sign_id = wos.id
    WHERE work_order_id = $1`;
    let params = [workOrderId]
    
    client.query(query,params,(err,res)=>{
      if(err) return reject(err);
      return resolve(res.rows);
    })
  })
}