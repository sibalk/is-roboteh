var express = require('express');
var bcrypt = require('bcryptjs');
var dateFormat = require('dateformat');
const saltRounds = 10;

var db = require('../controllers/db');
var dbEmployees = require('../model/employees/dbEmployees');
let dbLogin = require('../model/logins/dbLogins');

var invalidLogins = [];
var invalidUsernameLogins = [];
const timeToKeepIp = 2*60*60*1000;
const timeToKeepUsername = 2*60*60*1000;
const hoursToKeepIp = 2, minutesToKeepIp = 0, secondsToKeepIp = 0;
const hoursToKeepUsername = 0, minutesToKeepUsername = 30, secondsToKeepUsername = 0;
const availableBadUsername = 5, availableBadIpAddress = 10;
//const timeToKeepIp = 10*60*1000;// 10 minut
//const timeToKeepUsername = 5*60*1000;//5 minut
//const timeToKeepIp = 2*60*60*1000;//2 uri

function logFailedLogin(userIp) {
  //console.log(userIp);
  if(invalidLogins[userIp] && invalidLogins[userIp].counter) {
    invalidLogins[userIp].counter = invalidLogins[userIp].counter + 1;
  }
  else{
    invalidLogins[userIp] = {
      'counter': 1,
      'date': new Date()
    }
  }
}
function logFailedUsernameLogin(username) {
  //console.log(userIp);
  if(invalidUsernameLogins[username] && invalidUsernameLogins[username].counter) {
    invalidUsernameLogins[username].counter = invalidUsernameLogins[username].counter + 1;
  }
  else{
    invalidUsernameLogins[username] = {
      'counter': 1,
      'date': new Date()
    }
  }
}
function authenticate(req, res, next) {
  if(req.session.authenticated && req.session.authenticated == true){
    if(req.sessionOptions.secret && req.sessionOptions.secret == process.env.APP_SECRET){
      dbEmployees.getUserById(req.session.user_id, req.session.user_id).then(user => {
        // console.log(user);
        if(user.active)
          return next();
        else{
          req.session = null;
          return res.redirect('/login');
        }
      })
      .catch(e => {
        return res.json({success:false, error: e});
      })
    }
    else
      return res.redirect('/login');
  }
  else
    return res.redirect('/login');
}
function calculateTimeDifference(date1, date2) {
  let fromDate = new Date(date1);
  let nowDate = date2 ? new Date(date2) : new Date();
  
  let dateDif = nowDate.getTime() - fromDate.getTime();
  let hh = Math.floor(dateDif / 1000 / 60 / 60);
  dateDif -= hh * 1000 * 60 * 60;
  let mm = Math.floor(dateDif / 1000 / 60);
  dateDif -= mm * 1000 * 60;
  let ss = Math.floor(dateDif / 1000);

  let hLabel = '', mLabel = '', sLabel;
  switch (hh) {
    case 1: hLabel = ' uro, '; break;
    case 2: hLabel = ' uri, '; break;
    case 3 || 4: hLabel = ' ure, '; break;
    default: hLabel = ' ur, '; break;
  }
  switch (mm) {
    case 1: mLabel = ' minuto '; break;
    case 2: mLabel = ' minuti '; break;
    case 3 || 4: mLabel = ' minute '; break;
    default: mLabel = ' minut '; break;
  }
  switch (ss) {
    case 1: sLabel = ' sekundo.'; break;
    case 2: sLabel = ' sekundi.'; break;
    case 3 || 4: sLabel = ' sekunde.'; break;
    default: sLabel = ' sekund.'; break;
  }

  return {hh, mm, ss, hLabel, mLabel, sLabel};
}
function getNAvailableMessageForNoUsername(n) {
  switch (n) {
    case 1: message = 'Neveljavna prijava z praznim uporabniškim imenom. Imate še '+n+' veljaven poizkus.'; break;
    case 2: message = 'Neveljavna prijava z praznim uporabniškim imenom. Imate še '+n+' veljavna poizkusa.'; break;
    case 3: message = 'Neveljavna prijava z praznim uporabniškim imenom. Imate še '+n+' veljavne poizkuse.'; break;
    case 4: message = 'Neveljavna prijava z praznim uporabniškim imenom. Imate še '+n+' veljavne poizkuse.'; break;
    default: message = 'Neveljavna prijava z praznim uporabniškim imenom. Imate še '+n+' veljavnih poizkusov.'; break;
  }
  return message;
}
function getNAvailableMessageForBadUsername(n) {
  switch (n) {
    case 1: message = 'Neveljavna prijava z ne veljavnim uporabniškim imenom (posebni znaki). Imate še '+n+' veljaven poizkus.'; break;
    case 2: message = 'Neveljavna prijava z ne veljavnim uporabniškim imenom (posebni znaki). Imate še '+n+' veljavna poizkusa.'; break;
    case 3: message = 'Neveljavna prijava z ne veljavnim uporabniškim imenom (posebni znaki). Imate še '+n+' veljavne poizkuse.'; break;
    case 4: message = 'Neveljavna prijava z ne veljavnim uporabniškim imenom (posebni znaki). Imate še '+n+' veljavne poizkuse.'; break;
    default: message = 'Neveljavna prijava z ne veljavnim uporabniškim imenom (posebni znaki). Imate še '+n+' veljavnih poizkusov.'; break;
  }
  return message;
}
function getNAvailableMessageForNoUser(n) {
  switch (n) {
    case 1: message = 'Neveljavna prijava. Uporabniški račun ne obstaja. Imate še '+n+' veljaven poizkus.'; break;
    case 2: message = 'Neveljavna prijava. Uporabniški račun ne obstaja. Imate še '+n+' veljavna poizkusa.'; break;
    case 3: message = 'Neveljavna prijava. Uporabniški račun ne obstaja. Imate še '+n+' veljavne poizkuse.'; break;
    case 4: message = 'Neveljavna prijava. Uporabniški račun ne obstaja. Imate še '+n+' veljavne poizkuse.'; break;
    default: message = 'Neveljavna prijava. Uporabniški račun ne obstaja. Imate še '+n+' veljavnih poizkusov.'; break;
  }
  return message;
}
function getNAvailableMessageForUserNotActive(n) {
  switch (n) {
    case 1: message = 'Neveljavna prijava. Vaš uporabniški račun ni več aktiven. Imate še '+n+' veljaven poizkus.'; break;
    case 2: message = 'Neveljavna prijava. Vaš uporabniški račun ni več aktiven. Imate še '+n+' veljavna poizkusa.'; break;
    case 3: message = 'Neveljavna prijava. Vaš uporabniški račun ni več aktiven. Imate še '+n+' veljavne poizkuse.'; break;
    case 4: message = 'Neveljavna prijava. Vaš uporabniški račun ni več aktiven. Imate še '+n+' veljavne poizkuse.'; break;
    default: message = 'Neveljavna prijava. Vaš uporabniški račun ni več aktiven. Imate še '+n+' veljavnih poizkusov.'; break;
  }
  return message;
}
function getNAvailableMessageForIncorrectPassword(nbip,nbu) {
  switch (nbu) {
    case 1: message = 'Nepreavilno geslo. Imate še '+nbu+' veljaven poizkus.'; break;
    case 2: message = 'Nepreavilno geslo. Imate še '+nbu+' veljavna poizkusa.'; break;
    case 3: message = 'Nepreavilno geslo. Imate še '+nbu+' veljavne poizkuse.'; break;
    case 4: message = 'Nepreavilno geslo. Imate še '+nbu+' veljavne poizkuse.'; break;
    default: message = 'Nepreavilno geslo. Imate še '+nbu+' veljavnih poizkusov.'; break;
  }
  if(nbip<nbu){
    switch (nbip) {
      case 1: message += '(Ip naslov ima samo še '+nbip+' veljaven poizkus.)'; break;
      case 2: message += '(Ip naslov ima samo še '+nbip+' veljavna poizkusa.)'; break;
      case 3: message += '(Ip naslov ima samo še '+nbip+' veljavne poizkuse.)'; break;
      case 4: message += '(Ip naslov ima samo še '+nbip+' veljavne poizkuse.)'; break;
      default: message += '(Ip naslov ima samo še '+nbip+' veljavnih poizkusov.)'; break;
    }
  }
  return message;
}
function formatDate(date){
  return {
    "date": dateFormat(date, "yyyy-mm-dd"),
    "time": dateFormat(date, "HH:MM:ss")
  }
}
function login(req, res, next) {
  //console.log(invalidLogins);
  //console.log(invalidUsernameLogins);
  //console.log('-------------------------------------------------------------');
  userIp = req.header('x-forwarded-for') || req.connection.remoteAddress;
  if(req.body.username){
    let myRegEx  = /[^a-z\d]/i;
    if((myRegEx.test(req.body.username))){
      //console.log('neveljavno up. ime: '+req.body.username);
      logFailedLogin(userIp);
      logFailedUsernameLogin(req.body.username);
    }
  }
  //preveri ali je z istim up imenom bilo preveč neuspešnih poizkusov in če je pretekel čas, ki ga mora počakati
  if(invalidUsernameLogins[req.body.username]){
    if(new Date().getTime() - invalidUsernameLogins[req.body.username].date.getTime() > timeToKeepUsername){
      invalidUsernameLogins[req.body.username] = undefined;
    }
    else{
      if(invalidUsernameLogins[req.body.username].counter >= 5){
        //
        return res.render('login', {
          message: "Preveč neuspešnih poizkusov z istim uporabniškim imenom."
        });
      }
    }
  }
  //preveri ali je iz tega ip naslova bilo preveč neuspešnih poizkusov in če je pretekel čas, ki ga mora počakati
  if(invalidLogins[userIp]) {
    if(new Date().getTime() - invalidLogins[userIp].date.getTime() > timeToKeepIp){
      invalidLogins[userIp] = undefined;
    }
    else{
      if(invalidLogins[userIp].counter >= 10){
        return res.render('login', {
          message: "Preveč neuspešnih poizkusov z istega naslova."
        });
      }
    }
  }
  //preveri ali je uporabnik sploh poslal up. ime
  if(!req.body.username) {
    logFailedLogin(userIp);
    return res.render('login', {
      message: "Napačno uporabniško ime ali geslo."
    });
  }
  //console.log('preverjam ustreznost, uporabnik prestal prve teste');
  //pridobim uporabnika in preverim ali je pravilno geslo
  db.getUser({ username: req.body.username}, (err, user) =>{
    if(err){
      return res.render('login', {
        message: "Napaka na strežniku."
      });
    }
    if(user && req.body.password && bcrypt.compareSync(req.body.password, user.password)){
      req.session.authenticated = true;
      req.session.user_id = user.user_id;
      req.session.user_name = user.name;
      req.session.user_surname = user.surname;
      //req.session.user_username = user.username;
      req.session.role_id = user.role;
      req.session.type = user.type;
      req.session.maxAge = new Date().getTime() + 10*60*60*1000;
      res.redirect('/');
    }
    else{
      //preveri, če je up. račun še aktiven
      dbEmployees.isActive(req.body.username).then(active=>{
        if(active){
          //console.log("");
          if(active.active){
            logFailedUsernameLogin(req.body.username);
            logFailedLogin(userIp);
            return res.render('login', {
              message: "Napačno uporabniško ime ali geslo."
            });
          }
          else{
            return res.render('login', {
              message: "Vaš uporabniški račun ni več aktiven."
            });
          }
        }
        else{
          //console.log("");
          logFailedUsernameLogin(req.body.username);
          logFailedLogin(userIp);
          return res.render('login', {
            message: "Napačno uporabniško ime ali geslo."
          });
        }
      })
      .catch((e) => {
        console.log(e);
      })
    }
  })
}
//////////////////////////////////// LOGIN REREDO - WITH ONLY USERNAME LOCK ////////////////////////////////////////
async function loginReRedo(req, res, next) {
  let username = req.body.username;
  let password = req.body.password;
  if (username && password){
    let myRegEx  = /[^a-z\d]/i;
    if (myRegEx.test(username))
      loginRRDBadUsername(req, res, next)
    else
      loginRRDNormalLogin(req, res, next)
  }
  else{
    loginRRDNoUsername(req,res,next)
  }
}
async function loginRRDNormalLogin(req, res, next) {
  let username = req.body.username;
  let password = req.body.password;
  //get user and check if even exist
  try {
    let user = await dbLogin.getUser(username);
    if (user){
      if (user.active){
        //every bad scenario is out, do normal check, get latest login, check if withing threshold and password is ok
        let fromDate = new Date();
        fromDate.setHours(fromDate.getHours() - hoursToKeepUsername),fromDate.setMinutes(fromDate.getMinutes() - minutesToKeepUsername),fromDate.setSeconds(fromDate.getSeconds() - secondsToKeepUsername);
        let formattedFromDate = formatDate(fromDate);
        let badUsernameLogin = await dbLogin.getLastLoginWithUsername(username, formattedFromDate.date + ' ' + formattedFromDate.time, availableBadUsername);
        //console.log('badUsernameLogin: '+badUsernameLogin);
        let nbu = badUsernameLogin ? badUsernameLogin.n_bad_username : 0;

        if (nbu < availableBadUsername && bcrypt.compareSync(password, user.password)){
          let savedLogin = await dbLogin.saveLogin(null, username, 0, 0);
          //console.log('savedLogin: ' + savedLogin);
          //console.log('------------------------');
          req.session.authenticated = true;
          req.session.user_id = user.id;
          req.session.user_name = user.name;
          req.session.user_surname = user.surname;
          //req.session.user_username = user.username;
          req.session.role_id = user.role;
          req.session.type = user.type;
          req.session.maxAge = new Date().getTime() + 10*60*60*1000;
          res.redirect('/');
        }
        else if(nbu < (availableBadUsername - 1)){
          let latestBadUsernameLogin = await dbLogin.getLastLoginWithUsername(username, formattedFromDate.date + ' ' + formattedFromDate.time);
          let nbu = badUsernameLogin ? badUsernameLogin.n_bad_username : 0;
          //console.log('latestBadUsernameLogin: '+latestBadUsernameLogin);
          let savedLogin = await dbLogin.saveLogin(null, username, 0, (nbu+1), 'false');
          
          let n = (availableBadUsername - nbu)-1;
          let message = '';
          switch (n) {
            case 1: message = 'Nepreavilno geslo. Imate še '+n+' veljaven poizkus.'; break;
            case 2: message = 'Nepreavilno geslo. Imate še '+n+' veljavna poizkusa.'; break;
            case 3: message = 'Nepreavilno geslo. Imate še '+n+' veljavne poizkuse.'; break;
            case 4: message = 'Nepreavilno geslo. Imate še '+n+' veljavne poizkuse.'; break;
            default: message = 'Nepreavilno geslo. Imate še '+n+' veljavnih poizkusov.'; break;
          }
          //console.log('------------------------');
          return res.render('login', {
            message,
          })
        }
        else{
          let latestBadUsernameLogin = await dbLogin.getLastLoginWithUsername(username, formattedFromDate.date + ' ' + formattedFromDate.time);
          let nbu = latestBadUsernameLogin ? latestBadUsernameLogin.n_bad_username : 0;
          //console.log('latestBadUsernameLogin: '+latestBadUsernameLogin);
          let savedLogin = await dbLogin.saveLogin(null, username, 0, (nbu+1), 'false');
          
          let timeDif = calculateTimeDifference(fromDate, badUsernameLogin.date);
          let message = 'Preveč neuspešnih poizkusov z istim uporabniškim računom. Ponovno lahko poizkusite čez  ' + timeDif.hh + timeDif.hLabel + timeDif.mm + timeDif.mLabel + ' in ' + timeDif.ss + timeDif.sLabel;
          //console.log('------------------------');
          return res.render('login', {
            message,
            hours: timeDif.hh,
            minutes: timeDif.mm,
            seconds: timeDif.ss
          })
        }
      }
      else{
        let savedLogin = await dbLogin.saveLogin(null, username+' (unactive user)', 0, 0, 'false');
        let message = 'Neuspešna prijava! Uporabnik s tem uporabniškim imenom ni več aktiven.';
        //console.log('------------------------');
        return res.render('login', {
          message,
        })
      }
    }
    else{
      let savedLogin = await dbLogin.saveLogin(null, username + ' (no user)', 0, 0, 'false');
      let message = 'Neuspešna prijava! Uporabnik s tem uporabniškim imenom ne obstaja.';
      //console.log('------------------------');
      return res.render('login', {
        message,
      })
    }
  } catch (error) {
    return res.render('login', {
      message: 'Napaka na strežniku.'
    })
  }
}
async function loginRRDBadUsername(req, res, next) {
  try {
    let savedLogin = await dbLogin.saveLogin(null, '(bad.username.regex)', 0, 0, 'false');
    let message = 'Neuspešna prijava! Prijava s posebnimi znaki v uporabniškem imenu ni dovoljena.';
    //console.log('------------------------');
    return res.render('login', {
      message,
    })
  } catch (error) {
    return res.render('login', {
      message: 'Napaka na strežniku.'
    })
  }
}
async function loginRRDNoUsername(req, res, next) {
  try {
    let savedLogin = await dbLogin.saveLogin(null, '(no username or password)', 0, 0, 'false');
    let message = 'Neuspešna prijava! Prijava s praznim uporabniškim imenom ali geslom ni dovoljena.';
    //console.log('------------------------');
    return res.render('login', {
      message,
    })
  } catch (error) {
    return res.render('login', {
      message: 'Napaka na strežniku.'
    })
  }
}
//////////////////////////////////// LOGIN REDO - WITH USERNAME AND IP ADDRESS LOCK ////////////////////////////////////////
async function loginRedo(req, res, next) {
  let userIp = req.header('x-forwarded-for') || req.connection.remoteAddress;
  let username = req.body.username;
  let password = req.body.password;
  //check valid username with regex (sql inject) and check if username & password was send
  if (username){
    let myRegEx  = /[^a-z\d]/i;
    if((myRegEx.test(req.body.username))){
      loginBadUsername(req,res,next);
    }
    else{
      //GOOD USERNAME ----> PROCEED TO NORMAL LOGIN
      //console.log('---------GOOD USERNAME -> PROCEED---------');
      let user = await dbLogin.getUser(username);
      if(user){
        if(user.active){
          // console.log('-----------USER EXIST & IS ACTIVE-----------');
          // return res.render('login', {
          //   message: 'Napaka na strežniku.'
          // })
          loginCheckPass(req, res, next, user);
        }
        else{
          loginNotActiveUser(req, res, next);
        }
      }
      else{
        loginNoUser(req, res, next);
      }
    }
  }
  else{
    loginNoUsername(req,res,next);
  }
}
async function loginCheckPass(req, res, next, user) {
  //USER EXIST, CHECK PASSWORD
  //console.log('---------NORMAL->CHECK PASSWORD---------');
  let userIp = req.header('x-forwarded-for') || req.connection.remoteAddress;
  let username = req.body.username;
  let password = req.body.password;
  let fromDate = new Date();
  let fromDateUsername = new Date();
  fromDate.setHours(fromDate.getHours() - hoursToKeepIp),fromDate.setMinutes(fromDate.getMinutes() - minutesToKeepIp),fromDate.setSeconds(fromDate.getSeconds() - secondsToKeepIp);
  let formattedFromDate = formatDate(fromDate);
  fromDateUsername.setHours(fromDateUsername.getHours() - hoursToKeepUsername),fromDateUsername.setMinutes(fromDateUsername.getMinutes() - minutesToKeepUsername),fromDateUsername.setSeconds(fromDateUsername.getSeconds() - secondsToKeepUsername);
  let formattedFromDateUsername = formatDate(fromDateUsername);
  try {
    //console.log('userIp: ' + userIp + ' fromDate: ' + formattedFromDate.date + ' ' + formattedFromDate.time + ' abia: ' + availableBadIpAddress);
    let badIpLogin = await dbLogin.getLastLoginWithIPAddress(userIp, formattedFromDate.date + ' ' + formattedFromDate.time, availableBadIpAddress);
    //console.log('badIpLogin: '+badIpLogin);
    let badUsernameLogin = await dbLogin.getLastLoginWithUsername(username, formattedFromDateUsername.date + ' ' + formattedFromDateUsername.time, availableBadUsername);
    //console.log('badUsernameLogin: '+badUsernameLogin);
    let nbip = badIpLogin ? badIpLogin.n_bad_ip : 0;
    let nbu = badUsernameLogin ? badUsernameLogin.n_bad_username : 0;

    let nabip = availableBadIpAddress - ++nbip;
    let nabu = availableBadUsername - ++nbu;

    if (nabip >= 0 && nabu >= 0 && bcrypt.compareSync(password, user.password)){
      //let message = getNAvailableMessageForNoUsername(nabip);
      let savedLogin = await dbLogin.saveLogin(userIp, username, 0, 0);
      //console.log('savedLogin: ' + savedLogin);
      //console.log('------------------------');
      req.session.authenticated = true;
      req.session.user_id = user.id;
      req.session.user_name = user.name;
      req.session.user_surname = user.surname;
      //req.session.user_username = user.username;
      req.session.role_id = user.role;
      req.session.type = user.type;
      req.session.maxAge = new Date().getTime() + 10*60*60*1000;
      res.redirect('/');
    }
    else if(nabip > 0 && nabu > 0){
      //INCORRECT PASSWORD -> Can try again in nabu tries; if nabip < nabu show ip has less tries
      let message = getNAvailableMessageForIncorrectPassword(nabip,nabu);
      let savedLogin = await dbLogin.saveLogin(userIp, username, nbip, nbu, 'false');
      //console.log('savedLogin: ' + savedLogin);
      //console.log('------------------------');
      return res.render('login', {
        message
      })
    }
    else{
      //PROBLEM -> WHICH TIME TO SHOW
      let latestBadIpLogin = await dbLogin.getLastLoginWithIPAddress(userIp, formattedFromDate.date + ' ' + formattedFromDate.time);
      //console.log('latestBadIpLogin: '+latestBadIpLogin);
      let latestBadUsernameLogin = await dbLogin.getLastLoginWithUsername(username, formattedFromDateUsername.date + ' ' + formattedFromDateUsername.time);
      //console.log('latestBadUsernameLogin: '+latestBadUsernameLogin);
      
      if (badIpLogin && badUsernameLogin){
        let timeBadIp = new Date(badIpLogin.date);
        let timeBadUsername = new Date(badUsernameLogin.date);
        let savedLogin = await dbLogin.saveLogin(userIp, username, (latestBadIpLogin.n_bad_ip+1), (latestBadUsernameLogin.n_bad_username+1), 'false');
        //console.log(savedLogin);
        if (nabip <= 0 && nabu > 0){
          let timeDif = calculateTimeDifference(fromDate, badIpLogin.date);
          let message = 'Preveč neuspešnih poizkusov z istega naslova. Ponovno lahko poizkusite čez  ' + timeDif.hh + timeDif.hLabel + timeDif.mm + timeDif.mLabel + ' in ' + timeDif.ss + timeDif.sLabel;
          //console.log('------------------------');
          return res.render('login', {
            message,
            hours: timeDif.hh,
            minutes: timeDif.mm,
            seconds: timeDif.ss
          })
        }
        else if (nabu <= 0 && nabip > 0){
          let timeDif = calculateTimeDifference(fromDateUsername, badUsernameLogin.date);
          let message = 'Preveč neuspešnih poizkusov z istim uporabniškim računom. Ponovno lahko poizkusite čez  ' + timeDif.hh + timeDif.hLabel + timeDif.mm + timeDif.mLabel + ' in ' + timeDif.ss + timeDif.sLabel;
          //console.log('------------------------');
          return res.render('login', {
            message,
            hours: timeDif.hh,
            minutes: timeDif.mm,
            seconds: timeDif.ss
          })
        }
        else{
          if (timeBadUsername.getTime() < timeBadIp.getTime()){
            let timeDif = calculateTimeDifference(fromDateUsername, badUsernameLogin.date);
            let message = 'Preveč neuspešnih poizkusov z istim uporabniškim računom. Ponovno lahko poizkusite čez  ' + timeDif.hh + timeDif.hLabel + timeDif.mm + timeDif.mLabel + ' in ' + timeDif.ss + timeDif.sLabel;
            //console.log('------------------------');
            return res.render('login', {
              message,
              hours: timeDif.hh,
              minutes: timeDif.mm,
              seconds: timeDif.ss
            })
          }
          else{
            let timeDif = calculateTimeDifference(fromDate, badIpLogin.date);
            let message = 'Preveč neuspešnih poizkusov z istega naslova. Ponovno lahko poizkusite čez  ' + timeDif.hh + timeDif.hLabel + timeDif.mm + timeDif.mLabel + ' in ' + timeDif.ss + timeDif.sLabel;
            //console.log('------------------------');
            return res.render('login', {
              message,
              hours: timeDif.hh,
              minutes: timeDif.mm,
              seconds: timeDif.ss
            })
          }
        }
      }
      else if (badIpLogin){
        let savedLogin = await dbLogin.saveLogin(userIp, username, (latestBadIpLogin.n_bad_ip+1), 0, 'false');
        //console.log(savedLogin);
        let timeDif = calculateTimeDifference(fromDate, badIpLogin.date);
        let message = 'Preveč neuspešnih poizkusov z istega naslova. Ponovno lahko poizkusite čez  ' + timeDif.hh + timeDif.hLabel + timeDif.mm + timeDif.mLabel + ' in ' + timeDif.ss + timeDif.sLabel;
        //console.log('------------------------');
        return res.render('login', {
          message,
          hours: timeDif.hh,
          minutes: timeDif.mm,
          seconds: timeDif.ss
        })
      }
      else if (badUsernameLogin){
        let savedLogin = await dbLogin.saveLogin(userIp, username, 0, (latestBadUsernameLogin.n_bad_username+1), 'false');
        //console.log(savedLogin);
        let timeDif = calculateTimeDifference(fromDateUsername, badUsernameLogin.date);
        let message = 'Preveč neuspešnih poizkusov z istim uporabniškim računom. Ponovno lahko poizkusite čez  ' + timeDif.hh + timeDif.hLabel + timeDif.mm + timeDif.mLabel + ' in ' + timeDif.ss + timeDif.sLabel;
        //console.log('------------------------');
        return res.render('login', {
          message,
          hours: timeDif.hh,
          minutes: timeDif.mm,
          seconds: timeDif.ss
        })
      }
      else{
        return res.render('login', {
          message: 'Napaka na strežniku.'
        })    
      }
    }
  } catch (error) {
    //console.log('------------------------');
    return res.render('login', {
      message: 'Napaka na strežniku.'
    })
  }
}
async function loginNoUsername(req, res, next) {
  //NO USERNAME ----> REJECT AND SAVE BAD IP FOR LOCKING IF MULTIPLE BAD LOGINS
  //get bad ip login to increment nbip
  //console.log('---------NO USERNAME---------');
  let userIp = req.header('x-forwarded-for') || req.connection.remoteAddress;
  let fromDate = new Date();
  fromDate.setHours(fromDate.getHours() - hoursToKeepIp),fromDate.setMinutes(fromDate.getMinutes() - minutesToKeepIp),fromDate.setSeconds(fromDate.getSeconds() - secondsToKeepIp);
  let formattedFromDate = formatDate(fromDate);
  try {
    //console.log('userIp: ' + userIp + ' fromDate: ' + formattedFromDate.date + ' ' + formattedFromDate.time + ' abia: ' + availableBadIpAddress);
    let badIpLogin = await dbLogin.getLastLoginWithIPAddress(userIp, formattedFromDate.date + ' ' + formattedFromDate.time, availableBadIpAddress);
    //console.log(badIpLogin);
    let nbip = badIpLogin ? badIpLogin.n_bad_ip : 0;
    let nbu = 0;

    let nabip = availableBadIpAddress - ++nbip;

    if (nabip > 0){
      let message = getNAvailableMessageForNoUsername(nabip);
      let savedLogin = await dbLogin.saveLogin(userIp, null, nbip, nbu, 'false');
      //console.log(savedLogin);
      //console.log('------------------------');
      return res.render('login', {
        message
      })
    }
    else{
      let latestBadIpLogin = await dbLogin.getLastLoginWithIPAddress(userIp, formattedFromDate.date + ' ' + formattedFromDate.time);
      //console.log(latestBadIpLogin);
      let savedLogin = await dbLogin.saveLogin(userIp, null, (latestBadIpLogin.n_bad_ip+1), nbu, 'false');
      //console.log(savedLogin);
      let timeDif = calculateTimeDifference(fromDate, badIpLogin.date);
      let message = 'Preveč neuspešnih poizkusov z istega naslova. Ponovno lahko poizkusite čez  ' + timeDif.hh + timeDif.hLabel + timeDif.mm + timeDif.mLabel + ' in ' + timeDif.ss + timeDif.sLabel;
      //console.log('------------------------');
      return res.render('login', {
        message,
        hours: timeDif.hh,
        minutes: timeDif.mm,
        seconds: timeDif.ss
      })
    }
  } catch (error) {
    //console.log('------------------------');
    return res.render('login', {
      message: 'Napaka na strežniku.'
    })
  }
}
async function loginBadUsername(req, res, next) {
  //console.log('---------BAD USERNAME---------');
  //INCORECT LOGIN - BAD USERNAME BECAUSE IT CAN BE SQL INJECT ATTACK
  let userIp = req.header('x-forwarded-for') || req.connection.remoteAddress;
  let fromDate = new Date();
  fromDate.setHours(fromDate.getHours() - hoursToKeepIp),fromDate.setMinutes(fromDate.getMinutes() - minutesToKeepIp),fromDate.setSeconds(fromDate.getSeconds() - secondsToKeepIp);
  let formattedFromDate = formatDate(fromDate);

  try {
    //console.log('userIp: ' + userIp + ' fromDate: ' + formattedFromDate.date + ' ' + formattedFromDate.time + ' abia: ' + availableBadIpAddress);
    let badIpLogin = await dbLogin.getLastLoginWithIPAddress(userIp, formattedFromDate.date + ' ' + formattedFromDate.time, availableBadIpAddress);
    //console.log('badIpLogin: '+badIpLogin);
    let nbip = badIpLogin ? badIpLogin.n_bad_ip : 0;
    let nbu = 0;

    let nabip = availableBadIpAddress - ++nbip;  
    if (nabip > 0){
      //CAN TRY AGAIN
      let message = getNAvailableMessageForBadUsername(nabip);
      let savedLogin = await dbLogin.saveLogin(userIp, '(bad.username.regex)', nbip, nbu, 'false');
      //console.log(savedLogin);
      //console.log('------------------------');
      return res.render('login', {
        message
      })
    }
    else{
      let latestBadIpLogin = await dbLogin.getLastLoginWithIPAddress(userIp, formattedFromDate.date + ' ' + formattedFromDate.time);
      //console.log(latestBadIpLogin);
      let savedLogin = await dbLogin.saveLogin(userIp, '(bad.username.regex)', (latestBadIpLogin.n_bad_ip+1), nbu, 'false');
      //console.log(savedLogin);
      let timeDif = calculateTimeDifference(fromDate, badIpLogin.date);
      let message = 'Preveč neuspešnih poizkusov z istega naslova. Ponovno lahko poizkusite čez  ' + timeDif.hh + timeDif.hLabel + timeDif.mm + timeDif.mLabel + ' in ' + timeDif.ss + timeDif.sLabel;
      //console.log('------------------------');
      return res.render('login', {
        message,
        hours: timeDif.hh,
        minutes: timeDif.mm,
        seconds: timeDif.ss
      })
    }
  } catch (error) {
    //console.log('------------------------');
    return res.render('login', {
      message: 'Napaka na strežniku.'
    })
  }
}
async function loginNoUser(req, res, next) {
  //console.log('---------NO USER---------');
  //INCORECT LOGIN - BAD USERNAME BECAUSE IT CAN BE SQL INJECT ATTACK
  let userIp = req.header('x-forwarded-for') || req.connection.remoteAddress;
  let username = req.body.username;
  let fromDate = new Date();
  fromDate.setHours(fromDate.getHours() - hoursToKeepIp),fromDate.setMinutes(fromDate.getMinutes() - minutesToKeepIp),fromDate.setSeconds(fromDate.getSeconds() - secondsToKeepIp);
  let formattedFromDate = formatDate(fromDate);

  try {
    //console.log('userIp: ' + userIp + ' fromDate: ' + formattedFromDate.date + ' ' + formattedFromDate.time + ' abia: ' + availableBadIpAddress);
    let badIpLogin = await dbLogin.getLastLoginWithIPAddress(userIp, formattedFromDate.date + ' ' + formattedFromDate.time, availableBadIpAddress);
    //console.log('badIpLogin: '+badIpLogin);
    let nbip = badIpLogin ? badIpLogin.n_bad_ip : 0;
    let nbu = 0;

    let nabip = availableBadIpAddress - ++nbip;  
    if (nabip > 0){
      //CAN TRY AGAIN
      let message = getNAvailableMessageForNoUser(nabip);
      let savedLogin = await dbLogin.saveLogin(userIp, username + '(no user)', nbip, nbu, 'false');
      //console.log(savedLogin);
      //console.log('------------------------');
      return res.render('login', {
        message
      })
    }
    else{
      let latestBadIpLogin = await dbLogin.getLastLoginWithIPAddress(userIp, formattedFromDate.date + ' ' + formattedFromDate.time);
      //console.log(latestBadIpLogin);
      let savedLogin = await dbLogin.saveLogin(userIp, username + '(no user)', (latestBadIpLogin.n_bad_ip+1), nbu, 'false');
      //console.log(savedLogin);
      let timeDif = calculateTimeDifference(fromDate, badIpLogin.date);
      let message = 'Preveč neuspešnih poizkusov z istega naslova. Ponovno lahko poizkusite čez  ' + timeDif.hh + timeDif.hLabel + timeDif.mm + timeDif.mLabel + ' in ' + timeDif.ss + timeDif.sLabel;
      //console.log('------------------------');
      return res.render('login', {
        message,
        hours: timeDif.hh,
        minutes: timeDif.mm,
        seconds: timeDif.ss
      })
    }
  } catch (error) {
    //console.log('------------------------');
    return res.render('login', {
      message: 'Napaka na strežniku.'
    })
  }
}
async function loginNotActiveUser(req, res, next) {
  //console.log('---------USER NOT ACTIVE---------');
  //USER WAS DELETED, IS NOT ACTIVE, USER HAS NO LONGER ACCESS TO RIS
  let userIp = req.header('x-forwarded-for') || req.connection.remoteAddress;
  let username = req.body.username;
  let fromDate = new Date();
  fromDate.setHours(fromDate.getHours() - hoursToKeepIp),fromDate.setMinutes(fromDate.getMinutes() - minutesToKeepIp),fromDate.setSeconds(fromDate.getSeconds() - secondsToKeepIp);
  let formattedFromDate = formatDate(fromDate);

  try {
    //console.log('userIp: ' + userIp + ' fromDate: ' + formattedFromDate.date + ' ' + formattedFromDate.time + ' abia: ' + availableBadIpAddress);
    let badIpLogin = await dbLogin.getLastLoginWithIPAddress(userIp, formattedFromDate.date + ' ' + formattedFromDate.time, availableBadIpAddress);
    //console.log('badIpLogin: '+badIpLogin);
    let nbip = badIpLogin ? badIpLogin.n_bad_ip : 0;
    let nbu = 0;

    let nabip = availableBadIpAddress - ++nbip;  
    if (nabip > 0){
      //CAN TRY AGAIN
      let message = getNAvailableMessageForUserNotActive(nabip);
      let savedLogin = await dbLogin.saveLogin(userIp, username + '(not active)', nbip, nbu, 'false');
      //console.log(savedLogin);
      //console.log('------------------------');
      return res.render('login', {
        message
      })
    }
    else{
      let latestBadIpLogin = await dbLogin.getLastLoginWithIPAddress(userIp, formattedFromDate.date + ' ' + formattedFromDate.time);
      //console.log(latestBadIpLogin);
      let savedLogin = await dbLogin.saveLogin(userIp, username + '(not active)', (latestBadIpLogin.n_bad_ip+1), nbu, 'false');
      //console.log(savedLogin);
      let timeDif = calculateTimeDifference(fromDate, badIpLogin.date);
      let message = 'Preveč neuspešnih poizkusov z istega naslova. Ponovno lahko poizkusite čez  ' + timeDif.hh + timeDif.hLabel + timeDif.mm + timeDif.mLabel + ' in ' + timeDif.ss + timeDif.sLabel;
      //console.log('------------------------');
      return res.render('login', {
        message,
        hours: timeDif.hh,
        minutes: timeDif.mm,
        seconds: timeDif.ss
      })
    }
  } catch (error) {
    //console.log('------------------------');
    return res.render('login', {
      message: 'Napaka na strežniku.'
    })
  }
}
exports.authenticate = authenticate;
exports.login = login;
exports.loginRedo = loginRedo;
exports.loginReRedo = loginReRedo;