const { Client } = require('pg');

//console.log(process.env.DB_HOST);

const client = new Client({
  user: process.env.DB_USERNAME,
  host: process.env.DB_HOST,
  database: process.env.DB_DATABASE,
  password: process.env.DB_PASSWORD,
  port: process.env.DB_PORT
})
client.connect();
////////////////////////////
////LOGIN////
////////////////////////////
//Get user
function getUser(user, callback) {
  var query = `SELECT LOWER(username) as username, name, surname, password, "user".role as role_id, role.role as type, "user".id as user_id
  FROM "user", role
  WHERE "user".role = role.id AND
        active = true AND
        lower(username) = lower($1)`;
  var params = [user.username];

  client.query(query, params, (err, res) =>{
    if(err) {
      //console.log(err.stack);
      callback(err);
    }
    else{
      callback(null, res.rows[0]);
    }
  })
}
module.exports = {
  'getUser': getUser
};
///////////////EXAMPLE FOR CALLBACK WITH PROMISE////////////
/*
//Get numbers of projects
module.exports.getProjectNumbers = function(){
  return new Promise((resolve, reject)=>{
    let query = `SELECT project_number
    FROM project`;

    client.query(query, (err,res)=>{
      if(err) return reject(err);
      return resolve(res.rows);
    })
  })
};
*/