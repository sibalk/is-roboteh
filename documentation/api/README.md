# Dokumentacija API

Dokumentacija API za spletno aplikacijo RobotehIS. Aplikacija je dostopna [https://project.roboteh.si](https://project.roboteh.si). API je dostopen na naslovih `https://project.roboteh.si/api/`. Za testiranje je na voljo testna aplikacija z izmišljenimi podatki na [http://80.211.68.15:3001](http://80.211.68.15:3001) in njegov API na `http://80.211.68.15:3001/api/`.

# Login/OAuth2

Naslovi za pridobivanje žetona za dostop do api ter slike Insomnie za auth code grant in password grant. Client je nastavljen tako, da omogoča authorization code, refresh token in password grant. Client id in secret za dostop do pravega api se pošlje posebej.

- authorization url: url-aplikacije/api/oauth/authenticate

- access token url: url-aplikacije/api/oauth/access_token

- url-aplikacija/api/authenticate	`GET`
	- vrne stran za login z OAuth2

- url-aplikacija/api/authenticate	`POST`
	- zahteva username in password v body (vsi so v body razen file je multipart form)

## Authorization code grant

![alt text](./slike/authcode.png "Authorization code grant")

## Password grant

![alt text](./slike/password.png "Password grant")

# Poročila - Reports

Delovna poročila: zaposleni preko teh sporočajo delo narejo od doma. Zapiše se kratek opis, datum, začetek in konec ter za kaj je bilo opravljeno delo. V večini primerov so poročila vezana na projekt, je pa tudi nekaj takšnih poročil, ki ni.

## Seznam vseh uporabnikovih poročil
- url-aplikacije/api/reports `GET`
	- vrne vsa delovna poročila vpisanega uporabnika

```javascript
{
  "reports": [
    {
      "id": 6,
      "user_id": 1,
      "project_id": 8,
      "subscriber_id": null,
      "date": "2020-04-20T22:00:00.000Z",
      "time": "08:00:00",
      "description": "test novega poročila preko insomnie na testni strežnik",
      "report_type": 4,
      "active": true,
      "ma": true,
      "created": "2020-04-20T19:12:14.613Z",
      "other": null,
      "start": "07:00:00",
      "finish": "15:00:00"
    },
    {
      "id": 7,
      "user_id": 1,
      "project_id": 8,
      "subscriber_id": null,
      "date": "2020-04-22T22:00:00.000Z",
      "time": "11:00:00",
      "description": "Insomnia Update",
      "report_type": 4,
      "active": true,
      "ma": true,
      "created": "2020-04-23T08:40:15.034Z",
      "other": null,
      "start": "07:00:00",
      "finish": "15:00:00"
    }
  ]
}
```

## Izbrano poročilo
- url-aplikacije/api/reports/:id	`GET`
	- vrne delovno poročila z izbranim id

```javascript
{
  "report": {
    "id": 7,
    "user_id": 1,
    "project_id": 8,
    "subscriber_id": null,
    "date": "2020-04-22T22:00:00.000Z",
    "time": "11:00:00",
    "description": "Insomnia Update",
    "report_type": 4,
    "active": true,
    "ma": true,
    "created": "2020-04-23T08:40:15.034Z",
    "other": null,
    "start": "07:00:00",
    "finish": "15:00:00"
  }
}
```

## Dodajanje poročila
- url-aplikacije/api/reports/ `POST`
  - ustvari novo delovno poročilo
  - zahtevani so vsi vnosi razen time, ki se izračuna iz start in finish, če ta ni podan; pri dodajanju novega poročila je zahtevan vsaj eden od podatkov od projectId ali other, če sta oba se vzame projectId
  - vrne ustvarjeno poročilo
  - userId se prebere iz žetona


| Vnosi         | Primer vnosa                  | Opomba  |
| ------------- |:-----------------------------:| -------:|
| projectId     | 1                             |         |
| other         | Programiranje za naročnika A  |         |
| date          | 2020-04-20                    |         |
| start         | 07:00                         |         |
| finish        | 15:00                         |         |
| description   | Popravil problematičen prog...|         |
| type          | 4                             |         |
| time          | 08:00                         |         |
		

## Posodobitev poročila
- url-aplikacije/api/reports/:id `PUT`
	- enak kot POST klic le da je tu potreben id poročila, ki se posodablja
	- vrne posodobljeno poročilo

## Brisanje poročila
- url-aplikacije/api/reports/:id `DELETE`
	- izbriše izbrano delovno poročila: v bistvu se posodobi active na false
	- nazaj vrne to poročilo z active false

## Tipi poročil
- url-aplikacije/api/reports/types `GET`
	- vrne seznam vseh tipov za poročila

```javascript
{
  "reportTypes": [
    {
      "id": 1,
      "report_type": "ogled"
    },
    {
      "id": 2,
      "report_type": "montaža"
    },
    {
      "id": 3,
      "report_type": "servis"
    },
    {
      "id": 4,
      "report_type": "programiranje"
    },
    {
      "id": 5,
      "report_type": "testiranje"
    },
    {
      "id": 6,
      "report_type": "odpravljanje napak"
    },
    {
      "id": 7,
      "report_type": "konstruiranje"
    }
  ]
}
```


# Delovni nalogi - Work orders

Delovni nalogi: zaposleni na terenu ob koncu dela izpolnijo delovni nalog, kjer je vidno kakšno delo je opravljeno ter kdo je pooblaščenec tega naročnika. V večini primerov gre za delovni naloge za naročnike brez vezave na projekt.

## Seznam vseh uporabnikovih delovnih nalog
- url-aplikacije/api/workorders `GET`
	- vrne vsa delovne naloge vpisanega uporabnika

```javascript
{
  "workOrders": [
    {
      "id": 2,
      "subscriber_id": null,
      "project_id": 1,
      "number": "2/2020",
      "date": "2020-04-22T22:00:00.000Z",
      "location": "Šentjur",
      "description": "Insomnia insert delovnega naloga",
      "vehicle": "Škoda",
      "arrival": "09:00:00",
      "departure": "11:00:00",
      "active": true,
      "user_id": 1,
      "representative": "Janez Novak",
      "created": "2020-04-23T09:14:38.190Z",
      "ma": true,
      "location_cord": null,
      "work_order_type_id": 2
    },
    {
      "id": 1,
      "subscriber_id": 5,
      "project_id": null,
      "number": "2/2020",
      "date": "2020-04-22T22:00:00.000Z",
      "location": "šentjur",
      "description": "Delovni nalog preko spletne aplikacije",
      "vehicle": "Trafic",
      "arrival": "09:00:00",
      "departure": "11:00:00",
      "active": true,
      "user_id": 1,
      "representative": "Janez Novak",
      "created": "2020-04-23T09:14:17.024Z",
      "ma": false,
      "location_cord": null,
      "work_order_type_id": 5
    }
  ]
}
```

## Izbran delovni nalog
- url-aplikacije/api/workorders/:id `GET`
	- vrne delovni nalog z izbranim id

```javascript
{
  "workOrder": {
    "id": 72,
    "subscriber_id": 2,
    "project_id": null,
    "number": "JN-72/2020",
    "date": "2020-04-28T22:00:00.000Z",
    "location": "Šentjur",
    "description": "UPDATE podpisi",
    "vehicle": "Fiat 500",
    "arrival": "09:00:00",
    "departure": "11:00:00",
    "work_order_type_id": 2,
    "active": true,
    "user_id": 1,
    "representative": "Janez Novak",
    "created": "2020-05-22T08:10:07.265Z",
    "location_cord": null,
    "ma": true,
    "author": true,
    "workers": [
      {
        "id": 2,
        "worker_name": "Petra Močnik"
      },
      {
        "id": 7,
        "worker_name": "Matic Horvat"
      },
      {
        "id": 6,
        "worker_name": "Ana Pevec"
      }
    ],
    "files": [
      {
        "id": 4,
        "original_name": "bell4.png",
        "path_name": "1588148948237-bell4.png",
        "user_id": 1,
        "date": "2020-04-29T08:29:08.250Z",
        "type": "IMG",
        "note": null,
        "active": true
      }
    ],
    "signs": [
      {
        "id": 1,
        "original_name": "sign01.png",
        "path_name": "1590130693000-sign01.png",
        "user_id": 1,
        "date": "2020-05-22T06:58:13.013Z",
        "type": "IMG",
        "note": null,
        "active": true
      }
    ]
  }
}
```

## Dodajanje delovnega poročila
- url-aplikacije/api/workorders/ `POST`
	- ustvari novo delovno poročilo
	- vrne ustvarjeni delovni nalog
	- zahtevani so vsi vnosi razen locationCord in files; pri dodajanju novega poročila je zahtevan vsaj eden od podatkov projectId ali subscriberId, lahko sta oba
	- userId se prebere iz žetona

  | Vnosi         | Primer vnosa                  | Opomba  |
  | ------------- |:-----------------------------:|:-------:|
  | number        | 1/2020                        | trenutno je prosti vnos, vendar se zna to spremeniti, da se bo številka sestavila z kombinacijo id iz baze |
  | projectId     | 1                             |         |
  | subscriberId  | 4                             |         |
  | date          | 2020-04-20                    |         |
  | arrival       | 09:00                         |         |
  | departure     | 11:00                         |         |
  | description   | Popravilo programa, zamenjava senzorjev xxx in yyy|         |
  | type          | 2                             |         |
  | location      | Šentjur                       |         |
  | locationCord  | (12.44,15)                    |         |
  | vehicle       | Škoda                          |         |
  | representative| Janez Novak                            |         |
  | files         | 2,3,4	                            | niz id-jev datotek, ki so vezani na ta delovni nalog, brez presledkov     |
  | workers       | 1,6                           | niz id-jev uporabnikov, ki so vezani na ta delovni nalog, brez presledkov       |
  | signs         | 1                             | niz id-jev podpisov, ki so vezani na ta delovni nalog, brez presledkov       |	

## Posodabljanje delovnega naloga
- url-aplikacije/api/workorders/:id `PUT`
	- enak kot POST klic le da je tu potreben id delovnega naloga, ki se posodablja
	- vrne posodobljen delovni nalog

## Brisanje delovnega naloga
- url-aplikacije/api/workorders/:id `DELETE`
	- izbriše izbran delovno nalog: v bistvu se posodobi active na false
	- nazaj vrne to delovni nalog z active false

## Tipi delovnih nalog
- url-aplikacije/api/workorders/types `GET`
	- vrne seznam vseh tipov za delovne naloge

```javascript
{
  "types": [
    {
      "id": 1,
      "work_order_type": "servis"
    },
    {
      "id": 2,
      "work_order_type": "programiranje"
    },
    {
      "id": 3,
      "work_order_type": "montaža"
    },
    {
      "id": 4,
      "work_order_type": "zagon"
    },
    {
      "id": 5,
      "work_order_type": "storitev"
    },
    {
      "id": 6,
      "work_order_type": "svetovanje"
    },
    {
      "id": 7,
      "work_order_type": "pomoč"
    }
  ]
}
```

# Datoteke za delovne naloge - Work order files

Delovnim nalogam je možno dodati datoteke vrste pdf, word, excel, powepoint ter slike ('jpg', 'jpeg', 'png', 'gif'). Trenutno so datoteke omejene na velikosti maks 5MB. Do strežnika nimam dostopa zaradi varnostih razlogov. Iz istih razlogov slike niso dostopne preko javne mape ampak je potrebno klicati naslov za prenos datoteke. Ker so te datoteke vezane na delovne naloge sem raje naslov spremenil v /api/workorders/files.

## Dodajanje datoteke za delovni nalog
- url-aplikacije/api/workorders/files `POST`
	- v multipart form mora biti željena datoteka poimenova file, da jo bo api sprejel.
	- vrne podatke dodane datoteke (id, ime datoteke, pot datoteke, upor.id, datum, tip, note, active)

## Prenos izbrane datoteke
- url-aplikacije/api/workorders/files `GET`
	- zahteva filename in fileId v query
	- primer: url-aplikacije/api/workorders/files?filename=1587380292599-bell3.png&fileId=3

# Podpisi za delovne naloge - Work order signs

Delovnim nalogam je možno dodati podpise kot sliko ('jpg', 'jpeg', 'png', 'gif'). Trenutno so datoteke omejene na velikosti maks 5MB. Do strežnika nimam dostopa zaradi varnostih razlogov. Iz istih razlogov slike niso dostopne preko javne mape ampak je potrebno klicati naslov za prenos datoteke. Ker so podpisi vezani na delovne naloge je njihov api na naslovu: /api/workorders/signs. Obnašajo se enako kot datoteke delovnega naloga.

## Dodajanje podpisov za delovni nalog
- url-aplikacije/api/workorders/signs `POST`
	- v multipart form mora biti željena datoteka poimenova file, da jo bo api sprejel.
	- vrne podatke dodane datoteke (id, ime datoteke, pot datoteke, upor.id, datum, tip, note, active)

## Prenos izbranega podpisa
- url-aplikacije/api/workorders/signs `GET`
	- zahteva filename in signId v query
	- primer: url-aplikacije/api/workorders/signs?filename=1587380292599-bell3.png&signId=3

# Projekti - Projects

Seznam vseh projektov. Projekti so sortirani po tem ali so dokončani in po id. Torej so na vrhu bolj aktualni. Seznam vsebuje tudi neaktivne projekte, ker je lahko kakšno poročilo vezano na projekt, ki ni več v uporabi, lahko pa te tudi prefiltriram.

## Seznam vseh projektov, na katerih sodeluje uporabnik
- url-aplikacije/api/projects `GET`
	- vrne seznam vseh projektov na katerem sodeluje vpisani uporabnik

```javascript
{
  "projects": [
    {
      "project_id": 8,
      "project_number": "777/2019",
      "project_name": "Nova kišta",
      "subscriber_name": "Radenska",
      "subscriber_id": 5,
      "subscriber_icon": "1581318161118-radenska_adriatic_logo.jpg",
      "completion": 87,
      "finished": false,
      "start": "2019-10-25T05:00:00.000Z",
      "finish": "2019-12-31T14:00:00.000Z",
      "notes": "v bistvu gaming rig :)",
      "active": true
    },
    {
      "project_id": 5,
      "project_number": "xxx/2019",
      "project_name": "Testiram napake",
      "subscriber_name": "Razhroščejevalec",
      "subscriber_id": 3,
      "subscriber_icon": "1578465336558-83827-200.png",
      "completion": 20,
      "finished": false,
      "start": null,
      "finish": null,
      "notes": null,
      "active": false
    },
  ]
}
```
## Izbran projekt
- url-aplikacije/api/projects/:id `GET`
	- vrne izbrani projekt

```javascript
{
  "project": {
    "project_id": 5,
    "project_number": "xxx/2019",
    "project_name": "Testiram napake",
    "subscriber_name": "Razhroščejevalec",
    "subscriber_id": 3,
    "subscriber_icon": "1578465336558-83827-200.png",
    "completion": 20,
    "finished": false,
    "start": null,
    "finish": null,
    "notes": null,
    "active": false
  }
}
```
## Seznam vseh uporabnikovih poročil na izbranem projektu
- url-aplikacije/api/projects/reports/:id `GET`
	- vrne seznam vseh uporabnikovih poročil za izbrani projekt
	- dodal sem ta klic ker so poročila v večini vezana na projekte

# Naročniki - Subscribers

Seznam vseh naročnikov. Čisto na začetku sem naročnike prevedel v subscriber in jih vmes nisem popravljal, čeprav bi bolj ustrezalo client. Če je želja, lahko na api spremenim, da sprejemam namesto subscriberId clientId in vračam tudi client namesto subscriber.

## Seznam vseh naročnikov
- url-aplikacije/api/subscribers `GET`
	- vrne seznam vseh naročnikov

```javascript
{
  "subscribers": [
    {
      "id": 3,
      "name": "Razhroščejevalec",
      "icon": "1578465336558-83827-200.png",
      "active": true
    },
    {
      "id": 4,
      "name": "ROBOTEH",
      "icon": "1578465419765-roboteh.png",
      "active": true
    },
    {
      "id": 1,
      "name": "Google",
      "icon": "1578473748479-google.png",
      "active": true
    },
    {
      "id": 2,
      "name": "RTV SLO",
      "icon": "1580365300483-unnamed.jpg",
      "active": true
    },
    {
      "id": 5,
      "name": "Radenska",
      "icon": "1581318161118-radenska_adriatic_logo.jpg",
      "active": true
    },
    {
      "id": 6,
      "name": "Planet TV",
      "icon": null,
      "active": true
    }
  ]
}
```

## Izbran naročnik
- url-aplikacije/api/subscribers/:id `GET`
	- vrne izbranega naročnika

```javascript
{
  "subscriber": {
    "id": 5,
    "name": "Radenska",
    "icon": "1581318161118-radenska_adriatic_logo.jpg",
    "active": true
  }
}
```

## Dodajanje naročnika
- url-aplikacije/api/subscribers `POST`
	- doda novega naročnika. Ker obstaja možnost, da je zaposleni opravil delo pri naročniku, ki ga ni na seznamu, sem dodal možnost dodati novega naročnika.
	- zahteva name za ime novega naročnika, nazaj vrne ustvarnjega za pridobivanje novega id
	- nekateri naročniki imajo tudi pot do ikone v polju icon, ikona je dostopna v javni mapi url-aplikacije/uploads/images/pot-datoteke
	- v večini primerov gre za logo podjetja v png zapisu 200x200

## Seznam vseh uporabnikovih delovnih nalog za izbranega naročnika
- url-aplikacije/api/subscribers/workorders/:id `GET`
	- vrne seznam vseh uporabnikovih delovnih nalog pri izbranem naročniku
	- dodal sem ta klic ker so delovni nalogi v večini vezani na naročnike

# Uporabniki - Users

Seznam vseh uporabnikov. Dodal sem možnost pridobivanja tega seznama, ker pri delovnih nalogih lahko dela več ljudi hkrati. V aplikaciji imam nastavljen spusti seznam vseh zaposlenih, pri dodajanju novih delovnih nalog pa je dovolj samo id uporabnikov, ki so bili prisotni.

## Seznam vseh uporabnikov
- url-aplikacije/api/users `GET`
	- vrne seznam vseh zaposlenih, ki so vpisani v bazo. Seznam ima tudi neaktivne uporabnike iz istih razlogov kot drugje (active = false; pri delovnem nalogu je lahko nek uporabnik, ki ni več zaposlen).
	- seznam je urejen priimku in imenu (lahko tudi spremenim)

```javascript
{
  "users": [
    {
      "id": 8,
      "name": "Kosobrincl Dolgobradi",
      "active": true
    },
    {
      "id": 7,
      "name": "Bedanec Izkoče",
      "active": true
    },
    {
      "id": 10,
      "name": "Peter Kjoka",
      "active": true
    },
    {
      "id": 15,
      "name": "Kapitan Kljuka",
      "active": true
    }
  ]
}
```

## Izbrani naročnik
- url-aplikacije/api/users/:id `GET`
	- vrne izbranega uporabnika

```javascript
{
  "user": {
    "id": 1,
    "name": "ADMIN UPORABNIK",
    "active": true
  }
}
```

# Napake

Ko pride do napake bo API odgovoril z 400, če niso vsi zahtevani vnosi, drugače pa z 500. Primer napake, ko ni pravilen vnos za začetek pri poročilih:

```javascript
{
  "status": "error",
  "message": "Something went wrong",
  "error": {
    "name": "error",
    "length": 175,
    "severity": "ERROR",
    "code": "22007",
    "file": "d:\\pginstaller_12.auto\\postgres.windows-x64\\src\\backend\\utils\\adt\\datetime.c",
    "line": "3774",
    "routine": "DateTimeParseError"
  }
}
```