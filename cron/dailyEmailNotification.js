const cron = require('node-cron');
const nodemailer = require('nodemailer');
const dbAbsences = require('../model/absences/dbAbsences');
const dbEmployees = require('../model/employees/dbEmployees');
const dateFormat = require('dateformat');

// Function to format date
function formatDate(date) {
  return {
    "date": dateFormat(date, "d. m. yyyy"),
    "time": dateFormat(date, "HH:MM")
  };
}

// Function to send email
async function sendEmail(recipients, subject, htmlMessage, textMessage) {
  let transporter;
  if(process.env.MAIL_HOST){
    transporter = nodemailer.createTransport({
      host: process.env.MAIL_HOST,
      port: process.env.MAIL_PORT,
      secure: true,
      auth: {
        user: process.env.MAIL_USERNAME,
        pass: process.env.MAIL_PASSWORD
      },
      tls: {
        // do not fail on invalid certs
        rejectUnauthorized: false
      }
    });
  }
  if(process.env.ETHEREAL_HOST){
    transporter = nodemailer.createTransport({
      host: process.env.ETHEREAL_HOST,
      port: process.env.ETHEREAL_PORT,
      auth: {
        user: process.env.ETHEREAL_USERNAME,
        pass: process.env.ETHEREAL_PASSWORD
      },
    }); 
  }

  let attachmentsElement = [
    {
      filename: 'kuka.png',
      path: __dirname + `/..//icons/logo/kuka_partner.png`,
      cid: 'kuka@partner'
    }
  ];

  htmlMessage += `<img src="cid:kuka@partner" alt="Kuka Partner" />`;

  let mailOptions = {
    from: 'odsotnosti@roboteh.si',
    to: recipients.join(','),
    subject: subject,
    text: textMessage,
    html: htmlMessage,
    attachments: attachmentsElement
  };

  try {
    let info = await transporter.sendMail(mailOptions);
    console.log('Message sent: %s', info.messageId);
  } catch (error) {
    console.error('Error sending email: %s', error.message);
  }
}

// Schedule the cron job to run every day at 7:00 AM except for weekends
cron.schedule('0 7 * * 1-5', async () => {
  console.log('Running daily email notification job...');

  try {
    let absences = await dbAbsences.getAbsences(null, null, null, '3', null, null);
    let users = await dbEmployees.getUsersWithSupervisors(null, 1);
    let roles = await dbEmployees.getRolesWithSupervisors(null);

    const bufferTime = 5 * 60 * 1000;

    for (const absence of absences) {
      let supervisorsEmails = [];
      let user = users.find(u => u.id == absence.id_user);
      let role = roles.find(r => r.id == user.role_id);
      let userSupervisors = user.supervisors_ids ? user.supervisors_ids.split(',') : [];
      let roleSupervisors = role.supervisor_ids ? role.supervisor_ids.split(',') : [];
      supervisorsEmails = userSupervisors.concat(roleSupervisors);
      supervisorsEmails = supervisorsEmails.filter((v, i, a) => a.indexOf(v) === i);
      supervisorsEmails = supervisorsEmails.map(s => users.find(u => u.id == s).work_mail);

      let now = new Date();
      let firstNotificationDate = absence.notification_date ? new Date(absence.notification_date) : null;
      let reminderDate = absence.reminder_date ? new Date(absence.reminder_date) : null;
      let start = new Date(absence.start);
      let finish = new Date(absence.finish);
      start = formatDate(start);
      finish = formatDate(finish);

      let htmlMessage, textMessage;

      if (!firstNotificationDate) {
        htmlMessage = `<h2> Obvestilo o novi odsotnosti ${absence.reason} </h2>
        <p>Pozdravljeni,</p>
        <p>Obveščamo vas, da je bila dodana osebi <b>${user.name} ${user.surname}</b> nova odsotnost <i>${absence.reason}</i> od <b>${start.date}</b> do <b>${finish.date}</b>.</p>
        <p>Lep pozdrav,
        <br/>Roboteh Informacijski Sistem</p>`;
        textMessage = `Pozdravljeni,
        Obveščamo vas, da je bila dodana osebi ${user.name} ${user.surname} nova odsotnost ${absence.reason} od ${start.date} do ${finish.date}.
        Lep pozdrav,
        Roboteh Informacijski Sistem`;

        if (supervisorsEmails.length > 0) {
          await sendEmail(supervisorsEmails, 'Odsotnost - čakanje na odobritev', htmlMessage, textMessage);
        }
        await dbAbsences.updateAbsenceNotificationMails(absence.id, supervisorsEmails.toString(), now);

      } else if (!reminderDate && now - firstNotificationDate >= 2 * 24 * 60 * 60 * 1000 - bufferTime) {
        htmlMessage = `<h2> Opomnik za odsotnost ${absence.reason} - čakanje na odobritev </h2>
        <p>Pozdravljeni,</p>
        <p>Obveščamo vas, da je bila dodana osebi <b>${user.name} ${user.surname}</b> nova odsotnost <i>${absence.reason}</i> od <b>${start.date}</b> do <b>${finish.date}</b>.</p>
        <p>Lep pozdrav,
        <br/>Roboteh Informacijski Sistem</p>`;
        textMessage = `Pozdravljeni,
        Obveščamo vas, da je bila dodana osebi ${user.name} ${user.surname} nova odsotnost ${absence.reason} od ${start.date} do ${finish.date}.
        Lep pozdrav,
        Roboteh Informacijski Sistem`;

        if (supervisorsEmails.length > 0) {
          await sendEmail(supervisorsEmails, 'Opomnik za odsotnost - čakanje na odobritev', htmlMessage, textMessage);
        }
        await dbAbsences.updateAbsenceReminderMails(absence.id, supervisorsEmails.toString(), now);
      } else if (reminderDate && now - reminderDate >= 2 * 24 * 60 * 60 * 1000 - bufferTime) {
        htmlMessage = `<h2> Zadnji opomnik za odsotnost ${absence.reason} - čakanje na odobritev </h2>
        <p>Pozdravljeni,</p>
        <p>Obveščamo vas, da je bila dodana osebi <b>${user.name} ${user.surname}</b> nova odsotnost <i>${absence.reason}</i> od <b>${start.date}</b> do <b>${finish.date}</b>.</p>
        <p>Lep pozdrav,
        <br/>Roboteh Informacijski Sistem</p>`;
        textMessage = `Pozdravljeni,
        Obveščamo vas, da je bila dodana osebi ${user.name} ${user.surname} nova odsotnost ${absence.reason} od ${start.date} do ${finish.date}.
        Lep pozdrav,
        Roboteh Informacijski Sistem`;

        let ceoEmails = ['srecko.potocnik@roboteh.si', 'cveto.jakopic@roboteh.si', 'bostjan.skarlovnik@roboteh.si'];
        await sendEmail(ceoEmails, 'Zadnji opomnik za odsotnost', htmlMessage, textMessage);
        await dbAbsences.updateAbsenceAdminMails(absence.id, ceoEmails.toString(), now);
      }
    }

    console.log('Email notifications sent successfully.');
  } catch (error) {
    console.error('Error running daily email notification job:', error.message);
  }
});

console.log('Cron job scheduled: Daily email notification every day at 7:00 AM except for weekends');