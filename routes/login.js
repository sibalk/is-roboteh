var express = require('express');
var router = express.Router();
const csp = require('helmet-csp');
var auth = require('../controllers/authentication');

var loginCspHandler = csp({
  directives: {
    defaultSrc: ["'self'"],
    scriptSrc: ["'self'"],
    styleSrc: ["'self'", "https://fonts.googleapis.com"],
    fontSrc: ["'self'", "https://fonts.gstatic.com"],
    frameAncestors: ["'none'"],
  }
});
var normalCspHandler = csp({
  directives: {
    defaultSrc: ["'self'"],
    scriptSrc: ["'self'"],
    styleSrc: ["'self'", "https://fonts.googleapis.com", "https://use.fontawesome.com"],
    fontSrc: ["'self'", "https://fonts.gstatic.com", "https://use.fontawesome.com"],
    imgSrc: ["'self'", "data:"],
    frameAncestors: ["'self'"],
  }
});

router.get('/', normalCspHandler, auth.authenticate, function(req, res, next){
  //console.log(req);
  res.render('dashboard', {
    title: "Osnovna plošča",
    user_name: req.session.user_name,
    user_surname: req.session.user_surname,
    user_username: req.session.user_username,
    user_typeid: req.session.role_id,
    user_type: req.session.type,
    user_id: req.session.user_id,
  })
})
router.get('/login', loginCspHandler, function(req, res, next){
  res.render('login');
});

router.post('/login', loginCspHandler, auth.loginReRedo);

router.get('/logout', loginCspHandler, function(req, res, next){
  if(req.session)
    req.session = null;
  res.redirect('login');
});
router.get('/bootstrap', loginCspHandler, function(req, res, next){
  res.redirect('/');
});
module.exports = router;