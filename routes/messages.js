var express = require('express');
var router = express.Router();

//auth & dbModels
var auth = require('../controllers/authentication');
var dbMessages = require('../model/messages/dbMessages');

//get all messages for specific id
router.get('/get', auth.authenticate, function(req, res, next){
  let projectId = req.query.projectId;
  let taskId = req.query.taskId;
  let subtaskId = req.query.subtaskId;
  dbMessages.getMessages(projectId,taskId,subtaskId).then(messages=>{
    res.json({success:true, messages:messages});
  })
  .catch((e)=>{
    return res.json({success:false, error:e});
  })
})
//add new message
router.post('/post', auth.authenticate, function(req, res, next){
  let projectId;
  let taskId;
  let subtaskId;
  let lastMessageId = req.body.lastMessageId;
  let messagesId = req.body.messagesId;
  let messagesType = req.body.messagesType;
  let newMessage = req.body.newMessage;
  if(messagesType == 0)
    projectId = messagesId;
  else if(messagesType == 1)
    taskId = messagesId;
  else if(messagesType == 2)
    subtaskId = messagesId;
  //get new messages if there are any new
  dbMessages.refreshMessages(projectId,taskId,subtaskId,lastMessageId).then(newMessages=>{
    dbMessages.addNewMessage(newMessage,req.session.user_id,projectId,taskId,subtaskId).then(newDate=>{
      res.json({success:true, newMessages:newMessages,date:newDate});
    })
    .catch((e)=>{
      return res.json({success:false, error:e});
    })
  })
  .catch((e)=>{
    return res.json({success:false, error:e});
  })
})
//get any missed messages
router.get('/refresh', auth.authenticate, function(req, res, next){
  let projectId;
  let taskId;
  let subtaskId;
  let lastMessageId = req.query.lastMessageId;
  let messagesId = req.query.messagesId;
  let messagesType = req.query.messagesType;
  if(messagesType == 0)
    projectId = messagesId;
  else if(messagesType == 1)
    taskId = messagesId;
  else if(messagesType == 2)
    subtaskId = messagesId;
  //get new messages if there are any new
  dbMessages.refreshMessages(projectId,taskId,subtaskId,lastMessageId).then(newMessages=>{
    res.json({success:true, newMessages:newMessages});
  })
  .catch((e)=>{
    return res.json({success:false, error:e});
  })
})
module.exports = router;