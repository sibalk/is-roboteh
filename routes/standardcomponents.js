var express = require('express');
var router = express.Router();
var Jimp = require('jimp');
const csp = require('helmet-csp');
//var dateFormat = require('dateformat');
//var fs = require('fs'); // --TEST--

//auth & dbModels
var auth = require('../controllers/authentication');
let dbSubscribers = require('../model/subscribers/dbSubscribers');
let dbChanges = require('../model/changes/dbChanges');
let dbStdComponents = require('../model/stdComponents/dbStdComponents');
//add db for standard components

var normalCspHandler = csp({
  directives: {
    defaultSrc: ["'self'"],
    scriptSrc: ["'self'"],
    styleSrc: ["'self'", "https://fonts.googleapis.com", "https://use.fontawesome.com"],
    fontSrc: ["'self'", "https://fonts.gstatic.com", "https://use.fontawesome.com"],
    imgSrc: ["'self'", "data:"],
    frameAncestors: ["'self'"],
  }
});

//get page with all subscribers
router.get('/', normalCspHandler, auth.authenticate, function(req, res){
  res.render('standard_components', {
    title: "Standardne komponente",
    user_name: req.session.user_name,
    user_surname: req.session.user_surname,
    user_username: req.session.user_username,
    user_typeid: req.session.role_id,
    user_type: req.session.type,
    user_id: req.session.user_id,
    msg: '',
    type: 0
  })
})

//get all compontents and their tzpes
router.get('/all', auth.authenticate, function(req, res, next){
  // let promises = [];
  // promises.push(dbStdComponents.getAllStdComponents());
  // promises.push(dbStdComponents.getAllStdCompTypes());

  // Promise.all(promises).then(results => {
  //   res.json({success:true, stdComponents: results[0], stdCompTypes: results[1]});
  // })
  // .catch((e)=>{
  //   return res.json({success:false, error: e, msg: 'Napaka pri pridobivanju podatkov standardnih komponent in tipov.'});
  // })
  let stdCompActivity = (req.query.stdCompActivity) ? req.query.stdCompActivity : '1';

  dbStdComponents.getAllStdComponents(stdCompActivity).then(stdComponents => {
    res.json({success:true, stdComponents});
  })
  .catch((e)=>{
    return res.json({success:false, error: e, msg: 'Napaka pri pridobivanju podatkov standardnih komponent in tipov.'});
  })
})
//add new standard component
router.post('/', auth.authenticate, function(req, res, next){
  let manufacturer = req.body.manufacturer ? req.body.manufacturer : null;
  let name = req.body.name ? req.body.name : null;
  let type = req.body.type ? req.body.type : null;
  let orderNumber = req.body.orderNumber ? req.body.orderNumber : null;
  let ourPrice = req.body.ourPrice ? req.body.ourPrice : null;
  let listPrice = req.body.listPrice ? req.body.listPrice : null;
  let roleType = req.session.type.toLowerCase();

  if (!manufacturer || !name){
    return res.json({success:false, msg:'Manjkajoči podatki. Ni proizvajalca oz. naziva standardne komponente.'});
  }
  if (roleType == 'admin' || roleType == 'tajnik' || roleType == 'komercialist' || roleType == 'komerciala' || roleType == 'računovodstvo' || roleType.substring(0,5) == 'vodja'){
    dbStdComponents.addStdComponent(manufacturer, name, type, orderNumber, ourPrice, listPrice).then(stdComp => {
      return res.json({success:true, stdComp});
    })
    .catch((e) => {
      return res.json({success:false, error:e});
    })
  }
  else{
    return res.json({success:false, msg:'Uporabnik nima pravice dodajati standardnih komponent.'});
  }
})
//edit standard component
router.put('/', auth.authenticate, function(req, res, next){
  let stdCompId = req.body.stdCompId;
  let manufacturer = req.body.manufacturer ? req.body.manufacturer : null;
  let name = req.body.name ? req.body.name : null;
  let type = req.body.type ? req.body.type : null;
  let orderNumber = req.body.orderNumber ? req.body.orderNumber : null;
  let ourPrice = req.body.ourPrice ? req.body.ourPrice : null;
  let listPrice = req.body.listPrice ? req.body.listPrice : null;
  let roleType = req.session.type.toLowerCase();
  if (!manufacturer || !name){
    return res.json({success:false, msg:'Manjkajoči podatki. Ni proizvajalca oz. naziva standardne komponente.'});
  }
  if (roleType == 'admin' || roleType == 'tajnik' || roleType == 'komercialist' || roleType == 'komerciala' || roleType == 'računovodstvo' || roleType.substring(0,5) == 'vodja'){
    dbStdComponents.editStdComponent(stdCompId, manufacturer, name, type, orderNumber, ourPrice, listPrice).then(stdComp => {
      return res.json({success:true, stdComp});
    })
    .catch((e) => {
      return res.json({success:false, error:e});
    })
  }
  else{
    return res.json({success:false, msg:'Uporabnik nima pravice urejati standardnih komponent.'});
  }
})
//delete std component ( update std comp active attribute )
router.delete('/', auth.authenticate, function(req, res, next){
  let stdCompId = req.body.stdCompId;
  let active = req.body.active;
  // let active = req.body.active ? req.body.active : 'false';
  dbStdComponents.deleteStdComponent(stdCompId, active).then(stdComp => {
    return res.json({success:true, stdComp});
  })
  .catch((e)=>{
    return res.json({success:false, error:e});
  })
})

module.exports = router;