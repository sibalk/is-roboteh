var express = require('express');
var router = express.Router();
var dateFormat = require('dateformat');
var multer = require('multer');
var fs = require('fs');
const fsp = require('fs').promises;
var Jimp = require('jimp');
var path = require('path');
const csp = require('helmet-csp');

//auth & db
var auth = require('../controllers/authentication');
var dbComplaints = require('../model/complaints/dbComplaints');

//routing forward
let routerPdfComplaints = require('./pdfcomplaints');
router.use('/pdfs', routerPdfComplaints);

var normalCspHandler = csp({
  directives: {
    defaultSrc: ["'self'"],
    scriptSrc: ["'self'"],
    styleSrc: ["'self'", "https://fonts.googleapis.com", "https://use.fontawesome.com"],
    fontSrc: ["'self'", "https://fonts.gstatic.com", "https://use.fontawesome.com"],
    imgSrc: ["'self'", "data:"],
    frameAncestors: ["'self'"],
  }
});

function formatDate(date){
  return {
    "date": dateFormat(date, "d. m. yyyy"),
    "time": dateFormat(date, "HH:MM")
  }
}
//storage for documents
var storageDoc = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, 'files/complaints/')
  },
  filename: function (req, file, cb) {        
    // null as first argument means no error
    cb(null, Date.now() + '-' + file.originalname )
  }
});
//upload when adding subscriber image
var uploadDoc = multer({
  storage: storageDoc,
  limits: {
    fileSize: 5300000
  },
  fileFilter: function(req, file, cb){
    sanitizeFileDocument(file,cb);
  }
}).single('docNew');
//function to check if file is indeed document
function sanitizeFileDocument(file, cb){
  //what file extentions are ok
  let fileExts = ['pdf', //pdf 
                'doc', 'dot', 'wbk', 'docx', 'docm', 'dotx', 'dotm', 'docb', //word
                'xls', 'xlt', 'xlm', 'xlsx', 'xlsm', 'xltx', 'xltm', 'xlsb', 'xla', 'xlam', 'xll', 'xlw', //excel
                'ppt', 'pot', 'pps', 'pptx', 'pptm', 'potx', 'potm', 'ppam', 'ppsx', 'ppsm', 'sldx', 'sldm', //powepoint
                'zip', 'rar', //zips
                'txt', //txt
                'jpg', 'jpeg', 'png', 'gif']; //images
                //access has way diffrent ext and they probably wont use them
                //other file ext will be added when there will be request
  // MAYBE TODO add isAlowedMimeType back and test for all this type of extentions
  //check if file has no exts
  let fileExtsArray = file.originalname.split(".");
  if(fileExtsArray.length == 1)
    return cb('Datoteka brez končnice ni dovoljena');
  //check alowed exts
  let isAlowedExt = fileExts.includes(file.originalname.split(".")[1].toLowerCase());
  //mime type must be an image
  //let isAlowedMimeType = file.mimetype.startsWith("document/");

  if(isAlowedExt){
    //no errors
    return cb(null, true);
  }
  else{
    //error, not an image
    cb('Ta vrsta datoteke ni dovoljena!');
  }
}

///MAIN PAGE
router.get('/', normalCspHandler, auth.authenticate, function(req, res){
  //sent page for complaints
  res.render('complaints', {
    title: "Reklamacije",
    user_name: req.session.user_name,
    user_surname: req.session.user_surname,
    user_typeid: req.session.role_id,
    user_type: req.session.type,
    user_id: req.session.user_id,
  });
});
// //get all info (subscribers complaints, supplier complaint, users, error type, projects, subscribers)
// router.get('/info', auth.authenticate, function(req, res, next){
//   let meetingsActivity = (req.query.meetingsActivity) ? req.query.meetingsActivity : 1;
//   let activitiesActivity = (req.query.activitiesActivity) ? req.query.activitiesActivity : 1;
//   let activitiesStatus = (req.query.activitiesStatus) ? req.query.activitiesStatus : 0;
//   let promises = [];
//   promises.push(dbProjects.getWorkers());
//   promises.push(dbMeetings.getAllActivities(activitiesActivity,activitiesStatus));
//   promises.push(dbMeetings.getMeetings(meetingsActivity));
//   promises.push(dbMeetings.getStatus());
//   Promise.all(promises).then(result => {
//     return res.json({success:true, users:result[0], activities:result[1], meetings:result[2], status:result[3]});
//   })
//   .catch((e)=>{
//     return res.json({success:false, error:e});
//   })
// })
//////////////////////////////////////////////////////////////////////////////////////////////////COMPLAINTS SUBSCRIBERS - REKLAMACIJE NAROČNIKOV
//get all subscriber's complaints
router.get('/subscribers', auth.authenticate, function(req, res, next){
  let compSubActivity = (req.query.compSubActivity) ? req.query.compSubActivity : '1';
  let errorTypeFilter = (req.query.errorTypeFilter) ? req.query.errorTypeFilter : '0';
  compSubActivity = '0', errorTypeFilter = '0';
  dbComplaints.getComplaintsSubscribers(compSubActivity,errorTypeFilter).then(allCompSub => {
    return res.json({success:true, allCompSub});
  })
  .catch((e)=>{
    return res.json({success:false, error:e});
  })
})
//get all subscriber's complaints
router.get('/subscribers/errorTypes', auth.authenticate, function(req, res, next){
  // let compSubActivity = (req.query.compSubActivity) ? req.query.compSubActivity : 1;
  // let errorTypeFilter = (req.query.errorTypeFilter) ? req.query.errorTypeFilter : 0;
  dbComplaints.getErrorTypes().then(allErrorTypes => {
    return res.json({success:true, allErrorTypes});
  })
  .catch((e)=>{
    return res.json({success:false, error:e});
  })
})
//add new complaint subscriber
router.post('/subscribers', auth.authenticate, function(req, res, next){
  // let compSubId = req.body.id;
  let number = req.body.number;
  let date = req.body.date;
  let subscriberId = req.body.subscriberId;
  let contact = req.body.contact;
  let projectId = req.body.projectId;
  let description = req.body.description;
  let errorTypeId = req.body.errorTypeId;
  let cause = req.body.cause;
  let measure = req.body.measure;
  let finished = req.body.finished;
  let justification = req.body.justification;
  let cost = req.body.cost;
  let notes = req.body.notes;
  //let contactUsers = req.body.contactUsers;
  let solver = req.body.solver;
  let status = req.body.status;
  if(!number || !date || !subscriberId || !errorTypeId){
    return res.json({success:false, msg:'Manjkajoči podatki. Ni številke reklamacije oz. datuma oz. naročnika oz. tip napake.'});
  }
  dbComplaints.getComplaintsSubscribers(1,0).then(allCompSubs => {
    var tmp = allCompSubs.find(cs => cs.complaint_number == number);
    if(tmp)
      return res.json({success:false, msg:'Številka reklamacije že obstaja v bazi reklamacij naročnikov.'});
    //everything ok, continue like normal add
    dbComplaints.addCompSub(number, date, subscriberId, contact, projectId, description, errorTypeId, cause, measure, finished, justification, cost, notes, status, solver).then(compSub => {
      // if(contactUsers.length > 0){
      //   //contact for resolve users
      //   let promises = [];
      //   for(let i = 0; i < contactUsers.length; i++){
      //     promises.push(dbComplaints.addUserToCompSub(contactUsers[i], compSub.id));
      //   }
      //   Promise.all(promises).then(contUsers => {
      //     return res.json({success:true, compSub, contUsers});
      //   })
      //   .catch((e)=>{
      //     return res.json({success:false, error:e});
      //   })
      // }
      // else{
      //   //no contact user for resolve
      //   return res.json({success:true, compSub, contUsers:[]});
      // }
      return res.json({success:true, compSub, contUsers:[]});
    })
    .catch((e)=>{
      return res.json({success:false, error:e});
    })
  })
  .catch((e) => {
    return res.json({success:false, error:e});
  })
})
//edit complaint subscriber
router.put('/subscribers', auth.authenticate, function(req, res, next){
  let compSubId = req.body.id;
  let number = req.body.number;
  let date = req.body.date;
  let subscriberId = req.body.subscriberId;
  let contact = req.body.contact;
  let projectId = req.body.projectId;
  let description = req.body.description;
  let errorTypeId = req.body.errorTypeId;
  let cause = req.body.cause;
  let measure = req.body.measure;
  let finished = req.body.finished;
  let justificationId = req.body.justificationId;
  let cost = req.body.cost;
  let notes = req.body.notes;
  let solver = req.body.solver;
  // let addContactUsers = req.body.addContactUsers;
  // let deleteContactUsers = req.body.deleteContactUsers;
  //let present = req.body.present.split(',');
  //console.log(present);
  //return res.json({success:true, message:'just testing.'});
  if (!number || !date || !subscriberId || !errorTypeId){
    return res.json({success:false, msg:'Manjkajoči podatki. Ni številke reklamacije  oz. datuma vnosa oz. naročnika oz. tip napake.'});
  }
  else{
    //check if number is stil free or same as it was
    dbComplaints.getComplaintsSubscribers(1,0).then(allCompSubs => {
      let tmpConflict = allCompSubs.find(cs => cs.complaint_number == number && cs.id != compSubId);
      if (tmpConflict){
        return res.json({success:false, msg:'Številka reklamacije je že zasedena v bazi reklamacij naročnikov.'});
      }
      else{
        let updatePromises = [];
        updatePromises.push(dbComplaints.updateCompSub(compSubId, number, date, subscriberId, contact, projectId, description, errorTypeId, cause, measure, finished, justificationId, cost, notes, solver));
        // for(let i = 0, n = addContactUsers.length; i < n; i++){
        //   updatePromises.push(dbComplaints.addUserToCompSub(addContactUsers[i], compSubId));
        // }
        // for(let i = 0, n = deleteContactUsers.length; i < n; i++){
        //   updatePromises.push(dbComplaints.deleteUserToCompSub(deleteContactUsers[i], compSubId));
        // }
        Promise.all(updatePromises).then(result => {
          return res.json({success:true, compSub:result[0]});
        })
        .catch((e)=>{
          return res.json({success:false, error:e});
        })
      }
    })
    .catch((e)=> {
      return res.json({success:false, error:e});
    })
  }
})
//delete comp sub ( update comp sub active attribute )
router.delete('/subscribers', auth.authenticate, function(req, res, next){
  let compSubId = req.body.compSubId;
  let active = req.body.active;
  dbComplaints.deleteCompSub(compSubId, active).then(compSub => {
    return res.json({success:true, compSub});
  })
  .catch((e)=>{
    return res.json({success:false, error:e});
  })
})
//edit modification (status || name && category)
router.put('/subscribers/status', auth.authenticate, function(req, res, next){
  let compSubId = req.body.compSubId;
  let compSubStatusId = req.body.compSubStatusId;
  dbComplaints.updateCompSubStatus(compSubId, compSubStatusId).then(compSub => {
    return res.json({success:true, compSub});
  })
  .catch((e)=>{
    return res.json({success:false, error:e});
  })
})
//get all complaints justification
router.get('/subscribers/justification', auth.authenticate, function(req, res, next){
  dbComplaints.getCompJustification().then(allCompJustification => {
    return res.json({success:true, allCompJustification});
  })
  .catch((e)=>{
    return res.json({success:false, error:e});
  })
})
//////////////////////////////////////////////////////////////////////////////////////////////////COMPLAINTS SUPPLIERS - REKLAMACIJE DOBAVITELJEM
//get all supplier's complaints
router.get('/suppliers', auth.authenticate, function(req, res, next){
  //let compSupActivity = (req.query.compSupActivity) ? req.query.compSupActivity : 1;
  let compSupActivity = '0';
  dbComplaints.getComplaintsSuppliers(compSupActivity).then(allCompSup => {
    return res.json({success:true, allCompSup});
  })
  .catch((e)=>{
    return res.json({success:false, error:e});
  })
})
//add new complaint suppliers
router.post('/suppliers', auth.authenticate, function(req, res, next){
  // let compSupId = req.body.id;
  let number = req.body.number;
  let date = req.body.date;
  let supplier = req.body.supplier;
  let material = req.body.material;
  let compType = req.body.compType;
  let justification = req.body.justification;
  let quantity = req.body.quantity;
  let value = req.body.value;
  let status = req.body.status;
  if(!number || !date || !supplier){
    return res.json({success:false, msg:'Manjkajoči podatki. Ni številke reklamacije oz. datuma oz. dobavitelja.'});
  }
  dbComplaints.getComplaintsSuppliers(1,0).then(allCompSup => {
    var tmp = allCompSup.find(cs => cs.complaint_number == number);
    if(tmp)
      return res.json({success:false, msg:'Številka reklamacije že obstaja v bazi reklamacij dobaviteljev.'});
    //everything ok, continue like normal add
    dbComplaints.addCompSup(number, date, supplier, material, compType, quantity, value, status, justification).then(compSup => {
      return res.json({success:true, compSup});
    })
    .catch((e)=>{
      return res.json({success:false, error:e});
    })
  })
  .catch((e) => {
    return res.json({success:false, error:e});
  })
})
//edit complaint supplier
router.put('/suppliers', auth.authenticate, function(req, res, next){
  let compSupId = req.body.id;
  let number = req.body.number;
  let date = req.body.date;
  let supplier = req.body.supplier;
  let material = req.body.material;
  let compType = req.body.compType;
  let justificationId = req.body.justificationId;
  let quantity = req.body.quantity;
  let value = req.body.value;
  // let status = req.body.status;
  //let present = req.body.present.split(',');
  //console.log(present);
  //return res.json({success:true, message:'just testing.'});
  if (!number || !date || !supplier){
    return res.json({success:false, msg:'Manjkajoči podatki. Ni številke reklamacije  oz. datuma vnosa oz. dobavitelja.'});
  }
  else{
    //check if number is stil free or same as it was
    dbComplaints.getComplaintsSuppliers(1).then(allCompSup => {
      let tmpConflict = allCompSup.find(cs => cs.complaint_number == number && cs.id != compSupId);
      if (tmpConflict){
        return res.json({success:false, msg:'Številka reklamacije je že zasedena v bazi reklamacij dobaviteljem.'});
      }
      else{
        dbComplaints.updateCompSup(compSupId, number, date, supplier, material, compType, quantity, value, justificationId).then(compSup => {
          return res.json({success:true, compSup});
        })
        .catch((e) => {
          return res.json({success:false, error:e});
        })
      }
    })
    .catch((e)=> {
      return res.json({success:false, error:e});
    })
  }
})
//delete comp sup ( update comp sup active attribute )
router.delete('/suppliers', auth.authenticate, function(req, res, next){
  let compSupId = req.body.compSupId;
  let active = req.body.active;
  dbComplaints.deleteCompSup(compSupId, active).then(compSup => {
    return res.json({success:true, compSup});
  })
  .catch((e)=>{
    return res.json({success:false, error:e});
  })
})
//edit modification (status || name && category)
router.put('/suppliers/status', auth.authenticate, function(req, res, next){
  let compSupId = req.body.compSupId;
  let compSupStatusId = req.body.compSupStatusId;
  dbComplaints.updateCompSupStatus(compSupId, compSupStatusId).then(compSup => {
    return res.json({success:true, compSup});
  })
  .catch((e)=>{
    return res.json({success:false, error:e});
  })
})
///////////////////////////////////////////////////////////////////////////////////////////////////////// FILES
//Get project's files
router.get('/files', auth.authenticate, function(req, res, next){
  let compSubFileId = req.query.compSubFileId;
  let compSupFileId = req.query.compSupFileId;
  dbComplaints.getComplaintFiles(compSubFileId,compSupFileId).then(files=>{
    res.json({success:true, data: files});
  })
  .catch((e)=>{
    return res.json({success:false, error: e});
  })
})
//send files that is hidden from public and is connected to complaints
router.get('/files/resources', auth.authenticate, (req,res)=>{
  let fileName = req.query.fileName;
  //res.sendFile(path.resolve('files/projects/' + fileName));
  if (fileName.substring(fileName.length-3).toLocaleLowerCase() == 'pdf'){
    res.set({"Content-Type": "application/pdf",});
  }
  else if (fileName.substring(fileName.length-3).toLocaleLowerCase() == 'doc' || fileName.substring(fileName.length-4).toLocaleLowerCase() == 'docx'){
    res.set({"Content-Type": "application/msword",});
  }
  else if (fileName.substring(fileName.length-3).toLocaleLowerCase() == 'ppt'){
    res.set({"Content-Type": "application/mspowerpoint",});
  }
  else if (fileName.substring(fileName.length-3).toLocaleLowerCase() == 'xls'){
    res.set({"Content-Type": "application/excel",});
  }
  else if (fileName.substring(fileName.length-3).toLocaleLowerCase() == 'zip'){
    res.set({"Content-Type": "application/x-compressed",});
  }
  else if (fileName.substring(fileName.length-3).toLocaleLowerCase() == 'png'){
    res.set({"Content-Type": "image/png",});
  }
  else if (fileName.substring(fileName.length-3).toLocaleLowerCase() == 'jpg' || fileName.substring(fileName.length-4).toLocaleLowerCase() == 'jpeg'){
    res.set({"Content-Type": "image/jpeg",});
  }
  else if (fileName.substring(fileName.length-3).toLocaleLowerCase() == 'gif'){
    res.set({"Content-Type": "image/gif",});
  }
  else
    res.set({"Content-Type": "text/plain",});
  if (fs.existsSync(path.resolve('files/complaints/' + fileName))) {
    fs.createReadStream(path.resolve('files/complaints/' + fileName))
    .pipe(res);
  }
  else{
    res.json({success:false, msg:'Datoteka ne obstaja'});
  }
})
//get thumbnails for img and later for pdf as well
router.get('/files/thumbnail', auth.authenticate, function(req, res, next){
  let fileId = req.query.fileId;
  let filePath = req.query.filePath;
  let fileType = req.query.fileType;
  let fileLocation = `${__dirname}/../files/complaints/${filePath}`;
  let fileBase64 = '';
  if (fileType == 'IMG'){
    Jimp.read(fileLocation).then(img => {
      img.resize(150, Jimp.AUTO).quality(20).getBase64Async(Jimp.AUTO).then(base64 => {
        return res.json({success:true, data: base64, id: fileId, type: 'IMG'});
      })
      .catch(err => {
        return res.json({success:false, error: e});
      });
    })
    .catch((e) => {
      return res.json({success:false, error: e, msg: 'Datoteka ne obstaja.', id: fileId, type: fileType});
    })
  }
  else if (fileType == 'PDF'){
    if (fs.existsSync(fileLocation)) {
     fsp.readFile(fileLocation, 'base64').then(data => {
        //console.log(data);
        return res.json({success:true, data, id: fileId, type: 'PDF'});
      })
      .catch((e) => {
        return res.json({success:false, error: e});
      })
    }
    else{
      res.json({success:false, msg:'Datoteka ne obstaja'});
    }
    //return res.json({success:false, mgs:'not supported type yet'});
  }
  else{
    return res.json({success:false, mgs:'not supported type'});
  }
})
//upload for new file
router.post('/fileUpload', auth.authenticate, (req, res, next) => {
  //debugger;
  //save file and if no error send back json success true
  uploadDoc(req, res, (err) =>{
    //console.log("img upload");
    if(err){
      let msg = 'Napaka pri nalaganju datoteke na strežnik.';
      if(err.message == 'File too large')
        msg = 'Velikost datoteke je prevelika. Trenutno so dovoljene datoteke do 5MB.';
      return res.status(500).json({
        status: 'error',
        message: msg,
        error: err,
      })
    }
    else{
      //file not selected
      if(req.file == undefined){
        return res.status(400).json({
          status: 'error',
          message: 'Niste priložili datoteko, ki jo želite dodati delovnemu nalogu.',
        })
      }
      else{
        let type = "";
        let fileExtsPDF = ['pdf'];
        let fileExtsDOC = ['doc', 'dot', 'wbk', 'docx', 'docm', 'dotx', 'dotm', 'docb'];
        let fileExtsXLS = ['xls', 'xlt', 'xlm', 'xlsx', 'xlsm', 'xltx', 'xltm', 'xlsb', 'xla', 'xlam', 'xll', 'xlw'];
        let fileExtsPPT = ['ppt', 'pot', 'pps', 'pptx', 'pptm', 'potx', 'potm', 'ppam', 'ppsx', 'ppsm', 'sldx', 'sldm'];
        let fileExtsIMG = ['jpg', 'jpeg', 'png', 'gif'];
        let fileExtsZIP = ['zip', 'rar'];
        let fileExtsTXT = ['txt'];

        let isAlowedExtPDF = fileExtsPDF.includes(req.file.originalname.split(".")[1].toLowerCase());
        let isAlowedExtDOC = fileExtsDOC.includes(req.file.originalname.split(".")[1].toLowerCase());
        let isAlowedExtXLS = fileExtsXLS.includes(req.file.originalname.split(".")[1].toLowerCase());
        let isAlowedExtPPT = fileExtsPPT.includes(req.file.originalname.split(".")[1].toLowerCase());
        let isAlowedExtZIP = fileExtsZIP.includes(req.file.originalname.split(".")[1].toLowerCase());
        let isAlowedExtTXT = fileExtsTXT.includes(req.file.originalname.split(".")[1].toLowerCase());
        let isAlowedExtIMG = fileExtsIMG.includes(req.file.originalname.split(".")[1].toLocaleLowerCase());
        
        if(isAlowedExtPDF) type = 'PDF';
        else if(isAlowedExtDOC) type = 'DOC';
        else if(isAlowedExtPPT) type = 'PPT';
        else if(isAlowedExtXLS) type = 'XLS';
        else if(isAlowedExtIMG) type = 'IMG';
        else if(isAlowedExtZIP) type = 'ZIP';
        else if(isAlowedExtTXT) type = 'TXT';
        let compSubFileId = (req.body.compSubFileId != 'null') ? req.body.compSubFileId : null;
        let compSupFileId = (req.body.compSupFileId != 'null') ? req.body.compSupFileId : null;
        dbComplaints.addFileForComplaint(req.file.originalname, req.file.filename, req.session.user_id, type, compSubFileId, compSupFileId)
        .then(newFile =>{
          let msg = "Uspešno dodajanje nove datoteke.";
          let fileLocation = `${__dirname}/../files/complaints/${newFile.path_name}`;
          if (fs.existsSync(fileLocation)) {
            if (newFile.type == 'IMG'){
              Jimp.read(fileLocation).then(img => {
                img.resize(150, Jimp.AUTO).quality(20).getBase64Async(Jimp.AUTO).then(base64 => {
                  newFile.base64 = base64;
                  return res.status(200).json({
                    status: 'success',
                    file: newFile,
                    success: true,
                    message: msg,
                  });
                })
                .catch(err => {
                  return res.json({success:false, error: err});
                });
              })
              .catch((e) => {
                return res.json({success:false, error: e, msg: 'Datoteka ne obstaja.', id: fileId, type: fileType});
              })
            }
            else if (newFile.type == 'PDF'){
              fsp.readFile(fileLocation, 'base64').then(base64 => {
                newFile.base64 = base64;
                return res.status(200).json({
                  status: 'success',
                  file: newFile,
                  success: true,
                  message: msg,
                });
              })
              .catch((e) => {
                return res.json({success:false, error: e});
              })
            }
            else{
              res.status(200).json({
                status: 'success',
                file:newFile,
                success: true
              });
            }
          }
          else{
            return res.status(200).json({
              status: 'file not found',
              file: newFile,
              success: false,
              message: msg,
              type: type,
            });
          }
        })
        .catch((e)=>{
          return res.status(500).json({
            status: 'error',
            message: 'Napaka pri zapisovanju podatkov datoteke v tabelo z datotekami za reklamacije.',
            error: e,
            success: false,
          })
        })
      }
    }
  })
});
//delete file
router.delete('/files', auth.authenticate, function(req, res, next){
  let fileId = req.body.fileId;
  //check if file exist, if exist do unlink and then for both cases update deleted to true
  //dbFiles.deleteFile(fileId)
  dbComplaints.getFile(fileId).then(file => {
    if(file.active){ // file still exist, first delete/unlink it in directory and then mark it as deleted file
      fs.unlink(path.resolve('files/complaints/' + file.path_name), function(err) {
        if(err && err.code == 'ENOENT') {
          // file doens't exist
          console.info("File doesn't exist, won't remove it.");
          dbComplaints.deleteFile(fileId).then(deletedFile => {
            res.json({success:true, msg:'Datoteka je že izbrisana.', file:deletedFile});
          })
          .catch((e)=>{
            return res.json({success:false, error: e, msg:'Datoteka je že izbrisana, napaka pri zapisu v bazo o brisanju datoteke.'});
          })
        }
        else if (err) {
          // other errors, e.g. maybe we don't have enough permission
          console.error("Error occurred while trying to remove file");
          return res.json({success:false, error: e});
        }
        else {
          console.info(`removed`);
          dbComplaints.deleteFile(fileId).then(deletedFile => {
            res.json({success:true, msg:'Datoteka je uspešno izbrisana.', file:deletedFile});
          })
          .catch((e)=>{
            return res.json({success:false, error: e, msg:'Datoteka je uspešno izbrisana, napaka pri zapisu v bazo o brisanju datoteke.'});
          })
        }
      });
    }
    else{ // file doesnt exist anymore in directory or is corrupted or its named was changed from linux cmd --> just mark it as deleted
      dbComplaints.deleteFile(fileId).then(deletedFile => {
        res.json({success:true, msg:'Datoteka je označena kot izbrisana.', file:deletedFile});
      })
      .catch((e)=>{
        return res.json({success:false, error: e, msg:'Napaka pri zapisu v bazo o brisanju datoteke.'});
      })
    }
    //res.json({success:true});
  })
  .catch((e)=>{
    return res.json({success:false, error: e, msg:'Napaka pri iskanju datoteke v bazi.'});
  })
})
//FILE DOWNLOAD
router.get('/sendMeDoc', auth.authenticate, function(req, res){
  let filename = req.query.filename;
  console.log('Zahteva za datoteko: '+ filename);
  //var filePath = '/public/uploads/documents/1562059866128-Projekt Čisto novi projekt.pdf';
  var file = `${__dirname}/../files/complaints/${filename}`;
  if (fs.existsSync(file)) {
    //get file info from database to get correct name and MIME type
    dbComplaints.getComplaintFileInfo(filename).then(fileInfo => {
      switch (fileInfo.type) {
        case 'IMG': break;
        case 'PDF': res.type('application/pdf'); res.setHeader('Content-type', 'application/pdf'); res.set({"Content-Type": "application/pdf",}); break;
        case 'DOC': res.type('application/msword'); res.setHeader('Content-type', 'application/msword'); res.set({"Content-Type": "application/msword",}); break;
        case 'PPT': res.type('application/mspowerpoint'); res.setHeader('Content-type', 'application/mspowerpoint'); res.set({"Content-Type": "application/mspowerpoint",}); break;
        case 'ZIP': res.type('application/x-compressed'); res.setHeader('Content-type', 'application/x-compressed'); res.set({"Content-Type": "application/x-compressed",}); break;
        case 'TXT': res.type('text/plain'); res.setHeader('Content-type', 'text/plain'); res.set({"Content-Type": "text/plain",}); break;
        case 'XLS': res.type('application/excel'); res.setHeader('Content-type', 'application/excel'); res.set({"Content-Type": "application/excel",}); break;
        default: break;
      }
      if (fileInfo.type == 'IMG'){
        let tmp = fileInfo.original_name.split('.');
        if (tmp[tmp.length-1].toLocaleLowerCase() == 'png') {res.type('image/png'); res.setHeader('Content-type', 'image/png'); res.set({"Content-Type": "image/png",});}
        else if (tmp[tmp.length-1].toLocaleLowerCase() == 'jpg' || tmp[tmp.length-1] == 'jpeg') {res.type('image/jpeg'); res.setHeader('Content-type', 'image/jpeg'); res.set({"Content-Type": "image/jpeg",});}
        else if (tmp[tmp.length-1].toLocaleLowerCase() == 'gif') {res.type('image/gif'); res.setHeader('Content-type', 'image/gif'); res.set({"Content-Type": "image/gif",});}
      }
      // Do something
      res.download(file, fileInfo.original_name, function(err){
        if(err){
          console.error(err);
        }
      });
    })
    .catch(e => {
      let msg = "Napaka: Datoteka ne obstaja v bazi!";
      let type = 1;
      return res.status(500).json({
        status: 'error',
        message: msg,
        type
      })
    })
  }
  else{
    dbComplaints.updateActiveFile(filename)
      .then(file=>{
        console.log("Datoteka "+filename+" ne obstaja več, aktivnost v bazi popravljena.")
        let msg = "Napaka: Datoteka ne obstaja več!";
        return res.status(500).json({
          status: 'error',
          message: msg,
        })
      })
      .catch((e)=>{
        return res.status(500).json({
          status: 'error',
          message: 'Something went wrong',
          error: e,
        })
      })
  }
})
/////////////////////MISC
//get all complaints status
router.get('/status', auth.authenticate, function(req, res, next){
  dbComplaints.getCompStatus().then(allCompStatus => {
    return res.json({success:true, allCompStatus});
  })
  .catch((e)=>{
    return res.json({success:false, error:e});
  })
})
//get all changes for complaints (for sub or sup -> depends on )
router.get('/changes', auth.authenticate, function(req, res, next){
  let compSubId = req.query.compSubId;
  let compSupId = req.query.compSupId;
  dbComplaints.getCompChangesSystem(compSubId, compSupId).then(compChanges => {
    return res.json({success:true, compChanges});
  })
  .catch((e)=>{
    return res.json({success:false, error:e});
  })
  // return res.json({success:false, msg: 'Contact admin to implement this functionality.'});
})
module.exports = router;