var express = require('express');
var router = express.Router();
var dateFormat = require('dateformat');
const csp = require('helmet-csp');
// Define font files
var fonts = {
  Roboto: {
    normal: `${__dirname}/../public/pdfmake/Roboto-Regular.ttf`,
    bold: `${__dirname}/../public/pdfmake/Roboto-Medium.ttf`,
    italics: `${__dirname}/../public/pdfmake/Roboto-Italic.ttf`,
    bolditalics: `${__dirname}/../public/pdfmake/Roboto-MediumItalic.ttf`,
  }
};
var PdfPrinter = require('pdfmake');
var printer = new PdfPrinter(fonts);

//auth & dbModels
var auth = require('../controllers/authentication');
var dbReports = require('../model/reports/dbReports');
let dbUsers = require('../model/employees/dbEmployees');

var normalCspHandler = csp({
  directives: {
    defaultSrc: ["'self'"],
    scriptSrc: ["'self'"],
    styleSrc: ["'self'", "https://fonts.googleapis.com", "https://use.fontawesome.com"],
    fontSrc: ["'self'", "https://fonts.gstatic.com", "https://use.fontawesome.com"],
    imgSrc: ["'self'", "data:"],
    frameAncestors: ["'self'"],
  }
});

//functions
function formatDate(date){
  return {
    "date": dateFormat(date, "dd.mm.yyyy"),
    "time": dateFormat(date, "HH:MM")
  }
}
function formatTime(time){
	let tmp = time.split(':')
  return {
    "hours": tmp[0],
    "minutes": tmp[1]
  }
}
function addFormatedDateForReports(reports){
  //console.log("test");
  for(let i=0; i < reports.length; i++) {
    if(reports[i] && reports[i].date)
      reports[i]["formatted_date"] = formatDate(reports[i].date);
  }
  return reports;
}
function addFormatedTimeForReports(reports){
  //console.log("test");
  for(let i=0; i < reports.length; i++) {
    if(reports[i] && reports[i].time)
      reports[i]["formatted_time"] = formatTime(reports[i].time);
  }
  return reports;
}
//stran za poročila
router.get('/', normalCspHandler, auth.authenticate, function(req, res, next) {
  let userId = req.session.user_id;
	dbReports.getUserLastReports(userId).then(reports => {
		addFormatedDateForReports(reports);
		addFormatedTimeForReports(reports);
    res.render('reports', {
      title:"Pregled poročil",
      user_name: req.session.user_name,
      user_surname: req.session.user_username,
      user_surname: req.session.user_surname,
      user_typeid: req.session.role_id,
      user_type: req.session.type,
      user_id: req.session.user_id,
      reports: reports,
    })
  })
  .catch(e => {
    next(e);
  })
})
// get pdf for monlthly report
router.get('/pdfreport', auth.authenticate, function(req,res){
  let userId = req.query.userId;
  let month = req.query.month;
  let year = req.query.year;

  if(!userId || !month || !year)
    return res.json({success: false, error: 'Missing arguments', msg: 'Manjkajo spremeljivke.'});
  else{
    dbReports.getReportsForMonthlyReports(month, year, userId).then(monthlyReports =>{
      //return res.json({success: true, users: null, reports: reports});
      let monthName;
      switch (month) {
        case '2': monthName = 'Februar'; break;
        case '3': monthName = 'Marec'; break;
        case '4': monthName = 'April'; break;
        case '5': monthName = 'Maj'; break;
        case '6': monthName = 'Junij'; break;
        case '7': monthName = 'Julij'; break;
        case '8': monthName = 'Avgust'; break;
        case '9': monthName = 'September'; break;
        case '10': monthName = 'Oktober'; break;
        case '11': monthName = 'November'; break;
        case '12': monthName = 'December'; break;
        default: monthName = 'Januar'; break;
      }
      let pdfHeader = monthName+` `+year;
      var pdfTableMonthlyReport = {style: 'tableCategory',layout: '' ,table:{widths:['auto','*','auto', ], body: [
        [
          {fillColor: '#cccccc', text: 'Datum'},
          {fillColor: '#cccccc', text: 'Delovni čas/Ure'},
          {fillColor: '#cccccc', text: 'Vsota ur'},
        ]
      ]}};
      var lastDay, lastDayTime;
      for(var i = 0, j = 0; i < monthlyReports.length; i++){
        var currentDay = monthlyReports[i].date;
        var hoursLabel = '';
        var hoursSum = '';
        if(lastDay && lastDay == currentDay){
          //same day as last iteration, add time and description
          var start = monthlyReports[i].start;
          var finish = monthlyReports[i].finish;
          var newTime =  new Date("0000-01-01 " + monthlyReports[i].time);
          lastDayTime.setMinutes(newTime.getMinutes() + lastDayTime.getMinutes());
          lastDayTime.setHours(newTime.getHours() + lastDayTime.getHours());
    
          hoursLabel = start.substring(0,5)+' - '+finish.substring(0,5);
          hoursSum = elapsedTime.getHours()+'h '+elapsedTime.getMinutes()+'m';
          pdfTableMonthlyReport.table.body[j][1].text += ', '+hoursLabel;
          pdfTableMonthlyReport.table.body[j][2].text = hoursSum;
        }
        else{
          //new day, create new row
          j++;
          lastDay = currentDay;
          var start = monthlyReports[i].start;
          var finish = monthlyReports[i].finish;
          var elapsedTime = new Date("0000-01-01 " + monthlyReports[i].time);
          lastDayTime = elapsedTime;
    
          hoursLabel = start.substring(0,5)+' - '+finish.substring(0,5);
          hoursSum = elapsedTime.getHours()+`h `+elapsedTime.getMinutes()+`m`;
          var currentDayRow = [
            {text: new Date(monthlyReports[i].date).toLocaleDateString('sl')},
            {text: hoursLabel, noWrap: true},
            {text: hoursSum, noWrap: true},
          ];
          pdfTableMonthlyReport.table.body.push(currentDayRow);
        }
      }
      dbUsers.getUserById(userId, req.session.user_id).then(user => {
        pdfHeader += ' - ' + user.name + ' ' + user.surname;
        
        var docDefinition = {
          info: {
            title: pdfHeader,
            author: 'Roboteh-Informacijski-Sistem',
            subject: 'Monthly User Report',
          },
          pageSize: 'A4',
          pageOrientation: 'portrait',
          content: [
            {
              style: 'header',
              text: pdfHeader
            },
            pdfTableMonthlyReport,
          ],
          styles: {
            header: {
              fontSize: 22,
              bold: true,
              alignment: 'left',
              margin: [0, 0, 0, 5]
            },
            tableCategory: {
              margin: [0, 0, 0, 0]
            },
          }
        };
        let pdfDoc = printer.createPdfKitDocument(docDefinition);
        const filename = encodeURI(pdfHeader);
        res.setHeader('Content-disposition', `attachment; filename*=UTF-8''${filename}.pdf; filename=${filename}.pdf`);
        res.setHeader('Content-type', 'application/pdf');
        //res.setHeader('Content-disposition', 'attachment; filename='+pdfHeader+'.pdf');//motijo ga sumniki
        pdfDoc.pipe(res);
        pdfDoc.end();
      })
      .catch((e)=>{
        return res.json({success: false, error: e});
      })
    })
    .catch((e)=>{
      return res.json({success: false, error: e});
    })
  }
})
//get monthly reports
router.get('/monthly', auth.authenticate, function(req, res, next) {
  let userId = req.query.userId;
  let roleType = req.session.type.toLowerCase();
  if (roleType == 'admin' || roleType == 'tajnik' || roleType == 'komercialist' || roleType == 'komerciala' || roleType == 'računovodstvo' || roleType.substring(0,5) == 'vodja')
    userId = null;
  else
    userId = req.session.user_id;
  let month = req.query.month;
  let year = req.query.year;
  if(!month && !year){
    //admin or leader or secratary is asking for months for monthly reports
    dbReports.getDatesForMonthlyReports(userId)
    .then(dates =>{
      return res.json({success: true, dates: dates});
    })
    .catch((e)=>{
      return res.json({success: false, error: e});
    })
  }
	else{
    if (!req.query.userId && (roleType == 'admin' || roleType == 'tajnik' || roleType == 'komercialist' || roleType == 'komerciala' || roleType == 'računovodstvo' || roleType.substring(0,5) == 'vodja')){
      //admin or leader or secratary chose month & year and wants to get list of users for chosen month & year
      dbReports.getUsersForMonthlyReports(month, year).then(users =>{
        return res.json({success: true, users: users});
      })
      .catch((e)=>{
        return res.json({success: false, error: e});
      })
    }
    else{
      //get reports for choosen month & year & user
      if(req.query.userId)
        userId = req.query.userId;
      else
        userId = req.session.user_id;
      dbReports.getReportsForMonthlyReports(month, year, userId).then(reports =>{
        return res.json({success: true, users: null, reports: reports});
      })
      .catch((e)=>{
        return res.json({success: false, error: e});
      })
    }
  }
})
//get reports for inputs
router.get('/reports', auth.authenticate, function(req, res, next) {
	let userId;
	if(req.query.userId)
		userId = req.query.userId;
	else
		userId = req.session.user_id;
	let dateStart = req.query.dateStart;
	let dateFinish = req.query.dateFinish;
	let radio = req.query.radio;
	let projectId = req.query.projectId;
	let other = req.query.other;
	let reportType = req.query.reportType;
	dbReports.getUserReports(userId,dateStart,dateFinish,radio,projectId,other,reportType)
	.then(reports=>{
		addFormatedDateForReports(reports);
		addFormatedTimeForReports(reports);
		return res.json({success: true, data: reports});  
	})
	.catch((e)=>{
		return res.json({success:false, error:e});
	})
})
//add new report
router.post('/reports', auth.authenticate, function(req, res, next) {
	let userId;
	if(req.body.userId)
		userId = req.body.userId;
	else
		userId = req.session.user_id;
	let reportDate = req.body.date;
  let reportTime = req.body.time;
  let reportStart = req.body.start;
  let reportFinish = req.body.finish;
	let reportProjectId = req.body.projectId;
	let reportOther = req.body.other;
	let reportDescription = req.body.description;
	let reportType = req.body.type;
	let reportRadio = req.body.radio;
	dbReports.addNewReport(userId,reportDate,reportTime,reportStart,reportFinish,reportProjectId,reportType,reportOther,reportDescription,reportRadio)
	.then(newReport => {
		res.json({success:true, report:newReport})
	})
	.catch(e => {
		return res.json({success:false, error:e});
	})
})
//edit report
router.post('/edit', auth.authenticate, function(req, res, next) {
	let userId;
	if(req.body.userId)
		userId = req.body.userId;
	else
		userId = req.session.user_id;
	let reportId = req.body.reportId;
	let reportDate = req.body.date;
  let reportTime = req.body.time;
  let reportStart = req.body.start;
  let reportFinish = req.body.finish;
	let reportProjectId = req.body.projectId;
	let reportOther = req.body.other;
	let reportDescription = req.body.description;
	let reportType = req.body.type;
	let reportRadio = req.body.radio;
	dbReports.editReport(reportId,userId,reportDate,reportTime,reportStart,reportFinish,reportProjectId,reportType,reportOther,reportDescription,reportRadio)
	.then(report => {
		res.json({success:true})
	})
	.catch(e => {
		res.json({success:false, error:e});
	})
})
//delete work order(update work order active to false)
router.post('/delete', auth.authenticate, function(req, res, next){
  let reportId = req.body.reportId;
  let active = req.body.active;
  dbReports.deleteReport(reportId,active)
  .then(report=>{
    return res.json({success: true});  
  })
  .catch((e)=>{
    res.json({success:false, error:e});
  })
})
//get change types
router.get('/types', auth.authenticate, function(req, res, next){
  dbReports.getReportsTypes()
  .then(types=>{
    return res.json({success: true, data: types});  
  })
  .catch((e)=>{
    res.json({success:false, error:e});
  })
})
//create subscriber
router.post('/types', auth.authenticate, function(req, res, next){
  dbReports.addReportsType(req.body.type).then(type=>{
    res.json({success:true, type:type});
  })
  .catch((e)=>{
    res.json({success:false, error:e});
  })
})
//get all projects for select2 (id,text)
router.get('/projects', auth.authenticate, function(req, res, next){
  let active = req.query.activeProject;
  dbReports.getProjectsSelect(active).then(projects=>{
    res.json({success:true, data:projects});
  })
  .catch((e)=>{
    res.json({success:false, error:e});
  })
})

module.exports = router;