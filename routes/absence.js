var express = require('express');
var router = express.Router();
var dateFormat = require('dateformat');
const csp = require('helmet-csp');

//auth & dbModels
var auth = require('../controllers/authentication');
var dbEmployees = require('../model/employees/dbEmployees');
var dbAbsences = require('../model/absences/dbAbsences');

var normalCspHandler = csp({
  directives: {
    defaultSrc: ["'self'"],
    scriptSrc: ["'self'"],
    styleSrc: ["'self'", "https://fonts.googleapis.com", "https://use.fontawesome.com"],
    fontSrc: ["'self'", "https://fonts.gstatic.com", "https://use.fontawesome.com"],
    imgSrc: ["'self'", "data:"],
    frameAncestors: ["'self'"],
  }
});

//functions
function formatDate(date){
  return {
    "date": dateFormat(date, "dd.mm.yyyy"),
    "time": dateFormat(date, "HH:MM")
  }
}

function addFormatedDateForAbsences(absences){
  //console.log("test");
  for(let i=0; i < absences.length; i++) {
    if(absences[i] && absences[i].start)
      absences[i]["formatted_start"] = formatDate(absences[i].start);
    if(absences[i] && absences[i].finish)
      absences[i]["formatted_finish"] = formatDate(absences[i].finish);
  }
  return absences;
}
//POGLED STRAN VSEH ODSOTNOSTI
router.get('/', normalCspHandler, auth.authenticate, function(req, res, next){
  res.render('absences', {
    title: "Odsotnosti",
    user_name: req.session.user_name,
    user_username: req.session.user_username,
    user_surname: req.session.user_surname,
    user_typeid: req.session.role_id,
    user_type: req.session.type,
    user_id: req.session.user_id,
  })
})
//POGLED STRAN VSEH ODSOTNOSTI VPISANEGA UPORABNIKA
router.get('/id', normalCspHandler, auth.authenticate, function(req, res, next){
  let userId = req.query.id;
  if(!userId){
    console.error("No userId provided in query");
    return res.redirect('/dashboard');
  }
  dbEmployees.getUserById(userId, req.session.user_id).then(employee =>{
    if(employee){
      return dbAbsences.getUserAbsence(userId)
        .then(absences=>{return {employee, absences}})
        .catch((e)=>{
          next(e);
        })
      .then(data=>{
        //found employee with absences
        if(data.absences){
          addFormatedDateForAbsences(data.absences);
          let roleType = req.session.type.toLowerCase();
          if(roleType == 'admin' || roleType == 'tajnik' || roleType == 'komercialist' || roleType == 'komerciala' || roleType == 'računovodstvo' || roleType.substring(0,5) == 'vodja' || req.session.user_id == userId)
            res.render('absence', {
              title: "Moje odsotnosti",
              user_name: req.session.user_name,
              user_username: req.session.user_username,
              user_surname: req.session.user_surname,
              user_typeid: req.session.role_id,
              user_type: req.session.type,
              user_id: req.session.user_id,
              employee: data.employee,
              absences: data.absences
            })
          else
            res.redirect('/dashboard');
        }
        //found employee not absences
        else{
          let roleType = req.session.type.toLowerCase();
          if(roleType == 'admin' || roleType == 'tajnik' || roleType == 'komercialist' || roleType == 'komerciala' || roleType == 'računovodstvo' || roleType.substring(0,5) == 'vodja' || req.session.user_id == userId)
            res.render('absence', {
              title: "Moje odsotnosti",
              user_name: req.session.user_name,
              user_surname: req.session.user_surname,
              user_username: req.session.user_username,
              user_typeid: req.session.role_id,
              user_type: req.session.type,
              user_id: req.session.user_id,
              employee: data.employee,
              absences: []
            })
          else
            res.redirect('/dashboard');
        }
      })
      .catch((e)=>{
        next(e);
      })
    }
    //found nothing
    else{
      res.render('absence', {
        title: "Moje odsotnosti",
        user_name: req.session.user_name,
        user_surname: req.session.user_surname,
        user_username: req.session.user_username,
        user_typeid: req.session.role_id,
        user_type: req.session.type,
        user_id: req.session.user_id,
        employee: null,
        projects: []
      })
    }
  })
  .catch((e)=>{
    next(e);
  })
})
//get all absences for selected user
router.get('/all', auth.authenticate, function(req, res, next){
  let userId = req.query.userId;
  dbAbsences.getUserAbsence(userId).then(absences => {
    return res.json({success:true, absences});
  })
  .catch(e => {
    return res.json({success:false, error:e});
  })
})
//get all reasons (id, text)
router.get('/reasons', auth.authenticate, function(req, res, next){
  dbAbsences.getReasons()
  .then(reasons=>{
    return res.json({success:true, data:reasons});
  })
  .catch((e)=>{
    return res.json({success:false, error:e});
  })
})
//add new absence
router.post('/add', auth.authenticate, function(req, res, next){
  //console.log(req.body);
  if(isNaN(req.body.reason)){
    //new reason, first create new reason then create new absence
    dbAbsences.createReason(req.body.reason).then(reason=>{
      //new reason created, create now new absence
      dbAbsences.createAbsence(req.body.start, req.body.finish, reason.id, req.body.worker, req.body.projectId, req.body.taskId, req.body.name, req.body.approved)
      .then(absence=>{
        return res.json({success:true, absenceId:absence.id, reasonId:reason.id})
      })
      .catch((e)=>{
        return res.json({success:false, error:e});
      })
    })
    .catch((e)=>{
      return res.json({success:false, error:e});
    })
  }
  else{
    //reason exist, create new absence
    dbAbsences.createAbsence(req.body.start, req.body.finish, req.body.reason, req.body.worker, req.body.projectId, req.body.taskId, req.body.name, req.body.approved)
    .then(absence=>{
      return res.json({success:true, absenceId:absence.id, reasonId:null})
    })
    .catch((e)=>{
      return res.json({success:false, error:e});
    })
  }
})

//update absence
router.post('/update', auth.authenticate, function(req, res, next){
  //admin, tajnik and vodja can create new reason
  if(isNaN(req.body.reason)){
    //new reason
    dbAbsences.createReason(req.body.reason).then(reason=>{
      dbAbsences.updateAbsence(req.body.id, req.body.start, req.body.finish, reason.id, req.body.name)
      .then(absence=>{
        //update success
        return res.json({success:true, reasonId:reason.id});
      })
      .catch((e)=>{
        return res.json({success:false, error:e});
      })
    })
    .catch((e)=>{
      return res.json({success:false, error:e});
    })
  }
  else{
    //reason exist, update absence
    dbAbsences.updateAbsence(req.body.id, req.body.start, req.body.finish, req.body.reason, req.body.name)
    .then(absence=>{
      //update success
      return res.json({success:true, reasonId:null});
    })
    .catch((e)=>{
      return res.json({success:false, error:e});
    })
  }
})
//delete absence
router.post('/delete', auth.authenticate, function(req, res, next){
  let id = req.body.id;
  let newAbsenceActiveStatus = req.body.newAbsenceActiveStatus;
  dbAbsences.deleteAbsence(id, newAbsenceActiveStatus)
  .then(absence=>{
    return res.json({success:true});
  })
  .catch((e)=>{
    return res.json({success:false});
  })
})
//approve absence
router.post('/approve', auth.authenticate, function(req, res, next){
  let id = req.body.id;
  let newAbsenceApproveStatus = req.body.newAbsenceApproveStatus;
  dbAbsences.approveAbsence(id, newAbsenceApproveStatus)
  .then(absence=>{
    return res.json({success:true});
  })
  .catch((e)=>{
    return res.json({success:false, error:e});
  })
})

//get all daily absences
router.get('/date', auth.authenticate, function(req, res, next){
  let date = req.query.date;
  dbAbsences.getDailyAbsences(date)
  .then(absences =>{
    return res.json({success:true, data:absences})
  })
  .catch((e)=>{
    return res.json({success:false, error:e});
  })
})
module.exports = router;