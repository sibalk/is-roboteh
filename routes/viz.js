var express = require('express');
var router = express.Router();
const csp = require('helmet-csp');

//auth & db
var auth = require('../controllers/authentication');
var dbProjects = require('../model/projects/dbProjects');
var dbViz = require('../model/viz/dbViz');

var normalCspHandler = csp({
  directives: {
    defaultSrc: ["'self'"],
    scriptSrc: ["'self'"],
    styleSrc: ["'self'", "https://fonts.googleapis.com", "https://use.fontawesome.com"],
    fontSrc: ["'self'", "https://fonts.gstatic.com", "https://use.fontawesome.com"],
    imgSrc: ["'self'", "data:"],
    frameAncestors: ["'self'"],
  }
});

///MAIN PAGE --> VIZUALIZATION
router.get('/', auth.authenticate, function(req, res){
  res.render('viz', {
    title: "Vizualizacija",
    user_name: req.session.user_name,
    user_surname: req.session.user_surname,
    user_username: req.session.user_username,
    user_typeid: req.session.role_id,
    user_type: req.session.type,
    user_id: req.session.user_id
  });
});
//get all active projects with content and title ready
router.get('/allprojects', auth.authenticate, function(req, res, next){
  let loggedUser = req.session.user_id;
  dbViz.getAllActiveProjects(loggedUser).then(projects=>{
    res.json({success:true, data:projects, altData: projects});
  })
  .catch((e)=>{
    return res.json({success:false, error:e});
  })
})
//get all projects for select2 (id,text)
router.get('/projects', auth.authenticate, function(req, res, next){
  let active = req.query.activeProject;
  dbProjects.getProjectsSelect(active).then(projects=>{
    res.json({success:true, data:projects});
  })
  .catch((e)=>{
    return res.json({success:false, error:e});
  })
})
//get all assignments
router.get('/assignments', auth.authenticate, function(req, res, next){
  //let active = req.query.activeProject;
  let mechAssemblyRole = req.query.mechAssemblyRole,
      cncOperaterRole = req.query.cncOperaterRole,
      electricanRole = req.query.electricanRole,
      mechanicRole = req.query.mechanicRole,
      welderRole = req.query.welderRole,
      serviserRole = req.query.serviserRole,
      plcRole = req.query.plcRole,
      robotRole = req.query.robotRole,
      builderRole = req.query.builderRole,
      leaderRole = req.query.leaderRole,
      workerRole = req.query.workerRole,
      studentRole = req.query.studentRole,
      secrataryRole = req.query.secrataryRole,
      adminRole = req.query.adminRole;
  let promises = [];
  promises.push(dbViz.getUnfinishedAssignmentsByUserRole(mechAssemblyRole, cncOperaterRole, electricanRole, mechanicRole, welderRole, serviserRole, plcRole, robotRole, builderRole, leaderRole, workerRole, studentRole, secrataryRole, adminRole));
  promises.push(dbViz.getAbsencesByUserRole(mechAssemblyRole, cncOperaterRole, electricanRole, mechanicRole, welderRole, serviserRole, plcRole, robotRole, builderRole, leaderRole, workerRole, studentRole, secrataryRole, adminRole));
  // dbViz.getUnfinishedAssignmentsByUserRole(mechAssemblyRole, cncOperaterRole, electricanRole, mechanicRole, welderRole, serviserRole, plcRole, robotRole, builderRole, leaderRole, workerRole, studentRole, secrataryRole, adminRole).then(assignments=>{
  //   res.json({success:true, data:assignments});
  // })
  // .catch((e)=>{
  //   return res.json({success:false, error:e});
  // })
  Promise.all(promises).then(results =>{
    let assignments = results[0].concat(results[1]);
    res.json({success:true, data:assignments});
  })
  .catch((e)=>{
    return res.json({success:false, error:e});
  })
})
//get all users for assignmentgs visualisation
router.get('/users', auth.authenticate, function(req, res, next){
  let mechAssemblyRole = req.query.mechAssemblyRole,
      cncOperaterRole = req.query.cncOperaterRole,
      electricanRole = req.query.electricanRole,
      mechanicRole = req.query.mechanicRole,
      welderRole = req.query.welderRole,
      serviserRole = req.query.serviserRole,
      plcRole = req.query.plcRole,
      robotRole = req.query.robotRole,
      builderRole = req.query.builderRole,
      leaderRole = req.query.leaderRole,
      workerRole = req.query.workerRole,
      studentRole = req.query.studentRole,
      secrataryRole = req.query.secrataryRole,
      adminRole = req.query.adminRole;
  dbViz.getUsersOrderByRoles(mechAssemblyRole, cncOperaterRole, electricanRole, mechanicRole, welderRole, serviserRole, plcRole, robotRole, builderRole, leaderRole, workerRole, studentRole, secrataryRole, adminRole).then(users=>{
    res.json({success:true, data:users});
  })
  .catch((e)=>{
    return res.json({success:false, error:e});
  })
})
module.exports = router;