var express = require('express');
var router = express.Router();

var auth = require('../../controllers/authentication');
var dbEvents = require('../../model/events/dbEvents');
var dbChanges = require('../../model/changes/dbChanges');
var dbUsers = require('../../model/employees/dbEmployees');

// get user projects (projects where users is part of)
// get all events
router.get('/', auth.authenticate, function(req,res, next){
  let userId = req.session.user_id;
  let start = req.query.start;
  let end = req.query.end;

  dbEvents.getEvents(userId, start, end).then(events => {
    res.json({events});//200
  })
  .catch((e)=>{
    switch (parseInt(e.message)) {
      case 400: return res.json({message: 'Bad request'}, 400);
      case 403: return res.json({message: 'Forbidden'}, 403);
      case 404: return res.json({message: 'Not Found'}, 404);
      default: return res.json({error: e.message}, 500);
    }
  })
});
// get selected event
router.get('/:id', auth.authenticate, function(req,res, next){

  let eventId = req.params.id;
  dbEvents.getEventById(eventId).then(event => {
    if (event)
      res.json({event});//200
    else throw new Error(404);
  })
  .catch((e)=>{
    switch (parseInt(e.message)) {
      case 400: return res.json({message: 'Bad request'}, 400);
      case 403: return res.json({message: 'Forbidden'}, 403);
      case 404: return res.json({message: 'Not Found'}, 404);
      default: return res.json({error: e.message}, 500);
    }
  })
});
// add new event
router.post('/', auth.authenticate, function(req,res, next){

  let eventTmp;
  let start = req.body.start;
  let end = req.body.end;
  let title = req.body.title;
  let resource = '{' + req.body.resource.toString() + '}';
  let location = req.body.location;
  let notes = req.body.notes;
  let allDay = req.body.allDay;
  let color = req.body.color;
  let userId = req.session.user_id;
  // for role control
  // let userRole = req.session.type;
  if (!start || !end || !resource)
    return res.sendStatus(400);

  // check if user has permission to add event -> admin, leaders has full control, other user can create event for themself
  if (!(req.session.type.toLowerCase() == 'admin' || req.session.type.toLowerCase() == 'tajnik' || req.session.type.toLowerCase() == 'komercialist' || req.session.type.toLowerCase() == 'komerciala' || req.session.type.toLowerCase() == 'računovodstvo' || ( req.body.resource.find(r => r == 'user'+userId)) || req.session.type.toLowerCase().includes('vodja'))) {
    return res.sendStatus(403);
  }

  dbEvents.addNewEvent(start, end, title, resource, location, notes, allDay, color, userId).then(event => {
    eventTmp = event;
    return dbChanges.addNewChangeSystem(1,41,userId,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,event.id);
  })
  .then(change => {
    let event = eventTmp;
    return res.json({event});//200
  })
  .catch((e)=>{
    switch (parseInt(e.message)) {
      case 400: return res.json({message: 'Bad request'}, 400);
      case 403: return res.json({message: 'Forbidden'}, 403);
      case 404: return res.json({message: 'Not Found'}, 404);
      default: return res.json({error: e.message}, 500);
    }
  })
});
// update/edit event alt
router.put('/:id', auth.authenticate, function(req,res, next){

  let eventId = req.params.id;
  let start = req.body.start;
  let end = req.body.end;
  let title = req.body.title;
  let resource = '{' + req.body.resource.toString() + '}';
  let location = req.body.location;
  let notes = req.body.notes;
  let allDay = req.body.allDay;
  let color = req.body.color;
  let unvalid = req.body.unvalid;
  let noteChanges = '';
  let userId = req.session.user_id;
  let event;

  if (!start || !end || !resource)
    return res.sendStatus(400);

  // get event to be able to check what has changed and save it to notes for better history tracking and blame
  dbEvents.getEventById(eventId)
  .then(oldEvent => {
    if (!oldEvent)
      throw new Error(404);
    // check if user has permission to edit event -> admin has full control, other users can edit their own events
    if (!(req.session.type.toLowerCase() == 'admin' || req.session.type.substring(0,5).toLowerCase() == 'vodja' || oldEvent.resource.find(r => r == 'user'+userId))) {
      throw new Error(403);
    }
    if ( new Date(start).toISOString() != new Date(oldEvent.start).toISOString() ){
      let tmpDate = new Date(oldEvent.start);
      noteChanges +=', začetek: ' + tmpDate.getDate() + '.' + (tmpDate.getMonth()+1) + '.' + tmpDate.getFullYear();
    }
    if ( new Date(end).toISOString() != new Date(oldEvent.end).toISOString()){
      let tmpDate = new Date(oldEvent.end);
      noteChanges +=', konec: ' + tmpDate.getDate() + '.' + (tmpDate.getMonth()+1) + '.' + tmpDate.getFullYear();
    }
    if (title != oldEvent.title)
      noteChanges += ', lokacija: ' + oldEvent.title;
    if (location != oldEvent.location)
      noteChanges += ', projekt: ' + oldEvent.location;
    if (notes != oldEvent.notes)
      noteChanges += ', opravilo: ' + oldEvent.notes;
    if (resource != '{'+oldEvent.resource.toString()+'}')
      noteChanges += ', zadolženi: ' + oldEvent.resource.toString();
    if (noteChanges.length > 0)
      noteChanges = 'Spremembe:' + noteChanges.substring(1, noteChanges.length);
    
    return dbEvents.editEvent(eventId, start, end, title, resource, location, notes, allDay, color, unvalid)
  })
  .then(updatedEvent => {
    event = updatedEvent;
    if (event)
      return dbChanges.addNewChangeSystem(2,41,userId,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,eventId,noteChanges);
    else throw new Error(404);
  })
  .then(change => {
    return res.json({event});//200
  })
  .catch((e)=>{
    switch (parseInt(e.message)) {
      case 400: return res.json({message: 'Bad request'}, 400);
      case 403: return res.json({message: 'Forbidden'}, 403);
      case 404: return res.json({message: 'Not Found'}, 404);
      default: return res.json({error: e.message}, 500);
    }
  })
});

// patch event completion
router.patch('/:id', auth.authenticate, function(req,res, next){

  let eventId = req.params.id;
  let completion = req.body.completion;
  let userId = req.session.user_id;
  let event;

  if (completion == null || (typeof completion == 'string' && !(completion == 'true' || completion == 'false')) || !(typeof completion == 'string' || typeof completion == 'boolean'))
    return res.sendStatus(400);

  // get event to be able to check what has changed and save it to notes for better history tracking and blame
  Promise.all([dbEvents.getEventById(eventId), dbUsers.getUsers()])
  .then(([oldEvent, users]) => {
    if (!oldEvent)
      throw new Error(404);
    let = elektroUserCheck = false, strojnaUserCheck = false;
    if (req.session.type.toLowerCase().substring(0,4) == 'info'){
      for (const worker of oldEvent.resource) {
        if(users.find(u => u.id == worker.substring(4) && u.role.toLowerCase() == 'električar')) elektroUserCheck = true;
        if(users.find(u => u.id == worker.substring(4) && (u.role.toLowerCase() == 'strojnik' || u.role.toLowerCase() == 'varilec' || u.role.toLowerCase() == 'cnc operater') )) strojnaUserCheck = true;
      }
    }
    // check if user has permission to patch completion --> admins, project leaders, role leaders for role and assigned users
    if (!(req.session.type.toLowerCase() == 'admin' || 
    oldEvent.owner == userId || 
    oldEvent.resource.find(r => r == 'user'+userId) || 
    req.session.type.toLocaleLowerCase() == 'info elektro' && elektroUserCheck ||
    req.session.type.toLocaleLowerCase() == 'vodja električarjev' && elektroUserCheck ||
    req.session.type.toLocaleLowerCase() == 'vodja strojnikov' && strojnaUserCheck ||
    req.session.type.toLocaleLowerCase() == 'vodja cnc operaterjev' && strojnaUserCheck)) {
      throw new Error(403);
    }
    else
      return dbEvents.updateEventCompletion(eventId, completion)
  })
  .then(patchedEvent => {
    event = patchedEvent;
    if (event){
      if (event.completion)
        return dbChanges.addNewChangeSystem(4,41,userId,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,eventId);
      else
        return dbChanges.addNewChangeSystem(5,41,userId,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,eventId);
    }
    else throw new Error(404);
  })
  .then(change => {
    return res.json({event});//200
  })
  .catch((e)=>{
    switch (parseInt(e.message)) {
      case 400: return res.json({message: 'Bad request'}, 400);
      case 403: return res.json({message: 'Forbidden', info: 'Uporabnik nima pravice posodobiti končanost dogodka.'}, 403);
      case 404: return res.json({message: 'Not Found', info: 'Dogodka s tem ID-jem ni v podatkovni bazi.'}, 404);
      default: return res.json({error: e.message}, 500);
    }
  })
});

// delete 
router.delete('/:id', auth.authenticate, function(req,res, next){

  let eventId = req.params.id;
  let activity = req.body.activity ? req.body.activity : false;
  let userId = req.session.user_id;
  let event;
  dbEvents.deleteEvent(eventId,activity)
  .then(updatedDeletedEvent => {
    event = updatedDeletedEvent;
    if (event){
      if (activity)
        dbChanges.addNewChangeSystem(6,41,userId,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,eventId);
      else
        dbChanges.addNewChangeSystem(3,41,userId,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,eventId);
    }
    else throw new Error(404);
  })
  .then(change => {
    return res.json({event});
  })
  .catch((e)=>{
    switch (parseInt(e.message)) {
      case 400: return res.json({message: 'Bad request'}, 400);
      case 403: return res.json({message: 'Forbidden'}, 403);
      case 404: return res.json({message: 'Not Found'}, 404);
      default: return res.json({error: e.message}, 500);
    }
  })
});
//get all changes for events
router.get('/:id/changes', auth.authenticate, function(req, res, next){
  let eventId = req.params.id;
  dbEvents.getEventChangesSystem(eventId).then(eventChanges => {
    return res.json({success:true, eventChanges});
  })
  .catch((e)=>{
    return res.json({success:false, error:e});
  })
})
module.exports = router;