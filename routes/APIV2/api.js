var express = require('express');
var router = express.Router();
var dateFormat = require('dateformat');
const csp = require('helmet-csp');

//auth & dbModels
var auth = require('../../controllers/authentication');
var dbCategories = require('../../model/categories/dbCategories');
var dbPriorities = require('../../model/priorities/dbPriorities');

var normalCspHandler = csp({
  directives: {
    defaultSrc: ["'self'"],
    scriptSrc: ["'self'"],
    styleSrc: ["'self'", "https://fonts.googleapis.com", "https://use.fontawesome.com"],
    fontSrc: ["'self'", "https://fonts.gstatic.com", "https://use.fontawesome.com"],
    imgSrc: ["'self'", "data:"],
    frameAncestors: ["'self'"],
  }
});
var apiCspHandler = csp({
  directives: {
    defaultSrc: ["'self'"],
    scriptSrc: ["'self'"],
    styleSrc: ["'self'"],
    fontSrc: ["'self'"],
    imgSrc: ["'self'", "data:"],
    frameAncestors: ["'self'"],
  }
});
// var allowAllCspHandler = csp({
//   directives: {
//     defaultSrc: ['*',  'data: blob:', 'filesystem:', 'about:', 'ws:', 'wss:'],
//     scriptSrc: ['*', 'data: blob:', "'unsafe-inline'", "'unsafe-eval'"],
//     connectSrc: ['*', 'data:', 'blob:'],
//     imgSrc: ['*', 'data:', 'blob:'],
//     frameSrc: ['*', 'data:', 'blob:'], 
//     styleSrc: ['*', 'data:', 'blob:', "'unsafe-inline'"],
//     fontSrc: ['*', 'data:', 'blob:'],
//     frameAncestors: ['*', 'data:', 'blob:'],
//   }
// });

// //functions
// function formatDate(date){
//   return {
//     "date": dateFormat(date, "dd.mm.yyyy"),
//     "time": dateFormat(date, "HH:MM")
//   }
// }

var apiUsers = require('./users');
var apiEvents = require('./events');
var apiCars = require('./cars');
// var apiFiles = require('./files');
var apiProjects = require('./projects');
var apiTasks = require('./tasks');
var apiSubtasks = require('./subtasks');
var apiAbsences = require('./absences');
var apiBuildparts = require('./buildparts');
// var apiProjects = require('./projects');
// var apiSubscribers = require('./subscribers');
// var apiWorkOrders = require('./workorders');
// var apiReports = require('./reports');
//var apiImages = require('./image');
// var apiOAuth = require('./oauth');

///////////////////////////////////////////////////////////////////////////////////////OAUTH middleware
// router.use('/oauth', apiOAuth);
// router.use(require('./public'));

// calls that do not get their own files because they have one or two calls -> they are to get some info from database and its not used to add data to database (example: categories)
router.get('/cookie', auth.authenticate, function(req,res, next){
  // let userId = req.session.user_id;
  let cookieMaxAge = req.session.maxAge;
  res.json({success: true, cookieMaxAge});//200

});
//Get categories
router.get('/categories', auth.authenticate, function(req, res, next){
  dbCategories.getCategories().then(categories=>{
    res.json({success:true, data: categories});
  })
  .catch((e)=>{
    return res.json({success:false, error: e});
  })
})
//Get departments
router.get('/departments', auth.authenticate, function(req, res, next){
  dbCategories.getDepartments().then(departments =>{
    res.json({success:true, departments});
  })
  .catch((e)=>{
    return res.json({success:false, error: e});
  })
})
//Get priorities
router.get('/priorities', auth.authenticate, function(req, res, next){
  dbPriorities.getPriorities().then(priorities=>{
    res.json({success:true, data: priorities});
  })
  .catch((e)=>{
    return res.json({success:false, error: e});
  })
})


///////////////////////////////////////////////////////////////////////////////////////EVENTS
router.use('/events', normalCspHandler, auth.authenticate, apiEvents);
///////////////////////////////////////////////////////////////////////////////////////USERS/EMPLOYEES
router.use('/employees', normalCspHandler, auth.authenticate, apiUsers);
///////////////////////////////////////////////////////////////////////////////////////CARS & CAR RESERVATIONS
router.use('/cars', normalCspHandler, auth.authenticate, apiCars);
///////////////////////////////////////////////////////////////////////////////////////FILES
// router.use('/files', apiCspHandler, auth.authenticate, apiFiles);
///////////////////////////////////////////////////////////////////////////////////////PROJECTS
router.use('/projects', apiCspHandler, auth.authenticate, apiProjects);
///////////////////////////////////////////////////////////////////////////////////////TASKS
router.use('/tasks', apiCspHandler, auth.authenticate, apiTasks);
///////////////////////////////////////////////////////////////////////////////////////SUBTASKS
router.use('/subtasks', apiCspHandler, auth.authenticate, apiSubtasks);
///////////////////////////////////////////////////////////////////////////////////////BUILDPARTS
router.use('/buildparts', apiCspHandler, auth.authenticate, apiBuildparts);
///////////////////////////////////////////////////////////////////////////////////////ABSENCES
router.use('/absences', apiCspHandler, auth.authenticate, apiAbsences);
// ///////////////////////////////////////////////////////////////////////////////////////REPORTS
// router.use('/reports', oauth.authenticate(), apiReports);
// ///////////////////////////////////////////////////////////////////////////////////////SUBSCRIBERS
// router.use('/subscribers', oauth.authenticate(), apiSubscribers);
// ///////////////////////////////////////////////////////////////////////////////////////REPORTS
// router.use('/workorders', oauth.authenticate(), apiWorkOrders);
///////////////////////////////////////////////////////////////////////////////////////FILE?FOTO
module.exports = router;