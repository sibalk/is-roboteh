var express = require('express');
var router = express.Router();
const nodemailer = require('nodemailer');
var dateFormat = require('dateformat');

var auth = require('../../controllers/authentication');
var dbAbsences = require('../../model/absences/dbAbsences');
var dbEmployees = require('../../model/employees/dbEmployees');
// var dbService = require('../../model/service/dbServices');

//functions
function formatDate(date){
  return {
    "date": dateFormat(date, "d. m. yyyy"),
    "time": dateFormat(date, "HH:MM")
  }
}

///////////////////////////////////ADDITIONAL CALLS//////////////////////////
// GET all info related to absences (reasons, status, users)
router.get('/info', auth.authenticate, function(req, res, next){
  let loggedUserId = req.session.user_id;
  let promises = [];
  promises.push(dbEmployees.getUsersWithSupervisors(loggedUserId, 1));
  promises.push(dbAbsences.getReasons());

  Promise.all(promises).then(data=>{
    // data[0] = data[0].map(u => {return {id:u.id, name:u.name, surname:u.surname, role:u.role, text: u.name + ' ' + u.surname}})
    // data[1] = data[1].map(r => {return {id:r.id, reason:r.text, text: r.text}})
    res.json({success: true, users: data[0], reasons: data[1]});
  })
  .catch((e)=>{
    return res.json({success:false, error:e});
  })
})
// GET only reasons
router.get('/reasons', auth.authenticate, function(req, res, next){
  dbAbsences.getReasons().then(reasons => {
    // reasons = reasons.map(r => {return {id:r.id, reason:r.text, text: r.text}})
    res.json({reasons});
  })
  .catch((e)=>{
    return res.json({error:e.message}, 500);
  })
})
// for testing purposes - cron job for email notification on demand via API
router.get('/sendEmail', auth.authenticate, async function(req, res, next){
  let recipients = ['test@test.com'];
  let subject = 'Test email';
  let htmlMessage = `<h2> Test email </h2>
  <p>This is a test email.</p>
  <p>Lep pozdrav,
  <br/>Roboteh Informacijski Sistem</p>`;
  let textMessage = `This is a test email.
  Lep pozdrav,
  Roboteh Informacijski Sistem`;

  // first get all absences that are not approved
  let absences = await dbAbsences.getAbsences(null, null, null, '3', null, null);
  // get all users and roles for email info (one bigger query instead of multiple smaller)
  let users = await dbEmployees.getUsersWithSupervisors(req.session.user_id, 1);
  let roles = await dbEmployees.getRolesWithSupervisors(req.session.user_id);

  // buffer time in milliseconds (e.g., 5 minutes)
  const bufferTime = 5 * 60 * 1000;

  // for each unapproved absence send email to user's supervisors and to role supervisors
  for (const absence of absences) {
    let supervisorsEmails = [];
    // find all users that are supervisors of user with absence
    let user = users.find(u => u.id == absence.id_user);
    let role = roles.find(r => r.id == user.role_id);
    // add every mail to array from user supervisors and role supervisors
    let userSupervisors = user.supervisors_ids ? user.supervisors_ids.split(',') : [];
    let roleSupervisors = role.supervisor_ids ? role.supervisor_ids.split(',') : [];
    supervisorsEmails = userSupervisors.concat(roleSupervisors);
    // remove duplicates
    supervisorsEmails = supervisorsEmails.filter((v, i, a) => a.indexOf(v) === i);
    // get emails from users
    supervisorsEmails = supervisorsEmails.map(s => users.find(u => u.id == s).work_mail);
    // add user work mail if he has it
    // if (user.work_mail) supervisorsEmails.push(user.work_mail);

    let now = new Date();
    let firstNotificationDate = absence.notification_date ? new Date(absence.notification_date) : null;
    let reminderDate = absence.reminder_date ? new Date(absence.reminder_date) : null;
    let start = new Date(absence.start);
    let finish = new Date(absence.finish);
    start = formatDate(start);
    finish = formatDate(finish);

    if (!firstNotificationDate) {
      // fix htmlMessage and textMessage to include absence name and dates
      htmlMessage = `<h2> Obvestilo o novi odsotnosti ${absence.reason} </h2>
      <p>Pozdravljeni,</p>
      <p>Obveščamo vas, da je bila dodana osebi <b>${user.name} ${user.surname}</b> nova odsotnost <i>${absence.reason}</i> od <b>${start.date}</b> do <b>${finish.date}</b>.</p>
      <p>Lep pozdrav,
      <br/>Roboteh Informacijski Sistem</p>`;
      textMessage = `Pozdravljeni,
      Obveščamo vas, da je bila dodana osebi ${user.name} ${user.surname} nova odsotnost ${absence.reason} od ${start.date} do ${finish.date}.
      Lep pozdrav,
      Roboteh Informacijski Sistem`;

      if (supervisorsEmails.length > 0) {
        // send first notification email
        await sendEmail(supervisorsEmails, 'Odsotnost - čakanje na odobritev', htmlMessage, textMessage);
      }
      await dbAbsences.updateAbsenceNotificationMails(absence.id, supervisorsEmails.toString(), now);

    } else if (!reminderDate && now - firstNotificationDate >= 2 * 24 * 60 * 60 * 1000 - bufferTime) {
      // fix htmlMessage and textMessage to include absence name and dates
      htmlMessage = `<h2> Opomnik za odsotnost ${absence.reason} - čakanje na odobritev </h2>
      <p>Pozdravljeni,</p>
      <p>Obveščamo vas, da je bila dodana osebi <b>${user.name} ${user.surname}</b> nova odsotnost <i>${absence.reason}</i> od <b>${start.date}</b> do <b>${finish.date}</b>.</p>
      <p>Lep pozdrav,
      <br/>Roboteh Informacijski Sistem</p>`;
      textMessage = `Pozdravljeni,
      Obveščamo vas, da je bila dodana osebi ${user.name} ${user.surname} nova odsotnost ${absence.reason} od ${start.date} do ${finish.date}.
      Lep pozdrav,
      Roboteh Informacijski Sistem`;

      if (supervisorsEmails.length > 0) {
        // send reminder email if 2 days have passed since the first notification
        await sendEmail(supervisorsEmails, 'Opomnik za odsotnost - čakanje na odobritev', htmlMessage, textMessage);
      }
      await dbAbsences.updateAbsenceReminderMails(absence.id, supervisorsEmails.toString(), now);
    } else if (reminderDate && now - reminderDate >= 2 * 24 * 60 * 60 * 1000 - bufferTime) {
      // fix htmlMessage and textMessage to include absence name and dates
      htmlMessage = `<h2> Zadnji opomnik za odsotnost ${absence.reason} - čakanje na odobritev </h2>
      <p>Pozdravljeni,</p>
      <p>Obveščamo vas, da je bila dodana osebi <b>${user.name} ${user.surname}</b> nova odsotnost <i>${absence.reason}</i> od <b>${start.date}</b> do <b>${finish.date}</b>.</p>
      <p>Lep pozdrav,
      <br/>Roboteh Informacijski Sistem</p>`;
      textMessage = `Pozdravljeni,
      Obveščamo vas, da je bila dodana osebi ${user.name} ${user.surname} nova odsotnost ${absence.reason} od ${start.date} do ${finish.date}.
      Lep pozdrav,
      Roboteh Informacijski Sistem`;

      // send email to admins if 2 days have passed since the reminder email
      let ceoEmails = ['srecko.potocnik@roboteh.si', 'cveto.jakopic@roboteh.si', 'bostjan.skarlovnik@roboteh.si'];
      await sendEmail(ceoEmails, 'Zadnji opomnik za odsotnost', htmlMessage, textMessage);
      await dbAbsences.updateAbsenceAdminMails(absence.id, ceoEmails.toString(), now);
    }
  }

  return res.json({success:true});
});

///////////////////////////////////ABSENCES//////////////////////////
// GET all absences
router.get('/', auth.authenticate, async function(req, res, next){
  try {
    let absencesActivity = (req.query.absencesActivity) ? req.query.absencesActivity : null;
    let absencesLimit = (req.query.absencesLimit) ? req.query.absencesLimit : null;
    let absencesUserId = (req.query.absencesUserId) ? req.query.absencesUserId : null;
    let absencesUserGroups = (req.query.absencesUserGroups) ? req.query.absencesUserGroups : null;
    let absencesStatus = (req.query.absencesStatus) ? req.query.absencesStatus : null;
    let absencesDate = (req.query.absencesDate) ? req.query.absencesDate : null;
  
    let roleType = req.session.type.toLowerCase();
    let loggedUserId = req.session.user_id;
  
    // get users and roles for checking if user can see selected user based on supervising
    // let users = await dbEmployees.getUsersWithSupervisors(req.session.user_id, 1);
    // let roles = await dbEmployees.getRolesWithSupervisors(req.session.user_id, 1);
    // // case where user is not admin and is not supervisor of anyone or any role-> show only his absences
    // if (!users || !roles || (roleType != 'admin' && !users.find(u => u.id == req.session.user_id).is_supervisor && !roles.find(r => r.id == users.find(u => u.id == req.session.user_id).role).is_supervisor)){
    //   absencesUserId = req.session.user_id;
    // }
    // // case where user is admin or supervisor of someone -> show selected user absences
    // else if (absencesUserId && absencesUserId != req.session.user_id){
    //   let user = users.find(u => u.id == absencesUserId);
    //   let userRole = roles.find(r => r.id == user.role_id);
    //   let userSupervisor = user.is_supervisor ? user.is_supervisor : false;
    //   let userRoleSupervisor = userRole.is_supervisor ? userRole.is_supervisor : false;
    //   absencesUserId = (roleType == 'admin' || userSupervisor || userRoleSupervisor ) ? absencesUserId : req.session.user_id;
    // }
  
    const absences = await dbAbsences.getAbsences(absencesUserId, absencesDate, absencesUserGroups, absencesStatus, absencesActivity, absencesLimit, roleType, loggedUserId);
    res.json({ absences });
  }
  catch (e) {
    return res.json({error:e.message}, 500);
  }
})
// GET - pridovitev podatkov za izbrano odsotnost
router.get('/:id', auth.authenticate, function(req,res, next){
  dbAbsences.getAbsenceById(req.params.id)
  .then(absence=>{
    if (absence) return res.json({absence});
    else throw new Error(404);
  })
  .catch((e)=>{
    switch (parseInt(e.message)) {
      case 400: return res.json({message: 'Bad request'}, 400);
      case 403: return res.json({message: 'Forbidden'}, 403);
      case 404: return res.json({message: 'Not Found', info: 'Odsotnost s takšnim identifikatorjem ne obstaja'}, 404);
      default: return res.json({error: e.message}, 500);
    }
  })
});
// POST new absence
router.post('/', auth.authenticate, async function(req, res, next){
  try {
    let absenceName = req.body.absenceName;
    let absenceReason = req.body.absenceReason;
    let absenceUser = req.body.absenceUser;
    let absenceStart = req.body.absenceStart ? req.body.absenceStart + ' 07:00:00' : null;
    let absenceFinish = req.body.absenceFinish ? req.body.absenceFinish + ' 15:00:00' : null;
    let absenceUserRole = req.body.absenceUserRole ? req.body.absenceUserRole.toLowerCase() : null;
    let absenceStatus = req.body.absenceStatus;
    let absenceNote = req.body.absencePendingNote;

    if (!absenceUser){
      // no user id -> only possible if regular user with no special role is adding his own absence (asking for approval)
      absenceUser = req.session.user_id;
    }
    else if(absenceUser == req.session.user_id && !absenceStatus){
      // user is adding his own absence -> asking for approval and cannot approve his own absence
    }
    else{
      // get users and roles for checking if user can see selected user based on supervising
      let user = await dbEmployees.getUserById(absenceUser, req.session.user_id);
      let roles = await dbEmployees.getRolesWithSupervisors(req.session.user_id);

      // check if user has is_supervisor or if user part of role group where logged user is supervisor or is admin -> if not then he can't add absence for other user so its forbidden
      if (!(user.is_supervisor || roles.find(r => r.id == user.role_id).is_supervisor || req.session.type.toLowerCase() == 'admin')){
        return res.json({message: 'Forbidden', info:'Nimate pravice dodajati odsotnosti za zaposlene v drugem oddelku.'}, 403);
      }
    }
    // if no reason or start or finish -> return bad request
    if(!absenceReason || !absenceStart || !absenceFinish){
      return res.json({message: 'Bad request', info:'Manjkajoči podatki. Ni razloga odsotnosti oz. začetka oz. konca.'}, 400);
    }
    //just for testing now -> POST req is OK, return success is true
    let absence = await dbAbsences.addNewAbsence(absenceStart, absenceFinish, absenceReason, absenceUser, absenceName, absenceStatus, absenceNote);
    // send email to user that his absence is added, also send to ure@ so she can put it in the system
    let user = await dbEmployees.getUserById(absenceUser);
    let recipients = [];
    // add user work mail if he has it
    if (user.work_mail) recipients.push(user.work_mail);
    let subject = 'Nova odsotnost ' + absence.name;
    let start = new Date(absence.start);
    let finish = new Date(absence.finish);
    start = formatDate(start);
    finish = formatDate(finish);
    // mesaage for email (html and text)
    let htmlMessage = `<h2> Obvestilo o novi odsotnosti ${absence.reason}</h2>
    <p>Pozdravljeni,</p>
    <p>Obveščamo vas, da je bila dodana osebi <b>${user.name} ${user.surname}</b> nova odsotnost <i>${absence.reason}</i> od <b>${start.date}</b> do <b>${finish.date}</b>.</p>
    <p>Lep pozdrav,
    <br/>Roboteh Informacijski Sistem</p>`;
    let textMessage = `Pozdravljeni,
    Obveščamo vas, da je bila dodana osebi ${user.name} ${user.surname} nova odsotnost ${absence.reason} od ${start.date} do ${finish.date}.
    Lep pozdrav,
    Roboteh Informacijski Sistem`;

    if(absenceStatus == 1)
      recipients.push('ure@roboteh.si');
    // for testing purposes -> send email to me
    // recipients.push('klemen.sibal@roboteh.si');
    // send mail only if there are any recipients
    if (recipients.length > 0 && absenceStatus == 1)
      await sendEmail(recipients, subject, htmlMessage, textMessage);

    return res.json({absence});
  }
  catch (e) {
    return res.json({error:e.message}, 500);
  }
})
// PUT - update absence
router.put('/:id', auth.authenticate, async function(req, res, next){
  try {
    let absenceId = req.params.id;
    let name = req.body.absenceName;
    let start = req.body.absenceStart + ' 07:00:00';
    let finish = req.body.absenceFinish + ' 15:00:00';;
    let reasonId = req.body.absenceReason;
    let status = req.body.absenceStatus;
    let pendingNote = req.body.absencePendingNote;
    let resolveNote = req.body.absenceResolveNote;
  
    let roleType = req.session.type.toLowerCase(); // for checking if user is admin otherwise check if user is supervisor
    let absence = await dbAbsences.getAbsenceById(absenceId);
    // if there is no absence -> return error 404
    if (!absence) return res.json({message: 'Not Found', info: 'Odsotnost s takšnim identifikatorjem ne obstaja'}, 404);
    let absenceRole = absence.role.toLowerCase();
    // get users and roles for checking if user can see selected user based on supervising
    let user = await dbEmployees.getUserById(absence.id_user, req.session.user_id);
    let roles = await dbEmployees.getRolesWithSupervisors(req.session.user_id);

    if (status != null) {
      // user is approving or rejecting absence -> check if user can accept/reject based on his role or supervising
      // admin can approve/reject everything, others needs to be supervisor of user or user's role
      if (!(roleType == 'admin' || user.is_supervisor || roles.find(r => r.id == user.role_id).is_supervisor)) {
        return res.json({message: 'Forbidden', info:'Nimate pravice spreminjati statusa odsotnosti.'}, 403);
      }
      // everthing ok -> make absence update
      const updatedAbsence = await dbAbsences.approveAbsence(absenceId, status, resolveNote);
      // send email to user that his absence is approved/rejected, also send to tajnik so he can put it in the system
      let recipients = [];
      // add user work mail if he has it
      if (user.work_mail) recipients.push(user.work_mail);
      let subject = '';
      let statusText = status == 1 ? 'odobrena' : 'zavrnjena';
      let ResponseNote = resolveNote ? `Opomba: ${resolveNote}` : '';
      // iser formatDate to format start and finish in format DD. MM. YYYY
      let absenceStart = new Date(updatedAbsence.start);
      let absenceFinish = new Date(updatedAbsence.finish);
      absenceStart = formatDate(absenceStart);
      absenceFinish = formatDate(absenceFinish);
      // let absenceStart = new Date(updatedAbsence.start);
      // let absenceFinish = new Date(updatedAbsence.finish);
      // mesaage for email (html and text)
      let htmlMessage = `<h2> Obvestilo o posodobljeni odsotnosti ${updatedAbsence.reason} </h2>
      <p>Pozdravljeni,</p>
      <p>Obveščamo vas, da je bila osebi <b>${user.name} ${user.surname}</b> odsotnost <i>${updatedAbsence.reason}</i> od <b>${absenceStart.date}</b> do <b>${absenceFinish.date}</b> <b>${statusText}</b>.</p>
      <p>${ResponseNote}</p>
      <p>Lep pozdrav,
      <br/>Roboteh Informacijski Sistem</p>`;
      let textMessage = `Pozdravljeni,
      Obveščamo vas, da je bila osebi ${user.name} ${user.surname} odsotnost <i>${updatedAbsence.reason}</i> od ${absenceStart.date} do ${absenceFinish.date} ${statusText}.
      ${ResponseNote}
      
      Lep pozdrav,
      Roboteh Informacijski Sistem`;
      if (status == 0) {
        subject = `Odsotnost ${updatedAbsence.name} zavrnjena`;
      }
      else if (status == 1) {
        subject = `Odsotnost ${updatedAbsence.name} odobrena`;
        recipients.push('ure@roboteh.si');
      }
      // for testing purposes -> send email to me
      // recipients.push('klemen.sibal@roboteh.si');
      // send mail only if there are any recipients
      if (recipients.length > 0)
        await sendEmail(recipients, subject, htmlMessage, textMessage);

      return res.json({absence: updatedAbsence});
    }
    else{
      if (!reasonId || !start || !finish) {
        return res.json({message: 'Bad Request', info:'Manjkajoči podatki. Ni razloga odsotnosti oz. začetka oz. konca.'}, 400);
      }
      // regular absence update
      // admin and is_supervisor can edit absence, others can edit their own absence if it is not approved
      if (!(roleType == 'admin' || user.is_supervisor || roles.find(r => r.id == user.role_id).is_supervisor || (absence.id_user == req.session.user_id && absence.approved == null))) {
        return res.json({message: 'Forbidden', info:'Nimate pravice urejati odsotnosti.'}, 403);
      }
      // everthing ok -> make absence update
      const updatedAbsence = await dbAbsences.updateAbsence(absenceId, start, finish, reasonId, name, pendingNote);
      return res.json({absence: updatedAbsence});
    }
  }
  catch (e) {
    return res.json({error:e.message}, 500);
  }
})
// DELETE absence
router.delete('/', auth.authenticate, function(req, res, next){
  let id = req.body.absenceId;
  let active = req.body.active;
  // return res.json({success:false});
  dbAbsences.deleteAbsence(id, active)
  .then(absence=>{
    return res.json({absence});
  })
  .catch((e)=>{
    return res.json({error:e.message, info: 'Napaka pri brisanju odsotnosti (posodobitvi aktivnosti).'}, 500);
  })
})
//get all changes for task or subtask
router.get('/:id/changes', auth.authenticate, function(req, res, next){
  let absenceId = req.params.id;
  dbAbsences.getAbsenceChangesSystem(absenceId).then(absenceChanges => {
    if (absenceChanges) return res.json({absenceChanges});
    else throw new Error(404);
  })
  .catch((e)=>{
    switch (parseInt(e.message)) {
      case 400: return res.json({message: 'Bad request'}, 400);
      case 403: return res.json({message: 'Forbidden'}, 403);
      case 404: return res.json({message: 'Not Found', info: 'Odsotnost s takšnim identifikatorjem ne obstaja'}, 404);
      default: return res.json({error: e.message}, 500);
    }
  })
})

// async function get user with supervisors
async function getUsersWithSupervisorsAndHisRole(){
  let users = await dbEmployees.getUsersWithSupervisors(req.session.user_id);
  let roles = await dbEmployees.getRolesWithSupervisors(req.session.user_id);
  return users, roles;
}

module.exports = router;

// send email function
async function sendEmail(recipients, subject, htmlMessage, textMessage) {
  // Create a transporter object using SMTP transport and check if it is ethereal(testing env) or real(real env) email
  let transporter;
  if(process.env.MAIL_HOST){
    transporter = nodemailer.createTransport({
      host: process.env.MAIL_HOST,
      port: process.env.MAIL_PORT,
      secure: true,
      auth: {
        user: process.env.MAIL_USERNAME,
        pass: process.env.MAIL_PASSWORD
      },
      tls: {
        // do not fail on invalid certs
        rejectUnauthorized: false
      }
    });
  }
  if(process.env.ETHEREAL_HOST){
    transporter = nodemailer.createTransport({
      host: process.env.ETHEREAL_HOST,
      port: process.env.ETHEREAL_PORT,
      auth: {
        user: process.env.ETHEREAL_USERNAME,
        pass: process.env.ETHEREAL_PASSWORD
      },
    }); 
  }
  // attachments for adding picture in signature
  var attachmentsElement = [
    {
      filename: 'kuka.png',
      path: __dirname+`/../..//icons/logo/kuka_partner.png`,
      cid: 'kuka@partner' //same cid value as in the html img src
    }
  ];
  // add it to the htmlMessage
  htmlMessage = htmlMessage + `<img src="cid:kuka@partner" alt="Kuka Partner" />`;

  // Define email options
  let mailOptions = {
    from: 'odsotnosti@roboteh.si', // Replace with your name and email
    to: recipients.join(','), // Join the array of emails into a single string
    subject: subject,
    text: textMessage,
    html: htmlMessage,
    attachments: attachmentsElement
  };

  // Send the email
  try {
    let info = await transporter.sendMail(mailOptions);
    console.log('Message sent: %s', info.messageId);
  } catch (error) {
    console.error('Error sending email: %s', error.message);
  }
}