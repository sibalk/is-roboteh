var express = require('express');
var router = express.Router();

var auth = require('../../controllers/authentication');
var dbSubtasks = require('../../model/subtasks/dbSubtasks');
var dbTasks = require('../../model/tasks/dbTasks');
var dbProjects = require('../../model/projects/dbProjects');
var dbChanges = require('../../model/changes/dbChanges');
var dbUsers = require('../../model/employees/dbEmployees');

// GET ALL SUBTASKS FOR SPECIFIC TASK
router.get('/', auth.authenticate, function(req,res, next){
  let taskId = req.query.taskId;
  dbSubtasks.getSubTasksById(taskId).then(subtasks => {
    res.json({subtasks});//200
  })
  .catch((e)=>{
    switch (parseInt(e.message)) {
      case 400: return res.json({message: 'Bad request'}, 400);
      case 403: return res.json({message: 'Forbidden'}, 403);
      case 404: return res.json({message: 'Not Found'}, 404);
      default: return res.json({error: e.message}, 500);
    }
  })
});
// PATCH SUBTASKS
router.patch('/:id', auth.authenticate, function(req,res, next){
  let subtaskId = req.params.id;
  let completion = req.body.completion;
  let userId = req.session.user_id;
  let subtask, task, project, allActiveTasks, allTaskSubtasks;

  if (completion == null || (typeof completion == 'string' && !(completion == 'true' || completion == 'false')) || !(typeof completion == 'string' || typeof completion == 'boolean'))
    return res.sendStatus(400);
  
  dbSubtasks.getSubTask(subtaskId)
  .then(dataSubtask => {
    if (!dataSubtask)
      throw new Error(404);
    subtask = dataSubtask;
    return Promise.all([dbSubtasks.getSubTasksById(subtask.id_task), dbTasks.getTaskData(subtask.id_task)])
  })
  .then(([allTaskSubtasksData, taskData]) => {
    task = taskData;
    allTaskSubtasks = allTaskSubtasksData;
    return Promise.all([dbProjects.getWorkersByProjectId(task.project_id), dbProjects.getProjectById(task.project_id), dbTasks.getTasksByProjectIdFixed(task.project_id, 0, 1), dbUsers.getUsers()]);
  })
  .then(([projectWorkers, projectData, allActiveTasksData, users]) => {
    project = projectData;
    allActiveTasks = allActiveTasksData;
    let = elektroUserCheck = false, strojnaUserCheck = false;
    if (req.session.type.toLowerCase().substring(0,4) == 'info'){
      for (const worker of task.workers_id.split(',')) {
        if(users.find(u => u.id == worker && u.role.toLowerCase() == 'električar')) elektroUserCheck = true;
        if(users.find(u => u.id == worker && (u.role.toLowerCase() == 'strojnik' || u.role.toLowerCase() == 'varilec' || u.role.toLowerCase() == 'cnc operater') )) strojnaUserCheck = true;
      }
    }
    // check if user has permission to patch completion --> admins, project leaders, role leaders for role, assigned users, info users for department, author of task
    if (!(req.session.type.toLowerCase() == 'admin' || 
    projectWorkers.find(w => w.id == req.session.user_id && (w.workRole.toLowerCase() == 'vodja projekta' || w.workRole.toLowerCase() == 'vodja')) ||
    task.workers_id.split(',').find(w => w == req.session.user_id) ||
    task.author_id == req.session.user_id || 
    req.session.type.toLocaleLowerCase() == 'vodja strojnikov' && task.category.toLocaleLowerCase() == 'strojna izdelava' ||
    req.session.type.toLocaleLowerCase() == 'info strojna' && task.category.toLocaleLowerCase() == 'strojna izdelava' ||
    req.session.type.toLocaleLowerCase() == 'vodja električarjev' && task.category.toLocaleLowerCase() == 'elektro izdelava' ||
    req.session.type.toLocaleLowerCase() == 'info elektro' && elektroUserCheck)) {
      throw new Error(403);
    }
    return dbSubtasks.isSubtaskCompleted(subtaskId, completion);
  })
  .then(updatedSubtask => {
    // subtask was patched, save change to database and calculate new task percentage and if new percetage changed finished completion of task also calculate new percetage for project and also save all changes of task and project
    let completionPromises = [];
    let newTaskCompletion = 0;
    let newProjectCompletion = 0;
    updatedSubtask.completed ? completionPromises.push(dbChanges.addNewChangeSystem(4, 3, userId, project.id, task.id, subtaskId)) : completionPromises.push(dbChanges.addNewChangeSystem(5, 3, userId, project.id, task.id, subtaskId));
    
    allTaskSubtasks.find(s => s.id == subtaskId).completed = updatedSubtask.completed;
    let activeSubtaskLength = 0;
    for (const subtask of allTaskSubtasks) { 
      if (subtask.completed && subtask.active) newTaskCompletion++;
      if (subtask.active) activeSubtaskLength++;
    }
    allTaskSubtasks.length == 0 ? newTaskCompletion = 0: newTaskCompletion = Math.round(newTaskCompletion/activeSubtaskLength*100);
    completionPromises.push(dbTasks.updateTaskCompletion(task.id, newTaskCompletion));
    if (task.completion != 100 && newTaskCompletion == 100) completionPromises.push(dbChanges.addNewChangeSystem(4, 2, userId, project.id, task.id));
    if (task.completion == 100 && newTaskCompletion != 100) completionPromises.push(dbChanges.addNewChangeSystem(5, 2, userId, project.id, task.id));

    allActiveTasks.find(t => t.id == task.id).completion = newTaskCompletion;
    for (const task of allActiveTasks) { newProjectCompletion += task.completion; }
    allActiveTasks.length == 0 ? newProjectCompletion = 0: newProjectCompletion = Math.round(newProjectCompletion/allActiveTasks.length);
    if (project.completion != newProjectCompletion) completionPromises.push(dbProjects.updateCompletion(project.id, newProjectCompletion));
    if (project.completion != 100 && newProjectCompletion == 100) completionPromises.push(dbChanges.addNewChangeSystem(4, 1, userId, project.id));
    if (project.completion == 100 && newProjectCompletion != 100) completionPromises.push(dbChanges.addNewChangeSystem(5, 1, userId, project.id));

    return Promise.all(completionPromises)
  })
  .then(completionChanges => {
    return res.json({subtask});//200
  })
  .catch((e)=>{
    switch (parseInt(e.message)) {
      case 400: return res.json({message: 'Bad request'}, 400);
      case 403: return res.json({message: 'Forbidden'}, 403);
      case 404: return res.json({message: 'Not Found'}, 404);
      default: return res.json({error: e.message}, 500);
    }
  })
});

module.exports = router;