var express = require('express');
var router = express.Router();

var auth = require('../../controllers/authentication');
var dbUsers = require('../../model/employees/dbEmployees');

// ROLES (SYSTEM) CALS
// get all roles with their supervisors
router.get('/roles', auth.authenticate, function(req,res, next){
  let loggedUserId = req.session.user_id;
  dbUsers.getRolesWithSupervisors(loggedUserId).then(roles => {
    res.json({roles});//200
  })
  .catch((e)=>{
    console.log(e);
    return res.status(500).json({
      status: 'error',
      message: 'Something went wrong',
      error: e,
    })
  })
});
// update role with supervisor
router.put('/roles/:id', auth.authenticate, async function(req,res, next){
  let roleId = req.params.id;
  let supervisorsArray = req.body.supervisors;
  // return bad request if supervisors is not defined
  if(!supervisorsArray){
    return res.status(400).json({status: 'error', message: 'Supervisors not defined', error: 'Supervisors not defined'});//400
  }
  if(supervisorsArray[0] == "")
    supervisorsArray.pop();
  
  try{
    // get role to check if it exists and if what supervisor has to add or remove
    let role = await dbUsers.getRoleById(roleId);
    if(role.length == 0){
      return res.status(404).json({status: 'error', message: 'Role not found', error: 'Role not found'});//404
    }
    if (req.session.type.toLowerCase() !== 'admin') {
      return res.status(403).json({status: 'error', message: 'You are not allowed to update this role', error: 'You are not allowed to update this role'});//403
    }
    // compare every id from supervisorsArraz with role.supervisors_ids and add new ids and remove old ids
    let currentSupervisorsIds = role.supervisor_ids ? role.supervisor_ids.split(',') : [];
    for(let i=0; i<supervisorsArray.length; i++){
      if(currentSupervisorsIds.indexOf(supervisorsArray[i]) == -1){
        await dbUsers.addRoleSupervisor(roleId, supervisorsArray[i]);
      }
    }
    for(let i=0; i<currentSupervisorsIds.length; i++){
      if(supervisorsArray.indexOf(currentSupervisorsIds[i]) == -1){
        await dbUsers.removeRoleSupervisor(roleId, currentSupervisorsIds[i]);
      }
    }

    // return updated role
    let updatedRole = await dbUsers.getRoleById(roleId); 
    res.json({role: updatedRole});//200
  }
  catch(e){
    console.log(e);
    return res.status(500).json({
      status: 'error',
      message: 'Something went wrong',
      error: e,
    })
  }
});

// USERS (SYSTEM) CALS
//get user projects (projects where users is part of)
// get all users with role info
router.get('/', auth.authenticate, function(req,res, next){
  let loggedUserId = req.session.user_id;
  dbUsers.getUsersWithSupervisors(loggedUserId).then(users => {
    res.json({users});//200
  })
  .catch((e)=>{
    console.log(e);
    return res.status(500).json({
      status: 'error',
      message: 'Something went wrong',
      error: e,
    })
  })
});
//get user projects (projects where users is part of)
// get selected user
router.get('/:id', auth.authenticate, function(req,res, next){
  //get token to verify user and get his userId
  let userId = req.params.id;
  dbUsers.getUserById(userId, req.session.user_id).then(user => {
    res.json({user});//200
  })
  .catch((e)=>{
    console.log(e);
    return res.status(500).json({
      status: 'error',
      message: 'Something went wrong',
      error: e,
    })
  })
});
module.exports = router;