var express = require('express');
var router = express.Router();

var auth = require('../../controllers/authentication');
var dbCars = require('../../model/cars/dbCars');
var dbChanges = require('../../model/changes/dbChanges');

///////////////////////////////////CAR NOTES STATUS//////////////////////////
router.get('/notes/status', auth.authenticate, function(req,res, next){

  dbCars.getCarNotesStatus().then(carStatus => {
    res.json({carStatus});//200
  })
  .catch((e)=>{
    return res.status(500).json({
      status: 'error',
      message: 'Something went wrong',
      error: e,
    })
  })
});

///////////////////////////////////CAR NOTES//////////////////////////
// get all notes
router.get('/notes/', auth.authenticate, function(req,res, next){
  dbCars.getCarNotes().then(carNotes => {
    res.json({carNotes});//200
  })
  .catch((e)=>{
    return res.status(500).json({
      status: 'error',
      message: 'Something went wrong',
      error: e,
    })
  })
});
// get selected note
router.get('/notes/:id', auth.authenticate, function(req,res, next){
  let noteId = req.params.id;
  dbCars.getCarNoteById(noteId).then(carNote => {
    res.json({carNote});//200
  })
  .catch((e)=>{
    return res.status(500).json({
      status: 'error',
      message: 'Something went wrong',
      error: e,
    })
  })
});
// add new car note
router.post('/notes/', auth.authenticate, function(req,res, next){
  let start = req.body.start;
  let end = req.body.end;
  let carId = req.body.carId;
  let statusId = req.body.statusId ? req.body.statusId : 1;
  let userId = req.session.user_id;
  let note = req.body.note;
  let carNote;

  if (!carId || !note)
     return res.sendStatus(400);
  
  dbCars.addNewCarNote(userId, statusId, carId, start, end, note).then(newCarNote => {
    carNote = newCarNote;
    return dbChanges.addNewChangeSystem(1,45,req.session.user_id,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,carId,null,newCarNote.id)
  })
  .then(change => {
    return res.json({carNote});//200
  })
  .catch((e)=>{
    switch (parseInt(e.message)) {
      case 400: return res.json({message: 'Bad request'}, 400);
      case 403: return res.json({message: 'Forbidden'}, 403);
      case 404: return res.json({message: 'Not Found'}, 404);
      default: return res.json({error: e.message, message: 'Error while creating car into database and adding new change for new car.'}, 500);
    }
  })
});
// PUT car note - update car note
router.put('/notes/:id', auth.authenticate, function(req,res, next){
  let carNoteId = req.params.id;
  let carId = req.body.carId;
  let statusId = req.body.statusId;
  let start = req.body.start;
  let end = req.body.end;
  let note = req.body.note;
  
  if (!carNoteId || !statusId || !note)
     return res.sendStatus(400);
  dbCars.getCarNoteById(carNoteId)
  .then(carNote => {
    if (!carNote)
      throw new Error(404);
    // check if user has permission to edit car note -> admin has full control, other user can edit notes that they have created
    if (!(req.session.type.toLowerCase() == 'admin' || req.session.user_id == carNote.author_id))
      throw new Error(403);
    return Promise.all([dbCars.editCarNote(carNoteId, statusId, start, end, note), dbChanges.addNewChangeSystem(2,45,req.session.user_id,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,carNote.car_id,null,carNoteId)])
  })
  .then(([updatedCarNote, change]) => {
    return res.json({carNote:updatedCarNote});//200
  })
  .catch((e)=>{
    switch (parseInt(e.message)) {
      case 400: return res.json({message: 'Bad request'}, 400);
      case 403: return res.json({message: 'Forbidden'}, 403);
      case 404: return res.json({message: 'Not Found'}, 404);
      default: return res.json({error: e.message, message: 'Error while updating car note.'}, 500);
    }
  })
});
// delete car note
router.delete('/notes/:id', auth.authenticate, function(req,res, next){
  let carNoteId = req.params.id;
  let activity = req.body.activity ? req.body.activity : false;
  let deleteStatus = activity ? 6 : 3;
  
  dbCars.getCarNoteById(carNoteId)
  .then(carNote =>{
    if (!carNote)
      throw new Error(404);
    // check if user has permission to delete car note -> admin has full control, other user can delete notes that they have created
    if (!(req.session.type.toLowerCase() == 'admin' || req.session.user_id == carNote.author_id))
      throw new Error(403);
  
    return Promise.all([dbCars.deleteCarNote(carNoteId,activity),dbChanges.addNewChangeSystem(deleteStatus,45,req.session.user_id,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,carNote.car_id,null,carNote.id)])
  })
  .then(([carNote, change]) => {
    return res.json({carNote});//200
  })
  .catch((e)=>{
    switch (parseInt(e.message)) {
      case 400: return res.json({message: 'Bad request'}, 400);
      case 403: return res.json({message: 'Forbidden'}, 403);
      case 404: return res.json({message: 'Not Found'}, 404);
      default: return res.json({error: e.message, message: 'Error while creating car into database and adding new change for new car.'}, 500);
    }
  })
});
///////////////////////////////////CAR RESERVATIONS//////////////////////////
router.get('/reservations/', auth.authenticate, function(req,res, next){
  let userId = req.session.user_id;
  let start = req.query.start;
  let end = req.query.end;

  dbCars.getCarReservation(userId, start, end).then(carRevs => {
    res.json({carRevs});//200
  })
  .catch((e)=>{
    return res.status(500).json({
      status: 'error',
      message: 'Something went wrong',
      error: e,
    })
  })
});
// get selected car reservations
router.get('/reservations/:id', auth.authenticate, function(req,res, next){

  let carRevId = req.params.id;
  dbCars.getCarReservationById(carRevId).then(carRev => {
    res.json({carRev});//200
  })
  .catch((e)=>{
    return res.status(500).json({
      status: 'error',
      message: 'Something went wrong',
      error: e,
    })
  })
});
// add new car reservation
router.post('/reservations/', auth.authenticate, function(req,res, next){
  let start = req.body.start;
  let end = req.body.end;
  let carId = req.body.carId;
  let color = req.body.color ? req.body.color : '#1a73e8';
  let userId = req.session.user_id;
  let ownerId = req.session.user_id;
  let carRev;
  // no role control for now, everyone can create reservation, should create inside his department but sometimes there is no free car left
  
  dbCars.addNewCarRev(start, end, ownerId, userId, carId, color)
  .then(carRevData => {
    carRev = carRevData;
    return dbChanges.addNewChangeSystem(1,44,req.session.user_id,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,carRevData.car,carRevData.id)
  })
  .then(change => {
    return res.json({carRev});//200
  })
  .catch((e)=>{
    switch (parseInt(e.message)) {
      case 400: return res.json({message: 'Bad request'}, 400);
      case 403: return res.json({message: 'Forbidden'}, 403);
      case 404: return res.json({message: 'Not Found'}, 404);
      default: return res.json({error: e.message, message: 'Error while creating car into database and adding new change for new car.'}, 500);
    }
  })
});
// update/edit car reservation
router.put('/reservations/:id', auth.authenticate, function(req,res, next){
  let carRevId = req.params.id;
  let start = req.body.start;
  let end = req.body.end;
  let color = req.body.color;
  let keys = req.body.keys;
  let location = req.body.location;
  let userId = req.body.userId;
  let status = req.body.status;
  let car = req.body.car;

  dbCars.getCarReservationById(carRevId)
  .then(carRev => {
    // check if user has permission to edit car reservation -> admin has full control, other user can edit reservation for themself
    if (!(req.session.type.toLowerCase() == 'admin' || req.session.user_id == carRev.owner))
      throw new Error(403);
  
    return Promise.all([dbCars.editCarRev(carRevId, start, end, keys, location, userId, status, color, car),dbChanges.addNewChangeSystem(2,44,req.session.user_id,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,car,carRevId)])
  })
  .then(([carRev, change]) => {
    return res.json({carRev});//200
  })
  .catch((e)=>{
    switch (parseInt(e.message)) {
      case 400: return res.json({message: 'Bad request'}, 400);
      case 403: return res.json({message: 'Forbidden'}, 403);
      case 404: return res.json({message: 'Not Found'}, 404);
      default: return res.json({error: e.message, message: 'Error while creating car into database and adding new change for new car.'}, 500);
    }
  })
});
// delete car reservation
router.delete('/reservations/:id', auth.authenticate, function(req,res, next){
  let carRevId = req.params.id;
  let activity = req.body.activity ? req.body.activity : false;
  let deleteStatus = activity ? 6 : 3;
  
  dbCars.getCarReservationById(carRevId)
  .then(carRev =>{
    // check if user has permission to edit car reservation -> admin has full control, other user can delete reservation for themself
    if (!(req.session.type.toLowerCase() == 'admin' || req.session.user_id == carRev.owner))
      throw new Error(403);
  
    return Promise.all([dbCars.deleteCarRev(carRevId,activity),dbChanges.addNewChangeSystem(deleteStatus,44,req.session.user_id,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,carRev.car,carRevId)])
  })
  .then(([carRev, change]) => {
    return res.json({carRev});//200
  })
  .catch((e)=>{
    switch (parseInt(e.message)) {
      case 400: return res.json({message: 'Bad request'}, 400);
      case 403: return res.json({message: 'Forbidden'}, 403);
      case 404: return res.json({message: 'Not Found'}, 404);
      default: return res.json({error: e.message, message: 'Error while creating car into database and adding new change for new car.'}, 500);
    }
  })
});

///////////////////////////////////////////////////////////////////////////CARS
// get all cars
router.get('/', auth.authenticate, function(req,res, next){
  dbCars.getCars().then(cars => {
    res.json({cars});//200
  })
  .catch((e)=>{
    return res.status(500).json({
      status: 'error',
      message: 'Something went wrong',
      error: e,
    })
  })
});
// get selected car
router.get('/:id', auth.authenticate, function(req,res, next){

  let carId = req.params.id;
  dbCars.getCarById(carId).then(car => {
    res.json({car});//200
  })
  .catch((e)=>{
    return res.status(500).json({
      status: 'error',
      message: 'Something went wrong',
      error: e,
    })
  })
});
// GET selected car notes
router.get('/:id/notes', auth.authenticate, function(req,res, next){

  let carId = req.params.id;
  dbCars.getCarNotesById(carId).then(carNotes => {
    res.json({carNotes});//200
  })
  .catch((e)=>{
    return res.status(500).json({
      status: 'error',
      message: 'Something went wrong',
      error: e,
    })
  })
});
// POST car - add new car
router.post('/', auth.authenticate, function(req,res, next){
  let manufacturer = req.body.manufacturer;
  let model = req.body.model;
  let year = req.body.year;
  let licensePlate = req.body.licensePlate;
  let notes = req.body.notes ? req.body.notes : null;
  let department = req.body.department ? req.body.department : 1;
  let registrationExpiration = req.body.registrationExpiration;
  let vignetteExpiration = req.body.vignetteExpiration;
  let newCar;
  
  if (!(req.session.type.toLowerCase() == 'admin' || req.session.type.toLowerCase().includes('vodja') || req.session.type.toLowerCase() == 'tajnik' || req.session.type.toLowerCase() == 'komercialist' || req.session.type.toLowerCase() == 'komerciala' || req.session.type.toLowerCase() == 'računovodstvo'))
    return res.sendStatus(403);
  if (!manufacturer || !model || !year || !licensePlate)
     return res.sendStatus(400);
  
  dbCars.addCar(manufacturer, model, year, licensePlate, notes, department, registrationExpiration, vignetteExpiration).then(car => {
    newCar = car;
    return dbChanges.addNewChangeSystem(1,43,req.session.user_id,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,car.id)
  })
  .then(change => {
    return res.json({car:newCar});//200
  })
  .catch((e)=>{
    switch (parseInt(e.message)) {
      case 400: return res.json({message: 'Bad request'}, 400);
      case 403: return res.json({message: 'Forbidden'}, 403);
      case 404: return res.json({message: 'Not Found'}, 404);
      default: return res.json({error: e.message, message: 'Error while creating car into database and adding new change for new car.'}, 500);
    }
  })
});
// PUT car - update car
router.put('/:id', auth.authenticate, function(req,res, next){
  let carId = req.params.id;
  let manufacturer = req.body.manufacturer;
  let model = req.body.model;
  let year = req.body.year;
  let licensePlate = req.body.licensePlate;
  let notes = req.body.notes ? req.body.notes : null;
  let department = req.body.department;
  let registrationExpiration = req.body.registrationExpiration;
  let vignetteExpiration = req.body.vignetteExpiration;
  
  if (!(req.session.type.toLowerCase() == 'admin' || req.session.type.toLowerCase().includes('vodja') || req.session.type.toLowerCase() == 'tajnik' || req.session.type.toLowerCase() == 'komercialist' || req.session.type.toLowerCase() == 'komerciala' || req.session.type.toLowerCase() == 'računovodstvo'))
    return res.sendStatus(403);
  if (!manufacturer || !model || !year || !licensePlate || !department)
     return res.sendStatus(400);
  dbCars.getCarById(carId)
  .then(car => {
    if (!car)
      throw new Error(404);
    return Promise.all([dbCars.editCar(carId, manufacturer, model, year, licensePlate, notes, department, registrationExpiration, vignetteExpiration), dbChanges.addNewChangeSystem(2,43,req.session.user_id,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,carId)])
  })
  .then(([updatedCar, change]) => {
    return res.json({car:updatedCar});//200
  })
  .catch((e)=>{
    switch (parseInt(e.message)) {
      case 400: return res.json({message: 'Bad request'}, 400);
      case 403: return res.json({message: 'Forbidden'}, 403);
      case 404: return res.json({message: 'Not Found'}, 404);
      default: return res.json({error: e.message, message: 'Error while creating car into database and adding new change for new car.'}, 500);
    }
  })
});
// DELETE car - update car activity
router.delete('/:id', auth.authenticate, function(req,res, next){
  let carId = req.params.id;
  let activity = req.body.activity ? req.body.activity : false;
  let car;
  
  if (!(req.session.type.toLowerCase() == 'admin' || req.session.type.toLowerCase().includes('vodja') || req.session.type.toLowerCase() == 'tajnik' || req.session.type.toLowerCase() == 'komercialist' || req.session.type.toLowerCase() == 'komerciala' || req.session.type.toLowerCase() == 'računovodstvo'))
    return res.sendStatus(403);

  dbCars.deleteCar(carId,activity)
  .then(updatedDeletedCar => {
    car = updatedDeletedCar;
    if (car){
      if (activity)
        dbChanges.addNewChangeSystem(6,43,req.session.user_id,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,carId);
      else
        dbChanges.addNewChangeSystem(3,43,req.session.user_id,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,carId);
    }
    else throw new Error(404);
  })
  .then(change => {
    return res.json({car});
  })
  .catch((e)=>{
    switch (parseInt(e.message)) {
      case 400: return res.json({message: 'Bad request'}, 400);
      case 403: return res.json({message: 'Forbidden'}, 403);
      case 404: return res.json({message: 'Not Found'}, 404);
      default: return res.json({error: e.message}, 500);
    }
  })
});
module.exports = router;