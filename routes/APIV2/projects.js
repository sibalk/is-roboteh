var express = require('express');
var router = express.Router();
// var bodyParser = require('body-parser')

// express().use(bodyParser.json());

var auth = require('../../controllers/authentication');
var dbProjects = require('../../model/projects/dbProjects');
var dbChanges = require('../../model/changes/dbChanges');

// var apiCspHandler = csp({
//   directives: {
//     defaultSrc: ["'self'"],
//     scriptSrc: ["'self'"],
//     styleSrc: ["'self'"],
//     fontSrc: ["'self'"],
//     imgSrc: ["'self'", "data:"],
//     frameAncestors: ["'self'"],
//   }
// });

// GET - pridobivanje seznama vseh projektov
router.get('/', auth.authenticate, function(req,res, next){
  let active = req.session.type.toLowerCase() == 'admin' ? req.query.active : 1;
  let completed = req.query.completed;
  let start = req.query.start;
  let finish = req.query.finish;
  let loggedUser = req.session.user_id;

  dbProjects.getProjectsFixed(active, loggedUser, start, finish, completed)
  .then(projects=>{
    res.json({projects});
  })
  .catch((e)=>{
    return res.json({error: e.message}, 500);
  })
});

// GET - pridovitev podatkov za izbran projekt
router.get('/:projectId', auth.authenticate, function(req,res, next){
  let projectId = req.params.projectId;

  dbProjects.getProjectById(projectId).then(project=>{
    if (project)
      return res.json({project});
    else throw new Error(404);
  })
  .catch((e)=>{
    switch (parseInt(e.message)) {
      case 400: return res.json({message: 'Bad request'}, 400);
      case 403: return res.json({message: 'Forbidden'}, 403);
      case 404: return res.json({message: 'Not Found'}, 404);
      default: return res.json({error: e.message}, 500);
    }
  })
});

// POST - kreiranje novega projekta
router.post('/', auth.authenticate, function(req,res, next){
  let number = req.body.number;
  let name = req.body.name;
  let subscriber = req.body.subscriber;
  let start = req.body.start;
  let finish = req.body.finish;
  let notes = req.body.notes;

  // check if input parameters are missing
  if ( !number || !name || !subscriber )
    return res.json({message: 'Bad request', info: 'Manjkajoči parametri.'}, 400);
  
  // check roles, admin and leader can create projects
  if (!(req.session.type.toLowerCase() == 'admin' || req.session.type.substring(0,5).toLowerCase() == 'vodja')) {
    return res.json({message: 'Forbidden', info: 'Uporabnik nima pravice ustvariti nov projekt.'}, 403);
  }

  // check if project number is free
  dbProjects.getProjectConflicts(null, number)
  .then( conflicts => {
    // if number for new project is alredy taken
    if (conflicts.length > 0)
      throw new Error(409);
    // if start and finish are out of order if they exist
    else if ( start && finish && new Date(start) > new Date(finish))
      throw new Error(409);
    else return dbProjects.createProject(number, name, subscriber, start, finish, notes)
  })
  .then(project=>{
    //console.log(subscriber);
    // save change to database and return created project
    dbChanges.addNewChangeSystem(1, 1, req.session.user_id, project.id)
    .then(changes => {
      return res.json({project});
    })
    .catch((e) => {
      return res.json({project, msg:'Projekt je ustvarjen. Sprememba ni zapisana v bazo.', error:e});
    })
    // save change
    // if returned just id or reference to the new object/project then return status code 201
    // res.json({success:true, id:project.id}, 201);
  })
  .catch((e)=>{
    switch (parseInt(e.message)) {
      case 400: return res.json({message: 'Bad request'}, 400);
      case 403: return res.json({message: 'Forbidden'}, 403);
      case 404: return res.json({message: 'Not Found'}, 404);
      case 409: return res.json({message: 'Conflict', info: 'Številka projekta je že zasedena ali pa se datuma ne sledita pravilno.'}, 409);
      default: return res.json({error: e.message}, 500);
    }
  })
});

// PUT - posodobitev projekta glede na vhodne podatke
router.put('/:projectId', auth.authenticate, function(req, res, next){
  let projectId = req.params.projectId;
  let number = req.body.number;
  let name = req.body.name;
  let subscriber = req.body.subscriber;
  let start = req.body.start;
  let finish = req.body.finish;
  let notes = req.body.notes;
  let active = req.session.type.toLowerCase() == 'admin' ? req.body.active : true; // only admins can undelete project

  if ( !projectId || !number || !name || !subscriber || !start || !finish || !notes )
    return res.json({message: 'Bad request', info: 'Manjkajoči parametri.'}, 400);

  Promise.all([dbProjects.getWorkersByProjectId(projectId), dbProjects.getProjectById(projectId), dbProjects.getProjectConflicts(projectId, number)])
  .then(([workers, project, numberConflicts]) =>{
    if (!project)
      throw new Error(404);
    // if user is not admin and project is deleted/unactive it should say either its deleted or not authorized
    let tmpLeader = workers.find(w => w.workRole.toLowerCase() == 'vodja'  || w.workRole.toLowerCase() == 'vodja projekta')
    // check roles, admin and leader can update projects
    if (!(req.session.type.toLowerCase() == 'admin' || req.session.type.substring(0,5).toLowerCase() == 'vodja' || tmpLeader)) {
      // also normal user who got leader project/work role -> get work roles for project and check if he is leader and has rights to update project
      throw new Error(403);
    }
    // check if number is free and start&finish are in order -> send JSON with status code 409 - Conflict
    if (numberConflicts.length > 0)
      throw new Error(409);
    // if start and finish are out of order if they exist
    if ( start && finish && new Date(start) > new Date(finish))
      throw new Error(409);
    // everything is ok, do update and save change to database
    else return dbProjects.updateProject(projectId, number, name, subscriber, start, finish, notes, active);
  })
  .then(project=>{
    if (project){
      // return res.json({ project });
      dbChanges.addNewChangeSystem(2, 1, req.session.user_id, projectId)
      .then(change => {
        return res.json({ project });
      })
      .catch((e) => {
        return res.json({ project, msg:'Projekt je posodobljen. Sprememba ni zapisana v bazo.', error:e});
      })
    }
    else throw new Error(404);
  })
  .catch((e)=>{
    switch (parseInt(e.message)) {
      case 400: return res.json({message: 'Bad request', info: 'Manjkajoči parametri.'}, 400);
      case 403: return res.json({message: 'Forbidden', info: 'Uporabnik nima pravice do posodobitve projekta.'}, 403);
      case 404: return res.json({message: 'Not Found', info: 'Projekt ne obstaja.'}, 404);
      case 409: return res.json({message: 'Conflict', info: 'Številka projekta je že zasedena ali pa se datuma ne sledita pravilno.'}, 409);
      default: return res.json({error: e.message}, 500);
    }
  })
})

// DELETE project / set it as unactive
router.delete('/:projectId', auth.authenticate, function(req,res, next){
  let projectId = req.params.projectId;
  // check roles, admin and leader can delete projects
  if (!(req.session.type.toLowerCase() == 'admin' || req.session.type.substring(0,5).toLowerCase() == 'vodja'))
    return res.json({message: 'Forbidden', info: 'Uporabnik nima pravice odstranit projekt.'}, 403);

  // Promise.all([dbProjects.removeProject(projectId),dbChanges.addNewChangeSystem(3, 1, req.session.user_id, projectId)])
  dbProjects.removeProject(projectId)
  .then( project =>{
    if(project){
      // project removed (active set to false) and now record change for project
      dbChanges.addNewChangeSystem(3, 1, req.session.user_id, projectId)
      .then(change => {
        console.log('Uporabnik ' + req.session.user_id + ' je preko API izbrisal projekt  ' + projectId + ' (status:3, tip:1).' );
        return res.json({msg:'Projekt je odstranjen.'});
      })
      .catch((e)=>{
        return res.json({msg:'Projekt je odstranjen. Sprememba ni zapisana v bazo.', error:e});
      })
    }
    else throw new Error(404);
  })
  .catch((e)=>{
    switch (parseInt(e.message)) {
      case 400: return res.json({message: 'Bad request'}, 400);
      case 403: return res.json({message: 'Forbidden'}, 403);
      case 404: return res.json({message: 'Not Found'}, 404);
      default: return res.json({error: e.message}, 500);
    }
  })
})

// GET changes for project (created, updated, removed)
router.get('/:id/changes', auth.authenticate, function(req, res, next){
  let projectId = req.params.id;
  dbProjects.getProjectChanges(projectId).then(changes => {
    return res.json({success:true, changes});
  })
  .catch((e)=>{
    return res.json({error: e.message}, 500);
  })
})

module.exports = router;