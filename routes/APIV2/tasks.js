var express = require('express');
var router = express.Router();
// var bodyParser = require('body-parser')

// express().use(bodyParser.json());

var auth = require('../../controllers/authentication');
var dbProjects = require('../../model/projects/dbProjects');
var dbTasks = require('../../model/tasks/dbTasks');
var dbService = require('../../model/service/dbServices');
var dbEvents = require('../../model/events/dbEvents');
var dbChanges = require('../../model/changes/dbChanges');
var dbUsers = require('../../model/employees/dbEmployees');
var dbAbsences = require('../../model/absences/dbAbsences');

// var apiCspHandler = csp({
//   directives: {
//     defaultSrc: ["'self'"],
//     scriptSrc: ["'self'"],
//     styleSrc: ["'self'"],
//     fontSrc: ["'self'"],
//     imgSrc: ["'self'", "data:"],
//     frameAncestors: ["'self'"],
//   }
// });

// SPECIAL CASE -> GET all TASKS and EVENTS for mobiscroll calendar
// GET - pridobivanje seznama vseh opravil za izbrani projekt ali pa pridobivanje vseh opravil v izbranem časovnem okvirju
// sem imel posamezno pa mobiscroll ni pravilno prikazal opravila in dogodke ali pa je prikazal samo ene, ni rešilo nič kar sem probal, timeout ni rešil, samo pri prvotnem nalaganju je 2x naložil ene podatke zato sem pridobivanje obeh združil tukaj
router.get('/data', auth.authenticate, function(req,res, next){
  let start = req.query.start;
  let end = req.query.end;
  let userId = req.session.user_id;
  let getTasks = req.query.getTasks == 'true' ? true : false;
  let getEvents = req.query.getEvents == 'true' ? true : false;
  let getAbsences = req.query.getAbsences == 'true' ? true : false;
  let active = req.query.active ? req.query.active : '1';

  //check if input data is missing
  if ( start && end ){
    // get project tasks (project view) or get all tasks inside time frame (calendar view)
    let promises = [];
    getTasks ? promises.push(dbTasks.getAllTasksInsideTimeFrame(start, end, null, parseInt(active))): promises.push(dbTasks.getAllTasksInsideTimeFrame('2000-01-01', '2000-01-01', null, parseInt(active)));
    getEvents ? promises.push(dbEvents.getEvents(userId, start, end)) : promises.push(dbEvents.getEvents(userId, '2000-01-01', '2000-01-01'));
    getAbsences ? promises.push(dbAbsences.getAllAbsencesInsideTimeFrame(start, end)) : promises.push(dbAbsences.getAllAbsencesInsideTimeFrame('2000-01-01', '2000-01-01'));
    Promise.all(promises)
    .then(([tasks, events, absences])=>{
      
      return res.json({tasks, events, absences});
    })
    .catch((e)=>{
      switch (parseInt(e.message)) {
        case 400: return res.json({message: 'Bad request'}, 400);
        case 403: return res.json({message: 'Forbidden'}, 403);
        case 404: return res.json({message: 'Not Found'}, 404);
        default: return res.json({error: e.message}, 500);
      }
    })
  }
  else
    return res.json({message: 'Bad request', info: 'Manjkajoči parametri.'}, 400);
  
});

// GET - pridovitev podatkov za izbran projekt
router.get('/conflicts', auth.authenticate, function(req,res, next){
  let workers = req.query.workers;
  let projectId = req.query.projectId;
  let start = req.query.start;
  let finish = req.query.finish;

  if ( !workers || !projectId || !start || !finish){
    return res.json({message: 'Bad request', info: 'Manjkajoči parametri.'}, 400);
  }
  let workersArray = [];
  if(workers)
    workersArray = workers.split(",");
  let conflictPromises = [];

  for (const worker of workersArray) {
    conflictPromises.push(dbTasks.getWorkerConflicts(worker, projectId, start, finish))
  }
  Promise.all(conflictPromises)
  .then(conflicts => {
    let conflictsExists = false;
    for (const userConflicts of conflicts) {
      if (userConflicts.length > 0){
        conflictsExists = true;
        break;
      }
    }
    if (conflictsExists)
      return res.json({conflicts});
    else
      return res.json({conflicts: []});
  })
  .catch(e => {
    return res.json({error: e.message}, 500);
  })
});

// GET - pridobivanje seznama vseh opravil za izbrani projekt ali pa pridobivanje vseh opravil v iybranem časovnem okvirju
router.get('/', auth.authenticate, function(req,res, next){
  let projectId = req.query.projectId;
  let start = req.query.start;
  let end = req.query.end;
  let category = req.query.category;
  let active = req.query.active;
  let completion = req.query.completion;
  let showMyInputTasks = req.query.showMyInputTasks;
  let showMyTasks = req.query.showMyTasks;

  //check if input data is missing
  if ( projectId || (start && end ) ){
    // get project tasks (project view) or get all tasks inside time frame (calendar view)
    let getTasksPromise = (projectId) ? dbTasks.getTasksByProjectIdFixed(projectId, parseInt(category), parseInt(active)) : dbTasks.getAllTasksInsideTimeFrame(start, end, parseInt(category), parseInt(active));
    getTasksPromise
    .then(tasks=>{
      //tasks = multipleWorkers(tasks);
      if (completion){
        if (completion == 1) tasks = tasks.filter(t => t.completion != 100);
        else if (completion == 2)tasks = tasks.filter(t => t.completion == 100);
      }
      if (showMyInputTasks == 'true') tasks = tasks.filter(t => t.author_id == req.session.user_id)
      if (showMyTasks == 'true') tasks = tasks.filter(t => (t.workers_id ? t.workers_id : '').split(',').find(f => f == req.session.user_id) == req.session.user_id)
      // addFormatedDateForTasks(tasks);
      return res.json({tasks});
    })
    .catch((e)=>{
      switch (parseInt(e.message)) {
        case 400: return res.json({message: 'Bad request'}, 400);
        case 403: return res.json({message: 'Forbidden'}, 403);
        case 404: return res.json({message: 'Not Found'}, 404);
        default: return res.json({error: e.message}, 500);
      }
    })
  }
  else
    return res.json({message: 'Bad request', info: 'Manjkajoči parametri.'}, 400);
  
});

// GET - pridovitev podatkov za izbran projekt
router.get('/:id', auth.authenticate, function(req,res, next){
  dbTasks.getTaskById(req.params.id)
  .then(task=>{
    if (task) return res.json({task});
    else throw new Error(404);
  })
  .catch((e)=>{
    switch (parseInt(e.message)) {
      case 400: return res.json({message: 'Bad request'}, 400);
      case 403: return res.json({message: 'Forbidden'}, 403);
      case 404: return res.json({message: 'Not Found'}, 404);
      default: return res.json({error: e.message}, 500);
    }
  })
});

// POST - kreiranje novega opravila
router.post('/', auth.authenticate, function(req,res, next){
  let userId = req.session.user_id;
  let projectId = req.body.projectId;
  let subscriberId = req.body.subscriberId;
  let name = req.body.name;
  let notes = req.body.notes;
  let duration = req.body.duration;
  let start = req.body.start ? req.body.start + ' 07:00:00.000000' : req.body.start;
  let finish = req.body.finish ? req.body.finish + ' 15:00:00.000000' : req.body.finish;
  let completion = req.body.taskCompletion ? req.body.taskCompletion : 0; // not needed as leaders are adding new tasks which should have 0 completion
  let workers = req.body.workers;
  let category = req.body.category ? req.body.category : 1; // if no category then 1 since its marked as no category in database
  let priority = req.body.priority ? req.body.priority : 1; // if no priority then 1 since its marked as no priority in database
  let weekend = req.body.taskWeekend && (req.body.taskWeekend == 'true' || req.body.taskWeekend == 'false') ? req.body.taskWeekend : false;
  // let override = req.body.override;
  // let buildParts = JSON.parse(req.body.buildParts);
  // let parentName = req.body.parentName;
  // let parentStandard = req.body.parentStandard;
  let promises = [];
  let createdTask;

  // check if input parameters are missing
  if ( (!projectId && !subscriberId) || !name  )
    return res.json({message: 'Bad request', info: 'Manjkajoči parametri.'}, 400);
  
  // check roles, admin and leader can create projects
  if (!(req.session.type.toLowerCase() == 'admin' || req.session.type.substring(0,5).toLowerCase() == 'vodja' || req.session.type.substring(0,9).toLowerCase() == 'konstrukt')) {
    return res.json({message: 'Forbidden', info: 'Uporabnik nima pravice ustvariti nov projekt.'}, 403);
  }
  // in case of projectId user is adding new project task
  if (projectId){
    taskPromise = dbTasks.addTask(projectId, name, duration, start, finish, category, priority, completion, weekend, notes);
  }
  else{ // subscriberId and no projectId -> user is adding new service
    taskPromise = dbService.addServisNew(subscriberId, name, duration, start, finish, category, priority, completion, weekend);
  }
  taskPromise
  .then(task => {
    createdTask = task;
    let workersArray = [];
    if (workers){
      workersArray = workers.split(',');
    }
    for (const worker of workersArray) {
      promises.push(dbTasks.assignTask(task.id, worker));
    }
    return Promise.all(promises);
  })
  .then(workersResults => { // add new change to changes_system
    return dbChanges.addNewChangeSystem(1, 2, userId, projectId, createdTask.id, null, null, null, null, subscriberId);
  })// get all active task on project to calculate new completion and update project completion
  .then(change => {
    return dbTasks.getTasksByProjectIdFixed(projectId, 0, 1);
  }) // calculate new project completion and save it to project
  .then(allActiveTasks => {
    let completion = 0;
    for (const task of allActiveTasks) {
      completion += task.completion;
    }
    completion = Math.round(completion/allActiveTasks.length);
    if (allActiveTasks.length == 0)
      completion = 0;
    // completion should be calculated, update project with new completion
    return dbProjects.updateCompletion(projectId, completion);
  })
  .then(project => {
    return dbTasks.getTaskData(createdTask.id);
  })
  .then(task => {
    return res.json({task});
  })
  .catch((e)=>{
    switch (parseInt(e.message)) {
      case 400: return res.json({message: 'Bad request'}, 400);
      case 403: return res.json({message: 'Forbidden'}, 403);
      case 404: return res.json({message: 'Not Found'}, 404);
      default: return res.json({error: e.message}, 500);
    }
  })
});

// PUT - posodobitev opravila glede na vhodne podatke
router.put('/:id', auth.authenticate, function(req, res, next){
  let userId = req.session.user_id;
  let projectId = req.body.projectId;
  let taskId = req.params.id;
  let name = req.body.name;
  let notes = req.body.notes;
  let duration = req.body.duration;
  let start = req.body.start ? req.body.start + ' 07:00' : req.body.start;
  let finish = req.body.finish ? req.body.finish + ' 15:00': req.body.finish;
  let workers = req.body.workers;
  let completion = req.body.taskCompletion;
  let active = req.body.active;
  let category = req.body.category;
  let priority = req.body.priority;
  let weekend = req.body.weekend;
  let noteChanges = '';

  let promisesWorkers = [];
  let task;
  // let oldTask;

  // check if input parameters are missing
  if ( !taskId || (!projectId && !subscriberId) || !name  )
    return res.json({message: 'Bad request', info: 'Manjkajoči parametri.'}, 400);

  // check roles, admin and leader can create projects
  if (!(req.session.type.toLowerCase() == 'admin' || req.session.type.substring(0,5).toLowerCase() == 'vodja' || req.session.type.substring(0,9).toLowerCase() == 'konstrukt')) {
    return res.json({message: 'Forbidden', info: 'Uporabnik nima pravice ustvariti novo opravilo.'}, 403);
  }

  dbTasks.getTaskData(taskId)
  .then(oldTask => {
    // check what has changed and save it to notes for better history tracking and blame
    if ( new Date(start).toISOString() != new Date(oldTask.task_start).toISOString()){
      let tmpDate = new Date(oldTask.task_start);
      noteChanges +=', začetek: ' + tmpDate.getDate() + '.' + (tmpDate.getMonth()+1) + '.' + tmpDate.getFullYear();
    }
    if ( new Date(finish).toISOString() != new Date(oldTask.task_finish).toISOString()){
      let tmpDate = new Date(oldTask.task_finish);
      noteChanges +=', konec: ' + tmpDate.getDate() + '.' + (tmpDate.getMonth()+1) + '.' + tmpDate.getFullYear();
    }
    if (name != oldTask.task_name)
      noteChanges += ', ime opravila: ' + oldTask.task_name;
    // problem checking workers because string can be in different order, can be done in many ways (intersection, order and compare or difference in both ways)
    if (workers.split(',').sort().toString() != oldTask.workers_id.split(',').sort().toString())
      noteChanges += ', zadolženi: ' + oldTask.workers_id;
    if (noteChanges.length > 0)
      noteChanges = 'Spremembe:' + noteChanges.substring(1, noteChanges.length);
    // oldTask = result;
    let oldWorkers = oldTask.workers_id;
    if (oldWorkers != workers){
      // console.log('pobrisi delavce in dodaj nove ker niso isti')
      promisesWorkers.push(dbTasks.removeAssignTask(taskId));
      if (workers){
        for (const worker of workers.split(',')) {
          promisesWorkers.push(dbTasks.assignTask(taskId, worker));
        }
      }
    }
    return Promise.all(promisesWorkers);
  })
  .then(assignmentResult => {
    // workers are corrected, update task
    return dbTasks.updateTask(taskId, name, duration, start, finish, completion, active, category, priority, weekend, notes);
  })
  .then(result => {
    // task is updated, save change to changes_system
    // chance to check what is different and to save difference into notes for better history tracking and blame
    task = result;
    return dbChanges.addNewChangeSystem(2, 2, userId, projectId, task.id, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, noteChanges);
  })
  .then(change => {
    return dbTasks.getTaskData(task.id);
    // TODO get notify users and save to changes_notify
  })
  .then(task => {
    return res.json({task});
  })
  .catch((e)=>{
    switch (parseInt(e.message)) {
      case 400: return res.json({message: 'Bad request'}, 400);
      case 403: return res.json({message: 'Forbidden'}, 403);
      case 404: return res.json({message: 'Not Found'}, 404);
      default: return res.json({error: e.message}, 500);
    }
  })
})

// PATCH task completion and calculate new percentage of project and save new change for changing completion
router.patch('/:id', auth.authenticate, function(req,res, next){
  let taskId = req.params.id;
  let completion = req.body.completion;
  let userId = req.session.user_id;
  let task;

  if (completion == null || (typeof completion == 'string' && !(completion == 'true' || completion == 'false')) || !(typeof completion == 'string' || typeof completion == 'boolean'))
    return res.sendStatus(400);

  // get event to be able to check permissions
  dbTasks.getTaskData(taskId)
  .then(oldTask => {
    if (!oldTask)
      throw new Error(404);
    task = oldTask;
    return Promise.all([dbProjects.getWorkersByProjectId(task.project_id), dbUsers.getUsers()]);
  })
  .then(([projectWorkers, users]) => {
    let = elektroUserCheck = false, strojnaUserCheck = false;
    if (req.session.type.toLowerCase().substring(0,4) == 'info'){
      for (const worker of task.workers_id.split(',')) {
        if(users.find(u => u.id == worker && u.role.toLowerCase() == 'električar')) elektroUserCheck = true;
        if(users.find(u => u.id == worker && (u.role.toLowerCase() == 'strojnik' || u.role.toLowerCase() == 'varilec' || u.role.toLowerCase() == 'cnc operater') )) strojnaUserCheck = true;
      }
    }
    // check if user has permission to patch completion --> admins, project leaders, role leaders for role, assigned users, info users for department, author of task
    if (!(req.session.type.toLowerCase() == 'admin' || 
    projectWorkers.find(w => w.id == req.session.user_id && (w.workRole.toLowerCase() == 'vodja projekta' || w.workRole.toLowerCase() == 'vodja')) ||
    task.workers_id.split(',').find(w => w == req.session.user_id) ||
    task.author_id == req.session.user_id || 
    req.session.type.toLocaleLowerCase() == 'vodja strojnikov' && task.category.toLocaleLowerCase() == 'strojna izdelava' ||
    req.session.type.toLocaleLowerCase() == 'info strojna' && task.category.toLocaleLowerCase() == 'strojna izdelava' ||
    req.session.type.toLocaleLowerCase() == 'vodja električarjev' && task.category.toLocaleLowerCase() == 'elektro izdelava' ||
    req.session.type.toLocaleLowerCase() == 'info elektro' && elektroUserCheck)) {
      throw new Error(403);
    }
    if (typeof completion == 'boolean' && completion || typeof completion == 'string' && completion == 'true')
      return dbTasks.updateTaskCompletion(taskId, 100);
    else 
      return dbTasks.updateTaskCompletion(taskId, 0);
  })
  .then(updatedTask => {
    task.completion = updatedTask.completion;
    if (updatedTask){
      if (updatedTask.completion == 100)
        return Promise.all([dbChanges.addNewChangeSystem(4,2,userId,task.project_id,taskId), dbTasks.finishTask(taskId)]);
      else
        return Promise.all([dbChanges.addNewChangeSystem(5,2,userId,task.project_id,taskId)]);
    }
    else throw new Error(404);
  })
  .then(change => {
    // get all active task for calculating new percentage and get project info for checking
    let tasksWithProjectInfo = [dbTasks.getTasksByProjectIdFixed(task.project_id, 0, 1), dbProjects.getProjectById(task.project_id)];
    return Promise.all(tasksWithProjectInfo);
  })
  .then(([allActiveTasks, project]) => {
    // calculate new project percentage and save it to database and mark change if currentPercentage was 100 and now is not or if currentPercentage was < 100 and now is
    let completion = 0;
    for (const task of allActiveTasks) { completion += task.completion; }
    allActiveTasks.length == 0 ? completion = 0: completion = Math.round(completion/allActiveTasks.length);
    let completionPromises = [dbProjects.updateCompletion(project.id, completion)];
    if (project.completion != 100 && completion == 100) completionPromises.push(dbChanges.addNewChangeSystem(4, 1, userId, project.id));
    if (project.completion == 100 && completion != 100) completionPromises.push(dbChanges.addNewChangeSystem(5, 1, userId, project.id));
    return Promise.all(completionPromises);
  })
  .then(([projectUpdate, projectChange]) => {
    return res.json({task});//200
  })
  .catch((e)=>{
    switch (parseInt(e.message)) {
      case 400: return res.json({message: 'Bad request'}, 400);
      case 403: return res.json({message: 'Forbidden', info: 'Uporabnik nima pravice posodobiti končanost dogodka.'}, 403);
      case 404: return res.json({message: 'Not Found', info: 'Dogodka s tem ID-jem ni v podatkovni bazi.'}, 404);
      default: return res.json({error: e.message}, 500);
    }
  })
});

// DELETE - remove task, set task as non active task
router.delete('/:id', auth.authenticate, function(req, res, next){
  let userId = req.session.user_id;
  let taskId = req.params.id;
  let active = req.body.active == true ? req.body.active : false; // in case admin adds it back or leaders do undo action after deletion
  let tmpTask;
  let projectId;
  let subscriberId;
  // TODO add role control
  dbTasks.removeTask(taskId, active).then(task=>{
    tmpTask = task;
    projectId = tmpTask.id_project ? tmpTask.id_project : null;
    subscriberId = tmpTask.id_subscriber ? tmpTask.id_subscriber : null;
    if (task){
      if (active)
        return dbChanges.addNewChangeSystem(6, 2, userId, projectId, task.id, null, null, null, null, subscriberId);
      else
        return dbChanges.addNewChangeSystem(3, 2, userId, projectId, task.id, null, null, null, null, subscriberId);
    }
    else throw new Error(404);
  })
  .then(change => {// get all active task on project to calculate new completion and update project completion
    if (projectId)
      return dbTasks.getTasksByProjectIdFixed(projectId, 0, 1);
    else
      return res.json({success: true, tmpTask});  
  })
  .then(allActiveTasks => {
    let completion = 0;
    for (const task of allActiveTasks) {
      completion += task.completion;
    }
    completion = Math.round(completion/allActiveTasks.length);
    if (allActiveTasks.length == 0)
    completion = 0;
    // completion should be calculated, update project with new completion
    return dbProjects.updateCompletion(projectId, completion);
  })
  .then(project => {
    return res.json({success: true, tmpTask});
  })
  .catch((e)=>{
    switch (parseInt(e.message)) {
      case 400: return res.json({message: 'Bad request'}, 400);
      case 403: return res.json({message: 'Forbidden'}, 403);
      case 404: return res.json({message: 'Not Found'}, 404);
      default: return res.json({error: e.message}, 500);
    }
  })
})
//get all changes for task or subtask
router.get('/:id/changes', auth.authenticate, function(req, res, next){
  let taskId = req.params.id;
  dbService.getTaskChangesSystem(taskId).then(taskChanges => {
    return res.json({success:true, taskChanges});
  })
  .catch((e)=>{
    return res.json({success:false, error:e});
  })
})
module.exports = router;