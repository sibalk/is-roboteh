var express = require('express');
var router = express.Router();

var auth = require('../../controllers/authentication');
var dbBuildparts = require('../../model/buildparts/dbBuildParts');
var dbTasks = require('../../model/tasks/dbTasks');
var dbProjects = require('../../model/projects/dbProjects');
var dbChanges = require('../../model/changes/dbChanges');

// GET ALL SUBTASKS FOR SPECIFIC TASK
router.get('/', auth.authenticate, function(req,res, next){
  let taskId = req.query.taskId;
  if (taskId == null)
    throw new Error(400);
  dbBuildparts.getTaskBuildPartsFixed(taskId).then(buildparts => {
    res.json({buildparts});//200
  })
  .catch((e)=>{
    switch (parseInt(e.message)) {
      case 400: return res.json({message: 'Bad request'}, 400);
      case 403: return res.json({message: 'Forbidden'}, 403);
      case 404: return res.json({message: 'Not Found'}, 404);
      default: return res.json({error: e.message}, 500);
    }
  })
});

// PATCH buildpart completion
router.patch('/:id', auth.authenticate, function(req,res, next){
  let buildpartId = req.params.id;
  let completion = req.body.completion;
  let buildpart, task, project, allActiveTasks, allTaskBuildparts;
  if (completion == null || (typeof completion == 'string' && !(completion == 'true' || completion == 'false')) || !(typeof completion == 'string' || typeof completion == 'boolean'))
    throw new Error(400);
  
  // get buildpart data for taskId
  dbBuildparts.getBuildPart(buildpartId).then(buildpartData => {
    buildpart = buildpartData;
    // get task data for projectId and all build parts (maybe even only active ones)
    return Promise.all([dbTasks.getTaskData(buildpart.task_id), dbBuildparts.getTaskBuildPartsFixed(buildpart.task_id)]);
  })
  .then(([taskData, allTaskBuildpartsData]) => {
    task = taskData;
    allTaskBuildparts = allTaskBuildpartsData;
    // get project, all active tasks and list of working on project for permission control
    return Promise.all([dbProjects.getProjectById(task.project_id), dbProjects.getWorkersByProjectId(task.project_id), dbTasks.getTasksByProjectIdFixed(task.project_id, 0, 1)]);
  })
  .then(([projectData, projectWorkers, allActiveTasksData]) => {
    project = projectData;
    allActiveTasks = allActiveTasksData;
    // check if user has rights to update completion --> is leader on project, is leader of correct department (strojna, cnc), admin, assigned user and roles that work mostly on all parts (varilci, cnc operaterji, strojnik)
    if (!(req.session.type.toLowerCase() == 'admin' || 
    projectWorkers.find(w => w.id == req.session.user_id && (w.workRole.toLowerCase() == 'vodja projekta' || w.workRole.toLowerCase() == 'vodja')) ||
    task.workers_id.split(',').find(w => w == req.session.user_id) ||
    task.author_id == req.session.user_id || 
    req.session.type.toLocaleLowerCase() == 'vodja strojnikov' && task.category.toLocaleLowerCase() == 'strojna izdelava' ||
    req.session.type.toLocaleLowerCase() == 'vodja cnc operaterjev' && task.category.toLocaleLowerCase() == 'strojna izdelava' ||
    req.session.type.toLocaleLowerCase() == 'info strojna' && task.category.toLocaleLowerCase() == 'strojna izdelava' ||
    req.session.type.toLocaleLowerCase() == 'vodja električarjev' && task.category.toLocaleLowerCase() == 'elektro izdelava' ||
    req.session.type.toLocaleLowerCase() == 'info elektro' && task.category.toLocaleLowerCase() == 'elektro izdelava' ||
    req.session.type.toLocaleLowerCase() == 'strojnik' && task.category.toLocaleLowerCase() == 'strojna izdelava' ||
    req.session.type.toLocaleLowerCase() == 'cnc operater' && task.category.toLocaleLowerCase() == 'strojna izdelava' ||
    req.session.type.toLocaleLowerCase() == 'varilci' && task.category.toLocaleLowerCase() == 'strojna izdelava')) {
      throw new Error(403);
    }
    let updatePromises = [];
    // update completion and save change of buildpart completion
    buildpart.completion = (typeof completion == 'boolean' && completion || typeof completion == 'string' && completion == 'true') ? 100 : 0;
    (typeof completion == 'boolean' && completion || typeof completion == 'string' && completion == 'true') ? updatePromises.push(dbBuildparts.updateBuildPartCompletion(buildpartId, 100), dbChanges.addNewChangeSystem(4,20,req.session.user_id,null,buildpart.task_id,null,null,null,null,null,null,null,null,buildpartId)) : updatePromises.push(dbBuildparts.updateBuildPartCompletion(buildpartId, 0), dbChanges.addNewChangeSystem(5,20,req.session.user_id,null,buildpart.task_id,null,null,null,null,null,null,null,null,buildpartId));
    // update task completion based on all active build parts completion
    allTaskBuildparts.find(bp => bp.id == buildpartId).completion = buildpart.completion;
    let activeBuildPartsLength = 0, newTaskCompletion = 0;
    for (const buildpart of allTaskBuildparts) {
      if(buildpart.active){
        newTaskCompletion += buildpart.completion;
        activeBuildPartsLength++;
      }
    }
    newTaskCompletion = Math.round(newTaskCompletion/activeBuildPartsLength);
    updatePromises.push(dbTasks.updateTaskCompletion(buildpart.task_id, newTaskCompletion));
    if (task.completion != 100 && newTaskCompletion == 100) updatePromises.push(dbChanges.addNewChangeSystem(4, 2, req.session.user_id, project.id, task.id));
    if (task.completion == 100 && newTaskCompletion != 100) updatePromises.push(dbChanges.addNewChangeSystem(5, 2, req.session.user_id, project.id, task.id));
    // update project completion now that task completion has changed
    let newProjectCompletion = 0;
    allActiveTasks.find(t => t.id == task.id).completion = newTaskCompletion;
    for (const task of allActiveTasks) { newProjectCompletion += task.completion; }
    allActiveTasks.length == 0 ? newProjectCompletion = 0: newProjectCompletion = Math.round(newProjectCompletion/allActiveTasks.length);
    if (project.completion != newProjectCompletion) updatePromises.push(dbProjects.updateCompletion(project.id, newProjectCompletion));
    if (project.completion != 100 && newProjectCompletion == 100) updatePromises.push(dbChanges.addNewChangeSystem(4, 1, req.session.user_id, project.id));
    if (project.completion == 100 && newProjectCompletion != 100) updatePromises.push(dbChanges.addNewChangeSystem(5, 1, req.session.user_id, project.id));

    return Promise.all(updatePromises);
  })
  .then(changes => {
    // return patched buildpart info
    return res.json({buildpart});//200
  })
  .catch((e)=>{
    switch (parseInt(e.message)) {
      case 400: return res.json({message: 'Bad request'}, 400);
      case 403: return res.json({message: 'Forbidden'}, 403);
      case 404: return res.json({message: 'Not Found'}, 404);
      default: return res.json({error: e.message}, 500);
    }
  })
});

module.exports = router;