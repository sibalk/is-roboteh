var express = require('express');
var router = express.Router();
var dateFormat = require('dateformat');
var multer = require('multer');
var fs = require('fs');
const fsp = require('fs').promises;
var Jimp = require('jimp');

//auth & dbModels
var auth = require('../controllers/authentication');
var dbTasks = require('../model/tasks/dbTasks');
var dbService = require('../model/service/dbServices');
var dbFiles = require('../model/files/dbFiles');
let dbChanges = require('../model/changes/dbChanges');

//functions
function formatDate(date){
  return {
    "date": dateFormat(date, "d. m. yyyy"),
    "time": dateFormat(date, "HH:MM")
  }
}
function addFormatedDateForTasks(tasks){
  for(let i=0; i < tasks.length; i++) {
    if(tasks[i] && tasks[i].task_start)
      tasks[i]["formatted_start"] = formatDate(tasks[i].task_start);
    if(tasks[i] && tasks[i].task_finish)
      tasks[i]["formatted_finish"] = formatDate(tasks[i].task_finish);
  }
  return tasks;
}
//storage for documents
var storageDoc = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, 'files/projects/')
  },
  filename: function (req, file, cb) {        
    // null as first argument means no error
    cb(null, Date.now() + '-' + file.originalname )
  }
});
//upload when adding subscriber image
var uploadDoc = multer({
  storage: storageDoc,
  limits: {
    fileSize: 5300000
  },
  fileFilter: function(req, file, cb){
    sanitizeFileDocument(file,cb);
  }
}).single('docNew');
//function to check if file is indeed document
function sanitizeFileDocument(file, cb){
  //what file extentions are ok
  let fileExts = ['pdf', //pdf 
                'doc', 'dot', 'wbk', 'docx', 'docm', 'dotx', 'dotm', 'docb', //word
                'xls', 'xlt', 'xlm', 'xlsx', 'xlsm', 'xltx', 'xltm', 'xlsb', 'xla', 'xlam', 'xll', 'xlw', //excel
                'ppt', 'pot', 'pps', 'pptx', 'pptm', 'potx', 'potm', 'ppam', 'ppsx', 'ppsm', 'sldx', 'sldm', //powepoint
                'zip', 'rar', //zips
                'txt', //txt
                'jpg', 'jpeg', 'png', 'gif']; //images
                //access has way diffrent ext and they probably wont use them
                //other file ext will be added when there will be request
  // MAYBE TODO add isAlowedMimeType back and test for all this type of extentions
  //check if file has no exts
  let fileExtsArray = file.originalname.split(".");
  if(fileExtsArray.length == 1)
    return cb('Datoteka brez končnice ni dovoljena');
  //check alowed exts
  let isAlowedExt = fileExts.includes(fileExtsArray[fileExtsArray.length-1].toLowerCase());
  //mime type must be an image
  //let isAlowedMimeType = file.mimetype.startsWith("document/");

  if(isAlowedExt){
    //no errors
    return cb(null, true);
  }
  else{
    //error, not an image
    cb('Ta vrsta datoteke ni dovoljena!');
  }
}
//upload for new file
router.post('/fileUpload', auth.authenticate, (req, res, next) => {
  //debugger;
  //save file and if no error send back json success true
  uploadDoc(req, res, (err) =>{
    if(err){
      console.log("Napaka pri nalaganju datoteke pri uporabniku " + req.session.user_id);
      let msg = 'Napaka pri nalaganju datoteke na strežnik.';
      if(err.message == 'File too large')
        msg = 'Velikost datoteke je prevelika. Trenutno so dovoljene datoteke do 5MB.';
      return res.status(500).json({
        status: 'error',
        message: msg,
        type: 1,
        error: err,
      })
    }
    else{
      //file not selected
      if(req.file == undefined){
        console.log("Uporabnik " + req.session.user_id + " ni podal datoteke pri nalaganju datoteke.");
        return res.status(400).json({
          status: 'error',
          message: 'Niste priložili datoteko, ki jo želite dodati.',
        })
      }
      else{
        let type = "";
        let fileExtsPDF = ['pdf'];
        let fileExtsDOC = ['doc', 'dot', 'wbk', 'docx', 'docm', 'dotx', 'dotm', 'docb'];
        let fileExtsXLS = ['xls', 'xlt', 'xlm', 'xlsx', 'xlsm', 'xltx', 'xltm', 'xlsb', 'xla', 'xlam', 'xll', 'xlw'];
        let fileExtsPPT = ['ppt', 'pot', 'pps', 'pptx', 'pptm', 'potx', 'potm', 'ppam', 'ppsx', 'ppsm', 'sldx', 'sldm'];
        let fileExtsIMG = ['jpg', 'jpeg', 'png', 'gif'];
        let fileExtsZIP = ['zip', 'rar'];
        let fileExtsTXT = ['txt'];

        let fileExtsArray = req.file.originalname.split(".");
        let isAlowedExtPDF = fileExtsPDF.includes(fileExtsArray[fileExtsArray.length - 1].toLocaleLowerCase());
        let isAlowedExtDOC = fileExtsDOC.includes(fileExtsArray[fileExtsArray.length - 1].toLowerCase());
        let isAlowedExtXLS = fileExtsXLS.includes(fileExtsArray[fileExtsArray.length - 1].toLowerCase());
        let isAlowedExtPPT = fileExtsPPT.includes(fileExtsArray[fileExtsArray.length - 1].toLowerCase());
        let isAlowedExtZIP = fileExtsZIP.includes(fileExtsArray[fileExtsArray.length - 1].toLowerCase());
        let isAlowedExtTXT = fileExtsTXT.includes(fileExtsArray[fileExtsArray.length - 1].toLowerCase());
        let isAlowedExtIMG = fileExtsIMG.includes(fileExtsArray[fileExtsArray.length - 1].toLocaleLowerCase());
        
        if(isAlowedExtPDF) type = 'PDF';
        else if(isAlowedExtDOC) type = 'DOC';
        else if(isAlowedExtPPT) type = 'PPT';
        else if(isAlowedExtXLS) type = 'XLS';
        else if(isAlowedExtIMG) type = 'IMG';
        else if(isAlowedExtZIP) type = 'ZIP';
        else if(isAlowedExtTXT) type = 'TXT';
        let projectId = parseInt(req.body.projectIdFile);
        console.log("Uporabnik " + req.session.user_id + " je uspešno naložil datoteko " +req.file.originalname + " na projektu " + projectId);
        //success add new subscriber with image name
        dbFiles.addFileForProject(req.file.originalname, req.file.filename, projectId, req.session.user_id, type)
        .then(newFile =>{
          let fileLocation = `${__dirname}/../files/projects/${newFile.path_name}`;
          if (fs.existsSync(fileLocation) && newFile.type == 'IMG') {
            newFile.base64 = fs.readFileSync(fileLocation, 'base64');
          }
          res.status(200).json({
            status: 'success',
            file:newFile,
            success: 'true'
          });
        })
        .catch((e)=>{
          return res.status(500).json({
            status: 'error',
            message: 'Napaka pri zapisovanju podatkov datoteke v tabelo z datotekami.',
            error: e,
          })
        })
      }
    }
  })
});

//upload for new file based on taskId and projectId
router.post('/taskFileUpload', auth.authenticate, (req, res, next) => {
  //save file and if no error send back json success true
  uploadDoc(req, res, (err) =>{
    if(err){
      console.log("Napaka pri nalaganju datoteke pri uporabniku " + req.session.user_id);
      let msg = 'Napaka pri nalaganju datoteke na strežnik.';
      if(err.message == 'File too large')
        msg = 'Velikost datoteke je prevelika. Trenutno so dovoljene datoteke do 5MB.';
      return res.status(500).json({
        status: 'error',
        message: msg,
        type: 1,
        error: err,
      })
    }
    else{
      //file not selected
      if(req.file == undefined){
        console.log("Uporabnik " + req.session.user_id + " ni podal datoteke pri nalaganju datoteke.");
        return res.status(400).json({
          status: 'error',
          message: 'Niste priložili datoteko, ki jo želite dodati servisu.',
          type: 1,
        })
      }
      else{
        let type = "";
        let fileExtsPDF = ['pdf'];
        let fileExtsDOC = ['doc', 'dot', 'wbk', 'docx', 'docm', 'dotx', 'dotm', 'docb'];
        let fileExtsXLS = ['xls', 'xlt', 'xlm', 'xlsx', 'xlsm', 'xltx', 'xltm', 'xlsb', 'xla', 'xlam', 'xll', 'xlw'];
        let fileExtsPPT = ['ppt', 'pot', 'pps', 'pptx', 'pptm', 'potx', 'potm', 'ppam', 'ppsx', 'ppsm', 'sldx', 'sldm'];
        let fileExtsIMG = ['jpg', 'jpeg', 'png', 'gif'];
        let fileExtsZIP = ['zip', 'rar'];
        let fileExtsTXT = ['txt'];

        let fileExtsArray = req.file.originalname.split(".");
        let isAlowedExtPDF = fileExtsPDF.includes(fileExtsArray[fileExtsArray.length - 1].toLocaleLowerCase());
        let isAlowedExtDOC = fileExtsDOC.includes(fileExtsArray[fileExtsArray.length - 1].toLowerCase());
        let isAlowedExtXLS = fileExtsXLS.includes(fileExtsArray[fileExtsArray.length - 1].toLowerCase());
        let isAlowedExtPPT = fileExtsPPT.includes(fileExtsArray[fileExtsArray.length - 1].toLowerCase());
        let isAlowedExtZIP = fileExtsZIP.includes(fileExtsArray[fileExtsArray.length - 1].toLowerCase());
        let isAlowedExtTXT = fileExtsTXT.includes(fileExtsArray[fileExtsArray.length - 1].toLowerCase());
        let isAlowedExtIMG = fileExtsIMG.includes(fileExtsArray[fileExtsArray.length - 1].toLocaleLowerCase());
        
        if(isAlowedExtPDF) type = 'PDF';
        else if(isAlowedExtDOC) type = 'DOC';
        else if(isAlowedExtPPT) type = 'PPT';
        else if(isAlowedExtXLS) type = 'XLS';
        else if(isAlowedExtIMG) type = 'IMG';
        else if(isAlowedExtZIP) type = 'ZIP';
        else if(isAlowedExtTXT) type = 'TXT';
        let taskId = parseInt(req.body.taskId);
        console.log("Uporabnik " + req.session.user_id + " je uspešno naložil datoteko " +req.file.originalname + " za servis " + taskId);
        //success add new subscriber with image name
        dbFiles.addFileForService(req.file.originalname, req.file.filename, req.session.user_id, type, taskId)
        .then(newFile =>{
          if(newFile){
            let userId = req.session.user_id;
            let promises = [];
            let tmpFile = newFile;
            dbChanges.getServisNotifyUsers(taskId,userId).then(users => {
              let notify = [];
              let leaders = [];
              if(users.task_workers)
                notify = users.task_workers.split(",");
              if(users.project_workers)
                leaders = users.project_workers.split(",");
              for(let i = 0; i < leaders.length; i++){
                var x = notify.find(u => parseInt(u) == parseInt(leaders[i]));
                if(!x)
                  notify.push(leaders[i]);
              }
              dbChanges.addNewChangeSystem(1,13,userId,null,taskId,null,null,null,null,null,null,tmpFile.id).then(sysChange =>{
                console.log("Uporabnik " + userId + " je naredil spremembo tipa 1 s statusom 13 pri servisu " + taskId + ".");
                for(let i = 0; i < notify.length; i++){
                  //console.log("Uporabnik " + userId + " je naredil spremembe za " + notify[i] + " tipa " + type + " s statusom "+status+" pri servisu "+taskId+" za naročnika "+subscriberId+".");
                  promises.push(dbChanges.addNewChangeNotify(sysChange.id,notify[i]));
                }
                Promise.all(promises)
                .then((results) => {
                  //console.log("all done", results);
                  let msg = "Uspešno dodajanje nove datoteke.";
                  let type = 2;
                  let fileLocation = `${__dirname}/../files/projects/${newFile.path_name}`;
                  if (fs.existsSync(fileLocation)) {
                    // newFile.base64 = fs.readFileSync(fileLocation, 'base64');
                    if (newFile.type == 'IMG'){
                      Jimp.read(fileLocation).then(img => {
                        img.resize(150, Jimp.AUTO).quality(20).getBase64Async(Jimp.AUTO).then(base64 => {
                          //return res.json({success:true, data: base64, id: fileId, type: 'IMG'});
                          newFile.base64 = base64;
                          return res.status(200).json({
                            status: 'success',
                            file: newFile,
                            success: 'true',
                            message: msg,
                            type: type,
                          });
                        })
                        .catch(err => {
                          return res.json({success:false, error: err});
                        });
                      })
                      .catch((e) => {
                        return res.json({success:false, error: e, msg: 'Datoteka ne obstaja.', id: fileId, type: fileType});
                      })
                    }
                    else if (newFile.type == 'PDF'){
                      fsp.readFile(fileLocation, 'base64').then(base64 => {
                        //console.log(data);
                        newFile.base64 = base64;
                        return res.status(200).json({
                          status: 'success',
                          file: newFile,
                          success: 'true',
                          message: msg,
                          type: type,
                        });
                      })
                      .catch((e) => {
                        return res.json({success:false, error: e});
                      })
                    }
                    else{
                      return res.status(200).json({
                        status: 'success',
                        file: newFile,
                        success: 'true',
                        message: msg,
                        type: type,
                      });
                    }
                  }
                  else{
                    return res.status(200).json({
                      status: 'file not found',
                      file: newFile,
                      success: 'false',
                      message: msg,
                      type: type,
                    });
                  }
                })
                .catch((e)=>{
                  return res.status(500).json({
                    status: 'error',
                    message: 'Napaka pri zapisovanju obvestil o spremembi za obveščanje uporabnikov.',
                    error: e,
                  })
                })
              })
              .catch((e)=>{
                return res.status(500).json({
                  status: 'error',
                  message: 'Napaka pri zapisovanju v sprememb za novo datoeko.',
                  error: e,
                })
              })
            })
            .catch((e)=>{
              return res.status(500).json({
                status: 'error',
                message: 'Napaka pri pridobivanju podatkov uporabnikov za obveščevanje o novi datoteki.',
                error: e,
              })
            })
          }
        })
        .catch((e)=>{
          return res.status(500).json({
            status: 'error',
            message: 'Napaka pri zapisovanju podatkov datoteke v tabelo z datotekami.',
            error: e,
          })
        })
      }
    }
  })
});
//FILE DOWNLOAD
router.get('/sendMeDoc', auth.authenticate, function(req, res){
  let filename = req.query.filename;
  console.log('Zahteva za datoteko: '+ filename);
  var file = `${__dirname}/../files/projects/${filename}`;
  if (fs.existsSync(file)) {
    dbFiles.getFileInfo(filename).then(fileInfo => {
      switch (fileInfo.type) {
        case 'IMG': break;
        case 'PDF': res.type('application/pdf'); break;
        case 'DOC': res.type('application/msword'); break;
        case 'PPT': res.type('application/mspowerpoint'); break;
        case 'ZIP': res.type('application/x-compressed'); break;
        case 'TXT': res.type('text/plain'); break;
        case 'XLS': res.type('application/excel'); break;
        default: break;
      }
      //let fileExtsIMG = ['jpg', 'jpeg', 'png', 'gif'];
      if (fileInfo.type == 'IMG'){
        let tmp = fileInfo.original_name.split('.');
        if (tmp[tmp.length-1].toLocaleLowerCase() == 'png') res.type('image/png');
        else if (tmp[tmp.length-1].toLocaleLowerCase() == 'jpg' || tmp[tmp.length-1] == 'jpeg') res.type('image/jpeg');
        else if (tmp[tmp.length-1].toLocaleLowerCase() == 'gif') res.type('image/gif');
      }
      // Do something
      res.download(file, fileInfo.original_name, function(err){
        if(err){
          console.error(err);
        }
      });
    })
    .catch(e => {
      let msg = "Napaka: Datoteka ne obstaja v bazi!";
      let type = 1;
      return res.status(500).json({
        status: 'error',
        message: msg,
        type
      })
    })
  }
  else{
    dbFiles.updateActiveFile(filename)
      .then(file=>{
        console.log("Datoteka "+filename+" ne obstaja več, aktivnost v bazi popravljena.")
        let msg = "Napaka: Datoteka ne obstaja več!";
        let type = 1;
        return res.status(500).json({
          status: 'error',
          message: msg,
          type
        })
      })
      .catch((e)=>{
        console.log(e);
      })
  }
  
  //return
  //file exists
})

//SERVISI
router.get('/', auth.authenticate, function(req, res, next) {
  //for query params (msg or serviceId)
  let taskId = req.query.taskId;
  let notificationType = req.query.type;
  let today = new Date();
  //0 = all, 1 = unfinished, 2 = finished
  let completionTask = 1;
  let tid ='';
  if(notificationType){
    tid = notificationType;
    completionTask = 0;
  }
  else if(req.query.taskId)
    tid = '9';
  let msg='';
  let type=0;
  if(req.query.msg){
    msg = req.query.msg;
    type = req.query.type;
  }
  dbService.getServicesFixed(0,1).then(tasks => {
    if(tasks){
      if(!notificationType)
        tasks = tasks.filter(t => t.completion != 100);
      let stStrani = Math.ceil(tasks.length/10);
      let stran = 1;
      if(taskId){
        let index = tasks.findIndex(t => t.id == parseInt(taskId));
        index++;
        if(index == 0)
          index = 1;
        stran = Math.ceil(index / 10) * 10;
        let upperLimit = stran;
        let lowerLimit = upperLimit - 10;
        stran = stran / 10;
        //console.log(index);
        //console.log(stran);
        tasks = tasks.slice(lowerLimit,upperLimit);
        addFormatedDateForTasks(tasks);
      }
      else{
        tasks = tasks.slice(0,10);
        addFormatedDateForTasks(tasks);
      }
      res.render('servis', {
        title: "Seznam servisov - storitev",
        user_name: req.session.user_name,
        user_surname: req.session.user_surname,
        user_type: req.session.type,
        user_id: req.session.user_id,
        tasks: tasks,
        pages: stStrani,
        page: stran,
        msg: msg,
        type: type,
        tid: tid,
        completionTable: completionTask,
        today: today,
      })
    }
    else{
      res.render('servis', {
        title: "Seznam servisov - storitev",
        user_name: req.session.user_name,
        user_surname: req.session.user_surname,
        user_type: req.session.type,
        user_id: req.session.user_id,
        tasks: [],
        pages: 0,
        msg: msg,
        type: type,
        tid: tid,
        completionTable: completionTask,
        today: today,
      })
    }

  })
  .catch((e)=>{
    next(e);
  })
  //console.log(req.session);
})
//Get tasks with no projectId (services)
router.get('/tasks', auth.authenticate, function(req, res, next){
  let category = req.query.categoryTask;
  let active = req.query.activeTask;
  let completion = req.query.completionTask;
  let showMyInputTasks = req.query.showMyInputTasks;
  let showMyTasks = req.query.showMyTasks;
  dbService.getServicesFixed(parseInt(category), parseInt(active)).then(tasks=>{
    if(completion){
      if(completion == 1)
        tasks = tasks.filter(t => t.completion != 100);
      else if(completion == 2)
        tasks = tasks.filter(t => t.completion == 100);
    }
    if(showMyInputTasks == 'true'){
      tasks = tasks.filter(t => t.author_id == req.session.user_id)
    }
    if(showMyTasks == 'true'){
      tasks = tasks.filter(t => (t.workers_id ? t.workers_id : '').split(',').find(f => f == req.session.user_id) == req.session.user_id)
    }
    addFormatedDateForTasks(tasks);
    res.json({success:true, data:tasks});
  })
  .catch((e)=>{
    return res.json({success:false, error:e});
  })
})
//Add new service (task with no projectId and with subscriberId)
router.post('/add', auth.authenticate, function(req, res, next){
  //console.log("iz modalnega");
  //debugger;
  let taskName = req.body.taskName;
  let duration = req.body.taskDuration;
  let start = req.body.taskStart;
  let finish = req.body.taskFinish;
  let completion = 0;
  let subscriber = req.body.taskSubscriber;
  let workersId = req.body.taskAssignment;
  let category = req.body.taskCategory;
  let priority = req.body.taskPriority;
  let weekend = req.body.taskWeekend;
  let override = req.body.override;
  let promises = [];
  if(!duration)
    duration = 0;
  //debugger;
  let taskAssignmentArray = [];
  if(workersId)
    taskAssignmentArray = workersId.split(",");
  //add even if there are conflicts, dont need to check them
  if(override == 1){
    //if there are workers (might join these two cases)
    dbService.addServisNew(subscriber, taskName, duration, start, finish, category, priority, completion, weekend).then(task=>{
      //debugger;
      if(workersId){
        //console.log(task);
        for(let i = 0; i < taskAssignmentArray.length; i++){
          dbTasks.assignTask(task.id, taskAssignmentArray[i]).then(assignedTask=>{
            //console.log(assignedTask)
          })
          .catch((e)=>{
            return res.json({success:false, error:e});
          })
        }
      }
      return res.json({success:true, newTaskId:task.id, conflicts:null, case:"override"});
    })
    .catch((e)=>{
      return res.json({success:false, error:e});
    })
  }
  //no override, check if there are conflicts if there are workers (and both dates)
  else{
    if(workersId && start && finish){
      for(let i=0; i<taskAssignmentArray.length; i++){
        promises.push(dbService.getWorkerServiceConflicts(taskAssignmentArray[i], start, finish));
      }
      Promise.all(promises)
        .then((results)=>{
          //debugger;
          let conflictsExists = false;
          if(results.length > 0){
            for(let i = 0; i < results.length; i++){
              if(results[i].length > 0)
                conflictsExists = true;
            }
          }
          // there are conflicts, send them back and tell adding wasnt successful
          if(conflictsExists == true)
            res.json({success:false, newTaskId:null, conflicts:results, case:"normal, conflicts"});
          // no conflicts, add new task with new assignments
          else if(conflictsExists == false){
            dbService.addServisNew(subscriber, taskName, duration, start, finish, category, priority, completion, weekend).then(task=>{
              //debugger
              if(workersId){
                //console.log(task);
                for(let i = 0; i < taskAssignmentArray.length; i++){
                  dbTasks.assignTask(task.id, taskAssignmentArray[i]).then(assignedTask=>{
                    //console.log(assignedTask)
                  })
                  .catch((e)=>{
                    return res.json({success:false, error:e});
                  })
                }
              }
              return res.json({success:true, newTaskId:task.id, conflicts:null, case:"normal, no conflicts"});
            })
            .catch((e)=>{
              return res.json({success:false, error:e});
            })
          }
      })
      .catch((e)=>{
        return res.json({success:false, error:e});
      })
    }
    else{
      //debugger
      //either there were no workers or both dates
      dbService.addServisNew(subscriber, taskName, duration, start, finish, category, priority, completion, weekend).then(task=>{
        //debugger;
        if(workersId){
          //console.log(task);
          for(let i = 0; i < taskAssignmentArray.length; i++){
            dbTasks.assignTask(task.id, taskAssignmentArray[i]).then(assignedTask=>{
              //console.log(assignedTask)
            })
            .catch((e)=>{
              return res.json({success:false, error:e});
            })
          }
        }
        return res.json({success:true, newTaskId:task.id, conflicts:null, case:"no workers or no dates"});
      })
      .catch((e)=>{
        return res.json({success:false, error:e});
      })
    }
  }
})
//update the task
router.post('/update', auth.authenticate, function(req, res, next){
  let taskId = req.body.taskId;
  let taskName = req.body.taskName;
  let duration = req.body.taskDuration;
  let start = req.body.taskStart;
  let finish = req.body.taskFinish;
  let workersId = req.body.taskAssignment;
  let subscriber = req.body.taskSubscriber;
  let active = req.body.taskActive;
  let category = req.body.taskCategory;
  let priority = req.body.taskPriority;
  let weekend = req.body.taskWeekend;
  let override = req.body.override;
  let currentWorkers = req.body.currentWorkers;
  let promises = [];
  if(!duration)
    duration = 0;
  let taskAssignmentArray = [];
  if(workersId)
    taskAssignmentArray = workersId.split(",");
  if(active == false)
    override = 1;
  if(override == 1){
    dbService.updateServisNew(taskId, taskName, duration, start, finish, subscriber, active, category, priority, weekend).then(task => {
      if(workersId != currentWorkers){
        dbTasks.removeAssignTask(taskId).then(task => {
          if(workersId){
            for(let i = 0; i < taskAssignmentArray.length; i++){
              dbTasks.assignTask(taskId, taskAssignmentArray[i]).then(assignTask =>{
                //do nothing;
              })
              .catch((e)=>{
                return res.json({success:false, error:e});
              })
            }
          }
        })
        .catch((e)=>{
          return res.json({success:false, error:e});
        })
      }
      return res.json({success: true, conflicts:null, case:"override"});
    })
    .catch((e)=>{
      return res.json({success:false, error:e});
    })
  }
  else{
    if(workersId && start && finish){
      for(let i = 0; i < taskAssignmentArray.length; i++){
        promises.push(dbService.getWorkerServiceConflicts(taskAssignmentArray[i], start, finish, taskId));
      }
      Promise.all(promises).then((results) => {
        let conflictsExists = false;
        if(results.length > 0){
          for(let i = 0; i < results.length; i++){
            if(results[i].length > 0)
              conflictsExists = true;
          }
        }
        if(conflictsExists == true){
          res.json({success:false, conflicts:results, case:"normal, conflicts"});
        }
        else if(conflictsExists == false){
          dbService.updateServisNew(taskId, taskName, duration, start, finish, subscriber, active, category, priority, weekend).then(task => {
            if(workersId != currentWorkers){
              dbTasks.removeAssignTask(taskId).then(task =>{
                if(workersId){
                  for(let i = 0; i < taskAssignmentArray.length; i++){
                    dbTasks.assignTask(taskId, taskAssignmentArray[i]).then(assignTask =>{
                      //do nothing;
                    })
                    .catch((e)=>{
                      return res.json({success:false, error:e});
                    })
                  }
                }
              })
              .catch((e)=>{
                return res.json({success:false, error:e});
              })
            }
            return res.json({success:true, conflicts:null, case:"normal, no conflicts"});
          })
          .catch((e)=>{
            return res.json({success:false, error:e});
          })
        }
      })
      .catch((e)=>{
        return res.json({success:false, error:e});
      })
    }
    else{
      dbService.updateServisNew(taskId, taskName, duration, start, finish, subscriber, active, category, priority, weekend).then(task => {
        if(workersId != currentWorkers){
          dbTasks.removeAssignTask(taskId).then(task =>{
            if(workersId){
              for(let i = 0; i < taskAssignmentArray.length; i++){
                dbTasks.assignTask(taskId, taskAssignmentArray[i]).then(assignTask =>{
                  //do nothing;
                })
                .catch((e)=>{
                  return res.json({success:false, error:e});
                })
              }
            }
          })
          .catch((e)=>{
            return res.json({success:false, error:e});
          })
        }
        return res.json({success:true, conflicts:null, case:"no workers or no dates"});
      })
      .catch((e)=>{
        return res.json({success:false, error:e});
      })
    }
  }
})
//get all changes for task or subtask
router.get('/changes', auth.authenticate, function(req, res, next){
  let taskId = req.query.taskId;
  dbService.getTaskChangesSystem(taskId).then(taskChanges => {
    return res.json({success:true, taskChanges, subtaskChanges:null});
  })
  .catch((e)=>{
    return res.json({success:false, error:e});
  })
})
module.exports = router;