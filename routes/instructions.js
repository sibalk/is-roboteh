var express = require('express');
var router = express.Router();

//auth & db
var auth = require('../controllers/authentication');

///MAIN PAGE --> VIZUALIZATION
router.get('/', auth.authenticate, function(req, res){
  res.render('instruction', {
    title: "Navodila",
    user_name: req.session.user_name,
    user_surname: req.session.user_surname,
    user_username: req.session.user_username,
    user_typeid: req.session.role_id,
    user_type: req.session.type,
    user_id: req.session.user_id
  });
});
router.get('/new', auth.authenticate, function(req, res){
  res.render('instructions', {
    title: "Navodila",
    user_name: req.session.user_name,
    user_surname: req.session.user_surname,
    user_username: req.session.user_username,
    user_typeid: req.session.role_id,
    user_type: req.session.type,
    user_id: req.session.user_id
  });
});
module.exports = router;