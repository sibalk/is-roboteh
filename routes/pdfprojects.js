var express = require('express');
var router = express.Router();
var dateFormat = require('dateformat');
const fs = require('fs');
var Jimp = require('jimp');

// Define font files
var fonts = {
  Roboto: {
    normal: `${__dirname}/../public/pdfmake/Roboto-Regular.ttf`,
    bold: `${__dirname}/../public/pdfmake/Roboto-Medium.ttf`,
    italics: `${__dirname}/../public/pdfmake/Roboto-Italic.ttf`,
    bolditalics: `${__dirname}/../public/pdfmake/Roboto-MediumItalic.ttf`,
  }
};
var PdfPrinter = require('pdfmake');
var printer = new PdfPrinter(fonts);

let dbProjects = require('../model/projects/dbProjects');
let dbTasks = require('../model/tasks/dbTasks');
let dbBase64Images = require('../model/base64images/base64images');
let dbProjectDates = require('../model/dates/dbDates');

function formatDate(date){
  return {
    "date": dateFormat(date, "dd.mm.yyyy"),
    "time": dateFormat(date, "HH:MM")
  }
}

//return pdf for list of projects based on filters (default: all active projects)
router.get('/', function(req, res, next){
  let showCompletion = req.query.showCompletion == 'true' ? true : false;
  let showStart = req.query.showStart == 'true' ? true : false;
  let showFinish = req.query.showFinish == 'true' ? true : false;
  let optionPrefix = (req.query.optionPrefix == '0' || req.query.optionPrefix == '1' || req.query.optionPrefix == '2') ? req.query.optionPrefix : '0';
  let optionCompletion = (req.query.optionCompletion == '0' || req.query.optionCompletion == '1' || req.query.optionCompletion == '2') ? req.query.optionCompletion : '0';
  let optionActive = (req.query.optionActive == '0' || req.query.optionActive == '1' || req.query.optionActive == '2') ? req.query.optionActive : '1';
  //res.json({success:false, error:'not implemented yet, contact RIS administrator', msg:'Funkcionalnost še ni implementirana. Prosim kontaktirajte administratorja RIS-a.'})
  
  dbProjects.getProjects(0)
  Promise.all([dbProjects.getProjects(0), dbBase64Images.getRobotehImage()])
  .then(([projects, base64RobotehImg])=>{
    //apply filters and do sort based od STR & RT prefix
    let tmp1 = projects.filter(p => p.project_number.substring(0,2) == 'RT' );
    let tmp2 = projects.filter(p => p.project_number.substring(0,3) == 'STR' );
    let tmp3 = projects.filter(p => p.project_number.substring(0,3) != 'STR' && p.project_number.substring(0,2) != 'RT');
    tmp1.forEach(p => p.number_order = parseInt(p.project_number.substring(2)));
    tmp2.forEach(p => p.number_order = parseInt(p.project_number.substring(3)));
    let sortedTmp1 = tmp1.sort(function(a,b){
      if (isNaN(a.number_order))
        return 1;
      else if (isNaN(b.number_order))
        return -1;
      return b.number_order - a.number_order;
    });
    let sortedTmp2 = tmp2.sort(function(a,b){
      if (isNaN(a.number_order))
        return 1;
      else if (isNaN(b.number_order))
        return -1;
      return b.number_order - a.number_order;
    });
    //let allRTProjects = sortedTmp1, allSTRProjects = sortedTmp2;
    projects = sortedTmp2.concat(sortedTmp1,tmp3);//all projects sorted based on their number and prefix
    let tmp;
    //filter prefix
    if(optionPrefix == '1') tmp = sortedTmp1;
    else if(optionPrefix == '2') tmp = sortedTmp2;
    else tmp = sortedTmp2.concat(sortedTmp1,tmp3);
    //filter project activity
    if(optionActive == '1') tmp = tmp.filter(p => p.active == true);
    else if(optionActive == '2') tmp = tmp.filter(p => p.active == false);
    //filter project completion
    if(optionCompletion == '1') tmp = tmp.filter(p => p.completion != 100);
    else if(optionCompletion == '2') tmp = tmp.filter(p => p.completion == 100);

    //make pdf defenition
    let today = new Date();
    let pdfHeader = 'Seznam projektov - ' + (today.getDate()+'. '+(today.getMonth()+1)+'. '+today.getFullYear());
    var pdfTableProject = {
			style: 'tableExample',
			table: {
				widths: ['auto', 'auto', '*', '*'],
				body: [
					[
            'Št.proj.',
            'Leto',
            'Ime/Naziv',
            {text:'Naročnik', alignment:'center'}
          ],
				]
			},
      layout: {
				fillColor: function (rowIndex, node, columnIndex) {
				    if(rowIndex === 0) return '#CCCCCC'
				    else if(rowIndex % 2 === 0) return '#e8e8e8';
				}
			}
		}
    //add additional table headers (start, finish, completion)
    if(showStart){
      pdfTableProject.table.widths.push('auto');
      pdfTableProject.table.body[0].push({text:'Začetek', alignment:'center'})
    }
    if(showFinish){
      pdfTableProject.table.widths.push('auto');
      pdfTableProject.table.body[0].push({text:'Konec', alignment:'center'})
    }
    if(showCompletion){
      pdfTableProject.table.widths.push('auto');
      pdfTableProject.table.body[0].push({text:'%', alignment:'center'})
    }
    for (const project of tmp) {
      let year = new Date(project.created).getFullYear()+'';
      let startLabel = '/';
      if(project.start){
        let tmp = new Date(project.start);
        startLabel = tmp.getDate() + '. ' + (tmp.getMonth()+1) + '. ' + tmp.getFullYear();
      }
      let finishLabel = '/';
      if(project.finish){
        let tmp = new Date(project.finish);
        finishLabel = tmp.getDate() + '. ' + (tmp.getMonth()+1) + '. ' + tmp.getFullYear();
      }
      let projectRow = [
        {text: project.project_number},
        {text: year},
        {text: project.project_name},
        {text: project.name, alignment:'center'},
      ];
      if(showStart) projectRow.push({text: startLabel, noWrap: true, alignment:'center'});
      if(showFinish) projectRow.push({text: finishLabel, noWrap: true, alignment:'center'});
      if(showCompletion) projectRow.push({text: project.completion+'', alignment:'center'});
      pdfTableProject.table.body.push(projectRow);
    }
    //res.json({success:true, data:projects});
    var docDefinition = {
      info: {
        title: pdfHeader,
        author: 'Roboteh-Informacijski-Sistem',
        subject: 'List of all projects (with filters)',
      },
      pageSize: 'A4',
      pageOrientation: 'portrait',
      content: [
        {
          style: 'tableRoboteh',
          table: {
            widths: [110,'*',110],
            body: [
              [
                {image: 'roboteh',width: 110},
                {text: 'Seznam projektov',alignment: 'center',fontSize: 22},
                {text: (today.getDate()+'. '+(today.getMonth()+1)+'. '+today.getFullYear()),alignment: 'right'},
              ]
            ],
          }, layout: 'noBorders'
        },
        pdfTableProject,
      ],
      images: {roboteh: base64RobotehImg.base64,},
      styles: {
        tableRoboteh: {margin: [0, -15, 0, 15]},
      }
    };
    let pdfDoc = printer.createPdfKitDocument(docDefinition);
    const filename = encodeURI(pdfHeader);
    res.setHeader('Content-disposition', `attachment; filename*=UTF-8''${filename}.pdf; filename=${filename}.pdf`);
    res.setHeader('Content-type', 'application/pdf');
    pdfDoc.pipe(res);
    pdfDoc.end();
  })
  .catch((e)=>{
    //return res.json({success:false, error: e});
    next(e);
  })
})
//return pdf for specific project based on project id
router.get('/:id', function(req, res, next){
  let projectId = req.params.id;
  let style = req.query.style;
  let pdfOptionCompletion = req.query.pdfOptionCompletion;
  let pdfOptionLogo = req.query.pdfOptionLogo == 'true' ? true : false;
  let pdfOptionNumber = req.query.pdfOptionNumber;
  let pdfOptionDate = req.query.pdfOptionDate == 'true' ? true : false;
  let pdfOptionNote = req.query.pdfOptionNote == 'true' ? true : false;
  let pdfOptionProjectDates = req.query.pdfOptionProjectDates == 'true' ? true : false;
  let pdfOptionTodayDate = req.query.pdfOptionTodayDate == 'true' ? true : false;
  let pdfOptionComments = req.query.pdfOptionComments == 'true' ? true : false;
  let pdfOptionLeaders = req.query.pdfOptionLeaders;
  let pdfOptionBuilders = req.query.pdfOptionBuilders;
  let pdfOptionMechanics = req.query.pdfOptionMechanics;
  let pdfOptionElectricans = req.query.pdfOptionElectricans;
  let pdfOptionPlc = req.query.pdfOptionPlc;
  let pdfOptionRobot = req.query.pdfOptionRobot;
  let pdfCategoryPurchase = req.query.pdfCategoryPurchase == 'true' ? true : false;
  let pdfCategoryConstruction = req.query.pdfCategoryConstruction == 'true' ? true : false;
  let pdfCategoryMechanic = req.query.pdfCategoryMechanic == 'true' ? true : false;
  let pdfCategoryElectric = req.query.pdfCategoryElectric == 'true' ? true : false;
  let pdfCategoryAssembly = req.query.pdfCategoryAssembly == 'true' ? true : false;
  let pdfCategoryProgram = req.query.pdfCategoryProgram == 'true' ? true : false;
  let pdfCategoryNone = req.query.pdfCategoryNone == 'true' ? true : false;
  let pdfCategoryMechAssemblyRoboteh = req.query.pdfCategoryMechAssemblyRoboteh == 'true' ? true : false;
  let pdfCategoryMechAssemblyStranka = req.query.pdfCategoryMechAssemblyRoboteh == 'true' ? true : false;
  let pdfCategoryEleAssemblyRoboteh = req.query.pdfCategoryMechAssemblyRoboteh == 'true' ? true : false;
  let pdfCategoryEleAssemblyStranka = req.query.pdfCategoryMechAssemblyRoboteh == 'true' ? true : false;
  let pdfCategoryEleProektiranje = req.query.pdfCategoryMechAssemblyRoboteh == 'true' ? true : false;
  let pdfCategoryDobava = req.query.pdfCategoryMechAssemblyRoboteh == 'true' ? true : false;
  let pdfCategorySAT = req.query.pdfCategoryMechAssemblyRoboteh == 'true' ? true : false;
  let pdfCategoryFAT = req.query.pdfCategoryMechAssemblyRoboteh == 'true' ? true : false;
  let pdfCategoryZagon = req.query.pdfCategoryMechAssemblyRoboteh == 'true' ? true : false;
  let pdfCategoryNaklad = req.query.pdfCategoryMechAssemblyRoboteh == 'true' ? true : false;

  //debugger;

  let promises = [];
  // get project info
  promises.push(dbProjects.getProjectById(projectId));
  // get project workers
  promises.push(dbProjects.getWorkersByProjectId(projectId));
  // get project tasks
  promises.push(dbTasks.getTasksByProjectIdFixed(projectId, 0, 1));
  //get roboteh base64 image
  promises.push(dbBase64Images.getRobotehImage());
  //get kuka partner base64 image
  promises.push(dbBase64Images.getKukaPartnerImage());
  //get Manager icon base64 image
  promises.push(dbBase64Images.getManagerIcon());
  //get Robot programmer icon base64 image
  promises.push(dbBase64Images.getRobotProgrammerIcon());
  //get PLC programmer icon base64 image
  promises.push(dbBase64Images.getPlcProgrammerIcon());
  //get Mechanic icon base64 image
  promises.push(dbBase64Images.getMechanicIcon());
  //get Electrican icon base64 image
  promises.push(dbBase64Images.getElectricanIcon());
  //get Builder icon base64 image
  promises.push(dbBase64Images.getBuilderIcon());
  // get project dates
  promises.push(dbProjectDates.getProjectDates(projectId));

  Promise.all(promises).then(data => {
    let projectInfo = data[0];
    let allWorkersTable = data[1];
    let projectTasks = data[2];
    let base64RobotehImg = data[3];
    let base64KukaPartnerImg = data[4];
    let projectDates = data[11];
    if (pdfOptionCompletion == '1') {
      projectTasks = projectTasks.filter(task => task.completion != '100')
    }
    else if (pdfOptionCompletion == '2') {
      projectTasks = projectTasks.filter(task => task.completion == '100')
    }
    let labelTodayDate = pdfOptionTodayDate ? dateFormat(new Date(), 'dd. mm. yyyy') : '';
    
    let pdfTable = {margin:[0,5,0,15],table:{widths:[30,'*',50,'*'], body: []},layout:'noBorders'};
    let index = 0;
    let row = [];
    let worker;
    let imgManeger = data[5].base64;
    let imgRobot = data[6].base64;
    let imgPlc = data[7].base64;
    let imgMechanic = data[8].base64;
    let imgElectrican = data[9].base64;
    let imgBuilder = data[10].base64;
    let img;
    
    let date = '';
    if(pdfOptionDate){
      let start = projectInfo.start ? new Date(projectInfo.start) : undefined;
      let finish = projectInfo.finish ? new Date(projectInfo.finish) : undefined;
      date = start ? dateFormat(start, 'dd. mm. yyyy') + ' - ' : '/ - ';
      date = finish ? date + dateFormat(finish, 'dd. mm. yyyy') : date + '/';
      //debugger;
    }
    let number = pdfOptionNumber == 'true' ? projectInfo.project_number + ' - ' + projectInfo.project_name : '';
    // if(req.query.pdfOptionNumber){
    //   number = data[0].project_number + ' - ' + data[0].project_name;
    // }
    let tableStyle = '';
    if (style == '1')
      tableStyle = 'lightHorizontalLines';
    else if (style == '2')
      tableStyle = 'noBorders';
    //debugger;
    // note section
    let labelNote = {};
    let pdfSectionNote = {};
    if (pdfOptionNote){
      labelNote = {text: 'Opomba/Opis', margin: [20, 10, 0, 0], bold: true, fontSize: 13};
      pdfSectionNote = {text: projectInfo.notes, margin: [0, 0, 0, 10], preserveLeadingSpaces: true};
    }
    // project dates
    let labelProjectDates = {text: '', margin: [20, 10, 0, 0], bold: true, fontSize: 13};
    let pdfTableProjectDates = {style: 'tableCategory',layout: tableStyle ,table:{widths:['*','auto' ], body: [
      [
        {fillColor: '#cccccc', text: 'Glavni datumi'},
        {fillColor: '#cccccc', text: 'Datum'},
      ]
    ]}};
    let marginProjectDates = {text: '', margin: [20, 10, 0, 10], bold: true, fontSize: 13};
    if (pdfOptionProjectDates){
      for (const projectDate of projectDates) {
        let dateLabel = dateFormat(new Date(projectDate.date), 'dd. mm. yyyy');
        let currentDate = [
          {text: projectDate.name},
          {text: dateLabel, noWrap: true},
        ];
        pdfTableProjectDates.table.body.push(currentDate);
      }
    }
    else{
      labelProjectDates = {};
      pdfTableProjectDates = {};
      marginProjectDates = {};
    }

    // workers table
    for(let i=0;i<allWorkersTable.length; i++){
      if(allWorkersTable[i].workRole.substring(0,5).toLowerCase() == 'vodja' && pdfOptionLeaders == 'true'){
        img = imgManeger;
        worker = [
          {width:25,margin:[0,0,0,0],image: img},
          {text: allWorkersTable[i].name + " " + allWorkersTable[i].surname, italics: true}
        ];
        row.push(worker[0], worker[1]);
        index++;
      }
      else if(allWorkersTable[i].workRole.toLowerCase() == 'programer robotov' && pdfOptionRobot == 'true'){
        img = imgRobot;
        worker = [
          {width:25,margin:[0,0,0,0],image: img},
          {text: allWorkersTable[i].name + " " + allWorkersTable[i].surname, italics: true}
        ];
        row.push(worker[0], worker[1]);
        index++;
      }
      else if(allWorkersTable[i].workRole.toLowerCase() == 'programer plc-jev' && pdfOptionPlc == 'true'){
        img = imgPlc;
        worker = [
          {width:25,margin:[0,0,0,0],image: img},
          {text: allWorkersTable[i].name + " " + allWorkersTable[i].surname, italics: true}
        ];
        row.push(worker[0], worker[1]);
        index++; 
      }
      else if(allWorkersTable[i].workRole.toLowerCase() == 'strojnik' && pdfOptionMechanics == 'true'){
        img = imgMechanic;
        worker = [
          {width:25,margin:[0,0,0,0],image: img},
          {text: allWorkersTable[i].name + " " + allWorkersTable[i].surname, italics: true}
        ];
        row.push(worker[0], worker[1]);
        index++;
      }
      else if(allWorkersTable[i].workRole.toLowerCase() == 'električar' && pdfOptionElectricans == 'true'){
        img = imgElectrican;
        worker = [
          {width:25,margin:[0,0,0,0],image: img},
          {text: allWorkersTable[i].name + " " + allWorkersTable[i].surname, italics: true}
        ];
        row.push(worker[0], worker[1]);
        index++;
      }
      else if((allWorkersTable[i].workRole.toLowerCase() == 'konstruktor' || allWorkersTable[i].workRole.toLowerCase() == 'konstrukter') && pdfOptionBuilders == 'true'){
        img = imgBuilder;
        worker = [
          {width:25,margin:[0,0,0,0],image: img},
          {text: allWorkersTable[i].name + " " + allWorkersTable[i].surname, italics: true}
        ];
        row.push(worker[0], worker[1]);
        index++;
      }
      if(index != 0 && ((index)%2) == 0 && row.length != 0){
        pdfTable.table.body.push(row);
        row = [];
      }
    }
    if(index%2 != 0){
      worker = [
        {text: '', italics: true},
        {text: '', italics: true}
      ];
      row.push(worker[0], worker[1]);
      pdfTable.table.body.push(row);
    }
    if(pdfTable.table.body.length == 0){
      pdfTable = {text: ''};
    }
    let labelNabava = {text: '', margin: [20, 10, 0, 0], bold: true, fontSize: 13};
    let pdfTableNabava = {style: 'tableCategory',layout: tableStyle ,table:{widths:['*','auto','auto','auto',/* 'auto'*/ ], body: [
      [
        {fillColor: '#cccccc', text: 'Nabava'},
        {fillColor: '#cccccc', text: 'Št. dni'},
        {fillColor: '#cccccc', text: 'Začetek'},
        {fillColor: '#cccccc', text: 'Konec'},
        //{fillColor: '#cccccc', text: 'Dokončano'},
      ]
    ]}};
    let labelKonst = {text: '', margin: [20, 10, 0, 0], bold: true, fontSize: 13};
    let pdfTableKonstr = {style: 'tableCategory',layout: tableStyle ,table:{widths:['*','auto','auto','auto',/* 'auto'*/], body: [
      [
        {fillColor: '#cccccc', text: 'Konstrukcija'},
        {fillColor: '#cccccc', text: 'Št. dni'},
        {fillColor: '#cccccc', text: 'Začetek'},
        {fillColor: '#cccccc', text: 'Konec'},
        //{fillColor: '#cccccc', text: 'Dokončano'},
      ]
    ]}};
    let labelStrIzd = {text: '', margin: [20, 10, 0, 0], bold: true, fontSize: 13};
    let pdfTableStrIzdel = {style: 'tableCategory',layout: tableStyle ,table:{widths:['*','auto','auto','auto',/* 'auto'*/], body: [
      [
        {fillColor: '#cccccc', text: 'Strojna Izdelava'},
        {fillColor: '#cccccc', text: 'Št. dni'},
        {fillColor: '#cccccc', text: 'Začetek'},
        {fillColor: '#cccccc', text: 'Konec'},
        //{fillColor: '#cccccc', text: 'Dokončano'},
      ]
    ]}};
    let labelEleIzd = {text: '', margin: [20, 10, 0, 0], bold: true, fontSize: 13};
    let pdfTableEleIzdel = {style: 'tableCategory',layout: tableStyle ,table:{widths:['*','auto','auto','auto',/* 'auto'*/], body: [
      [
        {fillColor: '#cccccc', text: 'Elektro Izdelava'},
        {fillColor: '#cccccc', text: 'Št. dni'},
        {fillColor: '#cccccc', text: 'Začetek'},
        {fillColor: '#cccccc', text: 'Konec'},
        //{fillColor: '#cccccc', text: 'Dokončano'},
      ]
    ]}};
    let labelMontaza = {text: '', margin: [20, 10, 0, 0], bold: true, fontSize: 13};
    let pdfTableMontaza = {style: 'tableCategory',layout: tableStyle ,table:{widths:['*','auto','auto','auto',/* 'auto'*/], body: [
      [
        {fillColor: '#cccccc', text: 'Montaža'},
        {fillColor: '#cccccc', text: 'Št. dni'},
        {fillColor: '#cccccc', text: 'Začetek'},
        {fillColor: '#cccccc', text: 'Konec'},
        //{fillColor: '#cccccc', text: 'Dokončano'},
      ]
    ]}};
    let labelProg = {text: '', margin: [20, 10, 0, 0], bold: true, fontSize: 13};
    let pdfTableProgram = {style: 'tableCategory',layout: tableStyle ,table:{widths:['*','auto','auto','auto',/* 'auto'*/], body: [
      [
        {fillColor: '#cccccc', text: 'Programiranje'},
        {fillColor: '#cccccc', text: 'Št. dni'},
        {fillColor: '#cccccc', text: 'Začetek'},
        {fillColor: '#cccccc', text: 'Konec'},
        //{fillColor: '#cccccc', text: 'Dokončano'},
      ]
    ]}};
    let labelBrez = {text: '', margin: [20, 10, 0, 0], bold: true, fontSize: 13};
    let pdfTableBrez = {style: 'tableCategory',layout: tableStyle ,table:{widths:['*','auto','auto','auto',/* 'auto'*/], body: [
      [
        {fillColor: '#cccccc', text: 'Brez'},
        {fillColor: '#cccccc', text: 'Št. dni'},
        {fillColor: '#cccccc', text: 'Začetek'},
        {fillColor: '#cccccc', text: 'Konec'},
        //{fillColor: '#cccccc', text: 'Dokončano'},
      ]
    ]}};
    let labelMechAssemblyRoboteh = {text: '', margin: [20, 10, 0, 0], bold: true, fontSize: 13};
    let pdfTableMechAssemblyRoboteh = {style: 'tableCategory',layout: tableStyle ,table:{widths:['*','auto','auto','auto',/* 'auto'*/], body: [
      [
        {fillColor: '#cccccc', text: 'Strojna montaža - Roboteh'},
        {fillColor: '#cccccc', text: 'Št. dni'},
        {fillColor: '#cccccc', text: 'Začetek'},
        {fillColor: '#cccccc', text: 'Konec'},
      ]
    ]}};
    let labelMechAssemblyStranka = {text: '', margin: [20, 10, 0, 0], bold: true, fontSize: 13};
    let pdfTableMechAssemblyStranka = {style: 'tableCategory',layout: tableStyle ,table:{widths:['*','auto','auto','auto',/* 'auto'*/], body: [
      [
        {fillColor: '#cccccc', text: 'Strojna montaža - stranka'},
        {fillColor: '#cccccc', text: 'Št. dni'},
        {fillColor: '#cccccc', text: 'Začetek'},
        {fillColor: '#cccccc', text: 'Konec'},
      ]
    ]}};
    let labelEleAssemblyRoboteh = {text: '', margin: [20, 10, 0, 0], bold: true, fontSize: 13};
    let pdfTableEleAssemblyRoboteh = {style: 'tableCategory',layout: tableStyle ,table:{widths:['*','auto','auto','auto',/* 'auto'*/], body: [
      [
        {fillColor: '#cccccc', text: 'Elektro montaža - Roboteh'},
        {fillColor: '#cccccc', text: 'Št. dni'},
        {fillColor: '#cccccc', text: 'Začetek'},
        {fillColor: '#cccccc', text: 'Konec'},
      ]
    ]}};
    let labelEleAssemblyStranka = {text: '', margin: [20, 10, 0, 0], bold: true, fontSize: 13};
    let pdfTableEleAssemblyStranka = {style: 'tableCategory',layout: tableStyle ,table:{widths:['*','auto','auto','auto',/* 'auto'*/], body: [
      [
        {fillColor: '#cccccc', text: 'Elektro montaža - stranka'},
        {fillColor: '#cccccc', text: 'Št. dni'},
        {fillColor: '#cccccc', text: 'Začetek'},
        {fillColor: '#cccccc', text: 'Konec'},
      ]
    ]}};
    let labelEleProektiranje = {text: '', margin: [20, 10, 0, 0], bold: true, fontSize: 13};
    let pdfTableEleProektiranje = {style: 'tableCategory',layout: tableStyle ,table:{widths:['*','auto','auto','auto',/* 'auto'*/], body: [
      [
        {fillColor: '#cccccc', text: 'Elektro proektiranje'},
        {fillColor: '#cccccc', text: 'Št. dni'},
        {fillColor: '#cccccc', text: 'Začetek'},
        {fillColor: '#cccccc', text: 'Konec'},
      ]
    ]}};
    let labelDobava = {text: '', margin: [20, 10, 0, 0], bold: true, fontSize: 13};
    let pdfTableDobava = {style: 'tableCategory',layout: tableStyle ,table:{widths:['*','auto','auto','auto',/* 'auto'*/], body: [
      [
        {fillColor: '#cccccc', text: 'Dobava'},
        {fillColor: '#cccccc', text: 'Št. dni'},
        {fillColor: '#cccccc', text: 'Začetek'},
        {fillColor: '#cccccc', text: 'Konec'},
      ]
    ]}};
    let labelSAT = {text: '', margin: [20, 10, 0, 0], bold: true, fontSize: 13};
    let pdfTableSAT = {style: 'tableCategory',layout: tableStyle ,table:{widths:['*','auto','auto','auto',/* 'auto'*/], body: [
      [
        {fillColor: '#cccccc', text: 'SAT'},
        {fillColor: '#cccccc', text: 'Št. dni'},
        {fillColor: '#cccccc', text: 'Začetek'},
        {fillColor: '#cccccc', text: 'Konec'},
      ]
    ]}};
    let labelFAT = {text: '', margin: [20, 10, 0, 0], bold: true, fontSize: 13};
    let pdfTableFAT = {style: 'tableCategory',layout: tableStyle ,table:{widths:['*','auto','auto','auto',/* 'auto'*/], body: [
      [
        {fillColor: '#cccccc', text: 'FAT'},
        {fillColor: '#cccccc', text: 'Št. dni'},
        {fillColor: '#cccccc', text: 'Začetek'},
        {fillColor: '#cccccc', text: 'Konec'},
      ]
    ]}};
    let labelZagon = {text: '', margin: [20, 10, 0, 0], bold: true, fontSize: 13};
    let pdfTableZagon = {style: 'tableCategory',layout: tableStyle ,table:{widths:['*','auto','auto','auto',/* 'auto'*/], body: [
      [
        {fillColor: '#cccccc', text: 'Zagon'},
        {fillColor: '#cccccc', text: 'Št. dni'},
        {fillColor: '#cccccc', text: 'Začetek'},
        {fillColor: '#cccccc', text: 'Konec'},
      ]
    ]}};
    let labelNaklad = {text: '', margin: [20, 10, 0, 0], bold: true, fontSize: 13};
    let pdfTableNaklad = {style: 'tableCategory',layout: tableStyle ,table:{widths:['*','auto','auto','auto',/* 'auto'*/], body: [
      [
        {fillColor: '#cccccc', text: 'Naklad'},
        {fillColor: '#cccccc', text: 'Št. dni'},
        {fillColor: '#cccccc', text: 'Začetek'},
        {fillColor: '#cccccc', text: 'Konec'},
      ]
    ]}};
    for(let j = 0; j < projectTasks.length; j++){
      //debugger;
      let startDate = '/';
      let finishDate = '/';
      let currentDuration = '/'
      if(projectTasks[j].task_start){
        let tmpStart = new Date(projectTasks[j].task_start).toISOString();
        startDate = tmpStart.split('T')[0].split('-')[2] + '.' + tmpStart.split('T')[0].split('-')[1] + '.' + tmpStart.split('T')[0].split('-')[0];
      }
      if(projectTasks[j].task_finish){
        let tmpFinish = new Date(projectTasks[j].task_finish).toISOString();
        finishDate = tmpFinish.split('T')[0].split('-')[2] + '.' + tmpFinish.split('T')[0].split('-')[1] + '.' + tmpFinish.split('T')[0].split('-')[0];
      }
      if(projectTasks[j].task_duration)
        currentDuration = projectTasks[j].task_duration;
      let currentTask = [
        {text: projectTasks[j].task_name},
        {text: currentDuration, noWrap: true},
        {text: startDate, noWrap: true},
        {text: finishDate, noWrap: true},
        //{text: projectTasks[j].completion + '%', noWrap: true}
      ];
      let tmpComment = projectTasks[j].task_note ? projectTasks[j].task_note : '';
      let commentRow = pdfOptionComments ? [{text: tmpComment, colSpan:4, margin:[10,-2,0,-2], fontSize: 9}] : {};
      if(projectTasks[j].category == 'Nabava'){
        pdfTableNabava.table.body.push(currentTask);
        if (pdfOptionComments && projectTasks[j].task_note)
          pdfTableNabava.table.body.push(commentRow);
      }
      else if(projectTasks[j].category == 'Konstrukcija'){
        pdfTableKonstr.table.body.push(currentTask);
        if (pdfOptionComments && projectTasks[j].task_note)
          pdfTableKonstr.table.body.push(commentRow);
      }
      else if(projectTasks[j].category == 'Strojna izdelava'){
        pdfTableStrIzdel.table.body.push(currentTask);
        if (pdfOptionComments && projectTasks[j].task_note)
          pdfTableStrIzdel.table.body.push(commentRow);
      }
      else if(projectTasks[j].category == 'Elektro izdelava'){
        pdfTableEleIzdel.table.body.push(currentTask);
        if (pdfOptionComments && projectTasks[j].task_note)
          pdfTableEleIzdel.table.body.push(commentRow);
      }
      else if(projectTasks[j].category == 'Montaža'){
        pdfTableMontaza.table.body.push(currentTask);
        if (pdfOptionComments && projectTasks[j].task_note)
          pdfTableMontaza.table.body.push(commentRow);
      }
      else if(projectTasks[j].category == 'Programiranje'){
        pdfTableProgram.table.body.push(currentTask);
        if (pdfOptionComments && projectTasks[j].task_note)
          pdfTableProgram.table.body.push(commentRow);
      }
      else if(projectTasks[j].category == 'Brez'){
        pdfTableBrez.table.body.push(currentTask);
        if (pdfOptionComments && projectTasks[j].task_note)
          pdfTableBrez.table.body.push(commentRow);
      }
      else if(projectTasks[j].category == 'Strojna montaža - Roboteh'){
        pdfTableMechAssemblyRoboteh.table.body.push(currentTask);
        if (pdfOptionComments && projectTasks[j].task_note)
          pdfTableMechAssemblyRoboteh.table.body.push(commentRow);
      }
      else if(projectTasks[j].category == 'Strojna montaža - stranka'){
        pdfTableMechAssemblyStranka.table.body.push(currentTask);
        if (pdfOptionComments && projectTasks[j].task_note)
          pdfTableMechAssemblyStranka.table.body.push(commentRow);
      }
      else if(projectTasks[j].category == 'Elektro montaža - Roboteh'){
        pdfTableEleAssemblyRoboteh.table.body.push(currentTask);
        if (pdfOptionComments && projectTasks[j].task_note)
          pdfTableEleAssemblyRoboteh.table.body.push(commentRow);
      }
      else if(projectTasks[j].category == 'Elektro montaža - stranka'){
        pdfTableEleAssemblyStranka.table.body.push(currentTask);
        if (pdfOptionComments && projectTasks[j].task_note)
          pdfTableEleAssemblyStranka.table.body.push(commentRow);
      }
      else if(projectTasks[j].category == 'Elektro proektiranje'){
        pdfTableEleProektiranje.table.body.push(currentTask);
        if (pdfOptionComments && projectTasks[j].task_note)
          pdfTableEleProektiranje.table.body.push(commentRow);
      }
      else if(projectTasks[j].category == 'Dobava'){
        pdfTableDobava.table.body.push(currentTask);
        if (pdfOptionComments && projectTasks[j].task_note)
          pdfTableDobava.table.body.push(commentRow);
      }
      else if(projectTasks[j].category == 'SAT'){
        pdfTableSAT.table.body.push(currentTask);
        if (pdfOptionComments && projectTasks[j].task_note)
          pdfTableSAT.table.body.push(commentRow);
      }
      else if(projectTasks[j].category == 'FAT'){
        pdfTableFAT.table.body.push(currentTask);
        if (pdfOptionComments && projectTasks[j].task_note)
          pdfTableFAT.table.body.push(commentRow);
      }
      else if(projectTasks[j].category == 'Zagon'){
        pdfTableZagon.table.body.push(currentTask);
        if (pdfOptionComments && projectTasks[j].task_note)
          pdfTableZagon.table.body.push(commentRow);
      }
      else if(projectTasks[j].category == 'Naklad'){
        pdfTableNaklad.table.body.push(currentTask);
        if (pdfOptionComments && projectTasks[j].task_note)
          pdfTableNaklad.table.body.push(commentRow);
      }
    }
    if(!pdfCategoryPurchase || pdfTableNabava.table.body.length <= 1){
      labelNabava = {text: ''};
      pdfTableNabava = {text: ''};
    }
    if(!pdfCategoryConstruction || pdfTableKonstr.table.body.length <= 1){
      labelKonst = {text: ''};
      pdfTableKonstr = {text: ''};
    }
    if(!pdfCategoryMechanic || pdfTableStrIzdel.table.body.length <= 1){
      labelStrIzd = {text: ''};
      pdfTableStrIzdel = {text: ''};
    }
    if(!pdfCategoryElectric || pdfTableEleIzdel.table.body.length <= 1){
      labelEleIzd = {text: ''};
      pdfTableEleIzdel = {text: ''};
    }
    if(!pdfCategoryAssembly || pdfTableMontaza.table.body.length <= 1){
      labelMontaza = {text: ''};
      pdfTableMontaza = {text: ''};
    }
    if(!pdfCategoryProgram || pdfTableProgram.table.body.length <= 1){
      labelProg = {text: ''};
      pdfTableProgram = {text: ''};
    }
    if(!pdfCategoryNone || pdfTableBrez.table.body.length <= 1){
      labelBrez = {text: ''};
      pdfTableBrez = {text: ''};
    }
    if(!pdfCategoryMechAssemblyRoboteh || pdfTableMechAssemblyRoboteh.table.body.length <= 1){
      labelMechAssemblyRoboteh = {text: ''};
      pdfTableMechAssemblyRoboteh = {text: ''};
    }
    if(!pdfCategoryMechAssemblyStranka || pdfTableMechAssemblyStranka.table.body.length <= 1){
      labelMechAssemblyStranka = {text: ''};
      pdfTableMechAssemblyStranka = {text: ''};
    }
    if(!pdfCategoryEleAssemblyRoboteh || pdfTableEleAssemblyRoboteh.table.body.length <= 1){
      labelEleAssemblyRoboteh = {text: ''};
      pdfTableEleAssemblyRoboteh = {text: ''};
    }
    if(!pdfCategoryEleAssemblyStranka || pdfTableEleAssemblyStranka.table.body.length <= 1){
      labelEleAssemblyStranka = {text: ''};
      pdfTableEleAssemblyStranka = {text: ''};
    }
    if(!pdfCategoryEleProektiranje || pdfTableEleProektiranje.table.body.length <= 1){
      labelEleProektiranje = {text: ''};
      pdfTableEleProektiranje = {text: ''};
    }
    if(!pdfCategoryDobava || pdfTableDobava.table.body.length <= 1){
      labelDobava = {text: ''};
      pdfTableDobava = {text: ''};
    }
    if(!pdfCategorySAT || pdfTableSAT.table.body.length <= 1){
      labelSAT = {text: ''};
      pdfTableSAT = {text: ''};
    }
    if(!pdfCategoryFAT || pdfTableFAT.table.body.length <= 1){
      labelFAT = {text: ''};
      pdfTableFAT = {text: ''};
    }
    if(!pdfCategoryZagon || pdfTableZagon.table.body.length <= 1){
      labelZagon = {text: ''};
      pdfTableZagon = {text: ''};
    }
    if(!pdfCategoryNaklad || pdfTableNaklad.table.body.length <= 1){
      labelNaklad = {text: ''};
      pdfTableNaklad = {text: ''};
    }

    let marginForInfo = -55;
    let fileLocation = projectInfo.icon ? `${__dirname}/../public/uploads/images/`+projectInfo.icon : undefined;
    let imageWidth = 152;
    if (projectInfo.icon && fs.existsSync(fileLocation)){
      Jimp.read(fileLocation).then(img => {
        let tmpRatio = img.getWidth()/img.getHeight();
        marginForInfo = -60 + (105 - (150 / tmpRatio));
        imageWidth = Math.ceil(tmpRatio * 45.5);
        //debugger;
        img.resize(300, Jimp.AUTO).getBase64Async(Jimp.AUTO).then(base64 => {
          // return res.json({success:true, data: base64, id: fileId, type: 'IMG'});
          // newFile.base64 = base64;
          // return res.status(200).json({
          //   status: 'success',
          //   file: newFile,
          //   success: 'true',
          //   message: msg,
          // });
          let imagePart = {
            // border: [false,false,false,false],
            image: base64,
            alignment: 'center',
            width: imageWidth
          };
          if (!pdfOptionLogo) imagePart = {};
          let docDefinition = {
            info: {
              title: number,
              author: 'Roboteh-IS',
              subject: 'Info about project',
              keywords: number + ", " + projectInfo.name,
            },
            pageSize: 'A4',
            pageOrientation: 'portrait',
            pageMargins: [60, 85, 50, 70],
            header: [
              {
                style: 'headerTable',
                table: {
                  widths: [150,35,280],
                  body: [
                    [
                      {border: [false,false,false,true],image: base64RobotehImg.base64,width: 150},
                      {border: [false,false,false,true],image: base64KukaPartnerImg.base64,width: 35},
                      {border: [false,false,false,true],margin: [ 0, 33, 0, 0 ],alignment: 'right',fontSize: 10,text: [number,],},
                    ]
                  ]
                },layout: {defaultBorder: true,}
              },
            ],
            content: [
              {
                style: 'projectTable',
                table: {
                  widths: [150,340],
                  body: [
                    [
                      imagePart,
                      {
                        style: 'projectTableInfo',
                        table: {
                          widths: [335],
                          body: [
                            [{border: [false,false,false,false],text: ['Naziv projekta:    ',{text: number, bold: true},],},],
                            [{border: [false,false,false,false],text: ['Naročnik:    ',{text: projectInfo.name, bold: true},],},],
                            [{border: [false,false,false,false],text: [{text: date, bold: false},],},],
                          ]
                        },
                      },
                    ]
                  ]
                },layout: {defaultBorder: false,}
              },
              labelNote,
              pdfSectionNote,
              labelProjectDates,
              pdfTableProjectDates,
              marginProjectDates,
              pdfTable,
              labelNabava,
              pdfTableNabava,
              labelKonst,
              pdfTableKonstr,
              labelStrIzd,
              pdfTableStrIzdel,
              labelEleIzd,
              pdfTableEleIzdel,
              labelMontaza,
              pdfTableMontaza,
              labelProg,
              pdfTableProgram,
              labelBrez,
              pdfTableBrez,
              labelMechAssemblyRoboteh,
              pdfTableMechAssemblyRoboteh,
              labelMechAssemblyStranka,
              pdfTableMechAssemblyStranka,
              labelEleAssemblyRoboteh,
              pdfTableEleAssemblyRoboteh,
              labelEleAssemblyStranka,
              pdfTableEleAssemblyStranka,
              labelEleProektiranje,
              pdfTableEleProektiranje,
              labelDobava,
              pdfTableDobava,
              labelSAT,
              pdfTableSAT,
              labelFAT,
              pdfTableFAT,
              labelZagon,
              pdfTableZagon,
              labelNaklad,
              pdfTableNaklad,
            ],
            footer: [
              {
                style: 'footerTable',
                table: {
                  widths: [80,400],
                  body: [[{border: [false,true,false,false],text: labelTodayDate,},{border: [false,true,false,false],text: number,alignment: 'right'},],]
                },layout: {}
              },
            ],
            styles: {
              header: {fontSize: 18,bold: true,alignment: 'right',margin: [0, 190, 0, 80]},
              subheader: {fontSize: 14},
              superMargin: {margin: [20, 0, 40, 0],fontSize: 15},
              tableCategory: {margin: [0, 0, 0, 0]},
              info: {fontSize: 18,bold: true,margin: [180, marginForInfo, 0, 20],},
              additionalInfo: {fontSize: 13,},
              logo: {margin: [0, -30, 0, 0],},
              projectTable: {heights: 100,margin: [0, 0, 0, 0]},
              projectTableInfo: {fontSize: 12,margin: [ 0,0, 0, 0 ]},
              footerTable: {fontSize: 9,margin: [59, 35, 0, 0],},
              headerTable: {heights: 100,margin: [57, 5, 0, 0]},
            }
          };
          let pdfDoc = printer.createPdfKitDocument(docDefinition);
          const filename = encodeURI(number);
          res.setHeader('Content-disposition', `attachment; filename*=UTF-8''${filename}.pdf; filename=${filename}.pdf`);
          res.setHeader('Content-type', 'application/pdf');
          pdfDoc.pipe(res);
          pdfDoc.end();
        })
        .catch(err => {
          // return res.json({success:false, error: err});
          next(e);
        });
      })
      .catch((e) => {
        // return res.json({success:false, error: e, msg: 'Datoteka ne obstaja.', id: fileId, type: fileType});
        next(e);
      })
    }
    else{
      let docDefinition = {
        info: {
          title: number,
          author: 'Roboteh-IS',
          subject: 'Info about project',
          keywords: number + ", " + projectInfo.name,
        },
        pageSize: 'A4',
        pageOrientation: 'portrait',
        pageMargins: [60, 85, 50, 70],
        header: [
          {
            style: 'headerTable',
            table: {
              widths: [150,35,280],
              body: [
                [
                  { border: [false,false,false,true],image: base64RobotehImg.base64,width: 150},
                  { border: [false,false,false,true],image: base64KukaPartnerImg.base64,width: 35},
                  { border: [false,false,false,true],margin: [ 0, 33, 0, 0 ],alignment: 'right',fontSize: 10,text: [number,],},
                ]
              ]
            },
            layout: {defaultBorder: true,}
          },
        ],
        content: [
          {
            style: 'projectTable',
            table: {
              widths: [150,340],
              body: [
                [
                  {
                    // border: [false,false,false,false],
                    // image: base64,
                    // alignment: 'center',
                    // width: imageWidth
                  },
                  {
                    style: 'projectTableInfo',
                    table: {
                      widths: [335],
                      body: [
                        [{border: [false,false,false,false],text: ['Naziv projekta:    ',{text: number, bold: true},],},],
                        [{border: [false,false,false,false],text: ['Naročnik:    ',{text: projectInfo.name, bold: true},],},],
                        [{border: [false,false,false,false],text: [{text: date, bold: false},],},],
                      ]
                    },
                  },
                ]
              ]
            },
            layout: {
              defaultBorder: false,
            }
          },
          labelNote,
          pdfSectionNote,
          labelProjectDates,
          pdfTableProjectDates,
          marginProjectDates,
          pdfTable,
          labelNabava,
          pdfTableNabava,
          labelKonst,
          pdfTableKonstr,
          labelStrIzd,
          pdfTableStrIzdel,
          labelEleIzd,
          pdfTableEleIzdel,
          labelMontaza,
          pdfTableMontaza,
          labelProg,
          pdfTableProgram,
          labelBrez,
          pdfTableBrez,
          labelMechAssemblyRoboteh,
          pdfTableMechAssemblyRoboteh,
          labelMechAssemblyStranka,
          pdfTableMechAssemblyStranka,
          labelEleAssemblyRoboteh,
          pdfTableEleAssemblyRoboteh,
          labelEleAssemblyStranka,
          pdfTableEleAssemblyStranka,
          labelEleProektiranje,
          pdfTableEleProektiranje,
          labelDobava,
          pdfTableDobava,
          labelSAT,
          pdfTableSAT,
          labelFAT,
          pdfTableFAT,
          labelZagon,
          pdfTableZagon,
          labelNaklad,
          pdfTableNaklad,
        ],
        footer: [{
          style: 'footerTable',
          table: {
            widths: [480],
            body: [[{border: [false,true,false,false],text: number,alignment: 'right'},],]
          },
          layout: {}
        },],
        styles: {
          header: {fontSize: 18,bold: true,alignment: 'right',margin: [0, 190, 0, 80]},
          subheader: {fontSize: 14},
          superMargin: {margin: [20, 0, 40, 0],fontSize: 15},
          tableCategory: {margin: [0, 0, 0, 0]},
          info: {fontSize: 18,bold: true,margin: [180, marginForInfo, 0, 20],},
          additionalInfo: {fontSize: 13,},
          logo: {margin: [0, 0, 0, 0],},
          projectTable: {heights: 100,margin: [0, 0, 0, 0]},
          projectTableInfo: {fontSize: 12,margin: [ 0,0, 0, 0 ]},
          footerTable: {fontSize: 9,margin: [59, 35, 0, 0],},
          headerTable: {heights: 100,margin: [57, 5, 0, 0]},
        }
      };
      let pdfDoc = printer.createPdfKitDocument(docDefinition);
      const filename = encodeURI(number);
      res.setHeader('Content-disposition', `attachment; filename*=UTF-8''${filename}.pdf; filename=${filename}.pdf`);
      res.setHeader('Content-type', 'application/pdf');
      pdfDoc.pipe(res);
      pdfDoc.end();
    }
    
    //res.json({success:false, error:'not implemented yet, contact RIS administrator', msg:'Funkcionalnost še ni implementirana. Prosim kontaktirajte administratorja RIS-a.'})
  })
  .catch(e => {
    next(e);
  })
})
//get build part documentaion pdf
router.post('/buildparts', function(req, res, next){
  let allActiveBuildParts = req.body.allActiveBuildParts;
  let docBPProject = req.body.docBPProject;
  let docBPGiver = req.body.docBPGiver;
  let docBPTaker = req.body.docBPTaker;
  let docBPLeader = req.body.docBPLeader;
  let docBPCount = req.body.docBPCount;
  let givenDateCheck  = (req.body.givenDateCheck == true || req.body.givenDateCheck == false) ? req.body.givenDateCheck : true;
  let takenDateCheck = (req.body.takenDateCheck != true || req.body.takenDateCheck != false) ? req.body.takenDateCheck : true;
  let viewDateCheck = (req.body.viewDateCheck != true || req.body.viewDateCheck != false) ? req.body.viewDateCheck : true;
  let givenDate  = req.body.givenDate && givenDateCheck ? req.body.givenDate : '';
  let takenDate = req.body.takenDate && takenDateCheck ? req.body.takenDate : '';
  let viewDate = req.body.viewDate && viewDateCheck ? req.body.viewDate : '';
  let customSortBP = req.body.customSortBP ? req.body.customSortBP : false;

  let projectColSpan = 3;
  let countObject = {};
  if(docBPCount){
    projectColSpan = 2;
    countObject = {"text":"Količina: "+docBPCount, "alignment":"right"};
  }
  
  Promise.all([dbBase64Images.getKukaImage(),dbBase64Images.getRobotehImage()])
  .then(([base64KukaImg, base64RobotehImg]) => {
    let dd = {
      pageMargins: [40, 80, 40, 60],
      header: [{
        style: 'tableKuka',
        table: {
          widths: [375, 200],
          body: [[
              {image: 'roboteh',width: 180},
              {margin: [0, 10, 0, 0], image: 'kuka', width: 170},
          ]]
        }, layout: 'noBorders'
      },],
      content: [
        {margin: [0, 0, 0, 10], text: 'Predaja dokumentacije v proizvodnjo', decoration: 'underline', style: 'header'},
        {
          style: 'infoTable',
          table: {
            widths: [70, 200, 150, 100],
            body: [
              [
                {text: 'Projekt:'},
                {colSpan: projectColSpan, text: docBPProject},
                {},
                countObject,
              ],
              [
                {text: 'Predal:'},
                {text: docBPGiver},
                {text: 'Datum predaje:', alignment: 'right'},
                {text: givenDate, alignment: 'right'},
              ],
              [
                {text: 'Prevzel:'},
                {text: docBPTaker},
                {text: 'Datum prevzema:', alignment: 'right'},
                {text: takenDate, alignment: 'right'},
              ],
              [
                {text: 'Vodja projekta:'},
                {text: docBPLeader},
                {text: 'Datum pregleda:', alignment: 'right'},
                {text: viewDate, alignment: 'right'},
              ],
            ]
          }, layout: {fillColor: '#eeeeee',}
        },
        {
          style: 'buildPartTable',
          table: {
            widths: [70, 200, 150, 100],
            body: [
              [
                {text: 'Pozicija:',bold: true},
                {text: 'Opis:', bold: true},
                {text: 'Številka risbe:', bold: true},
                {text: 'Opomba:', bold: true},
              ],
            ]
          },
          layout: {fillColor: function (rowIndex, node, columnIndex) { return (rowIndex % 2 === 0) ? null : '#DDDDDD';}}
        },
        {
          style: 'signTable',
          table: {
            widths: [240, 30, 220, 30],
            body: [
              [
                { border: [false, false, false, false], colSpan: 4, text: 'Dokumentacijo:',},
                {},
                {},
                {},
              ],
              [
                {border: [false, false, false, false], text: 'Prevzel:'},
                {border: [false, false, false, false], text: ' '},
                {border: [false, false, false, false], text: 'Oddal:'},
                {border: [false, false, false, false], text: ' '},
              ],
              [
                {border: [false, false, false, true], text: ' '},
                {border: [false, false, false, false], text: ' '},
                {border: [false, false, false, true], text: ' '},
                {border: [false, false, false, false], text: ' '},
              ],
            ]
          },
        },
      ],
      images: {
        roboteh: base64RobotehImg.base64,
        kuka: base64KukaImg.base64,
      },
      footer: [
        {
          style: 'footerTable',
          table: {
            widths: [547],
            body: [
              [{border: [false, false, false, false], text: 'ROBOTEH d.o.o., Goričica 2B,  SI-3230 Šentjur, Tel: +386 / 0599-39-443', alignment: 'center'},],
              [{border: [false, false, false, false], text: 'E-mail: roboteh.online@gmail.com, Web: www.roboteh.si', alignment: 'center'},],
            ]
          },
          layout: {}
        },
      ],
      styles: {
        header: {fontSize: 16,alignment: 'center',bold: true,margin: [0, 10, 0, 0]},
        footer: {fontSize: 11,margin: [0, 0, 0, 0]},
        footerTable: {fontSize: 10,margin: [20, 15, 0, 0],},
        tableKuka: {heights: 100,margin: [20, 10, 0, 0]},
        signTable: {fontSize: 10,margin: [-20, 0, 0, 0],},
        infoTable: {fontSize: 10,margin: [-20, 0, 0, 0],},
        buildPartTable: {fontSize: 10,margin: [-20, 15, 0, 0],},
      },
    };
    
    if (!customSortBP){
      // normal build parts add to table (sorted by standard on client side)
      for(let i = 0; i < allActiveBuildParts.length; i++){
        //var position = (allActiveBuildParts[i].position) ? allActiveBuildParts[i].position.toString() : ' ';
        let name = (allActiveBuildParts[i].name) ? allActiveBuildParts[i].name : ' ';
        let standard = (allActiveBuildParts[i].standard) ? allActiveBuildParts[i].standard : ' ';
        let note = (allActiveBuildParts[i].note) ? allActiveBuildParts[i].note : ' ';
        let row = [{text: ((i+1)+''), alignment: 'center'}, {text: name}, {text: standard}, {text: note}];
        if(allActiveBuildParts[i].sestav){
          dd.content[2].table.body.push([{text: ((i+1)+''), alignment: 'center', bold:true, fillColor:"#BBBBBB"}, {text: name, bold:true, fillColor:"#BBBBBB"}, {text: standard, bold:true, fillColor:"#BBBBBB"}, {text: note, bold:true, fillColor:"#BBBBBB"}]);
          dd.content[2].table.body.push([{text: ''}, {text: ''}, {text: ''}, {text: ''}]);
        }
        else
          dd.content[2].table.body.push(row);
      }
    }
    else{
      // custom sort build parts, add to table same as normal and add empty rows when there are and not when sestav begins
      let i = 1;
      for (const buildPart of allActiveBuildParts) {
        if (buildPart.id){
          let name = (buildPart.name) ? buildPart.name : ' ';
          let standard = (buildPart.standard) ? buildPart.standard : ' ';
          let note = (buildPart.note) ? buildPart.note : ' ';
          let row = buildPart.sestav ? [{text: ((i++)+''), alignment: 'center', bold:true, fillColor:"#BBBBBB"}, {text: name, bold:true, fillColor:"#BBBBBB"}, {text: standard, bold:true, fillColor:"#BBBBBB"}, {text: note, bold:true, fillColor:"#BBBBBB"}] : [{text: ((i++)+''), alignment: 'center'}, {text: name}, {text: standard}, {text: note}];
          dd.content[2].table.body.push(row);
        }
        else
          dd.content[2].table.body.push([{text: ''}, {text: ''}, {text: ''}, {text: ''}]);
      }
    }
    
    let pdfDoc = printer.createPdfKitDocument(dd);
    const filename = encodeURI('Predaja - ' + docBPProject);
    res.setHeader('Content-disposition', `attachment; filename*=''${filename}.pdf; filename=${filename}.pdf`);
    res.setHeader('Content-type', 'blob');
    pdfDoc.pipe(res);
    pdfDoc.end();
  })
  .catch(e => {
    next(e);
  })

})

module.exports = router;