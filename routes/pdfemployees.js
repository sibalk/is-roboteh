var express = require('express');
var router = express.Router();

// Define font files
var fonts = {
  Roboto: {
    normal: `${__dirname}/../public/pdfmake/Roboto-Regular.ttf`,
    bold: `${__dirname}/../public/pdfmake/Roboto-Medium.ttf`,
    italics: `${__dirname}/../public/pdfmake/Roboto-Italic.ttf`,
    bolditalics: `${__dirname}/../public/pdfmake/Roboto-MediumItalic.ttf`,
  }
};
var PdfPrinter = require('pdfmake');
var printer = new PdfPrinter(fonts);

let dbUsers = require('../model/employees/dbEmployees');
let dbBase64Images = require('../model/base64images/base64images');

////////////////////////////FUNCTIONS
function compareValues(key, order = 'asc') {
  return function innerSort(a, b) {
    if (!a.hasOwnProperty(key) || !b.hasOwnProperty(key)) {
      // property doesn't exist on either object
      return 0;
    }

    const varA = (typeof a[key] === 'string')
      ? a[key].toUpperCase() : a[key];
    const varB = (typeof b[key] === 'string')
      ? b[key].toUpperCase() : b[key];

    let comparison = 0;
    if (varA > varB) {
      comparison = 1;
    } else if (varA < varB) {
      comparison = -1;
    }
    return (
      (order === 'desc') ? (comparison * -1) : comparison
    );
  };
}
////////////////////////////ROUTES

//return pdf for list of projects based on filters (default: all active projects)
router.get('/', function(req, res, next){
  //res.json({success:false, error:'not implemented yet, contact RIS administrator', msg:'Funkcionalnost še ni implementirana. Prosim kontaktirajte administratorja RIS-a.'})
  Promise.all([dbUsers.getUsers(1),dbBase64Images.getRobotehImage()])
  .then(([users,base64RobotehImg])=>{
    users.sort(compareValues('surname'))
    //make pdf defenition
    let today = new Date();
    let pdfHeader = 'Seznam zaposlenih - ' + (today.getDate()+'. '+(today.getMonth()+1)+'. '+today.getFullYear());
    var pdfTableEmployees = {
			style: 'tableExample',
			table: {
				widths: ['*', '*', '*'],
				body: [
					[
            'Ime',
            'Priimek',
            'Vloga',
          ],
				]
			},
      layout: {
				fillColor: function (rowIndex, node, columnIndex) {
				    if(rowIndex === 0) return '#CCCCCC'
				    else if(rowIndex % 2 === 0) return '#e8e8e8';
				}
			}
		}
    for (const user of users) {
      let userRow = [
        {text: user.name},
        {text: user.surname},
        {text: user.role},
      ];
      pdfTableEmployees.table.body.push(userRow);
    }
    //res.json({success:true, data:projects});
    var docDefinition = {
      info: {
        title: pdfHeader,
        author: 'Roboteh-Informacijski-Sistem',
        subject: 'List of all employees',
      },
      pageSize: 'A4',
      pageOrientation: 'portrait',
      content: [
        {
          style: 'tableRoboteh',
          table: {
            widths: [110,'*',110],
            body: [[
              {image: 'roboteh',width: 110},
              {text: 'Seznam zaposlenih',alignment: 'center',fontSize: 22},
              {text: (today.getDate()+'. '+(today.getMonth()+1)+'. '+today.getFullYear()),alignment: 'right'},
            ]],
          }, layout: 'noBorders'
        },
        pdfTableEmployees,
      ],
      images: {
        roboteh: base64RobotehImg.base64,
      },
      styles: {
        tableRoboteh: {margin: [0, -15, 0, 15]},
      }
    };
    let pdfDoc = printer.createPdfKitDocument(docDefinition);
    const filename = encodeURI(pdfHeader);
    res.setHeader('Content-disposition', `attachment; filename*=UTF-8''${filename}.pdf; filename=${filename}.pdf`);
    res.setHeader('Content-type', 'application/pdf');
    pdfDoc.pipe(res);
    pdfDoc.end();
  })
  .catch((e)=>{
    //return res.json({success:false, error: e});
    next(e);
  })
})
module.exports = router;