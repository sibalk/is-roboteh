const router = require('express').Router();

router.get('/', (req, res, next) => {
    res.render('index', {
        title: 'Node Express Example'
    });
});

module.exports = router;