var express = require('express');
var router = express.Router();

//var auth = require('../../controllers/authentication');
let dbWorkOrders = require('../../model/apiv2/workorders/dbWorkOrdersAPI');
let dbSubscribers = require('../../model/apiv2/subscribers/dbSubscribersAPI');
let dbChanges = require('../../model/changes/dbChanges');

//get user projects (projects where users is part of)
router.get('/', function(req,res, next){
  //get token to verify user and get his userId
  dbSubscribers.getSubscribersAPI().then(subscribers => {
    res.json({subscribers:subscribers});//200
  })
  .catch((e)=>{
    console.log(e);
    return res.status(500).json({
      status: 'error',
      message: 'Something went wrong',
      error: e,
    })
  })
});
//get user projects (projects where users is part of)
router.get('/:id', function(req,res, next){
  //get token to verify user and get his userId
  let subscriberId = req.params.id;
  dbSubscribers.getSubscriberAPI(subscriberId).then(subscriber => {
    res.json({subscriber:subscriber[0]});//200
  })
  .catch((e)=>{
    console.log(e);
    return res.status(500).json({
      status: 'error',
      message: 'Something went wrong',
      error: e,
    })
  })
});
//add new subscriber and return new id (projects where users is part of)
router.post('/', function(req,res, next){
  //get token to verify user and get his userId
  let userId = res.locals.oauth.token.user.id;
  let name = req.body.name;
  dbSubscribers.addSubscribersAPI(name).then(subscribers => {
    //add change to db -> change system
    dbChanges.addNewChangeSystem(1,28,userId,null,null,null,null,null,null,subscribers[0].id).then(sysChange => {
      console.log("Uporabnik " + userId + " je naredil spremembo tipa 28 s statusom 1 za naročnika " + subscribers[0].id + ".");
      return res.json({subscriber:subscribers});//200
    })
    .catch((e) => {
      return res.json({success:false, error:e});
    })
  })
  .catch((e)=>{
    console.log(e);
    return res.status(500).json({
      status: 'error',
      message: 'Something went wrong',
      error: e,
    })
  })
});
//get all user work orders for selected subscriber id
router.get('/workorders/:id', function(req,res){
  //get token to verify user and get his userId
  //All report data (client, datetime, type:servis;montaža;ogled …, descriptions, location (JSON field - longitude/langitude), images (JSON)
  let userId = res.locals.oauth.token.user.id;
  let subscriberId = req.params.id;
  dbWorkOrders.getUserWorkOrdersForSubscriberAPI(userId,subscriberId).then(workOrders => {
    res.json({workOrders:workOrders});//200
  })
  .catch((e)=>{
    console.log(e);
    return res.status(500).json({
      status: 'error',
      message: 'Something went wrong',
      error: e,
    })
  })
})
module.exports = router;