var express = require('express');
var router = express.Router();
const OAuthServer = require('express-oauth-server');
var OAuthModel = require('../../model/oauth/model');

let oauth = new OAuthServer({
  model: OAuthModel,
  debug: true
});

//var dbProjects = require('../model/API/projects/dbProjectsAPI');

var apiProjects = require('./projects');
var apiSubscribers = require('./subscribers');
var apiWorkOrders = require('./workorders');
var apiReports = require('./reports');
var apiUsers = require('./users');
//var apiImages = require('./image');
var apiOAuth = require('./oauth');

///////////////////////////////////////////////////////////////////////////////////////OAUTH middleware
router.use('/oauth', apiOAuth);
router.use(require('./public'));

///////////////////////////////////////////////////////////////////////////////////////REPORTS
router.use('/reports', oauth.authenticate(), apiReports);
///////////////////////////////////////////////////////////////////////////////////////PROJECTS
router.use('/projects', oauth.authenticate(), apiProjects);
///////////////////////////////////////////////////////////////////////////////////////SUBSCRIBERS
router.use('/subscribers', oauth.authenticate(), apiSubscribers);
///////////////////////////////////////////////////////////////////////////////////////REPORTS
router.use('/workorders', oauth.authenticate(), apiWorkOrders);
///////////////////////////////////////////////////////////////////////////////////////REPORTS
router.use('/users', oauth.authenticate(), apiUsers);
///////////////////////////////////////////////////////////////////////////////////////FILE?FOTO
module.exports = router;