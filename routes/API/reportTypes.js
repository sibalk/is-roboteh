var express = require('express');
var router = express.Router();

var auth = require('../../controllers/authentication');
var dbReports = require('../../model/apiv2/reports/dbReportsAPI');
//get all report info for specific report id
router.get('/:id', auth.authenticate, function(req,res){
  //get token to verify user and get his userId
  //All report data (client, datetime, type:servis;montaža;ogled …, descriptions, location (JSON field - longitude/langitude), images (JSON)
  dbReports.getReportsTypeAPI().then(reportTypes => {
    res.json({reportTypes:reportTypes});//200
  })
  .catch((e)=>{
    console.log(e);
    return res.status(500).json({
      status: 'error',
      message: 'Something went wrong',
      error: e,
    })
  })
})
module.exports = router;