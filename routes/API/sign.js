var express = require('express');
var router = express.Router();
var multer = require('multer');
var fs = require('fs');

let dbWorkOrderSigns = require('../../model/apiv2/workordersigns/dbWorkOrderSignsAPI');
let dbChanges = require('../../model/changes/dbChanges');

//storage for documents
var storageDoc = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, 'files/workorders/signs/')
  },
  filename: function (req, file, cb) {        
    // null as first argument means no error
    cb(null, Date.now() + '-' + file.originalname )
  }
});
//upload when adding subscriber image
var uploadDoc = multer({
  storage: storageDoc,
  limits: {
    fileSize: 5300000
  },
  fileFilter: function(req, file, cb){
    sanitizeFileDocument(file,cb);
  }
}).single('file');
//function to check if file is indeed document
function sanitizeFileDocument(file, cb){
  //what file extentions are ok
  let fileExts = ['jpg', 'jpeg', 'png', 'gif']; //images
                //access has way diffrent ext and they probably wont use them
                //other file ext will be added when there will be request
  // MAYBE TODO add isAlowedMimeType back and test for all this type of extentions
  //check if file has no exts
  let fileExtsArray = file.originalname.split(".");
  if(fileExtsArray.length == 1)
    return cb('Datoteka brez končnice ni dovoljena');
  //check alowed exts
  let isAlowedExt = fileExts.includes(file.originalname.split(".")[1].toLowerCase());
  //mime type must be an image
  //let isAlowedMimeType = file.mimetype.startsWith("document/");

  if(isAlowedExt){
    //no errors
    return cb(null, true);
  }
  else{
    //error, not an image
    cb('Ta vrsta datoteke ni dovoljena!');
  }
}
//upload for new file
router.post('/', (req, res, next) => {
  //debugger;
  //save file and if no error send back json success true
  let userId = res.locals.oauth.token.user.id;
  uploadDoc(req, res, (err) =>{
    if(err){
      return res.status(500).json({
        status: 'error',
        message: 'Something went wrong',
        error: err,
      })
    }
    else{
      //file not selected
      if(req.file == undefined){
        return res.status(400).json({
          status: 'error',
          message: 'No file was send',
        })
      }
      else{
        let type = "";
        let fileExtsIMG = ['jpg', 'jpeg', 'png', 'gif'];
        let isAlowedExtIMG = fileExtsIMG.includes(req.file.originalname.split(".")[1].toLocaleLowerCase());
        if(isAlowedExtIMG) type = 'IMG';

        //console.log("Uporabnik " + userId + " je uspešno naložil datoteko " +req.file.originalname + ". ");
        //success add new subscriber with image name
        dbWorkOrderSigns.addFileForUnknownWorkOrder(req.file.originalname, req.file.filename, userId, type)
        .then(image =>{
          //add change to db -> change system
          dbChanges.addNewChangeSystem(10,29,userId,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,image.id)
          .then(sysChange => {
            console.log("Uporabnik " + userId + " je nalozil podpis " + image.id + " za delovne naloge.");
            return res.json({image:image});
          })
          .catch((e)=>{
            res.json({success:false, error:e})
          })
        })
        .catch((e)=>{
          console.log(e);
          return res.status(500).json({
            status: 'error',
            message: 'Something went wrong',
            error: e,
          })
        })
      }
    }
  })
});

router.get('/', function(req, res){
  //console.log('send him doc');
  //console.log(req.query.filename);
  let filename = req.query.filename;
  let fileId = req.query.signId;
  if(!filename || !fileId)
    res.sendStatus(400);
  else{
    console.log('Zahteva za datoteko: '+ filename);
    var file = `${__dirname}/../../files/workorders/signs/${filename}`;
    if (fs.existsSync(file)) {
      // Do something
      res.download(file, function(err){
        if(err){
          console.error(err);
        }
      });
    }
    else{
      dbWorkOrderSigns.updateActiveFile(filename)
        .then(file=>{
          console.log("Datoteka "+filename+" ne obstaja več, aktivnost v bazi popravljena.")
          let msg = "Napaka: Datoteka ne obstaja več!";
          return res.status(500).json({
            status: 'error',
            message: msg,
          })
        })
        .catch((e)=>{
          return res.status(500).json({
            status: 'error',
            message: 'Something went wrong',
            error: e,
          })
        })
    }
  }
})
module.exports = router;