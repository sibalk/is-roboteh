var express = require('express');
var router = express.Router();
const bcrypt = require('bcryptjs')
const OAuthServer = require('express-oauth-server');
var OAuthModel = require('../../model/oauth/model');

let oauth = new OAuthServer({
  model: OAuthModel,
  debug: true
});

router.get('/authenticate', (req, res, next) => {
  return res.render('authenticate')
});

router.post('/authenticate', async (req, res, next) => {
  let username = req.body.username;
  let password = req.body.password;
  user = OAuthModel.getUser(username).then(user => {
    if(user && bcrypt.compareSync(password, user.password)){
      req.oauthUser = user;
      return next();
    }
    return res.status(401).json({
      status: 'error',
      message: 'Incorrect username or password',
    })
  })
  .catch(e =>{
    return res.status(500).json({
      status: 'error',
      message: e,
    })
  })
}, oauth.authorize({
  authenticateHandler: {
      handle: req => {
          return req.oauthUser;
      }
  }
}));

router.post('/access_token', (req, res, next)=>{
  if(req.body && req.body.grant_type == 'password'){
    let username = req.body.username;
    let password = req.body.password;
    req.body.scope = 'read-write';
    user = OAuthModel.getUser(username).then(user => {
      if(user && bcrypt.compareSync(password, user.password)){
        req.oauthUser = user;
        return next();
      }
      return res.status(401).json({
        status: 'error',
        message: 'Incorrect username or password',
      })
    })
    .catch(e =>{
      return res.status(500).json({
        status: 'error',
        message: e,
      })
    })
  }
  else
    next()
})

router.post('/access_token', oauth.token({
  refreshTokenLifetime: 100000,
  requireClientAuthentication: {
      authorization_code: false,
      refresh_token: false
  }
}));
module.exports = router;