var express = require('express');
var router = express.Router();

var dbUsers = require('../../model/apiv2/users/dbUsersAPI');

//get user projects (projects where users is part of)
router.get('/', function(req,res, next){
  //get token to verify user and get his userId
  dbUsers.getUsersAPI().then(users => {
    res.json({users:users});//200
  })
  .catch((e)=>{
    console.log(e);
    return res.status(500).json({
      status: 'error',
      message: 'Something went wrong',
      error: e,
    })
  })
});
//get user projects (projects where users is part of)
router.get('/:id', function(req,res, next){
  //get token to verify user and get his userId
  let userId = req.params.id;
  dbUsers.getUserAPI(userId).then(user => {
    res.json({user:user});//200
  })
  .catch((e)=>{
    console.log(e);
    return res.status(500).json({
      status: 'error',
      message: 'Something went wrong',
      error: e,
    })
  })
});
module.exports = router;