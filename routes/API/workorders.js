var express = require('express');
var router = express.Router();
let nodemailer = require('nodemailer');
var fs = require('fs');
var dateFormat = require('dateformat');

let dbWorkOrders = require('../../model/apiv2/workorders/dbWorkOrdersAPI');
let dbWorkOrdersOG = require('../../model/workOrders/dbWorkOrders');
let dbChanges = require('../../model/changes/dbChanges');
let dbBase64Images = require('../../model/base64images/base64images');
var apiImages = require('./image');
var apiSigns = require('./sign');

///////////////////////////////////////////////////////////////////////////////////////FILE?FOTO
router.use('/files', apiImages);
router.use('/signs', apiSigns);

//get all types for work orders
router.get('/types', function(req,res){
  //get token to verify user and get his userId
  //All report data (client, datetime, type:servis;montaža;ogled …, descriptions, location (JSON field - longitude/langitude), images (JSON)
  dbWorkOrders.getWorkOrderTypesAPI().then(types => {
    res.json({types:types});//200
  })
  .catch((e)=>{
    console.log(e);
    return res.status(500).json({
      status: 'error',
      message: 'Something went wrong',
      error: e,
    })
  })
})
////////////////////////////////////////////////////////////////////////MAILING PART
// Define font files
var fonts = {
  Roboto: {
    normal: `${__dirname}/../../public/pdfmake/Roboto-Regular.ttf`,
    bold: `${__dirname}/../../public/pdfmake/Roboto-Medium.ttf`,
    italics: `${__dirname}/../../public/pdfmake/Roboto-Italic.ttf`,
    bolditalics: `${__dirname}/../../public/pdfmake/Roboto-MediumItalic.ttf`,
  }
};
var PdfPrinter = require('pdfmake');
var printer = new PdfPrinter(fonts);
//send mail
let transporter;
if(process.env.MAIL_HOST){
  transporter = nodemailer.createTransport({
    host: process.env.MAIL_HOST,
    port: process.env.MAIL_PORT,
    secure: true,
    auth: {
      user: process.env.MAIL_USERNAME,
      pass: process.env.MAIL_PASSWORD
    },
    tls: {
      // do not fail on invalid certs
      rejectUnauthorized: false
    }
  });
}
if(process.env.ETHEREAL_HOST){
  transporter = nodemailer.createTransport({
    host: process.env.ETHEREAL_HOST,
    port: process.env.ETHEREAL_PORT,
    auth: {
      user: process.env.ETHEREAL_USERNAME,
      pass: process.env.ETHEREAL_PASSWORD
    },
  });
}
router.post('/:id/mail', function(req,res){
  let workOrderId = req.params.id;
  let email = req.body.email;
  let tmp = process.env.MAIL_HOST;
  if(!email)
    res.sendStatus(400);
  else if(!tmp){
    return res.status(500).json({
      status: 'error',
      message: 'Server has no mail settings. Contact server admin.',
    })
  }
  else{
    let promises = [];
    //get work order
    promises.push(dbWorkOrdersOG.getWorkOrder(workOrderId));
    //get workers
    promises.push(dbWorkOrders.getWorkOrderWorkersAPI(workOrderId));
    //get expenses
    promises.push(dbWorkOrders.getWorkOrderExpenses(workOrderId));
    //get materials
    promises.push(dbWorkOrders.getWorkOrderMaterial(workOrderId));
    //get signs
    promises.push(dbWorkOrders.getWorkOrderSignsAPI(workOrderId));
    //get roboteh base64 image
    promises.push(dbBase64Images.getRobotehImage());
    //get kuka partner base64 image
    promises.push(dbBase64Images.getKukaPartnerImage());
    // get all data together and create pdf document and send it through mail server
    Promise.all(promises).then(([workOrder, workOrderWorkers, workOrderExpenses, workOrderMaterials, workOrderSigns, robotehImg, kukaPartnerImg]) => {
      //PREPARE DATA FOR PDF DEFINITION
      var woName = workOrder.wo_number;
      var woSubscriber = '';
      if(workOrder.subscriber_id) woSubscriber = workOrder.subscriber_name;
      var woProject = ' ';
      if(workOrder.project_id) woProject = workOrder.project_name;
      var userName = ' ';
      if(workOrder.user_id) userName = workOrder.user_name;
      //TIP - OZNAČBA Z BARVO
      var typeOne = 'white', typeTwo = 'white', typeThree = 'white', typeFour = 'white', typeFive = 'white', typeSix = 'white', typeSeven = 'white';
      //if(workOrder.work_order_type_id)
      switch(workOrder.work_order_type_id) {
        case 1: typeOne = 'silver'; break;
        case 2: typeTwo = 'silver'; break;
        case 3: typeThree = 'silver'; break;
        case 4: typeFour = 'silver'; break;
        case 5: typeFive = 'silver'; break;
        case 6: typeSix = 'silver'; break;
        case 7: typeSeven = 'silver'; break;
        default: break;
      }
      //DELAVCI
      var workers = '';
      if(workOrderWorkers && workOrderWorkers.length > 0){
        for(var i = 0; i < workOrderWorkers.length; i++){
          if(i > 0 && i < workOrderWorkers.length)
            workers += ', ';
          workers += workOrderWorkers[i].worker_name;
        }
      }
      else
        workers = ' ';
      //EXPENSES
      var timeQuantity = '', timePrice = '', timeSum = '', distanceQunatity = '', distancePrice = '', distanceSum = '', materialQuantity = '', materialPrice = '', materialSum = '', otherQuantity = '', otherPrice = '', otherSum = '';
      if(workOrderExpenses[0]){
        workOrderExpenses = workOrderExpenses[0];
        if(workOrderExpenses.time_quantity){
          var time = workOrderExpenses.time_quantity.split(':');
          timeQuantity = time[0]+'h '+time[1]+'m';
        }
        if(workOrderExpenses.time_price) timePrice = workOrderExpenses.time_price/100;
        if(workOrderExpenses.time_sum) timeSum = workOrderExpenses.time_sum/100;
        if(workOrderExpenses.distance_quantity) distanceQunatity = workOrderExpenses.distance_quantity;
        if(workOrderExpenses.distance_price) distancePrice = workOrderExpenses.distance_price/100;
        if(workOrderExpenses.distance_sum) distanceSum = workOrderExpenses.distance_sum/100;
        if(workOrderExpenses.material_quantity) materialQuantity = workOrderExpenses.material_quantity;
        if(workOrderExpenses.material_price) materialPrice = workOrderExpenses.material_price/100;
        if(workOrderExpenses.material_sum) materialSum = workOrderExpenses.material_sum/100;
        if(workOrderExpenses.other_quantity) otherQuantity = workOrderExpenses.other_quantity;
        if(workOrderExpenses.other_price) otherPrice = workOrderExpenses.other_price/100;
        if(workOrderExpenses.other_sum) otherSum = workOrderExpenses.other_sum/100;
        //debugger
      }
      //MATERIALS
      var name1 = '',name2 = '',name3 = '',name4 = '',name5 = '',name6 = '',name7 = '',quantity1 = '',quantity2 = '',quantity3 = '',quantity4 = '',quantity5 = '',quantity6 = '',quantity7 = '',price1 = '',price2 = '',price3 = '',price4 = '',price5 = '',price6 = '',price7 = '',sum1 = '',sum2 = '',sum3 = '',sum4 = '',sum5 = '',sum6 = '',sum7 = '';
      if(workOrderMaterials[0]){
        workOrderMaterials = workOrderMaterials[0]
        if(workOrderMaterials.name1) name1 = workOrderMaterials.name1;
        if(workOrderMaterials.name2) name2 = workOrderMaterials.name2;
        if(workOrderMaterials.name3) name3 = workOrderMaterials.name3;
        if(workOrderMaterials.name4) name4 = workOrderMaterials.name4;
        if(workOrderMaterials.name5) name5 = workOrderMaterials.name5;
        if(workOrderMaterials.name6) name6 = workOrderMaterials.name6;
        if(workOrderMaterials.name7) name7 = workOrderMaterials.name7;
        if(workOrderMaterials.quantity1) quantity1 = workOrderMaterials.quantity1;
        if(workOrderMaterials.quantity2) quantity2 = workOrderMaterials.quantity2;
        if(workOrderMaterials.quantity3) quantity3 = workOrderMaterials.quantity3;
        if(workOrderMaterials.quantity4) quantity4 = workOrderMaterials.quantity4;
        if(workOrderMaterials.quantity5) quantity5 = workOrderMaterials.quantity5;
        if(workOrderMaterials.quantity6) quantity6 = workOrderMaterials.quantity6;
        if(workOrderMaterials.quantity7) quantity7 = workOrderMaterials.quantity7;
        if(workOrderMaterials.price1) price1 = workOrderMaterials.price1;
        if(workOrderMaterials.price2) price2 = workOrderMaterials.price2;
        if(workOrderMaterials.price3) price3 = workOrderMaterials.price3;
        if(workOrderMaterials.price4) price4 = workOrderMaterials.price4;
        if(workOrderMaterials.price5) price5 = workOrderMaterials.price5;
        if(workOrderMaterials.price6) price6 = workOrderMaterials.price6;
        if(workOrderMaterials.price7) price7 = workOrderMaterials.price7;
        if(workOrderMaterials.sum1) sum1 = workOrderMaterials.sum1;
        if(workOrderMaterials.sum2) sum2 = workOrderMaterials.sum2;
        if(workOrderMaterials.sum3) sum3 = workOrderMaterials.sum3;
        if(workOrderMaterials.sum4) sum4 = workOrderMaterials.sum4;
        if(workOrderMaterials.sum5) sum5 = workOrderMaterials.sum5;
        if(workOrderMaterials.sum6) sum6 = workOrderMaterials.sum6;
        if(workOrderMaterials.sum7) sum7 = workOrderMaterials.sum7;
      }
      //SIGN REPRESENTATIVE
      var sign = '';
      dateFormat(workOrder.date, "dd.mm.yyyy")
      var signElement = [];
      if(workOrderSigns[0]){
        //need to open image of sign and encode to base64 because of pdfmake
        var file = `${__dirname}/../../files/workorders/signs/${workOrderSigns[0].path_name}`;
        //console.log(file);
        if (fs.existsSync(file)) {
          // Do something
          let imageAsBase64 = fs.readFileSync(file, 'base64');
          sign = 'data:image/png;base64,' + imageAsBase64;
          signElement = [{
            image: 'podpis',
            absolutePosition: {x: 470, y: 755},
            fit: [100, 60],
          }]
        }
      }
      var docDefinition = preparePDFWorkOrder(woName, woSubscriber, woProject, userName, sign, signElement, workOrder, typeOne, typeTwo, typeThree, typeFour, typeFive, typeSix, typeSeven, workers, timeQuantity, timePrice, timeSum, distanceQunatity, distancePrice, distanceSum, materialQuantity, materialPrice, materialSum, otherQuantity, otherPrice, otherSum, name1, name2, name3, name4, name5, name6, name7, quantity1, quantity2, quantity3, quantity4, quantity5, quantity6, quantity7, price1, price2, price3, price4, price5, price6, price7, sum1, sum2, sum3, sum4, sum5, sum6, sum7, robotehImg, kukaPartnerImg);
      var pdfDoc = printer.createPdfKitDocument(docDefinition); 
      pdfDoc.end();
      //var htmlElement = "<p>Pošto lahko pošiljam preko RIS-a, imam še nekaj napak, samo je to najmanjši problem.</p>";
      let hiddenCopyMail = '';
      if(process.env.MAIL_COPY){
        hiddenCopyMail = process.env.MAIL_COPY
      }
      if(process.env.ETHEREAL_COPY){
        hiddenCopyMail = 'test.copy@roboteh.si'
      }
      let testSubject = '';
      let userId = res.locals.oauth.token.user.id;
      //userId 2 is for Novi Uporabnik that is meant for testing purposes
      if(userId == 2){
        testSubject = ' (TESTIRANJE)';
        hiddenCopyMail = 'klemen.sibal@roboteh.si';
      }
      var htmlElement = `<p>Pozdravljeni,</p>
      <p>pošiljamo kopijo delovnega naloga.</p>
      <p>Na ta elektronski naslov ne odgovarjate.</p>
      <p>Lep pozdrav
      <br/>Roboteh Informacijski Sistem</p>
      <img src="cid:kuka@partner"/>`;
      var message = {
        from: "noreply@roboteh.si",
        to: email,
        bcc: hiddenCopyMail,
        subject: "Kopija delovnega naloga"+testSubject,
        text: `Pozdravljeni,
pošiljamo kopijo delovnega naloga.

Na ta elektronski naslov ne odgovarjate.
        
Lep pozdrav
Roboteh Informacijski Sistem`,
        html: htmlElement,
        attachments: [
          {
            // stream as an attachment
            filename: 'DelovniNalog'+woName+'.pdf',
            content: pdfDoc
          },
          {
            filename: 'kuka.png',
            path: __dirname+`/../..//icons/logo/kuka_partner.png`,
            cid: 'kuka@partner' //same cid value as in the html img src
          }
        ],
      };
      //save email to workorder for later use
      dbWorkOrders.saveEMail(workOrderId,email).then(resultMail => {
        //console.log('Sending mail to: ' + email);
        transporter.sendMail(message)
        .then(info => {
          console.log('Email was successfully send to ' + email + ' with pdf copy of work order with id ' + workOrderId + '.');
          dbChanges.addNewChangeSystem(12,18,userId,null,null,null,null,null,null,null,null,null,workOrderId).then(change => {
            res.json({success:true, message:'Email was successfully send to ' + email + ' with pdf copy of work order with id ' + workOrderId + '.', info: info});//200
          })
          .catch((e)=>{
            console.log(e);
            return res.status(500).json({
              status: 'error',
              message: 'Something went wrong',
              error: e,
            })
          })
        })
        .catch((e)=>{
          console.log(e);
          return res.status(500).json({
            status: 'error',
            message: 'Something went wrong',
            error: e,
          })
        })
      })
      .catch((e)=>{
        console.log(e);
        return res.status(500).json({
          status: 'error',
          message: 'Something went wrong',
          error: e,
        })
      })

    })
    .catch((e)=>{
      console.log(e);
      return res.status(500).json({
        status: 'error',
        message: 'Something went wrong',
        error: e,
      })
    })
  }
})
///////////////////////////////////////////////////////////////////////WORK ORDER API
//get user projects (projects where users is part of)
router.get('/', function(req,res, next){
  //get token to verify user and get his userId
  let userId = res.locals.oauth.token.user.id;
  dbWorkOrders.getUserWorkOrdersAPI(userId).then(workOrders => {
    res.json({workOrders:workOrders});//200
  })
  .catch((e)=>{
    console.log(e);
    return res.status(500).json({
      status: 'error',
      message: 'Something went wrong',
      error: e,
    })
  })
})
//get user projects (projects where users is part of)
router.get('/:id', function(req,res, next){
  //get token to verify user and get his userId
  let userId = res.locals.oauth.token.user.id;
  let workOrderId = req.params.id;
  dbWorkOrders.getWorkOrderAPI(workOrderId,userId).then(workOrder => {
    dbWorkOrders.getWorkOrderWorkersAPI(workOrderId).then(workOrderWorkers =>{
      dbWorkOrders.getWorkOrderFilesAPI(workOrderId).then(workOrderFiles => {
        dbWorkOrders.getWorkOrderSignsAPI(workOrderId).then(workOrderSigns => {
          dbWorkOrdersOG.getWorkOrderMaterial(workOrderId).then(workOrderMaterials => {
            //let wo = workOrder
            workOrder[0].workers = workOrderWorkers;
            workOrder[0].files = workOrderFiles;
            workOrder[0].signs = workOrderSigns;
            workOrder[0].materials = [];
            if(workOrderMaterials[0]){
              if(workOrderMaterials[0].name1) workOrder[0].materials.push({name:workOrderMaterials[0].name1, quantity:workOrderMaterials[0].quantity1, price:workOrderMaterials[0].price1, sum:workOrderMaterials[0].sum1});
              if(workOrderMaterials[0].name2) workOrder[0].materials.push({name:workOrderMaterials[0].name2, quantity:workOrderMaterials[0].quantity2, price:workOrderMaterials[0].price2, sum:workOrderMaterials[0].sum2});
              if(workOrderMaterials[0].name3) workOrder[0].materials.push({name:workOrderMaterials[0].name3, quantity:workOrderMaterials[0].quantity3, price:workOrderMaterials[0].price3, sum:workOrderMaterials[0].sum3});
              if(workOrderMaterials[0].name4) workOrder[0].materials.push({name:workOrderMaterials[0].name4, quantity:workOrderMaterials[0].quantity4, price:workOrderMaterials[0].price4, sum:workOrderMaterials[0].sum4});
              if(workOrderMaterials[0].name5) workOrder[0].materials.push({name:workOrderMaterials[0].name5, quantity:workOrderMaterials[0].quantity5, price:workOrderMaterials[0].price5, sum:workOrderMaterials[0].sum5});
              if(workOrderMaterials[0].name6) workOrder[0].materials.push({name:workOrderMaterials[0].name6, quantity:workOrderMaterials[0].quantity6, price:workOrderMaterials[0].price6, sum:workOrderMaterials[0].sum6});
              if(workOrderMaterials[0].name7) workOrder[0].materials.push({name:workOrderMaterials[0].name7, quantity:workOrderMaterials[0].quantity7, price:workOrderMaterials[0].price7, sum:workOrderMaterials[0].sum7});
            }
            res.json({workOrder:workOrder[0]});
          })
          .catch((e)=>{
            console.log(e);
            return res.status(500).json({
              status: 'error',
              message: 'Something went wrong',
              error: e,
            })
          })
        })
        .catch((e)=>{
          console.log(e);
          return res.status(500).json({
            status: 'error',
            message: 'Something went wrong',
            error: e,
          })
        })
      })
      .catch((e)=>{
        console.log(e);
        return res.status(500).json({
          status: 'error',
          message: 'Something went wrong',
          error: e,
        })
      })
    })
    .catch((e)=>{
      console.log(e);
      return res.status(500).json({
        status: 'error',
        message: 'Something went wrong',
        error: e,
      })
    })
  })
  .catch((e)=>{
    console.log(e);
    return res.status(500).json({
      status: 'error',
      message: 'Something went wrong',
      error: e,
    })
  })
})
//create work order
router.post('/', function(req,res){
  //get token to verify user and get his userId
  //All report data (client, datetime, type:servis;montaža;ogled …, descriptions, location (JSON field - longitude/langitude), images (JSON)
  let userId = res.locals.oauth.token.user.id;
  let number = req.body.number
  let projectId = req.body.projectId;
  let subscriberId = req.body.subscriberId;
  let date = req.body.date;
  let arrival = req.body.arrival;
  let departure = req.body.departure;
  let description = req.body.description;
  let type = req.body.type;
  let location = req.body.location;
  let locationCord = req.body.locationCord;
  let vehicle = req.body.vehicle;
  let representative = req.body.representative;
  let files = req.body.files;
  let signs = req.body.signs;
  let workers = req.body.workers;
  let email = req.body.email;
  let materials = [];
  if(req.body.materials) materials = JSON.parse(req.body.materials)
  //materials part
  let name1 = null, quantity1 = null, price1 = null, sum1 = null, name2 = null, quantity2 = null, price2 = null, sum2 = null, name3 = null, quantity3 = null, price3 = null, sum3 = null, name4 = null, quantity4 = null, price4 = null, sum4 = null, name5 = null, quantity5 = null, price5 = null, sum5 = null, name6 = null, quantity6 = null, price6 = null, sum6 = null, name7 = null, quantity7 = null, price7 = null, sum7 = null;
  if(req.body.materials != undefined){
    for(let i = 0, l = materials.length; i < l; i++){
      if(i == 0){
        name1 = materials[i].name;
        quantity1 = materials[i].quantity;
        price1 = materials[i].price;
        sum1 = materials[i].sum;
      } else if(i >= l) break;
      if(i == 1){
        name2 = materials[i].name;
        quantity2 = materials[i].quantity;
        price2 = materials[i].price;
        sum2 = materials[i].sum;
      } else if(i >= l) break;
      if(i == 2){
        name3 = materials[i].name;
        quantity3 = materials[i].quantity;
        price3 = materials[i].price;
        sum3 = materials[i].sum;
      } else if(i >= l) break;
      if(i == 3){
        name4 = materials[i].name;
        quantity4 = materials[i].quantity;
        price4 = materials[i].price;
        sum4 = materials[i].sum;
      } else if(i >= l) break;
      if(i == 4){
        name5 = materials[i].name;
        quantity5 = materials[i].quantity;
        price5 = materials[i].price;
        sum5 = materials[i].sum;
      } else if(i >= l) break;
      if(i == 5){
        name6 = materials[i].name;
        quantity6 = materials[i].quantity;
        price6 = materials[i].price;
        sum6 = materials[i].sum;
      } else if(i >= l) break;
      if(i == 6){
        name7 = materials[i].name;
        quantity7 = materials[i].quantity;
        price7 = materials[i].price;
        sum7 = materials[i].sum;
      }
    }
  }
  if(!(projectId || subscriberId) || !date || !arrival || !departure || !description || !type || !location || !vehicle || !representative)
    res.sendStatus(400);
  else{
    if(!workers)
      workers = [userId];
    else
      workers = workers.split(',');
    if(files){
      files = files.split(',');
      if(files[0] == "")
        files.pop();
    } else files = [];
    if(signs){
      signs = signs.split(',');
      if(signs[0] == "")
        signs.pop();
    } else signs = [];
    dbWorkOrders.addNewWorkOrder(userId,number,type,date,location,projectId,subscriberId,description,vehicle,arrival,departure,representative,locationCord,email).then(workOrder => {
      let promisesWorkers = [];
      let promisesFiles = [];
      let promisesSigns = [];
      for(let i=0; i<workers.length; i++){
        promisesWorkers.push(dbWorkOrders.addWorker(workOrder.id,workers[i]));
      }
      Promise.all(promisesWorkers)
      .then((results)=>{
        if(files && files.length > 0){
          for(let i=0; i<files.length; i++){
            promisesFiles.push(dbWorkOrders.addFile(workOrder.id,files[i]));
          }
          Promise.all(promisesFiles)
          .then(workOrderFiles => {
            if(signs && signs.length > 0){
              for(let i=0; i<signs.length; i++){
                promisesSigns.push(dbWorkOrders.addSign(workOrder.id,signs[i]));
              }
              Promise.all(promisesSigns)
              .then(workOrderSigns => {
                //add change to db -> change system (files & signs)
                dbChanges.addNewChangeSystem(1,26,userId,null,null,null,null,null,null,null,null,null,workOrder.id)
                .then(sysChange => {
                  console.log("Uporabnik " + userId + " je naredil spremembo tipa 26 s statusom 1 za delovni nalog " + workOrder.id + ".");
                  //add changes for files
                  let promisesFilesChanges = [];
                  for(let i = 0; i < files.length; i++){
                    promisesFilesChanges.push(dbChanges.addNewChangeSystem(1,27,userId,null,null,null,null,null,null,null,null,null,workOrder.id,null,null,null,null,null,files[i]));
                    console.log("Uporabnik " + userId + " je dodal datoteko " + files[i] + " za delovni nalog " + workOrder.id + ".");
                  }
                  Promise.all(promisesFilesChanges)
                  .then(sysChangeFiles => {
                    //add changes for signs
                    let promisesSignsChanges = [];
                    for(let i = 0; i < signs.length; i++){
                      promisesSignsChanges.push(dbChanges.addNewChangeSystem(1,29,userId,null,null,null,null,null,null,null,null,null,workOrder.id,null,null,null,null,null,null,null,null,signs[i]));
                      console.log("Uporabnik " + userId + " je dodal podpis " + signs[i] + " za delovni nalog " + workOrder.id + ".");
                    }
                    Promise.all(promisesSignsChanges)
                    .then(sysChangeSigns => {
                      //update materials
                      if(req.body.materials != undefined){
                        dbWorkOrdersOG.updateWorkOrderMaterials(workOrder.id,name1,quantity1,price1,sum1,name2,quantity2,price2,sum2,name3,quantity3,price3,sum3,name4,quantity4,price4,sum4,name5,quantity5,price5,sum5,name6,quantity6,price6,sum6,name7,quantity7,price7,sum7).then(updateMaterialResp =>{
                          if(!updateMaterialResp){
                            //no materials exist, create new materials
                            dbWorkOrdersOG.addWorkOrderMaterials(workOrder.id,name1,quantity1,price1,sum1,name2,quantity2,price2,sum2,name3,quantity3,price3,sum3,name4,quantity4,price4,sum4,name5,quantity5,price5,sum5,name6,quantity6,price6,sum6,name7,quantity7,price7,sum7).then(newMaterialResp =>{
                              res.json({workOrder:workOrder});
                            })
                            .catch(e=>{
                              console.log(e);
                              return res.status(500).json({
                                status: 'error',
                                message: 'Error with updating work order materials',
                                error: e,
                              })
                            })
                          }
                          else{
                            //normal update
                            res.json({workOrder:workOrder});
                          }
                        })
                        .catch(e=>{
                          console.log(e);
                          return res.status(500).json({
                            status: 'error',
                            message: 'Error with updating work order materials',
                            error: e,
                          })
                        })
                      }
                      else
                        return res.json({workOrder:workOrder});
                    })
                    .catch((e)=>{
                      return res.json({success:false, error:e});
                    })
                  })
                  .catch((e)=>{
                    return res.json({success:false, error:e});
                  })
                })
              })
              .catch(e=>{
                console.log(e);
                return res.status(500).json({
                  status: 'error',
                  message: 'Something went wrong',
                  error: e,
                })
              })
            }
            else{
              //files & no signs
              //add change to db -> change system
              dbChanges.addNewChangeSystem(1,26,userId,null,null,null,null,null,null,null,null,null,workOrder.id)
              .then(sysChange => {
                console.log("Uporabnik " + userId + " je naredil spremembo tipa 26 s statusom 1 za delovni nalog " + workOrder.id + ".");
                //add changes for files
                let promisesFilesChanges = [];
                for(let i = 0; i < files.length; i++){
                  promisesFilesChanges.push(dbChanges.addNewChangeSystem(1,27,userId,null,null,null,null,null,null,null,null,null,workOrder.id,null,null,null,null,null,files[i]));
                  console.log("Uporabnik " + userId + " je dodal datoteko " + files[i] + " za delovni nalog " + workOrder.id + ".");
                }
                Promise.all(promisesFilesChanges)
                .then(sysChangeFiles => {
                  //update materials
                  if(req.body.materials != undefined){
                    dbWorkOrdersOG.updateWorkOrderMaterials(workOrder.id,name1,quantity1,price1,sum1,name2,quantity2,price2,sum2,name3,quantity3,price3,sum3,name4,quantity4,price4,sum4,name5,quantity5,price5,sum5,name6,quantity6,price6,sum6,name7,quantity7,price7,sum7).then(updateMaterialResp =>{
                      if(!updateMaterialResp){
                        //no materials exist, create new materials
                        dbWorkOrdersOG.addWorkOrderMaterials(workOrder.id,name1,quantity1,price1,sum1,name2,quantity2,price2,sum2,name3,quantity3,price3,sum3,name4,quantity4,price4,sum4,name5,quantity5,price5,sum5,name6,quantity6,price6,sum6,name7,quantity7,price7,sum7).then(newMaterialResp =>{
                          res.json({workOrder:workOrder});
                        })
                        .catch(e=>{
                          console.log(e);
                          return res.status(500).json({
                            status: 'error',
                            message: 'Error with updating work order materials',
                            error: e,
                          })
                        })
                      }
                      else{
                        //normal update
                        res.json({workOrder:workOrder});
                      }
                    })
                    .catch(e=>{
                      console.log(e);
                      return res.status(500).json({
                        status: 'error',
                        message: 'Error with updating work order materials',
                        error: e,
                      })
                    })
                  }
                  else
                    return res.json({workOrder:workOrder});
                })
                .catch((e)=>{
                  return res.json({success:false, error:e});
                })
              })
              .catch((e)=>{
                return res.json({success:false, error:e});
              })
            }
          })
          .catch(e=>{
            console.log(e);
            return res.status(500).json({
              status: 'error',
              message: 'Something went wrong',
              error: e,
            })
          })
        }
        else{
          //no files
          if(signs && signs.length > 0){
            for(let i=0; i<signs.length; i++){
              promisesSigns.push(dbWorkOrders.addSign(workOrder.id,signs[i]));
            }
            Promise.all(promisesSigns)
            .then(workOrderSigns => {
              //add change to db -> change system (signs)
              dbChanges.addNewChangeSystem(1,26,userId,null,null,null,null,null,null,null,null,null,workOrder.id)
              .then(sysChange => {
                console.log("Uporabnik " + userId + " je naredil spremembo tipa 26 s statusom 1 za delovni nalog " + workOrder.id + ".");
                //add changes for signs
                let promisesSignsChanges = [];
                for(let i = 0; i < signs.length; i++){
                  promisesSignsChanges.push(dbChanges.addNewChangeSystem(1,29,userId,null,null,null,null,null,null,null,null,null,workOrder.id,null,null,null,null,null,null,null,null,signs[i]));
                  console.log("Uporabnik " + userId + " je dodal podpis " + signs[i] + " za delovni nalog " + workOrder.id + ".");
                }
                Promise.all(promisesSignsChanges)
                .then(sysChangeSigns => {
                  //update materials
                  if(req.body.materials != undefined){
                    dbWorkOrdersOG.updateWorkOrderMaterials(workOrder.id,name1,quantity1,price1,sum1,name2,quantity2,price2,sum2,name3,quantity3,price3,sum3,name4,quantity4,price4,sum4,name5,quantity5,price5,sum5,name6,quantity6,price6,sum6,name7,quantity7,price7,sum7).then(updateMaterialResp =>{
                      if(!updateMaterialResp){
                        //no materials exist, create new materials
                        dbWorkOrdersOG.addWorkOrderMaterials(workOrder.id,name1,quantity1,price1,sum1,name2,quantity2,price2,sum2,name3,quantity3,price3,sum3,name4,quantity4,price4,sum4,name5,quantity5,price5,sum5,name6,quantity6,price6,sum6,name7,quantity7,price7,sum7).then(newMaterialResp =>{
                          res.json({workOrder:workOrder});
                        })
                        .catch(e=>{
                          console.log(e);
                          return res.status(500).json({
                            status: 'error',
                            message: 'Error with updating work order materials',
                            error: e,
                          })
                        })
                      }
                      else{
                        //normal update
                        res.json({workOrder:workOrder});
                      }
                    })
                    .catch(e=>{
                      console.log(e);
                      return res.status(500).json({
                        status: 'error',
                        message: 'Error with updating work order materials',
                        error: e,
                      })
                    })
                  }
                  else
                    return res.json({workOrder:workOrder});
                })
                .catch((e)=>{
                  return res.json({success:false, error:e});
                })
              })
              .catch((e)=>{
                return res.json({success:false, error:e});
              })
            })
            .catch(e=>{
              console.log(e);
              return res.status(500).json({
                status: 'error',
                message: 'Something went wrong',
                error: e,
              })
            })
          }
          else{
            //no files & no signs
            //add change to db -> change system
            dbChanges.addNewChangeSystem(1,26,userId,null,null,null,null,null,null,null,null,null,workOrder.id)
            .then(sysChange => {
              //console.log("Uporabnik " + userId + " je naredil spremembo tipa 18 s statusom 1 za delovni nalog " + workOrder.id + ".");
              //update materials
              if(req.body.materials != undefined){
                dbWorkOrdersOG.updateWorkOrderMaterials(workOrder.id,name1,quantity1,price1,sum1,name2,quantity2,price2,sum2,name3,quantity3,price3,sum3,name4,quantity4,price4,sum4,name5,quantity5,price5,sum5,name6,quantity6,price6,sum6,name7,quantity7,price7,sum7).then(updateMaterialResp =>{
                  if(!updateMaterialResp){
                    //no materials exist, create new materials
                    dbWorkOrdersOG.addWorkOrderMaterials(workOrder.id,name1,quantity1,price1,sum1,name2,quantity2,price2,sum2,name3,quantity3,price3,sum3,name4,quantity4,price4,sum4,name5,quantity5,price5,sum5,name6,quantity6,price6,sum6,name7,quantity7,price7,sum7).then(newMaterialResp =>{
                      res.json({workOrder:workOrder});
                    })
                    .catch(e=>{
                      console.log(e);
                      return res.status(500).json({
                        status: 'error',
                        message: 'Error with updating work order materials',
                        error: e,
                      })
                    })
                  }
                  else{
                    //normal update
                    res.json({workOrder:workOrder});
                  }
                })
                .catch(e=>{
                  console.log(e);
                  return res.status(500).json({
                    status: 'error',
                    message: 'Error with updating work order materials',
                    error: e,
                  })
                })
              }
              else
                return res.json({workOrder:workOrder});
            })
            .catch((e)=>{
              return res.json({success:false, error:e});
            })
          }
        }
      })
      .catch((e)=>{
        console.log(e);
        return res.status(500).json({
          status: 'error',
          message: 'Something went wrong',
          error: e,
        })
      })
    })
    .catch((e)=>{
      console.log(e);
      return res.status(500).json({
        status: 'error',
        message: 'Something went wrong',
        error: e,
      })
    })
  }
})
//update work order
router.put('/:id', function(req,res){
  //get token to verify user and get his userId
  //All report data (client, datetime, type:servis;montaža;ogled …, descriptions, location (JSON field - longitude/langitude), images (JSON)
  let workOrderId = req.params.id;
  let userId = res.locals.oauth.token.user.id;
  let number = req.body.number
  let projectId = req.body.projectId;
  let subscriberId = req.body.subscriberId;
  let date = req.body.date;
  let arrival = req.body.arrival;
  let departure = req.body.departure;
  let description = req.body.description;
  let type = req.body.type;
  let location = req.body.location;
  let locationCord = req.body.locationCord;
  let vehicle = req.body.vehicle;
  let representative = req.body.representative;
  let files = req.body.files;
  let signs = req.body.signs;
  let workers = req.body.workers;
  let email = req.body.email;
  let materials = [];
  if(req.body.materials) materials = JSON.parse(req.body.materials)
  if(!(projectId || subscriberId) || !date || !arrival || !departure || !description || !type || !location || !vehicle || !representative)
    res.sendStatus(400);
  else{
    if(!workers)
      workers = [userId];
    else
      workers = workers.split(',');
    if(files){
      files = files.split(',');
      if(files[0] == "")
        files.pop();
    } else files = [];
    if(signs){
      signs = signs.split(',');
      if(signs[0] == "")
        signs.pop();
    }
    else signs = [];
    dbWorkOrders.editWorkOrder(workOrderId,number,type,date,location,projectId,subscriberId,description,vehicle,arrival,departure,representative,locationCord,email).then(workOrder => {
      dbWorkOrders.getWorkOrderWorkersAPI(workOrderId)
      .then(oldWorkers => {
        let tmp1 = workers.map(x => parseInt(x));
        let tmp2 = oldWorkers.map(x => x.id);
        let addWorkers = tmp1.filter(x => !tmp2.includes(x));
        let deleteWorkers = tmp2.filter(x => !tmp1.includes(x));
        //console.log('dodaj delavce: ' + addWorkers);
        //console.log('odstrani delavce: ' + deleteWorkers);
        let promisesWorkers = [];
        for(let i = 0; i < addWorkers.length; i++){
          promisesWorkers.push(dbWorkOrders.addWorker(workOrderId,addWorkers[i]));
        }
        for(let i = 0; i < deleteWorkers.length; i++){
          promisesWorkers.push(dbWorkOrders.deleteWorker(workOrderId,deleteWorkers[i]));
        }
        //materials part update
        let name1 = null, quantity1 = null, price1 = null, sum1 = null, name2 = null, quantity2 = null, price2 = null, sum2 = null, name3 = null, quantity3 = null, price3 = null, sum3 = null, name4 = null, quantity4 = null, price4 = null, sum4 = null, name5 = null, quantity5 = null, price5 = null, sum5 = null, name6 = null, quantity6 = null, price6 = null, sum6 = null, name7 = null, quantity7 = null, price7 = null, sum7 = null;
        if(req.body.materials != undefined){
          for(let i = 0, l = materials.length; i < l; i++){
            if(i == 0){
              name1 = materials[i].name;
              quantity1 = materials[i].quantity;
              price1 = materials[i].price;
              sum1 = materials[i].sum;
            } else if(i >= l) break;
            if(i == 1){
              name2 = materials[i].name;
              quantity2 = materials[i].quantity;
              price2 = materials[i].price;
              sum2 = materials[i].sum;
            } else if(i >= l) break;
            if(i == 2){
              name3 = materials[i].name;
              quantity3 = materials[i].quantity;
              price3 = materials[i].price;
              sum3 = materials[i].sum;
            } else if(i >= l) break;
            if(i == 3){
              name4 = materials[i].name;
              quantity4 = materials[i].quantity;
              price4 = materials[i].price;
              sum4 = materials[i].sum;
            } else if(i >= l) break;
            if(i == 4){
              name5 = materials[i].name;
              quantity5 = materials[i].quantity;
              price5 = materials[i].price;
              sum5 = materials[i].sum;
            } else if(i >= l) break;
            if(i == 5){
              name6 = materials[i].name;
              quantity6 = materials[i].quantity;
              price6 = materials[i].price;
              sum6 = materials[i].sum;
            } else if(i >= l) break;
            if(i == 6){
              name7 = materials[i].name;
              quantity7 = materials[i].quantity;
              price7 = materials[i].price;
              sum7 = materials[i].sum;
            }
          }
        }
        //add change to db
        promisesWorkers.push(dbChanges.addNewChangeSystem(2,26,userId,null,null,null,null,null,null,null,null,null,workOrderId));
        Promise.all(promisesWorkers)
        .then(results => {
          //check if new files
          dbWorkOrders.getWorkOrderFilesAPI(workOrderId)
          .then(oldFiles => {
            let tmp1 = files.map(x => parseInt(x));
            let tmp2 = oldFiles.map(x => x.id);
            let addFiles = tmp1.filter(x => !tmp2.includes(x));
            let deleteFiles = tmp2.filter(x => !tmp1.includes(x));
            //console.log('dodaj datoteke:' + addFiles);
            //console.log('odstrani datoteke: ' + deleteFiles);
            let promisesFiles = [];
            for(let i = 0; i < addFiles.length; i++){
              promisesFiles.push(dbWorkOrders.addFile(workOrderId,addFiles[i]));
              promisesFiles.push(dbChanges.addNewChangeSystem(1,27,userId,null,null,null,null,null,null,null,null,null,workOrderId,null,null,null,null,null,addFiles[i]));
            }
            for(let i = 0; i < deleteFiles.length; i++){
              promisesFiles.push(dbWorkOrders.deleteFile(workOrderId,deleteFiles[i]));
              promisesFiles.push(dbChanges.addNewChangeSystem(3,27,userId,null,null,null,null,null,null,null,null,null,workOrderId,null,null,null,null,null,deleteFiles[i]));
            }
            Promise.all(promisesFiles)
            .then(results => {
              //check of new signs
              dbWorkOrders.getWorkOrderSignsAPI(workOrderId)
              .then(oldSigns => {
                let tmp1 = signs.map(x => parseInt(x));
                let tmp2 = oldSigns.map(x => x.id);
                let addSigns = tmp1.filter(x => !tmp2.includes(x));
                let deleteSigns = tmp2.filter(x => !tmp1.includes(x));
                //console.log('dodaj podpise:' + addSigns);
                //console.log('odstrani podpise: ' + deleteSigns);
                let promisesSigns = [];
                for(let i = 0; i < addSigns.length; i++){
                  promisesSigns.push(dbWorkOrders.addSign(workOrderId,addSigns[i]));
                  promisesSigns.push(dbChanges.addNewChangeSystem(1,29,userId,null,null,null,null,null,null,null,null,null,workOrderId,null,null,null,null,null,null,null,null,addSigns[i]));
                }
                for(let i = 0; i < deleteSigns.length; i++){
                  promisesSigns.push(dbWorkOrders.deleteSign(workOrderId,deleteSigns[i]));
                  promisesSigns.push(dbChanges.addNewChangeSystem(3,29,userId,null,null,null,null,null,null,null,null,null,workOrderId,null,null,null,null,null,null,null,null,deleteSigns[i]));
                }
                Promise.all(promisesSigns)
                .then(results => {
                  //update materials
                  if(req.body.materials != undefined){
                    dbWorkOrdersOG.updateWorkOrderMaterials(workOrderId,name1,quantity1,price1,sum1,name2,quantity2,price2,sum2,name3,quantity3,price3,sum3,name4,quantity4,price4,sum4,name5,quantity5,price5,sum5,name6,quantity6,price6,sum6,name7,quantity7,price7,sum7).then(updateMaterialResp =>{
                      if(!updateMaterialResp){
                        //no materials exist, create new materials
                        dbWorkOrdersOG.addWorkOrderMaterials(workOrderId,name1,quantity1,price1,sum1,name2,quantity2,price2,sum2,name3,quantity3,price3,sum3,name4,quantity4,price4,sum4,name5,quantity5,price5,sum5,name6,quantity6,price6,sum6,name7,quantity7,price7,sum7).then(newMaterialResp =>{
                          res.json({workOrder:workOrder});
                        })
                        .catch(e=>{
                          console.log(e);
                          return res.status(500).json({
                            status: 'error',
                            message: 'Error with updating work order materials',
                            error: e,
                          })
                        })
                      }
                      else{
                        //normal update
                        res.json({workOrder:workOrder});
                      }
                    })
                    .catch(e=>{
                      console.log(e);
                      return res.status(500).json({
                        status: 'error',
                        message: 'Error with updating work order materials',
                        error: e,
                      })
                    })
                  }
                  else
                    res.json({workOrder:workOrder});
                })
                .catch(e=>{
                  console.log(e);
                  return res.status(500).json({
                    status: 'error',
                    message: 'Something went wrong',
                    error: e,
                  })
                })
              })
              .catch(e=>{
                console.log(e);
                return res.status(500).json({
                  status: 'error',
                  message: 'Something went wrong',
                  error: e,
                })
              })
            })
            .catch(e=>{
              console.log(e);
              return res.status(500).json({
                status: 'error',
                message: 'Something went wrong',
                error: e,
              })
            })
          })
          .catch(e=>{
            console.log(e);
            return res.status(500).json({
              status: 'error',
              message: 'Something went wrong',
              error: e,
            })
          })
        })
        .catch(e=>{
          console.log(e);
          return res.status(500).json({
            status: 'error',
            message: 'Something went wrong',
            error: e,
          })
        })
      })
      .catch(e=>{
        console.log(e);
        return res.status(500).json({
          status: 'error',
          message: 'Something went wrong',
          error: e,
        })
      })
    })
    .catch((e)=>{
      console.log(e);
      return res.status(500).json({
        status: 'error',
        message: 'Something went wrong',
        error: e,
      })
    })
  }
})
//delete report (update active to false)
router.delete('/:id', function(req,res){
  //get token to verify user and get his userId
  let userId = res.locals.oauth.token.user.id;
  let workOrderId = req.params.id;
  dbWorkOrders.deleteWorkOrder(workOrderId).then(workOrder => {
    //add change to db -> change system
    dbChanges.addNewChangeSystem(3,18,userId,null,null,null,null,null,null,null,null,null,workOrder.id)
    .then(sysChange => {
    console.log("Uporabnik " + userId + " je naredil spremembo tipa 18 s statusom 3 za delovni nalog " + workOrder.id + ".");
      return res.json({workOrder:workOrder});
    })
    .catch((e)=>{
      return res.json({success:false, error:e});
    })
  })
  .catch((e)=>{
    console.log(e);
    return res.status(500).json({
      status: 'error',
      message: 'Something went wrong',
      error: e,
    })
  })
})

function preparePDFWorkOrder(woName, woSubscriber, woProject, userName, sign, signElement, workOrder, typeOne, typeTwo, typeThree, typeFour, typeFive, typeSix, typeSeven, workers, timeQuantity, timePrice, timeSum, distanceQunatity, distancePrice, distanceSum, materialQuantity, materialPrice, materialSum, otherQuantity, otherPrice, otherSum, name1, name2, name3, name4, name5, name6, name7, quantity1, quantity2, quantity3, quantity4, quantity5, quantity6, quantity7, price1, price2, price3, price4, price5, price6, price7, sum1, sum2, sum3, sum4, sum5, sum6, sum7, robotehImg, kukaPartnerImg) {
  return {
    info: {
      title: woName,
      author: 'Roboteh-IS',
      subject: 'Delovni nalog',
      keywords: woName + ", " + woSubscriber,
    },
    pageSize: 'A4',
    pageOrientation: 'portrait',
    header: {
      columns: [
        {
          style: 'noga',
          table: {
            widths: [280, 130, 105, 100],
            body: [
              [
                { text: '', color: 'white', alignment: 'center', fillColor: 'black' },
                { text: '', color: 'white', alignment: 'center', fillColor: 'black' },
                { text: '', preserveLeadingSpaces: true, color: 'white', alignment: 'center', fillColor: 'black' },
                { text: ' ', preserveLeadingSpaces: true, color: 'white', alignment: 'center', fillColor: 'black' },
              ],
            ]
          },
        },
      ]
    },
    footer: {
      columns: [
        {
          style: 'noga',
          table: {
            widths: [350,130,45,75],
            body: [
              [
                { text: '*Podpisan izvod velja kot dobavnica', color: 'white', alignment: 'center', fillColor: 'black' },
                { text: 'Hvala za zaupanje!', color: 'white', alignment: 'center', fillColor: 'black' },
                { text: '', preserveLeadingSpaces: true, color: 'white', alignment: 'center', fillColor: 'black' },
                { text: 'Obr.: št. 01/1', preserveLeadingSpaces: true, color: 'white', alignment: 'center', fillColor: 'black' },
              ],
            ]
          },
        },
      ]
    },
    content: [
      signElement,
      {
        style: 'tableRoboteh',
        table: {
          widths: [200],
          body: [ [ { image: 'roboteh', width: 200 }, ] ]
        }, layout: 'noBorders'
      },
      {
        style: 'tableKontaktHead',
        table: {
          widths: [200],
          body: [ [ { text: 'ROBOTEH', fontSize: 16, preserveLeadingSpaces: true }, ], ]
        }, layout: 'noBorders'
      },
      {
        style: 'tableKontakt',
        table: {
          widths: [200, 50, 240, 100],
          body: [
            [ 
              [
                { text: '                                     D.O.O', style: 'kontakt', preserveLeadingSpaces: true },
                { text: 'GORIČICA 2B, SI-3230 ŠENTJUR, SLOVENIA-EU', style: 'kontakt', preserveLeadingSpaces: true },
                { text: 'ID za DDV: SI24773832', style: 'kontakt', preserveLeadingSpaces: true },
                { text: 'Tel: +386 (0)3 746 42 44', style: 'kontakt' },
                { text: 'Mobile: +386 51 645 205 ', style: 'kontakt' },
                { text: 'E-mail: office@roboteh.si', style: 'kontakt' },
                { text: 'WWW.ROBOTEH.SI', style: 'kontakt' },

              ]
            ],
          ]
        }, layout: 'noBorders'
      },
      {
        style: 'tableKuka',
        table: {
          widths: [200],
          body: [ [ { image: 'kuka', width: 50 }, ] ]
        }, layout: 'noBorders'
      },
      {
        style: 'crta1',
        table: { widths: [600], body: [[" "], [" "]] },
        layout: {
          hLineWidth: function (i, node) { return (i === 0 || i === node.table.body.length) ? 0 : 2; },
          vLineWidth: function (i, node) { return 0; },
        }
      },
      {
        style: 'crta2',
        table: { widths: [600], body: [[" "], [" "]] },
        layout: {
          hLineWidth: function (i, node) { return (i === 0 || i === node.table.body.length) ? 0 : 2; },
          vLineWidth: function (i, node) { return 0; },
        }
      },
      {
        style: 'crta3',
        table: {
          widths: [600],
          body: [[" "], [" "]]
        },
        layout: {
          hLineWidth: function (i, node) {
            return (i === 0 || i === node.table.body.length) ? 0 : 2;
          },
          vLineWidth: function (i, node) {
            return 0;
          },
        }
      },
      {
        style: 'header',
        table: {
          body: [ [{ text: 'DELOVNI NALOG', preserveLeadingSpaces: true },] ]
        },
        layout: 'noBorders'
      },
      {
        style: 'infotable',
        table: {
          widths: [16, 90, 100, 37, 92],
          body: [
            [
              { rowSpan: 2, border: [false, false, false, false], text: 'št.: ', },
              { border: [false, false, false, true], text: woName, preserveLeadingSpaces: true },
              { rowSpan: 2, border: [false, false, false, false], text: ' ', preserveLeadingSpaces: true },
              { border: [false, false, false, false], text: 'Datum: ' },
              { border: [false, false, false, true], text: dateFormat(workOrder.date, "dd.mm.yyyy") }
            ],
            [
              { border: [false, false, false, false], text: '    1 = servis,   2 = programiranje,    3 = montaža,    4 = zagon,    5 = storitev,   6 = svetovanje,   7 = pomoč', preserveLeadingSpaces: true },
              { border: [false, false, false, false], text: '', preserveLeadingSpaces: true },
              { rowSpan: 2, border: [false, false, false, false], text: ' št.:', preserveLeadingSpaces: true },
              { border: [false, false, false, false], text: 'Kraj:' },
              { border: [false, false, false, true], text: workOrder.location }
            ]
          ]
        },
        layout: { defaultBorder: true, }
      },
      {
        style: 'typestable',
        table: {
          widths: [50, 90, 66, 60, 65, 70, 65],
          body: [
            [
              { border: [true, true, false, true], text: '1 = servis,', alignment: 'center', preserveLeadingSpaces: true, fillColor: typeOne },
              { border: [false, true, false, true], text: '2 = programiranje,', alignment: 'center', preserveLeadingSpaces: true, fillColor: typeTwo },
              { border: [false, true, false, true], text: '3 = montaža,', alignment: 'center', preserveLeadingSpaces: true, fillColor: typeThree },
              { border: [false, true, false, true], text: '4 = zagon,', alignment: 'center', preserveLeadingSpaces: true, fillColor: typeFour },
              { border: [false, true, false, true], text: '5 = storitev,', alignment: 'center', preserveLeadingSpaces: true, fillColor: typeFive },
              { border: [false, true, false, true], text: '6 = svetovanje,', alignment: 'center', preserveLeadingSpaces: true, fillColor: typeSix },
              { border: [false, true, true, true], text: '7 = pomoč', alignment: 'center', preserveLeadingSpaces: true, fillColor: typeSeven },
            ]
          ]
        },
        layout: { defaultBorder: false, }
      },
      {
        style: 'zacetek',
        table: {
          widths: [520],
          body: [ [ { border: [true, true, true, false], text: ' ', }, ], ]
        },
        layout: { defaultBorder: true, }
      },
      {
        style: 'subproject',
        table: {
          widths: [58, 370, 74],
          body: [
            [
              { border: [true, false, false, false], text: 'NAROČNIK: ', },
              { border: [false, false, false, true], text: woSubscriber, preserveLeadingSpaces: true },
              { rowSpan: 2, border: [false, false, true, false], text: ' ', preserveLeadingSpaces: true },
            ],
            [
              { border: [true, false, false, false], text: 'PROJEKT:', preserveLeadingSpaces: true },
              { border: [false, false, false, true], text: woProject, preserveLeadingSpaces: true },
              { rowSpan: 2, border: [false, false, false, false], text: ' ', preserveLeadingSpaces: true },
            ]
          ]
        },
        layout: { defaultBorder: true, }
      },
      {
        style: 'description',
        table: {
          widths: [141, 370, 2],
          body: [
            [
              { border: [true, false, false, false], text: 'OPIS OPRAVLJENEGA DELA: ', },
              { border: [false, false, true, false], text: '', preserveLeadingSpaces: true },
              { border: [false, false, false, false], text: ' ', preserveLeadingSpaces: true },
            ],
            [
              { colSpan: 2, rowSpan: 6, border: [true, false, true, false], text: workOrder.description, preserveLeadingSpaces: true },
              { text: ' ', preserveLeadingSpaces: true },
              { border: [false, false, false, false], text: ' ', preserveLeadingSpaces: true },
            ],
            [
              { colSpan: 2, text: ' ', preserveLeadingSpaces: true },
              { text: ' ', preserveLeadingSpaces: true },
              { border: [false, false, false, false], text: ' ', preserveLeadingSpaces: true },
            ],
            [
              { colSpan: 2, text: ' ', preserveLeadingSpaces: true },
              { text: ' ', preserveLeadingSpaces: true },
              { border: [false, false, false, false], text: ' ', preserveLeadingSpaces: true },
            ],
            [
              { colSpan: 2, text: ' ', preserveLeadingSpaces: true },
              { text: ' ', preserveLeadingSpaces: true },
              { border: [false, false, false, false], text: ' ', preserveLeadingSpaces: true },
            ],
            [
              { colSpan: 2, text: ' ', preserveLeadingSpaces: true },
              { text: ' ', preserveLeadingSpaces: true },
              { border: [false, false, false, false], text: ' ', preserveLeadingSpaces: true },
            ],
            [
              { colSpan: 2, text: ' ', preserveLeadingSpaces: true },
              { text: ' ', preserveLeadingSpaces: true },
              { border: [false, false, false, false], text: ' ', preserveLeadingSpaces: true },
            ]
          ]
        },
      },
      {
        style: 'workers',
        table: {
          widths: [74, 306, 122],
          body: [
            [
              { border: [true, false, false, false], text: 'Izvajalec naloge: ', },
              { border: [false, false, false, true], text: workers, preserveLeadingSpaces: true },
              { rowSpan: 2, border: [false, false, true, false], text: ' ', preserveLeadingSpaces: true },
            ],
            [
              { border: [true, false, false, false], text: 'Podatki o vozilu:', preserveLeadingSpaces: true },
              { border: [false, false, false, true], text: workOrder.vehicle, preserveLeadingSpaces: true },
              { rowSpan: 2, border: [false, false, false, false], text: ' ', preserveLeadingSpaces: true },
            ]
          ]
        },
        layout: { defaultBorder: true, }
      },
      {
        style: 'time',
        table: {
          widths: [56, 80, 20, 56, 80, 183],
          body: [
            [
              { border: [true, false, false, false], text: 'Čas prihoda: ', },
              { border: [false, false, false, true], text: workOrder.arrival.substring(0,5), preserveLeadingSpaces: true },
              { border: [false, false, false, false], text: ' ', preserveLeadingSpaces: true },
              { border: [false, false, false, false], text: 'Čas odhoda: ', },
              { border: [false, false, false, true], text: workOrder.departure.substring(0,5), preserveLeadingSpaces: true },
              { border: [false, false, true, false], text: ' ', preserveLeadingSpaces: true },
            ],
          ]
        },
        layout: { defaultBorder: true, }
      },
      {
        style: 'konec',
        table: {
          widths: [520],
          body: [ [ { border: [true, false, true, true], text: ' ', }, ], ]
        },
        layout: { defaultBorder: true, }
      },
      {
        style: 'stroskiheader',
        table: {
          widths: [333],
          body: [ [ { text: 'STROŠKI DELA, PREVOZ, OSTALO: ', fillColor: 'silver' }, ], ]
        },
      },
      {
        style: 'stroskiheaderPlus',
        table: {
          widths: [50, 50, 60],
          body: [
            [
              { text: 'Količina', alignment: 'center', fillColor: 'silver' },
              { text: 'Cena', alignment: 'center', fillColor: 'silver' },
              { text: 'Znesek', alignment: 'center', fillColor: 'silver' },
            ],
          ]
        },
      },
      {
        style: 'stroski',
        table: {
          widths: [333, 50, 50, 60],
          body: [
            [
              { text: 'Porabljen čas dela (h):', },
              { text: timeQuantity, alignment: 'center', preserveLeadingSpaces: true },
              { text: timePrice, alignment: 'center', preserveLeadingSpaces: true },
              { text: timeSum, alignment: 'center', preserveLeadingSpaces: true },
            ],
            [
              { text: 'Prevoženi kilometri - relacija (km):', preserveLeadingSpaces: true },
              { text: distanceQunatity, alignment: 'center', preserveLeadingSpaces: true },
              { text: distancePrice, alignment: 'center', preserveLeadingSpaces: true },
              { text: distanceSum, alignment: 'center', preserveLeadingSpaces: true },
            ],
            [
              { text: 'Materialni stroški:', preserveLeadingSpaces: true },
              { text: materialQuantity, alignment: 'center', preserveLeadingSpaces: true },
              { text: materialPrice, alignment: 'center', preserveLeadingSpaces: true },
              { text: materialSum, alignment: 'center', preserveLeadingSpaces: true },
            ],
            [
              { text: 'Ostalo:', preserveLeadingSpaces: true },
              { text: otherQuantity, alignment: 'center', preserveLeadingSpaces: true },
              { text: otherPrice, alignment: 'center', preserveLeadingSpaces: true },
              { text: otherSum, alignment: 'center', preserveLeadingSpaces: true },
            ]
          ]
        },
        layout: { defaultBorder: true, }
      },
      {
        style: 'materialheader',
        table: {
          widths: [520],
          body: [ [ { text: 'MATERIAL: ', fillColor: 'silver' }, ], ]
        },
      },
      {
        style: 'material',
        table: {
          widths: [34, 290, 50, 50, 60],
          body: [
            [
              { text: 'Zap. št.', },
              { text: 'Naziv materiala', preserveLeadingSpaces: true },
              { text: 'Količina', alignment: 'center', preserveLeadingSpaces: true },
              { text: 'Cena', alignment: 'center', preserveLeadingSpaces: true },
              { text: 'Znesek', alignment: 'center', preserveLeadingSpaces: true },
            ],
            [
              { text: '1.', preserveLeadingSpaces: true },
              { text: name1, preserveLeadingSpaces: true },
              { text: quantity1, preserveLeadingSpaces: true },
              { text: price1, preserveLeadingSpaces: true },
              { text: sum1, preserveLeadingSpaces: true },
            ],
            [
              { text: '2.', preserveLeadingSpaces: true },
              { text: name2, preserveLeadingSpaces: true },
              { text: quantity2, preserveLeadingSpaces: true },
              { text: price2, preserveLeadingSpaces: true },
              { text: sum2, preserveLeadingSpaces: true },
            ],
            [
              { text: '3.', preserveLeadingSpaces: true },
              { text: name3, preserveLeadingSpaces: true },
              { text: quantity3, preserveLeadingSpaces: true },
              { text: price3, preserveLeadingSpaces: true },
              { text: sum3, preserveLeadingSpaces: true },
            ],
            [
              { text: '4.', preserveLeadingSpaces: true },
              { text: name4, preserveLeadingSpaces: true },
              { text: quantity4, preserveLeadingSpaces: true },
              { text: price4, preserveLeadingSpaces: true },
              { text: sum4, preserveLeadingSpaces: true },
            ],
            [
              { text: '5.', preserveLeadingSpaces: true },
              { text: name5, preserveLeadingSpaces: true },
              { text: quantity5, preserveLeadingSpaces: true },
              { text: price5, preserveLeadingSpaces: true },
              { text: sum5, preserveLeadingSpaces: true },
            ],
            [
              { text: '6.', preserveLeadingSpaces: true },
              { text: name6, preserveLeadingSpaces: true },
              { text: quantity6, preserveLeadingSpaces: true },
              { text: price6, preserveLeadingSpaces: true },
              { text: sum6, preserveLeadingSpaces: true },
            ],
            [
              { text: '7.', preserveLeadingSpaces: true },
              { text: name7, preserveLeadingSpaces: true },
              { text: quantity7, preserveLeadingSpaces: true },
              { text: price7, preserveLeadingSpaces: true },
              { text: sum7, preserveLeadingSpaces: true },
            ]
          ]
        },
        layout: { defaultBorder: true, }
      },
      {
        style: 'podpis',
        table: {
          widths: [81, 110, 35, 137, 110, 20],
          heights: ['auto', 40],
          body: [
            [
              { border: [false, false, false, false], text: 'Izvajalec naloge:' },
              { border: [false, false, false, true], text: userName, preserveLeadingSpaces: true },
              { border: [false, false, false, false], text: ' ', preserveLeadingSpaces: true },
              { border: [false, false, false, false], text: 'Pooblaščenec za naročnika:', },
              { border: [false, false, false, true], text: workOrder.representative, preserveLeadingSpaces: true },
              { border: [false, false, false, false], text: ' ', preserveLeadingSpaces: true },
            ],
            [
              { border: [false, false, false, false], text: '', },
              { border: [false, false, false, true], text: '', preserveLeadingSpaces: true },
              { border: [false, false, false, false], text: ' ', preserveLeadingSpaces: true },
              { border: [false, false, false, false], text: '', },
              { border: [false, false, false, true], text: '', preserveLeadingSpaces: true },
              { border: [false, false, false, false], text: ' ', preserveLeadingSpaces: true },
            ],
          ]
        },
        layout: { defaultBorder: true, }
      },
    ],
    images: {
      podpis: sign,
      roboteh: robotehImg.base64,
      kuka: kukaPartnerImg.base64,
    },
    styles: {
      header: { fontSize: 16, bold: true, margin: [10, -25, 0, 10] },
      kontaktHeader: { fontSize: 13, bold: false, margin: [0, 0, 0, 5] },
      kontaktTable: { fontSize: 8, bold: false, margin: [0, 20, 0, 5] },
      kontakt: { fontSize: 8, bold: false, margin: [0, -6, 0, 5] },
      tableExample: { margin: [-30, -15, 0, 15] },
      tableRoboteh: { margin: [10, -15, 0, 15] },
      tableKontaktHead: { margin: [ 300, -85, 0, 15] },
      tableKontakt: { margin: [ 300, -25, 0, 15] },
      tableKuka: { margin: [490, -87, 0, 15] },
      subproject: { fontSize: 11, margin: [10, -27, 0, 15] },
      description: { fontSize: 11, margin: [10, -15, 0, 15] },
      workers: { fontSize: 10, margin: [10, -15, 0, 15] },
      time: { fontSize: 10, margin: [10, -15, 0, 15] },
      konec: { margin: [10, -26, 0, 15] },
      zacetek: { margin: [10, 0, 0, 15] },
      stroskiheader: { fontSize: 11, bold: true, margin: [10, 0, 0, 10] },
      stroskiheaderPlus: { fontSize: 11, bold: false, margin: [352, -29, 0, 10] },
      stroski: { fontSize: 10, margin: [10, -11, 0, 15] },
      materialheader: { fontSize: 11, bold: true, margin: [10, 0, 0, 10] },
      material: { fontSize: 10, margin: [10, -11, 0, 15], alignment: 'center', },
      podpis: { fontSize: 11, margin: [10, 20, 0, 15] },
      noga: { fontSize: 10, margin: [-40, 1, 0, 15] },
      glava: { fontSize: 10, margin: [0, 0, 0, 15] },
      infotable: { fontSize: 11, margin: [130, -31, 0, 15] },
      typestable: { fontSize: 10, margin: [10, 0, 0, 15] },
      crta1: { margin: [ -40, -25, 0, 15] },
      crta2: { margin: [ -40, -52, 0, 15] },
      crta3: { margin: [ -40, -52, 0, 15] },
      tableHeader: { bold: true, fontSize: 13, color: 'black' }
    },
  };
}

module.exports = router;