var express = require('express');
var router = express.Router();

var dbProjects = require('../../model/apiv2/projects/dbProjectsAPI');
var dbReports = require('../../model/apiv2/reports/dbReportsAPI');

//get user projects (projects where users is part of)
router.get('/', function(req,res, next){
  //get token to verify user and get his userId
  let userId = res.locals.oauth.token.user.id;
  dbProjects.getAllProjectsAPI(userId).then(projects => {
    res.json({projects:projects});//200
  })
  .catch((e)=>{
    console.log(e);
    return res.status(500).json({
      status: 'error',
      message: 'Something went wrong',
      error: e,
    })
  })
})
//get user projects (projects where users is part of)
router.get('/:id', function(req,res, next){
  //get token to verify user and get his userId
  //let userId = res.locals.oauth.token.user.id;
  //res.json(res.locals.oauth.token.user);
  let projectId = req.params.id;
  dbProjects.getProjectAPI(projectId).then(project => {
    res.json({project:project[0]});//200
  })
  .catch((e)=>{
    console.log(e);
    return res.status(500).json({
      status: 'error',
      message: 'Something went wrong',
      error: e,
    })
  })
})
//get all user reports for selected project id
router.get('/reports/:id', function(req,res){
  //get token to verify user and get his userId
  //All report data (client, datetime, type:servis;montaža;ogled …, descriptions, location (JSON field - longitude/langitude), images (JSON)
  //let projectId = req.body.projectId;
  let userId = res.locals.oauth.token.user.id;
  let projectId = req.params.id;
  dbReports.getUserReportsForProjectAPI(userId,projectId).then(reports => {
    res.json({reports:reports});//200
  })
  .catch((e)=>{
    console.log(e);
    return res.status(500).json({
      status: 'error',
      message: 'Something went wrong',
      error: e,
    })
  })
})
module.exports = router;