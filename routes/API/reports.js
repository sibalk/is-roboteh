var express = require('express');
var router = express.Router();

//var dbProjects = require('../../model/API/projects/dbProjectsAPI');
let dbReports = require('../../model/apiv2/reports/dbReportsAPI');
let dbChanges = require('../../model/changes/dbChanges');

//get all report info for specific report id
router.get('/types', function(req,res){
  //get token to verify user and get his userId
  //All report data (client, datetime, type:servis;montaža;ogled …, descriptions, location (JSON field - longitude/langitude), images (JSON)
  dbReports.getReportTypesAPI().then(reportTypes => {
    res.json({reportTypes:reportTypes});//200
  })
  .catch((e)=>{
    console.log(e);
    return res.status(500).json({
      status: 'error',
      message: 'Something went wrong',
      error: e,
    })
  })
})
//get all user reports 
router.get('/', function(req,res){
  //get token to verify user and get his userId
  //All report data (client, datetime, type:servis;montaža;ogled …, descriptions, location (JSON field - longitude/langitude), images (JSON)
  let userId = res.locals.oauth.token.user.id;
  dbReports.getUserReportsAPI(userId).then(reports => {
    res.json({reports:reports});//200
  })
  .catch((e)=>{
    console.log(e);
    return res.status(500).json({
      status: 'error',
      message: 'Something went wrong',
      error: e,
    })
  })
})
//get all report info for specific report id
router.get('/:id', function(req,res){
  //get token to verify user and get his userId
  //All report data (client, datetime, type:servis;montaža;ogled …, descriptions, location (JSON field - longitude/langitude), images (JSON)
  let reportId = req.params.id;
  dbReports.getReportAPI(reportId).then(report => {
    res.json({report:report[0]});//200
  })
  .catch((e)=>{
    console.log(e);
    return res.status(500).json({
      status: 'error',
      message: 'Something went wrong',
      error: e,
    })
  })
})
//create report
router.post('/', function(req,res){
  //get token to verify user and get his userId
  //All report data (client, datetime, type:servis;montaža;ogled …, descriptions, location (JSON field - longitude/langitude), images (JSON)
  let userId = res.locals.oauth.token.user.id;
  let projectId = req.body.projectId;
  let other = req.body.other;
  let date = req.body.date;
  let start = req.body.start;
  let finish = req.body.finish;
  let description = req.body.description;
  let type = req.body.type;

  if(!(projectId || other) || !date || !start || !finish || !description || !type)
    res.sendStatus(400);
  else{
    let radio = 0;
    if(!projectId)
      radio = 1;
    let time;
    if(req.body.time)
      time = req.body.time;
    else{
      var ary1=start.split(':'),ary2=finish.split(':');
      var minsdiff=parseInt(ary2[0],10)*60+parseInt(ary2[1],10)-parseInt(ary1[0],10)*60-parseInt(ary1[1],10);
      //console.log(String(100+Math.floor(minsdiff/60)).substr(1)+':'+String(100+minsdiff%60).substr(1));
      time = String(100+Math.floor(minsdiff/60)).substr(1)+':'+String(100+minsdiff%60).substr(1);
      if(minsdiff < 0)
        time = '00:00';
    }
    dbReports.addNewReport(userId,date,time,start,finish,projectId,type,other,description,radio).then(report => {
      //add change to db -> change system
      dbChanges.addNewChangeSystem(1,25,userId,null,null,null,null,null,null,null,null,null,null,null,null,null,report.id)
    .then(sysChange => {
      console.log("Uporabnik " + userId + " je preko mobilne aplikacije naredil spremembo tipa 25 s statusom 1 za poročilo " + report.id + ".");
      return res.status(201).json({report:report});
    })
    .catch((e)=>{
      return res.json({success:false, error:e})
    })
    })
    .catch((e)=>{
      console.log(e);
      return res.status(500).json({
        status: 'error',
        message: 'Something went wrong',
        error: e
      })
    })
  }
})
//update selected report
router.put('/:id', function(req,res){
  let reportId = req.params.id;
  let userId = res.locals.oauth.token.user.id;
  let projectId = req.body.projectId;
  let other = req.body.other;
  let date = req.body.date;
  let start = req.body.start;
  let finish = req.body.finish;
  let description = req.body.description;
  let type = req.body.type;
  
  if(!(projectId || other) || !date || !start || !finish || !description || !type || !reportId)
    res.sendStatus(400);
  else{
    let radio = 0;
    if(!projectId)
      radio = 1;
    let time;
    if(req.body.time)
      time = req.body.time;
    else
      time = "11:00";
    dbReports.editReport(reportId,userId,date,time,start,finish,projectId,type,other,description,radio).then(report => {
      //add change to db -> change system
      dbChanges.addNewChangeSystem(2,25,userId,null,null,null,null,null,null,null,null,null,null,null,null,null,reportId)
    .then(sysChange => {
      console.log("Uporabnik " + userId + " je preko mobilne aplikacije naredil spremembo tipa 25 s statusom 2 za poročilo " + reportId + ".");
      return res.json({report:report});
    })
    .catch((e)=>{
      return res.json({success:false, error:e})
    })
    })
    .catch((e)=>{
      console.log(e);
      return res.status(500).json({
        status: 'error',
        message: 'Something went wrong',
        error: e,
      })
    })
  }
})
//delete report (update active to false)
  router.delete('/:id', function(req,res){
  //get token to verify user and get his userId
  let reportId = req.params.id;
  let userId = res.locals.oauth.token.user.id;
  dbReports.deleteReport(reportId).then(report => {
    //add change to db -> change system
    dbChanges.addNewChangeSystem(3,25,userId,null,null,null,null,null,null,null,null,null,null,null,null,null,reportId)
    .then(sysChange => {
      console.log("Uporabnik " + userId + " je preko mobilne aplikacije naredil spremembo tipa 25 s statusom 3 za poročilo " + reportId + ".");
      return res.json({report:report});
    })
    .catch((e)=>{
      return res.json({success:false, error:e})
    })
  })
  .catch((e)=>{
    console.log(e);
    return res.status(500).json({
      status: 'error',
      message: 'Something went wrong',
      error: e,
    })
  })
})
module.exports = router;