var express = require('express');
var router = express.Router();
const csp = require('helmet-csp');

//auth & dbModels
var auth = require('../controllers/authentication');
var dbChanges = require('../model/changes/dbChanges');
var dbMessages = require('../model/messages/dbMessages');

var normalCspHandler = csp({
  directives: {
    defaultSrc: ["'self'"],
    scriptSrc: ["'self'"],
    styleSrc: ["'self'", "https://fonts.googleapis.com", "https://use.fontawesome.com"],
    fontSrc: ["'self'", "https://fonts.gstatic.com", "https://use.fontawesome.com"],
    imgSrc: ["'self'", "data:"],
    frameAncestors: ["'self'"],
  }
});

//page for seeing history of changes meant for you
router.get('/ctrlpanel', normalCspHandler, auth.authenticate, function(req, res){
  //let userId = req.session.user_id;
  res.render('control_changes', {
    title: "Pregled vseh obvestil",
    user_name: req.session.user_name,
    user_surname: req.session.user_surname,
    user_type: req.session.type,
    user_id: req.session.user_id,
    changes: [],
  })
})

//get all my unseen changes (active)
router.get('/get', auth.authenticate, function(req, res, next){
  let userId = req.session.user_id;
  let active = true;
  let start,finish,type,status,reverse;
  let first = req.query.first;
  if(req.query.active){
    if(req.query.active == "all")
      active = null;
    else
      active = req.query.active;
  }
  start = req.query.start;
  finish = req.query.finish;
  type = req.query.type;
  status = req.query.status;
  let roleType = req.session.type.toLowerCase();
  if (roleType == 'admin' || roleType.substring(0,5) == 'vodja' || roleType == 'tajnik' || roleType == 'komercialist' || roleType == 'komerciala' || roleType == 'računovodstvo'){
    if(req.query.userId)
      userId = req.query.userId;
    reverse = req.query.rev;
  }
  if(first == 'true') {
    //console.log('pridobi zadnjih 100');
    dbChanges.getUserNotifyLast100(userId)
    .then(changes=>{
      res.json({success:true, data:changes});
    })
    .catch((e)=>{
      return res.json({success:false, error:e});
    })
  }
  else{
    //dbChanges.getUserChanges(userId,start,finish,type,status,active,reverse)
    dbChanges.getUserNotify(userId,start,finish,type,status,active,reverse)
    .then(changes=>{
      res.json({success:true, data:changes});
    })
    .catch((e)=>{
      return res.json({success:false, error:e});
    })
  }
})
//dismiss selected change
router.post('/dismiss', auth.authenticate, function(req, res, next){
  let changeId = req.body.id;
  //dbChanges.dismissChange(changeId).then(data=>{
  dbChanges.dismissNotify(changeId).then(data=>{
    console.log("Uporabnik "+req.session.user_id+" je opazil spremembo "+changeId+".");
    res.json({success:true});
  })
  .catch((e)=>{
    return res.json({success:false, error:e});
  })
})
//modify selected change
router.post('/modify', auth.authenticate, function(req, res, next){
  let changeId = req.body.id;
  let active = req.body.active;
  let changeForUser = req.body.forUser;
  let activity = true;
  if(active == 'true')
    activity = false;
  else if(active == 'false')
    activity = true;
  if(changeForUser == req.session.user_id){
    //changing his own notification --> dismiss/modify (depends if he is unseeing his own notification)
    if(!activity){
      //dbChanges.dismissChange(changeId).then(data=>{
      dbChanges.dismissNotify(changeId).then(data=>{
        console.log("Uporabnik "+req.session.user_id+" je opazil spremembo "+changeId+".");
        res.json({success:true, type:0});
      })
      .catch((e)=>{
        return res.json({success:false, error:e});
      })
    }
    else{
      //dbChanges.modifyChange(changeId, req.session.user_id, activity).then(data=>{
      dbChanges.modifyNotify(changeId, req.session.user_id, activity).then(data=>{
        console.log("Uporabnik "+req.session.user_id+" je spremenil svojo spremembo "+changeId+".");
        res.json({success:true, type:1});
      })
      .catch((e)=>{
        return res.json({success:false, error:e});
      })
    }
  }
  else{
    let roleType = req.session.type.toLowerCase();
    //changing someone else notification --> modify
    if(roleType == 'admin' || roleType.substring(0,5) == 'vodja' || roleType == 'tajnik' || roleType == 'komercialist' || roleType == 'komerciala' || roleType == 'računovodstvo'){
      //user is allowed to modify change
      dbChanges.modifyNotify(changeId, req.session.user_id, activity).then(data=>{
        console.log("Uporabnik "+req.session.user_id+" je spremenil spremembo "+changeId+" za uporabnika "+changeForUser+".");
        res.json({success:true, type:2});
      })
      .catch((e)=>{
        return res.json({success:false, error:e});
      })
    }
    else{
      res.json({success:false});
    }
  }
})
//add a special notification
router.post('/special', auth.authenticate, function(req, res, next){
  let userId = req.session.user_id;
  let note = req.body.note;
  let notify = JSON.parse(req.body.notify);
  let controlAll = req.body.control;
  //console.log(notify);
  //console.log(JSON.parse(notify));
  //console.log(note);
  let roleType = req.session.type.toLowerCase();
  let promises = [];
  if(roleType == 'admin' || roleType.substring(0,5) == 'vodja' || roleType == 'tajnik' || roleType == 'komercialist' || roleType == 'komerciala' || roleType == 'računovodstvo'){
    if(controlAll == 'true'){
      dbMessages.addNewMessage(note,userId).then(message => {
        if(message){
          notify = notify.filter(u => u.id != userId);
          dbChanges.addNewChangeSystem(1,6,userId,null,null,null,null,null,message.id).then(sysChange=>{
            for(let i = 0; i < notify.length; i++){
              promises.push(dbChanges.addNewChangeNotify(sysChange.id,notify[i].id));
              //promises.push(dbChanges.addNewChange(6,1,notify[i].id,userId,null,null,null,message.id));
            }
            Promise.all(promises)
            .then((results) => {
              console.log("Uporabnik " + userId + " je uspešno dodal pomembno obvestilo za vse uporabnike.");
              return res.json({success:true, data:results, note:message, change:sysChange});
            })
            .catch((e)=>{
              return res.json({success:false, error:e});
            })
          })
          .catch((e)=>{
            return res.json({success:false, error:e});
          })
        }
        else{
          res.json({success:false});      
        }
      })
      .catch((e)=>{
        return res.json({success:false, error:e});
      })
    }
    else if(controlAll == 'false'){
      dbMessages.addNewMessage(note,userId).then(message => {
        if(message){
          dbChanges.addNewChangeSystem(1,6,userId,null,null,null,null,null,message.id).then(sysChange=>{
            for(let i = 0; i < notify.length; i++){
              promises.push(dbChanges.addNewChangeNotify(sysChange.id,notify[i]));
              //promises.push(dbChanges.addNewChange(6,1,notify[i],userId,null,null,null,message.id));
            }
            Promise.all(promises)
            .then((results) => {
              console.log("Uporabnik " + userId + " je uspešno dodal pomembno obvestilo za določene uporabnike.");
              return res.json({success:true, data:results, note:message, change:sysChange});
            })
            .catch((e)=>{
              return res.json({success:false, error:e});
            })
          })
          .catch((e)=>{
            return res.json({success:false, error:e});
          })
        }
        else{
          res.json({success:false});      
        }
      })
      .catch((e)=>{
        return res.json({success:false, error:e});
      })
    }
  }
  else{
    res.json({success:false});
  }
})
//add new change
router.post('/add', auth.authenticate, function(req, res, next){
  let userId = req.session.user_id;
  let type = req.body.type ? req.body.type : null;
  let status = req.body.status ? req.body.status : null;
  let projectId = req.body.projectId ? req.body.projectId : null;
  let taskId = req.body.taskId ? req.body.taskId : null;
  let subtaskId = req.body.subtaskId ? req.body.subtaskId : null;
  let noteId = req.body.noteId ? req.body.noteId : null;
  let absenceId = req.body.absenceId ? req.body.absenceId : null;
  let fileId = req.body.fileId ? req.body.fileId : null;
  let subscriberId = req.body.subscriberId ? req.body.subscriberId : null;
  let workOrderId = req.body.workOrderId ? req.body.workOrderId : null;
  let workOrderFileId = req.body.workOrderFileId ? req.body.workOrderFileId : null;
  let reportId = req.body.reportId ? req.body.reportId : null;
  let forUserId = req.body.userId ? req.body.userId : null;
  let roleId = req.body.roleId ? req.body.roleId : null;
  let workRoleId = req.body.workRoleId ? req.body.workRoleId : null;
  let buildPartId = req.body.buildPartId ? req.body.buildPartId : null;
  let buildPhaseId = req.body.buildPhaseId ? req.body.buildPhaseId : null;
  let meetingId = req.body.meetingId ? req.body.meetingId : null;
  let activityId = req.body.activityId ? req.body.activityId : null;
  let modificationId = req.body.modificationId ? req.body.modificationId : null;
  let modStatusId = req.body.modStatusId ? req.body.modStatusId : null;
  let compSubId = req.body.compSubId ? req.body.compSubId : null;
  let compSupId = req.body.compSupId ? req.body.compSupId : null;
  let compFileId = req.body.compFileId ? req.body.compFileId : null;
  let compStatusId = req.body.compStatusId ? req.body.compStatusId : null;
  let projectDateId = req.body.projectDateId ? req.body.projectDateId : null;
  let stdCompId = req.body.stdCompId ? req.body.stdCompId : null;
  let promises = [];
  if(type == 1){
    //project change
    dbChanges.getProjectNotifyUsers(projectId,userId).then(users => {
      let notify = [];
      if(users) notify = users;
      dbChanges.addNewChangeSystem(status,type,userId,projectId).then(sysChange => {
        console.log("Uporabnik " + userId + " je naredil spremembe tipa " + type + " s statusom " + status + " na projektu " + projectId + ".");
        for(let i = 0; i < notify.length; i++){
          //console.log("Uporabnik " + userId + " je naredil spremembe za " + notify[i].id_user + " tipa " + type + " s statusom "+status+" na projektu "+projectId+".");
          promises.push(dbChanges.addNewChangeNotify(sysChange.id,notify[i].id_user));
          //promises.push(dbChanges.addNewChange(type,status,notify[i].id_user,userId,projectId))
        }
        Promise.all(promises)
        .then((results) => {
          //console.log("all done", results);
          return res.json({success:true, data:results, change:sysChange});
        })
        .catch((e)=>{
          return res.json({success:false, error:e});
        })
      })
      .catch((e)=>{
        return res.json({success:false, error:e});
      })
    })
    .catch((e)=>{
      return res.json({success:false, error:e});
    })
  }
  else if(type == 2){
    //task change
    dbChanges.getTaskNotifyUsers(projectId,taskId,userId).then(users => {
      let notify = [];
      let leaders = [];
      if(users.task_workers)
        notify = users.task_workers.split(",");
      if(users.project_workers)
        leaders = users.project_workers.split(",");
      for(let i = 0; i < leaders.length; i++){
        var x = notify.find(u => parseInt(u) == parseInt(leaders[i]));
        if(!x)
          notify.push(leaders[i]);
      }
      dbChanges.addNewChangeSystem(status,type,userId,projectId,taskId).then(sysChange => {
        console.log("Uporabnik " + userId + " je naredil spremembo tipa " + type + " s statusom " + status + " za opravilo " + taskId + " na projektu " + projectId + ".");
        for(let i = 0; i < notify.length; i++){
          //console.log("Uporabnik " + userId + " je naredil spremembe za " + notify[i] + " tipa " + type + " s statusom "+status+" za opravilo "+taskId+" na projektu "+projectId+".");
          promises.push(dbChanges.addNewChangeNotify(sysChange.id,notify[i]));
        }
        Promise.all(promises)
        .then((results) => {
          //console.log("all done", results);
          return res.json({success:true, data:results, change:sysChange})
        })
        .catch((e)=>{
          return res.json({success:false, error:e});
        })
      })
      .catch((e)=>{
        return res.json({success:false, error:e});
      })
    })
    .catch((e)=>{
      return res.json({success:false, error:e});
    })
  }
  else if(type == 3){
    //subtask change
    dbChanges.getTaskNotifyUsers(projectId,taskId,userId).then(users => {
      let notify = [];
      let leaders = [];
      if(users.task_workers)
        notify = users.task_workers.split(",");
      if(users.project_workers)
        leaders = users.project_workers.split(",");
      for(let i = 0; i < leaders.length; i++){
        var x = notify.find(u => parseInt(u) == parseInt(leaders[i]));
        if(!x)
          notify.push(leaders[i]);
      }
      dbChanges.addNewChangeSystem(status,type,userId,projectId,taskId,subtaskId).then(sysChange => {
        console.log("Uporabnik " + userId + " je naredil spremembo tipa " + type + " s statusom " + status + " za nalogo " + subtaskId + " pri opravilu " + taskId + " na projektu " + projectId + ".");
        for(let i = 0; i < notify.length; i++){
          //console.log("Uporabnik " + userId + " je naredil spremembe za " + notify[i] + " tipa " + type + " s statusom "+status+" za nalogo " + subtaskId + " pri opravilu "+taskId+" na projektu "+projectId+".");
          promises.push(dbChanges.addNewChangeNotify(sysChange.id,notify[i]));
        }
        Promise.all(promises)
        .then((results) => {
          //console.log("all done", results);
          return res.json({success:true, data:results})
        })
        .catch((e)=>{
          return res.json({success:false, error:e});
        })
      })
      .catch((e) => {
        return res.json({success:false, error:e});
      })
    })
    .catch((e)=>{
      return res.json({success:false, error:e});
    })
  }
  else if(type == 4){
    //note change
    if(subtaskId){
      //subtasknote
      dbChanges.getTaskNotifyUsers(projectId,taskId,userId).then(users => {
        let notify = [];
        let leaders = [];
        if(users.task_workers)
          notify = users.task_workers.split(",");
        if(users.project_workers)
          leaders = users.project_workers.split(",");
        for(let i = 0; i < leaders.length; i++){
          var x = notify.find(u => parseInt(u) == parseInt(leaders[i]));
          if(!x)
            notify.push(leaders[i]);
        }
        dbChanges.addNewChangeSystem(status,type,userId,projectId,taskId,subtaskId,null,null,noteId).then(sysChange => {
          console.log("Uporabnik " + userId + " je naredil spremembo tipa " + type + " s statusom " + status + " za nalogo " + subtaskId + " pri opravilu " + taskId + " na projektu " + projectId + ".");
          for(let i = 0; i < notify.length; i++){
            //console.log("Uporabnik " + userId + " je naredil spremembe za " + notify[i] + " tipa " + type + " s statusom "+status+" za nalogo " + subtaskId + " pri opravilu "+taskId+" na projektu "+projectId+".");
            promises.push(dbChanges.addNewChangeNotify(sysChange.id,notify[i]));
          }
          Promise.all(promises)
          .then((results) => {
            //console.log("all done", results);
            return res.json({success:true, data:results, change:sysChange})
          })
          .catch((e)=>{
            return res.json({success:false, error:e});
          })
        })
        .catch((e)=>{
          return res.json({success:false, error:e});
        })
      })
      .catch((e)=>{
        return res.json({success:false, error:e});
      })
    }
    else{
      //task note
      dbChanges.getTaskNotifyUsers(projectId,taskId,userId).then(users => {
        let notify = [];
        let leaders = [];
        if(users.task_workers)
          notify = users.task_workers.split(",");
        if(users.project_workers)
          leaders = users.project_workers.split(",");
        for(let i = 0; i < leaders.length; i++){
          var x = notify.find(u => parseInt(u) == parseInt(leaders[i]));
          if(!x)
            notify.push(leaders[i]);
        }
        dbChanges.addNewChangeSystem(status,type,userId,projectId,taskId,null,null,null,noteId).then(sysChange => {
          console.log("Uporabnik " + userId + " je naredil spremembe tipa " + type + " s statusom " + status + " za opravilo " + taskId + " na projektu " + projectId + ".");
          for(let i = 0; i < notify.length; i++){
            //console.log("Uporabnik " + userId + " je naredil spremembe za " + notify[i] + " tipa " + type + " s statusom "+status+" za opravilo "+taskId+" na projektu "+projectId+".");
            promises.push(dbChanges.addNewChangeNotify(sysChange.id,notify[i]));
          }
          Promise.all(promises)
          .then((results) => {
            //console.log("all done", results);
            return res.json({success:true, data:results, change:sysChange});
          })
          .catch((e)=>{
            return res.json({success:false, error:e});
          })
        })
        .catch((e)=>{
          return res.json({success:false, error:e});
        })
      })
      .catch((e)=>{
        return res.json({success:false, error:e});
      })
    }
  }
  else if(type == 5){
    //absence change
    console.log(status)
    if(projectId && taskId){
      //project absence
      dbChanges.addNewChangeSystem(status,type,userId,projectId,taskId,null,null,null,null,null,absenceId).then(sysChange => {
        console.log("Uporabnik " + userId + " je dodal spremembo(projektna odsotnost) tipa " + type + " s statusom " + status + " za opravilo " + taskId + " na projektu " + projectId + ".");
        return res.json({success:true, change:sysChange});
      })
      .catch((e)=>{
        return res.json({success:false, error:e});
      })
    }
    else{
      //general absence
      dbChanges.addNewChangeSystem(status,type,userId,null,null,null,null,null,null,null,absenceId).then(sysChange => {
        console.log("Uporabnik " + userId + " je dodal spremembo(splošna odsotnost) tipa " + type + " s statusom " + status + ".");
        return res.json({success:true, change:sysChange});
      })
      .catch((e)=>{
        return res.json({success:false, error:e});
      })
    }
  }
  else if(type == 6){
    //special note, has its own route
  }
  else if(type == 7){
    //file change
    if(status == 1){
      if(taskId){
        //file for task
        dbChanges.getTaskNotifyUsers(projectId,taskId,userId).then(users => {
          let notify = [];
          let leaders = [];
          if(users.task_workers)
            notify = users.task_workers.split(",");
          if(users.project_workers)
            leaders = users.project_workers.split(",");
          for(let i = 0; i < leaders.length; i++){
            var x = notify.find(u => parseInt(u) == parseInt(leaders[i]));
            if(!x)
              notify.push(leaders[i]);
          }
          dbChanges.addNewChangeSystem(status,type,userId,projectId,taskId,null,null,null,null,null,null,fileId).then(sysChange =>{
            console.log("Uporabnik " + userId + " je naredil spremembe tipa " + type + " s statusom " + status + " za opravilo " + taskId + " na projektu " + projectId + ".");
            for(let i = 0; i < notify.length; i++){
              //console.log("Uporabnik " + userId + " je naredil spremembe za " + notify[i] + " tipa " + type + " s statusom "+status+" za opravilo "+taskId+" na projektu "+projectId+".");
              promises.push(dbChanges.addNewChangeNotify(sysChange.id,notify[i]));
            }
            Promise.all(promises)
            .then((results) => {
              //console.log("all done", results);
              return res.json({success:true, data:results, change:sysChange});
            })
            .catch((e)=>{
              return res.json({success:false, error:e});
            })
          })
          .catch((e)=>{
            return res.json({success:false, error:e});
          })
        })
        .catch((e)=>{
          return res.json({success:false, error:e});
        })
      }
      else{
        //file for project
        dbChanges.getProjectNotifyLeaders(projectId,userId).then(users => {
          let notify = [];
          if(users) notify = users;
          dbChanges.addNewChangeSystem(status,type,userId,projectId,null,null,null,null,null,null,null,fileId).then(sysChange => {
            console.log("Uporabnik " + userId + " je naredil spremembo tipa " + type + " s statusom " + status + " na projektu " + projectId + ".");
            for(let i = 0; i < notify.length; i++){
              //console.log("Uporabnik " + userId + " je naredil spremembe za " + notify[i].id_user + " tipa " + type + " s statusom "+status+" na projektu "+projectId+".");
              promises.push(dbChanges.addNewChangeNotify(sysChange.id,notify[i].id_user));
            }
            Promise.all(promises)
            .then((results) => {
              //console.log("all done", results);
              return res.json({success:true, data:results, change:sysChange});
            })
            .catch((e)=>{
              return res.json({success:false, error:e});
            })
          })
          .catch((e)=>{
            return res.json({success:false, error:e});  
          })
        })
        .catch((e)=>{
          return res.json({success:false, error:e});
        })
      }
    }
    else{
      dbChanges.addNewChangeSystem(status,type,userId,projectId,taskId,null,null,null,null,null,null,fileId).then(sysChange => {
        console.log("Uporabnik " + userId + " je naredil spremembo tipa " + type + " s statusom " + status + " na projektu " + projectId + ".");
        return res.json({success:true, change:sysChange});
      })
      .catch((e)=>{
        return res.json({success:false, error:e});  
      })
    }
  }
  else if(type == 8){
    //subscriber change
    dbChanges.addNewChangeSystem(status,type,userId,null,null,null,null,null,null,subscriberId).then(sysChange => {
      console.log("Uporabnik " + userId + " je naredil spremembo tipa " + type + " s statusom " + status + " za naročnika " + subscriberId + ".");
      return res.json({success:true, change:sysChange});
    })
    .catch((e) => {
      return res.json({success:false, error:e});
    })
  }
  else if(type == 9){
    //servis change
    dbChanges.getServisNotifyUsers(taskId,userId).then(users => {
      let notify = [];
      let leaders = [];
      if(users.task_workers)
        notify = users.task_workers.split(",");
      if(users.project_workers)
        leaders = users.project_workers.split(",");
      for(let i = 0; i < leaders.length; i++){
        var x = notify.find(u => parseInt(u) == parseInt(leaders[i]));
        if(!x)
          notify.push(leaders[i]);
      }
      dbChanges.addNewChangeSystem(status,type,userId,null,taskId,null,null,null,null,subscriberId).then(sysChange => {
        console.log("Uporabnik " + userId + " je naredil spremembo tipa " + type + " s statusom " + status + " za servis " + taskId + " za naročnika " + subscriberId + ".");
        for(let i = 0; i < notify.length; i++){
          //console.log("Uporabnik " + userId + " je naredil spremembe za " + notify[i] + " tipa " + type + " s statusom "+status+" za servis "+taskId+" za naročnika "+subscriberId+".");
          promises.push(dbChanges.addNewChangeNotify(sysChange.id,notify[i]));
        }
        Promise.all(promises)
        .then((results) => {
          //console.log("all done", results);
          return res.json({success:true, data:results, change:sysChange});
        })
        .catch((e)=>{
          return res.json({success:false, error:e});
        })
      })
      .catch((e) => {
        return res.json({success:false, error:e});
      })
    })
    .catch((e)=>{
      return res.json({success:false, error:e});
    })
  }
  else if(type == 10){
    //servis's subtask change
    dbChanges.getServisNotifyUsers(taskId,userId).then(users => {
      let notify = [];
      let leaders = [];
      if(users.task_workers)
        notify = users.task_workers.split(",");
      if(users.project_workers)
        leaders = users.project_workers.split(",");
      for(let i = 0; i < leaders.length; i++){
        var x = notify.find(u => parseInt(u) == parseInt(leaders[i]));
        if(!x)
          notify.push(leaders[i]);
      }
      dbChanges.addNewChangeSystem(status,type,userId,null,taskId,subtaskId,null,null,null,subscriberId).then(sysChange => {
        console.log("Uporabnik " + userId + " je naredil spremembo tipa " + type + " s statusom " + status + " za nalogo " + subtaskId + " pri servisu " + taskId + " za naročnika " + subscriberId + ".");
        for(let i = 0; i < notify.length; i++){
          //console.log("Uporabnik " + userId + " je naredil spremembe za " + notify[i] + " tipa " + type + " s statusom "+status+" za nalogo " + subtaskId + " pri servisu "+taskId+" za naročnika "+subscriberId+".");
          promises.push(dbChanges.addNewChangeNotify(sysChange.id,notify[i]));
        }
        Promise.all(promises)
        .then((results) => {
          //console.log("all done", results);
          return res.json({success:true, data:results, change:sysChange});
        })
        .catch((e)=>{
          return res.json({success:false, error:e});
        })
      })
      .catch((e) => {
        return res.json({success:false, error:e});
      })
    })
    .catch((e)=>{
      return res.json({success:false, error:e});
    })
  }
  else if(type == 11){
    //servis's note change
    if(subtaskId){
      //subtasknote
      dbChanges.getServisNotifyUsers(taskId,userId).then(users => {
        let notify = [];
        let leaders = [];
        if(users.task_workers)
          notify = users.task_workers.split(",");
        if(users.project_workers)
          leaders = users.project_workers.split(",");
        for(let i = 0; i < leaders.length; i++){
          var x = notify.find(u => parseInt(u) == parseInt(leaders[i]));
          if(!x)
            notify.push(leaders[i]);
        }
        dbChanges.addNewChangeSystem(status,type,userId,null,taskId,subtaskId,null,null,noteId,subscriberId).then(sysChange=>{
          console.log("Uporabnik " + userId + " je naredil spremembo tipa " + type + " s statusom " + status + " za nalogo " + subtaskId + " pri servisu " + taskId + " za naročnika " + subscriberId + ".");
          for(let i = 0; i < notify.length; i++){
            //console.log("Uporabnik " + userId + " je naredil spremembe za " + notify[i] + " tipa " + type + " s statusom "+status+" za nalogo " + subtaskId + " pri servisu "+taskId+" za naročnika "+subscriberId+".");
            promises.push(dbChanges.addNewChangeNotify(sysChange.id,notify[i]));
          }
          Promise.all(promises)
          .then((results) => {
            //console.log("all done", results);
            return res.json({success:true, data:results, change:sysChange});
          })
          .catch((e)=>{
            return res.json({success:false, error:e});
          })
        })
        .catch((e)=>{
          return res.json({success:false, error:e});
        })
      })
      .catch((e)=>{
        return res.json({success:false, error:e});
      })
    }
    else{
      //task note
      dbChanges.getServisNotifyUsers(taskId,userId).then(users => {
        let notify = [];
        let leaders = [];
        if(users.task_workers)
          notify = users.task_workers.split(",");
        if(users.project_workers)
          leaders = users.project_workers.split(",");
        for(let i = 0; i < leaders.length; i++){
          var x = notify.find(u => parseInt(u) == parseInt(leaders[i]));
          if(!x)
            notify.push(leaders[i]);
        }
        dbChanges.addNewChangeSystem(status,type,userId,null,taskId,null,null,null,noteId,subscriberId).then(sysChange => {
          console.log("Uporabnik " + userId + " je naredil spremembo tipa " + type + " s statusom " + status + " pri servisu " + taskId + " za naročnika " + subscriberId + ".");
          for(let i = 0; i < notify.length; i++){
            //console.log("Uporabnik " + userId + " je naredil spremembe za " + notify[i] + " tipa " + type + " s statusom "+status+" pri servisu "+taskId+" za naročnika "+subscriberId+".");
            promises.push(dbChanges.addNewChangeNotify(sysChange.id,notify[i]));
          }
          Promise.all(promises)
          .then((results) => {
            //console.log("all done", results);
            return res.json({success:true, data:results, change:sysChange});
          })
          .catch((e)=>{
            return res.json({success:false, error:e});
          })
        })
        .catch((e) => {
          return res.json({success:false, error:e});
        })
      })
      .catch((e)=>{
        return res.json({success:false, error:e});
      })
    }
  }
  else if(type == 12){
    //servis's absence change
    dbChanges.addNewChangeSystem(status,type,userId,null,taskId,null,null,null,null,subscriberId,absenceId).then(sysChange => {
      console.log("Uporabnik " + userId + " je dodal spremembo(servisna odsotnost) tipa " + type + " s statusom " + status + " za opravilo(servis) " + taskId + ".");
      return res.json({success:true, change:sysChange});
    })
    .catch((e)=>{
      return res.json({success:false, error:e});
    })
  }
  else if(type == 13){
    //servis's file change
    if(status == 1){
      dbChanges.getServisNotifyUsers(taskId,userId).then(users => {
        let notify = [];
        let leaders = [];
        if(users.task_workers)
          notify = users.task_workers.split(",");
        if(users.project_workers)
          leaders = users.project_workers.split(",");
        for(let i = 0; i < leaders.length; i++){
          var x = notify.find(u => parseInt(u) == parseInt(leaders[i]));
          if(!x)
            notify.push(leaders[i]);
        }
        dbChanges.addNewChangeSystem(status,type,userId,null,taskId,null,null,null,null,subscriberId,null,fileId).then(sysChange =>{
          console.log("Uporabnik " + userId + " je naredil spremembo tipa " + type + " s statusom " + status + " pri servisu " + taskId + " za naročnika " + subscriberId + ".");
          for(let i = 0; i < notify.length; i++){
            //console.log("Uporabnik " + userId + " je naredil spremembe za " + notify[i] + " tipa " + type + " s statusom "+status+" pri servisu "+taskId+" za naročnika "+subscriberId+".");
            promises.push(dbChanges.addNewChangeNotify(sysChange.id,notify[i]));
          }
          Promise.all(promises)
          .then((results) => {
            //console.log("all done", results);
            return res.json({success:true, data:results, change:sysChange});
          })
          .catch((e)=>{
            return res.json({success:false, error:e});
          })
        })
        .catch((e)=>{
          return res.json({success:false, error:e});
        })
      })
      .catch((e)=>{
        return res.json({success:false, error:e});
      })
    }
    else{
      dbChanges.addNewChangeSystem(status,type,userId,null,taskId,null,null,null,null,subscriberId,null,fileId).then(sysChange =>{
        console.log("Uporabnik " + userId + " je naredil spremembo tipa " + type + " s statusom " + status + " pri servisu " + taskId + " za naročnika " + subscriberId + ".");
        return res.json({success:true, change:sysChange});
      })
      .catch((e)=>{
        return res.json({success:false, error:e});
      })
    }
  }
  else if(type == 14){
    //user change
    dbChanges.addNewChangeSystem(status,type,userId,null,null,null,null,null,null,null,null,null,null,null,null,null,null,forUserId)
    .then(sysChange => {
      console.log("Uporabnik " + userId + " je naredil spremembo tipa " + type + " s statusom " + status + " za uporabnika " + forUserId + ".");
      return res.json({success:true, change:sysChange});
    })
    .catch((e)=>{
      return res.json({success:false, error:e})
    })
  }
  else if(type == 15){
    //system role change
    //can be added new role or changed for user
    if(status == 1){//adding new role
      dbChanges.addNewChangeSystem(status,type,userId,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,roleId)
      .then(sysChange => {
        console.log("Uporabnik " + userId + " je naredil spremembo tipa " + type + " s statusom " + status + ".");
        return res.json({success:true, change:sysChange});
      })
      .catch((e)=>{
        return res.json({success:false, error:e})
      })
    }
    else if(status == 2){//changing role for user
      dbChanges.addNewChangeSystem(status,type,userId,null,null,null,null,null,null,null,null,null,null,null,null,null,null,forUserId,null,roleId)
      .then(sysChange => {
        console.log("Uporabnik " + userId + " je naredil spremembo tipa " + type + " s statusom " + status + " za uporabnika " + forUserId + ".");
        return res.json({success:true, change:sysChange});
      })
      .catch((e)=>{
        return res.json({success:false, error:e})
      })
    }
  }
  else if(type == 16){
    //work role change
    dbChanges.addNewChangeSystem(status,type,userId,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,workRoleId)
    .then(sysChange => {
      console.log("Uporabnik " + userId + " je naredil spremembo tipa " + type + " s statusom " + status + ".");
      return res.json({success:true, change:sysChange});
    })
    .catch((e)=>{
      return res.json({success:false, error:e})
    })
  }
  else if(type == 17){
    //password change
    dbChanges.addNewChangeSystem(status,type,userId,null,null,null,null,null,null,null,null,null,null,null,null,null,null,forUserId)
    .then(sysChange => {
      console.log("Uporabnik " + userId + " je naredil spremembo tipa " + type + " s statusom " + status + " za uporabnika " + forUserId + ".");
      return res.json({success:true, change:sysChange});
    })
    .catch((e)=>{
      return res.json({success:false, error:e})
    })
  }
  else if(type == 18){
    //work order change
    dbChanges.addNewChangeSystem(status,type,userId,null,null,null,null,null,null,null,null,null,workOrderId)
    .then(sysChange => {
      console.log("Uporabnik " + userId + " je naredil spremembo tipa " + type + " s statusom " + status + " za delovni nalog " + workOrderId + ".");
      return res.json({success:true, change:sysChange});
    })
    .catch((e)=>{
      return res.json({success:false, error:e});
    })
  }
  else if(type == 19){
    //reports change
    dbChanges.addNewChangeSystem(status,type,userId,null,null,null,null,null,null,null,null,null,null,null,null,null,reportId)
    .then(sysChange => {
      console.log("Uporabnik " + userId + " je naredil spremembo tipa " + type + " s statusom " + status + " za poročilo " + reportId + ".");
      return res.json({success:true, change:sysChange});
    })
    .catch((e)=>{
      return res.json({success:false, error:e})
    })
  }
  else if(type == 20){
    //build part change
    //reports change
    dbChanges.addNewChangeSystem(status,type,userId,null,taskId,null,null,null,null,null,null,null,null,buildPartId)
    .then(sysChange => {
      console.log("Uporabnik " + userId + " je naredil spremembo tipa " + type + " s statusom " + status + " za kos " + buildPartId + ".");
      return res.json({success:true, change:sysChange});
    })
    .catch((e)=>{
      return res.json({success:false, error:e})
    })
  }
  else if(type == 21){
    //build part phase change
    dbChanges.addNewChangeSystem(status,type,userId,null,taskId,null,null,null,null,null,null,null,null,buildPartId,buildPhaseId)
    .then(sysChange => {
      console.log("Uporabnik " + userId + " je naredil spremembo tipa " + type + " s statusom " + status + " za fazo " + buildPhaseId + " pri kosu " + buildPartId + ".");
      return res.json({success:true, change:sysChange});
    })
    .catch((e)=>{
      return res.json({success:false, error:e})
    })
  }
  else if(type == 22){
    //build parts with csv change
    //route for adding built parts through csv adds their changes right away since he has new id for every new build part
    dbChanges.addNewChangeSystem(status,type,userId,null,taskId)
    .then(sysChange => {
      console.log("Uporabnik " + userId + " je naredil kosovnico tipa " + type + " s statusom " + status + " za opravilo " + taskId + ".");
      return res.json({success:true, change:sysChange});
    })
    .catch((e)=>{
      return res.json({success:false, error:e})
    })
  }
  else if(type == 23){
    //work order file change
    dbChanges.addNewChangeSystem(status,type,userId,null,null,null,null,null,null,null,null,null,workOrderId,null,null,null,null,null,workOrderFileId)
    .then(sysChange => {
      console.log("Uporabnik " + userId + " je naredil spremembo tipa " + type + " s statusom " + status + " za delovni nalog " + workOrderId + ".");
      return res.json({success:true, change:sysChange})
    })
    .catch((e)=>{
      res.json({success:false, error:e})
    })
  }
  else if(type == 24){
    //general absence change
    dbChanges.addNewChangeSystem(status,type,userId,null,null,null,null,null,null,null,absenceId)
    .then(sysChange => {
      console.log("Uporabnik " + userId + " je naredil spremembo tipa " + type + " s statusom " + status + " za odsotnost " + absenceId + ".");
      return res.json({success:true, change:sysChange})
    })
    .catch((e)=>{
      res.json({success:false, error:e})
    })
  }
  else if(type == 30 || type == 36){
    //meeting change
    dbChanges.addNewChangeSystem(status,type,userId,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,meetingId)
    .then(sysChange => {
      console.log("Uporabnik " + userId + " je naredil spremembo tipa " + type + " s statusom " + status + " za sestanek " + meetingId + ".");
      return res.json({success:true, change:sysChange})
    })
    .catch((e)=>{
      res.json({success:false, error:e})
    })
  }
  else if(type == 31 || type == 37){
    //activity change
    dbChanges.addNewChangeSystem(status,type,userId,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,activityId)
    .then(sysChange => {
      console.log("Uporabnik " + userId + " je naredil spremembo tipa " + type + " s statusom " + status + " za aktivnost sestanka " + activityId + ".");
      return res.json({success:true, change:sysChange})
    })
    .catch((e)=>{
      res.json({success:false, error:e})
    })
  }
  else if(type == 32){
    //project modification status change
    if(modStatusId){
      dbChanges.addNewChangeSystem(status,type,userId,projectId,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,modificationId,modStatusId)
    .then(sysChange => {
      console.log("Uporabnik " + userId + " je naredil spremembo tipa " + type + " s statusom " + status + " za popravek " + modificationId + " z statusom " + modStatusId + ".");
      return res.json({success:true, change:sysChange})
    })
    .catch((e)=>{
      res.json({success:false, error:e})
    })
    }
    else{
      //project modification change
      dbChanges.addNewChangeSystem(status,type,userId,projectId,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,modificationId)
      .then(sysChange => {
        console.log("Uporabnik " + userId + " je naredil spremembo tipa " + type + " s statusom " + status + " za popravek " + modificationId + ".");
        return res.json({success:true, change:sysChange})
      })
      .catch((e)=>{
        res.json({success:false, error:e})
      })
    }
  }
  else if(type == 33){
    if (compStatusId){
      //complaints subscribers status change
      dbChanges.addNewChangeSystem(status,type,userId,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,compSubId,null,null,compStatusId)
      .then(sysChange => {
        console.log("Uporabnik " + userId + " je spremenil status reklamaciji naročnika " + compSubId + " s statusom " + compStatusId + ".");
        return res.json({success:true, change:sysChange})
      })
      .catch((e)=>{
        res.json({success:false, error:e})
      })
    }
    else{
      //complaints subscribers change
      dbChanges.addNewChangeSystem(status,type,userId,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,compSubId)
      .then(sysChange => {
        console.log("Uporabnik " + userId + " je naredil spremembo tipa " + type + " s statusom " + status + " za reklamacijo naročnika " + compSubId + ".");
        return res.json({success:true, change:sysChange})
      })
      .catch((e)=>{
        res.json({success:false, error:e})
      })
    }
  }
  else if(type == 34){
    if (compStatusId){
      //complaints suppliers status change
      dbChanges.addNewChangeSystem(status,type,userId,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,compSupId,null,compStatusId)
      .then(sysChange => {
        console.log("Uporabnik " + userId + " je spremenil status reklamacije dobavitelja " + compSupId + " s statusom " + compStatusId + ".");
        return res.json({success:true, change:sysChange})
      })
      .catch((e)=>{
        res.json({success:false, error:e})
      })
    }
    else{
      //complaints suppliers change
      dbChanges.addNewChangeSystem(status,type,userId,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,compSupId)
      .then(sysChange => {
        console.log("Uporabnik " + userId + " je naredil spremembo tipa " + type + " s statusom " + status + " za reklamacijo dobavitelju " + compSupId + ".");
        return res.json({success:true, change:sysChange})
      })
      .catch((e)=>{
        res.json({success:false, error:e})
      })
    }
  }
  else if(type == 35){
    //complaints file change
    dbChanges.addNewChangeSystem(status,type,userId,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,compSubId,compSupId,compFileId)
    .then(sysChange => {
      if(compSubId)
        console.log("Uporabnik " + userId + " je naredil spremembo tipa " + type + " s statusom " + status + " za reklamacijo " + compSubId + ".");
      if(compSupId)
        console.log("Uporabnik " + userId + " je naredil spremembo tipa " + type + " s statusom " + status + " za reklamacijo  " + compSupId + ".");
      return res.json({success:true, change:sysChange})
    })
    .catch((e)=>{
      res.json({success:false, error:e})
    })
  }
  else{
    dbChanges.addNewChangeSystem(status,type,userId,projectId,taskId,subtaskId,null,null,noteId,subscriberId,absenceId,fileId,workOrderId,buildPartId,buildPhaseId,projectDateId,reportId,forUserId,workOrderFileId,roleId,workRoleId,workOrderFileId,meetingId,activityId,modificationId,modStatusId,compSubId,compSupId,compFileId,compStatusId,stdCompId)
    .then(sysChange => {
      console.log("Uporabnik " + userId + " je naredil spremembo tipa " + type + " s statusom " + status + ".");
      return res.json({success:true, change:sysChange})
    })
    .catch((e)=>{
      res.json({success:false, error:e})
    })
  }
})
//get change types
router.get('/types', auth.authenticate, function(req, res, next){
  dbChanges.getChangeTypes()
  .then(types=>{
    return res.json({success: true, data: types});  
  })
  .catch((e)=>{
    return res.json({success:false, error:e});
  })
})
//get change types
router.get('/status', auth.authenticate, function(req, res, next){
  dbChanges.getChangeStatus()
  .then(status=>{
    return res.json({success: true, data: status});  
  })
  .catch((e)=>{
    return res.json({success:false, error:e});
  })
})
module.exports = router;