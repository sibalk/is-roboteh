var express = require('express');
var router = express.Router();
var dateFormat = require('dateformat');
var multer = require('multer');
var fs = require('fs');
const fsp = require('fs').promises;
var Jimp = require('jimp');
var path = require('path');
const csp = require('helmet-csp');

//auth & dbModels
var auth = require('../controllers/authentication');
var dbProjects = require('../model/projects/dbProjects');
var dbModification = require('../model/projects/dbModifications');
var dbTasks = require('../model/tasks/dbTasks');
var dbSubtasks = require('../model/subtasks/dbSubtasks');
var dbAbsences = require('../model/absences/dbAbsences');
var dbChanges = require('../model/changes/dbChanges');
var dbFiles = require('../model/files/dbFiles');
var dbCategories = require('../model/categories/dbCategories');
var dbPriorities = require('../model/priorities/dbPriorities');
var dbDates = require('../model/dates/dbDates');
var dbWorkRoles = require('../model/workRoles/dbWorkRoles');
let dbBuildParts = require('../model/buildparts/dbBuildParts');

//routing forward
let routerPdfProjects = require('./pdfprojects');
router.use('/pdfs', routerPdfProjects);

var normalCspHandler = csp({
  directives: {
    defaultSrc: ["'self'"],
    scriptSrc: ["'self'"],
    styleSrc: ["'self'", "https://fonts.googleapis.com", "https://use.fontawesome.com"],
    fontSrc: ["'self'", "https://fonts.gstatic.com", "https://use.fontawesome.com"],
    imgSrc: ["'self'", "data:"],
    frameAncestors: ["'self'"],
  }
});

//functions
function formatDate(date){
  return {
    "date": dateFormat(date, "dd.mm.yyyy"),
    "time": dateFormat(date, "HH:MM")
  }
}
function addFormatedDateForTasks(tasks){
  //console.log("test");
  for(let i=0; i < tasks.length; i++) {
    if(tasks[i] && tasks[i].task_start)
      tasks[i]["formatted_start"] = formatDate(tasks[i].task_start);
    if(tasks[i] && tasks[i].task_finish)
      tasks[i]["formatted_finish"] = formatDate(tasks[i].task_finish);
  }
  return tasks;
}
function addFormatedDateForProject(project){
  if(project.start)
    project["formatted_start"] = formatDate(project.start);
    if(project.finish)
    project["formatted_finish"] = formatDate(project.finish);
  return project
}
//storage for documents
var storageDoc = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, 'files/projects/')
  },
  filename: function (req, file, cb) {        
    // null as first argument means no error
    cb(null, Date.now() + '-' + file.originalname )
  }
});
//upload when adding subscriber image
var uploadDoc = multer({
  storage: storageDoc,
  limits: {
    fileSize: 5300000
  },
  fileFilter: function(req, file, cb){
    sanitizeFileDocument(file,cb);
  }
}).single('docNew');
//function to check if file is indeed document
function sanitizeFileDocument(file, cb){
  //what file extentions are ok
  let fileExts = ['pdf', //pdf 
                'doc', 'dot', 'wbk', 'docx', 'docm', 'dotx', 'dotm', 'docb', //word
                'xls', 'xlt', 'xlm', 'xlsx', 'xlsm', 'xltx', 'xltm', 'xlsb', 'xla', 'xlam', 'xll', 'xlw', //excel
                'ppt', 'pot', 'pps', 'pptx', 'pptm', 'potx', 'potm', 'ppam', 'ppsx', 'ppsm', 'sldx', 'sldm', //powepoint
                'zip', 'rar', //zips
                'txt', //txt
                'jpg', 'jpeg', 'png', 'gif']; //images
                //access has way diffrent ext and they probably wont use them
                //other file ext will be added when there will be request
  // MAYBE TODO add isAlowedMimeType back and test for all this type of extentions
  //check if file has no exts
  let fileExtsArray = file.originalname.split(".");
  if(fileExtsArray.length == 1)
    return cb('Datoteka brez končnice ni dovoljena');
  //check alowed exts
  let isAlowedExt = fileExts.includes(fileExtsArray[fileExtsArray.length-1].toLowerCase());
  //mime type must be an image
  //let isAlowedMimeType = file.mimetype.startsWith("document/");

  if(isAlowedExt){
    //no errors
    return cb(null, true);
  }
  else{
    //error, not an image
    cb('Ta vrsta datoteke ni dovoljena!');
  }
}
var uploadDocTask = multer({
  storage: storageDoc,
  limits: {
    fileSize: 5300000
  },
  fileFilter: function(req, file, cb){
    sanitizeFileDocument(file,cb);
  }
}).single('docTaskNew');
//function to check if file is indeed document
function sanitizeFileDocument(file, cb){
  //what file extentions are ok
  let fileExts = ['pdf', //pdf 
                'doc', 'dot', 'wbk', 'docx', 'docm', 'dotx', 'dotm', 'docb', //word
                'xls', 'xlt', 'xlm', 'xlsx', 'xlsm', 'xltx', 'xltm', 'xlsb', 'xla', 'xlam', 'xll', 'xlw', //excel
                'ppt', 'pot', 'pps', 'pptx', 'pptm', 'potx', 'potm', 'ppam', 'ppsx', 'ppsm', 'sldx', 'sldm', //powepoint
                'zip', 'rar', //zips
                'txt', //txt
                'jpg', 'jpeg', 'png', 'gif']; //images
                //access has way diffrent ext and they probably wont use them
                //other file ext will be added when there will be request
  // MAYBE TODO add isAlowedMimeType back and test for all this type of extentions
  //check if file has no exts
  let fileExtsArray = file.originalname.split(".");
  if(fileExtsArray.length == 1)
    return cb('Datoteka brez končnice ni dovoljena');
  //check alowed exts
  let isAlowedExt = fileExts.includes(fileExtsArray[fileExtsArray.length-1].toLowerCase());
  //mime type must be an image
  //let isAlowedMimeType = file.mimetype.startsWith("document/");

  if(isAlowedExt){
    //no errors
    return cb(null, true);
  }
  else{
    //error, not an image
    cb('Ta vrsta datoteke ni dovoljena!');
  }
}
//upload for new file
router.post('/fileUpload', auth.authenticate, (req, res, next) => {
  //debugger;
  //save file and if no error send back json success true
  uploadDoc(req, res, (err) =>{
    //console.log("img upload");
    if(err){
      let projectId = parseInt(req.body.projectId);
      //res.redirect('/subscribers');
      console.log("Napaka pri nalaganju datoteke pri uporabniku " + req.session.user_id);
      //let msg = "Napaka: " + err;
      let msg = 'Napaka pri nalaganju datoteke na strežnik.';
      if(err.message == 'File too large')
        msg = 'Velikost datoteke je prevelika. Trenutno so dovoljene datoteke do 5MB.';
      let type = 1;
      return res.status(500).json({
        status: 'error',
        message: msg,
        type: type,
        error: err,
      })
      //res.redirect('/projects/id?id='+projectId+'&msg='+msg+'&type='+type);
    }
    else{
      //file not selected
      if(req.file == undefined){
        let projectId = parseInt(req.body.projectId);
        console.log("Uporabnik " + req.session.user_id + " ni podal datoteke pri nalaganju datoteke.");
        //var subscriberName = req.body.subscriberName;
        let msg = "Neuspešno! Niste podali datoteke, ki jo želite naložiti.";
        let type = 1;
        return res.status(400).json({
          status: 'error',
          message: msg,
          type: type,
        })
        //res.redirect('/projects/id?id='+projectId+'&msg='+msg+'&type='+type);
      }
      else{
        let type = "";
        let fileExtsPDF = ['pdf'];
        let fileExtsDOC = ['doc', 'dot', 'wbk', 'docx', 'docm', 'dotx', 'dotm', 'docb'];
        let fileExtsXLS = ['xls', 'xlt', 'xlm', 'xlsx', 'xlsm', 'xltx', 'xltm', 'xlsb', 'xla', 'xlam', 'xll', 'xlw'];
        let fileExtsPPT = ['ppt', 'pot', 'pps', 'pptx', 'pptm', 'potx', 'potm', 'ppam', 'ppsx', 'ppsm', 'sldx', 'sldm'];
        let fileExtsIMG = ['jpg', 'jpeg', 'png', 'gif'];
        let fileExtsZIP = ['zip', 'rar'];
        let fileExtsTXT = ['txt'];

        let fileExtsArray = req.file.originalname.split(".");
        let isAlowedExtPDF = fileExtsPDF.includes(fileExtsArray[fileExtsArray.length - 1].toLocaleLowerCase());
        let isAlowedExtDOC = fileExtsDOC.includes(fileExtsArray[fileExtsArray.length - 1].toLowerCase());
        let isAlowedExtXLS = fileExtsXLS.includes(fileExtsArray[fileExtsArray.length - 1].toLowerCase());
        let isAlowedExtPPT = fileExtsPPT.includes(fileExtsArray[fileExtsArray.length - 1].toLowerCase());
        let isAlowedExtZIP = fileExtsZIP.includes(fileExtsArray[fileExtsArray.length - 1].toLowerCase());
        let isAlowedExtTXT = fileExtsTXT.includes(fileExtsArray[fileExtsArray.length - 1].toLowerCase());
        let isAlowedExtIMG = fileExtsIMG.includes(fileExtsArray[fileExtsArray.length - 1].toLocaleLowerCase());
        
        if(isAlowedExtPDF) type = 'PDF';
        else if(isAlowedExtDOC) type = 'DOC';
        else if(isAlowedExtPPT) type = 'PPT';
        else if(isAlowedExtXLS) type = 'XLS';
        else if(isAlowedExtIMG) type = 'IMG';
        else if(isAlowedExtZIP) type = 'ZIP';
        else if(isAlowedExtTXT) type = 'TXT';
        let projectId = parseInt(req.body.projectId);
        let modId = req.body.modId ? parseInt(req.body.modId) : null;
        let userId = req.session.user_id;
        if (modId)
          console.log("Uporabnik " + req.session.user_id + " je uspešno naložil datoteko " +req.file.originalname + " na projektu " + projectId + " za popravek " + modId);
        else
          console.log("Uporabnik " + req.session.user_id + " je uspešno naložil datoteko " +req.file.originalname + " na projektu " + projectId);
        //success add new subscriber with image name
        //var subscriberName = req.body.subscriberName;
        //var imageName = req.file.filename;
        if (modId){
          dbFiles.addFileForProjectMod(req.file.originalname, req.file.filename, projectId, req.session.user_id, type, modId).then(newFile => {
            if(newFile){
              let tmpFile = newFile;
              dbChanges.addNewChangeSystem(1,38,userId,projectId,null,null,null,null,null,null,null,tmpFile.id,null,null,null,null,null,null,null,null,null,null,null,null,modId).then(sysChange => {
                let msg = "Uspešno dodajanje nove datoteke.";
                let type = 2;
                let fileLocation = `${__dirname}/../files/projects/${newFile.path_name}`;
                if (fs.existsSync(fileLocation)) {
                  if (newFile.type == 'IMG'){
                    Jimp.read(fileLocation).then(img => {
                      img.resize(150, Jimp.AUTO).quality(20).getBase64Async(Jimp.AUTO).then(base64 => {
                        //return res.json({success:true, data: base64, id: fileId, type: 'IMG'});
                        newFile.base64 = base64;
                        return res.status(200).json({
                          status: 'success',
                          file: newFile,
                          success: 'true',
                          message: msg,
                          type: type,
                        });
                      })
                      .catch(err => {
                        return res.json({success:false, error: err});
                      });
                    })
                    .catch((e) => {
                      return res.json({success:false, error: e, msg: 'Datoteka ne obstaja.', id: fileId, type: fileType});
                    })
                  }
                  else if (newFile.type == 'PDF'){
                    fsp.readFile(fileLocation, 'base64').then(base64 => {
                      //console.log(data);
                      newFile.base64 = base64;
                      return res.status(200).json({
                        status: 'success',
                        file: newFile,
                        success: 'true',
                        message: msg,
                        type: type,
                      });
                    })
                    .catch((e) => {
                      return res.json({success:false, error: e});
                    })
                  }
                  else{
                    return res.status(200).json({
                      status: 'success',
                      file: newFile,
                      success: 'true',
                      message: msg,
                      type: type,
                    });
                  }
                }
                else{
                  return res.status(200).json({
                    status: 'file not found',
                    file: newFile,
                    success: 'false',
                    message: msg,
                    type: type,
                  });
                }
              })
              .catch((e)=>{
                return res.status(500).json({
                  status: 'error',
                  message: 'Napaka pri zapisovanju v sprememb za novo datoeko.',
                  error: e,
                })
              })
            }
          })
          .catch(e => {
            return res.status(500).json({
              status: 'error',
              message: 'Napaka pri zapisovanju podatkov datoteke v tabelo z datotekami.',
              error: e,
            })
          })
        }
        else{
          dbFiles.addFileForProject(req.file.originalname, req.file.filename, projectId, req.session.user_id, type)
          .then(newFile =>{
            if(newFile){
              //add new change for file
              let tmpFile = newFile;
              let promises = [];
              dbChanges.getProjectNotifyLeaders(projectId,req.session.user_id).then(notify => {
                dbChanges.addNewChangeSystem(1,7,userId,projectId,null,null,null,null,null,null,null,tmpFile.id).then(sysChange => {
                  console.log("Uporabnik " + userId + " je naredil spremembo tipa 7 s statusom 1 na projektu "+projectId+".");
                  for(let i = 0; i < notify.length; i++){
                    //console.log("Uporabnik " + userId + " je naredil spremembe za " + notify[i].id_user + " tipa 7 s statusom 1 na projektu "+projectId+".");
                    //promises.push(dbChanges.addNewChange(7,1,notify[i].id_user,userId,projectId,null,null,null,null,null,tmpFile.id));
                    promises.push(dbChanges.addNewChangeNotify(sysChange.id,notify[i].id_user));
                  }
                  Promise.all(promises)
                  .then((results) => {
                    //console.log("all done", results);
                    //res.json({success:true, data:results})
                    let msg = "Uspešno dodajanje nove datoteke.";
                    let type = 2;
                    let fileLocation = `${__dirname}/../files/projects/${newFile.path_name}`;
                    if (fs.existsSync(fileLocation)) {
                      if (newFile.type == 'IMG'){
                        Jimp.read(fileLocation).then(img => {
                          img.resize(150, Jimp.AUTO).quality(20).getBase64Async(Jimp.AUTO).then(base64 => {
                            //return res.json({success:true, data: base64, id: fileId, type: 'IMG'});
                            newFile.base64 = base64;
                            return res.status(200).json({
                              status: 'success',
                              file: newFile,
                              success: 'true',
                              message: msg,
                              type: type,
                            });
                          })
                          .catch(err => {
                            return res.json({success:false, error: err});
                          });
                        })
                        .catch((e) => {
                          return res.json({success:false, error: e, msg: 'Datoteka ne obstaja.', id: fileId, type: fileType});
                        })
                      }
                      else if (newFile.type == 'PDF'){
                        fsp.readFile(fileLocation, 'base64').then(base64 => {
                          //console.log(data);
                          newFile.base64 = base64;
                          return res.status(200).json({
                            status: 'success',
                            file: newFile,
                            success: 'true',
                            message: msg,
                            type: type,
                          });
                        })
                        .catch((e) => {
                          return res.json({success:false, error: e});
                        })
                      }
                      else{
                        return res.status(200).json({
                          status: 'success',
                          file: newFile,
                          success: 'true',
                          message: msg,
                          type: type,
                        });
                      }
                    }
                    else{
                      return res.status(200).json({
                        status: 'file not found',
                        file: newFile,
                        success: 'false',
                        message: msg,
                        type: type,
                      });
                    }
                    //res.redirect('/projects/id?id='+projectId+'&msg='+msg+'&type='+type);
                  })
                  .catch((e)=>{
                    return res.status(500).json({
                      status: 'error',
                      message: 'Napaka pri zapisovanju obvestil o spremembi za obveščanje uporabnikov.',
                      error: e,
                    })
                  })
                })
                .catch((e)=>{
                  return res.status(500).json({
                    status: 'error',
                    message: 'Napaka pri zapisovanju v sprememb za novo datoeko.',
                    error: e,
                  })
                })
              })
              .catch((e)=>{
                return res.status(500).json({
                  status: 'error',
                  message: 'Napaka pri pridobivanju podatkov uporabnikov za obveščevanje o novi datoteki.',
                  error: e,
                })
              })
            }
          })
          .catch((e)=>{
            return res.status(500).json({
              status: 'error',
              message: 'Napaka pri zapisovanju podatkov datoteke v tabelo z datotekami.',
              error: e,
            })
          })
        }
      }
    }
  })
});

//upload for new file based on taskId and projectId
router.post('/taskFileUpload', auth.authenticate, (req, res, next) => {
  //console.log(req.body.projectIdTaskFile);
  //console.log(req.body.taskIdTaskFile);
  //save file and if no error send back json success true
  uploadDocTask(req, res, (err) =>{
    //console.log("img upload");
    if(err){
      //res.redirect('/subscribers');
      console.log("Napaka pri nalaganju datoteke pri uporabniku " + req.session.user_id);
      let msg = 'Napaka pri nalaganju datoteke na strežnik.';
      if(err.message == 'File too large')
        msg = 'Velikost datoteke je prevelika. Trenutno so dovoljene datoteke do 5MB.';
      let type = 1;
      return res.status(500).json({
        status: 'error',
        message: msg,
        type: type,
        error: err,
      })
      //res.redirect('/projects/id?id='+projectId+'&msg='+msg+'&type='+type);
    }
    else{
      //file not selected
      if(req.file == undefined){
        //let projectId = parseInt(req.body.projectIdTaskFile);
        //let taskId = parseInt(req.body.taskIdTaskFile);
        console.log("Uporabnik " + req.session.user_id + " ni podal datoteke pri nalaganju datoteke.");
        //var subscriberName = req.body.subscriberName;
        let msg = "Neuspešno! Niste priložili datoteke, ki jo želite naložiti.";
        let type = 1;
        return res.status(400).json({
          status: 'error',
          message: msg,
          type: type,
        })
        //res.redirect('/projects/id?id='+projectId+'&msg='+msg+'&type='+type);
      }
      else{
        let type = "";
        let fileExtsPDF = ['pdf'];
        let fileExtsDOC = ['doc', 'dot', 'wbk', 'docx', 'docm', 'dotx', 'dotm', 'docb'];
        let fileExtsXLS = ['xls', 'xlt', 'xlm', 'xlsx', 'xlsm', 'xltx', 'xltm', 'xlsb', 'xla', 'xlam', 'xll', 'xlw'];
        let fileExtsPPT = ['ppt', 'pot', 'pps', 'pptx', 'pptm', 'potx', 'potm', 'ppam', 'ppsx', 'ppsm', 'sldx', 'sldm'];
        let fileExtsIMG = ['jpg', 'jpeg', 'png', 'gif'];
        let fileExtsZIP = ['zip', 'rar'];
        let fileExtsTXT = ['txt'];

        //let isAlowedExtPDF = fileExtsPDF.includes(req.file.originalname.split(".")[1].toLowerCase());
        let fileExtsArray = req.file.originalname.split(".");
        let isAlowedExtPDF = fileExtsPDF.includes(fileExtsArray[fileExtsArray.length - 1].toLocaleLowerCase());
        let isAlowedExtDOC = fileExtsDOC.includes(fileExtsArray[fileExtsArray.length - 1].toLowerCase());
        let isAlowedExtXLS = fileExtsXLS.includes(fileExtsArray[fileExtsArray.length - 1].toLowerCase());
        let isAlowedExtPPT = fileExtsPPT.includes(fileExtsArray[fileExtsArray.length - 1].toLowerCase());
        let isAlowedExtZIP = fileExtsZIP.includes(fileExtsArray[fileExtsArray.length - 1].toLowerCase());
        let isAlowedExtTXT = fileExtsTXT.includes(fileExtsArray[fileExtsArray.length - 1].toLowerCase());
        let isAlowedExtIMG = fileExtsIMG.includes(fileExtsArray[fileExtsArray.length - 1].toLocaleLowerCase());
        
        if(isAlowedExtPDF) type = 'PDF';
        else if(isAlowedExtDOC) type = 'DOC';
        else if(isAlowedExtPPT) type = 'PPT';
        else if(isAlowedExtXLS) type = 'XLS';
        else if(isAlowedExtIMG) type = 'IMG';
        else if(isAlowedExtZIP) type = 'ZIP';
        else if(isAlowedExtTXT) type = 'TXT';
        let projectId = parseInt(req.body.projectId);
        let taskId = parseInt(req.body.taskId);
        let userId = req.session.user_id;
        console.log("Uporabnik " + req.session.user_id + " je uspešno naložil datoteko " +req.file.originalname + " na projektu " + projectId + " za opravilu " + taskId);
        //success add new subscriber with image name
        dbFiles.addFileForTask(req.file.originalname, req.file.filename, projectId, req.session.user_id, type, taskId)
          .then(newFile =>{
            if(newFile){
              let msg = "Uspešno dodajanje nove datoteke.";
              let type = 7;
              if(!projectId)
                type = 13;
              //add new change to db (new file)
              let tmpFile= newFile;
              let promises = [];
              dbChanges.getTaskNotifyUsers(projectId,taskId,userId).then(users => {
                let notify = [];
                if(users.task_workers)
                  notify = users.task_workers.split(",");
                let leaders = [];
                if(users.project_workers)
                  leaders = users.project_workers.split(",");
                for(let i = 0; i < leaders.length; i++){
                  var x = notify.find(u => parseInt(u) == parseInt(leaders[i]));
                  if(!x)
                    notify.push(leaders[i]);
                }
                dbChanges.addNewChangeSystem(1,7,userId,projectId,taskId,null,null,null,null,null,null,tmpFile.id).then(sysChange => {
                  console.log("Uporabnik " + userId + " je naredil spremembo tipa 7 s statusom 1 za opravilo "+taskId+" na projektu "+projectId+".");
                  for(let i = 0; i < notify.length; i++){
                    //console.log("Uporabnik " + userId + " je naredil spremembe za " + notify[i] + " tipa 7 s statusom 1 za opravilo "+taskId+" na projektu "+projectId+".");
                    promises.push(dbChanges.addNewChangeNotify(sysChange.id,notify[i]));
                  }
                  Promise.all(promises)
                  .then((results) => {
                    //console.log("all done", results);
                    let fileLocation = `${__dirname}/../files/projects/${newFile.path_name}`;
                    if (fs.existsSync(fileLocation)) {
                      //newFile.base64 = fs.readFileSync(fileLocation, 'base64');
                      if (newFile.type == 'IMG'){
                        Jimp.read(fileLocation).then(img => {
                          img.resize(150, Jimp.AUTO).quality(20).getBase64Async(Jimp.AUTO).then(base64 => {
                            //return res.json({success:true, data: base64, id: fileId, type: 'IMG'});
                            newFile.base64 = base64;
                            return res.status(200).json({
                              status: 'success',
                              file: newFile,
                              success: 'true',
                              message: msg,
                              type: type,
                            });
                          })
                          .catch(err => {
                            return res.json({success:false, error: err});
                          });
                        })
                        .catch((e) => {
                          return res.json({success:false, error: e, msg: 'Datoteka ne obstaja.', id: fileId, type: fileType});
                        })
                      }
                      else if (newFile.type == 'PDF'){
                        fsp.readFile(fileLocation, 'base64').then(base64 => {
                          //console.log(data);
                          newFile.base64 = base64;
                          return res.status(200).json({
                            status: 'success',
                            file: newFile,
                            success: 'true',
                            message: msg,
                            type: type,
                          });
                        })
                        .catch((e) => {
                          return res.json({success:false, error: e});
                        })
                      }
                      else{
                        return res.status(200).json({
                          status: 'success',
                          file: newFile,
                          success: 'true',
                          message: msg,
                          type: type,
                        });
                      }
                    }
                    else{
                      return res.status(200).json({
                        status: 'file not found',
                        file:newFile,
                        success: 'false',
                        message: "Uspešno dodajanje nove datoteke.",
                        type: 2,
                      });
                    }
                  })
                  .catch((e)=>{
                    return res.status(500).json({
                      status: 'error',
                      message: 'Napaka pri zapisovanju obvestil o spremembi za obveščanje uporabnikov.',
                      error: e,
                    })
                  })
                })
                .catch((e)=>{
                  return res.status(500).json({
                    status: 'error',
                    message: 'Napaka pri zapisovanju v sprememb za novo datoeko.',
                    error: e,
                  })
                })
              })
              .catch((e)=>{
                return res.status(500).json({
                  status: 'error',
                  message: 'Napaka pri pridobivanju podatkov uporabnikov za obveščevanje o novi datoteki.',
                  error: e,
                })
              })
            }
        })
        .catch((e)=>{
          return res.status(500).json({
            status: 'error',
            message: 'Napaka pri zapisovanju podatkov datoteke v tabelo z datotekami.',
            error: e,
          })
        })
      }
    }
  })
});
//FILE DOWNLOAD
router.get('/sendMeDoc', auth.authenticate, function(req, res){
  //console.log('send him doc');
  //console.log(req.query.filename);
  let filename = req.query.filename;
  let projectId = req.query.projectId;
  console.log('Zahteva za datoteko: '+ filename);
  var file = `${__dirname}/../files/projects/${filename}`;
  if (fs.existsSync(file)) {
    //get file info from database to get correct name and MIME type
    dbFiles.getFileInfo(filename).then(fileInfo => {
      switch (fileInfo.type) {
        case 'IMG': break;
        case 'PDF': res.type('application/pdf'); break;
        case 'DOC': res.type('application/msword'); break;
        case 'PPT': res.type('application/mspowerpoint'); break;
        case 'ZIP': res.type('application/x-compressed'); break;
        case 'TXT': res.type('text/plain'); break;
        case 'XLS': res.type('application/excel'); break;
        default: break;
      }
      //let fileExtsIMG = ['jpg', 'jpeg', 'png', 'gif'];
      if (fileInfo.type == 'IMG'){
        let tmp = fileInfo.original_name.split('.');
        if (tmp[tmp.length-1].toLocaleLowerCase() == 'png') res.type('image/png');
        else if (tmp[tmp.length-1].toLocaleLowerCase() == 'jpg' || tmp[tmp.length-1] == 'jpeg') res.type('image/jpeg');
        else if (tmp[tmp.length-1].toLocaleLowerCase() == 'gif') res.type('image/gif');
      }
      // Do something
      res.download(file, fileInfo.original_name, function(err){
        if(err){
          console.error(err);
        }
      });
    })
    .catch(e => {
      let msg = "Napaka: Datoteka ne obstaja v bazi!";
      let type = 1;
      return res.status(500).json({
        status: 'error',
        message: msg,
        type
      })
    })
  }
  else{
    dbFiles.updateActiveFile(filename)
      .then(file=>{
        console.log("Datoteka "+filename+" ne obstaja več, aktivnost v bazi popravljena.")
        let msg = "Napaka: Datoteka ne obstaja več!";
        let type = 1;
        return res.status(500).json({
          status: 'error',
          message: msg,
          type
        })
      })
      .catch((e)=>{
        console.log(e);
      })
  }
})

//PROJEKTI
router.get('/', normalCspHandler, auth.authenticate, function(req, res, next) {
  res.render('projects', {
    title: "Seznam projektov",
    user_name: req.session.user_name,
    user_surname: req.session.user_surname,
    user_username: req.session.user_username,
    user_typeid: req.session.role_id,
    user_type: req.session.type,
    user_id: req.session.user_id,
    projects: [],
  })
  //console.log(req.session);
})
//Create project
router.post('/create', auth.authenticate, function(req, res, next){
  dbProjects.createProject(req.body.number, req.body.name, req.body.subscriber, req.body.start, req.body.finish, req.body.notes)
  .then(project=>{
    //console.log(subscriber);
    res.json({success:true, id:project.id, project});
  })
  .catch((e)=>{
    return res.json({success:false, error: e});
  })
})
//Delete project / set it as inactive
router.post('/delete', auth.authenticate, function(req,res, next){
  let projectId = req.body.projectId;
  dbProjects.removeProject(projectId).then(project=>{
    return res.json({success:true});
  })
  .catch((e)=>{
    res.json({success:false, error:e});
  })
})
//Update project
router.post('/update', auth.authenticate, function(req, res, next){
  dbProjects.updateProject(req.body.projectId,req.body.number,req.body.name,req.body.subscriber,req.body.start,req.body.finish,req.body.notes, req.body.active)
  .then(project=>{
    return res.json({success:true});
  })
  .catch((e)=>{
    return res.json({success:false, error: e});
  })
})
//get all changes for project (created, updated, removed)
router.get('/changes', auth.authenticate, function(req, res, next){
  let projectId = req.query.projectId;
  dbProjects.getProjectChanges(projectId).then(changes => {
    return res.json({success:true, changes});
  })
  .catch((e)=>{
    return res.json({success:false, error:e});
  })
})
//PROJEKT
router.get('/id', normalCspHandler, auth.authenticate, function(req, res, next) {
  let projectId = req.query.id;
  let taskId = req.query.taskId;
  let notificationType = req.query.type;
  let today = new Date();
  //0 = all, 1 = unfinished, 2 = finished
  let completionTask = 1;
  let tid ='';
  if(notificationType){
    tid = notificationType;
    completionTask = 0;
  }
  else if(req.query.taskId)
    tid = '2';
  let msg='';
  let type=0;
  if(req.query.msg){
    msg = req.query.msg;
    type = req.query.type;
  }
  //console.log(projectId);
  if(projectId){
    dbProjects.getProjectById(projectId).then(project =>{
      if(project){
        return dbProjects.getWorkersByProjectId(projectId)
          .then(workers=>{return {project, workers}})
          .catch((e)=>{
            next(e);
          })
        .then(data=>{
          if(data.workers){
            let isPresent = false;
            let leader = false
            for(let i in data.workers){
              if(data.workers[i].id === req.session.user_id){
                isPresent = true;
                if(data.workers[i].workRole.substring(0,5) == 'vodja')
                  leader = true;
              }
            }
            addFormatedDateForProject(data.project);
            res.render('project', {
              title: data.project.project_number+" - "+data.project.project_name,
              user_name: req.session.user_name,
              user_surname: req.session.user_surname,
              user_type: req.session.type,
              user_id: req.session.user_id,
              project: data.project,
              workers: data.workers,
              present: isPresent,
              leader: leader, 
              projectId: projectId,
              files: [],
              msg: msg,
              type: type,
              tid: tid,
              completionTable: completionTask,
              today: today,
              //workerTasks: workerTasks
            })
          }
          //not found workers
          else{
            res.render('project', {
              title: "Projekt "+data.project.project_name,
              user_name: req.session.user_name,
              user_surname: req.session.user_surname,
              user_type: req.session.type,
              user_id: req.session.user_id,
              project: data.project,
              present: false,
              leader: false,
              workers: [],
              projectId: projectId,
              files: [],
              pages: 0,
              msg: msg,
              type: type,
              tid: tid,
              completionTable: completionTask,
              today: today,
            })
          }
        })
        .catch((e)=>{
          next(e);
        })
      }
      else{
        res.render('project', {
          title: "Projekt X",
          user_name: req.session.user_name,
          user_surname: req.session.user_surname,
          user_type: req.session.type,
          user_id: req.session.user_id,
          pages: 0,
          msg: msg,
          type: type,
          tid: tid,
          completionTable: completionTask,
          today: today,
        })
      }
    })
    .catch((e)=>{
      next(e);
    })
  }else{
    res.render('project', {
      title: "Projekt X",
      user_name: req.session.user_name,
      user_surname: req.session.user_surname,
      user_type: req.session.type,
      user_id: req.session.user_id,
      pages: 0,
      msg: msg,
      type: type,
      tid: tid,
      completionTable: completionTask,
      today: today,
    })
  }
})
//
router.get('/calendar', auth.authenticate, function(req, res) {
  let roleType = req.session.type.toLocaleLowerCase();
  if(roleType == 'admin' || roleType.substring(0,5) == 'vodja'){
    res.render('calendar', {
      title: "Seznam projektov",
      user_name: req.session.user_name,
      user_surname: req.session.user_surname,
      user_username: req.session.user_username,
      user_typeid: req.session.role_id,
      user_type: req.session.type,
      user_id: req.session.user_id,
    })
  }
  else{
    res.redirect('/dashboard');
  }
  //console.log(req.session);
})
//get project info
router.get('/info', auth.authenticate, function(req, res, next){
  let projectId = req.query.projectId;
  dbProjects.getProjectById(projectId).then(project=>{
      return res.json({success:true, data:project});
    })
    .catch((e)=>{
      return res.json({success:false, error: e});
    })
})
//Get task's files
router.get('/taskFiles', auth.authenticate, function(req, res, next){
  //let projectId = req.query.projectId;
  let taskId = req.query.taskId;
  dbFiles.getTaskFiles(taskId).then(files=>{
    /*
    files.forEach(file =>  {
      let fileLocation = `${__dirname}/../files/projects/${file.path_name}`;
      if(fs.existsSync(fileLocation) && file.type == 'IMG')
        file.base64 = fs.readFileSync(fileLocation, 'base64');
    })
    */
    res.json({success:true, data: files});
  })
  .catch((e)=>{
    return res.json({success:false, error: e});
  })
})
//Get project's files
router.get('/files', auth.authenticate, function(req, res, next){
  let projectId = req.query.projectId;
  dbFiles.getProjectFiles(projectId).then(files=>{
    /*
    files.forEach(file =>  {
      let fileLocation = `${__dirname}/../files/projects/${file.path_name}`;
      if(fs.existsSync(fileLocation) && file.type == 'IMG')
        file.base64 = fs.readFileSync(fileLocation, 'base64');
    })
    */
    res.json({success:true, data: files});
  })
  .catch((e)=>{
    return res.json({success:false, error: e});
  })
})
//get thumbnails for img and later for pdf as well
router.get('/files/thumbnail', auth.authenticate, function(req, res, next){
  let fileId = req.query.fileId;
  let filePath = req.query.filePath;
  let fileType = req.query.fileType;
  let fileLocation = `${__dirname}/../files/projects/${filePath}`;
  let fileBase64 = '';
  if (fileType == 'IMG'){
    Jimp.read(fileLocation).then(img => {
      img.resize(150, Jimp.AUTO).quality(20).getBase64Async(Jimp.AUTO).then(base64 => {
        return res.json({success:true, data: base64, id: fileId, type: 'IMG', filePath});
      })
      .catch(err => {
        return res.json({success:false, error: e});
      });
    })
    .catch((e) => {
      return res.json({success:false, error: e, msg: 'Datoteka ne obstaja.', id: fileId, type: fileType});
    })
  }
  else if (fileType == 'PDF'){
    if (fs.existsSync(fileLocation)) {
     fsp.readFile(fileLocation, 'base64').then(data => {
        //console.log(data);
        return res.json({success:true, data, id: fileId, type: 'PDF', filePath});
      })
      .catch((e) => {
        return res.json({success:false, error: e});
      })
    }
    else{
      res.json({success:false, msg:'Datoteka ne obstaja'});
    }
    //return res.json({success:false, mgs:'not supported type yet'});
  }
  else{
    return res.json({success:false, mgs:'not supported type'});
  }
})
//FILE SENT TEST
router.get('/files/data', auth.authenticate, function(req, res){
  //console.log('send him doc');
  //console.log(req.query.filename);
  let filename = req.query.filename;
  let fileId = req.query.fileId;
  let filePath = req.query.filePath;
  let fileType = req.query.fileType;
  let fileLocation = `${__dirname}/../files/projects/${filePath}`;
  let fileBase64 = '';
  //console.log('Zahteva za datoteko: '+ filename);
  var file = fileLocation;
  if (fs.existsSync(file)) {
    fsp.readFile(fileLocation, 'base64').then(data => {
      //console.log(data);
      return res.json({success:true, data, id: fileId});
    })
    .catch((e) => {
      return res.json({success:false, error: e});
    })
  }
  else{
    res.json({success:false});
  }
})
//send files that is hidden from public and is connected to projects
router.get('/files/resources', auth.authenticate, (req,res)=>{
  let fileName = req.query.fileName;
  //res.sendFile(path.resolve('files/projects/' + fileName));
  if (fileName.substring(fileName.length-3).toLocaleLowerCase() == 'pdf'){
    res.set({"Content-Type": "application/pdf",});
  }
  else if (fileName.substring(fileName.length-3).toLocaleLowerCase() == 'doc' || fileName.substring(fileName.length-4).toLocaleLowerCase() == 'docx'){
    res.set({"Content-Type": "application/msword",});
  }
  else if (fileName.substring(fileName.length-3).toLocaleLowerCase() == 'ppt'){
    res.set({"Content-Type": "application/mspowerpoint",});
  }
  else if (fileName.substring(fileName.length-3).toLocaleLowerCase() == 'xls'){
    res.set({"Content-Type": "application/excel",});
  }
  else if (fileName.substring(fileName.length-3).toLocaleLowerCase() == 'zip'){
    res.set({"Content-Type": "application/x-compressed",});
  }
  else if (fileName.substring(fileName.length-3).toLocaleLowerCase() == 'png'){
    res.set({"Content-Type": "image/png",});
  }
  else if (fileName.substring(fileName.length-3).toLocaleLowerCase() == 'jpg' || fileName.substring(fileName.length-4).toLocaleLowerCase() == 'jpeg'){
    res.set({"Content-Type": "image/jpeg",});
  }
  else if (fileName.substring(fileName.length-3).toLocaleLowerCase() == 'gif'){
    res.set({"Content-Type": "image/gif",});
  }
  else
    res.set({"Content-Type": "text/plain",});
  
  if (fs.existsSync(path.resolve('files/projects/' + fileName))) {
    fs.createReadStream(path.resolve('files/projects/' + fileName))
    .pipe(res);
  }
  else{
    res.json({success:false, msg:'Datoteka ne obstaja'});
  }
})
//delete file
router.delete('/files', auth.authenticate, function(req, res, next){
  let fileId = req.body.fileId;
  //check if file exist, if exist do unlink and then for both cases update deleted to true
  //dbFiles.deleteFile(fileId)
  dbFiles.getFile(fileId).then(file => {
    if(file.active){ // file still exist, first delete/unlink it in directory and then mark it as deleted file
      fs.unlink(path.resolve('files/projects/' + file.path_name), function(err) {
        if(err && err.code == 'ENOENT') {
          // file doens't exist
          console.info("File doesn't exist, won't remove it.");
          dbFiles.deleteFile(fileId).then(deletedFile => {
            res.json({success:true, msg:'Datoteka je že izbrisana.', file:deletedFile});
          })
          .catch((e)=>{
            return res.json({success:false, error: e, msg:'Datoteka je že izbrisana, napaka pri zapisu v bazo o brisanju datoteke.'});
          })
        }
        else if (err) {
          // other errors, e.g. maybe we don't have enough permission
          console.error("Error occurred while trying to remove file");
          return res.json({success:false, error: e});
        }
        else {
          console.info(`removed`);
          dbFiles.deleteFile(fileId).then(deletedFile => {
            res.json({success:true, msg:'Datoteka je uspešno izbrisana.', file:deletedFile});
          })
          .catch((e)=>{
            return res.json({success:false, error: e, msg:'Datoteka je uspešno izbrisana, napaka pri zapisu v bazo o brisanju datoteke.'});
          })
        }
      });
    }
    else{ // file doesnt exist anymore in directory or is corrupted or its named was changed from linux cmd --> just mark it as deleted
      dbFiles.deleteFile(fileId).then(deletedFile => {
        res.json({success:true, msg:'Datoteka je označena kot izbrisana.', file:deletedFile});
      })
      .catch((e)=>{
        return res.json({success:false, error: e, msg:'Napaka pri zapisu v bazo o brisanju datoteke.'});
      })
    }
    //res.json({success:true});
  })
  .catch((e)=>{
    return res.json({success:false, error: e, msg:'Napaka pri iskanju datoteke v bazi.'});
  })
})
//Get categories
router.get('/categories', auth.authenticate, function(req, res, next){
  dbCategories.getCategories().then(categories=>{
    res.json({success:true, data: categories});
  })
  .catch((e)=>{
    return res.json({success:false, error: e});
  })
})
//Get priorities
router.get('/priorities', auth.authenticate, function(req, res, next){
  dbPriorities.getPriorities().then(priorities=>{
    res.json({success:true, data: priorities});
  })
  .catch((e)=>{
    return res.json({success:false, error: e});
  })
})
//Get absence for users (task and general absences)
router.post('/absence', auth.authenticate, function(req, res, next){
  let taskId = req.body.taskId;
  let start = req.body.start;
  let finish = req.body.finish;
  let workerId = req.body.workersId;
  let workersArray = workerId.split(",");
  if(workersArray[0] == "")
    workersArray.pop();
  let promises = [];
  if(workersArray){
    for(let i=0; i<workersArray.length; i++){
      promises.push(dbAbsences.getUserAbsenceForTask(workersArray[i], taskId, start, finish));
    }
    Promise.all(promises)
    .then((results)=>{
      res.json({success:true, data:results});
    })
    .catch((e)=>{
      return res.json({success:false, error: e});
    })
  }
  else{
    //sem po pravem nebi smel pridet
    res.json({success:true, data:[]});
  }
})
//Update project completion
router.post('/completion', auth.authenticate, function(req, res, next){
  let projectId = req.body.projectId;
  if(projectId){
    dbTasks.getTasksByProjectIdFixed(projectId,0,1).then(tasks => {
      let completion = 0;
      for(let i = 0; i < tasks.length; i++){
        completion += tasks[i].completion;
      }
      completion = Math.round(completion/tasks.length);
      if(tasks.length == 0)
        completion = 0;
      dbProjects.updateCompletion(projectId, completion).then(task => {
        return res.json({success:true});
      })
      .catch((e)=>{
        return res.json({success:false, error: e});
      })
    })
    .catch((e)=>{
      return res.json({success:false, error: e});
    })
  }
  else
    return res.json({success:false});
})
////////////////////////////////SUBTASK CALLS/////////////////
//Get subtasks of task by taskId
router.get('/subtasks', auth.authenticate, function(req, res, next){
  let taskId = req.query.taskId;
  //let active = req.query.active;
  dbSubtasks.getSubTasksById(taskId).then(tasks=>{
    return res.json({success:true, data: tasks});
  })
  .catch((e)=>{
    return res.json({success:false, error: e});
  })
})
//Post subtask completion
router.post('/subtask', auth.authenticate, function(req, res, next){
  let subtaskId = req.body.subtaskId;
  let completed = req.body.completed;
  dbSubtasks.isSubtaskCompleted(subtaskId, completed).then(t=>{
      return res.json({success:true});
    })
    .catch((e)=>{
      return res.json({success:false, error: e});
    })
})
//Delete subtask by id
router.post('/subtask/delete', auth.authenticate, function(req, res, next){
  let subtaskId = req.body.id;
  let active = req.body.active;
  dbSubtasks.deleteSubtask(subtaskId, active).then(t=>{
    return res.json({success:true});
  })
  .catch((e)=>{
    return res.json({success:false, error: e});
  })
})
//Create subtask
router.post('/subtask/add', auth.authenticate, function(req, res, next){
  let name = req.body.newSubtask;
  let taskId = req.body.taskId;
  dbSubtasks.createSubtask(taskId, name).then(t=>{
    return res.json({success:true, data: t});
  })
  .catch((e)=>{
    return res.json({success:false, error: e});
  })
})
router.post('/subtask/edit', auth.authenticate, function(req, res, next){
  let name = req.body.name;
  let subtaskId = req.body.subtaskId;
  dbSubtasks.updateSubtask(subtaskId, name).then(t=>{
    return res.json({success:true});
  })
  .catch((e)=>{
    return res.json({success:false, error: e});
  })
})
//Change/update subtask's note
router.post('/subtask/note', auth.authenticate, function(req, res, next){
  let note = req.body.newNote;
  let subtaskId = req.body.subtaskId;
  dbSubtasks.updateSubtaskNote(subtaskId, note)
  .then(t=>{
    return res.json({success:true});
  })
  .catch((e)=>{
    return res.json({success:false, error: e});
  })
})
/////////////////////////////END OF SUBTASK CALLS//////////////////////

//////////////////////////////TASK CALLS////////////////////////////////
//Get project tasks
router.get('/tasks', auth.authenticate, function(req, res, next){
  let projectId = req.query.projectId;
  let category = req.query.categoryTask;
  let active = req.query.activeTask;
  let completion = req.query.completionTask;
  let showMyInputTasks = req.query.showMyInputTasks;
  let showMyTasks = req.query.showMyTasks;
  dbTasks.getTasksByProjectIdFixed(projectId, parseInt(category), parseInt(active)).then(tasks=>{
    //tasks = multipleWorkers(tasks);
    if(completion){
      if(completion == 1)
        tasks = tasks.filter(t => t.completion != 100);
      else if(completion == 2)
        tasks = tasks.filter(t => t.completion == 100);
    }
    if(showMyInputTasks == 'true'){
      tasks = tasks.filter(t => t.author_id == req.session.user_id)
    }
    if(showMyTasks == 'true'){
      tasks = tasks.filter(t => (t.workers_id ? t.workers_id : '').split(',').find(f => f == req.session.user_id) == req.session.user_id)
    }
    addFormatedDateForTasks(tasks);
    return res.json({success:true, data:tasks});
  })
  .catch((e)=>{
    return res.json({success:false, error: e});
  })
})
//Get task by id
router.get('/task', auth.authenticate, function(req, res, next){
  let taskId = req.query.id;
  dbTasks.getTaskById(taskId)
  .then(task=>{
    return res.json({success:true, data:task});
  })
  .catch((e)=>{
    return res.json({success:false, error: e});
  })
})
//Add task on project (through modal)
router.post('/addtask', auth.authenticate, function(req, res,next){
  //console.log("iz modalnega");
  //debugger;
  let projectId = req.body.projectId;
  let taskName = req.body.taskName;
  let taskNote = req.body.taskNote;
  let duration = req.body.taskDuration;
  let start = req.body.taskStart;
  let finish = req.body.taskFinish;
  let completion = req.body.taskCompletion;
  let workersId = req.body.taskAssignment;
  let category = req.body.taskCategory;
  let priority = req.body.taskPriority;
  let weekend = req.body.taskWeekend;
  let override = req.body.override;
  let buildParts = JSON.parse(req.body.buildParts);
  let userId = req.session.user_id;
  let parentName = req.body.parentName;
  let parentStandard = req.body.parentStandard;
  let promises = [];
  if(!duration)
    duration = 0;
  //debugger;
  let taskAssignmentArray = [];
  if(workersId)
    taskAssignmentArray = workersId.split(",");
  //add even if there are conflicts, dont need to check them
  if(override == 1){
    //if there are workers (might join these two cases)
    dbTasks.addTask(projectId, taskName, duration, start, finish, category, priority, completion, weekend, taskNote).then(task=>{
      //debugger;
      if(workersId || buildParts.length > 0){
        let promisesWorkersBuildParts = [];
        //console.log(task);
        for(let i = 0; i < taskAssignmentArray.length; i++){
          promisesWorkersBuildParts.push(dbTasks.assignTask(task.id,taskAssignmentArray[i]));
          /*
          dbTasks.assignTask(task.id, taskAssignmentArray[i]).then(assignedTask=>{
            //console.log(assignedTask)
          })
          .catch((e)=>{
            return res.json({success:false, error: e});
          })
          */
        }
        if(buildParts.length > 0){
          dbBuildParts.addNewBuildPartFixed(task.id,parentName,parentStandard,0,'-','-','SESTAV','',null,'true').then(parentBuildPart => {
            for(let i = 0, l = buildParts.length; i < l; i++){
              promisesWorkersBuildParts.push(dbBuildParts.addNewBuildPartFixed(task.id,buildParts[i].name,buildParts[i].standard,buildParts[i].position,buildParts[i].quantity,buildParts[i].material,buildParts[i].note,buildParts[i].tag,parentBuildPart.id));
            }
            Promise.all(promisesWorkersBuildParts).then(workersBuildparts => {
              //save system changes for build parts, first remove worker return promises
              workersBuildparts = workersBuildparts.splice(taskAssignmentArray.length);
              let promisesBPChanges = [];
              promisesBPChanges.push(dbChanges.addNewChangeSystem(1,22,userId,null,task.id,null,null,null,null,null,null,null,null,parentBuildPart.id));
              for(var i = 0; i < workersBuildparts.length; i++){
                promisesBPChanges.push(dbChanges.addNewChangeSystem(1,22,userId,null,task.id,null,null,null,null,null,null,null,null,workersBuildparts[i].id));
              }
              Promise.all(promisesBPChanges).then(chnagesBP => {
                return res.json({success:true, newTaskId:task.id, conflicts:null, case:"no workers or no dates"});
              })
              .catch((e) => {
                return res.json({success:false, error: e});
              })
            })
            .catch((e) => {
              return res.json({success:false, error: e});
            })
          })
          .catch((e) => {
            return res.json({success:false, error: e});
          })
        }
        else{
          Promise.all(promisesWorkersBuildParts).then(workersBuildparts => {
            //save system changes for build parts, first remove worker return promises
            workersBuildparts = workersBuildparts.splice(taskAssignmentArray.length);
            let promisesBPChanges = [];
            for(var i = 0; i < workersBuildparts.length; i++){
              promisesBPChanges.push(dbChanges.addNewChangeSystem(1,22,userId,null,task.id,null,null,null,null,null,null,null,null,workersBuildparts[i].id));
            }
            Promise.all(promisesBPChanges).then(chnagesBP => {
              return res.json({success:true, newTaskId:task.id, conflicts:null, case:"override"});  
            })
            .catch((e) => {
              return res.json({success:false, error: e});
            })
          })
          .catch((e) => {
            return res.json({success:false, error: e});
          })
        }
      }
      else{
        return res.json({success:true, newTaskId:task.id, conflicts:null, case:"override"});
      }
    })
    .catch((e)=>{
      return res.json({success:false, error: e});
    })
  }
  //no override, check if there are conflicts if there are workers (and both dates)
  else{
    if(workersId && start && finish){
      for(let i=0; i<taskAssignmentArray.length; i++){
        promises.push(dbTasks.getWorkerConflicts(taskAssignmentArray[i], projectId, start, finish));
      }
      Promise.all(promises)
        .then((results)=>{
          //debugger;
          let conflictsExists = false;
          if(results.length > 0){
            for(let i = 0; i < results.length; i++){
              if(results[i].length > 0)
                conflictsExists = true;
            }
          }
          // there are conflicts, send them back and tell adding wasnt successful
          if(conflictsExists == true)
            res.json({success:false, newTaskId:null, conflicts:results, case:"normal, conflicts"});
          // no conflicts, add new task with new assignments
          else if(conflictsExists == false){
            dbTasks.addTask(projectId, taskName, duration, start, finish, category, priority, completion, weekend, taskNote).then(task=>{
              //debugger
              if(workersId || buildParts.length > 0){
                let promisesWorkersBuildParts = [];
                //console.log(task);
                for(let i = 0; i < taskAssignmentArray.length; i++){
                  promisesWorkersBuildParts.push(dbTasks.assignTask(task.id, taskAssignmentArray[i]));
                  /*
                  dbTasks.assignTask(task.id, taskAssignmentArray[i]).then(assignedTask=>{
                    //console.log(assignedTask)
                  })
                  .catch((e)=>{
                    return res.json({success:false, error: e});
                  })
                  */
                }
                if(buildParts.length > 0){
                  dbBuildParts.addNewBuildPartFixed(task.id,parentName,parentStandard,0,'-','-','SESTAV','',null,'true').then(parentBuildPart => {
                    for(let i = 0, l = buildParts.length; i < l; i++){
                      promisesWorkersBuildParts.push(dbBuildParts.addNewBuildPartFixed(task.id,buildParts[i].name,buildParts[i].standard,buildParts[i].position,buildParts[i].quantity,buildParts[i].material,buildParts[i].note,buildParts[i].tag,parentBuildPart.id));
                    }
                    Promise.all(promisesWorkersBuildParts).then(workersBuildparts => {
                      //save system changes for build parts, first remove worker return promises
                      workersBuildparts = workersBuildparts.splice(taskAssignmentArray.length);
                      let promisesBPChanges = [];
                      promisesBPChanges.push(dbChanges.addNewChangeSystem(1,22,userId,null,task.id,null,null,null,null,null,null,null,null,parentBuildPart.id));
                      for(var i = 0; i < workersBuildparts.length; i++){
                        promisesBPChanges.push(dbChanges.addNewChangeSystem(1,22,userId,null,task.id,null,null,null,null,null,null,null,null,workersBuildparts[i].id));
                      }
                      Promise.all(promisesBPChanges).then(chnagesBP => {
                        return res.json({success:true, newTaskId:task.id, conflicts:null, case:"no workers or no dates"});
                      })
                      .catch((e) => {
                        return res.json({success:false, error: e});
                      })
                    })
                    .catch((e) => {
                      return res.json({success:false, error: e});
                    })
                  })
                  .catch((e) => {
                    return res.json({success:false, error: e});
                  })
                }
                else{
                  Promise.all(promisesWorkersBuildParts).then(workersBuildparts => {
                    //save system changes for build parts, first remove worker return promises
                    workersBuildparts = workersBuildparts.splice(taskAssignmentArray.length);
                    let promisesBPChanges = [];
                    for(var i = 0; i < workersBuildparts.length; i++){
                      promisesBPChanges.push(dbChanges.addNewChangeSystem(1,22,userId,null,task.id,null,null,null,null,null,null,null,null,workersBuildparts[i].id));
                    }
                    Promise.all(promisesBPChanges).then(chnagesBP => {
                      return res.json({success:true, newTaskId:task.id, conflicts:null, case:"normal, no conflicts"});
                    })
                    .catch((e) => {
                      return res.json({success:false, error: e});
                    })
                  })
                  .catch((e) => {
                    return res.json({success:false, error: e});
                  })
                }
              }
              else{
                return res.json({success:true, newTaskId:task.id, conflicts:null, case:"normal, no conflicts"});
              }
            })
            .catch((e)=>{
              return res.json({success:false, error: e});
            })
          }
      })
      .catch((e)=>{
        return res.json({success:false, error: e});
      })
    }
    else{
      //debugger
      //either there were no workers or both dates
      dbTasks.addTask(projectId, taskName, duration, start, finish, category, priority, completion, weekend, taskNote).then(task=>{
        //debugger;
        if(workersId || buildParts.length > 0){
          //console.log(task);
          let promisesWorkersBuildParts = [];
          for(let i = 0; i < taskAssignmentArray.length; i++){
            promisesWorkersBuildParts.push(dbTasks.assignTask(task.id, taskAssignmentArray[i]));
            /*
            dbTasks.assignTask(task.id, taskAssignmentArray[i]).then(assignedTask=>{
              //console.log(assignedTask)
            })
            .catch((e)=>{
              return res.json({success:false, error: e});
            })
            */
          }
          if(buildParts.length > 0){
            dbBuildParts.addNewBuildPartFixed(task.id,parentName,parentStandard,0,'-','-','SESTAV','',null,'true').then(parentBuildPart => {
              for(let i = 0, l = buildParts.length; i < l; i++){
                promisesWorkersBuildParts.push(dbBuildParts.addNewBuildPartFixed(task.id,buildParts[i].name,buildParts[i].standard,buildParts[i].position,buildParts[i].quantity,buildParts[i].material,buildParts[i].note,buildParts[i].tag,parentBuildPart.id));
              }
              Promise.all(promisesWorkersBuildParts).then(workersBuildparts => {
                //save system changes for build parts, first remove worker return promises
                workersBuildparts = workersBuildparts.splice(taskAssignmentArray.length);
                let promisesBPChanges = [];
                promisesBPChanges.push(dbChanges.addNewChangeSystem(1,22,userId,null,task.id,null,null,null,null,null,null,null,null,parentBuildPart.id));
                for(var i = 0; i < workersBuildparts.length; i++){
                  promisesBPChanges.push(dbChanges.addNewChangeSystem(1,22,userId,null,task.id,null,null,null,null,null,null,null,null,workersBuildparts[i].id));
                }
                Promise.all(promisesBPChanges).then(chnagesBP => {
                  return res.json({success:true, newTaskId:task.id, conflicts:null, case:"no workers or no dates"});
                })
                .catch((e) => {
                  return res.json({success:false, error: e});
                })
              })
              .catch((e) => {
                return res.json({success:false, error: e});
              })
            })
            .catch((e) => {
              return res.json({success:false, error: e});
            })
          }
          else{
            Promise.all(promisesWorkersBuildParts).then(workersBuildparts => {
              //save system changes for build parts, first remove worker return promises
              workersBuildparts = workersBuildparts.splice(taskAssignmentArray.length);
              let promisesBPChanges = [];
              for(var i = 0; i < workersBuildparts.length; i++){
                promisesBPChanges.push(dbChanges.addNewChangeSystem(1,22,userId,null,task.id,null,null,null,null,null,null,null,null,workersBuildparts[i].id));
              }
              Promise.all(promisesBPChanges).then(chnagesBP => {
                return res.json({success:true, newTaskId:task.id, conflicts:null, case:"no workers or no dates"});
              })
              .catch((e) => {
                return res.json({success:false, error: e});
              })
            })
            .catch((e) => {
              return res.json({success:false, error: e});
            })
          }
        }
        else{
          return res.json({success:true, newTaskId:task.id, conflicts:null, case:"no workers or no dates"});
        }
      })
      .catch((e)=>{
        return res.json({success:false, error: e});
      })
    }
  }
})
//remove task, set task as non active task
router.post('/removetask', auth.authenticate, function(req, res, next){
  //console.log("odstrani aktivnost");
  let taskId = req.body.taskId;
  dbTasks.removeTask(taskId).then(task=>{
    return res.json({success: true});
  })
  .catch((e)=>{
    return res.json({success: false, error: e});
  })
})
//update the task
router.post('/updatetask', auth.authenticate, function(req, res, next){
  let projectId = req.body.projectId;
  let taskId = req.body.taskId;
  let taskName = req.body.taskName;
  let taskNote = req.body.taskNote;
  let duration = req.body.taskDuration;
  let start = req.body.taskStart;
  let finish = req.body.taskFinish;
  let workersId = req.body.taskAssignment;
  let completion = req.body.taskCompletion;
  let active = req.body.taskActive;
  let category = req.body.taskCategory;
  let priority = req.body.taskPriority;
  let weekend = req.body.taskWeekend;
  let override = req.body.override;
  let currentWorkers = req.body.currentWorkers;
  let promises = [];
  if(!duration)
    duration = 0;
  if(!completion)
    completion = 0;
  let taskAssignmentArray = [];
  if(workersId)
    taskAssignmentArray = workersId.split(",");
  if(active == false)
    override = 1;
  if(override == 1){
    dbTasks.updateTask(taskId, taskName, duration, start, finish, completion, active, category, priority, weekend, taskNote).then(task => {
      if(workersId != currentWorkers){
        dbTasks.removeAssignTask(taskId).then(task => {
          if(workersId){
            for(let i = 0; i < taskAssignmentArray.length; i++){
              dbTasks.assignTask(taskId, taskAssignmentArray[i]).then(assignTask =>{
                //do nothing;
              })
              .catch((e)=>{
                return res.json({success:false, error: e});
              })
            }
          }
        })
        .catch((e)=>{
          return res.json({success:false, error: e});
        })
      }
      return res.json({success: true, conflicts:null, case:"override"});
    })
    .catch((e)=>{
      return res.json({success:false, error: e});
    })
  }
  else{
    if(workersId && start && finish){
      for(let i = 0; i < taskAssignmentArray.length; i++){
        promises.push(dbTasks.getWorkerConflicts(taskAssignmentArray[i], projectId, start, finish));
      }
      Promise.all(promises).then((results) => {
        let conflictsExists = false;
        if(results.length > 0){
          for(let i = 0; i < results.length; i++){
            if(results[i].length > 0)
              conflictsExists = true;
          }
        }
        if(conflictsExists == true){
          res.json({success:false, conflicts:results, case:"normal, conflicts"});
        }
        else if(conflictsExists == false){
          dbTasks.updateTask(taskId, taskName, duration, start, finish, completion, active, category, priority, weekend, taskNote).then(task => {
            if(workersId != currentWorkers){
              dbTasks.removeAssignTask(taskId).then(task =>{
                if(workersId){
                  for(let i = 0; i < taskAssignmentArray.length; i++){
                    dbTasks.assignTask(taskId, taskAssignmentArray[i]).then(assignTask =>{
                      //do nothing;
                    })
                    .catch((e)=>{
                      return res.json({success:false, error: e});
                    })
                  }
                }
              })
              .catch((e) =>{
                return res.json({success:false, error: e});
              })
            }
            return res.json({success:true, conflicts:null, case:"normal, no conflicts"});
          })
          .catch((e)=>{
            return res.json({success:false, error: e});
          })
        }
      })
      .catch((e)=>{
        return res.json({success:false, error: e});
      })
    }
    else{
      dbTasks.updateTask(taskId, taskName, duration, start, finish, completion, active, category, priority, weekend, taskNote).then(task => {
        if(workersId != currentWorkers){
          dbTasks.removeAssignTask(taskId).then(task =>{
            if(workersId){
              for(let i = 0; i < taskAssignmentArray.length; i++){
                dbTasks.assignTask(taskId, taskAssignmentArray[i]).then(assignTask =>{
                  //do nothing;
                })
                .catch((e)=>{
                  return res.json({success:false, error: e});
                })
              }
            }
          })
          .catch((e)=>{
            return res.json({success:false, error: e});
          })
        }
        return res.json({success:true, conflicts:null, case:"no workers or no dates"});
      })
      .catch((e)=>{
        return res.json({success:false, error: e});
      })
    }
  }
})
//Save completion of task
router.post('/task/completion', auth.authenticate, function(req, res, next){
  let completion = req.body.completion;
  let taskId = req.body.taskId;
  if(completion == "NaN")
    completion = 0;
  if(completion === true || completion === false){
    if(completion){
      dbTasks.updateTaskCompletion(taskId, 100).then(task=>{
        dbTasks.finishTask(taskId).then(t => {
          return res.json({success:true});
        })
        .catch((e)=>{
          return res.json({success:false, error: e});
        })
      })
      .catch((e)=>{
        return res.json({success:false, error: e});
      })
    }
    else{
      dbTasks.updateTaskCompletion(taskId, 0)
      .then(task=>{
        return res.json({success:true});
      })
      .catch((e)=>{
        return res.json({success:false, error: e});
      })
    }
  }
  else{
    dbTasks.updateTaskCompletion(taskId, completion)
    .then(task=>{
      if(completion == 100){
        dbTasks.finishTask(taskId).then(t => {
          return res.json({success:true});
        })
        .catch((e)=>{
          return res.json({success:false, error: e});
        })
      }else{
        return res.json({success:true});
      }
    })
    .catch((e)=>{
      return res.json({success:false, error: e});
    })
  }
})
//add new finished date
router.post('/task/finished', auth.authenticate, function(req, res, next){
  let taskId = req.body.taskId;
  dbTasks.finishTask(taskId)
  .then(task=>{
    return res.json({success:true});
  })
  .catch((e)=>{
    return res.json({success:false, error: e});
  })
})
//Change/update subtask's note
router.post('/note', auth.authenticate, function(req, res, next){
  let note = req.body.newNote;
  let taskId = req.body.taskId;
  dbTasks.updateTaskNote(taskId, note)
  .then(t=>{
    return res.json({success:true});
  })
  .catch((e)=>{
    return res.json({success:false, error: e});
  })
})
/////////////////////////////END OF TASK CALLS//////////////////////

//////////////////////////////WORKER CALLS////////////////////////////////
//add worker on project
router.post('/worker/add', auth.authenticate, function(req, res, next){
  dbProjects.addWorker(req.body.worker,req.body.roleNew,req.body.projectId)
  .then(worker=>{
    //console.log(worker);
    return res.json({success: true, id: worker});
  })
  .catch((e)=>{
    return res.json({success:false, error: e});
  })
})
//remove worker from project
router.post('/worker/remove', auth.authenticate, function(req, res, next){
  let id = req.body.id;
  dbProjects.removeWorker(id)
  .then(worker=>{
    return res.json({success:true});
  })
  .catch((e)=>{
    return res.json({success:false, error: e});
  })
  //return res.end("success")
})
//Get workers on project (id, text) (for select2)
router.get('/workers', auth.authenticate, function(req, res, next){
  let projectId = req.query.projectId;
  dbProjects.getWorkersOnProject(projectId)
  .then(workers=>{
    res.json({success:true, data: workers});
  })
  .catch((e)=>{
    return res.json({success:false, error: e});
  })
})
//Get workers on project
router.get('/worker/get', auth.authenticate, function(req, res, next){
  let projectId = req.query.projectId;
  let taskId = req.query.taskId;
  if(projectId){
    dbProjects.getWorkersByProjectId(projectId)
    .then(workers=>{
      res.json({success:true, data: workers});
    })
    .catch((e)=>{
      return res.json({success:false, error: e});
    })
  }
  else if(taskId){
    dbProjects.getWorkersByTaskId(taskId)
    .then(workers=>{
      res.json({success:true, data: workers});
    })
    .catch((e)=>{
      return res.json({success:false, error: e});
    })
  }
})
/////////////////////////////END OF WORKERS CALLS//////////////////////
//get work roles
router.get('/roles', auth.authenticate, function(req, res, next){
  dbWorkRoles.getRoles()
  .then(roles=>{
    return res.json({success: true, data: roles});  
  })
  .catch((e)=>{
    return res.json({success:false, error: e});
  })
})
//get users -- possible workers
router.get('/users', auth.authenticate, function(req, res, next){
  dbProjects.getWorkers()
  .then(workers=>{
    return res.json({success: true, data: workers});  
  })
  .catch((e)=>{
    return res.json({success:false, error: e});
  })
})
//Get all projects
router.get('/all', auth.authenticate, function(req, res, next){
  let active = req.query.activeProject;
  let loggedUser = req.session.user_id;
  dbProjects.getProjects(parseInt(active), loggedUser)
  .then(projects=>{
    res.json({success:true, data:projects});
  })
  .catch((e)=>{
    return res.json({success:false, error: e});
  })
})
//////////////////////////////PROJECT DATES CALLS////////////////////////////////
//get all project dates
router.get('/dates/get', auth.authenticate, function(req, res, next){
  let projectId = req.query.projectId;
  dbDates.getProjectDates(parseInt(projectId))
  .then(dates=>{
    res.json({success:true, data:dates});
  })
  .catch((e)=>{
    return res.json({success: false, error: e});  
  })
})
//add new project date
router.post('/dates/add', auth.authenticate, function(req, res, next){
  let projectId = req.body.projectId;
  let name = req.body.name;
  let date = req.body.date;
  dbDates.addProjectDate(projectId, name, date)
  .then(date=>{
    return res.json({success: true, id: date});
  })
  .catch((e)=>{
    return res.json({success: false, error: e});  
  })
})
//update project date
router.post('/dates/update', auth.authenticate, function(req, res, next){
  let dateId = req.body.dateId;
  let name = req.body.name;
  let date = req.body.date;
  dbDates.editProjectDate(dateId, name, date)
  .then(date=>{
    return res.json({success: true, data: date});
  })
  .catch((e)=>{
    return res.json({success: false, error: e});  
  })
})
//remove project date
router.post('/dates/remove', auth.authenticate, function(req, res, next){
  let id = req.body.id;
  dbDates.removeProjectDate(id)
  .then(worker=>{
    return res.end("success");
  })
  .catch((e)=>{
    return res.json({success: false, error: e});  
  })
})
//////////////////////////////EXCEL CALLS//////////////////////////////////////
//get info about projects depending on input checkboxes from form
router.get('/excel', auth.authenticate, function(req, res, next){
  let status = parseInt(req.query.status);
  let numberCheck = (req.query.numberCheck === 'true');
  let subCheck = (req.query.subCheck === 'true');
  let startCheck = (req.query.startCheck === 'true');
  let finishCheck = (req.query.finishCheck === 'true');
  let completionCheck = (req.query.completionCheck === 'true');
  let leaderCheck = (req.query.leaderCheck === 'true');
  let builderCheck = (req.query.builderCheck === 'true');
  let mechanicCheck = (req.query.mechanicCheck === 'true');
  let electricanCheck = (req.query.electricanCheck === 'true');
  let plcCheck = (req.query.plcCheck === 'true');
  let robotCheck = (req.query.robotCheck === 'true');
  let otherCheck = (req.query.otherCheck === 'true');
  dbProjects.getProjectsForExcel(status, numberCheck, subCheck, startCheck, finishCheck, completionCheck, leaderCheck, builderCheck, mechanicCheck, electricanCheck, plcCheck, robotCheck, otherCheck)
  .then(projects=>{
    return res.json({success: true, data: projects});
  })
  .catch((e)=>{
    return res.json({success:false, error: e});
  })
})
//////////////////////////MODIFICATION CALLS///////////////////////////////////
//get all project modifications
router.get('/modifications', auth.authenticate, function(req, res, next){
  let projectId = req.query.projectId;
  dbModification.getProjectModifications(projectId)
  .then(modifications=>{
    dbModification.getModStatus()
    .then(modStatuses => {
      return res.json({success:true, modifications, modStatuses});
    })
    .catch((e)=>{
      return res.json({success: false, error: e});  
    })
  })
  .catch((e)=>{
    return res.json({success: false, error: e});  
  })
})
//add new project modification
router.post('/modifications', auth.authenticate, function(req, res, next){
  let modName = req.body.modName;
  let modCategoryId = req.body.modCategoryId;
  let modStatusId = req.body.modStatusId;
  let projectId = req.body.projectId;
  let userId = req.session.user_id;
  //let present = req.body.present.split(',');
  //console.log(present);
  if(!modName || !modCategoryId || !modStatusId || !projectId){
    return res.json({success:false, message:'Manjkajoči podatki. Ni imena popravka oz. kategorije oz. statusa oz. ključa projekta.'});
  }
  dbModification.addModification(modName, modStatusId, modCategoryId, userId, projectId).then(modification => {
    return res.json({success:true, modification});
  })
  .catch((e)=>{
    return res.json({success:false, error:e});
  })
})
//edit modification (status || name && category)
router.put('/modifications', auth.authenticate, function(req, res, next){
  let modId = req.body.id;
  let modName = req.body.modName;
  let modStatusId = req.body.modStatusId;
  let modCategoryId = req.body.modCategoryId;
  //let present = req.body.present.split(',');
  //console.log(present);
  if(modStatusId){
    dbModification.updateModStatus(modId, modStatusId).then(modification => {
      return res.json({success:true, modification});
    })
    .catch((e)=>{
      return res.json({success:false, error:e});
    })
  }
  else{
    if(!modName || !modCategoryId){
      return res.json({success:false, message:'Manjkajoči podatki. Ni imena popravka oz. kategorije popravka.'});
    }
    else{
      dbModification.updateModification(modId,modName,modCategoryId).then(modification => {
        return res.json({success:true, modification});
      })
      .catch((e)=>{
        return res.json({success:false, error:e});
      })
    }
  }
})
//delete modification ( update mod's active attribute )
router.delete('/modifications', auth.authenticate, function(req, res, next){
  let modId = req.body.modId;
  let active = req.body.active;
  dbModification.deleteModification(modId, active).then(modification => {
    return res.json({success:true, modification});
  })
  .catch((e)=>{
    return res.json({success:false, error:e});
  })
})
//get all changes for project modification
router.get('/modifications/changes', auth.authenticate, function(req, res, next){
  let modId = req.query.modId;
  dbModification.getModsChangesSystem(modId).then(modChanges => {
    return res.json({success:true, modChanges});
  })
  .catch((e)=>{
    return res.json({success:false, error:e});
  })
})
//////////////////////////MODIFICATION FILES/////////////////////////////////
//Get modification's files
router.get('/modifications/files', auth.authenticate, function(req, res, next){
  //let projectId = req.query.projectId;
  //let taskId = req.query.taskId;
  let projectModId = req.query.projectModId;
  dbFiles.getProjectModificationFiles(projectModId).then(files=>{
    res.json({success:true, data: files});
  })
  .catch((e)=>{
    return res.json({success:false, error: e});
  })
})
//////////////////////////CALLS FROM OTHER SITES///////////////////////////////
//Get workers on project
router.get('/workerid', auth.authenticate, function(req, res, next){
  let projectId = req.query.projectId;
  let taskId = req.query.taskId;
  if(projectId){
    dbProjects.getWorkersByProjectId(projectId)
    .then(workers=>{
      res.json({success:true, data: workers});
    })
    .catch((e)=>{
      return res.json({success:false, error: e});
    })
  }
  else if(taskId){
    dbProjects.getWorkersByTaskId(taskId)
    .then(workers=>{
      res.json({success:true, data: workers});
    })
    .catch((e)=>{
      return res.json({success:false, error: e});
    })
  }
})
module.exports = router;