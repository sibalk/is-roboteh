var express = require('express');
var router = express.Router();
var bcrypt = require('bcryptjs');
var dateFormat = require('dateformat');
const saltRounds = 10;
const csp = require('helmet-csp');

//auth & dbModels
var auth = require('../controllers/authentication');
var dbProjects = require('../model/projects/dbProjects');
var dbTasks = require('../model/tasks/dbTasks');
var dbEmployees = require('../model/employees/dbEmployees');
var dbAbsences = require('../model/absences/dbAbsences');
var dbRoles = require('../model/roles/dbRoles');
var dbWorkRoles = require('../model/workRoles/dbWorkRoles');

let routerPdfEmployees = require('./pdfemployees');
router.use('/pdfs', routerPdfEmployees);

var normalCspHandler = csp({
  directives: {
    defaultSrc: ["'self'"],
    scriptSrc: ["'self'"],
    styleSrc: ["'self'", "https://fonts.googleapis.com", "https://use.fontawesome.com"],
    fontSrc: ["'self'", "https://fonts.gstatic.com", "https://use.fontawesome.com"],
    imgSrc: ["'self'", "data:"],
    frameAncestors: ["'self'"],
  }
});

///////////////////////////////////////FUNCTIONS
function compareValues(key, order = 'asc') {
  return function innerSort(a, b) {
    if (!a.hasOwnProperty(key) || !b.hasOwnProperty(key)) {
      // property doesn't exist on either object
      return 0;
    }

    const varA = (typeof a[key] === 'string')
      ? a[key].toUpperCase() : a[key];
    const varB = (typeof b[key] === 'string')
      ? b[key].toUpperCase() : b[key];

    let comparison = 0;
    if (varA > varB) {
      comparison = 1;
    } else if (varA < varB) {
      comparison = -1;
    }
    return (
      (order === 'desc') ? (comparison * -1) : comparison
    );
  };
}

function formatDate(date){
  return {
    "date": dateFormat(date, "d. m. yyyy"),
    "time": dateFormat(date, "HH:MM")
  }
}
function addFormatedDateForTasks(tasks){
  //console.log("test");
  for(let i=0; i < tasks.length; i++) {
    if(tasks[i] && tasks[i].task_start)
      tasks[i]["formatted_start"] = formatDate(tasks[i].task_start);
    if(tasks[i] && tasks[i].task_finish)
      tasks[i]["formatted_finish"] = formatDate(tasks[i].task_finish);
  }
  return tasks;
}
function addFormatedDateForAbsences(absences){
  //console.log("test");
  for(let i=0; i < absences.length; i++) {
    if(absences[i] && absences[i].start)
      absences[i]["formatted_start"] = formatDate(absences[i].start);
    if(absences[i] && absences[i].finish)
      absences[i]["formatted_finish"] = formatDate(absences[i].finish);
  }
  return absences;
}

///////////////////////////////////////ROUTES
router.get('/', normalCspHandler, auth.authenticate, function(req, res) {
  res.render('employees', {
    title: "Seznam zaposlenih",
    user_name: req.session.user_name,
    user_surname: req.session.user_surname,
    user_typeid: req.session.role_id,
    user_type: req.session.type,
    user_id: req.session.user_id,
  })
})

router.get('/userId', normalCspHandler, auth.authenticate, function(req, res, next) {
  let userId = req.query.userId;
  if(!userId){
    console.error("No userId provided in query");
    return res.redirect('/employees');
  }
  dbEmployees.getUserById(userId, req.session.user_id).then(employee =>{
    if(employee){
      return dbEmployees.getUserProjects(userId)
      .then(projects=>{return {employee, projects}})
      .then(data=>{
        //found employee with projects
        //console.log(data);
        let projects = data.projects;
        let tmp1 = projects.filter(p => p.project_number.substring(0,2) == 'RT' );
        let tmp2 = projects.filter(p => p.project_number.substring(0,3) == 'STR' );
        let tmp3 = projects.filter(p => p.project_number.substring(0,3) != 'STR' && p.project_number.substring(0,2) != 'RT');
        tmp1.forEach(p => p.number_order = parseInt(p.project_number.substring(2)));
        tmp2.forEach(p => p.number_order = parseInt(p.project_number.substring(3)));
        let sortedTmp1 = tmp1.sort(function(a,b){
          if (isNaN(a.number_order))
            return 1;
          else if (isNaN(b.number_order))
            return -1;
          return b.number_order - a.number_order;
        });
        let sortedTmp2 = tmp2.sort(function(a,b){
          if (isNaN(a.number_order))
            return 1;
          else if (isNaN(b.number_order))
            return -1;
          return b.number_order - a.number_order;
        });
        //let allRTProjects = sortedTmp1, allSTRProjects = sortedTmp2;
        projects = sortedTmp2.concat(sortedTmp1,tmp3);//all projects sorted based on their number and prefix
        if(data.projects){
          res.render('employee', {
            title: "Moji projekti - " + data.employee.name + ' ' + data.employee.surname,
            user_name: req.session.user_name,
            user_username: req.session.user_username,
            user_surname: req.session.user_surname,
            user_typeid: req.session.role_id,
            user_type: req.session.type,
            user_id: req.session.user_id,
            employee: data.employee,
            projects: projects
          })
        }
        //found employee not projects
        else{
          res.render('employee', {
            title: "Moji projekti - " + req.session.user_name + ' ' + req.session.user_surname,
            user_name: req.session.user_name,
            user_surname: req.session.user_surname,
            user_username: req.session.user_username,
            user_typeid: req.session.role_id,
            user_type: req.session.type,
            user_id: req.session.user_id,
            employee: data.employee,
            projects: []
          })
        }
      })
      .catch((e)=>{
        next(e);
      })
    }
    //found nothing
    else{
      res.render('employee', {
        title: "Moji projekti - " + req.session.user_name + ' ' + req.session.user_surname,
        user_name: req.session.user_name,
        user_surname: req.session.user_surname,
        user_username: req.session.user_username,
        user_typeid: req.session.role_id,
        user_type: req.session.type,
        user_id: req.session.user_id,
        employee: null,
        projects: []
      })
    }
  })
  .catch((e)=>{
    next(e);
  })
})
//POGLED OPRAVIL ZAPOSLENEGA // TEKOČA OPRAVILA
router.get('/tasks', normalCspHandler, auth.authenticate, function(req, res, next){
  let userId = req.query.userId;
  let projectId = req.query.projectId;
  if(projectId){
    dbEmployees.getUserById(userId, req.session.user_id).then(employee =>{
      if(employee){
        return dbProjects.getProjectUserTasks(projectId, userId)
          .then(tasks=>{return {employee, tasks}})
          .catch((e)=>{
            next(e);
          })
        .then(data=>{
          addFormatedDateForTasks(data.tasks);
          //found employee with tasks
          if(data.tasks){
            return dbProjects.getProjectById(projectId)
              .then(project=>{return {data,project}})
              .catch((e)=>{
                console.log(e);
              })
            .then(data=>{
              if(data.project){
                res.render('tasks', {
                  title: "Moja opravila "+data.project.project_number+'-'+data.project.project_name,
                  user_name: req.session.user_name,
                  user_username: req.session.user_username,
                  user_surname: req.session.user_surname,
                  user_typeid: req.session.role_id,
                  user_type: req.session.type,
                  user_id: req.session.user_id,
                  employee: data.data.employee,
                  tasks: data.data.tasks,
                  project: data.project
                })
              }
              else{
                res.render('tasks', {
                  title: "Moja opravila",
                  user_name: req.session.user_name,
                  user_username: req.session.user_username,
                  user_surname: req.session.user_surname,
                  user_typeid: req.session.role_id,
                  user_type: req.session.type,
                  user_id: req.session.user_id,
                  employee: data.data.employee,
                  tasks: data.data.tasks,
                  project: null
                })
              }
            })
            .catch((e)=>{
              next(e);
            })
          }
          //found employee not tasks
          else{
            res.render('tasks', {
              title: "Moja opravila",
              user_name: req.session.user_name,
              user_surname: req.session.user_surname,
              user_username: req.session.user_username,
              user_typeid: req.session.role_id,
              user_type: req.session.type,
              user_id: req.session.user_id,
              employee: data.employee,
              tasks: [],
              project: null
            })
          }
        })
        .catch((e)=>{
          next(e);
        })
      }
      //found nothing
      else{
        res.render('tasks', {
          title: "Moja opravila",
          user_name: req.session.user_name,
          user_surname: req.session.user_surname,
          user_username: req.session.user_username,
          user_typeid: req.session.role_id,
          user_type: req.session.type,
          user_id: req.session.user_id,
          employee: null,
          tasks: [],
          project: null
        })
      }
    })
    .catch((e)=>{
      next(e);
    })
  }
  else{
    dbEmployees.getUserById(userId, req.session.user_id).then(employee =>{
      if(employee){
        return dbTasks.getWorkerTasks(userId)
          .then(tasks=>{return {employee, tasks}})
          .catch((e)=>{
            next(e);
          })
        .then(data=>{
          addFormatedDateForTasks(data.tasks);
          //found employee with tasks
          if(data.tasks){
            res.render('tasks', {
              title: "Moja opravila",
              user_name: req.session.user_name,
              user_username: req.session.user_username,
              user_surname: req.session.user_surname,
              user_typeid: req.session.role_id,
              user_type: req.session.type,
              user_id: req.session.user_id,
              employee: data.employee,
              tasks: data.tasks
            })
          }
          //found employee not tasks
          else{
            res.render('tasks', {
              title: "Moja opravila",
              user_name: req.session.user_name,
              user_surname: req.session.user_surname,
              user_username: req.session.user_username,
              user_typeid: req.session.role_id,
              user_type: req.session.type,
              user_id: req.session.user_id,
              employee: data.employee,
              tasks: []
            })
          }
        })
        .catch((e)=>{
          next(e);
        })
      }
      //found nothing
      else{
        res.render('tasks', {
          title: "Moja opravila",
          user_name: req.session.user_name,
          user_surname: req.session.user_surname,
          user_username: req.session.user_username,
          user_typeid: req.session.role_id,
          user_type: req.session.type,
          user_id: req.session.user_id,
          employee: null,
          tasks: []
        })
      }
    })
    .catch((e)=>{
      next(e);
    })
  }
})
//POGLEDD ODSOTNOSTI ZAPOSLENEGA
router.get('/absence', auth.authenticate, function(req, res, next){
  let userId = req.query.id;
  let roleType = req.session.type.toLowerCase();
  if(!userId){
    console.error("No userId provided in query");
    return res.redirect('/dashboard');
  }
  dbEmployees.getUserById(userId, req.session.user_id).then(employee =>{
    if(employee){
      return dbAbsences.getUserAbsence(userId)
        .then(absences=>{return {employee, absences}})
        .catch((e)=>{
          next(e);
        })
      .then(data=>{
        //found employee with absences
        if(data.absences){
          addFormatedDateForAbsences(data.absences);
          if (roleType == 'admin' || roleType == 'tajnik' || roleType == 'komercialist' || roleType == 'komerciala' || roleType == 'računovodstvo' || roleType.substring(0,5) == 'vodja' || req.session.user_id == userId)
            res.render('absence', {
              title: "Moje odsotnosti",
              user_name: req.session.user_name,
              user_username: req.session.user_username,
              user_surname: req.session.user_surname,
              user_typeid: req.session.role_id,
              user_type: req.session.type,
              user_id: req.session.user_id,
              employee: data.employee,
              absences: data.absences
            })
          else
            res.redirect('/dashboard');
        }
        //found employee not absences
        else{
          if(roleType == 'admin' || roleType == 'tajnik' || roleType == 'komercialist' || roleType == 'komerciala' || roleType == 'računovodstvo' || roleType.substring(0,5) == 'vodja' || req.session.user_id == userId)
            res.render('absence', {
              title: "Moje odsotnosti",
              user_name: req.session.user_name,
              user_surname: req.session.user_surname,
              user_username: req.session.user_username,
              user_typeid: req.session.role_id,
              user_type: req.session.type,
              user_id: req.session.user_id,
              employee: data.employee,
              absences: []
            })
          else
            res.redirect('/dashboard');
        }
      })
      .catch((e)=>{
        next(e);
      })
    }
    //found nothing
    else{
      res.render('absence', {
        title: "Moje odsotnosti",
        user_name: req.session.user_name,
        user_surname: req.session.user_surname,
        user_username: req.session.user_username,
        user_typeid: req.session.role_id,
        user_type: req.session.type,
        user_id: req.session.user_id,
        employee: null,
        projects: []
      })
    }
  })
  .catch((e)=>{
    next(e);
  })
})
//Get tasks for specific project and specific user
router.get('/projectTasks', auth.authenticate, function(req, res, next){
  let projectId = req.query.projectId;
  let userId = req.query.userId;
  if(projectId && userId){
    dbProjects.getProjectUserTasks(projectId, userId).then(tasks=>{
      res.json({success: true, data: tasks});
    })
    .catch((e)=>{
      return res.json({success:false, error:e});
    })
  }
  else{
    let workerId = req.query.userId;
    let taskAssignmentArray = workerId.split(",");
    if(taskAssignmentArray[0] == "")
      taskAssignmentArray.pop();
    let promises = [];
    if(taskAssignmentArray){
      for(let i=0; i<taskAssignmentArray.length; i++){
        promises.push(dbTasks.getWorkerTasks(taskAssignmentArray[i]));
      }
      Promise.all(promises)
      .then((results)=>{
        res.json({success:true, data:results});
      })
      .catch((e)=>{
        return res.json({success:false, error: e});
      })
    }
    else{
      //sem po pravem nebi smel pridet
      res.json({success:true, data:[]});
    }
  }
})
//Get workers for specific task
router.get('/workers', auth.authenticate, function(req, res, next){
  let taskId = req.query.taskId;
  dbTasks.getTaskWorkers(taskId)
  .then(workers=>{
    res.json({success: true, data: workers});
  })
  .catch((e)=>{
    return res.json({success:false, error:e});
  })
})
//Get user roles
router.get('/roles', auth.authenticate, function(req, res, next){
  dbRoles.getUserRoles().then(roles=>{
    res.json({success: true, data: roles});
  })
  .catch((e)=>{
    return res.json({success:false, error:e});
  })
})
//Get usernames
router.get('/usernames', auth.authenticate, function(req, res, next){
  dbEmployees.getUsernames().then(usernames=>{
    res.json({success: true, data: usernames});
  })
  .catch((e)=>{
    return res.json({success:false, error:e});
  })
})
//Get all users
router.get('/all', auth.authenticate, function(req, res, next){
  let loggedUserId = req.session.user_id;
  let active = req.query.activeUser;
  dbEmployees.getUsersWithSupervisors(loggedUserId, parseInt(active)).then(users=>{
    // sort users by their roles in order konstrukter, strojnik, električar, programer plc-jev, programer robotov, vodja, vodja projektov, vodja strojnikov, vodja CNC obdelave, vodja električarjev, vodja programerjev, tajnik, komercialist, računovodstvo, komercialist, admin, info, študent
    res.json({success: true, data: users});
  })
  .catch((e)=>{
    return res.json({success:false, error:e});
  })
})
//get user
router.get('/user', auth.authenticate, function(req, res, next){
  let userId = req.query.userId;
  dbEmployees.getUserById(userId, req.session.user_id).then(user=>{
    res.json({success: true, data: user});
  })
  .catch((e)=>{
    return res.json({success:false, error:e});
  })
})
//Create new employee
router.post('/create', auth.authenticate, function(req, res, next){
  if(req.body.userPassword === req.body.userPasswordAgain){
    let supervisors = req.body.supervisor;
    let hash = bcrypt.hashSync(req.body.userPassword, saltRounds);
    let supervisorArray = supervisors.split(",");
    let newUser;
    if(supervisorArray[0] == "")
      supervisorArray.pop();
    dbEmployees.createUser(req.body.userUsername, req.body.userName, req.body.userSurname, req.body.userRole, hash, req.body.workMail)
    .then(user =>{
      newUser = user;
      // add supervisors to the new user with promises
      let promises = [];
      for(let i=0; i<supervisorArray.length; i++){
        promises.push(dbEmployees.addSupervisor(user.id, supervisorArray[i]));
      }
      return Promise.all(promises)
    })
    .then(()=>{
      return res.json({success: true, id: newUser});
    })
    .catch((e)=>{
      return res.json({success:false, error:e});
    })
  }
  else{
    return res.json({success: false, data: "Gesli se ne ujemata!"})
  }
})
//Update user (for now name and surname; password and role on user's page)
router.post('/update', auth.authenticate, function(req, res, next){
  let name = req.body.newName;
  let surname = req.body.newSurname;
  let id = req.body.userId;
  let active = req.body.newActive;
  let username = req.body.newUsername;
  let workMail = req.body.newWorkMail;
  let supervisors = req.body.newSupervisors;
  let supervisorArray = supervisors.split(",");
  if(supervisorArray[0] == "")
    supervisorArray.pop();
  // get user info
  dbEmployees.getUserById(id, req.session.user_id).then(user=>{
    let promises = [];
    // push call for update user info to promises
    promises.push(dbEmployees.updateUser(id, name, surname, active, username, workMail));
    
    let userSupervisors = user.supervisors_ids ? user.supervisors_ids.split(',') : []
    // check which supervisors are new and which are old, then add or remove them
    // old supervisors are in user.supervisors_ids and new supervisors are in supervisorArray
    // if supervisor is in user.supervisors_ids but not in supervisorArray, remove it
    // if supervisor is in supervisorArray but not in user.supervisors_ids, add it
    // if supervisor is in both, do nothing
    for(let i=0; i<supervisorArray.length; i++){
      if(userSupervisors.indexOf(supervisorArray[i]) == -1){
        promises.push(dbEmployees.addSupervisor(id, supervisorArray[i]));
      }
    }
    for(let i=0; i<userSupervisors.length; i++){
      if(supervisorArray.indexOf(userSupervisors[i]) == -1){
        promises.push(dbEmployees.removeSupervisor(id, userSupervisors[i]));
      }
    }

    return Promise.all(promises)
  })
  .then(([updatedUser, updatedSupervisors]) => {
    return res.json({success: true});
  })
  // dbEmployees.updateUser(id, name, surname, active, username).then(user=>{
  //   return res.json({success:true});
  // })
  .catch((e)=>{
    return res.json({success:false, error:e});
  })
})
//Delete user, set as inactive user
router.post('/delete', auth.authenticate, function(req, res, next){
  //console.log("odstrani aktivnost");
  let userId = req.body.userId;
  dbEmployees.removeUser(userId).then(user=>{
    return res.json({success: true})
  })
  .catch((e)=>{
    return res.json({success: false, error: e})
  })
})
//Update password
router.post('/password', auth.authenticate, function(req, res, next){
  let userId = req.body.userId;
  //console.log(userId);
  if(req.body.userPassword === req.body.userPasswordAgain){
    //console.log("gesli se ujemata")
    let hash = bcrypt.hashSync(req.body.userPassword, saltRounds);
    dbEmployees.updatePassword(userId, hash).then(user=>{
      return res.json({success: true, id: user})
    })
    .catch((e)=>{
      return res.json({success:false, error:e});
    })
  }
  else{
    return res.json({success: false, data: "Gesli se ne ujemata!"})
  }
})
//Update role
router.post('/role', auth.authenticate, function(req, res, next){
  let userId = req.body.userId;
  let role = req.body.userRole;
  dbEmployees.updateRole(userId, role).then(role=>{
    if(parseInt(userId) == req.session.user_id){
      dbRoles.getUserRoles().then(roles=>{
        var selectedRole = roles.find(r => r.id === parseInt(req.body.userRole))
        if(selectedRole){
          req.session.type = selectedRole.text
          return res.json({success: true, id: role})
        }
      })
      .catch((e)=>{
        return res.json({success:false, error:e});
      })
    }
    else{
      console.log("vloga nima veze na vpisanega up")
      return res.json({success: true, id: role})
    }
  })
  .catch((e)=>{
    return res.json({success:false, error:e});
  })
})
//add new system role (electrican, machanic, plc, robot, builder)
router.post('/addrole', auth.authenticate, function(req, res, next){
  let role = req.body.role;
  let type = req.body.newRoleType;
  if(type == 0){
    dbRoles.addNewRole(role).then(role=>{
      return res.json({success:true, roleId:role.id});
    })
    .catch((e)=>{
      return res.json({success:false, error:e});
    })
  }
  else{
    dbWorkRoles.addNewProjectRole(role).then(role=>{
      return res.json({success:true, workRoleId:role.id});
    })
    .catch((e)=>{
      return res.json({success:false, error:e});
    })
  }
})

//add new project role 
router.post('/addprojectrole', auth.authenticate, function(req, res, next){
  let role = req.body.roleNew;
  //db add new role and return success
  dbWorkRoles.addNewProjectRole(role).then(role=>{
    return res.json({success: true, id: role})
  })
  .catch((e)=>{
    return res.json({success:false, error:e});
  })
})
module.exports = router;