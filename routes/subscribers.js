var express = require('express');
var router = express.Router();
var Jimp = require('jimp');
const csp = require('helmet-csp');
//var dateFormat = require('dateformat');
//var fs = require('fs'); // --TEST--

//auth & dbModels
var auth = require('../controllers/authentication');
let dbSubscribers = require('../model/subscribers/dbSubscribers');
let dbChanges = require('../model/changes/dbChanges');

var normalCspHandler = csp({
  directives: {
    defaultSrc: ["'self'"],
    scriptSrc: ["'self'"],
    styleSrc: ["'self'", "https://fonts.googleapis.com", "https://use.fontawesome.com"],
    fontSrc: ["'self'", "https://fonts.gstatic.com", "https://use.fontawesome.com"],
    imgSrc: ["'self'", "data:"],
    frameAncestors: ["'self'"],
  }
});

var multer = require('multer');
//storage for images
var storage = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, 'public/uploads/images/')
  },
  filename: function (req, file, cb) {        
    // null as first argument means no error
    cb(null, Date.now() + '-' + file.originalname )
  }
});

//upload when editing subscriber image
var uploadEdit = multer({
  storage: storage,
  limits: {
    fileSize: 5000000
  },
  fileFilter: function(req, file, cb){
    sanitizeFileImage(file,cb);
  }
}).single('imageEdit');

//upload when adding subscriber image
var upload = multer({
  storage: storage,
  limits: {
    fileSize: 5000000
  },
  fileFilter: function(req, file, cb){
    sanitizeFileImage(file,cb);
  }
}).single('imageNew');
//here -----^ must be same name as name in html otherwise multer says error

//function to check if file is indeed image
function sanitizeFileImage(file, cb){
  //what file extentions are ok
  let fileExts = ['jpg', 'jpeg', 'png', 'gif'];

  //check alowed exts
  let isAlowedExt = fileExts.includes(file.originalname.split(".")[1].toLowerCase());
  //mime type must be an image
  let isAlowedMimeType = file.mimetype.startsWith("image/");

  if(isAlowedExt && isAlowedMimeType){
    //no errors
    return cb(null, true);
  }
  else{
    //error, not an image
    cb('Ta vrsta datoteke ni dovoljena!');
  }
}
//upload for updating subscriber
router.post('/fileUploadEdit', auth.authenticate, (req, res, next) => {
  let userId = req.session.user_id;
  //debugger;
  //save file and if no error send back json success true
  uploadEdit(req, res, (err) =>{
    //console.log("img upload");
    if(err){
      let msg = 'Napaka pri nalaganju datoteke na strežnik.';
      if(err.message == 'File too large')
        msg = 'Velikost datoteke je prevelika. Trenutno so dovoljene datoteke do 5MB.';
      let type = 1;
      return res.status(500).json({
        status: 'error',
        message: msg,
        type: type,
        error: err,
      })
    }
    else{
      //file not selected, create new subscriber with no image
      if(req.file == undefined){
        dbSubscribers.updateSubscriber(req.body.subscriberIdEdit, req.body.subscriberNameEdit)
        .then(subscriber => {
          dbChanges.addNewChangeSystem(2,8,userId,null,null,null,null,null,null,subscriber.id).then(sysChange => {
            console.log("Uporabnik " + userId + " je naredil spremembo tipa 8 s statusom 2 za naročnika " + subscriber.id + ".");
            let msg = 'Uspešna posodobitev naročnika brez slike.';
            return res.status(200).json({
              status: 'success',
              subscriber: subscriber,
              success: 'true',
              message: msg,
              type: 2,
            });
          })
          .catch((e)=>{
            return res.status(500).json({
              status: 'error',
              message: 'Napaka pri zapisovanju v sprememb za obstoječega naročnika.',
              error: e,
            })
          })
        })
        .catch((e)=>{
          return res.status(500).json({
            status: 'error',
            message: 'Napaka pri urejanju obstoječega naročnika.',
            error: e,
          })
        })
      }
      else{
        //success add update subscriber with new image name
        dbSubscribers.updateSubscriber(req.body.subscriberIdEdit, req.body.subscriberNameEdit, req.file.filename)
        .then(subscriber => {
          Jimp.read('public/uploads/images/'+req.file.filename)
          .then(lenna => {
            return lenna
              .resize(200, Jimp.AUTO) // resize
              .quality(60) // set JPEG quality
              .write('public/uploads/images/'+req.file.filename); // save
          })
          .catch((e)=>{
            return res.status(500).json({
              status: 'error',
              message: 'Napaka pri preoblikovanju slike v bolj primeren format za slike naročnikov.',
              error: e,
            })
          })
          dbChanges.addNewChangeSystem(2,8,userId,null,null,null,null,null,null,subscriber.id).then(sysChange => {
            console.log("Uporabnik " + userId + " je naredil spremembo tipa 8 s statusom 2 za naročnika " + subscriber.id + ".");
            let msg = 'Uspešna posodobitev naročnika s sliko.';
            return res.status(200).json({
              status: 'success',
              subscriber: subscriber,
              success: 'true',
              message: msg,
              type: 2,
            });
          })
          .catch((e)=>{
            return res.status(500).json({
              status: 'error',
              message: 'Napaka pri zapisovanju v sprememb za obstoječega naročnika.',
              error: e,
            })
          })
        })
        .catch((e)=>{
          return res.status(500).json({
            status: 'error',
            message: 'Napaka pri posodabljanju obstoječega naročnika.',
            error: e,
          })
        })
      }
    }
  })
});
//upload for new img
router.post('/fileUpload', auth.authenticate, (req, res, next) => {
  let userId = req.session.user_id;
  //debugger;
  //save file and if no error send back json success true
  upload(req, res, (err) =>{
    //console.log("img upload");
    if(err){
      //res.redirect('/subscribers');
      let msg = 'Napaka pri nalaganju datoteke na strežnik.';
      if(err.message == 'File too large')
        msg = 'Velikost datoteke je prevelika. Trenutno so dovoljene datoteke do 5MB.';
      let type = 1;
      return res.status(500).json({
        status: 'error',
        message: msg,
        type: type,
        error: err,
      })
    }
    else{
      //file not selected, create new subscriber with no image
      if(req.file == undefined){
        //var subscriberName = req.body.subscriberName;
        dbSubscribers.createSubscriber(req.body.subscriberName)
        .then(subscriber => {
          //save change to db
          dbChanges.addNewChangeSystem(1,8,userId,null,null,null,null,null,null,subscriber.id).then(sysChange => {
            console.log("Uporabnik " + userId + " je naredil spremembo tipa 8 s statusom 1 za naročnika " + subscriber.id + ".");
            //res.redirect('/subscribers');
            let msg = 'Uspešno dodajanje novega naročnika brez slike.';
            return res.status(200).json({
              status: 'success',
              subscriber: subscriber,
              success: 'true',
              message: msg,
              type: 2,
            });
          })
          .catch((e)=>{
            return res.status(500).json({
              status: 'error',
              message: 'Napaka pri zapisovanju v sprememb za novega naročnika.',
              error: e,
            })
          })
        })
        .catch((e)=>{
          return res.status(500).json({
            status: 'error',
            message: 'Napaka pri ustvarjanju novega naročnika.',
            error: e,
          })
        })
      }
      else{
        //success add new subscriber with image name
        //var subscriberName = req.body.subscriberName;
        //var imageName = req.file.filename;
        dbSubscribers.createSubscriber(req.body.subscriberName, req.file.filename)
        .then(subscriber => {
          Jimp.read('public/uploads/images/'+req.file.filename)
          .then(lenna => {
            return lenna
              .resize(200, Jimp.AUTO) // resize
              .quality(60) // set JPEG quality
              .write('public/uploads/images/'+req.file.filename); // save
          })
          .catch((e)=>{
            return res.status(500).json({
              status: 'error',
              message: 'Napaka pri preoblikovanju slike v bolj primeren format za slike naročnikov.',
              error: e,
            })
          })
          dbChanges.addNewChangeSystem(1,8,userId,null,null,null,null,null,null,subscriber.id).then(sysChange => {
            console.log("Uporabnik " + userId + " je naredil spremembo tipa 8 s statusom 1 za naročnika " + subscriber.id + ".");
            //res.redirect('/subscribers');
            let msg = 'Uspešno dodajanje novega naročnika s sliko.';
            return res.status(200).json({
              status: 'success',
              subscriber: subscriber,
              success: 'true',
              message: msg,
              type: 2,
            });
          })
          .catch((e)=>{
            return res.status(500).json({
              status: 'error',
              message: 'Napaka pri zapisovanju v sprememb za novega naročnika.',
              error: e,
            })
          })
        })
        .catch((e)=>{
          return res.status(500).json({
            status: 'error',
            message: 'Napaka pri ustvarjanju novega naročnika.',
            error: e,
          })
        })
      }
    }
  })
});
//get all subscribers (for select2; (id, text))
router.get('/all', auth.authenticate, function(req, res, next){
  dbSubscribers.getSubscribers().then(subscribers=>{
    res.json({success:true, data:subscribers});
  })
  .catch((e)=>{
    return res.json({success:false, error:e});
  })
})
//get all subscribers (for table, with all info)
router.get('/allinfo', auth.authenticate, function(req, res, next){
  dbSubscribers.getAllSubscribers().then(subscribers=>{
    res.json({success:true, data:subscribers});
  })
  .catch((e)=>{
    return res.json({success:false, error:e});
  })
})

//create subscriber
router.post('/create', auth.authenticate, function(req, res, next){
  //console.log(req.body);
  //console.log(projectId);
  
  dbSubscribers.createSubscriberNoIcon(req.body.subscriber).then(subscriber=>{
    //console.log(subscriber);
    res.json({success:true, id:subscriber});
  })
  .catch((e)=>{
    return res.json({success:false, error:e});
  })
})

//get page with all subscribers
router.get('/', normalCspHandler, auth.authenticate, function(req, res){
  res.render('subscribers', {
    title: "Naročniki",
    user_name: req.session.user_name,
    user_surname: req.session.user_surname,
    user_username: req.session.user_username,
    user_typeid: req.session.role_id,
    user_type: req.session.type,
    user_id: req.session.user_id,
    msg: '',
    type: 0
  })
})
module.exports = router;