var express = require('express');
var router = express.Router();
var dateFormat = require('dateformat');
const csp = require('helmet-csp');

//auth & dbModels
var auth = require('../controllers/authentication');
var dbEmployees = require('../model/employees/dbEmployees');
var dbAbsences = require('../model/absences/dbAbsences');

var normalCspHandler = csp({
  directives: {
    defaultSrc: ["'self'"],
    scriptSrc: ["'self'"],
    styleSrc: ["'self'", "https://fonts.googleapis.com", "https://use.fontawesome.com"],
    fontSrc: ["'self'", "https://fonts.gstatic.com", "https://use.fontawesome.com"],
    imgSrc: ["'self'", "data:"],
    frameAncestors: ["'self'"],
  }
});

//functions
function formatDate(date){
  return {
    "date": dateFormat(date, "dd.mm.yyyy"),
    "time": dateFormat(date, "HH:MM")
  }
}
//POGLED STRAN VSEH ODSOTNOSTI
router.get('/', normalCspHandler, auth.authenticate, function(req, res, next){
  res.render('absences', {
    title: "Odsotnosti",
    user_name: req.session.user_name,
    user_username: req.session.user_username,
    user_surname: req.session.user_surname,
    user_typeid: req.session.role_id,
    user_type: req.session.type,
    user_id: req.session.user_id,
  })
})
//get all info (reasons, status, users)
router.get('/info', auth.authenticate, function(req, res, next){
  let promises = [];
  promises.push(dbEmployees.getUsers(1));
  promises.push(dbAbsences.getReasons());

  Promise.all(promises).then(data=>{
    data[0] = data[0].map(u => {return {id:u.id, name:u.name, surname:u.surname, role:u.role, text: u.name + ' ' + u.surname}})
    data[1] = data[1].map(r => {return {id:r.id, reason:r.text, text: r.text}})
    res.json({success: true, users: data[0], reasons: data[1]});
  })
  .catch((e)=>{
    return res.json({success:false, error:e});
  })
})

//get all absences
router.get('/all', auth.authenticate, function(req, res, next){
  let test = 'test';
  let absencesActivity = (req.query.absencesActivity) ? req.query.absencesActivity : null;
  let absencesLimit = (req.query.absencesLimit) ? req.query.absencesLimit : null;
  let absencesUserId = (req.query.absencesUserId) ? req.query.absencesUserId : null;
  let absencesUserGroups = (req.query.absencesUserGroups) ? req.query.absencesUserGroups : null;
  let absencesStatus = (req.query.absencesStatus) ? req.query.absencesStatus : null;
  let absencesDate = (req.query.absencesDate) ? req.query.absencesDate : null;

  let roleType = req.session.type.toLowerCase();
  if (roleType == 'admin' || roleType == 'tajnik' || roleType == 'komercialist' || roleType == 'komerciala' || roleType == 'računovodstvo' || roleType.includes('vodja'))
    absencesUserId = absencesUserId;
  else
    absencesUserId = req.session.user_id;

  dbAbsences.getAbsences(absencesUserId, absencesDate, absencesUserGroups, absencesStatus, absencesActivity, absencesLimit).then(absences => {

    res.json({success: true, absences});
  })
  .catch((e)=>{
    return res.json({success:false, error:e});
  })
})

// post new absence
router.post('/', auth.authenticate, function(req, res, next){
  let absenceName = req.body.absenceName;
  let absenceReason = req.body.absenceReason;
  let absenceUser = req.body.absenceUser;
  let absenceStart = req.body.absenceStart ? req.body.absenceStart + ' 07:00:00' : null;
  let absenceFinish = req.body.absenceFinish ? req.body.absenceFinish + ' 15:00:00' : null;
  let absenceUserRole = req.body.absenceUserRole ? req.body.absenceUserRole.toLowerCase() : req.body.absenceUserRole;
  let absenceStatus = req.body.absenceStatus;

  if (!absenceUser){
    // no user id -> only possible if regular user with no special role is adding his own absence (asking for approval)
    absenceUser = req.session.user_id;
  }
  else{
    // only special role can have other user id because user is adding absence for user who belongs in his user group role
    // correct was is to check if both user belong to same group role, example, electric leader can add absence for electrican and not for cnc operater
    // -> so the post req needs to send role of the user as well so it can be checked if this user is allowed to add
    // if no role then reject adding absence
    let roleType = req.session.type.toLowerCase();
    if (!absenceUserRole){
      return res.json({success:false, msg:'Nimate pravice dodajati odsotnosti za zaposlene v drugem oddelku (ni oddelka).'});
    }
    else if (!(roleType == 'vodja električarjev' && absenceUserRole.includes('elektri'))
            && !(roleType == 'vodja programerjev' && absenceUserRole.includes('programer'))
            && !(roleType == 'vodja konstrukterjev' && absenceUserRole.includes('konstrukt'))
            && !(roleType == 'vodja strojnikov' && (absenceUserRole.includes('strojni') || absenceUserRole.includes('cnc') || absenceUserRole.includes('varilec')))
            && (roleType != 'admin')
            && (roleType != 'tajnik')
            && (roleType != 'komercialist')
            && (roleType != 'komerciala')
            && (roleType != 'računovodstvo')
            && (absenceUserRole != 'študent')){
      return res.json({success:false, msg:'Nimate pravice dodajati odsotnosti za zaposlene v drugem oddelku.'});
    }
  }
  
  if(!absenceReason || !absenceStart || !absenceFinish){
    return res.json({success:false, msg:'Manjkajoči podatki. Ni razloga odsotnosti oz. začetka oz. konca.'});
  }
  //just for testing now -> POST req is OK, return success is true
  dbAbsences.addNewAbsence(absenceStart, absenceFinish, absenceReason, absenceUser, absenceName, absenceStatus).then(absence => {
    return res.json({success:true, absence});
  })
  .catch(error => {
    return res.json({success:false, error});
  })
  //return res.json({success:true});
})
//update absence
router.put('/', auth.authenticate, function(req, res, next){
  let absenceId = req.body.absenceId;
  let name = req.body.absenceName;
  let start = req.body.absenceStart + ' 07:00:00';
  let finish = req.body.absenceFinish + ' 15:00:00';;
  let reasonId = req.body.absenceReason;
  let status = req.body.absenceStatus;

  
  // return res.json({success:false});
  dbAbsences.getAbsenceById(absenceId).then(absence => {
    //console.log('User wants to edit:');
    //console.log(absence);
    let roleType = req.session.type.toLowerCase();
    let absenceRole = absence.role.toLowerCase();
    if (status != null){
      // user is approving or rejecting absence -> check if user can accept/reject based on his role and absence user role
      // admin and tajnik can approve/reject everything, project leaders can accept/reject their own, category leader can accept/reject absences inside his group
      if ((!(roleType == 'vodja električarjev' && absenceRole.includes('elektri'))
          && !(roleType == 'vodja programerjev' && absenceRole.includes('programer'))
          && !(roleType == 'vodja konstrukterjev' && absenceRole.includes('konstrukt'))
          && !(roleType == 'vodja strojnikov' && (absenceRole.includes('strojni') || absenceRole.includes('cnc') || absenceRole.includes('varilec')))
          && (roleType != 'admin')
          && (roleType != 'tajnik')
          && (roleType != 'komercialist')
          && (roleType != 'komerciala')
          && (roleType != 'računovodstvo')
          && (absenceRole != 'študent'))
          && !(absence.id_user == req.session.user_id && roleType.substring(0,5) == 'vodja')) {
        return res.json({success:false, msg:'Nimate pravice potjevati odsotnosti za zaposlene v drugem oddelku.'});
      }
      // return res.json({success:false});
      // everthing ok -> make absence update
      dbAbsences.approveAbsence(absenceId, status).then(absence => {
        return res.json({success:true, absence});
      })
      .catch(error => {
        return res.json({success:false, error:e});
      })  
    }
    else{
      if (!reasonId || !start || !finish) {
        return res.json({success:false, msg:'Manjkajoči podatki. Ni razloga odsotnosti oz. začetka oz. konca.'});
      }
      // regular absence update
      // admin & tajnik can edit all absences, project leader his own, group leader his group, regular user his own only if approve status is null
      if ((!(roleType == 'vodja električarjev' && absenceRole.includes('elektri'))
          && !(roleType == 'vodja programerjev' && absenceRole.includes('programer'))
          && !(roleType == 'vodja konstrukterjev' && absenceRole.includes('konstrukt'))
          && !(roleType == 'vodja strojnikov' && (absenceRole.includes('strojni') || absenceRole.includes('cnc') || absenceRole.includes('varilec')))
          && (roleType != 'admin')
          && (roleType != 'tajnik')
          && (roleType != 'komercialist')
          && (roleType != 'komerciala')
          && (roleType != 'računovodstvo')
          && (absenceRole != 'študent'))
          && !(absence.id_user == req.session.user_id && absence.approved == null)
          && !(absence.id_user == req.session.user_id && roleType.substring(0,5) == 'vodja')) {
        return res.json({success:false, msg:'Nimate pravice urejati odsotnosti za zaposlene v drugem oddelku.'});
      }
      // everthing ok -> make absence update
      dbAbsences.updateAbsence(absenceId, start, finish, reasonId, name).then(absence => {
        return res.json({success:true, absence});
      })
      .catch(error => {
        return res.json({success:false, error:e});
      })
      // return res.json({success:false});
    }
  })
  .catch(error => {
    return res.json({success:false, error, msg:'Ni bilo možno pridobiti informacije o odsotnosti iz podatkovne baze za preverjanje pravilnosti. Osvežite stran in ob ponovni napaki obvestite administratorje'});  
  })
  //return res.json({success:false});
  // admin, tajnik and vodja can create new reason
  // if(isNaN(req.body.reason)){
  //   //new reason
  //   dbAbsences.createReason(req.body.reason).then(reason=>{
  //     dbAbsences.updateAbsence(req.body.id, req.body.start, req.body.finish, reason.id, req.body.name)
  //     .then(absence=>{
  //       //update success
  //       return res.json({success:true, reasonId:reason.id});
  //     })
  //     .catch((e)=>{
  //       return res.json({success:false, error:e});
  //     })
  //   })
  //   .catch((e)=>{
  //     return res.json({success:false, error:e});
  //   })
  // }
  // else{
  //   //reason exist, update absence
  //   dbAbsences.updateAbsence(req.body.id, req.body.start, req.body.finish, req.body.reason, req.body.name)
  //   .then(absence=>{
  //     //update success
  //     return res.json({success:true, reasonId:null});
  //   })
  //   .catch((e)=>{
  //     return res.json({success:false, error:e});
  //   })
  // }
})
//delete absence
router.delete('/', auth.authenticate, function(req, res, next){
  let id = req.body.absenceId;
  let active = req.body.active;
  // return res.json({success:false});
  dbAbsences.deleteAbsence(id, active)
  .then(absence=>{
    return res.json({success:true, absence});
  })
  .catch((e)=>{
    return res.json({success:false, error:e});
  })
})
// OLD FUNCTIONS
//get all daily absences
router.get('/date', auth.authenticate, function(req, res, next){
  let date = req.query.date;
  dbAbsences.getDailyAbsences(date)
  .then(absences =>{
    return res.json({success:true, data:absences})
  })
  .catch((e)=>{
    return res.json({success:false, error:e});
  })
})
module.exports = router;