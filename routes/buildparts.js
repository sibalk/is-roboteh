var express = require('express');
var router = express.Router();
var dateFormat = require('dateformat');
var multer = require('multer');

//auth & dbModels
var auth = require('../controllers/authentication');
let dbBuildParts = require('../model/buildparts/dbBuildParts');
let dbBuildPhases = require('../model/buildparts/dbBuildPhases');
let dbPriorities = require('../model/priorities/dbPriorities');
let dbChanges = require('../model/changes/dbChanges');

//get all build parts for task id ---> pridobi kosovnico (vezano na opravilo)
router.get('/get', auth.authenticate, function(req, res, next){
  let taskId = req.query.taskId;
  dbBuildParts.getTaskBuildPartsFixed(taskId).then(buildParts=>{
    res.json({success:true, buildParts:buildParts});
  })
  .catch((e)=>{
    res.json({success:false, error:e});
  })
})
//get all build parts for task id in array ---> pridobi kosovnice vseh id-jev opravil
router.get('/multiple', auth.authenticate, function(req, res, next){
  let tasksForBPDoc = req.query.tasksForBPDoc ? req.query.tasksForBPDoc : [];
  let buildPartsForBPDoc = req.query.buildPartsForBPDoc ? req.query.buildPartsForBPDoc : [];
  let promises = [];
  for (let i = 0; i < tasksForBPDoc.length; i++) {
    const id = tasksForBPDoc[i];
    promises.push(dbBuildParts.getTaskBuildPartsFixed(id));
  }
  for (const bpa of buildPartsForBPDoc) {
    //console.log('need to get build parts: ' + bpa.buildParts);
    for (const bp of bpa.buildParts) {
      //console.log('need to get build part: ' + bp);
      promises.push(dbBuildParts.getBuildPart(bp));
    }
  }
  Promise.all(promises).then(buildParts =>{
    return res.json({success:true, buildParts});
  })
  .catch(e => {
    res.json({success:false, error:e});
  })
  // dbBuildParts.getTaskBuildPartsFixed(taskId).then(buildParts=>{
  // })
  // .catch((e)=>{
  //   res.json({success:false, error:e});
  // })
})
//get all build parts for selected project ---> pridobi vse kose za izbrani projekt (za filtriranje kosov ki so že v izdelavi)
router.get('/project', auth.authenticate, function(req, res, next){
  let projectId = req.query.projectId;
  dbBuildParts.getAllActiveProjectBuildParts(projectId).then(allBuildParts=>{
    res.json({success:true, allBuildParts});
  })
  .catch((e)=>{
    res.json({success:false, error:e});
  })
})
//manual add new build part
router.post('/add', auth.authenticate, function(req, res){
  let taskId = req.body.taskId;
  let name = req.body.name;
  let standard = req.body.standard;
  let position = req.body.position;
  let quantity = req.body.quantity;
  let material = req.body.material ? req.body.material : null;
  let note = req.body.note;
  let tag = req.body.tag;
  let sestav = req.body.sestav;

  dbBuildParts.addNewBuildPartFixed(taskId,name,standard,position,quantity,material,note,tag,null,sestav).then(buildPart =>{
    res.json({success:true, buildPart:buildPart});
  })
  .catch((e)=>{
    res.json({success:false, error:e});
  })
})
//edit build part
router.post('/edit', auth.authenticate, function(req, res, next){
  let buildPartId = req.body.buildPartId;
  let name = req.body.name;
  let standard = req.body.standard;
  let position = req.body.position;
  let quantity = req.body.quantity;
  let material = req.body.material;
  let note = req.body.note;
  let tag = req.body.tag;

  dbBuildParts.editBuildPartFixed(buildPartId,name,standard,position,quantity,material,note,tag).then(buildPart =>{
    res.json({success:true, buildPart:buildPart, materialId:null, standardId:null});
  })
  .catch((e)=>{
    res.json({success:false, error:e});
  })
})
//Delete build part by id
router.post('/delete', auth.authenticate, function(req, res, next){
  let buildPartId = req.body.id;
  let active = req.body.active;
  dbBuildParts.deleteBuildPart(buildPartId, active).then(t=>{
    return res.json({success:true});
  })
  .catch((e)=>{
    res.json({success:false, error:e});
  })
})
//get all standards//NOT IN USE ANYMORE
router.get('/standards', auth.authenticate, function(req, res, next){
  dbBuildParts.getStandards().then(standards=>{
    dbBuildParts.getMaterials().then(materials=>{
      res.json({success:true, standards:standards, materials:materials});
    })
    .catch((e)=>{
      res.json({success:false, error:e});
    })
  })
  .catch((e)=>{
    res.json({success:false, error:e});
  })
})
//get all materials//NOT IN USE ANYMORE
router.get('/materials', auth.authenticate, function(req, res, next){
  dbBuildParts.getMaterials().then(materials=>{
    res.json({success:true, materials:materials});
  })
  .catch((e)=>{
    res.json({success:false, error:e});
  })
})
//post csv data for new build parts FIXED
router.post('/csv', auth.authenticate, function(req, res, next){
  let userId = req.session.user_id;
  let taskId = req.body.taskId;
  let newBuildParts = req.body.buildParts;
  let promisesBuildParts = [];
  let promisesFixBuildParts = [];
  let parentName = req.body.parentName;
  let parentStandard = req.body.parentStandard;

  dbBuildParts.findExistingChild(parentStandard, taskId).then(existingBuildPart => {
    if(existingBuildPart){
      //build part already exist as child, fix his sestav to true and then add new build parts with parent id from existing build part id
      dbBuildParts.promoteBuildPartToSestav(existingBuildPart.id).then(parentBuildPart => {
        let promiseFindParents = [];
        for(let i = 0; i < newBuildParts.length; i++){
          promiseFindParents.push(dbBuildParts.findExistingParent(newBuildParts[i].standard, taskId));
        }
        Promise.all(promiseFindParents).then(results => {
          let existingBuildPartsParents = [];
          for (const bp of results) {
            if(bp)
              existingBuildPartsParents.push(bp)
          }
          for(let i = 0; i < existingBuildPartsParents.length; i++){
            let newName = newBuildParts.find(nbp => nbp.standard == existingBuildPartsParents[i].standard)
            if(newName) newName = newName.name;
            promisesBuildParts.push(dbBuildParts.linkChildToParent(existingBuildPartsParents[i].id, parentBuildPart.id, newName));
          }
          for(let i = 0; i < newBuildParts.length; i++){
            let tmpParent = existingBuildPartsParents.find(bp => bp.standard == newBuildParts[i].standard);
            if(!tmpParent)
              promisesBuildParts.push(dbBuildParts.addNewBuildPartFixed(taskId,newBuildParts[i].name,newBuildParts[i].standard,newBuildParts[i].position,newBuildParts[i].quantity,newBuildParts[i].material,newBuildParts[i].note,newBuildParts[i].tag,parentBuildPart.id));
          }
          //in promisesFixBuildParts are all build parts that alredy exist and need to be link with new parent id, in promisesBuildParts are all build parts that doesnt exist yet
          // in short, promisesFixBuildParts will have changes with status 2, promisesBuildParts will have changes with status 1
          // no need for promisesFixBuildParts because it can be based on sestav -> if sestav true then 2 else 1
          Promise.all(promisesBuildParts).then(buildParts => {
            //add system changes for new build parts and for fixed build parts
            let promises = [];
            promises.push(dbChanges.addNewChangeSystem(2,22,userId,null,taskId,null,null,null,null,null,null,null,null,parentBuildPart.id));//new PARENT build part
            for(var i = 0; i < buildParts.length; i++){
              if(buildParts[i].sestav)
                promises.push(dbChanges.addNewChangeSystem(2,22,userId,null,taskId,null,null,null,null,null,null,null,null,buildParts[i].id));
              else
                promises.push(dbChanges.addNewChangeSystem(1,22,userId,null,taskId,null,null,null,null,null,null,null,null,buildParts[i].id));
            }
            Promise.all(promises)
            .then((results) => {
              //get fixed build parts
              dbBuildParts.getTaskBuildPartsFixed(taskId).then(fixedBuildParts => {
                return res.json({success:true, buildParts: fixedBuildParts});
              })
              .catch((e)=>{
                return res.json({success:false, error:e});
              })
            })
            .catch((e)=>{
              return res.json({success:false, error:e});
            })
          })
          .catch((e) => {
            return res.json({success:false, error: e});
          })
        })
        .catch((e) => {
          return res.json({success:false, error: e});
        })
      })
      .catch((e) => {
        return res.json({success:false, error: e});
      })
    }
    else{
      //new build part parent, same as before
      dbBuildParts.addNewBuildPartFixed(taskId,parentName,parentStandard,0,'-','-','SESTAV','',null,'true').then(parentBuildPart => {
        let promiseFindParents = [];
        for(let i = 0; i < newBuildParts.length; i++){
          promiseFindParents.push(dbBuildParts.findExistingParent(newBuildParts[i].standard, taskId));
        }
        Promise.all(promiseFindParents).then(results => {
          let existingBuildPartsParents = [];
          for (const bp of results) {
            if(bp)
              existingBuildPartsParents.push(bp)
          }
          for(let i = 0; i < existingBuildPartsParents.length; i++){
            let newName = newBuildParts.find(nbp => nbp.standard == existingBuildPartsParents[i].standard);
            let partNote = '';
            if(newName){
              partNote = newName.note;
              newName = newName.name;
            }
            promisesBuildParts.push(dbBuildParts.linkChildToParent(existingBuildPartsParents[i].id, parentBuildPart.id, newName, partNote));
          }
          for(let i = 0; i < newBuildParts.length; i++){
            let tmpParent = existingBuildPartsParents.find(bp => bp.standard == newBuildParts[i].standard);
            if(!tmpParent)
              promisesBuildParts.push(dbBuildParts.addNewBuildPartFixed(taskId,newBuildParts[i].name,newBuildParts[i].standard,newBuildParts[i].position,newBuildParts[i].quantity,newBuildParts[i].material,newBuildParts[i].note,newBuildParts[i].tag,parentBuildPart.id));
          }
          //in promisesFixBuildParts are all build parts that alredy exist and need to be link with new parent id, in promisesBuildParts are all build parts that doesnt exist yet
          // in short, promisesFixBuildParts will have changes with status 2, promisesBuildParts will have changes with status 1
          // no need for promisesFixBuildParts because it can be based on sestav -> if sestav true then 2 else 1
          Promise.all(promisesBuildParts).then(buildParts => {
            //add system changes for new build parts and for fixed build parts
            let promises = [];
            promises.push(dbChanges.addNewChangeSystem(1,22,userId,null,taskId,null,null,null,null,null,null,null,null,parentBuildPart.id));//new PARENT build part
            for(var i = 0; i < buildParts.length; i++){
              if(buildParts[i].sestav)
                promises.push(dbChanges.addNewChangeSystem(2,22,userId,null,taskId,null,null,null,null,null,null,null,null,buildParts[i].id));
              else
                promises.push(dbChanges.addNewChangeSystem(1,22,userId,null,taskId,null,null,null,null,null,null,null,null,buildParts[i].id));
            }
            Promise.all(promises)
            .then((results) => {
              //get fixed build parts
              dbBuildParts.getTaskBuildPartsFixed(taskId).then(fixedBuildParts => {
                return res.json({success:true, buildParts: fixedBuildParts});
              })
              .catch((e)=>{
                return res.json({success:false, error:e});
              })
            })
            .catch((e)=>{
              return res.json({success:false, error:e});
            })
          })
          .catch((e) => {
            return res.json({success:false, error: e});
          })
        })
        .catch((e) => {
          return res.json({success:false, error: e});
        })
      })
      .catch((e) => {
        return res.json({success:false, error: e});
      })
    }
  })
  .catch((e) => {
    return res.json({success:false, error: e});
  })  
})
///////////////////////////////////////////BUILD PHASES
//get all build phases for build part id ---> pridobi faze za izbran kos v kosovnici 
router.get('/buildphases/get', auth.authenticate, function(req, res, next){
  let buildPartId = req.query.buildPartId;
  dbBuildPhases.getBuildPartPhases(buildPartId).then(buildPhases=>{
    res.json({success:true, buildPhases:buildPhases});
  })
  .catch((e)=>{
    res.json({success:false, error:e});
  })
})
//add new build phase
router.post('/buildphases/add', auth.authenticate, function(req, res, next){
  let buildPartId = req.body.buildPartId;
  let type = req.body.type;
  let start = req.body.start;
  let finish = req.body.finish;
  let supplier = req.body.supplier;
  let workers = req.body.workers;
  let priority = req.body.priority;
  let promisesWorkers = [];
  workers = workers.split(',');
  if(isNaN(type) && isNaN(supplier)){
    //type and supplier are new
    dbBuildPhases.createNewPhaseType(type).then(newType =>{
      dbBuildPhases.createNewPhaseSupplier(supplier).then(newSupplier =>{
        dbBuildPhases.addNewBuildPhase(buildPartId,newType.id,start,finish,newSupplier.id,priority).then(buildPhase =>{
          //check if supplier is ROBOTEH --> connect workers(users) with new build phase
          if(supplier == 1){
            for(let i=0; i<workers.length; i++){
              promisesWorkers.push(dbBuildPhases.addWorker(buildPhase.id,workers[i]));
            }
            Promise.all(promisesWorkers)
            .then(buildPhaseWorkers => {
              res.json({success:true, buildPhase:buildPhase, supplierId:newSupplier.id, typeId:newType.id});
            })
            .catch((e)=>{
              res.json({success:false, error:e});
            })
          }
          else
            res.json({success:true, buildPhase:buildPhase, supplierId:newSupplier.id, typeId:newType.id});
        })
        .catch((e)=>{
          res.json({success:false, error:e});
        })
      })
      .catch((e)=>{
        res.json({success:false, error:e});
      })
    })
    .catch((e)=>{
      res.json({success:false, error:e});
    })
  }
  else if(isNaN(type)){
    //type is new
    dbBuildPhases.createNewPhaseType(type).then(newType =>{
      dbBuildPhases.addNewBuildPhase(buildPartId,newType.id,start,finish,supplier,priority).then(buildPhase =>{
        //check if supplier is ROBOTEH --> connect workers(users) with new build phase
        if(supplier == 1){
          for(let i=0; i<workers.length; i++){
            promisesWorkers.push(dbBuildPhases.addWorker(buildPhase.id,workers[i]));
          }
          Promise.all(promisesWorkers)
          .then(buildPhaseWorkers => {
            res.json({success:true, buildPhase:buildPhase, supplierId:null, typeId:newType.id});
          })
          .catch((e)=>{
            res.json({success:false, error:e});
          })
        }
        else
          res.json({success:true, buildPhase:buildPhase, supplierId:null, typeId:newType.id});
      })
      .catch((e)=>{
        res.json({success:false, error:e});
      })
    })
    .catch((e)=>{
      res.json({success:false, error:e});
    })
  }
  else if(isNaN(supplier)){
    //supplier is new
    dbBuildPhases.createNewPhaseSupplier(supplier).then(newSupplier =>{
      dbBuildPhases.addNewBuildPhase(buildPartId,type,start,finish,newSupplier.id,priority).then(buildPhase =>{
        //check if supplier is ROBOTEH --> connect workers(users) with new build phase
        if(supplier == 1){
          for(let i=0; i<workers.length; i++){
            promisesWorkers.push(dbBuildPhases.addWorker(buildPhase.id,workers[i]));
          }
          Promise.all(promisesWorkers)
          .then(buildPhaseWorkers => {
            res.json({success:true, buildPhase:buildPhase, supplierId:newSupplier.id, typeId:null});
          })
          .catch((e)=>{
            res.json({success:false, error:e});
          })
        }
        else
          res.json({success:true, buildPhase:buildPhase, supplierId:newSupplier.id, typeId:null});
      })
      .catch((e)=>{
        res.json({success:false, error:e});
      })
    })
    .catch((e)=>{
      res.json({success:false, error:e});
    })
  }
  else{
    //type and supplier exist
    dbBuildPhases.addNewBuildPhase(buildPartId,type,start,finish,supplier,priority).then(buildPhase =>{
      if(supplier == 1){
        for(let i=0; i<workers.length; i++){
          promisesWorkers.push(dbBuildPhases.addWorker(buildPhase.id,workers[i]));
        }
        Promise.all(promisesWorkers)
        .then(buildPhaseWorkers => {
          res.json({success:true, buildPhase:buildPhase, supplierId:null, typeId:null});
        })
        .catch((e)=>{
          res.json({success:false, error:e});
        })
      }
      else
        res.json({success:true, buildPhase:buildPhase, supplierId:null, typeId:null});
    })
    .catch((e)=>{
      res.json({success:false, error:e});
    })
  }
})
//add new build phase
router.post('/buildphases/update', auth.authenticate, function(req, res, next){
  let buildPhaseId = req.body.buildPhaseId;
  let type = req.body.type;
  let start = req.body.start;
  let finish = req.body.finish;
  let supplier = req.body.supplier;
  let workers = req.body.workers;
  let priority = req.body.priority;
  let finished = req.body.finished;
  let location = req.body.location;
  let promisesWorkers = [];
  workers = workers.split(',');
  dbBuildPhases.deleteWorkers(buildPhaseId).then(delWorkers=>{
    if(location && isNaN(location)){
      dbBuildPhases.createNewPhaseLocation(location).then(newLocation=>{
        if(isNaN(type) && isNaN(supplier)){
          //type and supplier are new
          dbBuildPhases.createNewPhaseType(type).then(newType =>{
            dbBuildPhases.createNewPhaseSupplier(supplier).then(newSupplier =>{
              dbBuildPhases.updateBuildPhase(buildPhaseId,newType.id,start,finish,newSupplier.id,priority,finished,newLocation.id).then(buildPhase =>{
                //check if supplier is ROBOTEH --> connect workers(users) with new build phase
                if(supplier == 1){
                  for(let i=0; i<workers.length; i++){
                    promisesWorkers.push(dbBuildPhases.addWorker(buildPhase.id,workers[i]));
                  }
                  Promise.all(promisesWorkers)
                  .then(buildPhaseWorkers => {
                    res.json({success:true, buildPhase:buildPhase, supplierId:newSupplier.id, typeId:newType.id, locationId:newLocation.id});
                  })
                  .catch((e)=>{
                    res.json({success:false, error:e});
                  })
                }
                else
                  res.json({success:true, buildPhase:buildPhase, supplierId:newSupplier.id, typeId:newType.id, locationId:newLocation.id});
              })
              .catch((e)=>{
                res.json({success:false, error:e});
              })
            })
            .catch((e)=>{
              res.json({success:false, error:e});
            })
          })
          .catch((e)=>{
            res.json({success:false, error:e});
          })
        }
        else if(isNaN(type)){
          //type is new
          dbBuildPhases.createNewPhaseType(type).then(newType =>{
            dbBuildPhases.updateBuildPhase(buildPhaseId,newType.id,start,finish,supplier,priority,finished,newLocation.id).then(buildPhase =>{
              //check if supplier is ROBOTEH --> connect workers(users) with new build phase
              if(supplier == 1){
                for(let i=0; i<workers.length; i++){
                  promisesWorkers.push(dbBuildPhases.addWorker(buildPhase.id,workers[i]));
                }
                Promise.all(promisesWorkers)
                .then(buildPhaseWorkers => {
                  res.json({success:true, buildPhase:buildPhase, supplierId:null, typeId:newType.id, locationId:newLocation.id});
                })
                .catch((e)=>{
                  res.json({success:false, error:e});
                })
              }
              else
                res.json({success:true, buildPhase:buildPhase, supplierId:null, typeId:newType.id, locationId:newLocation.id});
            })
            .catch((e)=>{
              res.json({success:false, error:e});
            })
          })
          .catch((e)=>{
            res.json({success:false, error:e});
          })
        }
        else if(isNaN(supplier)){
          //supplier is new
          dbBuildPhases.createNewPhaseSupplier(supplier).then(newSupplier =>{
            dbBuildPhases.updateBuildPhase(buildPhaseId,type,start,finish,newSupplier.id,priority,finished,newLocation.id).then(buildPhase =>{
              //check if supplier is ROBOTEH --> connect workers(users) with new build phase
              if(supplier == 1){
                for(let i=0; i<workers.length; i++){
                  promisesWorkers.push(dbBuildPhases.addWorker(buildPhase.id,workers[i]));
                }
                Promise.all(promisesWorkers)
                .then(buildPhaseWorkers => {
                  res.json({success:true, buildPhase:buildPhase, supplierId:newSupplier.id, typeId:null, locationId:newLocation.id});
                })
                .catch((e)=>{
                  res.json({success:false, error:e});
                })
              }
              else
                res.json({success:true, buildPhase:buildPhase, supplierId:newSupplier.id, typeId:null, locationId:newLocation.id});
            })
            .catch((e)=>{
              res.json({success:false, error:e});
            })
          })
          .catch((e)=>{
            res.json({success:false, error:e});
          })
        }
        else{
          //type and supplier exist
          dbBuildPhases.updateBuildPhase(buildPhaseId,type,start,finish,supplier,priority,finished,newLocation.id).then(buildPhase =>{
            if(supplier == 1){
              for(let i=0; i<workers.length; i++){
                promisesWorkers.push(dbBuildPhases.addWorker(buildPhase.id,workers[i]));
              }
              Promise.all(promisesWorkers)
              .then(buildPhaseWorkers => {
                res.json({success:true, buildPhase:buildPhase, supplierId:null, typeId:null, locationId:newLocation.id});
              })
              .catch((e)=>{
                res.json({success:false, error:e});
              })
            }
            else
              res.json({success:true, buildPhase:buildPhase, supplierId:null, typeId:null, locationId:newLocation.id});
          })
          .catch((e)=>{
            res.json({success:false, error:e});
          })
        }
      })
      .catch((e)=>{
        res.json({success:false, error:e});
      })
    }
    else{
      if(isNaN(type) && isNaN(supplier)){
        //type and supplier are new
        dbBuildPhases.createNewPhaseType(type).then(newType =>{
          dbBuildPhases.createNewPhaseSupplier(supplier).then(newSupplier =>{
            dbBuildPhases.updateBuildPhase(buildPhaseId,newType.id,start,finish,newSupplier.id,priority,finished,location).then(buildPhase =>{
              //check if supplier is ROBOTEH --> connect workers(users) with new build phase
              if(supplier == 1){
                for(let i=0; i<workers.length; i++){
                  promisesWorkers.push(dbBuildPhases.addWorker(buildPhase.id,workers[i]));
                }
                Promise.all(promisesWorkers)
                .then(buildPhaseWorkers => {
                  res.json({success:true, buildPhase:buildPhase, supplierId:newSupplier.id, typeId:newType.id, locationId:null});
                })
                .catch((e)=>{
                  res.json({success:false, error:e});
                })
              }
              else
                res.json({success:true, buildPhase:buildPhase, supplierId:newSupplier.id, typeId:newType.id, locationId:null});
            })
            .catch((e)=>{
              res.json({success:false, error:e});
            })
          })
          .catch((e)=>{
            res.json({success:false, error:e});
          })
        })
        .catch((e)=>{
          res.json({success:false, error:e});
        })
      }
      else if(isNaN(type)){
        //type is new
        dbBuildPhases.createNewPhaseType(type).then(newType =>{
          dbBuildPhases.updateBuildPhase(buildPhaseId,newType.id,start,finish,supplier,priority,finished,location).then(buildPhase =>{
            //check if supplier is ROBOTEH --> connect workers(users) with new build phase
            if(supplier == 1){
              for(let i=0; i<workers.length; i++){
                promisesWorkers.push(dbBuildPhases.addWorker(buildPhase.id,workers[i]));
              }
              Promise.all(promisesWorkers)
              .then(buildPhaseWorkers => {
                res.json({success:true, buildPhase:buildPhase, supplierId:null, typeId:newType.id, locationId:null});
              })
              .catch((e)=>{
                res.json({success:false, error:e});
              })
            }
            else
              res.json({success:true, buildPhase:buildPhase, supplierId:null, typeId:newType.id, locationId:null});
          })
          .catch((e)=>{
            res.json({success:false, error:e});
          })
        })
        .catch((e)=>{
          res.json({success:false, error:e});
        })
      }
      else if(isNaN(supplier)){
        //supplier is new
        dbBuildPhases.createNewPhaseSupplier(supplier).then(newSupplier =>{
          dbBuildPhases.updateBuildPhase(buildPhaseId,type,start,finish,newSupplier.id,priority,finished,location).then(buildPhase =>{
            //check if supplier is ROBOTEH --> connect workers(users) with new build phase
            if(supplier == 1){
              for(let i=0; i<workers.length; i++){
                promisesWorkers.push(dbBuildPhases.addWorker(buildPhase.id,workers[i]));
              }
              Promise.all(promisesWorkers)
              .then(buildPhaseWorkers => {
                res.json({success:true, buildPhase:buildPhase, supplierId:newSupplier.id, typeId:null, locationId:null});
              })
              .catch((e)=>{
                res.json({success:false, error:e});
              })
            }
            else
              res.json({success:true, buildPhase:buildPhase, supplierId:newSupplier.id, typeId:null, locationId:null});
          })
          .catch((e)=>{
            res.json({success:false, error:e});
          })
        })
        .catch((e)=>{
          res.json({success:false, error:e});
        })
      }
      else{
        //type and supplier exist
        dbBuildPhases.updateBuildPhase(buildPhaseId,type,start,finish,supplier,priority,finished,location).then(buildPhase =>{
          if(supplier == 1){
            for(let i=0; i<workers.length; i++){
              promisesWorkers.push(dbBuildPhases.addWorker(buildPhase.id,workers[i]));
            }
            Promise.all(promisesWorkers)
            .then(buildPhaseWorkers => {
              res.json({success:true, buildPhase:buildPhase, supplierId:null, typeId:null, locationId:null});
            })
            .catch((e)=>{
              res.json({success:false, error:e});
            })
          }
          else
            res.json({success:true, buildPhase:buildPhase, supplierId:null, typeId:null, locationId:null});
        })
        .catch((e)=>{
          res.json({success:false, error:e});
        })
      }
    }
  })
  .catch((e)=>{
    res.json({success:false, error:e});
  })
})
//Delete build phase by id
router.post('/buildphases/delete', auth.authenticate, function(req, res, next){
  let buildPhaseId = req.body.id;
  let active = req.body.active;
  dbBuildPhases.deleteBuildPhase(buildPhaseId, active).then(t=>{
    return res.json({success:true});
  })
  .catch((e)=>{
    res.json({success:false, error:e});
  })
})
//get build phase locations
router.get('/buildphases/locations', auth.authenticate, function(req, res, next){
  dbBuildPhases.getLocations().then(locations=>{
    res.json({success:true, locations:locations});
  })
  .catch((e)=>{
    res.json({success:false, error:e});
  })
})
//finish build phase with location and date
router.post('/buildphases/finish', auth.authenticate, function(req, res, next){
  let buildPhaseId = req.body.id;
  let location = req.body.location;
  if(isNaN(location)){
    dbBuildPhases.createNewPhaseLocation(location).then(newLocation=>{
      dbBuildPhases.updateBuildPhaseLocation(buildPhaseId,newLocation.id).then(bp=>{
        res.json({success:true, locationId:newLocation.id});
      })
      .catch((e)=>{
        res.json({success:false, error:e});
      })
    })
    .catch((e)=>{
      res.json({success:false, error:e});
    })
  }
  else{
    dbBuildPhases.updateBuildPhaseLocation(buildPhaseId,location).then(bp=>{
      res.json({success:true, locationId:null});
    })
    .catch((e)=>{
      res.json({success:false, error:e});
    })
  }
})
//undo approve action for selected build phase
router.post('/buildphases/unfinish', auth.authenticate, function(req, res, next){
  let buildPhaseId = req.body.id;
  dbBuildPhases.unfinishBuildPhase(buildPhaseId).then(bp=>{
    res.json({success:true});
  })
  .catch((e)=>{
    res.json({success:false, error:e});
  })
})
//get build phase locations
router.post('/buildphases/location', auth.authenticate, function(req, res, next){
  //let buildPartId = req.query.buildPartId;
  let buildPhaseId = req.body.id;
  let location = req.body.location;
  if(isNaN(location)){
    dbBuildPhases.createNewPhaseLocation(location).then(newLocation=>{
      dbBuildPhases.saveBuildPhaseLocation(buildPhaseId,newLocation.id).then(bp=>{
        res.json({success:true, locationId:newLocation.id});
      })
      .catch((e)=>{
        res.json({success:false, error:e});
      })
    })
    .catch((e)=>{
      res.json({success:false, error:e});
    })
  }
  else{
    dbBuildPhases.saveBuildPhaseLocation(buildPhaseId,location).then(bp=>{
      res.json({success:true, locationId:null});
    })
    .catch((e)=>{
      res.json({success:false, error:e});
    })
  }
})
//get phase suppliers, types, workers
router.get('/buildphases/phasedata', auth.authenticate, function(req, res, next){
  dbBuildPhases.getPhaseSuppliers().then(suppliers=>{
    dbBuildPhases.getPhaseTypes().then(types=>{
      dbBuildPhases.getPhaseWorkers().then(workers=>{
        dbPriorities.getPriorities().then(priorities=>{
          res.json({success:true, suppliers:suppliers, types:types, workers:workers, priorities:priorities});
        })
        .catch((e)=>{
          res.json({success:false, error:e});
        })
      })
      .catch((e)=>{
        res.json({success:false, error:e});
      })
    })
    .catch((e)=>{
      res.json({success:false, error:e});
    })
  })
  .catch((e)=>{
    res.json({success:false, error:e});
  })
})
//Save completion of build part
router.post('/completion', auth.authenticate, function(req, res, next){
  let completion = req.body.completion;
  let buildPartId = req.body.buildPartId;
  if(completion == "NaN")
    completion = 0;
  if(completion === true || completion === false){
    if(completion){
      dbBuildParts.updateBuildPartCompletion(buildPartId, 100)
      .then(bp=>{
        res.json({success:true});
      })
      .catch((e)=>{
        res.json({success:false, error:e});
      })
    }
    else{
      dbBuildParts.updateBuildPartCompletion(buildPartId, 0)
      .then(bp=>{
        res.json({success:true});
      })
      .catch((e)=>{
        res.json({success:false, error:e});
      })
    }
  }
  else{
    dbBuildParts.updateBuildPartCompletion(buildPartId, completion)
    .then(bp=>{
      res.json({success:true});
    })
    .catch((e)=>{
      res.json({success:false, error:e});
    })
  }
})

module.exports = router;