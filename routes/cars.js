var express = require('express');
var router = express.Router();
var dateFormat = require('dateformat');
const csp = require('helmet-csp');

//auth & dbModels
var auth = require('../controllers/authentication');
var dbCalendar = require('../model/calendar/dbCalendar');

var normalCspHandler = csp({
  directives: {
    defaultSrc: ["'self'"],
    scriptSrc: ["'self'"],
    styleSrc: ["'self'", "https://fonts.googleapis.com", "https://use.fontawesome.com"],
    fontSrc: ["'self'", "https://fonts.gstatic.com", "https://use.fontawesome.com"],
    imgSrc: ["'self'", "data:"],
    frameAncestors: ["'self'"],
  }
});

//cars resevation timeline
router.get('/', normalCspHandler, auth.authenticate, function(req, res, next){
  //console.log(req);
  res.render('cars_overview', {
    title: "Razpoložljivost vozil",
    user_name: req.session.user_name,
    user_surname: req.session.user_surname,
    user_username: req.session.user_username,
    user_typeid: req.session.role_id,
    user_type: req.session.type,
    user_id: req.session.user_id,
  })
})

module.exports = router;