var express = require('express');
var router = express.Router();
let nodemailer = require('nodemailer');
var dateFormat = require('dateformat');
var multer = require('multer');
const fs = require('fs');
const fsp = require('fs').promises;
var Jimp = require('jimp');
var path = require('path');

//auth & dbModels
var auth = require('../controllers/authentication');
//var dbFiles = require('../model/files/dbFiles');
var dbReports = require('../model/reports/dbReports');
let dbWorkOrders = require('../model/workOrders/dbWorkOrders');
let dbWorkOrdersAPI = require('../model/apiv2/workorders/dbWorkOrdersAPI');
let dbChanges = require('../model/changes/dbChanges');
let dbBase64Images = require('../model/base64images/base64images');
var routerSigns = require('./workordersign');
// //other constants (from file)
// let dbBase64Images = require('../model/base64images/base64images');

// // let robotehImg = biggerConsts.getRobotehImage();
// let robotehImg = 'test';

router.use('/signs', routerSigns);

// Define font files
var fonts = {
  Roboto: {
    normal: `${__dirname}/../public/pdfmake/Roboto-Regular.ttf`,
    bold: `${__dirname}/../public/pdfmake/Roboto-Medium.ttf`,
    italics: `${__dirname}/../public/pdfmake/Roboto-Italic.ttf`,
    bolditalics: `${__dirname}/../public/pdfmake/Roboto-MediumItalic.ttf`,
  }
};
var PdfPrinter = require('pdfmake');
var printer = new PdfPrinter(fonts);
//send mail
let transporter;
if(process.env.MAIL_HOST){
  transporter = nodemailer.createTransport({
    host: process.env.MAIL_HOST,
    port: process.env.MAIL_PORT,
    secure: true,
    auth: {
      user: process.env.MAIL_USERNAME,
      pass: process.env.MAIL_PASSWORD
    },
    tls: {
      // do not fail on invalid certs
      rejectUnauthorized: false
    }
  });
}
if(process.env.ETHEREAL_HOST){
  transporter = nodemailer.createTransport({
    host: process.env.ETHEREAL_HOST,
    port: process.env.ETHEREAL_PORT,
    auth: {
      user: process.env.ETHEREAL_USERNAME,
      pass: process.env.ETHEREAL_PASSWORD
    },
  });
}

//functions
function formatDate(date){
  return {
    "date": dateFormat(date, "dd.mm.yyyy"),
    "time": dateFormat(date, "HH:MM")
  }
}
function formatTime(time){
	let tmp = time.split(':')
  return {
    "hours": tmp[0],
    "minutes": tmp[1]
  }
}
function addFormatedDateForWorkOrders(workOrders){
  //console.log("test");
  for(let i=0; i < workOrders.length; i++) {
    if(workOrders[i] && workOrders[i].date)
      workOrders[i]["formatted_date"] = formatDate(workOrders[i].date);
  }
  return workOrders;
}
function addFormatedTimeForWorkOrders(workOrders){
  //console.log("test");
  for(let i=0; i < workOrders.length; i++) {
    if(workOrders[i] && workOrders[i].arrival)
      workOrders[i]["formatted_arrival"] = formatTime(workOrders[i].arrival);
    if(workOrders[i] && workOrders[i].departure)
      workOrders[i]["formatted_departure"] = formatTime(workOrders[i].departure);
  }
  return workOrders;
}
//storage for documents
var storageDoc = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, 'files/workorders/')
  },
  filename: function (req, file, cb) {        
    // null as first argument means no error
    cb(null, Date.now() + '-' + file.originalname )
  }
});
//upload when adding subscriber image
var uploadDoc = multer({
  storage: storageDoc,
  limits: {
    fileSize: 5300000
  },
  fileFilter: function(req, file, cb){
    sanitizeFileDocument(file,cb);
  }
}).single('docNew');
//function to check if file is indeed document
function sanitizeFileDocument(file, cb){
  //what file extentions are ok
  let fileExts = ['pdf', //pdf 
                'doc', 'dot', 'wbk', 'docx', 'docm', 'dotx', 'dotm', 'docb', //word
                'xls', 'xlt', 'xlm', 'xlsx', 'xlsm', 'xltx', 'xltm', 'xlsb', 'xla', 'xlam', 'xll', 'xlw', //excel
                'ppt', 'pot', 'pps', 'pptx', 'pptm', 'potx', 'potm', 'ppam', 'ppsx', 'ppsm', 'sldx', 'sldm', //powepoint
                'zip', 'rar', //zips
                'txt', //txt
                'jpg', 'jpeg', 'png', 'gif']; //images
                //access has way diffrent ext and they probably wont use them
                //other file ext will be added when there will be request
  // MAYBE TODO add isAlowedMimeType back and test for all this type of extentions
  //check if file has no exts
  let fileExtsArray = file.originalname.split(".");
  if(fileExtsArray.length == 1)
    return cb('Datoteka brez končnice ni dovoljena');
  //check alowed exts
  let isAlowedExt = fileExts.includes(fileExtsArray[fileExtsArray.length-1].toLowerCase());
  //mime type must be an image
  //let isAlowedMimeType = file.mimetype.startsWith("document/");

  if(isAlowedExt){
    //no errors
    return cb(null, true);
  }
  else{
    //error, not an image
    cb('Ta vrsta datoteke ni dovoljena!');
  }
}
//upload for new file
router.post('/fileUpload', auth.authenticate, (req, res, next) => {
  //debugger;
  //save file and if no error send back json success true
  uploadDoc(req, res, (err) =>{
    //console.log("img upload");
    if(err){
      let msg = 'Napaka pri nalaganju datoteke na strežnik.';
      if(err.message == 'File too large')
        msg = 'Velikost datoteke je prevelika. Trenutno so dovoljene datoteke do 5MB.';
      return res.status(500).json({
        status: 'error',
        message: msg,
        error: err,
      })
    }
    else{
      //file not selected
      if(req.file == undefined){
        return res.status(400).json({
          status: 'error',
          message: 'Niste priložili datoteko, ki jo želite dodati delovnemu nalogu.',
        })
      }
      else{
        let workOrderId = parseInt(req.body.workOrderId);
        let type = "";
        let fileExtsPDF = ['pdf'];
        let fileExtsDOC = ['doc', 'dot', 'wbk', 'docx', 'docm', 'dotx', 'dotm', 'docb'];
        let fileExtsXLS = ['xls', 'xlt', 'xlm', 'xlsx', 'xlsm', 'xltx', 'xltm', 'xlsb', 'xla', 'xlam', 'xll', 'xlw'];
        let fileExtsPPT = ['ppt', 'pot', 'pps', 'pptx', 'pptm', 'potx', 'potm', 'ppam', 'ppsx', 'ppsm', 'sldx', 'sldm'];
        let fileExtsIMG = ['jpg', 'jpeg', 'png', 'gif'];
        let fileExtsZIP = ['zip', 'rar'];
        let fileExtsTXT = ['txt'];

        let fileExtsArray = req.file.originalname.split(".");
        let isAlowedExtPDF = fileExtsPDF.includes(fileExtsArray[fileExtsArray.length - 1].toLocaleLowerCase());
        let isAlowedExtDOC = fileExtsDOC.includes(fileExtsArray[fileExtsArray.length - 1].toLowerCase());
        let isAlowedExtXLS = fileExtsXLS.includes(fileExtsArray[fileExtsArray.length - 1].toLowerCase());
        let isAlowedExtPPT = fileExtsPPT.includes(fileExtsArray[fileExtsArray.length - 1].toLowerCase());
        let isAlowedExtZIP = fileExtsZIP.includes(fileExtsArray[fileExtsArray.length - 1].toLowerCase());
        let isAlowedExtTXT = fileExtsTXT.includes(fileExtsArray[fileExtsArray.length - 1].toLowerCase());
        let isAlowedExtIMG = fileExtsIMG.includes(fileExtsArray[fileExtsArray.length - 1].toLocaleLowerCase());
        
        if(isAlowedExtPDF) type = 'PDF';
        else if(isAlowedExtDOC) type = 'DOC';
        else if(isAlowedExtPPT) type = 'PPT';
        else if(isAlowedExtXLS) type = 'XLS';
        else if(isAlowedExtIMG) type = 'IMG';
        else if(isAlowedExtZIP) type = 'ZIP';
        else if(isAlowedExtTXT) type = 'TXT';
        //success add new subscriber with image name
        //var subscriberName = req.body.subscriberName;
        //var imageName = req.file.filename;
        dbWorkOrders.addWorkOrderFile(req.file.originalname, req.file.filename, req.session.user_id, type)
        .then(newFile =>{
          let msg = "Uspešno dodajanje nove datoteke.";
          dbWorkOrders.addFile(workOrderId,newFile.id).then(keys => {
            //res.send('success')
            //console.log('success');
            let fileLocation = `${__dirname}/../files/workorders/${newFile.path_name}`;
            if (fs.existsSync(fileLocation)) {
              if (newFile.type == 'IMG'){
                Jimp.read(fileLocation).then(img => {
                  img.resize(150, Jimp.AUTO).quality(20).getBase64Async(Jimp.AUTO).then(base64 => {
                    //return res.json({success:true, data: base64, id: fileId, type: 'IMG'});
                    newFile.base64 = base64;
                    return res.status(200).json({
                      status: 'success',
                      file: newFile,
                      success: 'true',
                      message: msg,
                    });
                  })
                  .catch(err => {
                    return res.json({success:false, error: err});
                  });
                })
                .catch((e) => {
                  return res.json({success:false, error: e, msg: 'Datoteka ne obstaja.', id: fileId, type: fileType});
                })
              }
              else if (newFile.type == 'PDF'){
                fsp.readFile(fileLocation, 'base64').then(base64 => {
                  //console.log(data);
                  newFile.base64 = base64;
                  return res.status(200).json({
                    status: 'success',
                    file: newFile,
                    success: 'true',
                    message: msg,
                  });
                })
                .catch((e) => {
                  return res.json({success:false, error: e});
                })
              }
              else{
                //newFile.base64 = fs.readFileSync(fileLocation, 'base64');
                res.status(200).json({
                  status: 'success',
                  file:newFile,
                  success: 'true'
                });
              }
            }
            else{
              return res.status(200).json({
                status: 'file not found',
                file: newFile,
                success: 'false',
                message: msg,
                type: type,
              });
            }
          })
          .catch((e)=>{
            return res.status(500).json({
              status: 'error',
              message: 'Napaka pri zapisovanju podatkov katera datoteka pripada katerumu delovnemu nalogu.',
              error: e,
            })
          })
        })
        .catch((e)=>{
          return res.status(500).json({
            status: 'error',
            message: 'Napaka pri zapisovanju podatkov datoteke v tabelo z datotekami za delovne naloge.',
            error: e,
          })
        })
      }
    }
  })
});
//FILE DOWNLOAD
router.get('/sendMeDoc', auth.authenticate, function(req, res){
  //console.log('send him doc');
  //console.log(req.query.filename);
  let filename = req.query.filename;
  let projectId = req.query.projectId;
  console.log('Zahteva za datoteko: '+ filename);
  //var filePath = '/public/uploads/documents/1562059866128-Projekt Čisto novi projekt.pdf';
  var file = `${__dirname}/../files/workorders/${filename}`;
  //let imageAsBase64 = fs.readFileSync(file, 'base64');
  //console.log(imageAsBase64);
  if (fs.existsSync(file)) {
    //get file info from database to get correct name and MIME type
    dbWorkOrders.getWorkOrderFileInfo(filename).then(fileInfo => {
      switch (fileInfo.type) {
        case 'IMG': break;
        case 'PDF': res.type('application/pdf'); res.setHeader('Content-type', 'application/pdf'); res.set({"Content-Type": "application/pdf",}); break;
        case 'DOC': res.type('application/msword'); res.setHeader('Content-type', 'application/msword'); res.set({"Content-Type": "application/msword",}); break;
        case 'PPT': res.type('application/mspowerpoint'); res.setHeader('Content-type', 'application/mspowerpoint'); res.set({"Content-Type": "application/mspowerpoint",}); break;
        case 'ZIP': res.type('application/x-compressed'); res.setHeader('Content-type', 'application/x-compressed'); res.set({"Content-Type": "application/x-compressed",}); break;
        case 'TXT': res.type('text/plain'); res.setHeader('Content-type', 'text/plain'); res.set({"Content-Type": "text/plain",}); break;
        case 'XLS': res.type('application/excel'); res.setHeader('Content-type', 'application/excel'); res.set({"Content-Type": "application/excel",}); break;
        default: break;
      }
      //let fileExtsIMG = ['jpg', 'jpeg', 'png', 'gif'];
      if (fileInfo.type == 'IMG'){
        let tmp = fileInfo.original_name.split('.');
        if (tmp[tmp.length-1].toLocaleLowerCase() == 'png') {res.type('image/png'); res.setHeader('Content-type', 'image/png'); res.set({"Content-Type": "image/png",});}
        else if (tmp[tmp.length-1].toLocaleLowerCase() == 'jpg' || tmp[tmp.length-1] == 'jpeg') {res.type('image/jpeg'); res.setHeader('Content-type', 'image/jpeg'); res.set({"Content-Type": "image/jpeg",});}
        else if (tmp[tmp.length-1].toLocaleLowerCase() == 'gif') {res.type('image/gif'); res.setHeader('Content-type', 'image/gif'); res.set({"Content-Type": "image/gif",});}
      }
      // Do something
      res.download(file, fileInfo.original_name, function(err){
        if(err){
          console.error(err);
        }
      });
    })
    .catch(e => {
      let msg = "Napaka: Datoteka ne obstaja v bazi!";
      let type = 1;
      return res.status(500).json({
        status: 'error',
        message: msg,
        type
      })
    })
  }
  else{
    dbWorkOrders.updateActiveFile(filename)
      .then(file=>{
        console.log("Datoteka "+filename+" ne obstaja več, aktivnost v bazi popravljena.")
        let msg = "Napaka: Datoteka ne obstaja več!";
        return res.status(500).json({
          status: 'error',
          message: msg,
        })
      })
      .catch((e)=>{
        return res.status(500).json({
          status: 'error',
          message: 'Something went wrong',
          error: e,
        })
      })
  }
  
  //return
  //file exists
})
//FILE DOWNLOAD
router.get('/sendMeSign', auth.authenticate, function(req, res){
  //console.log('send him doc');
  //console.log(req.query.filename);
  let filename = req.query.filename;
  let projectId = req.query.projectId;
  console.log('Zahteva za podpis: '+ filename);
  //var filePath = '/public/uploads/documents/1562059866128-Projekt Čisto novi projekt.pdf';
  var file = `${__dirname}/../files/workorders/signs/${filename}`;
  if (fs.existsSync(file)) {
    // Do something
    let imageAsBase64 = fs.readFileSync(file, 'base64');
    //console.log(imageAsBase64);
    return res.json({success: true, sign: imageAsBase64});  
    res.download(file, function(err){
      if(err){
        console.error(err);
      }
    });
  }
  else{
    dbWorkOrders.updateActiveFile(filename)
      .then(file=>{
        console.log("Datoteka "+filename+" ne obstaja več, aktivnost v bazi popravljena.")
        let msg = "Napaka: Datoteka ne obstaja več!";
        return res.status(500).json({
          status: 'error',
          message: msg,
        })
      })
      .catch((e)=>{
        return res.status(500).json({
          status: 'error',
          message: 'Something went wrong',
          error: e,
        })
      })
  }
  
  //return
  //file exists
})
//stran za delovne naloge
router.get('/', auth.authenticate, function(req, res, next) {
  let userId = req.session.user_id;
  let userRole = req.session.type.toLowerCase();
  if(userRole == 'admin' || userRole == 'tajnik' || userRole == 'komercialist' || userRole == 'komerciala' || userRole == 'računovodstvo' || userRole.substring(0,5) == 'vodja'){
    dbWorkOrders.getLastWorkOrders(null,userRole).then(workOrders => {
      addFormatedDateForWorkOrders(workOrders);
      addFormatedTimeForWorkOrders(workOrders);
      res.render('work_orders', {
        title:"Delovni nalogi",
        user_name: req.session.user_name,
        user_username: req.session.user_username,
        user_surname: req.session.user_surname,
        user_typeid: req.session.role_id,
        user_type: req.session.type,
        user_id: req.session.user_id,
        workOrders: workOrders,
      })
    })
    .catch(e => {
      next(e);
    })
  }
  else{
    dbWorkOrders.getLastWorkOrders(userId,userRole).then(workOrders => {
      addFormatedDateForWorkOrders(workOrders);
      addFormatedTimeForWorkOrders(workOrders);
      res.render('work_orders', {
        title:"Delovni nalogi",
        user_name: req.session.user_name,
        user_username: req.session.user_username,
        user_surname: req.session.user_surname,
        user_typeid: req.session.role_id,
        user_type: req.session.type,
        user_id: req.session.user_id,
        workOrders: workOrders,
      })
    })
    .catch(e => {
      next(e);
    })
  }
})
//get reports for inputs
router.get('/get', auth.authenticate, function(req, res, next) {
  let userId;
  let userRole = req.session.type.toLowerCase();
  if(userRole == 'admin' || userRole == 'tajnik' || userRole == 'komercialist' || userRole == 'komerciala' || userRole == 'računovodstvo' || userRole.substring(0,5) == 'vodja'){
    userId = req.query.userId;
    if(userId == 0)
      userId = null;
  }
  else
    userId = req.session.user_id;
  let dateStart = req.query.dateStart;
  let dateFinish = req.query.dateFinish;
  let projectId = req.query.projectId;
  let subscriberId = req.query.subscriberId;
  let woType = req.query.woType;
  let woStatus = req.query.woStatus;
  if(projectId == 0)
    projectId = null;
  if(subscriberId == 0)
    subscriberId = null;
  if(woType == 0)
    woType = null;
  if (woStatus == 0)
    woStatus = null;
  if(!dateStart && !dateFinish && !projectId && !subscriberId && !woType && !woStatus){
    //first 100 for inital loading and listing last 100 work orders
    dbWorkOrders.getLastWorkOrders(userId,userRole)
    .then(workOrders=>{
      addFormatedDateForWorkOrders(workOrders);
      addFormatedTimeForWorkOrders(workOrders);
      return res.json({success: true, data: workOrders});  
    })
    .catch((e)=>{
      return res.json({success:false, error:e});
    })
  }
  else{
    //search form and limit to 100 if there is not start or finish
    dbWorkOrders.getUserWorkOrders(userId,dateStart,dateFinish,projectId,subscriberId,woType,userRole, woStatus)
    .then(workOrders=>{
      addFormatedDateForWorkOrders(workOrders);
      addFormatedTimeForWorkOrders(workOrders);
      return res.json({success: true, data: workOrders});  
    })
    .catch((e)=>{
      return res.json({success:false, error:e});
    })
  }
})
//add new work order
router.post('/add', auth.authenticate, function(req, res, next) {
	let userId;
	if(req.body.userId)
		userId = req.body.userId;
	else
		userId = req.session.user_id;
  //let woNumber = req.body.number;
	let woType = req.body.type;
  let woDate = req.body.date;
  let woLocation = req.body.location;
  let woLocationCord = null;
  let woLocationCordLat = null;
  let woLocationCordLon = null;
  if (req.body.locationCordLat && req.body.locationCordLon){
    // woLocationCordLat = req.body.locationCordLat;
    // woLocationCordLon = req.body.locationCordLon;
    //woLocationCord = (req.body.locationCordLat, req.body.locationCordLon);
    woLocationCord = ( '(' + req.body.locationCordLat + ',' + req.body.locationCordLon +')' );
  }
  let woProjectId = req.body.projectId;
  let woSubscriberId = req.body.subscriberId;
	let woDescription = req.body.description;
  let woVehicle = req.body.vehicle;
  let woArrival = req.body.arrival;
  let woDeparture = req.body.departure;
  let woRepresentative = req.body.representative;
  let contactId = req.body.contactId;
	dbWorkOrders.addNewWorkOrder(userId,woType,woDate,woLocation,woProjectId,woSubscriberId,woDescription,woVehicle,woArrival,woDeparture,woRepresentative,woLocationCord,contactId)
	.then(newWorkOrder => {
    res.json({success:true, woId:newWorkOrder.id, workerSuccess: false})
	})
	.catch(e => {
		return res.json({success:false, error:e});
	})
})
//edit work order
router.post('/edit', auth.authenticate, function(req, res, next) {
	let userId;
	if(req.body.userId)
		userId = req.body.userId;
	else
    userId = req.session.user_id;
  let woId = req.body.workOrderId;
  //let woNumber = req.body.number;
  let woType = req.body.type;
  let woDate = req.body.date;
  let woLocation = req.body.location;
  let woProjectId = req.body.projectId;
  let woSubscriberId = req.body.subscriberId;
  let woDescription = req.body.description;
  let woVehicle = req.body.vehicle;
  let woArrival = req.body.arrival;
  let woDeparture = req.body.departure;
  let woRepresentative = req.body.representative;
  let contactId = req.body.contactId;
	dbWorkOrders.editWorkOrder(woId,userId,woType,woDate,woLocation,woProjectId,woSubscriberId,woDescription,woVehicle,woArrival,woDeparture,woRepresentative,contactId)
	.then(workOrder => {
    res.json({success:true})
	})
	.catch(e => {
		res.json({success:false, error:e});
	})
})
//delete work order(update work order active to false)
router.post('/delete', auth.authenticate, function(req, res, next){
  let workOrderId = req.body.workOrderId;
  let active = req.body.active;
  dbWorkOrders.deleteWorkOrder(workOrderId,active)
  .then(workOrder=>{
    return res.json({success: true});  
  })
  .catch((e)=>{
    res.json({success:false, error:e});
  })
})
//get work order workers
router.get('/workers', auth.authenticate, function(req, res, next){
  let workOrderId = req.query.workOrderId;
  dbWorkOrders.getWorkOrderWorkers(workOrderId)
  .then(workers=>{
    return res.json({success: true, data: workers});  
  })
  .catch((e)=>{
    res.json({success:false, error:e});
  })
})
//add work order workers
router.post('/workers', auth.authenticate, function(req, res, next){
  let workOrderId = req.body.workOrderId;
  let addWorkers = req.body.addWorkers;
  let deleteWorkers = req.body.deleteWorkers;
  let deleteWorkersArray = null;
  if(deleteWorkers){
    deleteWorkersArray = deleteWorkers.split(',');
    if(deleteWorkersArray[0] == '')
    deleteWorkersArray.pop();
  }
  let addWorkersArray = null;
  if(addWorkers){
    addWorkersArray = addWorkers.split(',');
    if(addWorkersArray[0] == '')
      addWorkersArray.pop();
  }
  let promises = [];
  let promisesAdd = [];
  let promisesDelete = [];
  if(addWorkersArray){
    //adding new workers, need to check if there is also delete
    if(deleteWorkersArray){
      //adding and deleting workers
      for(let i=0; i<addWorkersArray.length; i++){
        promisesAdd.push(dbWorkOrders.addWorker(workOrderId,addWorkersArray[i]));
      }
      Promise.all(promisesAdd)
      .then(resultsA => {
        for(let i=0; i<deleteWorkersArray.length; i++){
          promisesDelete.push(dbWorkOrders.deleteWorker(deleteWorkersArray[i]));
        }
        Promise.all(promisesDelete)
        .then(resultsD => {
          res.json({success:true})
        })
        .catch((e)=>{
          res.json({success:false, error:e});
        })
      })
      .catch((e)=>{
        res.json({success:false, error:e});
      })
    }
    else{
      //only adding workers
      for(let i=0; i<addWorkersArray.length; i++){
        promisesAdd.push(dbWorkOrders.addWorker(workOrderId,addWorkersArray[i]));
      }
      Promise.all(promisesAdd)
      .then(results => {
        res.json({success:true})
      })
      .catch((e)=>{
        res.json({success:false, error:e});
      })  
    }
  }
  else if(deleteWorkersArray){
    //only deleting workers
    for(let i=0; i<deleteWorkersArray.length; i++){
      promisesDelete.push(dbWorkOrders.deleteWorker(deleteWorkersArray[i]));
    }
    Promise.all(promisesDelete)
    .then(results => {
      res.json({success:true})
    })
    .catch((e)=>{
      res.json({success:false, error:e});
    })
  }
  else{
    //sem nebi smel pridet ker more bit vedno vsaj en delavec v formi podan
    res.json({success:true})
  }
})
//get work order expenses
router.get('/expenses', auth.authenticate, function(req, res, next){
  let workOrderId = req.query.workOrderId;
  dbWorkOrders.getWorkOrderExpenses(workOrderId)
  .then(expenses=>{
    return res.json({success: true, data: expenses});  
  })
  .catch((e)=>{
    return res.json({success: false, error: e});  
  })
})
//post new work order expenses
router.post('/expenses/add', auth.authenticate, function(req, res, next){
  let workOrderId = req.body.workOrderId;
  let timeQuantity = req.body.timeQuantity;
  if(!timeQuantity)
    timeQuantity = null;
  let timePrice = req.body.timePrice;
  if(timePrice)
    timePrice = parseInt(Math.ceil(timePrice*100));
  else
    timePrice = null;
  let timeSum = req.body.timeSum;
  if(timeSum)
    timeSum = parseInt(Math.ceil(timeSum*100));
  else
    timeSum = null;
  let distanceQuantity = req.body.distanceQuantity;
  if (!distanceQuantity) distanceQuantity = null;
  // if(distanceQuantity)
  //   distanceQuantity = parseInt(Math.ceil(distanceQuantity*1000))
  // else
  //   distanceQuantity = null;
  let distancePrice = req.body.distancePrice;
  if(distancePrice)
    distancePrice = parseInt(Math.ceil(distancePrice*100))
  else
    distancePrice = null;
  let distanceSum = req.body.distanceSum;
  if(distanceSum)
    distanceSum = parseInt(Math.ceil(distanceSum*100));
  else
    distanceSum = null;
  let materialQuantity = req.body.materialQuantity;
  if(!materialQuantity) materialQuantity = null;
  let materialPrice = req.body.materialPrice;
  if(materialPrice)
    materialPrice = parseInt(Math.ceil(materialPrice*100))
  else
    materialPrice = null;
  let materialSum = req.body.materialSum;
  if(materialSum)
    materialSum = parseInt(Math.ceil(materialSum*100))
  else
    materialSum = null;
  let otherQuantity = req.body.otherQuantity;
  if(!otherQuantity)
    otherQuantity = null;
  let otherPrice = req.body.otherPrice;
  if(otherPrice)
    otherPrice = parseInt(Math.ceil(otherPrice*100))
  else
    otherPrice = null;
  let otherSum = req.body.otherSum;
  if(otherSum)
    otherSum = parseInt(Math.ceil(otherSum*100))
  else
    otherSum = null;
  dbWorkOrders.addWorkOrderExpenses(workOrderId,timeQuantity,timePrice,timeSum,distanceQuantity,distancePrice,distanceSum,materialQuantity,materialPrice,materialSum,otherQuantity,otherPrice,otherSum)
  .then(expenses=>{
    return res.json({success: true, data: expenses});  
  })
  .catch((e)=>{
    return res.json({success:false, error:e});
  })
})
//post new work order expenses
router.post('/expenses/update', auth.authenticate, function(req, res, next){
  let workOrderId = req.body.workOrderId;
  let timeQuantity = req.body.timeQuantity;
  if(!timeQuantity)
    timeQuantity = null;
  let timePrice = req.body.timePrice;
  if(timePrice)
    timePrice = parseInt(Math.ceil(timePrice*100));
  else
    timePrice = null;
  let timeSum = req.body.timeSum;
  if(timeSum)
    timeSum = parseInt(Math.ceil(timeSum*100));
  else
    timeSum = null;
  let distanceQuantity = req.body.distanceQuantity;
  if (!distanceQuantity) distanceQuantity = null;
  // if(distanceQuantity)
  //   distanceQuantity = parseInt(Math.ceil(distanceQuantity*1000))
  // else
  //   distanceQuantity = null;
  let distancePrice = req.body.distancePrice;
  if(distancePrice)
    distancePrice = parseInt(Math.ceil(distancePrice*100))
  else
    distancePrice = null;
  let distanceSum = req.body.distanceSum;
  if(distanceSum)
    distanceSum = parseInt(Math.ceil(distanceSum*100));
  else
    distanceSum = null;
  let materialQuantity = req.body.materialQuantity;
  if(!materialQuantity) materialQuantity = null;
  let materialPrice = req.body.materialPrice;
  if(materialPrice)
    materialPrice = parseInt(Math.ceil(materialPrice*100))
  else
    materialPrice = null;
  let materialSum = req.body.materialSum;
  if(materialSum)
    materialSum = parseInt(Math.ceil(materialSum*100))
  else
    materialSum = null;
  let otherQuantity = req.body.otherQuantity;
  if(!otherQuantity) otherQuantity = null;
  let otherPrice = req.body.otherPrice;
  if(otherPrice)
    otherPrice = parseInt(Math.ceil(otherPrice*100))
  else
    otherPrice = null;
  let otherSum = req.body.otherSum;
  if(otherSum)
    otherSum = parseInt(Math.ceil(otherSum*100))
  else
    otherSum = null;
  dbWorkOrders.updateWorkOrderExpenses(workOrderId,timeQuantity,timePrice,timeSum,distanceQuantity,distancePrice,distanceSum,materialQuantity,materialPrice,materialSum,otherQuantity,otherPrice,otherSum)
  .then(expenses=>{
    return res.json({success: true, data: expenses});  
  })
  .catch((e)=>{
    return res.json({success:false, error:e});
  })
})
//get work order materials
router.get('/materials', auth.authenticate, function(req, res, next){
  let workOrderId = req.query.workOrderId;
  dbWorkOrders.getWorkOrderMaterial(workOrderId)
  .then(materials=>{
    return res.json({success: true, data: materials});  
  })
  .catch((e)=>{
    return res.json({success:false, error:e});
  })
})
//post new work order materials
router.post('/materials/add', auth.authenticate, function(req, res, next){
  let workOrderId = req.body.workOrderId;
  let name1 = null, quantity1 = null, price1 = null, sum1 = null;
  let name2 = null, quantity2 = null, price2 = null, sum2 = null;
  let name3 = null, quantity3 = null, price3 = null, sum3 = null;
  let name4 = null, quantity4 = null, price4 = null, sum4 = null;
  let name5 = null, quantity5 = null, price5 = null, sum5 = null;
  let name6 = null, quantity6 = null, price6 = null, sum6 = null;
  let name7 = null, quantity7 = null, price7 = null, sum7 = null;
  if(req.body.name1) name1 = req.body.name1;
  if(req.body.quantity1) quantity1 = req.body.quantity1;
  if(req.body.price1) price1 = req.body.price1;
  if(req.body.sum1) sum1 = req.body.sum1;
  if(req.body.name2) name2 = req.body.name2;
  if(req.body.quantity2) quantity2 = req.body.quantity2;
  if(req.body.price2) price2 = req.body.price2;
  if(req.body.sum2) sum2 = req.body.sum2;
  if(req.body.name3) name3 = req.body.name3;
  if(req.body.quantity3) quantity3 = req.body.quantity3;
  if(req.body.price3) price3 = req.body.price3;
  if(req.body.sum3) sum3 = req.body.sum3;
  if(req.body.name4) name4 = req.body.name4;
  if(req.body.quantity4) quantity4 = req.body.quantity4;
  if(req.body.price4) price4 = req.body.price4;
  if(req.body.sum4) sum4 = req.body.sum4;
  if(req.body.name5) name5 = req.body.name5;
  if(req.body.quantity5) quantity5 = req.body.quantity5;
  if(req.body.price5) price5 = req.body.price5;
  if(req.body.sum5) sum5 = req.body.sum5;
  if(req.body.name6) name6 = req.body.name6;
  if(req.body.quantity6) quantity6 = req.body.quantity6;
  if(req.body.price6) price6 = req.body.price6;
  if(req.body.sum6) sum6 = req.body.sum6;
  if(req.body.name7) name7 = req.body.name7;
  if(req.body.quantity7) quantity7 = req.body.quantity7;
  if(req.body.price7) price7 = req.body.price7;
  if(req.body.sum7) sum7 = req.body.sum7;
  dbWorkOrders.addWorkOrderMaterials(workOrderId,name1,quantity1,price1,sum1,name2,quantity2,price2,sum2,name3,quantity3,price3,sum3,name4,quantity4,price4,sum4,name5,quantity5,price5,sum5,name6,quantity6,price6,sum6,name7,quantity7,price7,sum7)
  .then(materials=>{
    return res.json({success: true, data: materials});  
  })
  .catch((e)=>{
    return res.json({success:false, error:e});
  })
})
//post updated work order materials
router.post('/materials/update', auth.authenticate, function(req, res, next){
  let workOrderId = req.body.workOrderId;
  let name1 = null, quantity1 = null, price1 = null, sum1 = null;
  let name2 = null, quantity2 = null, price2 = null, sum2 = null;
  let name3 = null, quantity3 = null, price3 = null, sum3 = null;
  let name4 = null, quantity4 = null, price4 = null, sum4 = null;
  let name5 = null, quantity5 = null, price5 = null, sum5 = null;
  let name6 = null, quantity6 = null, price6 = null, sum6 = null;
  let name7 = null, quantity7 = null, price7 = null, sum7 = null;
  if(req.body.name1) name1 = req.body.name1;
  if(req.body.quantity1) quantity1 = req.body.quantity1;
  if(req.body.price1) price1 = req.body.price1;
  if(req.body.sum1) sum1 = req.body.sum1;
  if(req.body.name2) name2 = req.body.name2;
  if(req.body.quantity2) quantity2 = req.body.quantity2;
  if(req.body.price2) price2 = req.body.price2;
  if(req.body.sum2) sum2 = req.body.sum2;
  if(req.body.name3) name3 = req.body.name3;
  if(req.body.quantity3) quantity3 = req.body.quantity3;
  if(req.body.price3) price3 = req.body.price3;
  if(req.body.sum3) sum3 = req.body.sum3;
  if(req.body.name4) name4 = req.body.name4;
  if(req.body.quantity4) quantity4 = req.body.quantity4;
  if(req.body.price4) price4 = req.body.price4;
  if(req.body.sum4) sum4 = req.body.sum4;
  if(req.body.name5) name5 = req.body.name5;
  if(req.body.quantity5) quantity5 = req.body.quantity5;
  if(req.body.price5) price5 = req.body.price5;
  if(req.body.sum5) sum5 = req.body.sum5;
  if(req.body.name6) name6 = req.body.name6;
  if(req.body.quantity6) quantity6 = req.body.quantity6;
  if(req.body.price6) price6 = req.body.price6;
  if(req.body.sum6) sum6 = req.body.sum6;
  if(req.body.name7) name7 = req.body.name7;
  if(req.body.quantity7) quantity7 = req.body.quantity7;
  if(req.body.price7) price7 = req.body.price7;
  if(req.body.sum7) sum7 = req.body.sum7;
  dbWorkOrders.updateWorkOrderMaterials(workOrderId,name1,quantity1,price1,sum1,name2,quantity2,price2,sum2,name3,quantity3,price3,sum3,name4,quantity4,price4,sum4,name5,quantity5,price5,sum5,name6,quantity6,price6,sum6,name7,quantity7,price7,sum7)
  .then(materials=>{
    return res.json({success: true, data: materials});  
  })
  .catch((e)=>{
    return res.json({success:false, error:e});
  })
})
//get work order expenses
router.get('/files', auth.authenticate, function(req, res, next){
  let workOrderId = req.query.workOrderId;
  dbWorkOrders.getWorkOrderFiles(workOrderId)
  .then(files=>{
    // files.forEach(file =>  {
    //   let fileLocation = `${__dirname}/../files/workorders/${file.path_name}`;
    //   if (fs.existsSync(fileLocation) && file.type == 'IMG') {
    //     file.base64 = fs.readFileSync(fileLocation, 'base64');
    //   }
    // })
    return res.json({success: true, data: files});  
  })
  .catch((e)=>{
    return res.json({success: false, error: e});  
  })
})
//send files that is hidden from public and is connected to projects
router.get('/files/resources', auth.authenticate, (req,res)=>{
  let fileName = req.query.fileName;
  //res.sendFile(path.resolve('files/projects/' + fileName));
  if (fileName.substring(fileName.length-3).toLocaleLowerCase() == 'pdf'){
    res.set({"Content-Type": "application/pdf",});
  }
  else if (fileName.substring(fileName.length-3).toLocaleLowerCase() == 'doc' || fileName.substring(fileName.length-4).toLocaleLowerCase() == 'docx'){
    res.set({"Content-Type": "application/msword",});
  }
  else if (fileName.substring(fileName.length-3).toLocaleLowerCase() == 'ppt'){
    res.set({"Content-Type": "application/mspowerpoint",});
  }
  else if (fileName.substring(fileName.length-3).toLocaleLowerCase() == 'xls'){
    res.set({"Content-Type": "application/excel",});
  }
  else if (fileName.substring(fileName.length-3).toLocaleLowerCase() == 'zip'){
    res.set({"Content-Type": "application/x-compressed",});
  }
  else if (fileName.substring(fileName.length-3).toLocaleLowerCase() == 'png'){
    res.set({"Content-Type": "image/png",});
  }
  else if (fileName.substring(fileName.length-3).toLocaleLowerCase() == 'jpg' || fileName.substring(fileName.length-4).toLocaleLowerCase() == 'jpeg'){
    res.set({"Content-Type": "image/jpeg",});
  }
  else if (fileName.substring(fileName.length-3).toLocaleLowerCase() == 'gif'){
    res.set({"Content-Type": "image/gif",});
  }
  else
    res.set({"Content-Type": "text/plain",});
  if (fs.existsSync(path.resolve('files/workorders/' + fileName))) {
    fs.createReadStream(path.resolve('files/workorders/' + fileName))
    .pipe(res);
  }
  else{
    res.json({success:false, msg:'Datoteka ne obstaja'});
  }
})
//get thumbnails for img and later for pdf as well
router.get('/files/thumbnail', auth.authenticate, function(req, res, next){
  let fileId = req.query.fileId;
  let filePath = req.query.filePath;
  let fileType = req.query.fileType;
  let fileLocation = `${__dirname}/../files/workorders/${filePath}`;
  let fileBase64 = '';
  if (fileType == 'IMG'){
    Jimp.read(fileLocation).then(img => {
      img.resize(150, Jimp.AUTO).quality(20).getBase64Async(Jimp.AUTO).then(base64 => {
        return res.json({success:true, data: base64, id: fileId, type: 'IMG', filePath});
      })
      .catch(err => {
        return res.json({success:false, error: e});
      });
    })
    .catch((e) => {
      return res.json({success:false, error: e, msg: 'Datoteka ne obstaja.', id: fileId, type: fileType});
    })
  }
  else if (fileType == 'PDF'){
    if (fs.existsSync(fileLocation)) {
     fsp.readFile(fileLocation, 'base64').then(data => {
        //console.log(data);
        return res.json({success:true, data, id: fileId, type: 'PDF', filePath});
      })
      .catch((e) => {
        return res.json({success:false, error: e});
      })
    }
    else{
      res.json({success:false, msg:'Datoteka ne obstaja'});
    }
    //return res.json({success:false, mgs:'not supported type yet'});
  }
  else{
    return res.json({success:false, mgs:'not supported type'});
  }
})
//delete file
router.delete('/files', auth.authenticate, function(req, res, next){
  let fileId = req.body.fileId;
  //check if file exist, if exist do unlink and then for both cases update deleted to true
  //dbFiles.deleteFile(fileId)
  dbWorkOrders.getFile(fileId).then(file => {
    if(file.active){ // file still exist, first delete/unlink it in directory and then mark it as deleted file
      fs.unlink(path.resolve('files/workorders/' + file.path_name), function(err) {
        if(err && err.code == 'ENOENT') {
          // file doens't exist
          console.info("File doesn't exist, won't remove it.");
          dbWorkOrders.deleteFile(fileId).then(deletedFile => {
            res.json({success:true, msg:'Datoteka je že izbrisana.', file:deletedFile});
          })
          .catch((e)=>{
            return res.json({success:false, error: e, msg:'Datoteka je že izbrisana, napaka pri zapisu v bazo o brisanju datoteke.'});
          })
        }
        else if (err) {
          // other errors, e.g. maybe we don't have enough permission
          console.error("Error occurred while trying to remove file");
          return res.json({success:false, error: e});
        }
        else {
          console.info(`removed`);
          dbWorkOrders.deleteFile(fileId).then(deletedFile => {
            res.json({success:true, msg:'Datoteka je uspešno izbrisana.', file:deletedFile});
          })
          .catch((e)=>{
            return res.json({success:false, error: e, msg:'Datoteka je uspešno izbrisana, napaka pri zapisu v bazo o brisanju datoteke.'});
          })
        }
      });
    }
    else{ // file doesnt exist anymore in directory or is corrupted or its named was changed from linux cmd --> just mark it as deleted
      dbWorkOrders.deleteFile(fileId).then(deletedFile => {
        res.json({success:true, msg:'Datoteka je označena kot izbrisana.', file:deletedFile});
      })
      .catch((e)=>{
        return res.json({success:false, error: e, msg:'Napaka pri zapisu v bazo o brisanju datoteke.'});
      })
    }
    //res.json({success:true});
  })
  .catch((e)=>{
    return res.json({success:false, error: e, msg:'Napaka pri iskanju datoteke v bazi.'});
  })
})
// //get work order expenses
// router.get('/signs', auth.authenticate, function(req, res, next){
//   let workOrderId = req.query.workOrderId;
//   //var fs = require('fs');
//   dbWorkOrders.getWorkOrderSigns(workOrderId)
//   .then(signs=>{
//     //let name = signs[0].path_name;
//     //var file = `${__dirname}/../files/workorders/${name}`;
//     //let imageAsBase64 = base64_encode(file);
//     //let imageAsBase64 = fs.readFileSync(file, 'base64');
//     //console.log(imageAsBase64);
//     return res.json({success: true, data: signs});  
//   })
//   .catch((e)=>{
//     return res.json({success: false, error: e});  
//   })
// })
//get change types
router.get('/types', auth.authenticate, function(req, res, next){
  dbWorkOrders.getWorkOrderTypes()
  .then(types=>{
    return res.json({success: true, data: types});  
  })
  .catch((e)=>{
    return res.json({success:false, error:e});
  })
})
//get work order status
router.get('/status', auth.authenticate, function(req, res, next){
  dbWorkOrders.getWorkOrderStatus()
  .then(status=>{
    return res.json({success: true, data: status});  
  })
  .catch((e)=>{
    return res.json({success: false});
  })
})
//update work order status
router.post('/status', auth.authenticate, function(req, res, next){
  let woId = req.body.workOrderId;
  let statusId = req.body.statusId;
  dbWorkOrders.updateWorkOrderStatus(woId,statusId)
  .then(status=>{
    return res.json({success: true});  
  })
  .catch((e)=>{
    return res.json({success: false});
  })
})
//get all projects for select2 (id,text)
router.get('/projects', auth.authenticate, function(req, res, next){
  let active = req.query.activeProject;
  dbReports.getProjectsSelect(active).then(projects=>{
    res.json({success:true, data:projects});
  })
  .catch((e)=>{
    return res.json({success:false, error:e});
  })
})
//get all servises for select2 (id,text)
router.get('/services', auth.authenticate, function(req, res, next){
  dbWorkOrders.getServiseForWorkOrder().then(services=>{
    res.json({success:true, data:services});
  })
  .catch((e)=>{
    return res.json({success:false, error:e});
  })
})
//get all servises for select2 (id,text)
router.post('/services', auth.authenticate, function(req, res, next){
  let woId = req.body.workOrderId;
  let taskId = req.body.taskId;
  if(!taskId) taskId = null
  dbWorkOrders.saveServiseForWorkOrder(woId,taskId).then(services=>{
    res.json({success:true, data:services});
  })
  .catch((e)=>{
    return res.json({success:false, error:e});
  })
})
//get all changes for work order
router.get('/changes', auth.authenticate, function(req, res, next){
  let workOrderId = req.query.workOrderId;
  dbWorkOrders.getWorkOrderChangesSystem(workOrderId).then(changes=>{
    res.json({success:true, data:changes});
  })
  .catch((e)=>{
    return res.json({success:false, error:e});
  })
})
//get all work order contacts
router.get('/contacts', auth.authenticate, function(req, res, next){
  let contactId = req.query.contactId;
  if (contactId) {
    // get contact with right id
    dbWorkOrders.getAllContacts(contactId).then(contact=>{
      res.json({success:true, contact});
    })
    .catch((e)=>{
      return res.json({success:false, error:e});
    })
  }
  else{
    // get all contacts
    dbWorkOrders.getAllContacts().then(contacts=>{
      res.json({success:true, contacts});
    })
    .catch((e)=>{
      return res.json({success:false, error:e});
    })
  }
})
//add work order contact
router.post('/contacts', auth.authenticate, function(req, res, next){
  let name = req.body.contactName;
  let phone = req.body.contactPhone;
  dbWorkOrders.addNewWOContact(name, phone)
  .then(contact=>{
    return res.json({success: true, contact});  
  })
  .catch((e)=>{
    return res.json({success: false});
  })
})
//edit work order contact
router.put('/contacts', auth.authenticate, function(req, res, next){
  // let name = req.body.contactName;
  let contactId = req.body.contactId;
  let phone = req.body.contactPhone;
  dbWorkOrders.editWOContact(contactId, phone)
  .then(contact=>{
    return res.json({success: true, contact});  
  })
  .catch((e)=>{
    return res.json({success: false});
  })
})
//send mail
router.post('/mail', auth.authenticate, function(req, res, next){
  let workOrderId = req.body.workOrderId;
  let email = req.body.email;
  let officeCheck = req.body.officeCheck;
  let useTimeout = req.body.useTimeout ? req.body.useTimeout : false;
  let timeAmount = req.body.timeAmount ? req.body.timeAmount : 0;
  let signId = req.body.signId;
  let notificationMail = req.body.notificationMail ? req.body.notificationMail : false;
  let officeMail = req.body.officeMail ? req.body.officeMail : false;

  if(!useTimeout) timeAmount = 0;
  
  let tmp = process.env.MAIL_HOST;
  if(!email)
    res.sendStatus(400);
  else if(!tmp){
    return res.status(500).json({
      status: 'error',
      message: 'Server has no mail settings. Contact server admin.',
    })
  }
  else{
    setTimeout(() => {
      //get work order, expenses, materials, signs, robotehImage, kukaPartnerImage
      let promises = [];
      //get work order
      promises.push(dbWorkOrders.getWorkOrder(workOrderId));
      //get workers
      promises.push(dbWorkOrdersAPI.getWorkOrderWorkersAPI(workOrderId));
      //get expenses
      promises.push(dbWorkOrdersAPI.getWorkOrderExpenses(workOrderId));
      //get materials
      promises.push(dbWorkOrdersAPI.getWorkOrderMaterial(workOrderId));
      //get signs
      promises.push(dbWorkOrdersAPI.getWorkOrderSignsAPI(workOrderId));
      //get roboteh base64 image
      promises.push(dbBase64Images.getRobotehImage());
      //get kuka partner base64 image
      promises.push(dbBase64Images.getKukaPartnerImage());
      //get sign with signId (problems when creating work order and sign is still not attach to work order
      promises.push(dbWorkOrdersAPI.getSignWithIdAPI(signId));
      // get all data together and create pdf document and send it through mail server
      Promise.all(promises).then(([workOrder, workOrderWorkers, workOrderExpenses, workOrderMaterials, workOrderSigns, robotehImg, kukaPartnerImg, woSign]) => {
        // console.log(workOrder);
        // console.log('all data gathered, woId: ' + workOrderId);
        // let woName = 'test';
        //PREPARE DATA FOR PDF DEFINITION
        var woName = workOrder.wo_number;
        var woSubscriber = '';
        if(workOrder.subscriber_id) woSubscriber = workOrder.subscriber_name;
        var woProject = ' ';
        if(workOrder.project_id) woProject = workOrder.project_name;
        var userName = ' ';
        if(workOrder.user_id) userName = workOrder.user_name;
        //TIP - OZNAČBA Z BARVO
        var typeOne = 'white', typeTwo = 'white', typeThree = 'white', typeFour = 'white', typeFive = 'white', typeSix = 'white', typeSeven = 'white';
        //if(workOrder.work_order_type_id)
        switch(workOrder.work_order_type_id) {
          case 1: typeOne = 'silver';
            break;
          case 2: typeTwo = 'silver';
            break;
          case 3: typeThree = 'silver';
            break;
          case 4: typeFour = 'silver';
            break;
          case 5: typeFive = 'silver';
            break;
          case 6: typeSix = 'silver';
            break;
          case 7: typeSeven = 'silver';
            break
          default:
            // code block
        }
        //DELAVCI
        var workers = '';
        if(workOrderWorkers && workOrderWorkers.length > 0){
          for(var i = 0; i < workOrderWorkers.length; i++){
            if(i > 0 && i < workOrderWorkers.length)
              workers += ', ';
            workers += workOrderWorkers[i].worker_name;
          }
        }
        else
          workers = ' ';
        //EXPENSES
        var timeQuantity = '', timePrice = '', timeSum = '', distanceQunatity = '', distancePrice = '', distanceSum = '', materialQuantity = '', materialPrice = '', materialSum = '', otherQuantity = '', otherPrice = '', otherSum = '';
        if(workOrderExpenses[0]){
          workOrderExpenses = workOrderExpenses[0];
          if(workOrderExpenses.time_quantity){
            var time = workOrderExpenses.time_quantity.split(':');
            timeQuantity = time[0]+'h '+time[1]+'m';
          }
          if(workOrderExpenses.time_price) timePrice = workOrderExpenses.time_price/100;
          if(workOrderExpenses.time_sum) timeSum = workOrderExpenses.time_sum/100;
          if(workOrderExpenses.distance_quantity) distanceQunatity = workOrderExpenses.distance_quantity;
          if(workOrderExpenses.distance_price) distancePrice = workOrderExpenses.distance_price/100;
          if(workOrderExpenses.distance_sum) distanceSum = workOrderExpenses.distance_sum/100;
          if(workOrderExpenses.material_quantity) materialQuantity = workOrderExpenses.material_quantity;
          if(workOrderExpenses.material_price) materialPrice = workOrderExpenses.material_price/100;
          if(workOrderExpenses.material_sum) materialSum = workOrderExpenses.material_sum/100;
          if(workOrderExpenses.other_quantity) otherQuantity = workOrderExpenses.other_quantity;
          if(workOrderExpenses.other_price) otherPrice = workOrderExpenses.other_price/100;
          if(workOrderExpenses.other_sum) otherSum = workOrderExpenses.other_sum/100;
          //debugger
        }
        //MATERIALS
        var name1 = '',name2 = '',name3 = '',name4 = '',name5 = '',name6 = '',name7 = '',quantity1 = '',quantity2 = '',quantity3 = '',quantity4 = '',quantity5 = '',quantity6 = '',quantity7 = '',price1 = '',price2 = '',price3 = '',price4 = '',price5 = '',price6 = '',price7 = '',sum1 = '',sum2 = '',sum3 = '',sum4 = '',sum5 = '',sum6 = '',sum7 = '';
        if(workOrderMaterials[0]){
          workOrderMaterials = workOrderMaterials[0]
          if(workOrderMaterials.name1) name1 = workOrderMaterials.name1;
          if(workOrderMaterials.name2) name2 = workOrderMaterials.name2;
          if(workOrderMaterials.name3) name3 = workOrderMaterials.name3;
          if(workOrderMaterials.name4) name4 = workOrderMaterials.name4;
          if(workOrderMaterials.name5) name5 = workOrderMaterials.name5;
          if(workOrderMaterials.name6) name6 = workOrderMaterials.name6;
          if(workOrderMaterials.name7) name7 = workOrderMaterials.name7;
          if(workOrderMaterials.quantity1) quantity1 = workOrderMaterials.quantity1;
          if(workOrderMaterials.quantity2) quantity2 = workOrderMaterials.quantity2;
          if(workOrderMaterials.quantity3) quantity3 = workOrderMaterials.quantity3;
          if(workOrderMaterials.quantity4) quantity4 = workOrderMaterials.quantity4;
          if(workOrderMaterials.quantity5) quantity5 = workOrderMaterials.quantity5;
          if(workOrderMaterials.quantity6) quantity6 = workOrderMaterials.quantity6;
          if(workOrderMaterials.quantity7) quantity7 = workOrderMaterials.quantity7;
          if(workOrderMaterials.price1) price1 = workOrderMaterials.price1;
          if(workOrderMaterials.price2) price2 = workOrderMaterials.price2;
          if(workOrderMaterials.price3) price3 = workOrderMaterials.price3;
          if(workOrderMaterials.price4) price4 = workOrderMaterials.price4;
          if(workOrderMaterials.price5) price5 = workOrderMaterials.price5;
          if(workOrderMaterials.price6) price6 = workOrderMaterials.price6;
          if(workOrderMaterials.price7) price7 = workOrderMaterials.price7;
          if(workOrderMaterials.sum1) sum1 = workOrderMaterials.sum1;
          if(workOrderMaterials.sum2) sum2 = workOrderMaterials.sum2;
          if(workOrderMaterials.sum3) sum3 = workOrderMaterials.sum3;
          if(workOrderMaterials.sum4) sum4 = workOrderMaterials.sum4;
          if(workOrderMaterials.sum5) sum5 = workOrderMaterials.sum5;
          if(workOrderMaterials.sum6) sum6 = workOrderMaterials.sum6;
          if(workOrderMaterials.sum7) sum7 = workOrderMaterials.sum7;
        }
        //SIGN REPRESENTATIVE
        var sign = '';
        dateFormat(workOrder.date, "dd.mm.yyyy")
        var signElement = [];
        if(workOrderSigns[0]){
          //need to open image of sign and encode to base64 because of pdfmake
          var file = `${__dirname}/../files/workorders/signs/${workOrderSigns[0].path_name}`;
          //console.log(file);
          if (fs.existsSync(file)) {
            // Do something
            let imageAsBase64 = fs.readFileSync(file, 'base64');
            sign = 'data:image/png;base64,' + imageAsBase64;
            signElement = [{
              image: 'podpis',
              absolutePosition: {x: 470, y: 755},
              fit: [100, 60],
            }]
          }
        }
        else if(woSign[0]){
          //need to open image of sign and encode to base64 because of pdfmake
          var file = `${__dirname}/../files/workorders/signs/${woSign[0].path_name}`;
          //console.log(file);
          if (fs.existsSync(file)) {
            // Do something
            let imageAsBase64 = fs.readFileSync(file, 'base64');
            sign = 'data:image/png;base64,' + imageAsBase64;
            signElement = [{
              image: 'podpis',
              absolutePosition: {x: 470, y: 755},
              fit: [100, 60],
            }]
          }
        }
        let hiddenCopyMail = '';
        if(process.env.MAIL_COPY){
          hiddenCopyMail = process.env.MAIL_COPY
        }
        if(process.env.ETHEREAL_COPY){
          hiddenCopyMail = 'test.copy@roboteh.si'
        }
        let testSubject = '';
        let userId = req.session.user_id;
        //userId 2 is for Novi Uporabnik that is meant for testing purposes
        if(userId == 2){
          testSubject = ' (TESTIRANJE)';
          hiddenCopyMail = 'klemen.sibal@roboteh.si';
        }
        if(!officeCheck) hiddenCopyMail = '';
        var htmlElement = `<p>Pozdravljeni,</p>
        <p>pošiljamo kopijo delovnega naloga.</p>
        <p>Na ta elektronski naslov ne odgovarjate.</p>
        <p>Lep pozdrav
        <br/>Roboteh Informacijski Sistem</p>
        <img src="cid:kuka@partner"/>`;
        var textMailElement = `Pozdravljeni,
        pošiljamo kopijo delovnega naloga.
        
        Na ta elektronski naslov ne odgovarjate.
        
        Lep pozdrav
        Roboteh Informacijski Sistem`;
        var subjectElement = "Kopija delovnega naloga"+testSubject;
        var attachmentsElement = [
          {
            filename: 'kuka.png',
            path: __dirname+`/..//icons/logo/kuka_partner.png`,
            cid: 'kuka@partner' //same cid value as in the html img src
          }
        ];
        var docDefinition;
        var pdfDoc;
        if (!notificationMail){
          // work order is finished, should have all basic information
          docDefinition = preparePDFWorkOrder(woName, woSubscriber, woProject, userName, sign, signElement, workOrder, typeOne, typeTwo, typeThree, typeFour, typeFive, typeSix, typeSeven, workers, timeQuantity, timePrice, timeSum, distanceQunatity, distancePrice, distanceSum, materialQuantity, materialPrice, materialSum, otherQuantity, otherPrice, otherSum, name1, name2, name3, name4, name5, name6, name7, quantity1, quantity2, quantity3, quantity4, quantity5, quantity6, quantity7, price1, price2, price3, price4, price5, price6, price7, sum1, sum2, sum3, sum4, sum5, sum6, sum7, robotehImg, kukaPartnerImg);
          pdfDoc = printer.createPdfKitDocument(docDefinition); 
          pdfDoc.end();
          // stream as an attachment
          attachmentsElement.push({ filename: 'DelovniNalog'+woName+'.pdf', content: pdfDoc });
        }
        else{
          // work order is half empty, admin/leader/secratary made prepaired work order and now is sending notification mail to worker
          let author = req.session.user_name + ' ' + req.session.user_surname;
          let contactText = workOrder.contact_id ? workOrder.contact_name + ', ' + workOrder.contact_phone : 'kontakta ni bilo dodanega';
          htmlElement = `<p>Pozdravljeni,</p>
          <p>` + author + ` vam je dodelil delovni nalog ` + woName + ` za ` + woSubscriber + ` dne ` + dateFormat(workOrder.date, "dd.mm.yyyy") + `.</p>
          <p>Opis: ` + workOrder.description + `</p>
          <p>Kontaktna oseba: ` + contactText + `.</p>
          <p>Ob končani storitvi popravite obstoječi delovni nalog ` + woName + ` in ga pošljete naprej v tajništvo. Sistem RIS bo praviloma samodejno predlagal na dogovorjeni elektronski naslov.</p>
          <p>Lep pozdrav
          <br/>Roboteh Informacijski Sistem</p>
          <img src="cid:kuka@partner"/>`;
          textMailElement = `Pozdravljeni,
          ` + author + ` vam je dodelil delovni nalog ` + woName + ` za ` + woSubscriber + ` dne ` + dateFormat(workOrder.date, "dd.mm.yyyy") + `.

          Opis: ` + workOrder.description + `

          Kontaktna oseba: ` + contactText + `.

          Ob končani storitvi popravite obstoječi delovni nalog ` + woName + ` in ga pošljete naprej v tajništvo. Sistem RIS bo praviloma samodejno predlagal na dogovorjeni elektronski naslov.
          
          Na ta elektronski naslov ne odgovarjate.
          
          Lep pozdrav
          Roboteh Informacijski Sistem`;
        }
        var message = {
          from: "noreply@roboteh.si",
          to: email,
          bcc: hiddenCopyMail,
          subject: subjectElement,
          text: textMailElement,
          html: htmlElement,
          attachments: attachmentsElement,
        };
        //save email to workorder for later use
        dbWorkOrdersAPI.saveEMail(workOrderId,email, notificationMail, officeMail).then(resultMail => {
          setTimeout(() => {
            //console.log('Sending mail to: ' + email);
            transporter.sendMail(message)
            .then(info => {
              console.log('Email was successfully send to ' + email + ' with pdf copy of work order with id ' + workOrderId + '.');
              dbChanges.addNewChangeSystem(12,18,userId,null,null,null,null,null,null,null,null,null,workOrderId).then(change =>{
                res.json({success:true, message:'Email was successfully send to ' + email + ' with pdf copy of work order with id ' + workOrderId + '.', info: info});//200
              })
              .catch((e)=>{
                console.log(e);
                return res.status(500).json({
                  status: 'error',
                  message: 'Something went wrong',
                  error: e,
                })
              })
            })
            .catch((e)=>{
              console.log(e);
              return res.status(500).json({
                status: 'error',
                message: 'Something went wrong',
                error: e,
              })
            })
          }, timeAmount);
        })
        .catch((e)=>{
          console.log(e);
          return res.status(500).json({
            status: 'error',
            message: 'Something went wrong',
            error: e,
          })
        })
      })
      .catch((e)=>{
        console.log(e);
        return res.status(500).json({
          status: 'error',
          message: 'Something went wrong',
          error: e,
        })
      })
    }, timeAmount);
  }
})
// get/download pdf of work order id
router.get('/pdf', auth.authenticate, function(req, res, next){
  let workOrderId = req.query.workOrderId;
  
  if(!workOrderId)
    res.sendStatus(400);
  else{    
    //get work order, expenses, materials, signs, robotehImage, kukaPartnerImage
    let promises = [];
    //get work order
    promises.push(dbWorkOrders.getWorkOrder(workOrderId));
    //get workers
    promises.push(dbWorkOrdersAPI.getWorkOrderWorkersAPI(workOrderId));
    //get expenses
    promises.push(dbWorkOrdersAPI.getWorkOrderExpenses(workOrderId));
    //get materials
    promises.push(dbWorkOrdersAPI.getWorkOrderMaterial(workOrderId));
    //get signs
    promises.push(dbWorkOrdersAPI.getWorkOrderSignsAPI(workOrderId));
    //get roboteh base64 image
    promises.push(dbBase64Images.getRobotehImage());
    //get kuka partner base64 image
    promises.push(dbBase64Images.getKukaPartnerImage());
    // get all data together and create pdf document and send it through mail server
    Promise.all(promises).then(([workOrder, workOrderWorkers, workOrderExpenses, workOrderMaterials, workOrderSigns, robotehImg, kukaPartnerImg]) => {
      // console.log(workOrder);
      // console.log('all data gathered, woId: ' + workOrderId);
      // let woName = 'test';
      //PREPARE DATA FOR PDF DEFINITION
      var woName = workOrder.wo_number;
      var woSubscriber = '';
      if(workOrder.subscriber_id) woSubscriber = workOrder.subscriber_name;
      var woProject = ' ';
      if(workOrder.project_id) woProject = workOrder.project_name;
      var userName = ' ';
      if(workOrder.user_id) userName = workOrder.user_name;
      //TIP - OZNAČBA Z BARVO
      var typeOne = 'white', typeTwo = 'white', typeThree = 'white', typeFour = 'white', typeFive = 'white', typeSix = 'white', typeSeven = 'white';
      //if(workOrder.work_order_type_id)
      switch(workOrder.work_order_type_id) {
        case 1: typeOne = 'silver';
          break;
        case 2: typeTwo = 'silver';
          break;
        case 3: typeThree = 'silver';
          break;
        case 4: typeFour = 'silver';
          break;
        case 5: typeFive = 'silver';
          break;
        case 6: typeSix = 'silver';
          break;
        case 7: typeSeven = 'silver';
          break
        default:
          // code block
      }
      //DELAVCI
      var workers = '';
      if(workOrderWorkers && workOrderWorkers.length > 0){
        for(var i = 0; i < workOrderWorkers.length; i++){
          if(i > 0 && i < workOrderWorkers.length)
            workers += ', ';
          workers += workOrderWorkers[i].worker_name;
        }
      }
      else
        workers = ' ';
      //EXPENSES
      var timeQuantity = '', timePrice = '', timeSum = '', distanceQunatity = '', distancePrice = '', distanceSum = '', materialQuantity = '', materialPrice = '', materialSum = '', otherQuantity = '', otherPrice = '', otherSum = '';
      if(workOrderExpenses[0]){
        workOrderExpenses = workOrderExpenses[0];
        if(workOrderExpenses.time_quantity){
          var time = workOrderExpenses.time_quantity.split(':');
          timeQuantity = time[0]+'h '+time[1]+'m';
        }
        if(workOrderExpenses.time_price) timePrice = workOrderExpenses.time_price/100;
        if(workOrderExpenses.time_sum) timeSum = workOrderExpenses.time_sum/100;
        if(workOrderExpenses.distance_quantity) distanceQunatity = workOrderExpenses.distance_quantity;
        if(workOrderExpenses.distance_price) distancePrice = workOrderExpenses.distance_price/100;
        if(workOrderExpenses.distance_sum) distanceSum = workOrderExpenses.distance_sum/100;
        if(workOrderExpenses.material_quantity) materialQuantity = workOrderExpenses.material_quantity;
        if(workOrderExpenses.material_price) materialPrice = workOrderExpenses.material_price/100;
        if(workOrderExpenses.material_sum) materialSum = workOrderExpenses.material_sum/100;
        if(workOrderExpenses.other_quantity) otherQuantity = workOrderExpenses.other_quantity;
        if(workOrderExpenses.other_price) otherPrice = workOrderExpenses.other_price/100;
        if(workOrderExpenses.other_sum) otherSum = workOrderExpenses.other_sum/100;
        //debugger
      }
      //MATERIALS
      var name1 = '',name2 = '',name3 = '',name4 = '',name5 = '',name6 = '',name7 = '',quantity1 = '',quantity2 = '',quantity3 = '',quantity4 = '',quantity5 = '',quantity6 = '',quantity7 = '',price1 = '',price2 = '',price3 = '',price4 = '',price5 = '',price6 = '',price7 = '',sum1 = '',sum2 = '',sum3 = '',sum4 = '',sum5 = '',sum6 = '',sum7 = '';
      if(workOrderMaterials[0]){
        workOrderMaterials = workOrderMaterials[0]
        if(workOrderMaterials.name1) name1 = workOrderMaterials.name1;
        if(workOrderMaterials.name2) name2 = workOrderMaterials.name2;
        if(workOrderMaterials.name3) name3 = workOrderMaterials.name3;
        if(workOrderMaterials.name4) name4 = workOrderMaterials.name4;
        if(workOrderMaterials.name5) name5 = workOrderMaterials.name5;
        if(workOrderMaterials.name6) name6 = workOrderMaterials.name6;
        if(workOrderMaterials.name7) name7 = workOrderMaterials.name7;
        if(workOrderMaterials.quantity1) quantity1 = workOrderMaterials.quantity1;
        if(workOrderMaterials.quantity2) quantity2 = workOrderMaterials.quantity2;
        if(workOrderMaterials.quantity3) quantity3 = workOrderMaterials.quantity3;
        if(workOrderMaterials.quantity4) quantity4 = workOrderMaterials.quantity4;
        if(workOrderMaterials.quantity5) quantity5 = workOrderMaterials.quantity5;
        if(workOrderMaterials.quantity6) quantity6 = workOrderMaterials.quantity6;
        if(workOrderMaterials.quantity7) quantity7 = workOrderMaterials.quantity7;
        if(workOrderMaterials.price1) price1 = workOrderMaterials.price1;
        if(workOrderMaterials.price2) price2 = workOrderMaterials.price2;
        if(workOrderMaterials.price3) price3 = workOrderMaterials.price3;
        if(workOrderMaterials.price4) price4 = workOrderMaterials.price4;
        if(workOrderMaterials.price5) price5 = workOrderMaterials.price5;
        if(workOrderMaterials.price6) price6 = workOrderMaterials.price6;
        if(workOrderMaterials.price7) price7 = workOrderMaterials.price7;
        if(workOrderMaterials.sum1) sum1 = workOrderMaterials.sum1;
        if(workOrderMaterials.sum2) sum2 = workOrderMaterials.sum2;
        if(workOrderMaterials.sum3) sum3 = workOrderMaterials.sum3;
        if(workOrderMaterials.sum4) sum4 = workOrderMaterials.sum4;
        if(workOrderMaterials.sum5) sum5 = workOrderMaterials.sum5;
        if(workOrderMaterials.sum6) sum6 = workOrderMaterials.sum6;
        if(workOrderMaterials.sum7) sum7 = workOrderMaterials.sum7;
      }
      //SIGN REPRESENTATIVE
      var sign = '';
      dateFormat(workOrder.date, "dd.mm.yyyy")
      var signElement = [];
      if(workOrderSigns[0]){
        //need to open image of sign and encode to base64 because of pdfmake
        var file = `${__dirname}/../files/workorders/signs/${workOrderSigns[0].path_name}`;
        //console.log(file);
        if (fs.existsSync(file)) {
          // Do something
          let imageAsBase64 = fs.readFileSync(file, 'base64');
          sign = 'data:image/png;base64,' + imageAsBase64;
          signElement = [{
            image: 'podpis',
            absolutePosition: {x: 470, y: 755},
            fit: [100, 60],
          }]
        }
      }
      var docDefinition = preparePDFWorkOrder(woName, woSubscriber, woProject, userName, sign, signElement, workOrder, typeOne, typeTwo, typeThree, typeFour, typeFive, typeSix, typeSeven, workers, timeQuantity, timePrice, timeSum, distanceQunatity, distancePrice, distanceSum, materialQuantity, materialPrice, materialSum, otherQuantity, otherPrice, otherSum, name1, name2, name3, name4, name5, name6, name7, quantity1, quantity2, quantity3, quantity4, quantity5, quantity6, quantity7, price1, price2, price3, price4, price5, price6, price7, sum1, sum2, sum3, sum4, sum5, sum6, sum7, robotehImg, kukaPartnerImg);
      //res.json({workOrder:workOrder[0]});
      var pdfDoc = printer.createPdfKitDocument(docDefinition);
      let pdfHeader = 'Delovni nalog'+woName;
      const filename = encodeURI(pdfHeader);
      res.setHeader('Content-disposition', `attachment; filename*=UTF-8''${filename}.pdf; filename=${filename}.pdf`);
      res.setHeader('Content-type', 'application/pdf');
      pdfDoc.pipe(res);
      pdfDoc.end();

    })
    .catch((e)=>{
      console.log(e);
      return res.status(500).json({
        status: 'error',
        message: 'Something went wrong',
        error: e,
      })
    })
  }
})

function preparePDFWorkOrder(woName, woSubscriber, woProject, userName, sign, signElement, workOrder, typeOne, typeTwo, typeThree, typeFour, typeFive, typeSix, typeSeven, workers, timeQuantity, timePrice, timeSum, distanceQunatity, distancePrice, distanceSum, materialQuantity, materialPrice, materialSum, otherQuantity, otherPrice, otherSum, name1, name2, name3, name4, name5, name6, name7, quantity1, quantity2, quantity3, quantity4, quantity5, quantity6, quantity7, price1, price2, price3, price4, price5, price6, price7, sum1, sum2, sum3, sum4, sum5, sum6, sum7, robotehImg, kukaPartnerImg) {
  if( workOrder.arrival == null ) workOrder.arrival = '/';
  if( workOrder.departure == null ) workOrder.departure = '/';
  return {
    info: {
      title: woName,
      author: 'Roboteh-IS',
      subject: 'Delovni nalog',
      keywords: woName + ", " + woSubscriber,
    },
    pageSize: 'A4',
    pageOrientation: 'portrait',
    header: {
      columns: [
        {
          style: 'noga',
          table: {
            widths: [280, 130, 105, 100],
            body: [
              [
                { text: '', color: 'white', alignment: 'center', fillColor: 'black' },
                { text: '', color: 'white', alignment: 'center', fillColor: 'black' },
                { text: '', preserveLeadingSpaces: true, color: 'white', alignment: 'center', fillColor: 'black' },
                { text: ' ', preserveLeadingSpaces: true, color: 'white', alignment: 'center', fillColor: 'black' },
              ],
            ]
          },
        },
      ]
    },
    footer: {
      columns: [
        {
          style: 'noga',
          table: {
            widths: [350,130,45,75],
            body: [
              [
                { text: '*Podpisan izvod velja kot dobavnica', color: 'white', alignment: 'center', fillColor: 'black' },
                { text: 'Hvala za zaupanje!', color: 'white', alignment: 'center', fillColor: 'black' },
                { text: '', preserveLeadingSpaces: true, color: 'white', alignment: 'center', fillColor: 'black' },
                { text: 'Obr.: št. 01/1', preserveLeadingSpaces: true, color: 'white', alignment: 'center', fillColor: 'black' },
              ],
            ]
          },
        },
      ]
    },
    content: [
      signElement,
      {
        style: 'tableRoboteh',
        table: {
          widths: [200],
          body: [ [ { image: 'roboteh', width: 200 }, ] ]
        }, layout: 'noBorders'
      },
      {
        style: 'tableKontaktHead',
        table: {
          widths: [200],
          body: [ [ { text: 'ROBOTEH', fontSize: 16, preserveLeadingSpaces: true }, ], ]
        }, layout: 'noBorders'
      },
      {
        style: 'tableKontakt',
        table: {
          widths: [200, 50, 240, 100],
          body: [
            [ 
              [
                { text: '                                     D.O.O', style: 'kontakt', preserveLeadingSpaces: true },
                { text: 'GORIČICA 2B, SI-3230 ŠENTJUR, SLOVENIA-EU', style: 'kontakt', preserveLeadingSpaces: true },
                { text: 'ID za DDV: SI24773832', style: 'kontakt', preserveLeadingSpaces: true },
                { text: 'Tel: +386 (0)3 746 42 44', style: 'kontakt' },
                { text: 'Mobile: +386 51 645 205 ', style: 'kontakt' },
                { text: 'E-mail: office@roboteh.si', style: 'kontakt' },
                { text: 'WWW.ROBOTEH.SI', style: 'kontakt' },

              ]
            ],
          ]
        }, layout: 'noBorders'
      },
      {
        style: 'tableKuka',
        table: {
          widths: [200],
          body: [ [ { image: 'kuka', width: 50 }, ] ]
        }, layout: 'noBorders'
      },
      {
        style: 'crta1',
        table: { widths: [600], body: [[" "], [" "]] },
        layout: {
          hLineWidth: function (i, node) { return (i === 0 || i === node.table.body.length) ? 0 : 2; },
          vLineWidth: function (i, node) { return 0; },
        }
      },
      {
        style: 'crta2',
        table: { widths: [600], body: [[" "], [" "]] },
        layout: {
          hLineWidth: function (i, node) { return (i === 0 || i === node.table.body.length) ? 0 : 2; },
          vLineWidth: function (i, node) { return 0; },
        }
      },
      {
        style: 'crta3',
        table: {
          widths: [600],
          body: [[" "], [" "]]
        },
        layout: {
          hLineWidth: function (i, node) {
            return (i === 0 || i === node.table.body.length) ? 0 : 2;
          },
          vLineWidth: function (i, node) {
            return 0;
          },
        }
      },
      {
        style: 'header',
        table: {
          body: [ [{ text: 'DELOVNI NALOG', preserveLeadingSpaces: true },] ]
        },
        layout: 'noBorders'
      },
      {
        style: 'infotable',
        table: {
          widths: [16, 90, 100, 37, 92],
          body: [
            [
              { rowSpan: 2, border: [false, false, false, false], text: 'št.: ', },
              { border: [false, false, false, true], text: woName, preserveLeadingSpaces: true },
              { rowSpan: 2, border: [false, false, false, false], text: ' ', preserveLeadingSpaces: true },
              { border: [false, false, false, false], text: 'Datum: ' },
              { border: [false, false, false, true], text: dateFormat(workOrder.date, "dd.mm.yyyy") }
            ],
            [
              { border: [false, false, false, false], text: '    1 = servis,   2 = programiranje,    3 = montaža,    4 = zagon,    5 = storitev,   6 = svetovanje,   7 = pomoč', preserveLeadingSpaces: true },
              { border: [false, false, false, false], text: '', preserveLeadingSpaces: true },
              { rowSpan: 2, border: [false, false, false, false], text: ' št.:', preserveLeadingSpaces: true },
              { border: [false, false, false, false], text: 'Kraj:' },
              { border: [false, false, false, true], text: workOrder.location }
            ]
          ]
        },
        layout: { defaultBorder: true, }
      },
      {
        style: 'typestable',
        table: {
          widths: [50, 90, 66, 60, 65, 70, 65],
          body: [
            [
              { border: [true, true, false, true], text: '1 = servis,', alignment: 'center', preserveLeadingSpaces: true, fillColor: typeOne },
              { border: [false, true, false, true], text: '2 = programiranje,', alignment: 'center', preserveLeadingSpaces: true, fillColor: typeTwo },
              { border: [false, true, false, true], text: '3 = montaža,', alignment: 'center', preserveLeadingSpaces: true, fillColor: typeThree },
              { border: [false, true, false, true], text: '4 = zagon,', alignment: 'center', preserveLeadingSpaces: true, fillColor: typeFour },
              { border: [false, true, false, true], text: '5 = storitev,', alignment: 'center', preserveLeadingSpaces: true, fillColor: typeFive },
              { border: [false, true, false, true], text: '6 = svetovanje,', alignment: 'center', preserveLeadingSpaces: true, fillColor: typeSix },
              { border: [false, true, true, true], text: '7 = pomoč', alignment: 'center', preserveLeadingSpaces: true, fillColor: typeSeven },
            ]
          ]
        },
        layout: { defaultBorder: false, }
      },
      {
        style: 'zacetek',
        table: {
          widths: [520],
          body: [ [ { border: [true, true, true, false], text: ' ', }, ], ]
        },
        layout: { defaultBorder: true, }
      },
      {
        style: 'subproject',
        table: {
          widths: [58, 370, 74],
          body: [
            [
              { border: [true, false, false, false], text: 'NAROČNIK: ', },
              { border: [false, false, false, true], text: woSubscriber, preserveLeadingSpaces: true },
              { rowSpan: 2, border: [false, false, true, false], text: ' ', preserveLeadingSpaces: true },
            ],
            [
              { border: [true, false, false, false], text: 'PROJEKT:', preserveLeadingSpaces: true },
              { border: [false, false, false, true], text: woProject, preserveLeadingSpaces: true },
              { rowSpan: 2, border: [false, false, false, false], text: ' ', preserveLeadingSpaces: true },
            ]
          ]
        },
        layout: { defaultBorder: true, }
      },
      {
        style: 'description',
        table: {
          widths: [141, 370, 2],
          body: [
            [
              { border: [true, false, false, false], text: 'OPIS OPRAVLJENEGA DELA: ', },
              { border: [false, false, true, false], text: '', preserveLeadingSpaces: true },
              { border: [false, false, false, false], text: ' ', preserveLeadingSpaces: true },
            ],
            [
              { colSpan: 2, rowSpan: 6, border: [true, false, true, false], text: workOrder.description, preserveLeadingSpaces: true },
              { text: ' ', preserveLeadingSpaces: true },
              { border: [false, false, false, false], text: ' ', preserveLeadingSpaces: true },
            ],
            [
              { colSpan: 2, text: ' ', preserveLeadingSpaces: true },
              { text: ' ', preserveLeadingSpaces: true },
              { border: [false, false, false, false], text: ' ', preserveLeadingSpaces: true },
            ],
            [
              { colSpan: 2, text: ' ', preserveLeadingSpaces: true },
              { text: ' ', preserveLeadingSpaces: true },
              { border: [false, false, false, false], text: ' ', preserveLeadingSpaces: true },
            ],
            [
              { colSpan: 2, text: ' ', preserveLeadingSpaces: true },
              { text: ' ', preserveLeadingSpaces: true },
              { border: [false, false, false, false], text: ' ', preserveLeadingSpaces: true },
            ],
            [
              { colSpan: 2, text: ' ', preserveLeadingSpaces: true },
              { text: ' ', preserveLeadingSpaces: true },
              { border: [false, false, false, false], text: ' ', preserveLeadingSpaces: true },
            ],
            [
              { colSpan: 2, text: ' ', preserveLeadingSpaces: true },
              { text: ' ', preserveLeadingSpaces: true },
              { border: [false, false, false, false], text: ' ', preserveLeadingSpaces: true },
            ]
          ]
        },
      },
      {
        style: 'workers',
        table: {
          widths: [74, 306, 122],
          body: [
            [
              { border: [true, false, false, false], text: 'Izvajalec naloge: ', },
              { border: [false, false, false, true], text: workers, preserveLeadingSpaces: true },
              { rowSpan: 2, border: [false, false, true, false], text: ' ', preserveLeadingSpaces: true },
            ],
            [
              { border: [true, false, false, false], text: 'Podatki o vozilu:', preserveLeadingSpaces: true },
              { border: [false, false, false, true], text: workOrder.vehicle, preserveLeadingSpaces: true },
              { rowSpan: 2, border: [false, false, false, false], text: ' ', preserveLeadingSpaces: true },
            ]
          ]
        },
        layout: { defaultBorder: true, }
      },
      {
        style: 'time',
        table: {
          widths: [56, 80, 20, 56, 80, 183],
          body: [
            [
              { border: [true, false, false, false], text: 'Čas prihoda: ', },
              { border: [false, false, false, true], text: workOrder.arrival.substring(0,5), preserveLeadingSpaces: true },
              { border: [false, false, false, false], text: ' ', preserveLeadingSpaces: true },
              { border: [false, false, false, false], text: 'Čas odhoda: ', },
              { border: [false, false, false, true], text: workOrder.departure.substring(0,5), preserveLeadingSpaces: true },
              { border: [false, false, true, false], text: ' ', preserveLeadingSpaces: true },
            ],
          ]
        },
        layout: { defaultBorder: true, }
      },
      {
        style: 'konec',
        table: {
          widths: [520],
          body: [ [ { border: [true, false, true, true], text: ' ', }, ], ]
        },
        layout: { defaultBorder: true, }
      },
      {
        style: 'stroskiheader',
        table: {
          widths: [333],
          body: [ [ { text: 'STROŠKI DELA, PREVOZ, OSTALO: ', fillColor: 'silver' }, ], ]
        },
      },
      {
        style: 'stroskiheaderPlus',
        table: {
          widths: [50, 50, 60],
          body: [
            [
              { text: 'Količina', alignment: 'center', fillColor: 'silver' },
              { text: 'Cena', alignment: 'center', fillColor: 'silver' },
              { text: 'Znesek', alignment: 'center', fillColor: 'silver' },
            ],
          ]
        },
      },
      {
        style: 'stroski',
        table: {
          widths: [333, 50, 50, 60],
          body: [
            [
              { text: 'Porabljen čas dela (h):', },
              { text: timeQuantity, alignment: 'center', preserveLeadingSpaces: true },
              { text: timePrice, alignment: 'center', preserveLeadingSpaces: true },
              { text: timeSum, alignment: 'center', preserveLeadingSpaces: true },
            ],
            [
              { text: 'Prevoženi kilometri - relacija (km):', preserveLeadingSpaces: true },
              { text: distanceQunatity, alignment: 'center', preserveLeadingSpaces: true },
              { text: distancePrice, alignment: 'center', preserveLeadingSpaces: true },
              { text: distanceSum, alignment: 'center', preserveLeadingSpaces: true },
            ],
            [
              { text: 'Materialni stroški:', preserveLeadingSpaces: true },
              { text: materialQuantity, alignment: 'center', preserveLeadingSpaces: true },
              { text: materialPrice, alignment: 'center', preserveLeadingSpaces: true },
              { text: materialSum, alignment: 'center', preserveLeadingSpaces: true },
            ],
            [
              { text: 'Ostalo:', preserveLeadingSpaces: true },
              { text: otherQuantity, alignment: 'center', preserveLeadingSpaces: true },
              { text: otherPrice, alignment: 'center', preserveLeadingSpaces: true },
              { text: otherSum, alignment: 'center', preserveLeadingSpaces: true },
            ]
          ]
        },
        layout: { defaultBorder: true, }
      },
      {
        style: 'materialheader',
        table: {
          widths: [520],
          body: [ [ { text: 'MATERIAL: ', fillColor: 'silver' }, ], ]
        },
      },
      {
        style: 'material',
        table: {
          widths: [34, 290, 50, 50, 60],
          body: [
            [
              { text: 'Zap. št.', },
              { text: 'Naziv materiala', preserveLeadingSpaces: true },
              { text: 'Količina', alignment: 'center', preserveLeadingSpaces: true },
              { text: 'Cena', alignment: 'center', preserveLeadingSpaces: true },
              { text: 'Znesek', alignment: 'center', preserveLeadingSpaces: true },
            ],
            [
              { text: '1.', preserveLeadingSpaces: true },
              { text: name1, preserveLeadingSpaces: true },
              { text: quantity1, preserveLeadingSpaces: true },
              { text: price1, preserveLeadingSpaces: true },
              { text: sum1, preserveLeadingSpaces: true },
            ],
            [
              { text: '2.', preserveLeadingSpaces: true },
              { text: name2, preserveLeadingSpaces: true },
              { text: quantity2, preserveLeadingSpaces: true },
              { text: price2, preserveLeadingSpaces: true },
              { text: sum2, preserveLeadingSpaces: true },
            ],
            [
              { text: '3.', preserveLeadingSpaces: true },
              { text: name3, preserveLeadingSpaces: true },
              { text: quantity3, preserveLeadingSpaces: true },
              { text: price3, preserveLeadingSpaces: true },
              { text: sum3, preserveLeadingSpaces: true },
            ],
            [
              { text: '4.', preserveLeadingSpaces: true },
              { text: name4, preserveLeadingSpaces: true },
              { text: quantity4, preserveLeadingSpaces: true },
              { text: price4, preserveLeadingSpaces: true },
              { text: sum4, preserveLeadingSpaces: true },
            ],
            [
              { text: '5.', preserveLeadingSpaces: true },
              { text: name5, preserveLeadingSpaces: true },
              { text: quantity5, preserveLeadingSpaces: true },
              { text: price5, preserveLeadingSpaces: true },
              { text: sum5, preserveLeadingSpaces: true },
            ],
            [
              { text: '6.', preserveLeadingSpaces: true },
              { text: name6, preserveLeadingSpaces: true },
              { text: quantity6, preserveLeadingSpaces: true },
              { text: price6, preserveLeadingSpaces: true },
              { text: sum6, preserveLeadingSpaces: true },
            ],
            [
              { text: '7.', preserveLeadingSpaces: true },
              { text: name7, preserveLeadingSpaces: true },
              { text: quantity7, preserveLeadingSpaces: true },
              { text: price7, preserveLeadingSpaces: true },
              { text: sum7, preserveLeadingSpaces: true },
            ]
          ]
        },
        layout: { defaultBorder: true, }
      },
      {
        style: 'podpis',
        table: {
          widths: [81, 110, 35, 137, 110, 20],
          heights: ['auto', 40],
          body: [
            [
              { border: [false, false, false, false], text: 'Izvajalec naloge:' },
              { border: [false, false, false, true], text: userName, preserveLeadingSpaces: true },
              { border: [false, false, false, false], text: ' ', preserveLeadingSpaces: true },
              { border: [false, false, false, false], text: 'Pooblaščenec za naročnika:', },
              { border: [false, false, false, true], text: workOrder.representative, preserveLeadingSpaces: true },
              { border: [false, false, false, false], text: ' ', preserveLeadingSpaces: true },
            ],
            [
              { border: [false, false, false, false], text: '', },
              { border: [false, false, false, true], text: '', preserveLeadingSpaces: true },
              { border: [false, false, false, false], text: ' ', preserveLeadingSpaces: true },
              { border: [false, false, false, false], text: '', },
              { border: [false, false, false, true], text: '', preserveLeadingSpaces: true },
              { border: [false, false, false, false], text: ' ', preserveLeadingSpaces: true },
            ],
          ]
        },
        layout: { defaultBorder: true, }
      },
    ],
    images: {
      podpis: sign,
      roboteh: robotehImg.base64,
      kuka: kukaPartnerImg.base64,
    },
    styles: {
      header: { fontSize: 16, bold: true, margin: [10, -25, 0, 10] },
      kontaktHeader: { fontSize: 13, bold: false, margin: [0, 0, 0, 5] },
      kontaktTable: { fontSize: 8, bold: false, margin: [0, 20, 0, 5] },
      kontakt: { fontSize: 8, bold: false, margin: [0, -6, 0, 5] },
      tableExample: { margin: [-30, -15, 0, 15] },
      tableRoboteh: { margin: [10, -15, 0, 15] },
      tableKontaktHead: { margin: [ 300, -85, 0, 15] },
      tableKontakt: { margin: [ 300, -25, 0, 15] },
      tableKuka: { margin: [490, -87, 0, 15] },
      subproject: { fontSize: 11, margin: [10, -27, 0, 15] },
      description: { fontSize: 11, margin: [10, -15, 0, 15] },
      workers: { fontSize: 10, margin: [10, -15, 0, 15] },
      time: { fontSize: 10, margin: [10, -15, 0, 15] },
      konec: { margin: [10, -26, 0, 15] },
      zacetek: { margin: [10, 0, 0, 15] },
      stroskiheader: { fontSize: 11, bold: true, margin: [10, 0, 0, 10] },
      stroskiheaderPlus: { fontSize: 11, bold: false, margin: [352, -29, 0, 10] },
      stroski: { fontSize: 10, margin: [10, -11, 0, 15] },
      materialheader: { fontSize: 11, bold: true, margin: [10, 0, 0, 10] },
      material: { fontSize: 10, margin: [10, -11, 0, 15], alignment: 'center', },
      podpis: { fontSize: 11, margin: [10, 20, 0, 15] },
      noga: { fontSize: 10, margin: [-40, 1, 0, 15] },
      glava: { fontSize: 10, margin: [0, 0, 0, 15] },
      infotable: { fontSize: 11, margin: [130, -31, 0, 15] },
      typestable: { fontSize: 10, margin: [10, 0, 0, 15] },
      crta1: { margin: [ -40, -25, 0, 15] },
      crta2: { margin: [ -40, -52, 0, 15] },
      crta3: { margin: [ -40, -52, 0, 15] },
      tableHeader: { bold: true, fontSize: 13, color: 'black' }
    },
  };
}
module.exports = router;