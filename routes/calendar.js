var express = require('express');
var router = express.Router();
var dateFormat = require('dateformat');
const csp = require('helmet-csp');

//auth & dbModels
var auth = require('../controllers/authentication');
var dbCalendar = require('../model/calendar/dbCalendar');

var normalCspHandler = csp({
  directives: {
    defaultSrc: ["'self'"],
    scriptSrc: ["'self'"],
    styleSrc: ["'self'", "https://fonts.googleapis.com", "https://use.fontawesome.com"],
    fontSrc: ["'self'", "https://fonts.gstatic.com", "https://use.fontawesome.com"],
    imgSrc: ["'self'", "data:"],
    frameAncestors: ["'self'"],
  }
});
var mobiscrollCspHandler = csp({
  directives: {
    defaultSrc: ["'self'"],
    scriptSrc: ["'self'",],
    styleSrc: ["'self'", "https://fonts.googleapis.com", "https://use.fontawesome.com"],
    fontSrc: ["'self'", "https://fonts.gstatic.com", "https://use.fontawesome.com"],
    imgSrc: ["'self'", "data:"],
    frameAncestors: ["'self'"],
  }
});

//functions
function formatDate(date){
  return {
    "date": dateFormat(date, "dd.mm.yyyy"),
    "time": dateFormat(date, "HH:MM")
  }
}
function addFormatedDateForTasks(tasks){
  //console.log("test");
  for(let i=0; i < tasks.length; i++) {
    if(tasks[i] && tasks[i].task_start)
      tasks[i]["formatted_start"] = formatDate(tasks[i].task_start);
    if(tasks[i] && tasks[i].task_finish)
      tasks[i]["formatted_finish"] = formatDate(tasks[i].task_finish);
  }
  return tasks;
}

router.get('/', normalCspHandler, auth.authenticate, function(req, res, next){
  //console.log(req);
  dbCalendar.getCalendarUsers(req.session.type).then(users => {
    res.render('calendar_overview', {
      title: "Tedenski pregled",
      user_name: req.session.user_name,
      user_surname: req.session.user_surname,
      user_username: req.session.user_username,
      user_typeid: req.session.role_id,
      user_type: req.session.type,
      user_id: req.session.user_id,
      users: users
    })
  })
  .catch((e)=>{
    next(e);
  })
})
//calendar using mobiscroll library
router.get('/mobiscroll', normalCspHandler, auth.authenticate, function(req, res, next){
  //console.log(req);
  res.render('calendar-mobiscroll', {
    title: "Mobiscroll koledar",
    user_name: req.session.user_name,
    user_surname: req.session.user_surname,
    user_username: req.session.user_username,
    user_typeid: req.session.role_id,
    user_type: req.session.type,
    user_id: req.session.user_id,
  })
})

//dismiss selected change
router.get('/tasks', auth.authenticate, function(req, res, next){
  let date = req.query.date;
  let category = req.query.category;
  let selectedProject = req.query.selectedProject;
  let seeAll = req.query.seeAll;
  let count = 2;
  if(req.query.weekCount)
    count = parseInt(req.query.weekCount);
  if(seeAll == 'true')
    seeAll = true;
  else
    seeAll = false;
  date = new Date(date);
  let d,m,y;
  if( ( date.getDate() ) < 10 ) d = '0' + ( date.getDate() ); else d = date.getDate();
  if( ( date.getMonth() + 1 ) < 10 ) m = '0' + ( date.getMonth() + 1 ); else m = ( date.getMonth() + 1 );
  y = date.getFullYear();
  date = y + '-' + m + '-' + d;
  var userId;
  var role = req.session.type.toLowerCase();
  if(role.substring(0,4) == 'info'){
    //info category
    if(role.substring(5,11) == 'nabava') category = 2;
    else if(role.substring(5,17) == 'konstrukcija') category = 3;
    else if(role.substring(5,12) == 'strojna') category = 4;
    else if(role.substring(5,12) == 'elektro') category = 5;
    else if(role.substring(5,12) == 'montaža') category = 10;
    else if(role.substring(5,18) == 'programiranje' || role.substring(5,22) == 'programer plc-jev' || role.substring(5,22) == 'programer robotov') category = 7;
  }
  else if(role.substring(0,5) != 'vodja' && role.substring(0,5) != 'admin' && role != 'tajnik' && role != 'komercialist' && role != 'komerciala' && role != 'računovodstvo')
    userId = req.session.user_id;
  dbCalendar.getCalendarTasksFor2Weeks(date,category,selectedProject,seeAll,count,userId).then(activeTasks => {
    dbCalendar.getExpiredTasks(date,category,selectedProject,userId).then(expiredTasks => {
      addFormatedDateForTasks(activeTasks);
      res.json({success:true, activeTasks:activeTasks, expiredTasks:expiredTasks});
    })
    .catch((e)=>{
      return res.json({success:false, error:e});
    })
  })
  .catch((e)=>{
    return res.json({success:false, error:e});
  })
})

module.exports = router;