var express = require('express');
var router = express.Router();
var dateFormat = require('dateformat');
const fs = require('fs');
var Jimp = require('jimp');

// Define font files
var fonts = {
  Roboto: {
    normal: `${__dirname}/../public/pdfmake/Roboto-Regular.ttf`,
    bold: `${__dirname}/../public/pdfmake/Roboto-Medium.ttf`,
    italics: `${__dirname}/../public/pdfmake/Roboto-Italic.ttf`,
    bolditalics: `${__dirname}/../public/pdfmake/Roboto-MediumItalic.ttf`,
  }
};
var PdfPrinter = require('pdfmake');
var printer = new PdfPrinter(fonts);

let auth = require('../controllers/authentication');
let dbComplaints = require('../model/complaints/dbComplaints');
let dbBase64Images = require('../model/base64images/base64images');

function formatDate(date){
  return {
    "date": dateFormat(date, "dd. mm. yyyy"),
    "time": dateFormat(date, "HH:MM")
  }
}

//return pdf for list of projects based on filters (default: all active projects)
router.get('/', auth.authenticate, function(req, res, next){
  let compSubId = req.query.compSubId;
  let compSupId = req.query.compSupId;

  if (compSubId){
    // return pdf of complaint subscriber
    let promises = [];
    promises.push(dbComplaints.getComplaintsSubscriberId(compSubId));
    promises.push(dbComplaints.getComplaintsSubscriberAuthor(compSubId));
    //get roboteh base64 image
    promises.push(dbBase64Images.getRobotehImage());
    
    Promise.all(promises).then(results => {
      let compSub = results[0];
      let author = results[1];
      let base64RobotehImg = results[2];
      let authorLabel = author.name + ' ' + author.surname;
      let dateLabel = dateFormat(new Date(author.date), "dd. mm. yyyy");
      let errorMechanic = compSub.error_type_id == 1 ? '#000000' : '#ffffff';
      let errorElectric = compSub.error_type_id == 2 ? '#000000' : '#ffffff';
      let errorProgram = compSub.error_type_id == 3 ? '#000000' : '#ffffff';
      let statusOpen = compSub.status_id == 1 ? '#000000' : '#ffffff';
      let statusDeclined = compSub.status_id == 2 ? '#000000' : '#ffffff';
      let statusRunning = compSub.status_id == 3 ? '#000000' : '#ffffff';
      let statusClosed = compSub.status_id == 4 ? '#000000' : '#ffffff';
      let pdfHeader = 'Reklamacija naročniku ' + compSub.complaint_number;
      let documentNumberLabel = 'Številka reklamacije:  ' + compSub.complaint_number;
      let subscriberLabel = compSub.subscriber ? compSub.subscriber : ' ';
      let contactLabel = compSub.subscriber_contact ? compSub.subscriber_contact : ' ';
      let projectLabel = compSub.project_number ? (compSub.project_number + ' - ' + compSub.project_name) : ' ';
      let descriptionLabel = compSub.description ? compSub.description : ' ';
      let solverLabel = compSub.solver ? compSub.solver : ' ';
      let causeLabel = compSub.cause ? compSub.cause : ' ';
      let measureLabel = compSub.measure ? compSub.measure : ' ';
      let finishedLabel = compSub.finished ? dateFormat(new Date(compSub.finished), "dd. mm. yyyy") : ' ';
      let justificationLabel = compSub.justification ? compSub.justification : ' ';
      let costLabel = compSub.cost ? compSub.cost : ' ';
      let notesLabel = compSub.notes ? compSub.notes : ' ';
      
      let dd = {
        pageMargins: [60, 110, 50, 70],
        header: [
          {
            style: 'headerTable',
            table: {
              widths: [150,340],
              body: [
                [
                  {
                    border: [false,false,false,false],
                    image: 'roboteh',
                    width: 150
                  },
                  {
                    style: 'obrInfo',
                    table: {
                      widths: [175, 60, 70],
                      body: [
                        [
                          { border: [true,false,true,true], text: [ 'Identifikacijska številka:    ', {text: 'OBR 02.04', bold: true}, ], },
                          { border: [true,false,true,true], text: [ 'Izdaja:  ', {text: '01', bold: true}, ] },
                          { border: [true,false,true,true], alignment: 'center', text: [ 'Stran ', {text: '1', bold: true}, ' od ', {text: '1', bold: true}, ] },
                        ],
                        [
                          { border: [true,true,true,false], colSpan: 3, text: [ 'Naziv dokumenta: ', {text: '  REKLAMACIJA NAROČNIKA', bold: true}, ], },
                        ]
                      ]
                    },
                  },
                ]
              ]
            }, layout: { defaultBorder: false, }
          },
        ],
        content: [
          {
            style: 'mainTable',
            table: {
              widths: [110,'*',140],
              heights: ['*', 17, '*', 17, 17, 17, '*', 240],
              body: [
                [
                  { colSpan: 3, fontSize: 11, fillColor: '#eeeeee', text: [ {text: '  REKLAMACIJA NAROČNIKA', bold: true}, ], },
                  {},
                  {},
                ],
                [
                  { colSpan: 3, text: [ {text: documentNumberLabel, bold: true}, ], margin: [0,3,0,0], },
                  {},
                  {},
                ],
                [
                  { colSpan: 3, fillColor: '#eeeeee', text: ' ' }, 
                  {},
                  {},
                ],
                [
                  { text: [ {text: 'Naročnik:', bold: true}, ], margin: [0,3,0,0], },
                  { preserveLeadingSpaces: true, colSpan: 2, text: subscriberLabel, margin: [0,3,0,0], },
                  { text: 'test', alignment: 'center', preserveLeadingSpaces: true },
                ],
                [
                  { text: [ {text: 'Kontaktna oseba:', bold: true}, ], margin: [0,3,0,0], },
                  { colSpan: 2, preserveLeadingSpaces: true, text: contactLabel, margin: [0,3,0,0], },
                  { text: 'test', alignment: 'center', preserveLeadingSpaces: true },
                ],
                [
                  { text: [ {text: 'Projekt:', bold: true}, ], margin: [0,3,0,0], },
                  { colSpan: 2, preserveLeadingSpaces: true, text: projectLabel, margin: [0,3,0,0], },
                  { text: 'test', alignment: 'center', preserveLeadingSpaces: true },
                ],
                [
                  { colSpan: 3, fillColor: '#eeeeee', alignment: 'center', text: { text: 'Opis reklamacije - napaka', bold: true } },
                  {},
                  {},
                ],
                [
                  { colSpan: 3, text: descriptionLabel, preserveLeadingSpaces: true },
                  {},
                  {},
                ],
                [
                  { colSpan: 3, fillColor: '#eeeeee', alignment: 'center', text: { text:'Vrsta napake', bold: true } },
                  {},
                  {},
                ],
                [
                  {
                    colSpan:3,
                    stack: [
                      {
                        table: {
                          widths: [80,15,'*',10,15,50,10,15,'*',80],
                          body: [
                            [
                              { border: [false,false,false,false], text: '' },
                              {
                                border: [false,false,false,false],
                                stack: [{
                                  table: {
                                    widths: [7],
                                    heights: [8],
                                    body: [[{
                                      margin: [-2,0,0,0],
                                      table: {
                                        widths: [1],
                                        heights: [5],
                                        body: [ [ { border: [false,false,false,false], fillColor: errorMechanic, text: '' } ], ]
                                      }
                                    }]]
                                  }
                                },]
                              },
                              { border: [false,false,false,false], alignment: 'center', text: 'Mehanska', margin: [0,3,0,0], },
                              { border: [false,false,false,false], text: '' },
                              {
                                border: [false,false,false,false],
                                stack: [{
                                  table: {
                                    widths: [7],
                                    heights: [8],
                                    body: [[{
                                      margin: [-2,0,0,0],
                                      table: {
                                        widths: [1],
                                        heights: [5],
                                        body: [ [{ border: [false,false,false,false], fillColor: errorElectric, text: '' } ], ]
                                      }
                                    }]]
                                  }
                                }]
                              },
                              { border: [false,false,false,false], alignment: 'center', text: 'Elektro', margin: [0,3,0,0], },
                              { border: [false,false,false,false], text: '' },
                              {
                                border: [false,false,false,false],
                                stack: [{
                                  table: {
                                    widths: [7],
                                    heights: [8],
                                    body: [[{
                                      margin: [-2,0,0,0],
                                      table: {
                                        widths: [1],
                                        heights: [5],
                                        body: [ [ { border: [false,false,false,false], fillColor: errorProgram, text: '' } ], ]
                                      }
                                    }],]
                                  }
                                },]
                              },
                              { border: [false,false,false,false], alignment: 'center', text: 'Programska', margin: [0,3,0,0], },
                              { border: [false,false,false,false], text: '' },
                            ],
                          ]
                        },
                      }
                    ]
                  },
                  {},
                  {},
                ],
                [
                  { colSpan: 3, fillColor: '#eeeeee', alignment: 'center', text: ' ' },
                  {},
                  {},
                ],
                [
                  { text: 'Napotena oseba za reševanje reklamacije:' },
                  { colSpan: 2, text: solverLabel, },
                  {},
                ],
                [
                  { text: 'Vzrok:' },
                  { colSpan: 2, text: causeLabel, },
                  {},
                ],
                [
                  { text: 'Ukrep za odpravo vzroka:' },
                  { colSpan: 2, text: measureLabel, },
                  {},
                ],
                [
                  { text: 'Datum zaključka:' },
                  { colSpan: 2, text: finishedLabel, },
                  {},
                ],
                [
                  { text: 'Upravičenost (ukrep):' },
                  { colSpan: 2, text: justificationLabel, },
                  {},
                ],
                [
                  { text: 'Strošek:' },
                  { colSpan: 2, text: costLabel, },
                  {},
                ],
                [
                  { text: 'Opombe:' },
                  { colSpan: 2, text: notesLabel, },
                  {},
                ],
                [
                  { colSpan: 3, fillColor: '#eeeeee', alignment: 'center', text: { text: 'Status', bold: true } },
                  {},
                  {},
                ],
                [
                  {
                    colSpan:3,
                    stack: [{
                      table: {
                        widths: [40,15,'*',10,15,'*',10,15,'*',10,15,'*',40],
                        body: [
                          [
                            { border: [false,false,false,false], text: '' },
                            { 
                              border: [false,false,false,false],
                              stack: [
                                {
                                  table: {
                                    widths: [7],
                                    heights: [8],
                                    body: [[{
                                      margin: [-2,0,0,0],
                                      table: {
                                        widths: [1],
                                        heights: [5],
                                        body: [ [ { border: [false,false,false,false], fillColor: statusOpen, text: '' } ], ]
                                      }
                                    }]]
                                  }
                                },  
                              ]
                            },
                            { border: [false,false,false,false], alignment: 'center', text: 'Odprto', margin: [0,3,0,0], },
                            { border: [false,false,false,false], text: '' },
                            {
                              border: [false,false,false,false],
                              stack: [{
                                table: {
                                  widths: [7],
                                  heights: [8],
                                  body: [[{
                                    margin: [-2,0,0,0],
                                    table: {
                                      widths: [1],
                                      heights: [5],
                                      body: [ [ { border: [false,false,false,false], fillColor: statusDeclined, text: '' } ], ]
                                    }
                                  }]]
                                }
                              }]
                            },
                            { border: [false,false,false,false], alignment: 'center', text: 'Zavrnjeno', margin: [0,3,0,0], },
                            { border: [false,false,false,false], text: '' },
                            {
                              border: [false,false,false,false],
                              stack: [{
                                table: {
                                  widths: [7],
                                  heights: [8],
                                  body: [[{
                                    margin: [-2,0,0,0],
                                    table: {
                                      widths: [1],
                                      heights: [5],
                                      body: [[{ border: [false,false,false,false], fillColor: statusRunning, text: '' }],]
                                    }
                                  }],]
                                }
                              },]
                            },
                            { border: [false,false,false,false], alignment: 'center', text: 'V teku', margin: [0,3,0,0], },
                            { border: [false,false,false,false], text: '' },
                            {
                              border: [false,false,false,false],
                              stack: [{
                                table: {
                                  widths: [7],
                                  heights: [8],
                                  body: [[{
                                    margin: [-2,0,0,0],
                                    table: {
                                      widths: [1],
                                      heights: [5],
                                      body: [[{ border: [false,false,false,false], fillColor: statusClosed, text: '' }],]
                                    }
                                  }],]
                                }
                              },]
                            },
                            { border: [false,false,false,false], alignment: 'center', text: 'Rešeno', margin: [0,3,0,0], },
                            { border: [false,false,false,false], text: '' },
                          ],
                        ]
                      },
                    }]
                  },
                  {},
                  {},
                ],
                [
                  { colSpan: 3, fillColor: '#eeeeee', alignment: 'center', text: ' ' },
                  {},
                  {},
                ],
                [
                  { text: '' },
                  { text: 'Priimek in ime' },
                  { text: 'Datum' },
                ],
                [
                  { text: 'Pripravil:' },
                  { text: authorLabel },
                  { text: dateLabel },
                ],
              ]
            }, layout: { defaultBorder: true, } 
          },
        ],
        footer: [
          {
            style: 'footerTable',
            table: {
              widths: [480],
              body: [ [ { border: [false,true,false,false], text: 'OBR 02.04', alignment: 'right' }, ], ]
            },
            layout: { }
          },
        ],
        images: { roboteh: base64RobotehImg.base64, },
        styles: {
          mainTable: { margin: [ 0, 0, 0, 0 ], fontSize: 9 },
          checkbox: { margin: [ 0, 0, 0, 0 ] },
          errorTable: { margin: [ 0, -1, 0, 0 ] },
          obrInfo: { fontSize: 9, margin: [ -5,6, 0, 0 ] },
          footerTable: { fontSize: 9, margin: [59, 20, 0, 0], },
          headerTable: { heights: 100, margin: [57, 35, 0, 0] },
        },
      }

      let pdfDoc = printer.createPdfKitDocument(dd);
      const filename = encodeURI(pdfHeader);
      res.setHeader('Content-disposition', `attachment; filename*=UTF-8''${filename}.pdf; filename=${filename}.pdf`);
      res.setHeader('Content-type', 'application/pdf');
      pdfDoc.pipe(res);
      pdfDoc.end();
    })
    .catch((e)=>{
      //return res.json({success:false, error: e});
      next(e);
    })
  }
  else if (compSupId){
    // return pdf of complaint supplier

    let promises = [];
    promises.push(dbComplaints.getComplaintsSupplierId(compSupId));
    promises.push(dbComplaints.getComplaintsSupplierAuthor(compSupId));
    //get roboteh base64 image
    promises.push(dbBase64Images.getRobotehImage());

    Promise.all(promises).then(results => {
      let compSup = results[0];
      let author = results[1];
      let base64RobotehImg = results[2];
      let authorLabel = author.name + ' ' + author.surname;
      let dateLabel = dateFormat(new Date(author.date), "dd. mm. yyyy");
      let statusOpen = compSup.status_id == 1 ? '#000000' : '#ffffff';
      let statusDeclined = compSup.status_id == 2 ? '#000000' : '#ffffff';
      let statusRunning = compSup.status_id == 3 ? '#000000' : '#ffffff';
      let statusClosed = compSup.status_id == 4 ? '#000000' : '#ffffff';
      let pdfHeader = 'Reklamacija dobavitelju ' + compSup.complaint_number;
      let documentNumberLabel = 'Številka reklamacije:  ' + compSup.complaint_number;
      let supplierLabel = compSup.supplier ? compSup.supplier : ' ';
      let materialLabel = compSup.material ? compSup.material : ' ';
      let typeLabel = compSup.complaint_type ? compSup.complaint_type : ' ';
      let quantityLabel = compSup.quantity ? compSup.quantity : ' ';
      let valueLabel = compSup.value ? compSup.value : ' ';
      let justificationLabel = compSup.justification ? compSup.justification : ' ';

      let dd = {
        pageMargins: [60, 110, 50, 70],
        header: [
          {
            style: 'headerTable',
            table: {
              widths: [150,340],
              body: [
                [
                  { border: [false,false,false,false], image: 'roboteh', width: 150 },
                  {
                    style: 'obrInfo',
                    table: {
                      widths: [175, 60, 70],
                      body: [
                        [
                          {border: [true,false,true,true],text: ['Identifikacijska številka:    ',{text: 'OBR 02.05', bold: true},],},
                          {border: [true,false,true,true],text: ['Izdaja:  ',{text: '01', bold: true},]},
                          {border: [true,false,true,true],alignment: 'center',text: ['Stran ',{text: '1', bold: true},' od ',{text: '1', bold: true},]},
                        ],
                        [
                          {border: [true,true,true,false],colSpan: 3,text: ['Naziv dokumenta: ',{text: '  REKLAMACIJA DOBAVITELJU', bold: true},],},
                        ]
                      ]
                    },
                  },
                ]
              ]
            },
            layout: { defaultBorder: false,}
          },
        ],
        content: [
          {
            style: 'mainTable',
            table: {
              widths: [110,'*',140],
              heights: ['*', 17, '*', 17, '*',300, '*', 50],
              body: [
                [
                  {colSpan: 3,fontSize: 11,fillColor: '#eeeeee',text: [{text: '  REKLAMACIJA DOBAVITELJU', bold: true},],},
                  {},
                  {},
                ],
                [
                  {colSpan: 3,text: [{text: documentNumberLabel, bold: true},],margin: [0,3,0,0],},
                  {},
                  {},
                ],
                [
                  {colSpan: 3,fillColor: '#eeeeee',text: ' '},
                  {},
                  {},
                ],
                [
                  {text: [{text: 'Dobavitelj:', bold: true},],margin: [0,3,0,0],},
                  {preserveLeadingSpaces: true,colSpan: 2,text: supplierLabel,margin: [0,3,0,0],},
                  {text: 'test',alignment: 'center',preserveLeadingSpaces: true},
                ],
                [
                  {colSpan: 3,fillColor: '#eeeeee',alignment: 'center',text: {text: 'Vhodni material / storitev',bold: true}},
                  {},
                  {},
                ],
                [
                  {colSpan: 3,text: materialLabel,preserveLeadingSpaces: true},
                  {},
                  {},
                ],
                [
                  {colSpan: 3,fillColor: '#eeeeee',alignment: 'center',text: {text: 'Vrsta reklamacije',bold: true}},
                  {},
                  {},
                ],
                [
                  {colSpan: 3,text: typeLabel,preserveLeadingSpaces: true},
                  {},
                  {},
                ],
                [
                  {colSpan: 3,fillColor: '#eeeeee',alignment: 'center',text: ' '},
                  {},
                  {},
                ],
                [
                  {text: 'Upravičenost (ukrep):'},
                  {colSpan: 2,text: justificationLabel},
                  {},
                ],
                [
                  {text: 'Reklamirana količina:'},
                  {colSpan: 2,text: quantityLabel},
                  {},
                ],
                [
                  {text: 'Vrednost reklamacije:'},
                  {colSpan: 2,text: valueLabel},
                  {},
                ],
                [
                  {colSpan: 3,fillColor: '#eeeeee',alignment: 'center',text: {text: 'Status',bold: true}},
                  {},
                  {},
                ],
                [
                  {
                    colSpan:3,
                    stack: [{
                      table: {
                        widths: [40,15,'*',10,15,'*',10,15,'*',10,15,'*',40],
                        body: [[
                          {border: [false,false,false,false],text: ''},
                          {
                            border: [false,false,false,false],
                            stack: [{
                              table: {
                                widths: [7],
                                heights: [8],
                                body: [[{
                                  margin: [-2,0,0,0],
                                  table: {
                                    widths: [1],
                                    heights: [5],
                                    body: [[{border: [false,false,false,false],fillColor: statusOpen,text: ''}],]
                                  }
                                }],]
                              }
                            },]
                          },
                          { border: [false,false,false,false],alignment: 'center',text: 'Odprto',margin: [0,3,0,0],},
                          { border: [false,false,false,false], text: ''},
                          {
                            border: [false,false,false,false],
                            stack: [{
                              table: {
                                widths: [7],
                                heights: [8],
                                body: [[{
                                  margin: [-2,0,0,0],
                                  table: {
                                    widths: [1],
                                    heights: [5],
                                    body: [[{border: [false,false,false,false],fillColor: statusDeclined,text: ''}],]
                                  }
                                }],]
                              }
                            },]
                          },
                          {border: [false,false,false,false],alignment: 'center',text: 'Zavrnjeno',margin: [0,3,0,0],},
                          {border: [false,false,false,false],text: ''},
                          {
                            border: [false,false,false,false],
                            stack: [{
                              table: {
                                widths: [7],
                                heights: [8],
                                body: [[{
                                  margin: [-2,0,0,0],
                                  table: {
                                    widths: [1],
                                    heights: [5],
                                    body: [[{border: [false,false,false,false],fillColor: statusRunning,text: ''}],]
                                  }
                                }],]
                              }
                            },]
                          },
                          {border: [false,false,false,false],alignment: 'center',text: 'V teku',margin: [0,3,0,0],},
                          {border: [false,false,false,false],text: ''},
                          {
                            border: [false,false,false,false],
                            stack: [{
                              table: {
                                widths: [7],
                                heights: [8],
                                body: [[{
                                  margin: [-2,0,0,0],
                                  table: {
                                    widths: [1],
                                    heights: [5],
                                    body: [[{border: [false,false,false,false],fillColor: statusClosed,text: ''}],]
                                  }
                                }],]
                              }
                            },]
                          },
                          {border: [false,false,false,false],alignment: 'center',text: 'Rešeno',margin: [0,3,0,0],},
                          {border: [false,false,false,false],text: ''},
                        ],]
                      },
                    }]
                  },
                  {},
                  {},
                ],
                [
                  {colSpan: 3,fillColor: '#eeeeee',alignment: 'center',text: ' '},
                  {},
                  {},
                ],
                [
                  {text: ''},
                  {text: 'Priimek in ime'},
                  {text: 'Datum'},
                ],
                [
                  {text: 'Pripravil:'},
                  {text: authorLabel},
                  {text: dateLabel},
                ],
              ]
            },layout: {defaultBorder: true,}
          },
        ],
        footer: [{
          style: 'footerTable',
          table: {widths: [480],body: [[{border: [false,true,false,false],text: 'OBR 02.05',alignment: 'right'},],]},
          layout: {}
        },],
        images: {roboteh: base64RobotehImg.base64,},
        styles: {
          mainTable: {margin: [ 0, 0, 0, 0 ],fontSize: 9},
          checkbox: {margin: [ 0, 0, 0, 0 ]},
          errorTable: {margin: [ 0, -1, 0, 0 ]},
          obrInfo: {fontSize: 9,margin: [ -5,6, 0, 0 ]},
          footerTable: {fontSize: 9,margin: [59, 20, 0, 0],},
          headerTable: {heights: 100,margin: [57, 35, 0, 0]},
        },
      }

      let pdfDoc = printer.createPdfKitDocument(dd);
      const filename = encodeURI(pdfHeader);
      res.setHeader('Content-disposition', `attachment; filename*=UTF-8''${filename}.pdf; filename=${filename}.pdf`);
      res.setHeader('Content-type', 'application/pdf');
      pdfDoc.pipe(res);
      pdfDoc.end();
    })
    .catch((e)=>{
      //return res.json({success:false, error: e});
      next(e);
    })
  }
  else{
    // no id, return error
    return next();
  }
})

module.exports = router;