var compression = require('compression');
var createError = require('http-errors');
var express = require('express');
var cors = require('cors');
const csp = require('helmet-csp');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
var session = require('cookie-session');
var helmet = require('helmet');
var favicon = require('serve-favicon');
var bodyParser = require('body-parser')
var shell = require('shelljs');
var fs = require('fs');
var uglifyJS = require("uglify-es");
var uglifycss = require('uglifycss');
require('dotenv').config()
var bitbucketUsername = 'sibalk';

// Load cron jobs
require('./cron/dailyEmailNotification');

//UGLIFY
//JS
//timeline
/*
var timelineUglify = uglifyJS.minify({
  'timeline.js': fs.readFileSync('public/timelines-chart/dist/timelines-chart.js', 'utf-8')
});
fs.writeFile('public/timelines-chart/dist/timeline.min.js', timelineUglify.code, function(napaka) {
  if(napaka) console.log(napaka);
  else console.log('Skripta je zgenerirana in shranjena v "timeline.min.js"');
})
*/
////////////////////////////////////////////////////////////////////////////////////////UGLIFY FOR TESTING AND MASTER
//daily

var dailyUglify = uglifyJS.minify({
  'daily.js': fs.readFileSync('public/javascripts/daily/daily.js', 'utf-8')
});
fs.writeFile('public/javascripts/daily/daily.min.js', dailyUglify.code, function(napaka) {
  if (napaka) console.log(napaka);
  else console.log('Skripta je zgenerirana in shranjena v "daily.min.js".');
});
//notifications/changes
var notificationUglify = uglifyJS.minify({
  'notificationBar.js': fs.readFileSync('public/javascripts/notifications/notificationBar.js', 'utf-8')
});
fs.writeFile('public/javascripts/notifications/notifications.min.js', notificationUglify.code, function(napaka) {
  if (napaka) console.log(napaka);
  else console.log('Skripta je zgenerirana in shranjena v "notifications.min.js".');
});
//notifications/ctrlpanel
var ctrlpanel = uglifyJS.minify({
  'ctrlpanel.js': fs.readFileSync('public/javascripts/changes/ctrlpanel.js', 'utf-8')
});
fs.writeFile('public/javascripts/changes/ctrlpanel.min.js', ctrlpanel.code, function(napaka) {
  if (napaka) console.log(napaka);
  else console.log('Skripta je zgenerirana in shranjena v "ctrlpanel.min.js".');
});
var employeeUglify = uglifyJS.minify({
  'change.js': fs.readFileSync('public/javascripts/employee/change.js', 'utf-8'),
  'projects.js': fs.readFileSync('public/javascripts/employee/projects.js', 'utf-8'),
});
fs.writeFile('public/javascripts/employee/employee.min.js', employeeUglify.code, function(napaka) {
  if (napaka) console.log(napaka);
  else console.log('Skripta je zgenerirana in shranjena v "employee.min.js".');
});
//employees
var employeesUglify = uglifyJS.minify({
  'manage.js': fs.readFileSync('public/javascripts/employee/manage.js', 'utf-8'),
});
fs.writeFile('public/javascripts/employee/employees.min.js', employeesUglify.code, function(napaka) {
  if (napaka) console.log(napaka);
  else console.log('Skripta je zgenerirana in shranjena v "employees.min.js".');
});
//project
var projectUglify = uglifyJS.minify({
  'project.js': fs.readFileSync('public/javascripts/project/project.js', 'utf-8'),
  'tasks.js': fs.readFileSync('public/javascripts/project/tasks.js', 'utf-8'),
  'workers.js': fs.readFileSync('public/javascripts/project/workers.js', 'utf-8'),
  'absences.js': fs.readFileSync('public/javascripts/project/absences.js', 'utf-8'),
  'subtasks.js': fs.readFileSync('public/javascripts/project/subtasks.js', 'utf-8'),
  'files.js': fs.readFileSync('public/javascripts/project/files.js', 'utf-8'),
  'viz.js': fs.readFileSync('public/javascripts/project/viz.js', 'utf-8'),
  'pdf.js': fs.readFileSync('public/javascripts/project/pdf.js', 'utf-8'),
  'dates.js': fs.readFileSync('public/javascripts/project/dates.js', 'utf-8'),
  'messages.js': fs.readFileSync('public/javascripts/messages/messages.js', 'utf-8'),
  'buildparts.js': fs.readFileSync('public/javascripts/project/buildparts.js', 'utf-8'),
  'buildphases.js': fs.readFileSync('public/javascripts/project/buildphases.js', 'utf-8'),
  'buildpartstask.js': fs.readFileSync('public/javascripts/project/buildpartstask.js', 'utf-8'),
  'buildpartdocpdf.js': fs.readFileSync('public/javascripts/project/buildpartdocpdf.js', 'utf-8'),
  'modifications.js': fs.readFileSync('public/javascripts/project/modifications.js', 'utf-8'),
});
fs.writeFile('public/javascripts/project/project.min.js', projectUglify.code, function(napaka) {
  if (napaka) console.log(napaka);
  else console.log('Skripta je zgenerirana in shranjena v "project.min.js".');
});
//projects
var projectsUglify = uglifyJS.minify({
  'projects.js': fs.readFileSync('public/javascripts/project/projects.js', 'utf-8'),
  'projectsViz.js': fs.readFileSync('public/javascripts/project/projectsViz.js', 'utf-8'),
  'projectsMobiscroll.js': fs.readFileSync('public/javascripts/project/projectsMobiscroll.js', 'utf-8'),
});
fs.writeFile('public/javascripts/project/projects.min.js', projectsUglify.code, function(napaka) {
  if (napaka) console.log(napaka);
  else console.log('Skripta je zgenerirana in shranjena v "projects.min.js".');
});
//servis
var servisUglify = uglifyJS.minify({
  'servis.js': fs.readFileSync('public/javascripts/servis/servis.js', 'utf-8'),
  'absences.js': fs.readFileSync('public/javascripts/servis/absences.js', 'utf-8'),
  'files.js': fs.readFileSync('public/javascripts/servis/files.js', 'utf-8'),
  'subtasks.js': fs.readFileSync('public/javascripts/servis/subtasks.js', 'utf-8'),
  'tasks.js': fs.readFileSync('public/javascripts/servis/tasks.js', 'utf-8'),
  'messages.js': fs.readFileSync('public/javascripts/messages/messages.js', 'utf-8'),
});
fs.writeFile('public/javascripts/servis/servis.min.js', servisUglify.code, function(napaka) {
  if (napaka) console.log(napaka);
  else console.log('Skripta je zgenerirana in shranjena v "servis.min.js".');
});
//subscribers
var subscribersUglify = uglifyJS.minify({
  'manageSubscribers.js': fs.readFileSync('public/javascripts/subscriber/manageSubscribers.js', 'utf-8')
});
fs.writeFile('public/javascripts/subscriber/subscribers.min.js', subscribersUglify.code, function(napaka) {
  if (napaka) console.log(napaka);
  else console.log('Skripta je zgenerirana in shranjena v "subscribers.min.js".');
});
//tasks
var tasksUglify = uglifyJS.minify({
  'tasks.js': fs.readFileSync('public/javascripts/employee/tasks.js', 'utf-8'),
  'change.js': fs.readFileSync('public/javascripts/employee/change.js', 'utf-8'),
  'commonTasks.js': fs.readFileSync('public/javascripts/employee/commonTasks.js', 'utf-8'),
  'commonSubtasks.js': fs.readFileSync('public/javascripts/employee/commonSubtasks.js', 'utf-8'),
  'commonFiles.js': fs.readFileSync('public/javascripts/employee/commonFiles.js', 'utf-8'),
  'commonAbsence.js': fs.readFileSync('public/javascripts/employee/commonAbsence.js', 'utf-8'),
  'commonVis.js': fs.readFileSync('public/javascripts/employee/commonVis.js', 'utf-8'),
  'messages.js': fs.readFileSync('public/javascripts/messages/messages.js', 'utf-8'),
  'vizTasks.js': fs.readFileSync('public/javascripts/employee/vizTasks.js', 'utf-8'),
});
fs.writeFile('public/javascripts/employee/tasks.min.js', tasksUglify.code, function(napaka) {
  if (napaka) console.log(napaka);
  else console.log('Skripta je zgenerirana in shranjena v "tasks.min.js".');
});
//visualisation
var vizUglify = uglifyJS.minify({
  'viz.js': fs.readFileSync('public/javascripts/viz/viz.js', 'utf-8'),
  'vizProject.js': fs.readFileSync('public/javascripts/viz/vizProject.js', 'utf-8'),
  'vizProjects.js': fs.readFileSync('public/javascripts/viz/vizProjects.js', 'utf-8'),
  'vizAssignments.js': fs.readFileSync('public/javascripts/viz/vizAssignments.js', 'utf-8')
});
fs.writeFile('public/javascripts/viz/viz.min.js', vizUglify.code, function(napaka) {
  if (napaka) console.log(napaka);
  else console.log('Skripta je zgenerirana in shranjena v "viz.min.js".');
});
//absences
// var absencesUglify = uglifyJS.minify({
//   'absences.js': fs.readFileSync('public/javascripts/absences/absences.js', 'utf-8'),
//   'change.js': fs.readFileSync('public/javascripts/employee/change.js', 'utf-8'),
// });
// fs.writeFile('public/javascripts/absences/absences.min.js', absencesUglify.code, function(napaka) {
//   if (napaka) console.log(napaka);
//   else console.log('Skripta je zgenerirana in shranjena v "absences.min.js".');
// });
var absencesUglify = uglifyJS.minify({
  'absences.js': fs.readFileSync('public/javascripts/absences/absencesnew.js', 'utf-8'),
});
fs.writeFile('public/javascripts/absences/absences.min.js', absencesUglify.code, function(napaka) {
  if (napaka) console.log(napaka);
  else console.log('Skripta je zgenerirana in shranjena v "absences.min.js".');
});
// //calendar-overview
// var calendarOverviewUglify = uglifyJS.minify({
//   'overview.js': fs.readFileSync('public/javascripts/calendar/overview.js', 'utf-8'),
//   'tasks.js': fs.readFileSync('public/javascripts/calendar/tasks.js', 'utf-8'),
//   'subtasks.js': fs.readFileSync('public/javascripts/calendar/subtasks.js', 'utf-8'),
//   'messages.js': fs.readFileSync('public/javascripts/messages/messages.js', 'utf-8'),
// });
// fs.writeFile('public/javascripts/calendar/overview.min.js', calendarOverviewUglify.code, function(napaka) {
//   if (napaka) console.log(napaka);
//   else console.log('Skripta je zgenerirana in shranjena v "overview.min.js".');
// });
// //calendar-info
// var calendarInfoUglify = uglifyJS.minify({
//   'info.js': fs.readFileSync('public/javascripts/calendar/info.js', 'utf-8')
// });
// fs.writeFile('public/javascripts/calendar/info.min.js', calendarInfoUglify.code, function(napaka) {
//   if (napaka) console.log(napaka);
//   else console.log('Skripta je zgenerirana in shranjena v "info.min.js".');
// });
//report
var reportUglify = uglifyJS.minify({
  'reports.js': fs.readFileSync('public/javascripts/reports/reports.js', 'utf-8')
});
fs.writeFile('public/javascripts/reports/reports.min.js', reportUglify.code, function(napaka) {
  if (napaka) console.log(napaka);
  else console.log('Skripta je zgenerirana in shranjena v "reports.min.js".');
});
//work order
var workOrderUglify = uglifyJS.minify({
  'wo.js': fs.readFileSync('public/javascripts/orders/wo.js', 'utf-8'),
  'files.js': fs.readFileSync('public/javascripts/orders/files.js', 'utf-8'),
  // 'pdf.js': fs.readFileSync('public/javascripts/orders/pdf.js', 'utf-8'),
  'sign.js': fs.readFileSync('public/javascripts/orders/sign.js', 'utf-8'),
  'geolocation.js': fs.readFileSync('public/javascripts/orders/geolocation.js', 'utf-8'),
});
fs.writeFile('public/javascripts/orders/wo.min.js', workOrderUglify.code, function(napaka) {
  if (napaka) console.log(napaka);
  else console.log('Skripta je zgenerirana in shranjena v "wo.min.js".');
});
//meetings
var meetingsUglify = uglifyJS.minify({
  'manage.js': fs.readFileSync('public/javascripts/meetings/manage.js', 'utf-8'),
  'meetings.js': fs.readFileSync('public/javascripts/meetings/meetings.js', 'utf-8'),
  'activities.js': fs.readFileSync('public/javascripts/meetings/activities.js', 'utf-8'),
});
fs.writeFile('public/javascripts/meetings/meetings.min.js', meetingsUglify.code, function(napaka) {
  if (napaka) console.log(napaka);
  else console.log('Skripta je zgenerirana in shranjena v "meetings.min.js".');
});
//complaints
var complaintsUglify = uglifyJS.minify({
  'complaints.js': fs.readFileSync('public/javascripts/complaints/complaints.js', 'utf-8'),
  'compSub.js': fs.readFileSync('public/javascripts/complaints/compSub.js', 'utf-8'),
  'compSup.js': fs.readFileSync('public/javascripts/complaints/compSup.js', 'utf-8'),
  'compFiles.js': fs.readFileSync('public/javascripts/complaints/compFiles.js', 'utf-8'),
});
fs.writeFile('public/javascripts/complaints/complaints.min.js', complaintsUglify.code, function(napaka) {
  if (napaka) console.log(napaka);
  else console.log('Skripta je zgenerirana in shranjena v "complaints.min.js".');
});
//std comps
var stdCompsUglify = uglifyJS.minify({
  'stdComponents.js': fs.readFileSync('public/javascripts/components/stdComponents.js', 'utf-8'),
});
fs.writeFile('public/javascripts/components/components.min.js', stdCompsUglify.code, function(napaka) {
  if (napaka) console.log(napaka);
  else console.log('Skripta je zgenerirana in shranjena v "components.min.js".');
});
//login
var loginUglify = uglifyJS.minify({
  'login.js': fs.readFileSync('public/javascripts/login/login.js', 'utf-8')
});
fs.writeFile('public/javascripts/login/login.min.js', loginUglify.code, function(napaka) {
  if (napaka) console.log(napaka);
  else console.log('Skripta je zgenerirana in shranjena v "login.min.js".');
});
// calendar mobiscroll
var calendarMobiUglify = uglifyJS.minify({
  'mobiscroll.js': fs.readFileSync('public/javascripts/calendar/mobiscroll.js', 'utf-8'),
});
fs.writeFile('public/javascripts/calendar/mobiscroll.min.js', calendarMobiUglify.code, function(napaka) {
  if (napaka) console.log(napaka);
  else console.log('Skripta je zgenerirana in shranjena v "mobiscroll.min.js".');
});
//cars reservations callendar
var carsRevCalendarUglify = uglifyJS.minify({
  'carRevs.js': fs.readFileSync('public/javascripts/cars/carRevs.js', 'utf-8'),
  'carNotes.js': fs.readFileSync('public/javascripts/cars/carNotes.js', 'utf-8'),
  'carManage.js': fs.readFileSync('public/javascripts/cars/carManage.js', 'utf-8'),
});
fs.writeFile('public/javascripts/cars/carsCalendar.min.js', carsRevCalendarUglify.code, function(napaka) {
  if (napaka) console.log(napaka);
  else console.log('Skripta je zgenerirana in shranjena v "carsCalendar.min.js".');
});
//CSS
var stil = uglifycss.processFiles(
  [ 'public/stylesheets/style.css'],
  { expandVars: true }
);
fs.writeFile('public/stylesheets/style.min.css', stil, function(napaka) {
  if (napaka)
    console.log(napaka);
  else
    console.log('Skripta je zgenerirana in shranjena v "style.min.css".');
});

//var feedbackRouter = require('./routes/feedback');
var employeesRouter = require('./routes/employees');
var projectRouter = require('./routes/projects');
var loginRouter = require('./routes/login');
var subscribersRouter = require('./routes/subscribers');
var dailyRouter = require('./routes/daily');
var calendarRouter = require('./routes/calendar');
var servisRouter = require('./routes/servis');
var vizRouter = require('./routes/viz');
var changesRouter = require('./routes/changes');
// var absenceRouter = require('./routes/absence');
var absencesRouter = require('./routes/absences');
var messagesRouter = require('./routes/messages');
var reportsRouter = require('./routes/reports');
var workOrdersRouter = require('./routes/workorder');
var buildPartsRouter = require('./routes/buildparts');
// var apiRouter = require('./routes/API/api');
var apiV2Router = require('./routes/APIV2/api');
var instrRouter = require('./routes/instructions');
var meetingsRouter = require('./routes/meetings');
var complaintsRouter = require('./routes/complaints');
var stdComponentsRouter = require('./routes/standardcomponents');
var carsRouter = require('./routes/cars');

var app = express();
app.use(helmet());
var allowedOrigins = process.env.WHITELIST || ['https://project.roboteh.si'];
app.use(cors({
  origin: function(origin, callback){
    // allow requests with no origin 
    // (like mobile apps or curl requests)
    if(!origin) return callback(null, true);
    if(allowedOrigins.indexOf(origin) === -1){
      var msg = 'The CORS policy for this site does not ' +
                'allow access from the specified Origin.';
      return callback(new Error(msg), false);
    }
    return callback(null, true);
  }
}));

var normalCspHandler = csp({
  directives: {
    defaultSrc: ["'self'"],
    scriptSrc: ["'self'", "'unsafe-inline'", "'unsafe-eval'"],
    styleSrc: ["'self'", "'unsafe-inline'", "https://fonts.googleapis.com", "https://use.fontawesome.com"],
    fontSrc: ["'self'", "https://fonts.gstatic.com", "https://use.fontawesome.com"],
    imgSrc: ["'self'", "data:"],
    frameAncestors: ["'self'"],
  }
});

var loginCspHandler = csp({
  directives: {
    defaultSrc: ["'self'"],
    scriptSrc: ["'self'"],
    styleSrc: ["'self'", "https://fonts.googleapis.com"],
    fontSrc: ["'self'", "https://fonts.gstatic.com"],
    frameAncestors: ["'none'"],
  }
});
app.use(normalCspHandler);
app.use(function(req, res, next) {
  res.setHeader('X-Frame-Options', 'SAMEORIGIN');
  res.setHeader('X-XSS-Protection', '1; mode=block');
  res.setHeader('X-Content-Type-Options', 'nosniff');
  next();
});
app.use(compression());
app.set('trust proxy', '::ffff:127.0.0.1');

//var db = require('./controllers/db');
var dbFiles = require('./model/files/dbFiles');
//var auth = require('./controllers/authentication');
// create application/json parser
var jsonParser = bodyParser.json()

// create application/x-www-form-urlencoded parser
var urlencodedParser = bodyParser.urlencoded({ extended: false })

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');


app.post("/webhooks/bitbucket", jsonParser, function(req, res){
  if(!req.body) return res.sendStatus(400);
  console.log("Webhook received!");
  //console.log(req.body);
  console.log("Repo is " + req.body.repository.name);
  var sender = req.body.actor.nickname;
  console.log("User is " + req.body.actor.nickname);
  //console.log("Branch that is commited is " + req.body.push.changes.commit.new.name);
  
  req.body.push.changes.forEach(function (commit) {
      console.log("Branch/Tag is " + commit.new.name);
      var branch = commit.new.name;
      console.log("Type is " + commit.new.type);
      if(branch === 'master' && sender === bitbucketUsername){
        console.log("BRANCH and USERNAME are matching, lets pull/update the code");
        deploy(res);
      }
      else{
        console.log("BRANCH and USERNAME ARE NOT MATCHING");
        res.sendStatus(500);
      }
  });
})
function deploy(res){
  
  shell.exec('echo "Pulling from master"');
  shell.exec('git pull');
  shell.exec('echo "Installing new packages if there are new"');
  shell.exec('npm install')
  shell.exec('echo "About to restart process"');
  shell.exec('pm2 start www', function(code, stdout, stderr){
    console.log('Exit code' + code);
    console.log('Program output' + stdout);
    console.log('Program stderr:' + stderr);
    res.sendStatus(500);
  });
  res.sendStatus(200);
}

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, '/public')));
app.use(express.static(path.join(__dirname, 'icons')));
//app.use('/scripts', express.static(__dirname + '/timelines-chart/dist/'));

/*
app.use(function(req, res, next) {
  res.setHeader('X-Frame-Options', 'DENY');
  res.setHeader('X-XSS-Protection', '1; mode=block');
  res.setHeader('X-Content-Type-Options', 'nosniff');
  next();
});
*/

app.use(favicon(path.join(__dirname, 'icons', 'icon.png')))
//app.use(favicon(__dirname + '/icons/favicon.ico')); 

app.use(session({
  name: 'session',
  domain: process.env.DOMAIN || 'project.roboteh.si',
  secret: process.env.APP_SECRET,
  maxAge: 10*60*60*1000,
  sameSite: "lax",
}))
//Extending the session expiration
// app.use(function (req, res, next) {
//   req.session.nowInMinutes = Math.floor(Date.now() / 60e3)
//   next()
// })

//app.use('/', indexRouter);
//app.use('/dashboard', dashboardRouter);
//app.use('/my', myRouter);
app.use('/', loginRouter);
app.use('/employees', employeesRouter);
app.use('/projects', projectRouter);
app.use('/subscribers', subscribersRouter);
app.use('/daily', dailyRouter);
app.use('/servis', servisRouter);
app.use('/viz', vizRouter);
app.use('/changes', changesRouter);
// app.use('/absence', absenceRouter);
app.use('/absences', absencesRouter);
app.use('/messages', messagesRouter);
app.use('/calendar', calendarRouter);
app.use('/reports', reportsRouter);
app.use('/workorders', workOrdersRouter);
app.use('/buildparts', buildPartsRouter);
// app.use('/api', apiRouter);
app.use('/apiv2', apiV2Router);
app.use('/instructions', instrRouter);
app.use('/meetings', meetingsRouter);
app.use('/complaints', complaintsRouter);
app.use('/components', stdComponentsRouter);
app.use('/cars', carsRouter);

// catch 404 and forward to error handler
app.use(loginCspHandler, function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};
  if(err.message.substring(0,33) == 'ENOENT: no such file or directory')
    res.locals.message = 'Datoteka ' + err.message.substring(41, err.message.length-1) + ' ne obstaja.';
  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;
